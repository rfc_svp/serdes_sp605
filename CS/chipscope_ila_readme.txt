The following files were generated for 'chipscope_ila' in directory
C:\Users\rfernandezc\Desktop\SVP_FW\CS\

ISE file generator:
   Add description here...

   * chipscope_ila_flist.txt

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * chipscope_ila.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * chipscope_ila.cdc
   * chipscope_ila.ejp
   * chipscope_ila.ncf
   * chipscope_ila.ngc
   * chipscope_ila.vhd
   * chipscope_ila.vho
   * chipscope_ila_xmdf.tcl

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * chipscope_ila.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * chipscope_ila.asy

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * _xmsgs/pn_parser.xmsgs
   * chipscope_ila.gise
   * chipscope_ila.xise

Deliver Readme:
   Readme file for the IP.

   * chipscope_ila_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * chipscope_ila_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

