library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.text_util_common_pkg.all;
use work.utils_common_pkg.all;
use work.sim_AMC_pkg.all;
use work.sim_common_pkg.all;

entity userserial_tb is
generic(BAUD_RATE : INTEGER := 9600);		-- 19200 bps, to simulate set to 1000000
port (
		enable_tb		: in std_logic:='0';
		complete_tb		: out std_logic;
		UserSerialTx 	: out std_logic;
		UserSerialRx 	: in std_logic		 
	 );
end userserial_tb;

architecture tb of userserial_tb is


-- {b.1} Baud rate
--constant BAUD_RATE : integer := 9600;--115200;
-- Baud period
constant BIT_PERIOD : time := (1.0/real(BAUD_RATE))*(1 sec);
constant CHARGAP : time := 1 * BIT_PERIOD;

signal response_o,response_i	: std_logic_vector(7 downto 0):=(others=>'0');
signal response_we,response_re	: std_logic:='0';
signal FifoEmpty				: std_logic;

constant timeout_max	: natural := 20;
signal timeout_cnt		: integer range 0 to timeout_max;
signal byte_clk			: std_logic:='0';

begin

process

	procedure queue_response (data : byte_vector) is
	begin

		for i in 0 to data'length-1 loop
			response_i<=data(i);
			response_we<='1';
			wait for 1 ps;
			response_we<='0';
			wait for 1 ps;
		end loop;

	end procedure;

	procedure WRITE_UART ( 	u_address			: string (4 downto 1);
							u_data				: string (4 downto 1);
							fault_in_writting	: boolean) is
		variable write_command_msg:string(1 to 13);
		variable write_data_ok:string(1 to 3);
		variable write_data_ko:string(1 to 4);
	begin
	
		write_command_msg:=('W',u_address(4),u_address(3),u_address(2),u_address(1),
							',',u_data(4),u_data(3),u_data(2),u_data(1),
							'?',CR,LF);
							
		print("UART CMD TX : " & write_command_msg);
							
		for i in 1 to 13 loop
			serial_tx(UserSerialTx,BIT_PERIOD,false,char_to_ascii(write_command_msg(i)));
			wait for CHARGAP;	
		end loop;	
	
		-- expected response
		if fault_in_writting=false then
			write_data_ok := 'Y' & CR & LF;
			--print("UART RX : " & write_data_ok);
			queue_response((char_to_ascii(write_data_ok(1)),char_to_ascii(write_data_ok(2)),char_to_ascii(write_data_ok(3))));
		else
			write_data_ko := 'N' & '0' & CR & LF;
			--print("UART RX : " & write_data_ko);
			queue_response((char_to_ascii(write_data_ko(1)),char_to_ascii(write_data_ko(2)),
							char_to_ascii(write_data_ko(3)),char_to_ascii(write_data_ko(4))));
		end if;			
		
	end procedure;

	procedure READ_UART (	u_address			: string (4 downto 1);
							expected_data		: string (4 downto 1);
							fault_in_reading	: boolean) is
		variable read_command_msg:string(1 to 8);
		variable read_data_ok:string(1 to 7);
		variable read_data_ko:string(1 to 4);	
	begin
	
		read_command_msg:=(	'R',u_address(4),u_address(3),u_address(2),u_address(1),
							'?',CR,LF);
							
		print("UART CMD TX : " & read_command_msg);
							
		for i in 1 to 8 loop
			serial_tx(UserSerialTx,BIT_PERIOD,false,char_to_ascii(read_command_msg(i)));
			wait for CHARGAP;
			-- DEBUG -- assert FALSE report "Byte sent" severity note;
		end loop;		

		-- expected response
		if fault_in_reading=false then
			read_data_ok := expected_data & 'Y' & CR & LF;
			--print("UART RX : " & read_data_ok);
			queue_response((char_to_ascii(read_data_ok(1)),char_to_ascii(read_data_ok(2)),char_to_ascii(read_data_ok(3)),
							char_to_ascii(read_data_ok(4)),char_to_ascii(read_data_ok(5)),char_to_ascii(read_data_ok(6)),
							char_to_ascii(read_data_ok(7))));
		else
			read_data_ko := 'N' & '0' & CR & LF;
			--print("UART RX : " & read_data_ko);
			queue_response((char_to_ascii(read_data_ko(1)),char_to_ascii(read_data_ko(2)),
							char_to_ascii(read_data_ko(3)),char_to_ascii(read_data_ko(4))));
		end if;	
		
	end procedure;						


begin

	complete_tb<='0';
	wait until enable_tb'event and enable_tb='1';

	-------------------------------------
	-- READ VERSION INJ
	--
	READ_UART("9002","0001",false);
	
	-------------------------------------
	-- FCI 0 START
	--
	-- WRITE_UART("0000","0001",true);
	-- READ_UART("0000","0001",false);
	
	wait for 10*(10*BIT_PERIOD);
	
	-------------------------------------
	-- CTC 0 START LINK
	--
	
	-- WRITE_UART("9000","0001",true);
	--READ_UART("9000","0001",true);
	--WRITE_UART("5004","0000",true);
	

	
	
	wait for 10*(10*BIT_PERIOD);
	
	assert_proc(FifoEmpty,'1',"UserSerial didn't respond");
	
	complete_tb<='1';	

	wait;

end process;

process
variable false: boolean;
variable data: std_logic_vector(7 downto 0);
begin
	while(true) loop
		--
		serial_rx(UserSerialRx, BIT_PERIOD, false, data);
		--
		if conv_integer(data)<32 or conv_integer(data)=127 then
			print(HT&"UART BYTE RX : "&ctrl_ascii(conv_integer(data)));
			--print("expected->"&ctrl_ascii(conv_integer(response_o)));
		else
			print(HT&"UART BYTE RX : "&ascii(conv_integer(data)));
			--print("expected->"&ascii(conv_integer(response_o)));		
		end if;
		--
		assert_proc(ascii(conv_integer(data)),ascii(conv_integer(response_o)),"UserSerial received byte error");
		response_re<='1';
		wait for 1 ps;
		response_re<='0';		
		wait for 1 ps;
	end loop;
end process;

response_fifo : entity work.fifo_sim
	generic map(Bit_Wide=>8,Deep=>256)	
	Port map( 
		    Reset_n	 	=> enable_tb,
		    WriteEnable  => response_we,
		    ReadEnable   => response_re,
		    DataIn       => response_i,
		    DataOut      => response_o,
   		    FifoEmpty    => FifoEmpty,
		    FifoFull     => open
    );
	
rx_timeout:process(byte_clk,enable_tb,response_re)
begin
	if enable_tb='0' or response_re='1' then
		timeout_cnt<=0;
	elsif rising_edge(byte_clk) then
		if timeout_cnt<timeout_max then
			timeout_cnt<=timeout_cnt+1;
		else
			assert false report "UserSerial rx timeout"
			severity failure;
		end if;
	end if;
end process;

process
begin
	wait for 5*BIT_PERIOD;
	byte_clk <= not byte_clk;
end process;

end tb;