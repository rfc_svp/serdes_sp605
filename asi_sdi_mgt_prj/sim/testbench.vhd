library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use std.textio.all;
use work.txt_util.all;

library modelsim_lib;
use modelsim_lib.util.all;

entity testbench is 
end;



architecture BHU of testbench is

-------------------------------------------------
-- component port listings
-------------------------------------------------

component asdi_mgt 
generic(
   EXAMPLE_SIM_GTPRESET_SPEEDUP  : integer  :=   1;
   EXAMPLE_USE_CHIPSCOPE         : integer  :=   1;
   EXAMPLE_SIMULATION            : integer  :=   0 
);
port
(

  --*******************SDI**************************************
  TILE0_GTP0_REFCLK_PAD_N_IN:   in  std_logic;              -- GTP
  TILE0_GTP0_REFCLK_PAD_P_IN:   in  std_logic;          
  USER_SMA_GPIO_P:              out std_logic;          
  USER_SMA_GPIO_N:              out std_logic;          
  GPIO_SWITCH_0:                in  std_logic;          
  GPIO_SWITCH_1:                in  std_logic;          
  GPIO_SWITCH_2:                in  std_logic;          
  GPIO_SWITCH_3:                in  std_logic;          
  GPIO_BUTTON0:                 in  std_logic;          
  GPIO_BUTTON1:                 in  std_logic;          
  GPIO_BUTTON2:                 in  std_logic;          
  GPIO_BUTTON3:                 in  std_logic;          
  CPU_RESET:                    in  std_logic;          
  GPIO_HEADER_0_LS:             out std_logic;          
  GPIO_HEADER_1_LS:             out std_logic;          
  GPIO_HEADER_2_LS:             out std_logic;          
  GPIO_HEADER_3_LS:             out std_logic;          
  GPIO_LED_0:                   out std_logic;          
  GPIO_LED_1:                   out std_logic;          
  GPIO_LED_2:                   out std_logic;          
  GPIO_LED_3:                   out std_logic;          
  FPGA_AWAKE:                   out std_logic;          
  RXN_IN:                       in  std_logic_vector (1 downto 0);   
  RXP_IN:                       in  std_logic_vector (1 downto 0);   
  TXN_OUT:                      out std_logic_vector (1 downto 0);   
  TXP_OUT:                      out std_logic_vector (1 downto 0);  


--*******************uart**************************************
	
	USER_CLOCK               : in   std_logic;
	UART_RXDATA            : in   std_logic;
	UART_TXDATA            : out   std_logic
	
);

   
end component;



component userserial_tb 
generic(BAUD_RATE : INTEGER := 9600);		-- 19200 bps, to simulate set to 1000000
port (
		enable_tb		: in std_logic:='0';
		complete_tb		: out std_logic;
		UserSerialTx 	: out std_logic;
		UserSerialRx 	: in std_logic		 
	 );
end component;



-------------------------------------------------
-- internal signals
-------------------------------------------------

signal    sys_clk            : std_logic := '1';

signal    reset            	  : std_logic := '0'; 
signal    reset_n             : std_logic := '1'; 

signal    uart_rx            	  : std_logic := '0'; 
signal    uart_tx            	  : std_logic := '0'; 

signal    enable_tb           : std_logic := '0';
signal    complete_tb         : std_logic := '0'; 


    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
    signal  tied_to_vcc_i                   : std_logic;
    signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);


 --*******************SDI**************************************
signal  TILE0_GTP0_REFCLK_PAD_N_IN_I:   std_logic:='0';              -- GTP
signal  TILE0_GTP0_REFCLK_PAD_P_IN_I:   std_logic:='0';          
signal  USER_SMA_GPIO_P_I:              std_logic;         
signal  USER_SMA_GPIO_N_I:              std_logic;       
signal  GPIO_SWITCH_0_I:                std_logic:='0';         
signal  GPIO_SWITCH_1_I:                std_logic:='0';        
signal  GPIO_SWITCH_2_I:                std_logic:='0';         
signal  GPIO_SWITCH_3_I:                std_logic:='0';         
signal  GPIO_BUTTON0_I:                 std_logic:='0';         
signal  GPIO_BUTTON1_I:                 std_logic:='0';        
signal  GPIO_BUTTON2_I:                 std_logic:='0';         
signal  GPIO_BUTTON3_I:                 std_logic:='0';        
signal  GPIO_HEADER_0_LS_I:             std_logic;          
signal  GPIO_HEADER_1_LS_I:             std_logic;          
signal  GPIO_HEADER_2_LS_I:             std_logic;          
signal  GPIO_HEADER_3_LS_I:             std_logic;          
signal  GPIO_LED_0_I:                   std_logic;          
signal  GPIO_LED_1_I:                   std_logic;          
signal  GPIO_LED_2_I:                   std_logic;          
signal  GPIO_LED_3_I:                   std_logic;          
signal  FPGA_AWAKE_I:                   std_logic;          
signal  RXN_IN_I:                       std_logic_vector (1 downto 0):="00";   
signal  RXP_IN_I:                       std_logic_vector (1 downto 0):="00"; 
signal  TXN_OUT_I:                      std_logic_vector (1 downto 0);   
signal  TXP_OUT_I:                      std_logic_vector (1 downto 0);  

-- ************************** FMC connector *****************************
signal  clk_27M_in:            std_logic:='0';                -- 27 MHz X0
            

-------------------------------------------------
-- internal signals
-------------------------------------------------

begin

  --  Static signal Assigments
    tied_to_ground_i                        <= '0';
    tied_to_ground_vec_i                    <= x"0000000000000000";
    tied_to_vcc_i                           <= '1';
    tied_to_vcc_vec_i                       <= x"ff";
	reset_n <= not reset;


-------------------------------------------------
-- drive clock
-------------------------------------------------

 
sys_clk_p: process
 begin
  wait for 18.518 ns;
  sys_clk <= not sys_clk;
 end process;
 
 -- mgt_clk: process
 -- begin
  -- wait for 3.367 ns;
  -- TILE0_GTP0_REFCLK_PAD_P_IN_I <= not TILE0_GTP0_REFCLK_PAD_P_IN_I;
 -- end process;
 
 TILE0_GTP0_REFCLK_PAD_N_IN_I <= not TILE0_GTP0_REFCLK_PAD_P_IN_I;
 
   mgt_clk: process
 begin
  wait for 6.734 ns;
  TILE0_GTP0_REFCLK_PAD_P_IN_I <= not TILE0_GTP0_REFCLK_PAD_P_IN_I;
 end process;
 
-------------------------------------------------
-- component instantiations
-------------------------------------------------

asdi_mgt_p: asdi_mgt 
generic map(
   EXAMPLE_SIM_GTPRESET_SPEEDUP  => 1,
   EXAMPLE_USE_CHIPSCOPE         => 0,
   EXAMPLE_SIMULATION            => 1
)
port map
(

  TILE0_GTP0_REFCLK_PAD_N_IN    =>  TILE0_GTP0_REFCLK_PAD_N_IN_I,           
  TILE0_GTP0_REFCLK_PAD_P_IN	=>  TILE0_GTP0_REFCLK_PAD_P_IN_I,      
  USER_SMA_GPIO_P				=>  USER_SMA_GPIO_P_I,			        
  USER_SMA_GPIO_N				=>  USER_SMA_GPIO_N_I,			    
  GPIO_SWITCH_0					=>  GPIO_SWITCH_0_I,				        
  GPIO_SWITCH_1					=>  GPIO_SWITCH_1_I,				       
  GPIO_SWITCH_2					=>  GPIO_SWITCH_2_I,				       
  GPIO_SWITCH_3					=>  GPIO_SWITCH_3_I,				        
  GPIO_BUTTON0					=>  GPIO_BUTTON0_I,				  
  GPIO_BUTTON1					=>  GPIO_BUTTON1_I,				       
  GPIO_BUTTON2					=>  GPIO_BUTTON2_I,				       
  GPIO_BUTTON3					=>  GPIO_BUTTON3_I,				       
  CPU_RESET						=>  reset,					        
  GPIO_HEADER_0_LS				=>  GPIO_HEADER_0_LS_I,			       
  GPIO_HEADER_1_LS				=>  GPIO_HEADER_1_LS_I,			       
  GPIO_HEADER_2_LS				=>  GPIO_HEADER_2_LS_I,			        
  GPIO_HEADER_3_LS				=>  GPIO_HEADER_3_LS_I,			       
  GPIO_LED_0					=>  GPIO_LED_0_I,				      
  GPIO_LED_1					=>  GPIO_LED_1_I,				     
  GPIO_LED_2					=>  GPIO_LED_2_I,				      
  GPIO_LED_3					=>  GPIO_LED_3_I,				     
  FPGA_AWAKE					=>  FPGA_AWAKE_I,				       
  RXN_IN						=>  RXN_IN_I,					
  RXP_IN						=>  RXP_IN_I,					
  TXN_OUT						=>  RXN_IN_I,					 
  TXP_OUT						=>  RXP_IN_I,					
    
-- ************************** FMC connector *****************************
  USER_CLOCK					=> sys_clk,               -- 27 MHz X0
  
--*******************uart**************************************
	
	UART_RXDATA              =>      uart_tx,
	UART_TXDATA              =>      uart_rx
    
);
		

userserial_tb_p:  userserial_tb 
generic map(BAUD_RATE =>      100000)		-- 19200 bps, to simulate set to 1000000
port map (
		enable_tb		 =>      enable_tb,
		complete_tb		 =>      complete_tb,
		UserSerialTx 	 =>      uart_tx,
		UserSerialRx 	 =>      uart_rx 
	 );

	

TB_SDI: process
begin
 
  print("asdi_mgt TB v1.0 ");
  print("--------------------------------------"); 
  print(" ");
  
  -------------------------------------------------
  -- drive the resets
  -------------------------------------------------
    print("Applying Reset ....."); 
    reset <= '1';
	enable_tb  <= '0';
    
	GPIO_SWITCH_0_I <= '0';
	GPIO_SWITCH_1_I <= '0';
	GPIO_SWITCH_2_I <= '0';
	GPIO_SWITCH_3_I <= '0';
  
	GPIO_BUTTON0_I  <= '0';
	GPIO_BUTTON1_I  <= '0';
	GPIO_BUTTON2_I  <= '0';
	GPIO_BUTTON3_I  <= '0';
  
  
  
    wait for 100 ns;
    print("Reset desasserted ....."); 
    reset <= '0'; 
      
   
    -- wait for 10000 ns;
    -- print("UAR Test Enable"); 
    -- enable_tb			 <= '1';
	
		
	
	-- assert false
      -- report "End simulation"
     -- severity Failure; 
   
	wait;
   
end process;  	
	
	
	

end BHU;                           
