onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider TB
add wave -noupdate /testbench/reset
add wave -noupdate /testbench/reset_n
add wave -noupdate /testbench/uart_rx
add wave -noupdate /testbench/uart_tx
add wave -noupdate /testbench/enable_tb
add wave -noupdate /testbench/complete_tb
add wave -noupdate /testbench/GPIO_SWITCH_0_I
add wave -noupdate /testbench/GPIO_SWITCH_1_I
add wave -noupdate /testbench/GPIO_SWITCH_2_I
add wave -noupdate /testbench/GPIO_SWITCH_3_I
add wave -noupdate /testbench/GPIO_BUTTON0_I
add wave -noupdate /testbench/GPIO_BUTTON1_I
add wave -noupdate /testbench/GPIO_BUTTON2_I
add wave -noupdate /testbench/GPIO_BUTTON3_I
add wave -noupdate /testbench/GPIO_LED_0_I
add wave -noupdate /testbench/GPIO_LED_1_I
add wave -noupdate /testbench/GPIO_LED_2_I
add wave -noupdate /testbench/GPIO_LED_3_I
add wave -noupdate /testbench/FPGA_AWAKE_I
add wave -noupdate /testbench/RXN_IN_I
add wave -noupdate -divider TOP_SDI
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/TILE0_GTP0_REFCLK_PAD_N_IN
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/TILE0_GTP0_REFCLK_PAD_P_IN
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/USER_SMA_GPIO_P
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/USER_SMA_GPIO_N
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_SWITCH_0
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_SWITCH_1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_SWITCH_2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_SWITCH_3
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_BUTTON0
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_BUTTON1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_BUTTON2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_BUTTON3
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/CPU_RESET
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_HEADER_0_LS
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_HEADER_1_LS
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_HEADER_2_LS
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_HEADER_3_LS
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_LED_0
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_LED_1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_LED_2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/GPIO_LED_3
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/FPGA_AWAKE
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/RXN_IN
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/RXP_IN
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/TXN_OUT
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/TXP_OUT
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/clk_27M_in
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_resetdone1_r
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_resetdone1_r2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_loopback1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_gtpreset1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_plllkdet1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_resetdone1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_rxrecclk1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rxrecclk1_buf
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_rxcdrreset1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/auto_rxcdrreset
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_rxbufreset1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/auto_rxbufreset
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_rxbufstatus1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_gtpclkout1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rxdata1_20
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/txdata1_20
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/txdata1_20_gtp
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_txoutclk1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/txusrclk
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/txusrclk2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/txpipeclk
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rxusrclk
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rxusrclk2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rxpipeclk
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtpclkout1_1_pll_locked
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtpclkout1_1_pll_reset
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtpclkout1_1_bufio
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtpreset1_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp_reset1_in
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/user_tx1_reset_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/user_rx_reset_i
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/dip_switch_in
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/dip_switch_sync
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/push_button_bus
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/hdgen_y
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/hdgen_c
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/hdgen_ln
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/ntsc_patgen
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/pal_patgen
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/sd_patgen
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_txbufstatus0
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tile0_txbufstatus1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_sdimode_sel
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_sdimode_sel_1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_sdimode_sel_2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_sdimode_sel_3
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx_sd_mode
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx1_slew
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_format
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_format_sel
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_sel
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_3
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_4
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_5
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_6
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_7
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_bitrate_8
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_rate_change
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_rate_change_del
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_pattern_sel
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_y_in
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_c_in
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_vpid_byte2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_ds1a
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_ds2a
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_ds1b
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_ds2b
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_eav
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_sav
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_out_mode
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/txreset1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/auto_txreset1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx1_fabric_reset
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/auto_tx1_fabric_reset
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp101_daddr
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp101_di
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp101_drpo
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp101_den
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp101_dwe
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp101_drdy
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/sd_ntsc_vid
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/sd_pal_vid
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_mode_HD
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_mode_SD
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_mode_3G
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/mode_switches
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/mode_switches_dly
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/old_mode
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/vidgen_enable
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/vidgen_disable_dly
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/vidgen_disable_dly_tc
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/mode_change
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/tx_ce
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/sd_ce
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gen_sd_ce
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/ce_mux
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_ce
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_lvlb_drdy
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp123_1_refclk
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_mode
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_mode_HD
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_mode_SD
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_mode_3G
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_mode_locked
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_rate
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_format
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_locked
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_crc_err
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_ln_a
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_ln_b
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_a_vpid
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_a_vpid_valid
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_b_vpid
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_b_vpid_valid
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_a_y
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_a_c
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_trs
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_eav
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_sav
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_b_y
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_b_c
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_crc_err2
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_level_b
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_crc_err_ff
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rxreset1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/auto_rxreset
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gtp_reset_in
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_fabric_reset
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/auto_rx1_fabric_reset
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/edh_errcnt
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/rx1_err
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/edh_err
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/std
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/sd_locked
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/locked
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gpioLED1
add wave -noupdate -expand -group {TOP_SDI
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/gpioLED2
add wave -noupdate -divider PAL_GEN
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/vidgen_enable
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/vidgen_disable_dly
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/vidgen_disable_dly_tc
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/clk
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/rst
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/ce
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/std
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/pattern
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/user_opt
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/y
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/c
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/h_blank
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/v_blank
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/field
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/trs
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/xyz
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/line_num
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/std_q
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/std_change
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/h_region
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/v_inc
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/h_counter_lsb
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/trs_int
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/xyz_int
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/h_int
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/v_counter
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/v_band
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/f_int
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/v_int
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/first_line
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/y_ramp_inc_sel
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/y_int
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/c_int
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/trs_reg
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/xyz_reg
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/h_reg
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/v_reg
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/f_reg
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/c_reg
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/y_reg
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/delay_rst
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/reset
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/GND
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/VCC
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/GND4
add wave -noupdate -group {VID_GEN
} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/VIDGEN1/GND32
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/clk
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/rst
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/data_in
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/frame_en
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_HD
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_SD
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_3G
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_locked
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/rx_locked
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/t_format
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/level_b_3G
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ce_sd
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/nsp
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ln_a
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/a_vpid
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/a_vpid_valid
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/b_vpid
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/b_vpid_valid
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/crc_err_a
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds1_a
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds2_a
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/eav
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/sav
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/trs
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ln_b
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/dout_rdy_3G
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/crc_err_b
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds1_b
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds2_b
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ce_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ce_lvlb_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ce_sd_ff
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/dout_rdy_3G_ff
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/dru_drdy
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/dru_center_f
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/dru_g1
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/dru_g1_p
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/dru_g2
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/sam
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/samv
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/dru_dout
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_drdy
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/rxdata
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/sd_rxdata
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_locked_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_HD_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_SD_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/mode_3G_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/descrambler_in
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/descrambler_out
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/framer_ds1
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/framer_ds2
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/framer_eav
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/framer_sav
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/framer_trs
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/framer_xyz
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/framer_trs_err
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/level_a
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/level_b
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/a_vpid_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/b_vpid_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/a_vpid_valid_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/b_vpid_valid_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/vpid_b_in
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_a_y
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_a_c
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_b_y
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_b_c
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_trs
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_eav
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_sav
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_dout_rdy_gen
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/lvlb_sav_err
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/autodetect_sav
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/autodetect_trs_err
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ad_format
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ad_locked
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/eav_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/sav_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/trs_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds1_a_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds2_a_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds1_b_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds2_b_int
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds1_a_crc_err
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds2_a_crc_err
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds1_b_crc_err
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/ds2_b_crc_err
add wave -noupdate -expand -group {SDI_RX1} -radix hexadecimal /testbench/asdi_mgt_p/SVP_SDI_P/SDIRX1/framer_nsp
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {20240496 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 380
configure wave -valuecolwidth 226
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {56997729 ps}
