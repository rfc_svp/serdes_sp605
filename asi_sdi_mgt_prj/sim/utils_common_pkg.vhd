library IEEE;
use IEEE.STD_LOGIC_1164.all;

package utils_common_pkg is

-- main subtypes
subtype HALFBYTE is std_logic_vector(3 downto 0);
subtype BYTE is std_logic_vector(7 downto 0);
subtype WORD is std_logic_vector(15 downto 0);
subtype DWORD is std_logic_vector(31 downto 0);

-- main arrays
TYPE HALFBYTE_vector IS ARRAY (NATURAL RANGE <>) OF HALFBYTE;
TYPE BYTE_vector IS ARRAY (NATURAL RANGE <>) OF BYTE;
TYPE WORD_vector IS ARRAY (NATURAL RANGE <>) OF WORD;
TYPE DWORD_vector IS ARRAY (NATURAL RANGE <>) OF DWORD;

-- program type
type program_type is (FSD,UKTDP);

-- prototype type
type prototype_type is (xlx_v5lx50t_pcie_dev,ACTEL_FUSION_ADV_DEV,controller_cpu);

-- base-2 logarithm
function log2 (Taps : integer) return integer;

-- bit reverse
function REVERSE(X : in std_logic_vector) return std_logic_vector;

-- parity functions
function bit_parity_gen (Data:std_logic_vector;OddEvenN:std_logic) return std_logic;
function bit_parity_check (Data:std_logic_vector;Parity:std_logic;OddEvenN:std_logic) return std_logic;
function byte_parity_gen (Data:std_logic_vector;OddEvenN:std_logic) return std_logic_vector;
function byte_parity_check (Data:std_logic_vector;Parity:std_logic_vector;OddEvenN:std_logic) return std_logic_vector;

-- expb memory map types
type reg_property is (none,fifo);
type reg_addr_order is array (NATURAL RANGE <>) of DWORD;
type reg_PerCSn_order is array (NATURAL RANGE <>) of natural range 0 to 7;
type reg_addr_property_order is array (NATURAL RANGE <>) of reg_property;

end utils_common_pkg;

package body utils_common_pkg is

function log2 (Taps : integer) return integer is
   variable i: integer;
begin
   
   if Taps>1 then
	   for i in 0 to Taps - 1 loop
		  if 2**i >= Taps then
			return i;
		  end if;
	   end loop;
   else
		return 1;
   end if;   
   
end log2;

---------------------------------------------------------------------------
function REVERSE(X : in std_logic_vector) return std_logic_vector is
  variable Y : std_logic_vector(X'range);
  variable low : integer;
  variable high : integer;
begin
  if (X'left = X'low) then
    for i in 0 to X'length - 1 loop
      Y(X'low + X'length - i - 1) := X(i + X'low);
    end loop;
  else
    for i in 0 to X'length - 1 loop
      Y(X'low + i) := X(X'low + X'length - 1 - i);
    end loop;
  end if;
  return Y;
end REVERSE;

---------------------------------------------------------------------------

  function bit_parity_gen (Data: std_logic_vector;OddEvenN:std_logic) return std_logic is
    variable y : std_logic := OddEvenN;
  begin
    for i in Data'RANGE loop
      y := y xor Data(i);
    end loop;
    return y;
  end bit_parity_gen;

---------------------------------------------------------------------------
  function bit_parity_check (Data:std_logic_vector;Parity:std_logic;OddEvenN:std_logic) return std_logic is
    variable y : std_logic;
  begin
    y:=bit_parity_gen (Data,OddEvenN);
    return (y xor Parity);
  end bit_parity_check;
  
---------------------------------------------------------------------------
  function byte_parity_gen (DATA:in std_logic_vector;OddEvenN:std_logic) return std_logic_vector is
    variable aux : std_logic_vector((DATA'length/8)-1 downto 0);
  begin
    for i in 0 to aux'high loop
	  aux(i) := OddEvenN;
	  for j in 0 to 7 loop
	    --aux(i):=aux(i) xor DATA((8*(aux'high-i+1))-1-j);
		aux(i):=aux(i) xor DATA((8*(i+1))-1-j);
	  end loop;
    end loop;
    return aux;	
  end byte_parity_gen;
  
---------------------------------------------------------------------------
  function byte_parity_check (Data:std_logic_vector;Parity:std_logic_vector;OddEvenN:std_logic) return std_logic_vector is
    variable aux,result : std_logic_vector((DATA'length/8)-1 downto 0);
  begin
    aux:=byte_parity_gen(Data,OddEvenN);
	for i in 0 to aux'high loop
	  result(i):=aux(i) xor Parity(i);
    end loop;
    return result;	
  end byte_parity_check;

end utils_common_pkg;