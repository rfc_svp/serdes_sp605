
#### refresh precompiled libraries

#vmap mylib mylib
#vmap work work

vlog -work mylib -refresh
vcom -work mylib -refresh
vlog -work work -refresh
vcom -work work -refresh

#
# Compile sources
#
vcom -93 -explicit -work work "utils_common_pkg.vhd"
vcom -93 -explicit -work work "text_util_common_pkg.vhd"
vcom -93 -explicit -work work "sim_AMC_pkg.vhd"
vcom -93 -explicit -work work "sim_common_pkg.vhd"
vcom -93 -explicit -work work "txt_util.vhd"
vcom -93 -explicit -work work "fifo_sim.vhd"

#BRAM

vcom -93 -work work -2002 -explicit "../../ASDI_MGT/ipcore_dir/blk_mem_gen_v6_3.vhd"

#UART
vcom -93 -work work -2002 -explicit "../vhdl/uart/COREUART/4.1.114b/rtl/vhdl/core_obfuscated/Clock_gen.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/uart/COREUART/4.1.114b/rtl/vhdl/core_obfuscated/components.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/uart/COREUART/4.1.114b/rtl/vhdl/core_obfuscated/fifo_256x8.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/uart/COREUART/4.1.114b/rtl/vhdl/core_obfuscated/Rx_async.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/uart/COREUART/4.1.114b/rtl/vhdl/core_obfuscated/Tx_async.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/uart/COREUART/4.1.114b/rtl/vhdl/core_obfuscated/CoreUART.vhd"


vcom -93 -work work -2002 -explicit "../vhdl/uart/utils_uart_pkg.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/uart/USERUART.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/uart/USER_UART.vhd"


#TRIPLE SDI MGT

vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/anc_edh_pkg.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/anc_rx.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/autodetect.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/AVBSPI.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/bshift10to10.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/control.vhd"
#vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/dru.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/edh_crc16.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/edh_crc.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/edh_errcnt.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/edh_flags.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/edh_loc.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/edh_processor.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/edh_rx.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/edh_tx.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/fly_field.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/fly_fsm.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/fly_horz.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/fly_vert.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/flywheel.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/hdsdi_pkg.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/gtp_interface_pll.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/hdsdi_crc2.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/hdsdi_insert_crc.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/hdsdi_insert_ln.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/hdsdi_rx_crc.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/kcpsm3.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/main_avb_control.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/maskencoder.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/multi_sdi_decoder.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/multi_sdi_encoder.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/multi_sdi_framer.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/multigenHD_pkg.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/multigenHD.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/multigenHD_horz.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/multigenHD_output.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/multigenHD_vert.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/mux12_wide.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/Osc9.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/rot20.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/s6_sdi_rx_light_20b.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/s6gtp_sdi_control.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/s6gtp_sdi_drp_control.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/s6gtp_sdi_rate_detect.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/s6gtp_sdi_rx_reset.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/sdi_bitrep_20b.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/Si5324_fsel_lookup.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/wide_SRLC16E.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/SMPTE352_vpid_capture.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/SMPTE352_vpid_insert.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/SMPTE425_B_demux2.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/smpte_encoder.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/sync_one_shot.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/triple_sdi_autodetect_ln.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/triple_sdi_rx_autorate.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/triple_sdi_tx_output_20b.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/triple_sdi_vpid_insert.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/trs_detect.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/usrclk_pll.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/video_decode.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/vidgen_ntsc.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/vidgen_pal.vhd"
#vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/wiz1_4_20b.vhd"
#vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/wiz1_4_20b_tile.vhd"
vcom -93 -work work -2002 -explicit "../vhdl/SVP_TripleRateSDI/VHDL/SVP_SDI.vhd"

#GTP 

vcom -93 -work work -2002 -explicit "../../ASDI_MGT/ipcore_dir/s6_gtpwizard_v1_11_tile.vhd"
vcom -93 -work work -2002 -explicit "../../ASDI_MGT/ipcore_dir/s6_gtpwizard_v1_11.vhd"

#ASI SDI MGT

#vcom -93 -work work -2002 -explicit "../vhdl/asdi_mgt_pkg.vhd" 
vcom -93 -work work -2002 -explicit "../vhdl/asdi_mgt.vhd" 

#TB
vcom -93 -explicit -work work "../sim/userserial_tb.vhd"
vcom -93 -explicit -work work "../sim/testbench.vhd"


set StdArithNoWarnings 0
set NumericStdNoWarnings 0

vsim -voptargs="+acc" -t ps  -L work -L mylib work.testbench(BHU) 
#vsim -t ps  -L work -L mylib work.testbench(BHU) 
do wave.do
run -all