--
--	APSC Controller AAIF/AMC FW text utilities Simulation Package
--
--		26/10/2012 - drhoms - Initial Version
--		09/11/2012 - drhoms - print character to the screen added
--		12/11/2012 - drhoms - char_to_ascii function included
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;

use work.utils_common_pkg.all;

package text_util_common_pkg is

--------------------------------------------------------------------------------
-- Global simulation data ------------------------------------------------------
file log_file : text open write_mode is "./tb_log.txt";

    --
	TYPE string_vector IS ARRAY (NATURAL RANGE <>) OF string (1 to 16);
	subTYPE slv_7dt0 IS std_logic_vector (7 downto 0);

    -- prints a message to the screen
    procedure print(text: string);

    -- prints the message when active
    -- useful for debug switches
    procedure print(active: boolean; text: string);
	
    -- print character to the screen
    procedure print(char:       in  character);	

    -- converts std_logic into a character
    function chr(sl: std_logic) return character;

    -- converts std_logic into a string (1 to 1)
    function str(sl: std_logic) return string;

    -- converts std_logic_vector into a string (binary base)
    function str(slv: std_logic_vector) return string;

    -- converts boolean into a string
    function str(b: boolean) return string;

    -- converts an integer into a single character
    -- (can also be used for hex conversion and other bases)
    function chr(int: integer) return character;

    -- converts integer into string using specified base
    function str(int: integer; base: integer) return string;

    -- converts integer to string, using base 10
    function str(int: integer) return string;

    -- convert std_logic_vector into a string in hex format
    function hstr(input: std_logic_vector) return string;
    
	-- convert slv array into a string vector in hex format
	function hstrv(input: BYTE_vector) return string_vector;
    function hstrv(input: WORD_vector) return string_vector;
    function hstrv(input: DWORD_vector) return string_vector;

	-- convert string array into a string
	function to_string(input: string_vector) return string;

    -- converts an integer into an ASCII character
    function ascii(int: integer) return character;
	
    -- converts an integer into an control ASCII character representation
    function ctrl_ascii(int: integer) return string;
	
	-- converts a character into an ASCII character
	function char_to_ascii (ch : in character) return slv_7dt0;
		
    -- functions to manipulate strings
    -----------------------------------

    -- convert a character to upper case
    function to_upper(c: character) return character;

    -- convert a character to lower case
    function to_lower(c: character) return character;

    -- convert a string to upper case
    function to_upper(s: string) return string;

    -- convert a string to lower case
    function to_lower(s: string) return string;

   
    
    -- functions to convert strings into other formats
    --------------------------------------------------
    
    -- converts a character into std_logic
    function to_std_logic(c: character) return std_logic; 
    
    -- converts a string into std_logic_vector
    function to_std_logic_vector(s: string) return std_logic_vector; 


  
    -- file I/O
    -----------
       
    -- read variable length string from input file
    procedure str_read(file in_file: TEXT; 
                       res_string: out string);
        
    -- print string to a file and start new line
    procedure print(file out_file: TEXT;
                    new_string: in  string);
    
    -- print character to a file and start new line
    procedure print(file out_file: TEXT;
                    char:       in  character);

--------------------------------------------------------------------------------
-- Log note to log file and/or console
procedure log_note (
    module :    in string;
    msg :       in string
    );

--------------------------------------------------------------------------------
-- Log test fault to log file and/or console
procedure log_fault (
    module :    in string;
    msg :       in string
    );

--------------------------------------------------------------------------------
-- Log success or failure of a test case (same as above, diff. format)
procedure log_case_result (
    module :            in string;
    failed :            in boolean
    );
    
--------------------------------------------------------------------------------
-- Log a header line with some introductory info
procedure log_header;

end package;

--------------------------------------------------------------------------------

package body text_util_common_pkg is

procedure log_header is
begin
    print(log_file, ""& "Simulation log (log header TBD)");
end procedure log_header;

procedure log_fault (
    module :            in string;
    msg :               in string
    ) is
variable modname : string(1 to 16);
begin
    modname := (others => ' ');
    modname(1 to module'length) := module;
    --print (modname & " -- FAULT: "& msg);
    print(log_file, modname & " -- FAULT: "& msg);

    assert false
    report msg
    severity warning;
end procedure log_fault;

procedure log_note (
    module :            in string;
    msg :               in string
    ) is
variable modname : string(1 to 16);
begin
    modname := (others => ' ');
    modname(1 to module'length) := module;
    return; -- FIXME add some means to enable/disable notes globally
    print (modname& " -- NOTE:  "& msg);
    assert false
    report msg
    severity note;
end procedure log_note;

procedure log_case_result (
    module :            in string;
    failed :            in boolean
    ) is
variable modname : string(1 to 16);
begin
    print(log_file, str(""));

    modname := (others => ' ');
    modname(1 to module'length) := module;
    
    if not failed then
        print(modname& " ******** TEST PASSED");
        print(log_file, modname & " ******** TEST PASSED");
    else
        print(modname& " ******** TEST FAILED");
        print(log_file, modname & " ******** TEST FAILED");
    end if;

    print(log_file, str(""));
end procedure log_case_result;

   -- prints text to the screen

   procedure print(text: string) is
     variable msg_line: line;
     begin
       write(msg_line, text);
       writeline(output, msg_line);
   end print;




   -- prints text to the screen when active

   procedure print(active: boolean; text: string)  is
     begin
      if active then
         print(text);
      end if;
   end print;
   
   
	-- print character to a file and start new line
	procedure print(char: in  character) is
		variable l: line;
	begin
		write(l, char);
		writeline(output, l);
	end print;   

   -- converts std_logic into a character

   function chr(sl: std_logic) return character is
    variable c: character;
    begin
      case sl is
         when 'U' => c:= 'U';
         when 'X' => c:= 'X';
         when '0' => c:= '0';
         when '1' => c:= '1';
         when 'Z' => c:= 'Z';
         when 'W' => c:= 'W';
         when 'L' => c:= 'L';
         when 'H' => c:= 'H';
         when '-' => c:= '-';
      end case;
    return c;
   end chr;



   -- converts std_logic into a string (1 to 1)

   function str(sl: std_logic) return string is
    variable s: string(1 to 1);
    begin
        s(1) := chr(sl);
        return s;
   end str;



   -- converts std_logic_vector into a string (binary base)
   -- (this also takes care of the fact that the range of
   --  a string is natural while a std_logic_vector may
   --  have an integer range)

   function str(slv: std_logic_vector) return string is
     variable result : string (1 to slv'length);
     variable r : integer;
   begin
     r := 1;
     for i in slv'range loop
        result(r) := chr(slv(i));
        r := r + 1;
     end loop;
     return result;
   end str;


   function str(b: boolean) return string is

    begin
       if b then
          return "true";
      else
        return "false";
       end if;
    end str;


   -- converts an integer into a character
   -- for 0 to 9 the obvious mapping is used, higher
   -- values are mapped to the characters A-Z
   -- (this is usefull for systems with base > 10)
   -- (adapted from Steve Vogwell's posting in comp.lang.vhdl)

   function chr(int: integer) return character is
    variable c: character;
   begin
        case int is
          when  0 => c := '0';
          when  1 => c := '1';
          when  2 => c := '2';
          when  3 => c := '3';
          when  4 => c := '4';
          when  5 => c := '5';
          when  6 => c := '6';
          when  7 => c := '7';
          when  8 => c := '8';
          when  9 => c := '9';
          when 10 => c := 'A';
          when 11 => c := 'B';
          when 12 => c := 'C';
          when 13 => c := 'D';
          when 14 => c := 'E';
          when 15 => c := 'F';
          when 16 => c := 'G';
          when 17 => c := 'H';
          when 18 => c := 'I';
          when 19 => c := 'J';
          when 20 => c := 'K';
          when 21 => c := 'L';
          when 22 => c := 'M';
          when 23 => c := 'N';
          when 24 => c := 'O';
          when 25 => c := 'P';
          when 26 => c := 'Q';
          when 27 => c := 'R';
          when 28 => c := 'S';
          when 29 => c := 'T';
          when 30 => c := 'U';
          when 31 => c := 'V';
          when 32 => c := 'W';
          when 33 => c := 'X';
          when 34 => c := 'Y';
          when 35 => c := 'Z';
          when others => c := '?';
        end case;
        return c;
    end chr;

   -- converts an integer into an ASCII character

   function ascii(int: integer) return character is
    variable c: character;
   begin
        case int is
			when 0 => c:=NUL;
			when 1 => c:=SOH;
			when 2 => c:=STX;
			when 3 => c:=ETX;
			when 4 => c:=EOT;
			when 5 => c:=ENQ;
			when 6 => c:=ACK;
			when 7 => c:=BEL;
			when 8 => c:=BS;
			when 9 => c:=HT;
			when 10 => c:=LF;
			when 11 => c:=VT;
			when 12 => c:=FF;
			when 13 => c:=CR;
			when 14 => c:=SO;
			when 15 => c:=SI;
			when 16 => c:=DLE;
			when 17 => c:=DC1;
			when 18 => c:=DC2;
			when 19 => c:=DC3;
			when 20 => c:=DC4;
			when 21 => c:=NAK;
			when 22 => c:=SYN;
			when 23 => c:=ETB;
			when 24 => c:=CAN;
			when 25 => c:=EM;
			when 26 => c:=SUB;
			when 27 => c:=ESC;
			when 28 => c:=FSP;
			when 29 => c:=GSP;
			when 30 => c:=RSP;
			when 31 => c:=USP;
			------------------------------
			when 32 => c:=' ';
			when 33 => c:='!';
			when 34 => c:='"';
			when 35 => c:='#';
			when 36 => c:='$';
			when 37 => c:='%';
			when 38 => c:='&';
			when 39 => c:=''';
			when 40 => c:='(';
			when 41 => c:=')';
			when 42 => c:='*';
			when 43 => c:='+';
			when 44 => c:=',';
			when 45 => c:='-';
			when 46 => c:='.';
			when 47 => c:='/';
			when 48 => c:='0';
			when 49 => c:='1';
			when 50 => c:='2';
			when 51 => c:='3';
			when 52 => c:='4';
			when 53 => c:='5';
			when 54 => c:='6';
			when 55 => c:='7';
			when 56 => c:='8';
			when 57 => c:='9';
			when 58 => c:=':';
			when 59 => c:=';';
			when 60 => c:='<';
			when 61 => c:='=';
			when 62 => c:='>';
			when 63 => c:='?';
			when 64 => c:='@';
			when 65 => c:='A';
			when 66 => c:='B';
			when 67 => c:='C';
			when 68 => c:='D';
			when 69 => c:='E';
			when 70 => c:='F';
			when 71 => c:='G';
			when 72 => c:='H';
			when 73 => c:='I';
			when 74 => c:='J';
			when 75 => c:='K';
			when 76 => c:='L';
			when 77 => c:='M';
			when 78 => c:='N';
			when 79 => c:='O';
			when 80 => c:='P';
			when 81 => c:='Q';
			when 82 => c:='R';
			when 83 => c:='S';
			when 84 => c:='T';
			when 85 => c:='U';
			when 86 => c:='V';
			when 87 => c:='W';
			when 88 => c:='X';
			when 89 => c:='Y';
			when 90 => c:='Z';
			when 91 => c:='[';
			when 92 => c:='\';
			when 93 => c:=']';
			when 94 => c:='^';
			when 95 => c:='_';
			when 96 => c:='`';
			when 97 => c:='a';
			when 98 => c:='b';
			when 99 => c:='c';
			when 100 => c:='d';
			when 101 => c:='e';
			when 102 => c:='f';
			when 103 => c:='g';
			when 104 => c:='h';
			when 105 => c:='i';
			when 106 => c:='j';
			when 107 => c:='k';
			when 108 => c:='l';
			when 109 => c:='m';
			when 110 => c:='n';
			when 111 => c:='o';
			when 112 => c:='p';
			when 113 => c:='q';
			when 114 => c:='r';
			when 115 => c:='s';
			when 116 => c:='t';
			when 117 => c:='u';
			when 118 => c:='v';
			when 119 => c:='w';
			when 120 => c:='x';
			when 121 => c:='y';
			when 122 => c:='z';
			when 123 => c:='{';
			when 124 => c:='|';
			when 125 => c:='}';
			when 126 => c:='~';
			when 127 => c:=DEL;
			------------------------------
			when 128 => c:=c128;	--'�';
			when 129 => c:=c129;	--'�';
			when 130 => c:=c130;	--'�';
			when 131 => c:=c131;	--'�';
			when 132 => c:=c132;	--'�';
			when 133 => c:=c133;	--'�';
			when 134 => c:=c134;	--'�';
			when 135 => c:=c135;	--'�';
			when 136 => c:=c136;	--'�';
			when 137 => c:=c137;	--'�';
			when 138 => c:=c138;	--'�';
			when 139 => c:=c139;	--'�';
			when 140 => c:=c140;	--'�';
			when 141 => c:=c141;	--'�';
			when 142 => c:=c142;	--'�';
			when 143 => c:=c143;	--'�';
			when 144 => c:=c144;	--'�';
			when 145 => c:=c145;	--'�';
			when 146 => c:=c146;	--'�';
			when 147 => c:=c147;	--'�';
			when 148 => c:=c148;	--'�';
			when 149 => c:=c149;	--'�';
			when 150 => c:=c150;	--'�';
			when 151 => c:=c151;	--'�';
			when 152 => c:=c152;	--'�';
			when 153 => c:=c153;	--'�';
			when 154 => c:=c154;	--'�';
			when 155 => c:=c155;	--'�';
			when 156 => c:=c156;	--'�';
			when 157 => c:=c157;	--'�';
			when 158 => c:=c158;	--'�';
			when 159 => c:=c159;	--'�';
			when 160 => c:='�';
			when 161 => c:='�';
			when 162 => c:='�';
			when 163 => c:='�';
			when 164 => c:='�';
			when 165 => c:='�';
			when 166 => c:='�';
			when 167 => c:='�';
			when 168 => c:='�';
			when 169 => c:='�';
			when 170 => c:='�';
			when 171 => c:='�';
			when 172 => c:='�';
			when 173 => c:='�';
			when 174 => c:='�';
			when 175 => c:='�';
			when 176 => c:='�';
			when 177 => c:='�';
			when 178 => c:='�';
			when 179 => c:='�';
			when 180 => c:='�';
			when 181 => c:='�';
			when 182 => c:='�';
			when 183 => c:='�';
			when 184 => c:='�';
			when 185 => c:='�';
			when 186 => c:='�';
			when 187 => c:='+';
			when 188 => c:='+';
			when 189 => c:='�';
			when 190 => c:='�';
			when 191 => c:='+';
			when 192 => c:='+';
			when 193 => c:='-';
			when 194 => c:='-';
			when 195 => c:='+';
			when 196 => c:='-';
			when 197 => c:='+';
			when 198 => c:='�';
			when 199 => c:='�';
			when 200 => c:='+';
			when 201 => c:='+';
			when 202 => c:='-';
			when 203 => c:='-';
			when 204 => c:='�';
			when 205 => c:='-';
			when 206 => c:='+';
			when 207 => c:='�';
			when 208 => c:='�';
			when 209 => c:='�';
			when 210 => c:='�';
			when 211 => c:='�';
			when 212 => c:='�';
			when 213 => c:='i';
			when 214 => c:='�';
			when 215 => c:='�';
			when 216 => c:='�';
			when 217 => c:='+';
			when 218 => c:='+';
			when 219 => c:='�';
			when 220 => c:='_';
			when 221 => c:='�';
			when 222 => c:='�';
			when 223 => c:='�';
			when 224 => c:='�';
			when 225 => c:='�';
			when 226 => c:='�';
			when 227 => c:='�';
			when 228 => c:='�';
			when 229 => c:='�';
			when 230 => c:='�';
			when 231 => c:='�';
			when 232 => c:='�';
			when 233 => c:='�';
			when 234 => c:='�';
			when 235 => c:='�';
			when 236 => c:='�';
			when 237 => c:='�';
			when 238 => c:='�';
			when 239 => c:='�';
			when 240 => c:='�';
			when 241 => c:='�';
			when 242 => c:='=';
			when 243 => c:='�';
			when 244 => c:='�';
			when 245 => c:='�';
			when 246 => c:='�';
			when 247 => c:='�';
			when 248 => c:='�';
			when 249 => c:='�';
			when 250 => c:='�';
			when 251 => c:='�';
			when 252 => c:='�';
			when 253 => c:='�';
			when 254 => c:='�';
			--when 255 => c:=nbsp;
			when others => c := NUL;
        end case;
        return c;
    end ascii;
	
   function ctrl_ascii(int: integer) return string is
    variable c: string(1 to 5);
   begin
        case int is
			when 0 => 	c:="'NUL'";
			when 1 => 	c:="'SOH'";
			when 2 => 	c:="'STX'";
			when 3 => 	c:="'ETX'";
			when 4 => 	c:="'EOT'";
			when 5 => 	c:="'ENQ'";
			when 6 => 	c:="'ACK'";
			when 7 => 	c:="'BEL'";
			when 8 => 	c:="'BS' ";
			when 9 => 	c:="'HT' ";
			when 10 => 	c:="'LF' ";
			when 11 => 	c:="'VT' ";
			when 12 => 	c:="'FF' ";
			when 13 => 	c:="'CR' ";
			when 14 => 	c:="'SO' ";
			when 15 => 	c:="'SI' ";
			when 16 => 	c:="'DLE'";
			when 17 => 	c:="'DC1'";
			when 18 => 	c:="'DC2'";
			when 19 => 	c:="'DC3'";
			when 20 => 	c:="'DC4'";
			when 21 => 	c:="'NAK'";
			when 22 => 	c:="'SYN'";
			when 23 => 	c:="'ETB'";
			when 24 => 	c:="'CAN'";
			when 25 => 	c:="'EM' ";
			when 26 => 	c:="'SUB'";
			when 27 => 	c:="'ESC'";
			when 28 => 	c:="'FSP'";
			when 29 => 	c:="'GSP'";
			when 30 => 	c:="'RSP'";
			when 31 => 	c:="'USP'";	
			when 127 => c:="'DEL'";
			when others => null;
        end case;
        return c;
    end ctrl_ascii;

	-- character to ascii
	
	function char_to_ascii (
		ch :                in character
		) return slv_7dt0 is
	begin
		return std_logic_vector(to_unsigned(character'pos(ch),8));
	end function char_to_ascii;
	
   -- convert integer to string using specified base
   -- (adapted from Steve Vogwell's posting in comp.lang.vhdl)

   function str(int: integer; base: integer) return string is

    variable temp:      string(1 to 10);
    variable num:       integer;
    variable abs_int:   integer;
    variable len:       integer := 1;
    variable power:     integer := 1;

   begin

    -- bug fix for negative numbers
    abs_int := abs(int);

    num     := abs_int;

    while num >= base loop                     -- Determine how many
      len := len + 1;                          -- characters required
      num := num / base;                       -- to represent the
    end loop ;                                 -- number.

    for i in len downto 1 loop                 -- Convert the number to
      temp(i) := chr(abs_int/power mod base);  -- a string starting
      power := power * base;                   -- with the right hand
    end loop ;                                 -- side.

    -- return result and add sign if required
    if int < 0 then
       return '-'& temp(1 to len);
     else
       return temp(1 to len);
    end if;

   end str;


  -- convert integer to string, using base 10
  function str(int: integer) return string is

   begin

    return str(int, 10) ;

   end str;



   -- converts a std_logic_vector into a hex string.
   function hstr(input: std_logic_vector) return string is
       variable hexlen: integer;
       variable longslv : std_logic_vector(67 downto 0) := (others => '0');
       variable hex : string(1 to 16);
       variable fourbit : std_logic_vector(3 downto 0);
     begin
       --hexlen := (input'left+1)/4;
	   hexlen := (input'length)/4;
       --if (input'left+1) mod 4 /= 0 then
       if (input'length) mod 4 /= 0 then
         hexlen := hexlen + 1;
       end if;
       --longslv(input'left downto 0) := input;
       longslv(input'length-1 downto 0) := input;
       for i in (hexlen -1) downto 0 loop
         fourbit := longslv(((i*4)+3) downto (i*4));
         case fourbit is
           when "0000" => hex(hexlen -I) := '0';
           when "0001" => hex(hexlen -I) := '1';
           when "0010" => hex(hexlen -I) := '2';
           when "0011" => hex(hexlen -I) := '3';
           when "0100" => hex(hexlen -I) := '4';
           when "0101" => hex(hexlen -I) := '5';
           when "0110" => hex(hexlen -I) := '6';
           when "0111" => hex(hexlen -I) := '7';
           when "1000" => hex(hexlen -I) := '8';
           when "1001" => hex(hexlen -I) := '9';
           when "1010" => hex(hexlen -I) := 'A';
           when "1011" => hex(hexlen -I) := 'B';
           when "1100" => hex(hexlen -I) := 'C';
           when "1101" => hex(hexlen -I) := 'D';
           when "1110" => hex(hexlen -I) := 'E';
           when "1111" => hex(hexlen -I) := 'F';
           when "ZZZZ" => hex(hexlen -I) := 'z';
           when "UUUU" => hex(hexlen -I) := 'u';
           when "XXXX" => hex(hexlen -I) := 'x';
           when others => hex(hexlen -I) := '?';
         end case;
       end loop;
       return hex(1 to hexlen);
     end hstr;
	 
	 
	 
	-- convert slv array into a string vector in hex format
	function hstrv(input: BYTE_vector) return string_vector is
	   variable hex : string_vector(input'length-1 downto 0);
	begin
	  for i in 0 to input'length-1 loop
        hex(i):=hstr(input(i));
      end loop;
	  return hex;
	end hstrv;
	
    function hstrv(input: WORD_vector) return string_vector is
	   variable hex : string_vector(input'length-1 downto 0);
	begin
	  for i in 0 to input'length-1 loop
        hex(i):=hstr(input(i));
      end loop;
	  return hex;
    end hstrv;
    
	function hstrv(input: DWORD_vector) return string_vector is
	   variable hex : string_vector(input'length-1 downto 0);
	begin
	  for i in 0 to input'length-1 loop
        hex(i):=hstr(input(i));
      end loop;
	  return hex;
	end hstrv;
	
	-- convert string array into a string
	function to_string(input: string_vector) return string is
	   variable stringa:string(1 to 16*input'length);
	begin
	  for i in 0 to input'length-1 loop
	    for j in 1 to 16 loop
          stringa(j+(16*i)):=input(i)(j);
        end loop;
      end loop;
	  return stringa;
	end to_string;

   -- functions to manipulate strings
   -----------------------------------


   -- convert a character to upper case

   function to_upper(c: character) return character is

      variable u: character;

    begin

       case c is
        when 'a' => u := 'A';
        when 'b' => u := 'B';
        when 'c' => u := 'C';
        when 'd' => u := 'D';
        when 'e' => u := 'E';
        when 'f' => u := 'F';
        when 'g' => u := 'G';
        when 'h' => u := 'H';
        when 'i' => u := 'I';
        when 'j' => u := 'J';
        when 'k' => u := 'K';
        when 'l' => u := 'L';
        when 'm' => u := 'M';
        when 'n' => u := 'N';
        when 'o' => u := 'O';
        when 'p' => u := 'P';
        when 'q' => u := 'Q';
        when 'r' => u := 'R';
        when 's' => u := 'S';
        when 't' => u := 'T';
        when 'u' => u := 'U';
        when 'v' => u := 'V';
        when 'w' => u := 'W';
        when 'x' => u := 'X';
        when 'y' => u := 'Y';
        when 'z' => u := 'Z';
        when others => u := c;
    end case;

      return u;

   end to_upper;


   -- convert a character to lower case

   function to_lower(c: character) return character is

      variable l: character;

    begin

       case c is
        when 'A' => l := 'a';
        when 'B' => l := 'b';
        when 'C' => l := 'c';
        when 'D' => l := 'd';
        when 'E' => l := 'e';
        when 'F' => l := 'f';
        when 'G' => l := 'g';
        when 'H' => l := 'h';
        when 'I' => l := 'i';
        when 'J' => l := 'j';
        when 'K' => l := 'k';
        when 'L' => l := 'l';
        when 'M' => l := 'm';
        when 'N' => l := 'n';
        when 'O' => l := 'o';
        when 'P' => l := 'p';
        when 'Q' => l := 'q';
        when 'R' => l := 'r';
        when 'S' => l := 's';
        when 'T' => l := 't';
        when 'U' => l := 'u';
        when 'V' => l := 'v';
        when 'W' => l := 'w';
        when 'X' => l := 'x';
        when 'Y' => l := 'y';
        when 'Z' => l := 'z';
        when others => l := c;
    end case;

      return l;

   end to_lower;



   -- convert a string to upper case

   function to_upper(s: string) return string is

     variable uppercase: string (s'range);

   begin

     for i in s'range loop
        uppercase(i):= to_upper(s(i));
     end loop;
     return uppercase;

   end to_upper;



   -- convert a string to lower case

   function to_lower(s: string) return string is

     variable lowercase: string (s'range);

   begin

     for i in s'range loop
        lowercase(i):= to_lower(s(i));
     end loop;
     return lowercase;

   end to_lower;



-- functions to convert strings into other types


-- converts a character into a std_logic

function to_std_logic(c: character) return std_logic is 
    variable sl: std_logic;
    begin
      case c is
        when 'U' => 
           sl := 'U'; 
        when 'X' =>
           sl := 'X';
        when '0' => 
           sl := '0';
        when '1' => 
           sl := '1';
        when 'Z' => 
           sl := 'Z';
        when 'W' => 
           sl := 'W';
        when 'L' => 
           sl := 'L';
        when 'H' => 
           sl := 'H';
        when '-' => 
           sl := '-';
        when others =>
           sl := 'X'; 
    end case;
   return sl;
  end to_std_logic;


-- converts a string into std_logic_vector

function to_std_logic_vector(s: string) return std_logic_vector is 
  variable slv: std_logic_vector(s'high-s'low downto 0);
  variable k: integer;
begin
   k := s'high-s'low;
  for i in s'range loop
     slv(k) := to_std_logic(s(i));
     k      := k - 1;
  end loop;
  return slv;
end to_std_logic_vector;                                       
                                       
                                       
                                       
                                       
                                       
                                       
----------------
--  file I/O  --
----------------



-- read variable length string from input file
     
procedure str_read(file in_file: TEXT; 
                   res_string: out string) is
       
       variable l:         line;
       variable c:         character;
       variable is_string: boolean;
       
   begin
           
     readline(in_file, l);
     -- clear the contents of the result string
     for i in res_string'range loop
         res_string(i) := ' ';
     end loop;   
     -- read all characters of the line, up to the length  
     -- of the results string
     for i in res_string'range loop
        read(l, c, is_string);
        res_string(i) := c;
        if not is_string then -- found end of line
           exit;
        end if;   
     end loop; 
                     
end str_read;


-- print string to a file
procedure print(file out_file: TEXT;
                new_string: in  string) is
       
       variable l: line;
       
   begin
      
     write(l, new_string);
     writeline(out_file, l);
                     
end print;


-- print character to a file and start new line
procedure print(file out_file: TEXT;
                char: in  character) is
       
       variable l: line;
       
   begin
      
     write(l, char);
     writeline(out_file, l);
                     
end print;



-- appends contents of a string to a file until line feed occurs
-- (LF is considered to be the end of the string)

procedure str_write(file out_file: TEXT; 
                    new_string: in  string) is
 begin
      
   for i in new_string'range loop
      print(out_file, new_string(i));
      if new_string(i) = LF then -- end of string
         exit;
      end if;
   end loop;               
                     
end str_write;

end package body;
