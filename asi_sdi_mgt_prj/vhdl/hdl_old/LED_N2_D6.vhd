--                                                                            
-- .--------------------------------------------------------------------------.
-- |                                         |                                |
-- |     .oOo.                _              |  Project:                      |
-- |   .oOOOOOo.   _  _ _  __| |_ _ ___      |                                |
-- |   oOOOOOOOo  | || ' \/ _` | '_/ _ |     | BU2 for MIDS LVT               |
-- |   .oOOOOOO.  |_||_||_\__,_|_| \___|     |                                |
-- |     ·oOo·                               |                                |
-- |                                         |                                |
-- |--------------------------------------------------------------------------|
-- |                                                                          |
-- |        All rights reserved. (C) 2014 Indra Sistemas S.A. (Spain)         |
-- |        This file may not be used, copied, distributed, corrected,        |
-- |           modified, translated, transmitted or assigned without          |
-- |                         Indra's prior authorization.                     |
-- |                                                                          |
-- |--------------------------------------------------------------------------|
-- |   Engineer        |  Miguel Viñe Viñuelas (mvine@indra.es)               |
-- |--------------------------------------------------------------------------|
-- |   Module name     |  LED_N2_D6.vhd                                         |
-- |--------------------------------------------------------------------------|
-- |   Parent module   |                                                      |
-- |--------------------------------------------------------------------------|
-- |   Ver. / Rev.     |  02 / D       |   Date            |   11/02/2015     |
-- |--------------------------------------------------------------------------|
-- |   References      | [1] Test Procedure for VME Cycles Emulator FPGA      |
-- |                   | 0822291211180TP00_A0.pdf
-- |--------------------------------------------------------------------------|

-- Version: 02 / D		
-- Last version: 02 /C      
-- Begin date:         11/02/2015 
-- End date:           
----------------------------------------------------------------------------

-------------------------- Description -------------------------------------
-- This module turns on the light D6 of Reference [1], when the signal BERR_N is activated.
-- D6 of reference [1] corresponds to LED_N(2) of the file vme_emu.vhd. (The main file of the
-- VME cycles emulator firmware FPGA).

-- When BERR_N goes low, then LED_N must light.
-- When BERR_N changes from '0' to '1', then LED_N must keep lighting for 100 ms (Constant TimeLEDms)

-- BERR_N    -------------|_____|--------------|___|-----|____|---------------------
-- LED_N     -------------|______________|-----|________________________|---------
--                               <------->          <---->     <------->
--                                 100 ms            <100 ms     100 ms
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
---library ACT3;
---use ACT3.COMPONENTS.all;
use work.Int_Pkg.all;


entity LED_N2_D6 is
port(
    -- Inputs
    CLK         : in std_logic; -- 50 MHz
    Reset_N     : in std_logic; -- asynchronous reset (active low)
	BERR_N      : in std_logic;  -- Bus error
	-- Outputs
	LED_N 		: out std_logic);  --'0' LED is ON, '1' LED is OFF
end LED_N2_D6;

architecture Behavioral of LED_N2_D6 is
    type SelectLedState is (Led_OFF, Led_ON_BERR_Activated, Led_ON_BERR_Deactivated);
	signal Led_State: SelectLedState; -- There is a warning if if you initializes the signal 
begin

LightD6: process(Reset_N, clk)
	variable Counter	: integer	:= 0;

begin
if Reset_N = '0' then
	LED_N <= '1';
	Counter := 0;
	Led_State <= Led_OFF;
	
elsif CLK'event and CLK ='1' then
	CASE Led_State IS
		when Led_OFF => -- Initial State. Led is OFF
			if BERR_N = '0' then -- Led turns ON
				LED_N <= '0';
				Counter := 1;
				Led_State <= Led_ON_BERR_Activated;	
			else  -- Led keeps OFF
				LED_N <= '1';
				Counter := 0;
				Led_State <= Led_OFF;				
			end if;
		when Led_ON_BERR_Activated => -- Led is ON
			LED_N <= '0';
			if BERR_N = '0' then -- Led keeps ON
				Led_State <= Led_ON_BERR_Activated;	
				Counter := 1;
			else	
				Led_State <= Led_ON_BERR_Deactivated;
				Counter := 2;
			end if;
		when Led_ON_BERR_Deactivated => -- Led keeps off for TimeLEDms	
			if BERR_N = '0' then
				LED_N <= '0';
				Led_State <= Led_ON_BERR_Activated;	
				-- If Counter= TimeLEDms, then counter will be TimeLEDms+1.
				-- So LED_N will light 1 TCLK more than 100 
				Counter := 1; 
			else
				if Counter < TimeLEDms then
					Counter := Counter + 1;
					LED_N <= '0';
					Led_State <= Led_ON_BERR_Deactivated;	
				else
					Counter := 0;
					LED_N <= '1';
					Led_State <= Led_OFF;	
				end if;					
			end if;
		 when others => -- There is an error. The code must never reach this sentence.
			LED_N <= '1';
			Counter := 0;
			Led_State <= Led_OFF;	
	end case;
end if; 
end Process LightD6;
end behavioral;