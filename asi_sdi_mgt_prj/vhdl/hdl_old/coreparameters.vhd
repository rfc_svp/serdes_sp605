----------------------------------------------------------------------
-- Created by Actel SmartDesign Tue Dec 16 11:42:05 2014
-- Parameters for COREUART
----------------------------------------------------------------------


package coreparameters is
    constant FAMILY : integer := 17;
    constant HDL_license : string( 1 to 1 ) := "O";
    constant RX_FIFO : integer := 1;
    constant RX_LEGACY_MODE : integer := 0;
    constant testbench : string( 1 to 4 ) := "User";
    constant TX_FIFO : integer := 1;
    constant USE_SOFT_FIFO : integer := 0;
end coreparameters;
