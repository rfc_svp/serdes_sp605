--                                                                            
-- .--------------------------------------------------------------------------.
-- |                                         |                                |
-- |     .oOo.                _              |  Project:                      |
-- |   .oOOOOOo.   _  _ _  __| |_ _ ___      |                                |
-- |   oOOOOOOOo  | || ' \/ _` | '_/ _ |     | BU2 for MIDS LVT               |
-- |   .oOOOOOO.  |_||_||_\__,_|_| \___|     |                                |
-- |     �oOo�                               |                                |
-- |                                         |                                |
-- |--------------------------------------------------------------------------|
-- |                                                                          |
-- |        All rights reserved. (C) 2014 Indra Sistemas S.A. (Spain)         |
-- |        This file may not be used, copied, distributed, corrected,        |
-- |           modified, translated, transmitted or assigned without          |
-- |                         Indra's prior authorization.                     |
-- |                                                                          |
-- |--------------------------------------------------------------------------|
-- |   Engineer        |  Miguel Vi�e Vi�uelas (mvine@indra.es)               |
-- |--------------------------------------------------------------------------|
-- |   Module name     |  Int_Pkg                                  |
-- |--------------------------------------------------------------------------|
-- |   Parent module   |                                                      |
-- |--------------------------------------------------------------------------|
-- |   Ver. / Rev.     |  01 / A       |   Date            |   1/10/2014     |
-- |--------------------------------------------------------------------------|


-- Version: 01 / A		
-- Last version: This is the first version      
-- Begin date:         1/10/2014 
-- End date:           
----------------------------------------------------------------------------

-------------------------- Description -------------------------------------
-- This module includes the types, constants, functions and procedures that are necessary for the
-- modules Int_Handler and Int_Generator. 
-------------------------- End Description ---------------------------------

-------------------------- Arhitecture -------------------------------------
-- 
-------------------------- End Architecture --------------------------------

-------------------------- Changes made in version v0  ---------------------
-- 
----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
---library ACT3;
---use ACT3.COMPONENTS.all;

package Int_Pkg is

    type SelectBusRelease is (ROAK, RORA);
    type Select_8_16Bits is (Select8, Select16);
    type State_Handler is (Wait_IRQ_N, Wait_Request, Wait_DTACK, Execute_Routine); 
    type State_Interrupter is (WaitIntRequest, WaitIACK, GatedDaisyChain1, GatedDaisyChain2,  WaitDeactivateRORA1, WaitDeactivateRORA2);
    Type StatusID_Matrix is array (7 downto 1) of std_logic_vector(15 downto 0);
	constant TimeLEDms :integer := 1999999; --(2*10^7)-1 1s
    
    constant HalfTclk : integer :=10; --TCLK = 20 ns, FCLK=50 MHz
    constant TWO_us: integer := 97;
--    Constant IRQ4_GMUX :std_logic_vector(15 downto 0) := x"FF_D7";
--    Constant IRQ7_GMUX :std_logic_vector(15 downto 0) := x"FF_D6";
--    Constant IRQ4_AMUX :std_logic_vector(15 downto 0) := x"FF_DB";
--    Constant IRQ7_AMUX :std_logic_vector(15 downto 0) := x"FF_DA";
  
constant StatusID :StatusID_Matrix :=
(x"FF_E6", -- IRQ7, Corresponds to IRQ7 ETP
x"FF_E5",  -- IRQ6, spare ETP
x"FF_EC",  -- IRQ5, Corresponds to IRQ5 DP
x"FF_D7",  -- IRQ4, Corresponds to IRQ4 GMUX
x"FF_C0",  -- IRQ3, Corresponds to IRQ3 DISC
x"FF_E1",  -- IRQ2, spare ETP,
x"FF_E0"); -- IRQ1, spare ETP

-- Int_Handler Procedures

-- This procedure gives the value for all outputs in the module Int_Handler after a Reset, or in the Wait State.
procedure Reset_Handler (signal IACK_N, AS_N, DS0_N, DS1_N, DeviceWants_Bus: out std_logic; signal Address: out std_logic_vector(3 downto 1); signal NextSateHandler: out State_Handler; variable Timer: out std_logic_vector(2 downto 0); Signal Status_ID: out std_logic_vector(15 downto 0); Variable Count_Routine: out std_logic_vector(3 downto 0); Variable Wait2us: out integer);

-- This procedure arbitres all the requested interruptions. The most prioritary is the highest x in IRQ_N(x)
-- function Arbitration_Ints(signal IRQ_N: in std_logic_vector(7 downto 1)) return (signal std_logic_vector(2 downto 0));
procedure Arbitration_Ints(signal IRQ_N, ValidInts: in std_logic_vector(7 downto 1); signal Address :out std_logic_vector(3 downto 1));


-- Interrupter Procedures
-- This procedure gives the value for all outputs in the module Interrupter after a Reset.
procedure Reset_Interrupter (signal IACK_OUT_N, DTACK_N: out std_logic; signal DataOut: out std_logic_vector(15 downto 0); signal Next_State_Int: out State_Interrupter; signal Access_DTB: out std_logic);

-- This procedure assign value for the IRQ_N signal
-- Use of variable Activate_int:
      --'1' if the interrupter has to activate IRQ_N(x) --> IRQ_N(x)='0' and IRQ_N[y]='1' foy y /=x
      --'0' if the interrupter has to deactivate IRQ_N(x) --> IRQ_N(x)='1' and IRQ_N[y]='1' foy y /=x
procedure Assign_IRQ_X(Signal IntLevel_X: in std_logic_vector(2 downto 0); Activate_int: in std_logic; Signal IRQ_N: out std_logic_vector(7 downto 1));

end Int_Pkg;

package body Int_Pkg is

-- This procedure gives the value for all outputs in the module Int_Handler after a Reset, or in the Wait State.
procedure Reset_Handler (signal IACK_N, AS_N, DS0_N, DS1_N, DeviceWants_Bus: out std_logic; signal Address: out std_logic_vector(3 downto 1); signal NextSateHandler: out State_Handler; variable Timer: out std_logic_vector(2 downto 0); Signal Status_ID: out std_logic_vector(15 downto 0); Variable Count_Routine: out std_logic_vector(3 downto 0); Variable Wait2us: out integer) is
begin
    IACK_N <= '1';
    AS_N <= '1';
    DS0_N <= '1';
    DS1_N <= '1';
    DeviceWants_Bus <='0';
    Address <= "111";
    NextSateHandler <= Wait_IRQ_N;
    Timer := "000";
    Status_ID <= (others =>'0');
    Count_Routine := (others =>'0'); 
    Wait2us := 0;

--    IACK_N <= '1';
--    AS_N <= '1';
--    DS0_N <= '1';
--    DS1_N <= '1';
--    DeviceWants_Bus <='0';
--    Address >= "000";
--    NextSateHandler <= Wait_Handler
end Reset_Handler;

-- This functions arbitres all the requested interruptions. The most prioritary is the highest x in IRQ_N(x)
procedure Arbitration_Ints(signal IRQ_N, ValidInts: in std_logic_vector(7 downto 1); signal Address :out std_logic_vector(3 downto 1)) is
begin
    if IRQ_N(7) = '0' and ValidInts(7) = '1' then
        Address <="111";
    elsif IRQ_N(6) = '0' and ValidInts(6) = '1' then
        Address <="110";
    elsif IRQ_N(5) = '0' and ValidInts(5) = '1' then
        Address <="101";
    elsif IRQ_N(4) = '0' and ValidInts(4) = '1' then
        Address <="100";
    elsif IRQ_N(3) = '0' and ValidInts(3) = '1' then
        Address <="011";
    elsif IRQ_N(2) = '0' and ValidInts(2) = '1' then
        Address <="010";
    elsif IRQ_N(1) = '0' and ValidInts(1) = '1' then
        Address <= "001";
    else  -- There is an error if the functions reachs this else
        Address <= "000";
    end if;
end Arbitration_Ints;

procedure Reset_Interrupter (signal IACK_OUT_N, DTACK_N: out std_logic; signal DataOut: out std_logic_vector(15 downto 0); signal Next_State_Int: out State_Interrupter; signal Access_DTB: out std_logic) is
begin
    IACK_OUT_N <= '1';
    DTACK_N <= '1';
    DataOut <= (others =>'1');
    Next_State_Int <= WaitIntRequest; 
    Access_DTB <='0'; 
end Reset_Interrupter;

procedure Assign_IRQ_X(Signal IntLevel_X: in std_logic_vector(2 downto 0); Activate_int: in std_logic; Signal IRQ_N: out std_logic_vector(7 downto 1)) is
begin
    case IntLevel_X is       
        when "000" =>
            if Activate_int = '1' then
                IRQ_N <= "1111110";
            else
                IRQ_N <= "1111111";
            end if;
        when "001" =>
            if Activate_int = '1' then
                IRQ_N <= "1111110";
            else
                IRQ_N <= "1111111";
            end if;
        when "010" =>
            if Activate_int = '1' then
                IRQ_N <= "1111101";
            else
                IRQ_N <= "1111111";
            end if;
        when "011" =>
            if Activate_int = '1' then
                IRQ_N <= "1111011";
            else
                IRQ_N <= "1111111";
            end if;
        when "100" =>
            if Activate_int = '1' then
                IRQ_N <= "1110111";
            else
                IRQ_N <= "1111111";
            end if;
        when "101" =>
            if Activate_int = '1' then
                IRQ_N <= "1101111";
            else
                IRQ_N <= "1111111";
            end if;
        when "110" =>
            if Activate_int = '1' then
                IRQ_N <= "1011111";
            else
                IRQ_N <= "1111111";
            end if;
        when "111" =>
            if Activate_int = '1' then
                IRQ_N <= "0111111";
            else
                IRQ_N <= "1111111";
            end if;
        when others => -- There is an error. The code must never reach this sentence.
            IRQ_N <= (others => '1');
    end case;
end Assign_IRQ_X;

end Int_Pkg;
