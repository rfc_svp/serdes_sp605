--                                                                            
-- .--------------------------------------------------------------------------.
-- |                                         |                                |
-- |     .oOo.                _              |  Project:                      |
-- |   .oOOOOOo.   _  _ _  __| |_ _ ___      |                                |
-- |   oOOOOOOOo  | || ' \/ _` | '_/ _ |     | BU2 for MIDS LVT               |
-- |   .oOOOOOO.  |_||_||_\__,_|_| \___|     |                                |
-- |     �oOo�                               |                                |
-- |                                         |                                |
-- |--------------------------------------------------------------------------|
-- |                                                                          |
-- |        All rights reserved. (C) 2014 Indra Sistemas S.A. (Spain)         |
-- |        This file may not be used, copied, distributed, corrected,        |
-- |           modified, translated, transmitted or assigned without          |
-- |                         Indra's prior authorization.                     |
-- |                                                                          |
-- |--------------------------------------------------------------------------|
-- |   Engineer        |  Miguel Vi�e Vi�uelas (mvine@indra.es)               |
-- |--------------------------------------------------------------------------|
-- |   Module name     |  Int_Handler                                   |
-- |--------------------------------------------------------------------------|
-- |   Parent module   |                                                      |
-- |--------------------------------------------------------------------------|
-- |   Ver. / Rev.     |  01 / A       |   Date            |   30/09/2014     |
-- |--------------------------------------------------------------------------|
-- |   References      | [1]  Title: American National Standard for VME64     |
-- |                   | ANSI/VITA 1-1994 (R2002)	                          |
-- |                   | File name in disk: NormaVME.pdf                      |
-- |                   |                                                      |
-- |                   |                                                      |
-- |                   |                                                      |
-- |--------------------------------------------------------------------------|

-- Version: 01 / A		
-- Last version: This is the first version      
-- Begin date:         30/09/2014 
-- End date:           
----------------------------------------------------------------------------

-------------------------- Description -------------------------------------
-- This is the handler interrupt module of a VME protocol.(Chapter 4 of reference [1]).
-- 1) This module receives the interrupt request from an Interrupt_Generator. 
-- 2) Then arbitrates all the interrupt request received and select the more prioritary.
-- 3) Then request the DTB activating DeviceWants_Bus.
-- 4) When the arbitrer grants the DTB for this module (when DTB_Granted�=�1�) then this module
--    activates IACK_N and request the status I/D from the interrupt generator.
-- 5) When DTACK ='0' this module reads the Status I/D.
-- 6) The module executes the interruption routine indicates in the status IID     

-- Note: this module drives the following bus signals: IACK_N, AS_N, DS0_N, DS1_N and Address.
-- The Int_Handler need access to the DTB since DTB_Granted ='1', until DeviceWants_Bus ='0'

------------ Process of accessing the DTB------------------------------------------:
-- 1) When an external module activates DTB_Granted, then the Int_Handler needs access to the DTB.
-- 2) The Int_Handler keeps the access to the DTB while DeviceWants_Bus ='1'
-- 3) The Int_Handler keeps the access to the DTB until the falling edge of DeviceWants_Bus.
-- Note: When IRQ_N(x) is active, the Int_Handler activates DeviceWants_Bus. But Int_Handler does not need the DTB until 
-- DTB_Granted = '1'.
-------------------------- End Description ---------------------------------

-------------------------- Arhitecture -------------------------------------
-- 
-------------------------- End Architecture --------------------------------

-------------------------- Changes made in version v0  ---------------------
-- 
----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
---library ACT3;
---use ACT3.COMPONENTS.all;
use work.Int_Pkg.all;


entity Int_Handler is
port(
    -- Three State input signals: IRQ_N, DTACK_N, BERR_N, DataIn. 
    -- Three State output signals: IACK_N, AS_N, DS0_N, DS1_N, Address .

    -- Inputs
    CLK         : in std_logic; -- 50 MHz
    Reset_N     : in std_logic; -- asynchronous reset (active low)
    IRQ_N       : in std_logic_vector(7 downto 1);  --Interrupt request.
    IACK_IN_N   : in std_logic;  -- Input of the IACK daisy chain.
    DTACK_N     : in std_logic;  -- Data transfer acknowledge
    BERR_N      : in std_logic;  -- Bus error
    DataIn      : in std_logic_vector(15 downto 0); -- For reading the Status I/D from the interrupt generator.
    ValidInts   : in std_logic_vector(7 downto 1); -- Indicates if an interruption is accepted or not accepted by the handler.
                                                   -- ValidInts(7) ='1'; the handler accepts IRQ7
                                                   -- ValidInts(7) ='0'; the handler does not accept IRQ7
                                                   -- ValidInts(1) ='1'; the handler accepts IRQ1
                                                   -- ValidInts(1) ='0'; the handler does not accept IRQ1
    DTB_Granted : in std_logic; --'1' if the arbitrer grants the DTB to this module.
                                -- '0' if the arbitrer does not grant the DTB to this module.
    SelectBits : in Select_8_16Bits; --Select8 --> The handler generates 8-bit interrupt acknowledge cycles.
                                     --Select16 --> The handler generates 16-bit interrupt acknowledge cycles.
    BusRelease  : in SelectBusRelease; -- ROAK or RORA. 


    -- Outputs
    IACK_N       : out std_logic;-- Interrupt acknowledge. It must be connected to the IACK_IN_N pin of the system controller module.
    IACK_Out_N   : out std_logic;-- Output of the IACK daisy chain.
    
    AS_N         : out std_logic; -- Address strobe
    DS0_N        : out std_logic; -- Data strobe 0
    DS1_N        : out std_logic; -- Data strobe 1
    DeviceWants_Bus: out std_logic; --'1' if the handler needs to acessing to the DTB.
                                     --'0' if the handler does not need to acessing to the DTB.
    Address       		: out std_logic_vector(3 downto 1); 
    StatusID_irqh       : out std_logic_vector(31 downto 0)
	);    -- Three State output signals: IACK_N, AS_N, DS0_N, DS1_N and Address
end Int_Handler;


architecture Behavioral of Int_Handler is
    --Signal NextSateHandler : State_Handler:= Wait_IRQ_N;   -- There is a warning if if you initializes the signal 
    Signal NextSateHandler : State_Handler;    
    Signal reg_status_id : std_logic;    
    Signal Status_ID: std_logic_vector(15 downto 0) := (others => '0'); -- We use a signal for monitoring it in tue d�simulation.
	
	signal dtack_b_in_reg	 		: std_logic;
	signal dtack_b_in_flt	 		: std_logic;
begin

-- The handler always pass the value of IACK_IN_N to IACK_Out_N. It is only a bridge. 
    IACK_Out_N <= IACK_IN_N;

    
Handler: process(Reset_N, clk)

    Variable Timer: std_logic_vector(2 downto 0):="000";
    Variable Count_Routine : std_logic_vector(3 downto 0):=(others => '0');
    variable Wait2us :integer:=0;
begin
if Reset_N = '0' then
    Reset_Handler (IACK_N, AS_N, DS0_N, DS1_N, DeviceWants_Bus, Address, NextSateHandler, Timer, Status_ID, Count_Routine, Wait2us);
	reg_status_id <= '0';
elsif CLK'event and CLK ='1' then
         CASE NextSateHandler IS
            when Wait_IRQ_N => --State waiting for a IRQ_N
			reg_status_id <= '0';
                if IRQ_N = "1111111" then
                    Reset_Handler (IACK_N, AS_N, DS0_N, DS1_N, DeviceWants_Bus, Address, NextSateHandler, Timer, Status_ID, Count_Routine, Wait2us);
                elsif ((not IRQ_N) and ValidInts) > "0000000" then
                     --There is at least one IRQ_N(x) activate ('0'). And the IRQ_N(x) activated are
                     -- accepted by the Interrupt Handler. 
                     -- We have IRQ_N(x)='0' (activated) and ValidInts(x) ='1', the interrupt handler accepts the x interrupt.            
                   -- Request access to DTB and pass to next state.
                   NextSateHandler <= Wait_Request;
                   DeviceWants_Bus <='1';                   
                   IACK_N <= '1';
                   AS_N <= '1';
                   DS0_N <= '1';
                   DS1_N <= '1';
                   Address <= "111";
                   Timer := "000";
                   Status_ID <= (others =>'0');
                   Count_Routine := (others => '0');
                   Wait2us := 0;
                else 
                     --There is at least one IRQ_N(x) activate ('0'). But the IRQ_N(x) activated are not 
                     -- accepted by the Interrupt Handler. 
                     -- We have IRQ_N(x)='0' (activated) and ValidInts(x) ='0', the interrupt handler does not accept the
                     -- x interrupt.
                    Reset_Handler (IACK_N, AS_N, DS0_N, DS1_N, DeviceWants_Bus, Address, NextSateHandler, Timer, Status_ID, Count_Routine, Wait2us);
                end if;
            when Wait_Request =>-- State between the handler request the DTB, (activating DeviceWants_Bus), and the requester 
            reg_status_id <= '0';                 -- grants the DTB (activating DTB_Granted).                 
                if DTB_Granted = '1' then
                    NextSateHandler <= Wait_DTACK;
                else
                    NextSateHandler <= Wait_Request;
                end if;            
                IACK_N <= '1';
                AS_N <= '1';
                DS0_N <= '1';
                DS1_N <= '1';
                DeviceWants_Bus <='1';                
                Address <= "111";
                Timer := "000";
                Status_ID <= (others =>'0');
                Count_Routine := (others =>'0');
                Wait2us :=0;
             when Wait_DTACK => -- State between the handler has the control of the DTB, (DTB_Granted is active), and the interrupter
             reg_status_id <= '0';                 -- activates DTACK_N or BERR_N                                                              
                    Wait2us :=0;
                    Case Timer is
                        when "000" =>
                            if BERR_N = '0' then -- Returns to initial state. No more actions are required 
                                IACK_N <= '1';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='0';
                                Address <= "111";
                                NextSateHandler <= Wait_IRQ_N;
                                Timer := "000";
                                Status_ID <= (others =>'0');
                                Count_Routine := (others =>'0');
                            elsif DTACK_N = '1' then
                                IACK_N <= '0';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='1';
                                -- Arbitration of all the interrupts requested   
                                Arbitration_Ints(IRQ_N, ValidInts, Address);
                                NextSateHandler <= Wait_DTACK;
                                Timer := "001"; 
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');  
                            else -- The DTB is busy, so it is necessary to wait until DTACK is released        
                                IACK_N <= '1';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='1';                         
                                Address <= "111";
                                NextSateHandler <= Wait_DTACK;
                                Timer := "000"; 
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');                 
                            end if;                              
                         when "001" => -- Waiting for at least 40 ns before activating AS_N
                            if BERR_N = '0' then
                                IACK_N <= '1';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='1'; --'1' for releasing the DeviceWants_Bus at the same time of IACK_N and address.                                
                                Address <= "000";
                                NextSateHandler <= Wait_IRQ_N;
                                Timer := "000"; 
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');   
                            else
                                IACK_N <= '0';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='1';                             
                                -- Address keeps the value. We do not assign value in this moment to simplify the code.
                                -- Arbitration_Ints(IRQ_N, ValidInts, Address);
                                NextSateHandler <= Wait_DTACK;
                                Timer := "010";
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');
                            end if;                              
                         when "010" => -- Waiting for at least 40 ns before activating AS_N
                            if BERR_N = '0' then
                                IACK_N <= '1';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='1'; --'1' for releasing the DeviceWants_Bus at the same time of IACK_N and address.                               
                                Address <= "000";
                                NextSateHandler <= Wait_IRQ_N;
                                Timer := "000"; 
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');                               
                            else
                                IACK_N <= '0';
                                AS_N <= '0';
                                DS0_N <= '0';
                                if SelectBits = Select8 then 
                                    DS1_N <= '1';
                                else
                                    DS1_N <= '0';
                                end if;
                                DeviceWants_Bus <='1';                                
                                -- Address keeps the value. We do not assign value in this moment to simplify the code.
                                -- Arbitration_Ints(IRQ_N, ValidInts, Address);
                                NextSateHandler <= Wait_DTACK;
                                Timer := "011";                            
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');
                            end if;                            
                        when "011" => --AS_N must be activated for at least 40 ns.
                            if BERR_N = '0' then
                                IACK_N <= '1';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='1'; --'1' for releasing the DeviceWants_Bus at the same time of IACK_N, AS_N, DS0_N, DS1_N and address.                                
                                Address <= "000";
                                NextSateHandler <= Wait_IRQ_N;
                                Timer := "000"; 
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');                               
                            else
                                IACK_N <= '0';
                                AS_N <= '0';
                                DS0_N <= '0';
                                if SelectBits = Select8 then 
                                    DS1_N <= '1';
                                else
                                    DS1_N <= '0';
                                end if;
                                DeviceWants_Bus <='1';                               
                                -- Address keeps the value. We do not assign value in this moment to simplify the code.
                                -- Arbitration_Ints(IRQ_N, ValidInts, Address);
                                NextSateHandler <= Wait_DTACK;
                                Timer := "100";                             
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');
                            end if;                           
                        when "100" =>--AS_N must be activated for at least 40 ns.        
                            if BERR_N = '0' then
                                IACK_N <= '1';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='1'; --'1' for releasing the DeviceWants_Bus at the same time of IACK_N, AS_N, DS0_N, DS1_N and address.                                
                                Address <= "000";
                                NextSateHandler <= Wait_IRQ_N;
                                Timer := "000"; 
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');                               
                            else                                                        
                                DS0_N <= '0';
                                if SelectBits = Select8 then 
                                    DS1_N <= '1';
                                else
                                    DS1_N <= '0';
                                end if;
                                DeviceWants_Bus <='1';
                                if DTACK_N = '0' then
                                    NextSateHandler <= Execute_Routine;
                                    Timer := "101";
                                    IACK_N <= '1';
                                    Address <= "000";
                                    AS_N <= '1';
                                else
                                    NextSateHandler <= Wait_DTACK;
                                    Timer := "100";
                                    IACK_N <= '0';
                                    AS_N <= '0';                                    
                                end if;
                                -- Address keeps the value. We do not assign value in this moment to simplify the code.
                                -- Arbitration_Ints(IRQ_N, ValidInts, Address);
                                Status_ID <= (others =>'0');
                                Count_Routine:= (others =>'0');                               
                            end if;
                        when others => -- There is an error. The code must never reach this sentence.
                            IACK_N <= '1';
                            AS_N <= '1';
                            DS0_N <= '1';
                            DS1_N <= '1';
                            DeviceWants_Bus <='0';
                            Address <= "111";
                            NextSateHandler <= Wait_IRQ_N;     
                            Timer := "000";                       
                            Status_ID <= (others =>'0');
                            Count_Routine:= (others =>'0');                               
                       end case;                   
            when Execute_Routine=> -- In this state the interrupt handler executes the interrpution routine
			 if BERR_N = '0' then
                    IACK_N <= '1';
                    AS_N <= '1';
                    DS0_N <= '1';
                    DS1_N <= '1';
                    DeviceWants_Bus <='1'; --'1' for releasing the DeviceWants_Bus at the same time of IACK_N, AS_N, DS0_N, DS1_N and address.                                
                    Address <= "000";
                    NextSateHandler <= Wait_IRQ_N;
                    Timer := "000"; 
                    Status_ID <= (others =>'0');
                    Count_Routine:= (others =>'0'); 
                    Wait2us :=0;       
                  else
                Case Timer is
                    when "101" =>-- Waiting for at least 25 ns before release DS0 and DS1 
						reg_status_id <= '0';
                        IACK_N <= '1';
                        AS_N <= '1';
                        DS0_N <= '0';
                        if SelectBits = Select8 then 
                            DS1_N <= '1';
                        else
                            DS1_N <= '0';
                        end if;
                        DeviceWants_Bus <='1';
                         
                        Address <= "000";
                        NextSateHandler <= Execute_Routine;                        
                        Timer := "110";                        
                        Status_ID <= (others =>'0');
                        Count_Routine := (others => '0');
                        Wait2us :=0;
                    when "110" =>-- Finished waiting the 25 ns minimum time.
					reg_status_id <= '1';
                        IACK_N <= '1';
                        AS_N <= '1';
                        DS0_N <= '1';
                        DS1_N <= '1';
                        DeviceWants_Bus <='1';  --'1' for releasing the DeviceWants_Bus at the same time of IACK_N, AS_N, DS0_N, DS1_N and address.
                        
                        Address <= "000";
                        NextSateHandler <= Execute_Routine;
                        Timer := "111";
                        if SelectBits = Select8 then 
                            Status_ID(15 downto 8) <= x"FF";
                            Status_ID(7 downto 0) <= DataIn(7 downto 0);
                        else
                            Status_ID <= DataIn;
                        end if;
                        Count_Routine := (others => '0');
                        Wait2us :=0;
                    when "111" => --Execute the interruption routine.
					reg_status_id <= '0';
                        If Count_Routine = "1111" then -- Finished the routine
                            IACK_N <= '1';
                            AS_N <= '1';
                            DS0_N <= '1';
                            DS1_N <= '1';
                            DeviceWants_Bus <='0';                                                                                                         
                            Address <= "111";                                                        
                            NextSateHandler <= Wait_IRQ_N;
                            Timer := "000";
                            Status_ID <= (others =>'0');                            
                            Count_Routine := (others =>'0');
                            Wait2us :=0;
                        else -- Executing the routine.
                            if BusRelease = ROAK then 
                                IACK_N <= '1';
                                AS_N <= '1';
                                DS0_N <= '1';
                                DS1_N <= '1';
                                DeviceWants_Bus <='1';  --'1' for releasing the DeviceWants_Bus at the same time of IACK_N, AS_N, DS0_N, DS1_N and address.                               
                                Address <= "000";
                                NextSateHandler <= Execute_Routine;
                                Timer := "111";
                                --Status_ID keeps the value
                                Count_Routine := Count_Routine +"0001";  
                                Wait2us :=0;                                
                            else --RORA mode                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                Timer := "111";
                                DeviceWants_Bus <='1';  --'1' for releasing the DeviceWants_Bus at the same time of IACK_N, AS_N, DS0_N, DS1_N and address.
                                NextSateHandler <= Execute_Routine;
                                if Count_Routine < "0101" then -- 5 TCLKS in this if
                                    IACK_N <= '1';
                                    Address <= "000";  
                                    AS_N <= '1';
                                    DS0_N <= '1';
                                    DS1_N <= '1';                                    
                                    if SelectBits = Select8 then 
                                        Status_ID(15 downto 8) <= x"FF";
                                        Status_ID(7 downto 0) <= DataIn(7 downto 0);
                                    else
                                        Status_ID <= DataIn;
                                    end if;
                                    Count_Routine := Count_Routine +"0001";
                                    Wait2us :=0;
                                elsif Count_Routine < "1010" then -- 5 TCLKS in this if
                                    IACK_N <= '1';
                                    Address <= "000";  
                                    AS_N <= '0';
                                    DS0_N <= '0';
                                    DS1_N <= '0';
                                    Status_ID <= (others =>'0');  
                                    Count_Routine := Count_Routine +"0001";
                                    Wait2us :=0;
                                elsif Count_Routine < "1101" then -- 3 TCLKS in this if 
                                   IACK_N <= '1';
                                   Address <= "000";  
                                   AS_N <= '1';
                                   DS0_N <= '1';
                                   DS1_N <= '1';                                
                                   Status_ID <= (others =>'0');  
                                   Count_Routine := Count_Routine +"0001";
                                   Wait2us := 0;                                   
                                 else 
                                   IACK_N <= '1';
                                   Address <= "111";  
                                   AS_N <= '1';
                                   DS0_N <= '1';
                                   DS1_N <= '1';                                
                                   Status_ID <= (others =>'0');  
                                   Wait2us := Wait2us +1;
                                    if Wait2us = TWO_us then
                                        Count_Routine := "1111"; -- Finished waiting
                                         -- Wait2us keeps value
                                    else
                                        Count_Routine := "1110"; -- Keeps in this if sentence                                       
                                    end if;                                                                   
                                 end if;                                                                                                                                                                                                                           
                            end if;
                        end if;
                    when others => -- There is an error. The code must never reach this sentence.
                        Reset_Handler (IACK_N, AS_N, DS0_N, DS1_N, DeviceWants_Bus, Address, NextSateHandler, Timer, Status_ID, Count_Routine, Wait2us);
						reg_status_id <= '0';
                end case;
                end if;
            when others =>-- There is an error. The code must never reach this sentence.
                Reset_Handler (IACK_N, AS_N, DS0_N, DS1_N, DeviceWants_Bus, Address, NextSateHandler, Timer, Status_ID, Count_Routine, Wait2us);
				reg_status_id <= '0';
          end case;
end if;
end process Handler;


	REG_STATUS_ID_IRQH: process (CLK, Reset_n) 
		begin
		  if (Reset_n = '0') then
				StatusID_irqh <= (OTHERS => '0');
			elsif (CLK'event and CLK = '1') then
				if dtack_b_in_flt = '0' then 
					StatusID_irqh <= X"0000" & DataIn;
				end if;
		  end if;
		end process; 
	
	--DTACK EVENT
		
		REG_DTACK_IN: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				dtack_b_in_reg<= '1';
			elsif (CLK'event and CLK = '1') then
				dtack_b_in_reg <= DTACK_N;
			end if;
		end process;
		
		dtack_b_in_flt <= not(not DTACK_N and dtack_b_in_reg);

end Behavioral;
