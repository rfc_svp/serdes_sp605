library ieee;
use ieee.std_logic_1164.all;

entity reset_circuit is
    port( rst_p	: in std_logic;
          CLK	: in std_logic;
          rst_n	: out std_logic
        );
end reset_circuit;

architecture DEF_ARCH of reset_circuit is 

  signal DFF_0_Q,DFF_1_Q : std_logic;

begin 

DFF_0:process(CLK,rst_p)
begin
	if rst_p='0' then
		DFF_0_Q <= '0';
	elsif CLK='1' and CLK'event then
		DFF_0_Q <= '1';
	end if;
end process;

DFF_1:process(CLK,rst_p)
begin
	if rst_p='0' then
		DFF_1_Q <= '0';
	elsif CLK='1' and CLK'event then
		DFF_1_Q <= DFF_0_Q;
	end if;
end process;

rst_n <= DFF_1_Q;
  
-- mycounter_0 : entity work.mycounter
    -- port map(
			-- CLK => CLK,
			-- Resetn_in => DFF_1_Q, 
			-- Resetn_out => rst_n);

end DEF_ARCH; 