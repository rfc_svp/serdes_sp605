--
--	APSC Controller AAIF/AMC FW Simulation Package
--
--		08/11/2011 - drhoms
--

library ieee;
use ieee.std_logic_1164.all;
use work.text_util_common_pkg.all;
use work.utils_common_pkg.all;

package sim_common_pkg is
		
	--
    procedure report_read_data(
		   CS : in natural;
		   ADDRESS : in std_logic_vector;
		   DATA_TO_REPORT : in std_logic_vector;
		   NBITS : in natural;
		   BURST : in natural);
    procedure report_write_data(
		   CS : in natural;
		   ADDRESS : in std_logic_vector;
		   DATA_TO_REPORT : in std_logic_vector;
		   NBITS : in natural;
		   Write_Enable_s : in string;
		   BURST : in natural);
	procedure check_read_data(
   		   CS : in natural;
		   ADDRESS : in std_logic_vector;
		   DATA_TO_REPORT : in std_logic_vector;
		   DATA_EXPECTED : in std_logic_vector;
		   NBITS : in natural;
		   PAR: in std_logic_vector;
		   BURST : in natural);

    --
	procedure assert_proc(
				condition_left  : in std_logic_vector; 
				condition_right : in std_logic_vector; 
				report_string   : in string);
	procedure assert_proc(
				condition_left  : in std_logic; 
				condition_right : in std_logic; 
				report_string   : in string);
	procedure assert_proc(
				condition_left  : in character; 
				condition_right : in character; 
				report_string   : in string);

	--
	procedure assert_proc_note(
				condition_left  : in std_logic_vector; 
				condition_right : in std_logic_vector; 
				report_string   : in string);
	procedure assert_proc_note(
				condition_left  : in std_logic; 
				condition_right : in std_logic; 
				report_string   : in string);
				
	--
    function valid_BIT(arg : in std_logic) return std_logic;
    function valid_STD_LOGIC_VECTOR(arg : in std_logic_vector) return std_logic;	

end package;

package body sim_common_pkg is

    procedure report_read_data(
	  CS : in natural;
	  ADDRESS : in std_logic_vector;
	  DATA_TO_REPORT : in std_logic_vector;
	  NBITS : in natural;
	  BURST : in natural) is
    begin
	  print ("READ TRANSFER "&str(BURST)&HT&
	          "CS="&str(CS)&HT&
	          "ADDRESS="&hstr(ADDRESS)&HT&
	          "DATA="&hstr(DATA_TO_REPORT(31 DOWNTO 31-NBITS+1))
	          &CR);
    end procedure;
	
	procedure report_write_data(
	  CS : in natural;
	  ADDRESS : in std_logic_vector;
	  DATA_TO_REPORT : in std_logic_vector;
	  NBITS : in natural;
	  Write_Enable_s : in string;
	  BURST : in natural) is
    begin
	  print ("WRITE TRANSFER "&str(BURST)&HT&
	          "CS="&str(CS)&HT&
	          "ADDRESS="&hstr(ADDRESS)&HT&
	          "DATA="&hstr(DATA_TO_REPORT(31 DOWNTO 31-NBITS+1))&HT&
			  Write_Enable_s
	          &CR);
    end procedure;
	 
	procedure check_read_data(
   	  CS : in natural;
	  ADDRESS : in std_logic_vector;
	  DATA_TO_REPORT : in std_logic_vector;
	  DATA_EXPECTED : in std_logic_vector;
	  NBITS : in natural;
	  PAR: in std_logic_vector;
	  BURST : in natural) is
    begin

	    report_read_data(CS,ADDRESS,DATA_TO_REPORT,NBITS,BURST);
	 	 
		-- COMPROBACIÓN DE VALOR
		assert_proc(DATA_TO_REPORT(31 DOWNTO 31-NBITS+1),DATA_EXPECTED(31 DOWNTO 31-NBITS+1),
			"READ TRANSFER FAIL");

		-- COMPROBACIÓN DE PARIDAD
		assert_proc(byte_parity_check(DATA_TO_REPORT,PAR,'1'),"0000",
			"DATA PARITY ERROR");
		--PRINT(str(byte_parity_gen(DATA_TO_REPORT,'1')));
		--PRINT(str(PAR));
			
     end procedure;
	 
	procedure assert_proc(
				condition_left  : in std_logic_vector; 
				condition_right : in std_logic_vector; 
				report_string   : in string) is
	begin
		assert condition_left = condition_right
		report report_string &" "& hstr(condition_left) &" "& hstr(condition_right)
		severity failure;
		--severity error;
	end procedure;

	procedure assert_proc(
				condition_left  : in std_logic; 
				condition_right : in std_logic; 
				report_string   : in string) is
	begin
		assert condition_left = condition_right
		report report_string &" "& str(condition_left) &" "& str(condition_right)
		severity failure;
		--severity error;
	end procedure;
	
	procedure assert_proc(
				condition_left  : in character; 
				condition_right : in character; 
				report_string   : in string) is
	begin
		assert condition_left = condition_right
		report report_string &" "& condition_left &" "& condition_right
		severity failure;
		--severity error;
	end procedure;	

	procedure assert_proc_note(
				condition_left  : in std_logic_vector; 
				condition_right : in std_logic_vector; 
				report_string   : in string) is
	begin
		assert condition_left = condition_right
		report report_string &" "& hstr(condition_left) &" "& hstr(condition_right)
		severity note;
		--severity error;
	end procedure;

	procedure assert_proc_note(
				condition_left  : in std_logic; 
				condition_right : in std_logic; 
				report_string   : in string) is
	begin
		assert condition_left = condition_right
		report report_string &" "& str(condition_left) &" "& str(condition_right)
		severity note;
	end procedure;
	
    function valid_BIT(arg : in std_logic) return std_logic is
    begin
      case arg is
          when '0' => return '1';
          when '1' => return '1';
          when others => return '0';
      end case;
    end;
    
    function valid_STD_LOGIC_VECTOR(arg : in std_logic_vector) return std_logic is
      variable aux:std_logic;
    begin
      aux:='1';
	  for i in arg'low to arg'high loop
          aux:=aux and valid_BIT(arg(i));
	  end loop;
	  return aux;
    end;	
	
end package body;