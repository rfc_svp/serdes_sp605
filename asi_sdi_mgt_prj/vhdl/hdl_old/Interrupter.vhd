--                                                                            
-- .--------------------------------------------------------------------------.
-- |                                         |                                |
-- |     .oOo.                _              |  Project:                      |
-- |   .oOOOOOo.   _  _ _  __| |_ _ ___      |                                |
-- |   oOOOOOOOo  | || ' \/ _` | '_/ _ |     | BU2 for MIDS LVT               |
-- |   .oOOOOOO.  |_||_||_\__,_|_| \___|     |                                |
-- |     �oOo�                               |                                |
-- |                                         |                                |
-- |--------------------------------------------------------------------------|
-- |                                                                          |
-- |        All rights reserved. (C) 2014 Indra Sistemas S.A. (Spain)         |
-- |        This file may not be used, copied, distributed, corrected,        |
-- |           modified, translated, transmitted or assigned without          |
-- |                         Indra's prior authorization.                     |
-- |                                                                          |
-- |--------------------------------------------------------------------------|
-- |   Engineer        |  Miguel Vi�e Vi�uelas (mvine@indra.es)               |
-- |--------------------------------------------------------------------------|
-- |   Module name     |  Interrupter                                         |
-- |--------------------------------------------------------------------------|
-- |   Parent module   |                                                      |
-- |--------------------------------------------------------------------------|
-- |   Ver. / Rev.     |  01 / A       |   Date            |   17/10/2014     |
-- |--------------------------------------------------------------------------|
-- |   References      | [1]  Title: American National Standard for VME64     |
-- |                   | ANSI/VITA 1-1994 (R2002)	                          |
-- |                   | File name in disk: NormaVME.pdf                      |
-- |                   |                                                      |
-- |                   |                                                      |
-- |                   |                                                      |
-- |--------------------------------------------------------------------------|

-- Version: 01 / A		
-- Last version: This is the first version      
-- Begin date:         17/10/2014 
-- End date:           
----------------------------------------------------------------------------

-------------------------- Description -------------------------------------
-- This is the interrupter or generator module of a VME protocol. (Chapter 4 of reference [1]).


-- The IntRequest indicates this module that it must request an interruption.
-- The parent module must deactivate IntRequest as soon as possible, for avoid that this module requests
-- the same interruption when it has finished the last requesting.
-- The best option is that the parent module activates IntRequest for  only 1 TCLK. But it is possible activating it for a few TCLKs.
 
-- 1) When the IntRequest signal ='1', then this module activates IRQ_N(x).
-- 2) All the IRQ_N[y] foy y /=x are always '1'.
-- 3) The IRQ_N(x) can be '1' deactivated, or '0' activated. 
-- 4) The x number is provided by IntLevelX(2 downto 0).
-- 5) When IACK_N = '0' then (The handler has the control of the DTB and can execute the interruption routine) 
--    5 a) The interrupter module has IRQ_N(x) activate (a interrupt request pending) and
--    5 b) If Address (3 downto 1) = IntLevel_X(2 downto 0) and
--    5 c) if (SelectBits = Select8 and DS0_N=0) or (SelectBits = Select16 and DS0_N=0 and DS1_N=0).
--          If conditions a, b and c are met, then 
--             The interrupter puts '1' in at IACK_Out_N (blocks the interruption daisy chain).
--             Puts Status ID at the Data Bus (DataOut)
--             Deactivates IRQ_N(x) (ROAK mode)
--             Activates DTACK
--          else
--             The interrupter continues the IACK daisy chain. (IACK_OUT_N= IACK_IN_N)
--          end if
-- 6) In RORA mode, IRQN(x) is deactivated in the falling edge of DS0_N in the register access cycle, so we need:
--    6a) Wait until DS0_N= '1'
--    6b) Wait until DS0_N = '0'. At this moment, the module deactivates IRQ_N(x) 

------------ Process of accessing the DTB------------------------------------------:
-- If Access_DTB='1', then the Interrupter needs access to the DTB
-- If Access_DTB='0', then the Interrupter does not need access to the DTB
-------------------------- End Description ---------------------------------

-------------------------- Arhitecture -------------------------------------
-- 
-------------------------- End Architecture --------------------------------

-------------------------- Changes made in version v0  ---------------------
-- 
----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
---library ACT3;
---use ACT3.COMPONENTS.all;
use work.Int_Pkg.all;


entity Interrupter is
port(
    -- Three State input signals: Address, AS_N, DS0_N and DS1_N. 
    -- Three State output signals: IRQ_N, DTACK_N, DataOut 
     
    -- Inputs
    CLK         : in std_logic; -- 50 MHz
    Reset_N     : in std_logic; -- asynchronous reset (active low)
    IACK_IN_N   : in std_logic;  -- Input of the IACK daisy chain.
    Address     : in std_logic_vector(3 downto 1); -- Interruption level sent by the handler
    AS_N        : in std_logic; -- Adress strobe
    DS0_N       : in std_logic; -- Data strobe 0
    DS1_N       : in std_logic; -- Data strobe 1
    
    SelectBits  : in Select_8_16Bits; --Select8 --> The handler generates 8-bit interrupt acknowledge cycles.
                                     --Select16 --> The handler generates 16-bit interrupt acknowledge cycles.
    BusRelease  : in SelectBusRelease; -- ROAK or RORA. (Only ROAK is implemented at this time).
    IntLevel_X  : in std_logic_vector(2 downto 0); -- To indicate the interruption level of this module.
                                                   -- "111" --> IRQ7
                                                   -- "001" --> IRQ1.
                                                   -- "000" is allowed Only while the sistem is in reset condition. 
                                                   -- If Reset_N = 0 then IntLevel_X may be "000"
                                                   -- If Reset_N = 1, then IntLevel_X must be in range "001" to "111".
    IntRequest  : in std_logic; --'1' To request and interruption.

     -- Outputs
    IRQ_N       : out std_logic_vector(7 downto 1);  --Interrupt request.    
    IACK_OUT_N  : out std_logic; -- Output of the IACK daisy chain
    DTACK_N     : out std_logic;  -- Data transfer acknowledge    
    DataOut     : out std_logic_vector(15 downto 0);  -- For writing the interruption Status I/D.
    Access_DTB  : out std_logic); -- '1' if the Interrupter needs access to the DTB
                                  -- '0' if the interrupter does not need access to the DTB.
end Interrupter;

architecture Behavioral of Interrupter is

--signal Next_State_Int: State_Interrupter:= WaitIntRequest;
signal Next_State_Int: State_Interrupter; -- There is a warning if if you initializes the signal 
begin
Interrupt: process(Reset_N, clk)
 variable X_Integer: integer:= 0;
 variable WaitDTACK_High :std_logic_vector(1 downto 0):="00"; -- for keeping DTACK high 3 TCLKs after DS0_N goes high.
begin
if Reset_N = '0' then
   Reset_Interrupter (IACK_OUT_N, DTACK_N, DataOut, Next_State_Int, Access_DTB);
   IRQ_N <= (others =>'1');
   X_Integer :=1;
   WaitDTACK_High :="00";
elsif CLK'event and CLK ='1' then
    X_Integer := conv_integer(IntLevel_X);
    CASE Next_State_Int IS
    when WaitIntRequest => -- Waiting until IntRequest = '1'; -- Propagate interruption DaisyChain.
       if IntRequest = '1' then
            Assign_IRQ_X(IntLevel_X, '1', IRQ_N);                            
            Next_State_Int <= WaitIACK;  
       else                
            Assign_IRQ_X(IntLevel_X, '0', IRQ_N);                                      
            Next_State_Int <= WaitIntRequest; 
       end if;  
       IACK_OUT_N <= IACK_IN_N;
       DTACK_N <= '1';      
       DataOut <= (others =>'1');
       WaitDTACK_High :="00";
       Access_DTB <= '0';
    when WaitIACK => -- Waiting until IACK_IN_N = '0'
        if IACK_IN_N = '0' then
            if AS_N = '0'then
                if Address = IntLevel_X then
                    if DS0_N = '0' then
                       if SelectBits = Select16 then
                            if DS1_N = '0' then  -- Gate Interruption Daisy Chain. The IACK_IN_N corresponds to this interrupt.                               
                                if BusRelease = ROAK then
                                    Assign_IRQ_X(IntLevel_X, '0', IRQ_N); --Deactivate IRQx                                    
                                else
                                    Assign_IRQ_X(IntLevel_X, '1', IRQ_N); -- IRQx keeps activate                                    
                                end if;
                                IACK_OUT_N <= '1';
                                DTACK_N <= '1';                                
                                DataOut <= StatusID(X_Integer);
                                Next_State_Int <= GatedDaisyChain1;
                                Access_DTB <= '1';
                            else -- Wait until DS1_N = '0';
                                Assign_IRQ_X(IntLevel_X, '1', IRQ_N); 
                                IACK_OUT_N <= '1';  --Gated the daisy chain until the module examines if the IACK_IN_N corresponds to this module or
                                    -- corresponds to other module. 
                                    -- This sentences are necessary for the situation in which AS_N='0'; DS0_N='0' and DS1_N =0 arrives 
                                    -- after IACK_IN_N ='0'. 
                                DTACK_N <= '1';                
                                DataOut <= (others =>'1');
                                Next_State_Int <= WaitIACK; 
                                Access_DTB <= '0';
                            end if;                       
                       else -- 8 bits
                            if DS1_N = '0' then -- Propagate interruption DaisyChain. (The IACK_IN_N does not correspond to this interrupter)
                            -- waits for the IACK_IN_N that correspond to this interrupter. To prevent the case
                            -- in which IACK_IN_N keeps the '0' value when it has finished the last
                            --  interruption in process. (IACK_IN_N keeps '0' between two or more consecutive interruption request.
                                Assign_IRQ_X(IntLevel_X, '1', IRQ_N);   
                                IACK_OUT_N <= '0';
                                DTACK_N <= '1';                                
                                DataOut <= (others =>'1');    
                                Next_State_Int <= WaitIACK;
                                Access_DTB <= '0';
                            else -- Gate Interruption Daisy Chain. The IACK_IN_N corresponds to this interrupt.
                               if BusRelease = ROAK then
                                    Assign_IRQ_X(IntLevel_X, '0', IRQ_N); --Deactivate IRQx                                    
                                else
                                    Assign_IRQ_X(IntLevel_X, '1', IRQ_N); -- IRQx keeps activate                                    
                                end if;
                                IACK_OUT_N <= '1';
                                DTACK_N <= '1';                                
                                DataOut <= StatusID(X_Integer);
                                Next_State_Int <= GatedDaisyChain1;
                                Access_DTB <= '1';
                            end if;                               
                       end if; 
                    else -- Wait until DS0_N = '0';
                        Assign_IRQ_X(IntLevel_X, '1', IRQ_N); 
                        IACK_OUT_N <= '1';  --Gated the daisy chain until the module examines if the IACK_IN_N corresponds to this module or
                                    -- corresponds to other module. 
                                    -- This sentences are necessary for the situation in which AS_N='0'; DS0_N='0' and DS1_N =0 arrives 
                                    -- after IACK_IN_N ='0'. 
                        DTACK_N <= '1';                
                        DataOut <= (others =>'1');
                        Next_State_Int <= WaitIACK; 
                        Access_DTB <= '0';
                    end if;
                else  -- if Address = IntLevel_X then
                      -- Propagate interruption DaisyChain. (The IACK_IN_N does not correspond to this interrupter)
                      -- waits for the IACK_IN_N that correspond to this interrupter. To prevent the case
                      -- in which IACK_IN_N keeps the '0' value when it has finished the last
                      --  interruption in process. (IACK_IN_N keeps '0' between two or more consecutive interruption request.
                    Assign_IRQ_X(IntLevel_X, '1', IRQ_N);  
                    IACK_OUT_N <= '0';
                    DTACK_N <= '1';
                    DataOut <= (others =>'1');    
                    Next_State_Int <= WaitIACK;  
                    Access_DTB <= '0';         
                end if;           
            else -- Wait until AS_N = '0';
                Assign_IRQ_X(IntLevel_X, '1', IRQ_N); 
                IACK_OUT_N <= '1';  --Gated the daisy chain until the module examines if the IACK_IN_N corresponds to this module or
                                    -- corresponds to other module. 
                                    -- This sentences are necessary for the situation in which AS_N='0'; DS0_N='0' and DS1_N =0 arrives 
                                    -- after IACK_IN_N ='0'. 
                DTACK_N <= '1';                
                DataOut <= (others =>'1');
                Next_State_Int <= WaitIACK; 
                Access_DTB <= '0';            
            end if; 
        else -- Waiting until IACK_IN_N = '0'
            Assign_IRQ_X(IntLevel_X, '1', IRQ_N); 
            IACK_OUT_N <= IACK_IN_N; --IACK_IN_N= '1'
            DTACK_N <= '1';            
            DataOut <= (others =>'1');
            Next_State_Int <= WaitIACK;  
            Access_DTB <= '0';
        end if;
        WaitDTACK_High :="00";
   
    when GatedDaisyChain1 => -- When IACK_N = '0'  and conditions 5a, 5b and 5c are met.
        -- waits 2 TCLKs before activates DTACK
        if BusRelease = ROAK then
            Assign_IRQ_X(IntLevel_X, '0', IRQ_N); --Deactivate IRQx
         else
            Assign_IRQ_X(IntLevel_X, '1', IRQ_N); -- IRQx keeps activate    
         end if;
         IACK_OUT_N <= '1';
         DTACK_N <= '1';         
         DataOut <= StatusID(X_Integer);
         Next_State_Int <= GatedDaisyChain2;
         WaitDTACK_High :="00"; 
         Access_DTB <= '1';
          
    when GatedDaisyChain2 =>  -- Finished waiting 30 ns before activate DTACK
        if BusRelease = ROAK then
            Assign_IRQ_X(IntLevel_X, '0', IRQ_N); --Deactivate IRQx         
         else
            Assign_IRQ_X(IntLevel_X, '1', IRQ_N); -- IRQx keeps activate                
         end if;
         IACK_OUT_N <= '1';
         DTACK_N <= '0';         
         DataOut <= StatusID(X_Integer);
         Next_State_Int <= WaitDeactivateRORA1;
         WaitDTACK_High :="00";
         Access_DTB <= '1';

   when WaitDeactivateRORA1 =>  -- Waits until the rising edge of DS0_N before the register access cycle, (condition 6a):
        if BusRelease = ROAK then
            Assign_IRQ_X(IntLevel_X, '0', IRQ_N); --Deactivate IRQx         
         else
            Assign_IRQ_X(IntLevel_X, '1', IRQ_N); -- IRQx keeps activate                
         end if;

         if DS0_N = '1' then
            Next_State_Int <= WaitDeactivateRORA2; 
            DTACK_N <= '1';
            DataOut <= (others=>'0'); 
            IACK_OUT_N <= IACK_IN_N;    
         else
            Next_State_Int <= WaitDeactivateRORA1; 
            DTACK_N <= '0';
            DataOut <= StatusID(X_Integer);
            IACK_OUT_N <= '1';   
         end if;
         WaitDTACK_High :="00";
         Access_DTB <= '1';

   when WaitDeactivateRORA2 =>  -- Waits until the falling edge of DS0_N in the register access cycle, (condition 6b):        
         IACK_OUT_N <= '1';
         if WaitDTACK_High = "10" then -- for keeping DTACK high 3 TCLKs after DS0_N goes high.
                                        -- waits 60 ns before setting DTACK and Data to '1'
            DTACK_N <= '1'; 
            DataOut <= (others =>'1');  
            if BusRelease = ROAK then
                Next_State_Int <= WaitIntRequest; 
                Assign_IRQ_X(IntLevel_X, '0', IRQ_N);
                WaitDTACK_High := "00";
            else
                if DS0_N = '0' then
                    Next_State_Int <= WaitIntRequest; 
                    Assign_IRQ_X(IntLevel_X, '0', IRQ_N); --Deactivate IRQx
                    WaitDTACK_High := "00";
                else
                    Next_State_Int <= WaitDeactivateRORA2; 
                    Assign_IRQ_X(IntLevel_X, '1', IRQ_N); -- IRQx keeps activate   
                    WaitDTACK_High := "10";
                end if;                
            end if;
            Access_DTB <= '0';
         else
            DTACK_N <= '1';
            DataOut <= (others=>'0'); 
            Next_State_Int <= WaitDeactivateRORA2;
            WaitDTACK_High := WaitDTACK_High +"01"; 
            if BusRelease = ROAK then
                Assign_IRQ_X(IntLevel_X, '0', IRQ_N); --Deactivate IRQx
            else
                Assign_IRQ_X(IntLevel_X, '1', IRQ_N); -- IRQx keeps activate
            end if;
            Access_DTB <= '1';
         end if;
       
    when others => -- There is an error. The code must never reach this sentence.
        Reset_Interrupter (IACK_OUT_N, DTACK_N, DataOut, Next_State_Int, Access_DTB);
        IRQ_N <= (others =>'1');
        WaitDTACK_High :="00";
    end case;
end if;
end Process Interrupt;

end behavioral;