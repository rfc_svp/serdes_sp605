--  Copyright 2008 Actel Corporation.  All rights reserved.
-- ANY USE OR REDISTRIBUTION IN PART OR IN WHOLE MUST BE HANDLED IN
-- ACCORDANCE WITH THE ACTEL LICENSE AGREEMENT AND MUST BE APPROVED
--  Revision Information:
-- Jun09    Revision 4.1
-- SVN Revision Information:
-- SVN $Revision: 8508 $
library Ieee;
use Ieee.std_logIC_1164.all;
use ieee.sTD_LOGIC_ARITh.all;
use ieee.STD_LOgic_unsigned.all;
entity CUARTiil is
generic (tx_fifo: integER := 0); port (CLK: in STD_LOGIC;
CUARTi: in std_logic;
RESET_n: in std_logic;
CUARTO0L: in std_logic;
CUARTl0l: in std_logic_vECTOR(7 downto 0);
CUARTI0L: in stD_LOGIC_VECTOr(7 downto 0);
CUARTO1L: in STD_LOGIc;
CUARTL1L: in stD_LOGIC;
BIT8: in std_LOGIC;
PARIty_en: in STD_LOGIC;
odd_n_even: in STD_LOgic;
txrdy: out STD_LOGic;
Tx: out std_logiC;
CUARTI1l: out std_loGIC);
end entity CUARTIIL;

architecture CUARTl0 of CUARTiil is

constant CUARTI10I: INTEGER := 0;

constant CUARToo1i: integer := 1;

constant CUARTlo1i: integer := 2;

constant CUARTio1I: integer := 3;

constant CUARTOL1I: INTEGER := 4;

constant CUARTLl1i: INTEGEr := 5;

constant CUARTil1i: Integer := 6;

signal CUARTOI1I: INTEGER;

signal CUARTlI1I: STD_LOGIC;

signal CUARTII1i: std_logic_VECTOR(7 downto 0);

signal CUARTo01i: std_logic_veCTOR(3 downto 0);

signal CUARTl01i: STD_logic;

signal CUARTi01i: STD_LOGIC;

signal CUARTO11I: std_lOGIC;

signal CUARTl11i: STD_LOGIC;

signal CUARTi11I: Std_logic;

signal CUARTooo0: STD_logic;

signal CUARTloo0: STD_LOGIC;

function to_INTEGER(val: std_logic_vECTOR)
return INTEGER is
constant CUARTLL1L: std_logiC_VECTOR(val'high-val'LOW downto 0) := VAL;
variable CUARTil1l: INTEGER := 0;
begin
for CUARToi1l in CUARTll1l'range
loop
if (CUARTLL1L(CUARToi1L) = '1') then
CUARTIL1L := CUARTIL1l+(2**CUARToi1l);
end if;
end loop;
return (CUARTIl1l);
end TO_INTEGER;

begin
txrdY <= CUARTi11I;
tx <= CUARTOOO0;
CUARTi1l <= CUARTloo0;
CUARTIOO0:
process (clk,RESET_N)
begin
if (not RESET_N = '1') then
CUARTLI1I <= '1';
elsif (CLK'EVENT and CLK = '1') then
if (tX_FIFO = 2#0#) then
if (CUARTi = '1') then
if (CUARToi1i = CUARTOo1i) then
CUARTli1i <= '1';
end if;
end if;
if (CUARTo0l = '1') then
CUARTli1i <= '0';
end if;
else
CUARTlI1I <= not CUARTl1l;
end if;
end if;
end process CUARTioo0;
CUARTolo0:
process (clk,RESET_n)
begin
if (not reset_n = '1') then
CUARToi1i <= CUARTi10i;
CUARTii1i <= "00000000";
CUARTI01I <= '1';
elsif (clk'event and CLk = '1') then
if (CUARTI = '1') then
CUARTi01i <= '1';
case CUARTOI1i is
when CUARTI10I =>
if (tx_FIFO = 2#0#) then
if (not CUARTli1i = '1') then
CUARToi1i <= CUARToo1i;
else
CUARToi1i <= CUARTi10i;
end if;
else
if (CUARTO1L = '0') then
CUARTi01i <= '0';
CUARTOI1I <= CUARTil1i;
else
CUARTOI1I <= CUARTI10I;
CUARTi01i <= '1';
end if;
end if;
when CUARToo1I =>
if (TX_FIFO = 2#0#) then
CUARTII1I <= CUARTL0L;
else
CUARTii1i <= CUARTi0l;
end if;
CUARTOI1i <= CUARTLO1I;
when CUARTLO1i =>
CUARTOI1i <= CUARTIO1I;
when CUARTio1i =>
if (bit8 = '1') then
if (CUARTo01I = "0111") then
if (parity_en = '1') then
CUARToi1i <= CUARTol1i;
else
CUARTOI1i <= CUARTLl1i;
end if;
else
CUARTOI1I <= CUARTIO1I;
end if;
else
if (CUARTO01i = "0110") then
if (parity_en = '1') then
CUARTOI1I <= CUARTOL1I;
else
CUARToi1i <= CUARTll1I;
end if;
else
CUARToi1i <= CUARTio1i;
end if;
end if;
when CUARTol1i =>
CUARTOi1i <= CUARTLL1I;
when CUARTll1i =>
CUARTOI1i <= CUARTi10i;
when CUARTil1i =>
CUARTOi1i <= CUARToo1i;
when others =>
CUARToi1i <= CUARTi10i;
end case;
end if;
end if;
end process CUARTOlo0;
CUARTllO0:
process (CLK,RESET_N)
begin
if (not reset_n = '1') then
CUARTloo0 <= '1';
CUARTo11i <= '1';
elsif (CLK'EVENT and CLK = '1') then
CUARTLOO0 <= '1';
CUARTO11I <= CUARTI01i;
if (CUARTl11I = '0') then
CUARTloo0 <= '0';
end if;
end if;
end process CUARTlLO0;
CUARTl11i <= not CUARTo11i or CUARTi01i;
CUARTilo0:
process (clk,RESET_N)
begin
if (not reset_n = '1') then
CUARTO01I <= "0000";
elsif (CLk'EVENT and clk = '1') then
if (CUARTi = '1') then
if (CUARTOI1I /= CUARTio1i) then
CUARTo01i <= "0000";
else
CUARTo01i <= CUARTo01i+"0001";
end if;
end if;
end if;
end process CUARTilo0;
CUARTOIo0:
process (Clk,RESET_N)
begin
if (not RESEt_n = '1') then
CUARTooo0 <= '1';
elsif (CLK'eveNT and clk = '1') then
if (CUARTI = '1') then
case CUARTOI1I is
when CUARTi10I =>
CUARTooo0 <= '1';
when CUARToo1i =>
CUARTOOO0 <= '1';
when CUARTlo1I =>
CUARTooo0 <= '0';
when CUARTIO1I =>
CUARTOOO0 <= CUARTII1i(to_integer(CUARTo01i));
when CUARTol1I =>
CUARTooo0 <= odd_n_even xor CUARTl01i;
when CUARTll1i =>
CUARTooo0 <= '1';
when others =>
CUARTOOO0 <= '1';
end case;
end if;
end if;
end process CUARToio0;
CUARTlIO0:
process (clk,RESET_n)
begin
if (not reset_n = '1') then
CUARTL01I <= '0';
elsif (clk'EVENT and clk = '1') then
if ((CUARTi and PARITY_EN) = '1') then
if (CUARTOi1i = CUARTio1I) then
CUARTL01i <= CUARTl01i xor CUARTii1i(to_iNTEGER(CUARTO01I));
else
CUARTL01i <= CUARTL01i;
end if;
end if;
if (CUARToi1i = CUARTLL1I) then
CUARTl01I <= '0';
end if;
end if;
end process CUARTLIO0;
CUARTi11i <= CUARTLi1i;
end architecture CUARTL0;
