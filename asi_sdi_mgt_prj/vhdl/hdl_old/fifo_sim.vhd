library IEEE;
use IEEE.STD_LOGIC_1164.all;

package log2_pkg is

-- base-2 logarithm
function log2 (Taps : integer) return integer;

end log2_pkg;

package body log2_pkg is

function log2 (Taps : integer) return integer is
   variable i: integer;
begin
   
   if Taps>1 then
	   for i in 0 to Taps - 1 loop
		  if 2**i >= Taps then
			return i;
		  end if;
	   end loop;
   else
		return 1;
   end if;   
   
end log2;

end log2_pkg;

---------------------------------------------------------------------------------------------------
-- FIFO simulation model
---------------------------------------------------------------------------------------------------
LIbrary IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

use work.log2_pkg.all;

Entity fifo_sim is
	generic(Bit_Wide:natural:=8;
			Deep:natural:=16);	
	Port ( 
		    Reset_n	 	: in std_logic;
		    WriteEnable  : in std_logic;
		    ReadEnable   : in std_logic;
		    DataIn       : in std_logic_vector(Bit_Wide-1 downto 0);
		    DataOut      : out std_logic_vector(Bit_Wide-1 downto 0);
   		    FifoEmpty    : out std_logic;
		    FifoFull     : out std_logic
	    );
END fifo_sim;-- entity declarations ends

Architecture A_fifo of fifo_sim is

Type Mem is array (Deep-1 downto 0) of std_logic_vector(Bit_Wide-1 downto 0);
Signal Memory : Mem;

Signal Addr_s,ReadPointer,WritePointer	: std_logic_vector(log2(Deep)-1 downto 0);
Signal Counter,Counter_z				: std_logic_vector(log2(Deep) downto 0);

Signal WriteEnable_s : std_logic;
Signal FifoFull_s    : std_logic;
Signal FifoEmpty_s   : std_logic;

Begin

	Counter_z<=(others=>'0');
			 
	mem_Process : Process(WriteEnable_s)
	Begin
		if (WriteEnable_s'event and WriteEnable_s = '1') then
			Memory(Conv_Integer(Addr_s)) <= Datain;
		end if;
	end process; --	Write Process Ends

	Dataout <= Memory(Conv_Integer(Addr_s));			 

	ReadWriteFifoOut   : Process(WriteEnable,ReadEnable,Reset_n)
	Begin
		IF ( Reset_n = '0') then
			ReadPointer  <= (others=>'0');
			WritePointer <= (others=>'0');
			Counter  <= (others=>'0');
		ELSIF(WriteEnable'event and WriteEnable = '1') then
			IF ( FifoFull_s = '0' and ReadEnable = '0') then
				WritePointer <= WritePointer + 1;
				Counter  <= Counter + 1;
			END IF;
		ELSIF(ReadEnable'event and ReadEnable = '1') then
			IF ( FifoEmpty_s = '0' and WriteEnable = '0') then
				ReadPointer  <= ReadPointer + 1;
				Counter  <= Counter - 1;
			END IF;
		END IF;
	END process;-- ReadWriteFifo Process ends
-----------------------------------------------------------
--  Combinatorial Logic
-----------------------------------------------------------
	FifoEmpty_s <= '1' when ( Counter = Counter_z) else
							   '0';
	FifoFull_s  <= Counter(Counter'high);

	FifoFull  <= FifoFull_s;
	FifoEmpty <= FifoEmpty_s;

	WriteEnable_s <= '1' when ( WriteEnable = '1' and FifoFull_s = '0') else
									 '0';
	Addr_s  <= WritePointer  when ( WriteEnable = '1') else
						 ReadPointer;
------------------------------------------------------------
END A_fifo;--Architecture Ends