library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

---library ACT3;
---use ACT3.COMPONENTS.all;

entity sys_ctl_vme is
  port (
	-- VME:
    bgin_b     : in	   std_logic_vector(3 downto 0);  -- VMEbus Bus Grant Input
    bgout_b    : out   std_logic_vector(3 downto 0);  -- VMEbus Bus Grant Output
    br_b       : inout std_logic_vector(3 downto 0);  -- VMEbus Request
    -- iack_b     : inout std_logic;       			  -- VMEbus Interrupt Acknowledge
    -- iackin_b   : in    std_logic;       -- VMEbus Interrupt Acknowledge Input
    -- iackout_b  : out   std_logic;       -- VMEbus Interrupt Acknowledge Output
    -- irq_b      : inout std_logic_vector(7 downto 1);  -- VMEbus Int Request
    bclr_b     : inout std_logic;       -- VMEbus Clear
	
	n_RESET,sys_clk		: in        std_logic;
	
	ctl_1     : in std_logic_vector(31 downto 0);  -- ctl1 Input
	ctl_2     : in std_logic_vector(31 downto 0)   -- ctl2 Input
	      
            );
end sys_ctl_vme;

------------------------------------------------------------------------
architecture RTL of sys_ctl_vme is

	
   signal gnd_vec : std_logic_vector     (31   downto 0);
   signal vcc_vec : std_logic_vector     (31   downto 0);
   signal gnd      : std_logic            := '0';    
   signal vcc      : std_logic            := '1'; 
   signal rst      : std_logic            := '1';
   
   
	
component vme_arbiter is
  port (
    br_b_in     : in  std_ulogic_vector(3 downto 0);
    int_br_b_in : in  std_ulogic_vector(3 downto 0);
    bgout_b     : out std_ulogic_vector(3 downto 0);
    bclr_b_out  : out std_ulogic;
    bclr_b_oen  : out std_ulogic;
    bbsy_b_in   : in  std_ulogic;
    bbsy_set    : out std_ulogic;
    bbsy_rst    : out std_ulogic;
    scon_b      : in  std_ulogic;
    arb_fail    : out std_ulogic;
    arcr        : in  std_ulogic_vector(7 downto 0);
    clk         : in  std_ulogic;
    rst         : in  std_ulogic);
end component;
   
   


begin


	gnd_vec	<= X"00000000";
	vcc_vec	<= X"00000000";
	gnd    	<= '0';
	vcc    	<= '1';
	rst    	<= not n_RESET;






end RTL;

