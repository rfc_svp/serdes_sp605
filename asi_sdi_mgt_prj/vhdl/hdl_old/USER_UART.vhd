library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

use work.user_serial_pkg.all;

entity USER_UART is 
	generic (
		ClkFreq				: INTEGER;		-- clock freq (Hz)
		BaudRate			: INTEGER;		-- bps
		--BAUD_VAL_FRACTION 	: std_logic_vector(2 downto 0) := "000";
		bit8				: std_logic;	-- byte = '1' 
		parity_en			: std_logic;	-- enable = '1' 
		odd_n_even			: std_logic;	-- odd = '1' even = '0'
		-- sizes
		ADDR_NIBBLES		: natural := 8;
		DATA_NIBBLES 		: natural := 8;
		-- protocol config
		command_echo		: boolean := true;
		timeout_mult		: natural := 2
	);
	port (
		n_RESET,sys_clk		: in        std_logic;
		enable				: in        std_logic;
		-- i/f CORE IP
		RXDATA         		: in        std_logic;
		TXDATA         		: out       std_logic;
		-- i/f MASTER BUS
		ADDRESS_UART   		:   out		std_logic_vector ((4*ADDR_NIBBLES)-1 downto 0); -- Address bus
		RD_DATA_BUS_UART    :   in		std_logic_vector ((4*DATA_NIBBLES)-1 downto 0); -- Input data bus
		WD_DATA_BUS_UART  	:   out		std_logic_vector ((4*DATA_NIBBLES)-1 downto 0); -- Output data bus
		RD_nWD_EN_UART		:   out		std_logic;                      -- Read(active high)/write(active low) enable
		nCS_UART			:	out		std_logic;						-- Operation request(active low)
		ACCESS_VALID_UART	:	in		ACCESS_VALID_UART_type;			-- Access valid acknowledge
		WAIT_UP_ADC_DATA	:	in		std_logic;						-- Ready data in the ADC
		--	error flags
		UART_BIT_I_S		:	out	std_logic;						-- module in invalid state
		UART_BIT_W_D		:	out	std_logic;						-- module is STOPED
		-- debug outputs
		RXRDY_o 			:	out	std_logic;
		TXRDY_o 			:	out	std_logic;
		-- trigger port
		TRIGGER_IN    		:   in std_logic_vector((4*DATA_NIBBLES)-1 downto 0);
		TRIGGER_IN_MASK		:   in std_logic_vector((4*DATA_NIBBLES)-1 downto 0)	-- if 1 rising, if 0 falling
	);
end;



architecture bhu of USER_UART is

-- UART baud rate
constant baud_val: std_logic_vector(12 downto 0) := conv_std_logic_vector((ClkFreq/(16*BaudRate))-1,13);

-- timeouts
constant TIMEOUT : natural := natural(((11/BaudRate)+1)*ClkFreq);

-------------------------------------------------
-- internal signals
-------------------------------------------------

signal    OEN_S                   : std_logic := '1';
signal    WEN_S                   : std_logic := '1';
signal    CSN_S                   : std_logic := '1'; 
signal    RXRDY_S                 : std_logic := '0'; 
signal    TXRDY_S                 : std_logic := '0'; 
signal    FRAMING_ERR_S           : std_logic := '0'; 
signal    PARITY_ERR_S            : std_logic := '0'; 
signal    OVERFLOW_S              : std_logic := '0'; 
signal    FRAMING_ERR_S1          : std_logic := '0'; 
signal    PARITY_ERR_S1           : std_logic := '0'; 
signal    OVERFLOW_S1             : std_logic := '0'; 
signal    DATA_FROM_UART         : std_logic_vector(7 downto 0);
signal    DATA_TO_UART          : std_logic_vector(7 downto 0);

type STADOS is (WAIT_OPERATION, WAIT_OPERATION_1, DECODE_OPERATION, SELECT_OP, CLEAR_TRIGGER,
				--CLEAR_OEN_1, CLEAR_OEN_2, CLEAR_OEN_3, CLEAR_OEN_4, 
				CLEAR_OEN_TO_READ_ADDR,
				--CLEAR_OEN_5,
				CLEAR_OEN_TO_READ_DATA_TO_WRITE,
				--CLEAR_OEN_6, CLEAR_OEN_7, CLEAR_OEN_8, CLEAR_OEN_9,
				CLEAR_OEN_TO_READ_DATA_FROM_WRITE,
				CLEAR_OEN_FROM_READ_DATA_TO_WRITE,
				CLEAR_OEN_10, CLEAR_OEN_11,
				--READ_ADDR_1, READ_ADDR_2, READ_ADDR_3, READ_ADDR_4,
				READ_ADDR,
				READ_COMA, READ_QUESTION, READ_CR, READ_LF,
				--READ_DATA_TO_WRITE_1, READ_DATA_TO_WRITE_2, READ_DATA_TO_WRITE_3, READ_DATA_TO_WRITE_4,
				READ_DATA_TO_WRITE,
				WAIT_RESPONSE, WAIT_VALIDATION_R_DELAY, WAIT_VALIDATION_W_DELAY, WAIT_VALIDATION_W, WAIT_VALIDATION_R,
				SEND_DATA_RD, SEND_DATA_RD_1, SEND_FAIL_INFO, SEND_FAIL_INFO_1,
				INVALID_DATA, INVALID_DATA_1, VALID_DATA, VALID_DATA_1, VALID_TRIGGER,
				SEND_CR_CHAR, SEND_CR_CHAR_1, SEND_LF_CHAR, SEND_LF_CHAR_1,
				SEND_ECHO,SEND_ECHO_1,SEND_ECHO_2,SEND_ECHO_3);  

signal STADO           :   STADOS; 


--signal ADDRESS_UART_R			:	std_logic_vector (15 downto 0); 
--signal RD_DATA_BUS_UART_R		:	std_logic_vector (15 downto 0); 
--signal WD_DATA_BUS_UART_R		:	std_logic_vector (15 downto 0); 

--signal COUNT_SEND_DATA			:	std_logic_vector (4 downto 0); 



-- time out signal & counter
signal	TIME_OUT_UART,TIME_OUT_MX_UART  :   integer range 0 to timeout_mult*TIMEOUT;
signal	RST_TIME_OUT_UART				: 	std_logic;

-- Watch dog equivalent
signal	WATCH_DOG_TIMER_UART	:	integer range 0 to TIMEOUT;
signal	RELOAD_WD				:	std_logic := '0';


-- READ / WRITE FLAG
--signal	READ_FLAG		:	std_logic := '0';
--signal	WRITE_FLAG		:	std_logic := '0';
--signal	VALID_FLAG		:	std_logic := '0';
--signal	INVALID_FLAG	:	std_logic := '0';

-- READ / WRITE STATE
type TRANSFER_STATE_TYPE is (IDLE_ST,READ_ST,WRITE_ST);  
signal TRANSFER_STATE : TRANSFER_STATE_TYPE;

signal	FAILED_UART		:	std_logic := '0';
signal	NO_CLEAN_OEN	:	std_logic := '0';
signal	RESET_UART_FAILURE	:	std_logic := '0';

signal RESET_N : std_logic;

signal READ_ADDR_NIBBLE_CNT	: integer range 0 to ADDR_NIBBLES;
signal ADDRESS_UART_R		: nibble_vector (ADDR_NIBBLES downto 1); 

signal READ_DATA_NIBBLE_CNT		: integer range 0 to DATA_NIBBLES;
signal WD_DATA_BUS_UART_R		: nibble_vector (DATA_NIBBLES downto 1); 
signal RD_DATA_BUS_UART_R		: nibble_vector (DATA_NIBBLES downto 1);

--signal RD_DATA_BUS_UART_R_MSB	: std_logic;

TYPE data_width_vector IS ARRAY (NATURAL RANGE <>) OF std_logic_vector((4*DATA_NIBBLES)-1 downto 0);
constant TRIGGER_IN_EDGE_ZEROS			: std_logic_vector((4*DATA_NIBBLES)-1 downto 0) := (others=>'0');
signal TRIGGER_IN_R						: data_width_vector (1 downto 0);
signal TRIGGER_IN_SRE,TRIGGER_IN_SFE	: std_logic_vector((4*DATA_NIBBLES)-1 downto 0);
signal TRIGGER_IN_SEND,TRIGGER_IN_EDGE	: std_logic_vector((4*DATA_NIBBLES)-1 downto 0);
signal TRIGGER							: std_logic;

constant READ_CMD_LENGTH	: natural := 1+ADDR_NIBBLES;	-- R + addr
constant WRITE_CMD_LENGHT	: natural := 1+ADDR_NIBBLES+1+DATA_NIBBLES;	-- W + addr + , + data
TYPE DATA_UART_vector IS ARRAY (NATURAL RANGE <>) OF std_logic_vector(7 downto 0);
signal COMMAND_TEMP						: DATA_UART_vector(WRITE_CMD_LENGHT downto 0);
signal CMD_TEMP_WAVE					: std_logic_vector((8*(COMMAND_TEMP'high+1)-1) downto 0);
signal CMD_TEMP_W_CNT,CMD_TEMP_R_CNT	: integer range COMMAND_TEMP'low to COMMAND_TEMP'high;
signal CLEAR_CMD_TEMP,GET_CMD,SHIFT_CMD	: std_logic;

begin

RXRDY_o <= RXRDY_S;
TXRDY_o <= TXRDY_S;

-------------------------------------------------
-- component instantiations
-------------------------------------------------

UART1 : entity work.USERUART 
        port map (
            BAUD_VAL    =>	baud_val,
			BIT8        =>	bit8,
			CLK         =>	sys_clk,
			CSN         =>	CSN_S,
			DATA_IN     =>	DATA_TO_UART,
			DATA_OUT    =>	DATA_FROM_UART,
			ODD_N_EVEN  => 	odd_n_even,
			OEN         =>	OEN_S,
			OVERFLOW    =>	OVERFLOW_S1,
			PARITY_EN   => 	parity_en,
			PARITY_ERR  =>	PARITY_ERR_S1,
			RESET_N     =>	RESET_N, --n_RESET, 
			RX          =>	RXDATA,
			RXRDY       =>	RXRDY_S,
			TX          =>	TXDATA,
			TXRDY       =>	TXRDY_S,
			WEN         =>	WEN_S,
			FRAMING_ERR =>	FRAMING_ERR_S1
			--BAUD_VAL_FRACTION	=> BAUD_VAL_FRACTION
);

CSN_S				<= '0';
RESET_N				<= n_RESET and ENABLE;

-------------------failure register process--------------------------------
process(sys_clk, n_RESET)   

Begin
if ( n_RESET= '0') then  
	FAILED_UART		<= '0';
	FRAMING_ERR_S	<= '0';
	PARITY_ERR_S 	<= '0';
	OVERFLOW_S   	<= '0';
	
elsif (sys_clk'event and sys_clk='1') then
	if ((FRAMING_ERR_S1 = '1') or (PARITY_ERR_S1 = '1') or (OVERFLOW_S1 = '1') ) then
		FAILED_UART		<=	'1' ;
		FRAMING_ERR_S	<=	FRAMING_ERR_S1 ;
		PARITY_ERR_S 	<=	PARITY_ERR_S1 ;
		OVERFLOW_S   	<=	OVERFLOW_S1   ;
		
	elsif (RESET_UART_FAILURE = '0') then
		FAILED_UART		<= '0';
		FRAMING_ERR_S	<= '0';
		PARITY_ERR_S 	<= '0';
		OVERFLOW_S   	<= '0';
	end if;
end if;
end process;


process (sys_clk, n_RESET)                                   
begin




if ( n_RESET= '0') then     
	-- DATA_FROM_UART		<= (others => '0');
    DATA_TO_UART  		<= (others => '0');
	UART_BIT_I_S		<= '0';
	RELOAD_WD			<= '0';
	WEN_S 				<= '1';
	OEN_S				<= '1';
	--READ_FLAG			<= '0';
	--WRITE_FLAG      	<= '0';
	--VALID_FLAG			<= '0';
	--INVALID_FLAG    	<= '1';
	TRANSFER_STATE		<= IDLE_ST;
	nCS_UART 			<= '1';
	ADDRESS_UART_R		<= (others=>(others => '0'));		
	ADDRESS_UART		<= (others => '0');	
	RD_DATA_BUS_UART_R	<= (others=>(others => '0'));	
	WD_DATA_BUS_UART_R	<= (others=>(others => '0'));	
	WD_DATA_BUS_UART	<= (others => '0');	
	RD_nWD_EN_UART 		<= '1';
	NO_CLEAN_OEN 		<= '0';
	--COUNT_SEND_DATA		<= (others => '0');	
	RESET_UART_FAILURE	<= '0';
	STADO <= WAIT_OPERATION;
	CLEAR_CMD_TEMP	<= '0';
	GET_CMD 		<= '0';
	SHIFT_CMD 		<= '0';
elsif (sys_clk'event and sys_clk='1') then
	CLEAR_CMD_TEMP	<= '0';
	GET_CMD 		<= '0';
	SHIFT_CMD 		<= '0';
    case STADO is
		when WAIT_OPERATION =>
			WEN_S 				<= '1';
			OEN_S				<= '1';
			--READ_FLAG			<= '0';
			--WRITE_FLAG  		<= '0';
			TRANSFER_STATE		<= IDLE_ST;
			RELOAD_WD			<= '0';
			nCS_UART 			<= '1';
			--VALID_FLAG			<= '0';
			--INVALID_FLAG		<= '1';
			RD_nWD_EN_UART		<= '1';
			NO_CLEAN_OEN 		<= '0';
			RST_TIME_OUT_UART	<= '1';
			ADDRESS_UART_R		<= (others=>(others => '0'));
			ADDRESS_UART		<= (others => '0');	
			RD_DATA_BUS_UART_R	<= (others=>(others => '0'));	
			WD_DATA_BUS_UART_R	<= (others=>(others => '0'));		
			WD_DATA_BUS_UART	<= (others => '0');	
			--COUNT_SEND_DATA		<= (others => '0');		
			RESET_UART_FAILURE	<= '0';
			if (( RXRDY_S = '1') and (WAIT_UP_ADC_DATA = '1')) then
				STADO <= DECODE_OPERATION;
				CLEAR_CMD_TEMP <= '1';				
			elsif (TRIGGER='1') then	
				STADO <= SEND_DATA_RD;
				for i in 1 to DATA_NIBBLES loop
					RD_DATA_BUS_UART_R(i) <= TRIGGER_IN_SEND(((DATA_NIBBLES-(i-1))*4)-1 downto (DATA_NIBBLES-i)*4);
				end loop;				
			else
				STADO <= WAIT_OPERATION_1;
			end if;
			
		when WAIT_OPERATION_1 =>
			RELOAD_WD	<='1';
			if (FAILED_UART = '1')then
				STADO <= INVALID_DATA;
			else
				STADO <= WAIT_OPERATION;		
			end if;
			
		when DECODE_OPERATION =>
			RESET_UART_FAILURE	<= '1';
			RELOAD_WD	<='1';
			GET_CMD		<= '1';
			if (((DATA_FROM_UART = useruart_ascii(uR)) or (DATA_FROM_UART = useruart_ascii(lR))) and (FAILED_UART = '0') )then
					--READ_FLAG	<= '1';
					--WRITE_FLAG  <= '0';
					TRANSFER_STATE <= READ_ST;
					OEN_S <= '0';
--					STADO <= CLEAR_OEN_1;
					STADO <= CLEAR_OEN_TO_READ_ADDR;
			elsif (((DATA_FROM_UART = useruart_ascii(uW)) or (DATA_FROM_UART = useruart_ascii(lW))) and (FAILED_UART = '0') ) then
					--READ_FLAG	<= '0';
					--WRITE_FLAG  <= '1';
					TRANSFER_STATE <= WRITE_ST;
					OEN_S <= '0';
--					STADO <= CLEAR_OEN_1;
					STADO <= CLEAR_OEN_TO_READ_ADDR;
			else
				STADO <= INVALID_DATA;
			end if;
		
--		when CLEAR_OEN_1 =>
--			if ( RXRDY_S = '0')then
--				OEN_S <= '1';
--				RELOAD_WD	<='0';
--				STADO <= READ_ADDR_1;
--			elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--			else
--				STADO <= CLEAR_OEN_1;
--			
--			end if;

		when CLEAR_OEN_TO_READ_ADDR =>
			if ( RXRDY_S = '0')then
				OEN_S <= '1';
				RELOAD_WD	<='0';
--				STADO <= READ_ADDR_1;
				STADO <= READ_ADDR;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
				STADO <= CLEAR_OEN_TO_READ_ADDR;
			
			end if;
		
		--------------------------------------------------------
		-- Read Addres Operation -- 
		-- MSB_1--
--		when READ_ADDR_1 =>
--			RST_TIME_OUT_UART <= '0';
--			RELOAD_WD	<='1';
--			if ( RXRDY_S = '1') then
--				RST_TIME_OUT_UART <= '1';
--				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0') )  then
--					ADDRESS_UART_R(15 downto 12) <= DATA_FROM_UART(3 downto 0);
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_2;
--				elsif ( (((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
--						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF)))) and (FAILED_UART = '0')  )then
--					ADDRESS_UART_R(15 downto 12) <= DATA_FROM_UART(3 downto 0) + x"9";
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_2;
--				else
--					STADO 		<= INVALID_DATA;
--				end if;
--				
--			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
--				STADO 		<= INVALID_DATA;
--			else
--				STADO <= READ_ADDR_1;
--			end if;

		when READ_ADDR =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				GET_CMD <= '1';
				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0') )  then
					ADDRESS_UART_R(READ_ADDR_NIBBLE_CNT) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					if READ_ADDR_NIBBLE_CNT<ADDR_NIBBLES then
						STADO 		<= CLEAR_OEN_TO_READ_ADDR;
					else
						STADO 		<= SELECT_OP;
					end if;
				elsif ( (((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF)))) and (FAILED_UART = '0')  )then
					ADDRESS_UART_R(READ_ADDR_NIBBLE_CNT) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					if READ_ADDR_NIBBLE_CNT<ADDR_NIBBLES then
						STADO 		<= CLEAR_OEN_TO_READ_ADDR;
					else
						STADO 		<= SELECT_OP;
					end if;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_ADDR;
			end if;

		
--		when CLEAR_OEN_2 =>
--			if ( RXRDY_S = '0')then
--				RELOAD_WD	<='0';
--				OEN_S <= '1';
--				STADO <= READ_ADDR_2;
--			elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--			else
--				STADO <= CLEAR_OEN_2;
--			end if;
--			
--		-- MSB_2--
--		when READ_ADDR_2 =>
--			RST_TIME_OUT_UART <= '0';
--			RELOAD_WD	<='1';
--			if ( RXRDY_S = '1') then
--				RST_TIME_OUT_UART <= '1';
--				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0')) then
--					ADDRESS_UART_R(11 downto 8) <= DATA_FROM_UART(3 downto 0);
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_3;
--				elsif ( (((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
--						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF))) ) and (FAILED_UART = '0') )then
--					ADDRESS_UART_R(11 downto 8) <= DATA_FROM_UART(3 downto 0) + x"9";
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_3;
--				else
--					STADO 		<= INVALID_DATA;
--				end if;
--				
--			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
--				STADO 		<= INVALID_DATA;
--			else
--				STADO <= READ_ADDR_2;
--			end if;	
--		
--		when CLEAR_OEN_3 =>
--			if ( RXRDY_S = '0')then	
--				RELOAD_WD	<='0';
--				OEN_S <= '1';
--				STADO <= READ_ADDR_3;
--			elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--			else									
--				STADO <= CLEAR_OEN_3;               
--			end if;                                 
--		
--		-- MSB_3--
--		when READ_ADDR_3 =>
--			RST_TIME_OUT_UART <= '0';
--			RELOAD_WD	<='1';
--			if ( RXRDY_S = '1') then
--				RST_TIME_OUT_UART <= '1';
--				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0')) then
--					ADDRESS_UART_R(7 downto 4) <= DATA_FROM_UART(3 downto 0);
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_4;
--				elsif ((((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
--						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF)))) and (FAILED_UART = '0')) then
--					ADDRESS_UART_R(7 downto 4) <= DATA_FROM_UART(3 downto 0) + x"9";
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_4;
--				else
--					STADO 		<= INVALID_DATA;
--				end if;
--				
--			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
--				STADO 		<= INVALID_DATA;
--			else
--				STADO <= READ_ADDR_3;
--			end if;	
--		
--		when CLEAR_OEN_4 =>
--			if ( RXRDY_S = '0')then
--				RELOAD_WD	<='0';
--				OEN_S <= '1';
--				STADO <= READ_ADDR_4;
--			elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--		    else
--		    	STADO <= CLEAR_OEN_4;
--		    end if;
--		
--		-- MSB_4--
--		
--		when READ_ADDR_4 =>
--			RST_TIME_OUT_UART <= '0';
--			RELOAD_WD	<='1';
--			if ( RXRDY_S = '1') then
--				RST_TIME_OUT_UART <= '1';
--				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0')) then
--					ADDRESS_UART_R(3 downto 0) <= DATA_FROM_UART(3 downto 0);
--					OEN_S <= '0';
--					STADO 		<= SELECT_OP;
--				elsif ( (((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
--						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF)))) and (FAILED_UART = '0'))then
--					ADDRESS_UART_R(3 downto 0) <= DATA_FROM_UART(3 downto 0) + x"9";
--					OEN_S <= '0';
--					STADO 		<= SELECT_OP;
--				else
--					STADO 		<= INVALID_DATA;
--				end if;
--				
--			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
--				STADO 		<= INVALID_DATA;
--			else
--				STADO <= READ_ADDR_4;
--			end if;	
		
			
		when SELECT_OP =>
			RELOAD_WD	<='0';
			if ( RXRDY_S = '0')then											
				OEN_S <= '1';                           
				--if ( READ_FLAG = '1') then              
				if (TRANSFER_STATE = READ_ST) then
					STADO <= READ_QUESTION;   
				--elsif (WRITE_FLAG = '1') then           
				elsif (TRANSFER_STATE = WRITE_ST) then
					STADO <= READ_COMA;  		
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= SELECT_OP;
			end if;
			
		--------------------------------------------------------	
		-- Read coma mark --	
		when READ_COMA =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				GET_CMD	<= '1';						
				if (DATA_FROM_UART = useruart_ascii(COMA)) then
					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_5;
					STADO		<= CLEAR_OEN_TO_READ_DATA_TO_WRITE;
				else
					STADO 		<= INVALID_DATA;
				end if;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_COMA;
			end if;	
		
--		when CLEAR_OEN_5 =>													
--			if ( RXRDY_S = '0')then																		
--				RELOAD_WD	<='0';
--				OEN_S <= '1';														
--				STADO <= READ_DATA_TO_WRITE_1;	
--			elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--			else																	
--		    	STADO <= CLEAR_OEN_5;
--			end if;

		when CLEAR_OEN_TO_READ_DATA_TO_WRITE =>													
			if ( RXRDY_S = '0')then																		
				RELOAD_WD	<='0';
				OEN_S <= '1';														
--				STADO <= READ_DATA_TO_WRITE_1;	
				STADO <= READ_DATA_TO_WRITE;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else																	
--		    	STADO <= CLEAR_OEN_5;
		    	STADO <= CLEAR_OEN_TO_READ_DATA_TO_WRITE;
			end if;
--		
--		-- read Data operation ---
--		--------------------------------------------------------
--		-- MSB_1--   
--		when READ_DATA_TO_WRITE_1 =>
--			RST_TIME_OUT_UART <= '0';
--			RELOAD_WD	<='1';
--			if ( RXRDY_S = '1') then
--				RST_TIME_OUT_UART <= '1';
--				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0'))then
--					WD_DATA_BUS_UART_R(15 downto 12) <= DATA_FROM_UART(3 downto 0);
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_6;
--				elsif ( (((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
--						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF))))and (FAILED_UART = '0') )then
--					WD_DATA_BUS_UART_R(15 downto 12) <= DATA_FROM_UART(3 downto 0) + x"9";
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_6;
--				else
--					STADO 		<= INVALID_DATA;
--				end if;
--				
--			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
--				STADO 		<= INVALID_DATA;
--			else
--				STADO <= READ_DATA_TO_WRITE_1;
--			end if;

		when READ_DATA_TO_WRITE =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				GET_CMD <= '1';					
				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0'))then
					WD_DATA_BUS_UART_R(READ_DATA_NIBBLE_CNT) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					if READ_DATA_NIBBLE_CNT<DATA_NIBBLES then
						STADO 		<= CLEAR_OEN_TO_READ_DATA_TO_WRITE;
					else
						STADO 		<= CLEAR_OEN_FROM_READ_DATA_TO_WRITE;
					end if;
				elsif ( (((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF))))and (FAILED_UART = '0') )then
					WD_DATA_BUS_UART_R(READ_DATA_NIBBLE_CNT) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					if READ_DATA_NIBBLE_CNT<DATA_NIBBLES then
						STADO 		<= CLEAR_OEN_TO_READ_DATA_TO_WRITE;
					else
						STADO 		<= CLEAR_OEN_FROM_READ_DATA_TO_WRITE;
					end if;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_DATA_TO_WRITE;
			end if;

--		
--		when CLEAR_OEN_6 =>
--			if ( RXRDY_S = '0')then
--				RELOAD_WD	<='0';
--				OEN_S <= '1';
--				STADO <= READ_DATA_TO_WRITE_2;
--			elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--			else
--            	STADO <= CLEAR_OEN_6;
--			end if;
--			
--		-- MSB_2--
--		when READ_DATA_TO_WRITE_2 =>
--			RST_TIME_OUT_UART <= '0';
--			RELOAD_WD	<='1';
--			if ( RXRDY_S = '1') then
--				RST_TIME_OUT_UART <= '1';
--				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0')) then
--					WD_DATA_BUS_UART_R(11 downto 8) <= DATA_FROM_UART(3 downto 0);
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_7;
--				elsif ((((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
--						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF)))) and (FAILED_UART = '0') )then
--					WD_DATA_BUS_UART_R(11 downto 8) <= DATA_FROM_UART(3 downto 0) + x"9";
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_7;
--				else
--					STADO 		<= INVALID_DATA;
--				end if;
--				
--			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
--				STADO 		<= INVALID_DATA;
--			else
--				STADO <= READ_DATA_TO_WRITE_2;
--			end if;	
--			
--		when CLEAR_OEN_7 =>
--			if ( RXRDY_S = '0')then
--				RELOAD_WD	<='0';
--				OEN_S <= '1';
--				STADO <= READ_DATA_TO_WRITE_3;	
--			elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--			else
--				STADO <= CLEAR_OEN_7;
--			end if;
--			
--		-- MSB_3--
--		when READ_DATA_TO_WRITE_3 =>
--			RST_TIME_OUT_UART <= '0';
--			RELOAD_WD	<='1';
--			if ( RXRDY_S = '1') then
--				RST_TIME_OUT_UART <= '1';
--				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0'))then
--					WD_DATA_BUS_UART_R(7 downto 4) <= DATA_FROM_UART(3 downto 0);
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_8;
--				elsif ((((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or 
--						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF)))) and (FAILED_UART = '0'))then
--					WD_DATA_BUS_UART_R(7 downto 4) <= DATA_FROM_UART(3 downto 0) + x"9";
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_8;
--				else
--					STADO 		<= INVALID_DATA;
--				end if;
--				
--			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
--				STADO 		<= INVALID_DATA;
--			else
--				STADO <= READ_DATA_TO_WRITE_3;
--			end if;	
--		
--		when CLEAR_OEN_8 =>
--			if ( RXRDY_S = '0')then
--				RELOAD_WD	<='0';
--				OEN_S <= '1';
--				STADO <= READ_DATA_TO_WRITE_4;	
--		    elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--			else
--		    	STADO <= CLEAR_OEN_8;
--		    end if;
--		
--		-- MSB_4--
--		when READ_DATA_TO_WRITE_4 =>
--			RST_TIME_OUT_UART <= '0';
--			RELOAD_WD	<='1';
--			if ( RXRDY_S = '1') then
--				RST_TIME_OUT_UART <= '1';
--				if (((DATA_FROM_UART >= useruart_ascii(x0)) and (DATA_FROM_UART <= useruart_ascii(x9))) and (FAILED_UART = '0'))then
--					WD_DATA_BUS_UART_R(3 downto 0) <= DATA_FROM_UART(3 downto 0);
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_9;
--				elsif ((((DATA_FROM_UART >= useruart_ascii(uA)) and (DATA_FROM_UART <= useruart_ascii(uF))) or
--						((DATA_FROM_UART >= useruart_ascii(lA)) and (DATA_FROM_UART <= useruart_ascii(lF))))and (FAILED_UART = '0') )then
--					WD_DATA_BUS_UART_R(3 downto 0) <= DATA_FROM_UART(3 downto 0) + x"9";
--					OEN_S <= '0';
--					STADO 		<= CLEAR_OEN_9;
--				else
--					STADO 		<= INVALID_DATA;
--				end if;
--			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
--				STADO 		<= INVALID_DATA;
--			else
--				STADO <= READ_DATA_TO_WRITE_4;
--			end if;	
--		
--		when CLEAR_OEN_9 =>
--			if ( RXRDY_S = '0')then
--				RELOAD_WD	<='0';
--				OEN_S <= '1';
--				STADO <= READ_QUESTION;	
--	        elsif (FAILED_UART = '1') then
--				STADO <= INVALID_DATA;
--			else
--	        	STADO <= CLEAR_OEN_9;
--	        end if;

		when CLEAR_OEN_FROM_READ_DATA_TO_WRITE =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_QUESTION;	
	        elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
	        	STADO <= CLEAR_OEN_FROM_READ_DATA_TO_WRITE;
	        end if;
	
		-- Read question mark --		
		when READ_QUESTION =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (DATA_FROM_UART = useruart_ascii(QUES)) then
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_10;
				else
					GET_CMD		<= '1';
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_QUESTION;
			end if;	
		
		when CLEAR_OEN_10 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_CR;	
		    elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
		    	STADO <= CLEAR_OEN_10;
		    end if;
			
		-- Read CR --
		when READ_CR =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (DATA_FROM_UART = useruart_ascii(CarriageReturn)) then
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_11;
				else
					STADO 		<= INVALID_DATA;
					GET_CMD		<= '1';					
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_CR;
			end if;	
		
		when CLEAR_OEN_11 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_LF;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
				STADO <= CLEAR_OEN_11;
			end if;
			
		-- Read LF --
		when READ_LF =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (DATA_FROM_UART = useruart_ascii(LineFeed)) then
					OEN_S <= '0';
					--ADDRESS_UART <= ADDRESS_UART_R;
					for i in 1 to ADDR_NIBBLES loop
						ADDRESS_UART(((ADDR_NIBBLES-(i-1))*4)-1 downto (ADDR_NIBBLES-i)*4) <= ADDRESS_UART_R(i);
					end loop;
					STADO 		<= WAIT_RESPONSE;
				else
					STADO 		<= INVALID_DATA;
					GET_CMD		<= '1';					
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_LF;
			end if;	
		---------------------------------------------------
		
		---- SEND RESPONSE TO THE COMMAND
		when WAIT_RESPONSE =>
			NO_CLEAN_OEN<='1';
			RELOAD_WD <= '1';
			OEN_S <= '1';
			--if (READ_FLAG = '1') then
			if (TRANSFER_STATE = READ_ST) then
				RD_nWD_EN_UART <= '1';
				nCS_UART <= '0';
				STADO <= WAIT_VALIDATION_R_DELAY;	--WAIT_VALIDATION_R;
			--elsif (WRITE_FLAG = '1')then
			elsif (TRANSFER_STATE = WRITE_ST) then
				RD_nWD_EN_UART 	<= '0';
				nCS_UART <= '0';
				--WD_DATA_BUS_UART<= WD_DATA_BUS_UART_R;
				for i in 1 to DATA_NIBBLES loop
					WD_DATA_BUS_UART(((DATA_NIBBLES-(i-1))*4)-1 downto (DATA_NIBBLES-i)*4) <= WD_DATA_BUS_UART_R(i);
				end loop;
				STADO <= WAIT_VALIDATION_W_DELAY;	--WAIT_VALIDATION_W;
			end if;
			
		when WAIT_VALIDATION_R_DELAY =>	
			STADO <= WAIT_VALIDATION_R;

		when WAIT_VALIDATION_W_DELAY =>	
			STADO <= WAIT_VALIDATION_W;			
			
		when WAIT_VALIDATION_W =>
			RST_TIME_OUT_UART <= '0';
			--nCS_UART <= '0';
			NO_CLEAN_OEN<='1';
			if (ACCESS_VALID_UART = DEV_DATA_OK) then
				STADO	<= VALID_DATA;
			elsif (ACCESS_VALID_UART = DEV_DATA_FL) then 
				STADO	<= INVALID_DATA;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= WAIT_VALIDATION_W;
			end if;
		

		when WAIT_VALIDATION_R =>
			RST_TIME_OUT_UART <= '0';
			--nCS_UART <= '0';
			NO_CLEAN_OEN<='1';
			--COUNT_SEND_DATA		<= (others => '0');	
			if (ACCESS_VALID_UART = DEV_DATA_OK) then
				--RD_DATA_BUS_UART_R	<= RD_DATA_BUS_UART;
				for i in 1 to DATA_NIBBLES loop
					RD_DATA_BUS_UART_R(i) <= RD_DATA_BUS_UART(((DATA_NIBBLES-(i-1))*4)-1 downto (DATA_NIBBLES-i)*4);
				end loop;
				STADO	<= SEND_DATA_RD;
			elsif (ACCESS_VALID_UART = DEV_DATA_FL) then 
				RD_DATA_BUS_UART_R	<= (others=>(others => '0'));
				STADO	<= INVALID_DATA;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= WAIT_VALIDATION_R;
			end if;
			
		-- Send data adquired --
			when SEND_DATA_RD =>
				nCS_UART 	<= '1';
				WEN_S 		<= '1';
				RELOAD_WD	<='1';
				NO_CLEAN_OEN<='1';
				--if (COUNT_SEND_DATA <= x"3") then	
				--	if (COUNT_SEND_DATA = x"0") then
				if (READ_DATA_NIBBLE_CNT <= DATA_NIBBLES-1) then

					if ((RD_DATA_BUS_UART_R(READ_DATA_NIBBLE_CNT+1) >= x"0") and
						(RD_DATA_BUS_UART_R(READ_DATA_NIBBLE_CNT+1) <= x"9")) then	
						DATA_TO_UART <= x"3" & RD_DATA_BUS_UART_R(READ_DATA_NIBBLE_CNT+1);

					else
						DATA_TO_UART <= x"4" & RD_DATA_BUS_UART_R(READ_DATA_NIBBLE_CNT+1) - x"9";
					end if;
					
--					if (READ_DATA_NIBBLE_CNT = 0) then
--					-- MSB 1 --
--						if (RD_DATA_BUS_UART_R(1) >= x"0") and (RD_DATA_BUS_UART_R(1) <= x"9") then
--							--DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(15 downto 12);
--							DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(1);
--						else	
--							--DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(15 downto 12) - x"9");
--							DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(1) - x"9");
--						end if;
--					-- MSB 2 --
--				--	elsif (COUNT_SEND_DATA = x"1") then
--					elsif (READ_DATA_NIBBLE_CNT = 1) then
--						if (RD_DATA_BUS_UART_R(2) >= x"0") and (RD_DATA_BUS_UART_R(2) <= x"9") then
--							--DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(11 downto 8);
--							DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(2);
--						else	
--							--DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(11 downto 8) - x"9");
--							DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(2) - x"9");
--						end if;
--					-- MSB 3 --
--				--	elsif (COUNT_SEND_DATA = x"2") then
--					elsif (READ_DATA_NIBBLE_CNT = 2) then
--						if (RD_DATA_BUS_UART_R(3) >= x"0") and (RD_DATA_BUS_UART_R(3) <= x"9") then
--							--DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(7 downto 4);
--							DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(3);
--						else	
--							--DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(7 downto 4) - x"9");
--							DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(3) - x"9");
--						end if;
--					-- MSB 4 --
--				--	elsif (COUNT_SEND_DATA = x"3") then
--					elsif (READ_DATA_NIBBLE_CNT = 3) then
--						if (RD_DATA_BUS_UART_R(4) >= x"0") and (RD_DATA_BUS_UART_R(4) <= x"9") then
--							--DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(3 downto 0);
--							DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(4);
--						else	
--							--DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(3 downto 0)- x"9");
--							DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(4)- x"9");
--						end if;
--					end if;
--					--COUNT_SEND_DATA <= COUNT_SEND_DATA + x"1";
					STADO 		<= SEND_DATA_RD_1;
					
				--elsif (COUNT_SEND_DATA > x"3") then
				else	-- READ_DATA_NIBBLE_CNT = DATA_NIBBLES-1
					if TRIGGER='1' then
						STADO 		<= VALID_TRIGGER;
					else
						STADO 		<= VALID_DATA;
					end if;
				end if;
			
			when SEND_DATA_RD_1 =>
				NO_CLEAN_OEN<='1';
				if( TXRDY_S = '1') then
					WEN_S <= '0';
					RELOAD_WD	<='0';
					STADO <= SEND_DATA_RD;	
				elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
					STADO 		<= INVALID_DATA;
				else
						STADO <= SEND_DATA_RD_1;
				end if;
			
			
			
		--------------------------------------------------------	
		-- Send 'N' character INVALID ACCESS OR DATA-- 
		when INVALID_DATA =>
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= useruart_ascii(N);
			RST_TIME_OUT_UART <= '1';
			STADO 		<= INVALID_DATA_1;

		when INVALID_DATA_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				STADO <= SEND_FAIL_INFO;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				--STADO 		<= WAIT_OPERATION;
				STADO <= CLEAR_TRIGGER;
			else
				STADO <= INVALID_DATA_1;
			end if;
		
		-- Send 'y' character VALID ACCESS OR DATA-- 
		when VALID_DATA =>
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= useruart_ascii(Y);
			RST_TIME_OUT_UART <= '1';
			STADO 		<= VALID_DATA_1;

		when VALID_TRIGGER =>
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= useruart_ascii(T);
			RST_TIME_OUT_UART <= '1';
			STADO 		<= VALID_DATA_1;			
			
		when VALID_DATA_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				if command_echo=false then
					STADO <= SEND_CR_CHAR;
				else
					STADO <= SEND_ECHO;
				end if;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				--STADO 		<= WAIT_OPERATION;
				STADO <= CLEAR_TRIGGER;				
			else
				STADO <= VALID_DATA_1;
			end if;
		
		-- send fail info -------
		when SEND_FAIL_INFO =>
			RST_TIME_OUT_UART <= '1';
			RELOAD_WD	<='1';
			STADO 		<= SEND_FAIL_INFO_1;
			if (FRAMING_ERR_S = '1') and (PARITY_ERR_S = '1') and (OVERFLOW_S = '1') then
				WEN_S <= '1';
				DATA_TO_UART<= useruart_ascii(x7);
					
			elsif (FRAMING_ERR_S = '1') and (PARITY_ERR_S = '1') and (OVERFLOW_S = '0') then
				WEN_S <= '1';
				DATA_TO_UART<= useruart_ascii(x6);

			elsif (FRAMING_ERR_S = '1') and (PARITY_ERR_S = '0') and (OVERFLOW_S = '1') then
				WEN_S <= '1';
				DATA_TO_UART<= useruart_ascii(x5);
			elsif (FRAMING_ERR_S = '1') and (PARITY_ERR_S = '0') and (OVERFLOW_S = '0') then
				WEN_S <= '1';
				DATA_TO_UART<= useruart_ascii(x4);	
			elsif (FRAMING_ERR_S = '0') and (PARITY_ERR_S = '1') and (OVERFLOW_S = '1') then
				WEN_S <= '1';
				DATA_TO_UART<= useruart_ascii(x3);
			elsif (FRAMING_ERR_S = '0') and (PARITY_ERR_S = '1') and (OVERFLOW_S = '0') then
				WEN_S <= '1';
				DATA_TO_UART<= useruart_ascii(x2);	
			elsif (FRAMING_ERR_S = '0') and (PARITY_ERR_S = '0') and (OVERFLOW_S = '1') then
				WEN_S <= '1';
				DATA_TO_UART<= useruart_ascii(x1);	
			else
				WEN_S <= '1';
				DATA_TO_UART<= useruart_ascii(x0);	
			end if;

		when SEND_FAIL_INFO_1 =>
			if (NO_CLEAN_OEN ='0') then
				OEN_S <= '0';
			else
				OEN_S <= '1';
			end if;
				
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				if command_echo=false then
					STADO <= SEND_CR_CHAR;
				else
					STADO <= SEND_ECHO;
				end if;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				--STADO 		<= WAIT_OPERATION;
				STADO <= CLEAR_TRIGGER;				
			else
				STADO <= SEND_FAIL_INFO_1;
			end if;
			
		-- Send echo -- 
		when SEND_ECHO =>
			NO_CLEAN_OEN <='0';
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= useruart_ascii(COMA);
			RST_TIME_OUT_UART <= '1';
			STADO 		<= SEND_ECHO_1;

		when SEND_ECHO_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				STADO <= SEND_ECHO_2;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				--STADO 		<= WAIT_OPERATION;
				STADO <= CLEAR_TRIGGER;				
			else
				STADO <= SEND_ECHO_1;
			end if;

		when SEND_ECHO_2 =>
			NO_CLEAN_OEN <='0';
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= COMMAND_TEMP(CMD_TEMP_R_CNT);
			SHIFT_CMD <= '1';
			RST_TIME_OUT_UART <= '1';
			STADO 		<= SEND_ECHO_3;

		when SEND_ECHO_3 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				if CMD_TEMP_W_CNT + 1 < CMD_TEMP_R_CNT then
					STADO <= SEND_ECHO_2;				
				else
					STADO <= SEND_CR_CHAR;
				end if;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				--STADO 		<= WAIT_OPERATION;
				STADO <= CLEAR_TRIGGER;				
			else
				STADO <= SEND_ECHO_3;
			end if;			
			
		-- Send 'CR' character -- 
		when SEND_CR_CHAR =>
			NO_CLEAN_OEN <='0';
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= useruart_ascii(CarriageReturn);
			RST_TIME_OUT_UART <= '1';
			STADO 		<= SEND_CR_CHAR_1;

		when SEND_CR_CHAR_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				STADO <= SEND_LF_CHAR;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				--STADO 		<= WAIT_OPERATION;
				STADO <= CLEAR_TRIGGER;				
			else
				STADO <= SEND_CR_CHAR_1;
			end if;
			
		-- Send 'LF' character -- 
		when SEND_LF_CHAR =>
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= useruart_ascii(LineFeed);
			RST_TIME_OUT_UART <= '1';
			STADO 		<= SEND_LF_CHAR_1;

		when SEND_LF_CHAR_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				--STADO <= WAIT_OPERATION;
				STADO <= CLEAR_TRIGGER;				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				--STADO 		<= WAIT_OPERATION;
				STADO <= CLEAR_TRIGGER;				
			else
				STADO <= SEND_LF_CHAR_1;
			end if;
			------------------------- 

		when CLEAR_TRIGGER =>			
			WEN_S		<= '1';
			STADO 		<= WAIT_OPERATION;
			
		when others =>
			UART_BIT_I_S	<= '0';
			RELOAD_WD	<='0';
			--STADO <= WAIT_OPERATION;
			STADO <= CLEAR_TRIGGER;				
			
    end case;
end if;	

	
end process;

-- Rx Data timeout
process (sys_clk, n_RESET)                                   
begin

if ( n_RESET= '0') then     
	TIME_OUT_UART<=0; 
elsif (sys_clk'event and sys_clk='1') then 
	if (RST_TIME_OUT_UART='1') then
		TIME_OUT_UART<=0; 
	elsif TIME_OUT_UART<TIME_OUT_MX_UART then
		TIME_OUT_UART<=TIME_OUT_UART+1; 
	end if;
end if;
end process; 
     
-- Watch Dog timer control, after 100 useg a fail is detected until this module is reset
process (sys_clk, n_RESET)                                   
begin

if ( n_RESET= '0') then     
	UART_BIT_W_D<='0'; 
	WATCH_DOG_TIMER_UART<=TIMEOUT;
	
elsif (sys_clk'event and sys_clk='1') then 
	if (RELOAD_WD='1') then
		if (WATCH_DOG_TIMER_UART = 0) then
			UART_BIT_W_D<='1';
			WATCH_DOG_TIMER_UART<=TIMEOUT;
		else
			WATCH_DOG_TIMER_UART<=WATCH_DOG_TIMER_UART-1;
		end if;
	elsif (RELOAD_WD='0') then
		WATCH_DOG_TIMER_UART<=TIMEOUT;
	end if;
end if;
end process;
	
-- nibble counters
process (sys_clk, n_RESET)                                   
begin
if ( n_RESET= '0') then     
	READ_ADDR_NIBBLE_CNT <= 0;
	READ_DATA_NIBBLE_CNT <= 0;
elsif (sys_clk'event and sys_clk='1') then 
	-- address
	if (STADO = DECODE_OPERATION) then
		READ_ADDR_NIBBLE_CNT <= 0;
	elsif READ_ADDR_NIBBLE_CNT<ADDR_NIBBLES AND
			(( RXRDY_S = '0') and STADO = CLEAR_OEN_TO_READ_ADDR) then
			READ_ADDR_NIBBLE_CNT <= READ_ADDR_NIBBLE_CNT+1;
	end if;
	-- data
	--if STADO = READ_COMA or STADO = WAIT_VALIDATION_R then
	if STADO = WAIT_OPERATION then
		READ_DATA_NIBBLE_CNT <= 0; 
	elsif READ_DATA_NIBBLE_CNT<DATA_NIBBLES AND
			(((RXRDY_S = '0') and STADO = CLEAR_OEN_TO_READ_DATA_TO_WRITE) or STADO = SEND_DATA_RD) then
			READ_DATA_NIBBLE_CNT <= READ_DATA_NIBBLE_CNT+1;
	end if;
end if;
end process;

-- trigger
process (sys_clk, n_RESET)                                   
begin
if ( n_RESET= '0') then     
	TRIGGER_IN_R <= (others=>(others=>'0'));
	TRIGGER_IN_SEND <= (others=>'0');
	TRIGGER <= '0';
elsif (sys_clk'event and sys_clk='1') then
	TRIGGER_IN_R <= TRIGGER_IN_R(0)&TRIGGER_IN;
	if STADO = CLEAR_TRIGGER then
		TRIGGER <= '0';
	elsif TRIGGER_IN_EDGE/=TRIGGER_IN_EDGE_ZEROS AND TRIGGER='0' then
		TRIGGER_IN_SEND <= TRIGGER_IN_R(0);
		TRIGGER <= '1';
	end if;
end if;
end process;

TRIGGER_IN_SRE <= TRIGGER_IN_R(0) and (not TRIGGER_IN_R(1));
TRIGGER_IN_SFE <= (not TRIGGER_IN_R(0)) and TRIGGER_IN_R(1);

TRIGGER_IN_EDGE <= (TRIGGER_IN_MASK and TRIGGER_IN_SRE) or ((not TRIGGER_IN_MASK) and TRIGGER_IN_SFE);

-- command echo
command_echo_true:
   if command_echo=true generate
      begin

		COMMAND_TEMP_FIFO : process(sys_clk, n_RESET)   
		Begin
			if ( n_RESET= '0') then  
				COMMAND_TEMP <= (others=>(others=>'0'));
				CMD_TEMP_W_CNT <= COMMAND_TEMP'high;					
				CMD_TEMP_R_CNT <= COMMAND_TEMP'high;					
			elsif (sys_clk'event and sys_clk='1') then
				if CLEAR_CMD_TEMP='1' then
					COMMAND_TEMP <= (others=>(others=>'0'));
					CMD_TEMP_W_CNT <= COMMAND_TEMP'high;					
					CMD_TEMP_R_CNT <= COMMAND_TEMP'high;					
				elsif GET_CMD='1' then
					if (DATA_FROM_UART = useruart_ascii(CarriageReturn)) or
						(DATA_FROM_UART = useruart_ascii(LineFeed)) then
						COMMAND_TEMP(CMD_TEMP_W_CNT) <= x"00";
					else
						COMMAND_TEMP(CMD_TEMP_W_CNT) <= DATA_FROM_UART;
					end if;
					if CMD_TEMP_W_CNT>COMMAND_TEMP'low then
						CMD_TEMP_W_CNT <= CMD_TEMP_W_CNT-1;
					end if;
				elsif SHIFT_CMD='1' then	
					if CMD_TEMP_R_CNT>COMMAND_TEMP'low then
						CMD_TEMP_R_CNT <= CMD_TEMP_R_CNT-1;
					end if;
				end if;
			end if;
		end process;
		
		COMMAND_TEMP_DEBUG : process(COMMAND_TEMP)
		begin
			for i in 0 to COMMAND_TEMP'length-1 loop
				CMD_TEMP_WAVE((8*(i+1))-1 downto i*8) <= COMMAND_TEMP(i);
			end loop;		
		end process;
   
   end generate;
   
-- timeout relax
TIME_OUT_MX_UART <= timeout_mult*TIMEOUT;
   
end bhu;