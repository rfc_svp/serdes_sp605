library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;

entity vme_arbiter is
  port (
    br_b_in     : in  std_logic_vector(3 downto 0);
    int_br_b_in : in  std_logic_vector(3 downto 0);
    bgout_b     : out std_logic_vector(3 downto 0);
    bclr_b_out  : out std_logic;
    bclr_b_oen  : out std_logic;
    bbsy_b_in   : in  std_logic;
    bbsy_set    : out std_logic;
    bbsy_rst    : out std_logic;
    scon_b      : in  std_logic;
    arb_fail    : out std_logic;
    arcr        : in  std_logic_vector(7 downto 0);
    clk         : in  std_logic;
    rst         : in  std_logic);
end entity vme_arbiter;

architecture rtl of vme_arbiter is
  type   arb_state_type is (idle, arb, gtd, busy);
  signal arb_crt      : arb_state_type;
  signal last_gt      : std_logic_vector(3 downto 0);
  signal ready        : std_logic;
  signal arb_timer    : unsigned(8 downto 0);
  signal i_bclr_b_out : std_logic;
  signal bclr_oen     : std_logic;
  signal r_br         : std_logic_vector(3 downto 0);
  signal r_bbsy       : std_logic;
  signal i_bgout_b    : std_logic_vector(3 downto 0);
  signal i_br         : std_logic_vector(3 downto 0);
  signal i_bsy        : std_logic_vector(5 downto 0);

begin  -- architecture rtl2

--  bclr_b_out <= i_bclr_b_out;
  bclr_b_oen <= bclr_oen or not i_bclr_b_out;
  bgout_b    <= i_bgout_b;
  i_br       <= r_br or not int_br_b_in;
  bbsy_set   <= i_bsy(0);
  bbsy_rst   <= i_bsy(5);

  process (clk, rst) is
    variable v_gt : std_logic_vector(3 downto 0);
  begin  -- process
    if rst = '0' then                   -- asynchronous reset (active low)
      arb_crt      <= idle;
      r_br         <= (others => '0');
      r_bbsy       <= '0';
      i_bclr_b_out <= '1';
      bclr_b_out   <= '0';
      bclr_oen     <= '0';
      arb_timer    <= (others => '1');
      ready        <= '1';
      last_gt      <= x"1";
      i_bgout_b    <= (others => '1');
      arb_fail     <= '0';
      i_bsy        <= (others => '0');
    elsif rising_edge(clk) then         -- rising clock edge
      bclr_oen   <= not i_bclr_b_out;
      bclr_b_out <= i_bclr_b_out;
      if scon_b = '0' then
        arb_fail <= '0';
        r_br     <= not br_b_in;
        r_bbsy   <= not bbsy_b_in;
        case arb_crt is
          when idle =>
            i_bsy     <= (others => '0');
            i_bgout_b <= x"F";
            arb_timer <= (others => '1');
            if i_br /= x"0" then
              arb_crt <= arb;
            end if;
          when arb =>
            if arcr(7) = '1' then
--              v_gt := x"1";             -- priority arbitration
              v_gt := x"8";             -- priority arbitration
            else
              v_gt := last_gt;
            end if;
            for i in i_br'range loop
              v_gt := v_gt(2 downto 0) & v_gt(3);
              if (i_br and v_gt) /= x"0" then
                i_bgout_b <= not(i_br and v_gt);
                last_gt   <= i_br and v_gt;  -- round-robin arbitration
                ready     <= '0';
              end if;
            end loop;  -- i
            arb_crt <= gtd;
          when gtd =>
            if arb_timer = 0 then
              i_bsy        <= i_bsy(4 downto 0) & '1';
              arb_fail     <= '1';
            else
              arb_timer <= arb_timer - 1;
            end if;
            if r_bbsy = '1' then
              arb_crt <= busy;
            end if;
          when busy =>
            i_bsy     <= i_bsy(4 downto 0) & '0';
            i_bgout_b <= x"F";
            if i_br /= x"0" then        -- other request
              if arcr(7) = '1' then     -- priority arbitration
                for j in br_b_in'range loop
                  if (last_gt(j) and not i_br(j)) = '1' then  -- lower priority
                    exit;               --    request
                  elsif (i_br(j) and i_bgout_b(j)) = '1' then
                    i_bclr_b_out <= '0';
                    bclr_b_out   <= '0';
                    exit;
                  end if;
                end loop;  -- j
              elsif i_bgout_b = x"F" then    -- round-robin arbitration
                i_bclr_b_out <= '0';
                bclr_b_out   <= '0';
              end if;
            end if;
            if r_bbsy = '0' then
              arb_crt      <= idle;
              i_bclr_b_out <= '1';
              bclr_b_out   <= '1';
            end if;
          when others =>
            null;
        end case;
      end if;
    end if;
  end process;

end architecture rtl;
