-- Version: 9.1 9.1.0.18

library ieee;
use ieee.std_logic_1164.all;
library fusion;
use fusion.all;

entity RAM_TEST is 
    port( DINA : in std_logic_vector(15 downto 0); DOUTA : out 
        std_logic_vector(15 downto 0); DINB : in 
        std_logic_vector(15 downto 0); DOUTB : out 
        std_logic_vector(15 downto 0); ADDRA : in 
        std_logic_vector(9 downto 0); ADDRB : in 
        std_logic_vector(9 downto 0);RWA, RWB, BLKA, BLKB, CLK, 
        RESET : in std_logic) ;
end RAM_TEST;


architecture DEF_ARCH of  RAM_TEST is

    component RAM4K9
    generic (MEMORYFILE:string := "");

        port(ADDRA11, ADDRA10, ADDRA9, ADDRA8, ADDRA7, ADDRA6, 
        ADDRA5, ADDRA4, ADDRA3, ADDRA2, ADDRA1, ADDRA0, ADDRB11, 
        ADDRB10, ADDRB9, ADDRB8, ADDRB7, ADDRB6, ADDRB5, ADDRB4, 
        ADDRB3, ADDRB2, ADDRB1, ADDRB0, DINA8, DINA7, DINA6, 
        DINA5, DINA4, DINA3, DINA2, DINA1, DINA0, DINB8, DINB7, 
        DINB6, DINB5, DINB4, DINB3, DINB2, DINB1, DINB0, WIDTHA0, 
        WIDTHA1, WIDTHB0, WIDTHB1, PIPEA, PIPEB, WMODEA, WMODEB, 
        BLKA, BLKB, WENA, WENB, CLKA, CLKB, RESET : in std_logic := 
        'U'; DOUTA8, DOUTA7, DOUTA6, DOUTA5, DOUTA4, DOUTA3, 
        DOUTA2, DOUTA1, DOUTA0, DOUTB8, DOUTB7, DOUTB6, DOUTB5, 
        DOUTB4, DOUTB3, DOUTB2, DOUTB1, DOUTB0 : out std_logic) ;
    end component;

    component VCC
        port( Y : out std_logic);
    end component;

    component GND
        port( Y : out std_logic);
    end component;

    signal VCC_1_net, GND_1_net : std_logic ;
    begin   

    VCC_2_net : VCC port map(Y => VCC_1_net);
    GND_2_net : GND port map(Y => GND_1_net);
    RAM_TEST_R0C0 : RAM4K9
      port map(ADDRA11 => GND_1_net, ADDRA10 => GND_1_net, 
        ADDRA9 => ADDRA(9), ADDRA8 => ADDRA(8), ADDRA7 => 
        ADDRA(7), ADDRA6 => ADDRA(6), ADDRA5 => ADDRA(5), 
        ADDRA4 => ADDRA(4), ADDRA3 => ADDRA(3), ADDRA2 => 
        ADDRA(2), ADDRA1 => ADDRA(1), ADDRA0 => ADDRA(0), 
        ADDRB11 => GND_1_net, ADDRB10 => GND_1_net, ADDRB9 => 
        ADDRB(9), ADDRB8 => ADDRB(8), ADDRB7 => ADDRB(7), 
        ADDRB6 => ADDRB(6), ADDRB5 => ADDRB(5), ADDRB4 => 
        ADDRB(4), ADDRB3 => ADDRB(3), ADDRB2 => ADDRB(2), 
        ADDRB1 => ADDRB(1), ADDRB0 => ADDRB(0), DINA8 => 
        GND_1_net, DINA7 => GND_1_net, DINA6 => GND_1_net, 
        DINA5 => GND_1_net, DINA4 => GND_1_net, DINA3 => DINA(3), 
        DINA2 => DINA(2), DINA1 => DINA(1), DINA0 => DINA(0), 
        DINB8 => GND_1_net, DINB7 => GND_1_net, DINB6 => 
        GND_1_net, DINB5 => GND_1_net, DINB4 => GND_1_net, 
        DINB3 => DINB(3), DINB2 => DINB(2), DINB1 => DINB(1), 
        DINB0 => DINB(0), WIDTHA0 => GND_1_net, WIDTHA1 => 
        VCC_1_net, WIDTHB0 => GND_1_net, WIDTHB1 => VCC_1_net, 
        PIPEA => GND_1_net, PIPEB => GND_1_net, WMODEA => 
        VCC_1_net, WMODEB => VCC_1_net, BLKA => BLKA, BLKB => 
        BLKB, WENA => RWA, WENB => RWB, CLKA => CLK, CLKB => CLK, 
        RESET => RESET, DOUTA8 => OPEN , DOUTA7 => OPEN , 
        DOUTA6 => OPEN , DOUTA5 => OPEN , DOUTA4 => OPEN , 
        DOUTA3 => DOUTA(3), DOUTA2 => DOUTA(2), DOUTA1 => 
        DOUTA(1), DOUTA0 => DOUTA(0), DOUTB8 => OPEN , DOUTB7 => 
        OPEN , DOUTB6 => OPEN , DOUTB5 => OPEN , DOUTB4 => OPEN , 
        DOUTB3 => DOUTB(3), DOUTB2 => DOUTB(2), DOUTB1 => 
        DOUTB(1), DOUTB0 => DOUTB(0));
    RAM_TEST_R0C2 : RAM4K9
      port map(ADDRA11 => GND_1_net, ADDRA10 => GND_1_net, 
        ADDRA9 => ADDRA(9), ADDRA8 => ADDRA(8), ADDRA7 => 
        ADDRA(7), ADDRA6 => ADDRA(6), ADDRA5 => ADDRA(5), 
        ADDRA4 => ADDRA(4), ADDRA3 => ADDRA(3), ADDRA2 => 
        ADDRA(2), ADDRA1 => ADDRA(1), ADDRA0 => ADDRA(0), 
        ADDRB11 => GND_1_net, ADDRB10 => GND_1_net, ADDRB9 => 
        ADDRB(9), ADDRB8 => ADDRB(8), ADDRB7 => ADDRB(7), 
        ADDRB6 => ADDRB(6), ADDRB5 => ADDRB(5), ADDRB4 => 
        ADDRB(4), ADDRB3 => ADDRB(3), ADDRB2 => ADDRB(2), 
        ADDRB1 => ADDRB(1), ADDRB0 => ADDRB(0), DINA8 => 
        GND_1_net, DINA7 => GND_1_net, DINA6 => GND_1_net, 
        DINA5 => GND_1_net, DINA4 => GND_1_net, DINA3 => DINA(11), 
        DINA2 => DINA(10), DINA1 => DINA(9), DINA0 => DINA(8), 
        DINB8 => GND_1_net, DINB7 => GND_1_net, DINB6 => 
        GND_1_net, DINB5 => GND_1_net, DINB4 => GND_1_net, 
        DINB3 => DINB(11), DINB2 => DINB(10), DINB1 => DINB(9), 
        DINB0 => DINB(8), WIDTHA0 => GND_1_net, WIDTHA1 => 
        VCC_1_net, WIDTHB0 => GND_1_net, WIDTHB1 => VCC_1_net, 
        PIPEA => GND_1_net, PIPEB => GND_1_net, WMODEA => 
        VCC_1_net, WMODEB => VCC_1_net, BLKA => BLKA, BLKB => 
        BLKB, WENA => RWA, WENB => RWB, CLKA => CLK, CLKB => CLK, 
        RESET => RESET, DOUTA8 => OPEN , DOUTA7 => OPEN , 
        DOUTA6 => OPEN , DOUTA5 => OPEN , DOUTA4 => OPEN , 
        DOUTA3 => DOUTA(11), DOUTA2 => DOUTA(10), DOUTA1 => 
        DOUTA(9), DOUTA0 => DOUTA(8), DOUTB8 => OPEN , DOUTB7 => 
        OPEN , DOUTB6 => OPEN , DOUTB5 => OPEN , DOUTB4 => OPEN , 
        DOUTB3 => DOUTB(11), DOUTB2 => DOUTB(10), DOUTB1 => 
        DOUTB(9), DOUTB0 => DOUTB(8));
    RAM_TEST_R0C3 : RAM4K9
      port map(ADDRA11 => GND_1_net, ADDRA10 => GND_1_net, 
        ADDRA9 => ADDRA(9), ADDRA8 => ADDRA(8), ADDRA7 => 
        ADDRA(7), ADDRA6 => ADDRA(6), ADDRA5 => ADDRA(5), 
        ADDRA4 => ADDRA(4), ADDRA3 => ADDRA(3), ADDRA2 => 
        ADDRA(2), ADDRA1 => ADDRA(1), ADDRA0 => ADDRA(0), 
        ADDRB11 => GND_1_net, ADDRB10 => GND_1_net, ADDRB9 => 
        ADDRB(9), ADDRB8 => ADDRB(8), ADDRB7 => ADDRB(7), 
        ADDRB6 => ADDRB(6), ADDRB5 => ADDRB(5), ADDRB4 => 
        ADDRB(4), ADDRB3 => ADDRB(3), ADDRB2 => ADDRB(2), 
        ADDRB1 => ADDRB(1), ADDRB0 => ADDRB(0), DINA8 => 
        GND_1_net, DINA7 => GND_1_net, DINA6 => GND_1_net, 
        DINA5 => GND_1_net, DINA4 => GND_1_net, DINA3 => DINA(15), 
        DINA2 => DINA(14), DINA1 => DINA(13), DINA0 => DINA(12), 
        DINB8 => GND_1_net, DINB7 => GND_1_net, DINB6 => 
        GND_1_net, DINB5 => GND_1_net, DINB4 => GND_1_net, 
        DINB3 => DINB(15), DINB2 => DINB(14), DINB1 => DINB(13), 
        DINB0 => DINB(12), WIDTHA0 => GND_1_net, WIDTHA1 => 
        VCC_1_net, WIDTHB0 => GND_1_net, WIDTHB1 => VCC_1_net, 
        PIPEA => GND_1_net, PIPEB => GND_1_net, WMODEA => 
        VCC_1_net, WMODEB => VCC_1_net, BLKA => BLKA, BLKB => 
        BLKB, WENA => RWA, WENB => RWB, CLKA => CLK, CLKB => CLK, 
        RESET => RESET, DOUTA8 => OPEN , DOUTA7 => OPEN , 
        DOUTA6 => OPEN , DOUTA5 => OPEN , DOUTA4 => OPEN , 
        DOUTA3 => DOUTA(15), DOUTA2 => DOUTA(14), DOUTA1 => 
        DOUTA(13), DOUTA0 => DOUTA(12), DOUTB8 => OPEN , 
        DOUTB7 => OPEN , DOUTB6 => OPEN , DOUTB5 => OPEN , 
        DOUTB4 => OPEN , DOUTB3 => DOUTB(15), DOUTB2 => DOUTB(14), 
        DOUTB1 => DOUTB(13), DOUTB0 => DOUTB(12));
    RAM_TEST_R0C1 : RAM4K9
      port map(ADDRA11 => GND_1_net, ADDRA10 => GND_1_net, 
        ADDRA9 => ADDRA(9), ADDRA8 => ADDRA(8), ADDRA7 => 
        ADDRA(7), ADDRA6 => ADDRA(6), ADDRA5 => ADDRA(5), 
        ADDRA4 => ADDRA(4), ADDRA3 => ADDRA(3), ADDRA2 => 
        ADDRA(2), ADDRA1 => ADDRA(1), ADDRA0 => ADDRA(0), 
        ADDRB11 => GND_1_net, ADDRB10 => GND_1_net, ADDRB9 => 
        ADDRB(9), ADDRB8 => ADDRB(8), ADDRB7 => ADDRB(7), 
        ADDRB6 => ADDRB(6), ADDRB5 => ADDRB(5), ADDRB4 => 
        ADDRB(4), ADDRB3 => ADDRB(3), ADDRB2 => ADDRB(2), 
        ADDRB1 => ADDRB(1), ADDRB0 => ADDRB(0), DINA8 => 
        GND_1_net, DINA7 => GND_1_net, DINA6 => GND_1_net, 
        DINA5 => GND_1_net, DINA4 => GND_1_net, DINA3 => DINA(7), 
        DINA2 => DINA(6), DINA1 => DINA(5), DINA0 => DINA(4), 
        DINB8 => GND_1_net, DINB7 => GND_1_net, DINB6 => 
        GND_1_net, DINB5 => GND_1_net, DINB4 => GND_1_net, 
        DINB3 => DINB(7), DINB2 => DINB(6), DINB1 => DINB(5), 
        DINB0 => DINB(4), WIDTHA0 => GND_1_net, WIDTHA1 => 
        VCC_1_net, WIDTHB0 => GND_1_net, WIDTHB1 => VCC_1_net, 
        PIPEA => GND_1_net, PIPEB => GND_1_net, WMODEA => 
        VCC_1_net, WMODEB => VCC_1_net, BLKA => BLKA, BLKB => 
        BLKB, WENA => RWA, WENB => RWB, CLKA => CLK, CLKB => CLK, 
        RESET => RESET, DOUTA8 => OPEN , DOUTA7 => OPEN , 
        DOUTA6 => OPEN , DOUTA5 => OPEN , DOUTA4 => OPEN , 
        DOUTA3 => DOUTA(7), DOUTA2 => DOUTA(6), DOUTA1 => 
        DOUTA(5), DOUTA0 => DOUTA(4), DOUTB8 => OPEN , DOUTB7 => 
        OPEN , DOUTB6 => OPEN , DOUTB5 => OPEN , DOUTB4 => OPEN , 
        DOUTB3 => DOUTB(7), DOUTB2 => DOUTB(6), DOUTB1 => 
        DOUTB(5), DOUTB0 => DOUTB(4));
end DEF_ARCH;

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:9.1.0.18
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:Fusion
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_RAM
-- LPM_HINT:DUAL
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:H:/INDRA/MIDS/FPGA_FW/test_uart_rfc/smartgen\RAM_TEST
-- GEN_BEHV_MODULE:T
-- SMARTGEN_DIE:M1IR10X10M3
-- SMARTGEN_PACKAGE:fg484
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- WWIDTH:16
-- WDEPTH:1024
-- RWIDTH:16
-- RDEPTH:1024
-- CLKS:1
-- CLOCK_PN:CLK
-- RESET_PN:RESET
-- RESET_POLARITY:0
-- INIT_RAM:F
-- DEFAULT_WORD:0x0000
-- CASCADE:0
-- WCLK_EDGE:RISE
-- WMODE1:1
-- WMODE2:1
-- PMODE1:0
-- PMODE2:0
-- DATAA_IN_PN:DINA
-- DATAA_OUT_PN:DOUTA
-- ADDRESSA_PN:ADDRA
-- RWA_PN:RWA
-- BLKA_PN:BLKA
-- DATAB_IN_PN:DINB
-- DATAB_OUT_PN:DOUTB
-- ADDRESSB_PN:ADDRB
-- RWB_PN:RWB
-- BLKB_PN:BLKB
-- WE_POLARITY:0
-- RE_POLARITY:0
-- PTYPE:2

-- _End_Comments_

