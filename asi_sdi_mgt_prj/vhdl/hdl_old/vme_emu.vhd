library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
---library ACT3;
---use ACT3.COMPONENTS.all;
-- library synplify;
-- use synplify.attributes.all;
use work.Int_Pkg.all;
use work.user_serial_pkg.all;

-------------------------- Changes made in version 02_B0  ---------------------
-- 23-1-2015 MVV: Constant TIME_OUT_MX_UART =x"FFFF"; It is in the USER_UART.vhd module

--26-1-2015 MVV change the sentences:
-- Original sentence:
--pn_ctrl (0) <= "00000000000000"&irq_b_in&berr_b_in&sysfail_b_in&bclr_b_in&bbsy_b_in&br_b_in&helath_b&tpfail_b&'0';
--New sentence:
--pn_ctrl (0) <= "0000000000000"&irq_b_in&berr_b_in&sysfail_b_in&bclr_b_in&bbsy_b_in&br_b_in&helath_b&tpfail_b&"00";

-- We do the same change in pn_ctrl (0); change in pn_ctrl (1); change in pn_ctrl (2); pn_ctrl_out (0); 
-- pn_ctrl_out (1) and pn_ctrl_out (2)
----------------------------------------------------------------------------

------------------------- Changes made in version 02_0C/D  ---------------------
-- 31-1-2015  RFC: Register inputs add
-- 31-1-2015  RFC: PLL modification 50 MHz to 20 MHz CLK in order to fulfil timing constrains
-- 31-1-2015  RFC: Reset modification
-- 31-1-2015  RFC: IRQ Handler not enable
-- 02-02-2015 RFC: sysfail_b_out_i enable
-- 03-02-2015 RFC: scon_b pin enable 
-- 03-02-2015 RFC: fix bus arbiter BG/BR (brqg_reg active low level) negated	
-- 03-02-2015 RFC: LED_N(3) 		<= not run_c(4);		slave mode on in LED
-- 04-02-2015 RFC: Modified add vme init R/W, see mem map ver 1.4
---------------------------------------------------------------------------


------------------------- Changes made in version 02_0E  ---------------------
-- 11-02-2015  RFC: ADDED MASTER BROAD CAST run_c(7)
-- 11-02-2015  RFC: REG DATA IN in Master mode with dtack falling

-- 12-2-2015  MVV: Add the module LED_N2_D6.vhd This module keeps D6 lighting for 100 ms after 
--                 berr_b_in changes from '0' to'1'.
---------------------------------------------------------------------------

------------------------- Changes made in version 02_0F  ---------------------
-- 24-02-2015  RFC: lword_b_out stack up '1'; No 32d access/ Identify 
-- 24-02-2015  RFC: Identify added to debug Libero SOC 11.5
-- 24-02-2015  RFC: FIX run_c
-- 24-02-2015  RFC: FIX DS timing
-- 02-03-2015  RFC: Register inputs control & remove prq state (pinout is latched continually in wait_cmd state)
-- 03-03-2015  RFC: Fix R/W RAM-UART
---------------------------------------------------------------------------

------------------------- Changes made in version 02_10  ---------------------
-- 14-03-2015  RFC: ICK Test Daisy Chain
-- 14-03-2015  RFC: BG Test Daisy Chain
-- 14-03-2015  RFC: EOS pulse width control reg
-- 14-03-2015  RFC: Bus error Feature run_c_fix(8). (Only in Master Mode because ldsoe_i status)
-- 24-03-2015  RFC: FIX DS in DATING
---------------------------------------------------------------------------

------------------------- Changes made in version 02_11  ---------------------
-- 13-04-2015  RFC: IRQ Handler implementation			
-- 17-04-2015  JAP and MVV: Bus error implementation
-- Change sentence: loe <= (not mui_reg(1)) and loe_i and not(run_c_fix(7));
-- To sentence loe <= (not mui_reg(1)) and loe_i and not(run_c_fix(7)) and berr_b_out_i;
-- Change sentence to_counter <= to_counter + 1;
-- To:

-- if to_counter < TO_MAX then  
--		to_counter <= to_counter + 1;
-- else
--		to_counter <= TO_MAX;
-- end if;
---------------------------------------------------------------------------

------------------------- Changes made in version 02_12  ---------------------
-- 30-10-2015  RFC: EOS FQ ADDE			
---------------------------------------------------------------------------

------------------------- Changes made in version 04 ---------------------
-- 05-04-2016  RFC: BBLT	(BLT in MEM blocks)	 4.00
-- 05-04-2016  RFC: EOS independient control	 4.00
-- 12-04-2016  RFC: IRQ ID configurable			 4.01
-- 12-04-2016  RFC: Reset PN					 4.01
-- 31-06-2016  FIX PN issue					 	 4.02
-- 12-04-2016  FIX BBLT issues increase time in BLT to BLT 		 4.03
-- 07-07-2016  FIX several isssue revision A
---------------------------------------------------------------------------

entity vme_emu is
  port (
		
	-- System signals:
    CLK_IN     	   : in  std_logic;
    Reset_n_in     : in  std_logic;
	LED_N     	   : out std_logic_vector(4 downto 1);
		
	---- VME bus signals  ----
	 ldoe       : out   std_logic;       -- Latch Outgoing VMEbus Data
    d_in       : in std_logic_vector(15 downto 0);  	-- VMEbus Data
	d_out      : out std_logic_vector(15 downto 0);  	-- VMEbus Data
	 laoe       : out   std_logic;       -- Latch Outgoing VMEbus Address
    a_in       : in std_logic_vector(23 downto 1);  	-- VMEbus Address
	a_out      : out std_logic_vector(23 downto 1);  	-- VMEbus Address
	 lamoe       : out   std_logic;       -- Latch Outgoing VMEbus Address Modifier
    am_in      : in std_logic_vector(5 downto 0);  		-- VMEbus Address Modifier
	am_out    : out std_logic_vector(5 downto 0);  	-- VMEbus Address Modifier
    
	 lbgoe      : out   std_logic;       -- VMEbus BG S O Enable
	 lbgdir     : out   std_logic;       -- VMEbus BG Dir
	bgin_b 	   	: in std_logic_vector(3 downto 0);  		-- VMEbus Bus Grant Input
	iack_b     	: in    std_logic;       				-- VMEbus Interrupt Acknowledge 
	sysreset_b  : inout std_logic;       					-- VMEbus System Reset
	
	 lbgoe_metro      : out   std_logic;       -- VMEbus BG S O Enable
	 lbgdir_metro     : out   std_logic;       -- VMEbus BG Dir
	bgin_b_metro 	  : out std_logic_vector(3 downto 0);  		-- VMEbus Bus Grant Input
	iack_b_metro     : out    std_logic;       				-- VMEbus Interrupt Acknowledge 
	reseta  : out std_logic;       					-- VMEbus System Reset
	
  
	 lirqo    : out   std_logic;       -- Latch Outgoing INT
	irq_b_in   : in std_logic_vector(7 downto 1);  		-- VMEbus Int Request
	irq_b_out  : out std_logic_vector(7 downto 1);  	-- VMEbus Int Request
   
	 ldsoe      : out   std_logic;       -- VMEbus Data S O Enable
    as_b_in    : in std_logic;       					-- VMEbus Address Strobe
	as_b_out   : out std_logic;       					-- VMEbus Address Strobe
    ds_b_in    : in std_logic_vector(1 downto 0);  		-- VMEbus DataStrobe
	ds_b_out   : out std_logic_vector(1 downto 0);  	-- VMEbus DataStrobe
	write_b_in : in std_logic;       					-- VMEbus Data Direction
	write_b_out: out std_logic;       					-- VMEbus Data Direction
    lword_b_in : in std_logic;       					-- VMEbus Long-Word
	lword_b_out: out std_logic;       					-- VMEbus Long-Word
	berr_b_in  : in std_logic;       					-- VMEbus Error
	--berr_b_out : out std_logic;       					-- VMEbus Error
	sysfail_b_in	: in std_logic;       				-- VMEbus System Fail
	sysfail_b_out   : out std_logic;       				-- VMEbus System Fail
	
	 lbroe      : out   std_logic;       -- VMEbus Request O Enable
	dtack_b_in : in std_logic;       					-- VMEbus Data Transfer Acknowledge
	--dtack_b_out_i: out std_logic;       					-- VMEbus Data Transfer Acknowledge
	bclr_b_in  : in std_logic;       					-- VMEbus Clear
	bclr_b_out : out std_logic;       					-- VMEbus Clear
	bbsy_b_in  : in std_logic;       					-- VMEbus Busy
	bbsy_b_out : out std_logic;       					-- VMEbus Busy
	br_b_in    : in std_logic_vector(3 downto 0);  		-- VMEbus Request
	br_b_out   : out std_logic_vector(3 downto 0);  	-- VMEbus Request
  
     loe      : out   std_logic;      
	 ldir     : out   std_logic;      
	eos_b   	: out    std_logic;       			-- VMEbus EOS Fail
	dcfail_b   	: out    std_logic;       			-- VMEbus DC Fail
	spare_A	    : inout std_logic_vector(11 downto 6);  -- SPARE
	
	--ONLY IN 12/12/2014
	helath_b   	: in    std_logic;       				-- VMEbus HEALTH
	tpfail_b   	: in    std_logic;       				-- VMEbus TP Fail
	scon_b	    : in    std_logic;  					-- SCON 
	spare_B	    : in std_logic_vector(15 downto 12);  	-- SPARE
	
	user_in	    : in std_logic_vector(7 downto 0);  -- SPARE
	xsysclk	    : out std_logic; 
			
    ---- Serial signals  ----  
    TX_RS232_FPGA   : out std_logic;
    RX_RS232_FPGA   : in  std_logic
        
            );
end vme_emu;

------------------------------------------------------------------------
architecture RTL of vme_emu is


component USER_UART is 
	generic (
		ClkFreq				: INTEGER;		-- clock freq (Hz)
		BaudRate			: INTEGER;		-- bps
		bit8				: std_logic;	-- byte = '1' 
		parity_en			: std_logic;	-- enable = '1' 
		odd_n_even			: std_logic;	-- odd = '1' even = '0'
		-- sizes
		ADDR_NIBBLES		: natural := 8;
		DATA_NIBBLES 		: natural := 8;
	-- protocol config
		command_echo		: boolean := true;
		timeout_mult		: natural := 2		
	);
	port (
		n_RESET,sys_clk		: in        std_logic;
		enable				: in        std_logic;
		-- i/f CORE IP
		RXDATA         		: in        std_logic;
		TXDATA         		: out       std_logic;
		-- i/f MASTER BUS
		ADDRESS_UART   		:   out		std_logic_vector ((4*ADDR_NIBBLES)-1 downto 0); -- Address bus
		RD_DATA_BUS_UART    :   in		std_logic_vector ((4*DATA_NIBBLES)-1 downto 0); -- Input data bus
		WD_DATA_BUS_UART  	:   out		std_logic_vector ((4*DATA_NIBBLES)-1 downto 0); -- Output data bus
		RD_nWD_EN_UART		:   out		std_logic;                      -- Read(active high)/write(active low) enable
		nCS_UART			:	out		std_logic;						-- Operation request(active low)
		ACCESS_VALID_UART	:	in		ACCESS_VALID_UART_type;			-- Access valid acknowledge
		WAIT_UP_ADC_DATA	:	in		std_logic;						-- Ready data in the ADC
		--	error flags
		UART_BIT_I_S		:	out	std_logic;						-- module in invalid state
		UART_BIT_W_D		:	out	std_logic;						-- module is STOPED
		-- debug outputs
		RXRDY_o 			:	out	std_logic;
		TXRDY_o 			:	out	std_logic;
		-- trigger port
		TRIGGER_IN    		:   in std_logic_vector((4*DATA_NIBBLES)-1 downto 0);
		TRIGGER_IN_MASK		:   in std_logic_vector((4*DATA_NIBBLES)-1 downto 0)	-- if 1 rising, if 0 falling
	);
end component;


component vme_arbiter is
  port (
    br_b_in     : in  std_logic_vector(3 downto 0);
    int_br_b_in : in  std_logic_vector(3 downto 0);
    bgout_b     : out std_logic_vector(3 downto 0);
    bclr_b_out  : out std_logic;
    bclr_b_oen  : out std_logic;
    bbsy_b_in   : in  std_logic;
    bbsy_set    : out std_logic;
    bbsy_rst    : out std_logic;
    scon_b      : in  std_logic;
    arb_fail    : out std_logic;
    arcr        : in  std_logic_vector(7 downto 0);
    clk         : in  std_logic;
    rst         : in  std_logic);
end component;


component RAM is -- 64 words
    port( DINA : in std_logic_vector(31 downto 0); DOUTA : out 
        std_logic_vector(31 downto 0); DINB : in 
        std_logic_vector(31 downto 0); DOUTB : out 
        std_logic_vector(31 downto 0); ADDRA : in 
        std_logic_vector(5 downto 0); ADDRB : in 
        std_logic_vector(5 downto 0);RWA, RWB, BLKA, BLKB, CLKAB, 
        RESET : in std_logic) ;
end component;


component RAM16 is  -- 128 words
    port( DINA : in std_logic_vector(15 downto 0); DOUTA : out 
        std_logic_vector(15 downto 0); DINB : in 
        std_logic_vector(15 downto 0); DOUTB : out 
        std_logic_vector(15 downto 0); ADDRA : in 
        std_logic_vector(6 downto 0); ADDRB : in 
        std_logic_vector(6 downto 0);RWA, RWB, BLKA, BLKB, CLKAB, 
        RESET : in std_logic) ;
end component;


component PLL_SYSCLK is 
    port(POWERDOWN, CLKA : in std_logic;  LOCK, GLA : out 
        std_logic;  OADIVRST : in std_logic) ;
end component;

component PLL_CLKIN is 
    port(POWERDOWN, CLKA : in std_logic;  LOCK, GLA : out 
        std_logic;  OADIVRST : in std_logic) ;
end component;

component reset_circuit is
    port( rst_p	: in std_logic;
          CLK	: in std_logic;
          rst_n	: out std_logic
        );
end component;

	component Int_Handler 
	port(
		-- Three State input signals: IRQ_N, DTACK_N, BERR_N, DataIn. 
		-- Three State output signals: IACK_N, AS_N, DS0_N, DS1_N, Address .

		-- Inputs
		CLK         : in std_logic; -- 50 MHz
		Reset_N     : in std_logic; -- asynchronous reset (active low)
		IRQ_N       : in std_logic_vector(7 downto 1);  --Interrupt request.
		IACK_IN_N   : in std_logic;  -- Input of the IACK daisy chain.
		DTACK_N     : in std_logic;  -- Data transfer acknowledge
		BERR_N      : in std_logic;  -- Bus error
		DataIn      : in std_logic_vector(15 downto 0); -- For reading the Status I/D from the interrupt generator.
		ValidInts   : in std_logic_vector(7 downto 1); -- Indicates if an interruption is accepted or not accepted by the handler.
													   -- ValidInts(7) ='1'; the handler accepts IRQ7
													   -- ValidInts(7) ='0'; the handler does not accept IRQ7
													   -- ValidInts(1) ='1'; the handler accepts IRQ1
													   -- ValidInts(1) ='0'; the handler does not accept IRQ1
		DTB_Granted : in std_logic; --'1' if the arbitrer grants the DTB to this module.
									-- '0' if the arbitrer does not grant the DTB to this module.
		SelectBits : in Select_8_16Bits; --Select8 --> The handler generates 8-bit interrupt acknowledge cycles.
										 --Select16 --> The handler generates 16-bit interrupt acknowledge cycles.
		BusRelease  : in SelectBusRelease; -- ROAK or RORA. (Only ROAK is implemented at this time).

		-- Outputs
		IACK_N       : out std_logic;-- Interrupt acknowledge. It must be connected to the IACK_IN_N pin of the system controller module.
		IACK_Out_N   : out std_logic;-- Output of the IACK daisy chain.
		
		AS_N         : out std_logic; -- Adress strobe
		DS0_N        : out std_logic; -- Data strobe 0
		DS1_N        : out std_logic; -- Data strobe 1
		DeviceWants_Bus: out std_logic; --'1' if the handler needs to acessing to the DTB.
										 --'0' if the handler does not need to acessing to the DTB.
		Address       : out std_logic_vector(3 downto 1);
		StatusID_irqh       : out std_logic_vector(31 downto 0)
		 ); 
	end component;

Component LED_N2_D6 
port(
    -- Inputs
    CLK         : in std_logic; -- 50 MHz
    Reset_N     : in std_logic; -- asynchronous reset (active low)
	BERR_N      : in std_logic;  -- Bus error
	-- Outputs
	LED_N 		: out std_logic);  --'0' LED is ON, '1' LED is OFF
end component;


component Interrupter
port(
    -- Three State input signals: Address, AS_N, DS0_N and DS1_N. 
    -- Three State output signals: IRQ_N, DTACK_N, DataOut 
 
     -- Inputs
    CLK         : in std_logic; -- 50 MHz
    Reset_N     : in std_logic; -- asynchronous reset (active low)
    IACK_IN_N   : in std_logic;  -- Input of the IACK daisy chain.
    Address     : in std_logic_vector(3 downto 1); -- Interruption level sent by the handler
    AS_N        : in std_logic; -- Address strobe
    DS0_N       : in std_logic; -- Data strobe 0
    DS1_N       : in std_logic; -- Data strobe 1
    
    SelectBits  : in Select_8_16Bits; --Select8 --> The handler generates 8-bit interrupt acknowledge cycles.
                                     --Select16 --> The handler generates 16-bit interrupt acknowledge cycles.
    BusRelease  : in SelectBusRelease; -- ROAK or RORA. (Only ROAK is implemented at this time).
    IntLevel_X  : in std_logic_vector(2 downto 0); -- To indicate the interruption level of this module.
                                                   -- "111" --> IRQ7
                                                   -- "001" --> IRQ1.
                                                   -- "000" is allowed Only while the system is in reset condition. 
                                                   -- If Reset_N = 0 then IntLevel_X may be "000"
                                                   -- If Reset_N = 1, then IntLevel_X must be in range "001" to "111".
    IntRequest  : in std_logic; --'1' To request and interruption.

     -- Outputs
    IRQ_N       : out std_logic_vector(7 downto 1);  --Interrupt request.    
    IACK_OUT_N  : out std_logic; -- Output of the IACK daisy chain
    DTACK_N     : out std_logic;  -- Data transfer acknowledge    
    DataOut     : out std_logic_vector(15 downto 0);  -- For writing the interruption Status I/D.
    Access_DTB  : out std_logic); -- '1' if the Interrupter needs access to the DTB
                                  -- '0' if the interrupter does not need access to the DTB.

end component;

	signal ValidInts  : std_logic_vector(7 downto 1); -- Indicates if an interruption is accepted or not accepted by the handler.
													   -- ValidInts(7) ='1'; the handler accepts IRQ7
													   -- ValidInts(7) ='0'; the handler does not accept IRQ7
													   -- ValidInts(1) ='1'; the handler accepts IRQ1
													   -- ValidInts(1) ='0'; the handler does not accept IRQ1
									-- '0' if the arbiter does not grant the DTB to this module.
	signal SelectBits : Select_8_16Bits; --Select8 --> The handler generates 8-bit interrupt acknowledge cycles.
										 --Select16 --> The handler generates 16-bit interrupt acknowledge cycles.
	signal BusRelease  : SelectBusRelease; -- ROAK or RORA. (Only ROAK is implemented at this time).
	signal IntLevel_X  : std_logic_vector(2 downto 0); -- To indicate the interruption level of this module.
                                                   -- "111" --> IRQ7
                                                   -- "001" --> IRQ1.
                                                   -- "000" is allowed Only while the system is in reset condition. 
                                                   -- If Reset_N = 0 then IntLevel_X may be "000"
                                                   -- If Reset_N = 1, then IntLevel_X must be in range "001" to "111".
	signal IntRequest  : std_logic; --'1' To request and interruption.
	signal dtack_b_irq : std_logic;  -- Data transfer acknowledge    
	signal d_vme_irq   : std_logic_vector(15 downto 0);  -- For writing the interruption Status I/D.
	signal Access_DTB  : std_logic; -- '1' if the Interrupter needs access to the DTB
									-- '0' if the interrupter does not need access to the DTB.
									
	signal Access_DTB_reg  : std_logic;						
									
							  
		-- Outputs
	--signal    IACK_N       : std_logic;-- Interrupt acknowledge. It must be connected to the IACK_IN_N pin of the system controller module.
	--signal    IACK_Out_N   : std_logic;-- Output of the IACK daisy chain.
		
	-- signal    AS_N         : std_logic; -- Address strobe
	-- signal    DS0_N        : std_logic; -- Data strobe 0
	-- signal    DS1_N        : std_logic; -- Data strobe 1
	
	signal    DeviceWants_Bus: std_logic; --'1' if the handler needs to accessing to the DTB.
										  --'0' if the handler does not need to accessing to the DTB.
	signal Address      : std_logic_vector(3 downto 1); 
	signal IACK_IN_N	: std_logic;  -- Input of the IACK daisy chain.
	signal IRQ_N       	: std_logic_vector(7 downto 1);  --Interrupt request.    
	signal DTB_Granted 	: std_logic; --'1' if the arbiter grants the DTB to this module.
									-- '0' if the arbiter does not grant the DTB to this module.

	constant VERSION			: std_logic_vector (31 downto 0):= X"00000004";
	constant REVISON			: std_logic_vector (31 downto 0):= X"A0000003";

	signal	d_out_i: std_logic_vector(15 downto 0);
	signal	ds_b_out_i: std_logic_vector(1 downto 0);
	signal	write_b_out_i: std_logic;
	signal	lword_b_out_i: std_logic;
	signal	dtack_b_out_i: std_logic;
		
	signal	ds_b_in_reg: std_logic_vector(1 downto 0);
	signal	a_out_i: std_logic_vector(23 downto 1);
	signal	am_out_i: std_logic_vector(5 downto 0);
	signal	as_b_out_i: std_logic;
		
	signal	irq_b_out_i: std_logic_vector(7 downto 1); 	
	signal	berr_b_out_i: std_logic;	
	signal	sysfail_b_out_i: std_logic;	
	signal	bclr_b_out_i: std_logic;	
	signal	bbsy_b_out_i: std_logic;	
	signal	br_b_out_i: std_logic_vector(3 downto 0);

	signal data_out_uart: std_logic_vector(31 downto 0);
	signal add_uart: std_logic_vector(31 downto 0);
	
	signal add_m: std_logic_vector(31 downto 0);
	signal add_sl: std_logic_vector(31 downto 0);
	
	
	
	signal d_vme_sl	: std_logic_vector(15 downto 0);
	signal d_vme_m			: std_logic_vector (15 downto 0);
	signal d_vme_m_bblt		: std_logic_vector (15 downto 0);

		
	signal  RnW : std_logic;
	signal  CS_N_UART : std_logic;
	
   signal gnd_vec : std_logic_vector     (31   downto 0);
   signal vcc_vec : std_logic_vector     (31   downto 0);
   signal gnd      : std_logic            := '0';    
   signal vcc      : std_logic            := '1'; 
   
   signal UART_RXDATA      : std_logic            := '1';    
   signal UART_TXDATA      : std_logic            := '1'; 
   
   signal  counter_led : std_logic_vector     (31   downto 0);
   signal LED		: std_logic_vector(4 downto 1);
   signal up,Tcnt	: std_logic;

	signal RD					: std_logic_vector (31 downto 0):= X"00000000";
	signal RD_CTL				: std_logic_vector (31 downto 0);
	signal RD_R					: std_logic_vector (15 downto 0);
	signal RD_PN				: std_logic_vector (31 downto 0);
	signal RD_W					: std_logic_vector (15 downto 0);
	signal RD_DATA_BUS			: std_logic_vector (31 downto 0);
	signal not_RD_nWD_EN		: std_logic;
	signal RAMB_ACCESS_VALID	: ACCESS_VALID_UART_type;
	signal wait_state 			: std_logic;
	signal CS_CTL 			: std_logic;
	signal CS_W 			: std_logic;
	signal CS_W_FIX 		: std_logic;
	signal CS_R 			: std_logic;
	signal CS_R_FIX			: std_logic;
	signal CS_PN 			: std_logic;
	signal CS_N_UART_REG	: std_logic;
	signal add_uart_before 			: std_logic_vector (5 downto 0);
		
	signal end_run 			: std_logic;

	--signal a_vme			: std_logic_vector (31 downto 0);
	--0-23 add init vme
	signal add_vme			: std_logic_vector (31 downto 0);
	signal add_vme_bblt		: std_logic_vector (31 downto 0);
	--28:lword; 25-24:ds; 5-0:am
	signal am_vme			: std_logic_vector (31 downto 0);
	signal eos_width		: std_logic_vector (31 downto 0);
	
	signal run_c_fix		: std_logic_vector (31 downto 0);
	
	signal time_cycle 			: std_logic;
	signal tm_count 			: integer;
	signal wr_pointer			:  std_logic_vector(5 downto 0);
	signal blt_pointer			:  std_logic_vector(6 downto 0);
	signal bblt_pointer			:  std_logic_vector(31 downto 0);
	signal ram_pointer			:  std_logic_vector(6 downto 0);
	signal size_block			:  std_logic_vector(6 downto 0);
	signal start_blt 			: std_logic;
	signal rst_blt_pointer 		: std_logic;
	signal rst_bblt_pointer		: std_logic;
	signal rst_bblt_pointer_reg : std_logic;
	signal rst_bblt_pointer_fd	: std_logic;
	signal start_blt_fd 		: std_logic;
	signal start_blt_reg 		: std_logic;
	signal MSW_rst 				: std_logic;
		
	signal ctl_wen_in 			: std_logic;
	signal ctl_data_in			:  std_logic_vector(31 downto 0);
	signal ctl_data_out			:  std_logic_vector(31 downto 0);
	
	signal CS_W_B 		: std_logic;
	signal RnW_B 		: std_logic;
	signal brqg_reg		:  std_logic_vector(31 downto 0);
	
	signal RnW_B_sl 		: std_logic;
	signal blkb_sl	 		: std_logic;
	signal blkb_m	 		: std_logic;
	
	signal bus_grant	: std_logic;
	signal bus_rq 		: std_logic;
				
	type state_type_cycle is (stc_wait, stc_wait_cmd, stc_br, --stc_prq, 
	stc_rst, stc_rst_a, stc_int, stc_int_h, 
	stc_mui, stc_m, stc_m_addresing, stc_m_dating, stc_m_ending,
	stc_m_bblt, stc_m_addresing_bblt, stc_m_dating_bblt, stc_m_ending_bblt,	
	stc_sl, stc_sl_addresing, stc_sl_dating, stc_sl_ending, stc_error_m, stc_error_sl, stc_error_irq, stc_error_br, stc_end_run); 
	signal state_cycle, next_state_cycle : state_type_cycle; 
			
	type t_pn_reg is array(2 downto 0) of std_logic_vector(31 downto 0);
	signal pn_data 		: t_pn_reg;
	signal pn_add 		: t_pn_reg;
	signal pn_ctrl 		: t_pn_reg;
	signal pn_spare 	: t_pn_reg;
	
	signal pn_data_out 		: t_pn_reg;
	signal pn_add_out 		: t_pn_reg;
	signal pn_ctrl_out 		: t_pn_reg;
		
	
	signal s_con: std_logic;
	--signal bgout_b    :   std_logic_vector(3 downto 0);  	-- VMEbus Bus Grant Output
		
	signal error_reg		:  std_logic_vector(31 downto 0);
	signal status_error		:  std_logic_vector(31 downto 0);
	signal error_flag		: std_logic;
	
	signal lock_pll  : std_logic;
	signal lock_pll_clkin  : std_logic;
	signal reset_vme : std_logic; 
	signal Reset_n : std_logic;	
	signal Reset_n_p : std_logic;	
	
	signal tm_count_rst		: integer;	
	signal time_rst 		: std_logic;
	signal tm_count_rst_a	: integer;	
	signal time_rst_a 		: std_logic;
	signal reset_reg		:  std_logic_vector(31 downto 0);
	
	constant ClkFreq	: integer := 20000000; --Hz
	constant T_COUNT	: integer := 500; --ms --500 --1 to -- ONLY FOR SIMULATION:!!!!!!! (The value for simulation is 1).
											-- The value to generate the *.pdb is 500.															
	--constant PULSE_EOS	: integer := 5;      --ms --5 --1 to -- ONLY FOR SIMULATION:!!!!!!!!	
	constant BR			: integer := 9600;      --ms --9600 --100000 to -- ONLY FOR SIMULATION:!!!!!!!!	(The value for simulation is 100000).
											-- The value to generate the *.pdb is 9600.															
	--constant T_PULSE_EOS	: integer := (ClkFreq*PULSE_EOS)/1000;	--FEQ in Hertz & T_PULSE_EOS in ms.
	constant TM_RESET_MAX	: integer := (ClkFreq*T_COUNT)/1000;	--FEQ in Hertz & T_COUNT in ms.
	
	constant TM_AS	: integer := 1; 
	constant TM_AS_BBLT	: integer := 5; 
	constant TM_DS	: integer := 2; 
	constant TM_BC	: integer := 10; 
	constant TM_TIMOUT_ERROR	: integer := 5000; 

		
	signal CLK 	: std_logic;
		
	--Added Add slave start & end
	signal slave_add_start	:  std_logic_vector(31 downto 0);
	signal slave_add_end	:  std_logic_vector(31 downto 0);
	
	signal int_handler_reg	:  std_logic_vector(31 downto 0);
	signal interrupter_reg	:  std_logic_vector(31 downto 0);
	signal mui_reg			:  std_logic_vector(31 downto 0);
	signal eos_fq			:  std_logic_vector(31 downto 0);
	signal tm_count_eos		:  std_logic_vector(31 downto 0);
	
	signal status_id_irqh	:  std_logic_vector(31 downto 0);
	
	signal dct_reg			:  std_logic_vector(31 downto 0);
	
	signal IntRequest_fl 		: std_logic;
	signal IntRequest_reg 		: std_logic;
	signal iack_b_int_h 		: std_logic;
	signal iack_b_int_h_out 	: std_logic;
	signal iack_b_int	 		: std_logic;
	signal end_int	 		: std_logic;
	
	signal dtack_b_in_reg	 		: std_logic;
	signal dtack_b_in_flt	 		: std_logic;
		
	signal eos_b_i 			: std_logic;
	signal en_count_eos 	: std_logic;
	signal loe_i 			: std_logic;
	signal ldsoe_i	 		: std_logic;
	signal scon_b_i	 		: std_logic;
	signal as_b_irq	 		: std_logic;
	signal ds_b_irq			: std_logic_vector(1 downto 0);
	signal a_b_irq			: std_logic_vector(3 downto 1);
	
	signal state_reg		: std_logic_vector(31 downto 0);
	
	signal SBBLT		: std_logic_vector(31 downto 0);
	signal EBBLT		: std_logic_vector(31 downto 0);
	signal DBBLT		: std_logic_vector(31 downto 0);
	
	signal iack_b_i	 		: std_logic;
	signal bgin_b_i			: std_logic_vector(3 downto 0);
	signal as_b_in_reg	 	: std_logic;
	signal am_in_reg		: std_logic_vector(5 downto 0);
	signal a_in_reg			: std_logic_vector(23 downto 1);
	signal bgin_b_metro_i	: std_logic_vector(3 downto 0);
	signal d_in_reg			: std_logic_vector(15 downto 0);
	
	signal to_counter		: std_logic_vector(15 downto 0);
	constant TO_MAX			: std_logic_vector(15 downto 0) := X"0280";  -- 32us (50ns cycle)
	
	type state_type_eos is (st_eos_wait, st_eos_active, st_eos_noactive, st_eos_error); 
   signal state_eos, next_state_eos : state_type_eos; 
   
   signal dtack_event_flt	 	: std_logic;
   signal dtack_b_in_flt2	 	: std_logic;
   signal dtack_b_in_reg2	 	: std_logic;
   signal dtack_b_out_flt	 	: std_logic;
   signal dtack_b_out_reg	 	: std_logic;
   signal ds_event	 		: std_logic;
   signal ds_event_reg	 	: std_logic;
   signal ds_event_flt	 	: std_logic;
   signal as_event	 		: std_logic;
   signal as_event_reg	 		: std_logic;
   signal as_event_flt	 		: std_logic;
   
   signal ds_b_out_reg		: std_logic_vector(1 downto 0);
   signal as_b_out_reg		: std_logic;
   signal blkb_m_flt		: std_logic := '0';
   signal reg_blkb_m		: std_logic := '1';
   signal flp			: std_logic := '0';
   signal flp_reg		: std_logic := '0';
   signal flp_event		: std_logic := '0';
   signal eos_enable_buffer		: std_logic := '0';

begin


gnd_vec	<= X"00000000";
vcc_vec	<= X"FFFFFFFF";
gnd    	<= '0';
vcc    	<= '1';
---LED_N(4) <= '1';
eos_b <= eos_b_i and not run_c_fix(6);


-- FIX PCB BBUSY & DACTK ISSUE 04/09/2014

	ldir 		<= vcc;
	spare_A(6)  <= dtack_b_out_i;
	spare_A(7)  <= berr_b_out_i;

	--LED_N(2)  		<= berr_b_in;
	
	uut_LED_D6: LED_N2_D6 port map(
    CLK         => CLK, 
    Reset_N     => Reset_N,
	BERR_N      => berr_b_in,
	LED_N 		=> LED_N(2));  --'0' LED is ON, '1' LED is OFF
	
	
	LED_N(3) 		<= not run_c_fix(4);
	
	dcfail_b		<=  not mui_reg(1);
	
	sysfail_b_out_i	<=  not mui_reg(0);
	ldsoe 			<=  not mui_reg(0)  and ldsoe_i;
	-- Change 17-4-2015
	-- Original sentence: loe <= (not mui_reg(1)) and loe_i and not(run_c_fix(7));
	--New Sentence:
	loe 			<= eos_enable_buffer and (not mui_reg(1)) and loe_i and not(run_c_fix(7)) and berr_b_out_i and dtack_b_irq; --MODIFICATION INT DTACK 25/01/2016
	
-- FIX REG INPUT ADDRESS 28/01/2015

	REG_INPUT_A: process (CLK, Reset_n)
	begin
		if (Reset_n = '0') then
			as_b_in_reg <= '1';
			am_in_reg 	<= (others=>'1');
			a_in_reg 	<= (others=>'1');
		elsif (CLK'event and CLK = '1') then
			as_b_in_reg <= as_b_in;
			am_in_reg 	<= am_in;
			a_in_reg 	<= a_in;
		end if;
	end process;

	
USER_UART_P: USER_UART  
	generic map (
		ClkFreq				  =>      ClkFreq,	-- clock freq (Hz)
		BaudRate			  =>      BR,		
		bit8				  =>      '1',		-- byte = '1' 
		parity_en			  =>      '1',		-- enable = '1' 
		odd_n_even			  =>      '0',		
		ADDR_NIBBLES		  =>      8,	
		DATA_NIBBLES		  =>      8,		
		command_echo		  =>     true,		
		timeout_mult		  =>      8		
	)
	port map (
		n_RESET        		  =>     RESET_N,
		sys_clk               =>     CLK,
		enable                =>     vcc,
		-- i/f CORE 0P
		RXDATA         		  =>     UART_RXDATA,
		TXDATA         		  =>     UART_TXDATA,
		-- i/f MASTER BUS
		ADDRESS_UART   		  =>     add_uart, 			    -- Address bus
		RD_DATA_BUS_UART      =>     RD_DATA_BUS, 			-- input data bus
		WD_DATA_BUS_UART  	  =>     data_out_uart,			-- Output data bus
		RD_nWD_EN_UART		  =>     RnW,              		-- Read(active high)/write(active low) enable
		nCS_UART			  =>     CS_N_UART,				-- Operation request(active low)
		ACCESS_VALID_UART	  =>     RAMB_ACCESS_VALID,		-- Access valid acknowledge
		WAIT_UP_ADC_DATA	  =>     vcc,			-- Ready data in the ADC
		--	error flags
		UART_BIT_I_S		  =>     open,					-- module in invalid state
		UART_BIT_W_D		  =>     open,					-- module is STOPED
		-- debug outputs
		RXRDY_o 			  =>     open,
		TXRDY_o 			  =>     open,
		-- trigger port
		TRIGGER_IN 			  =>     gnd_vec,
		TRIGGER_IN_MASK 	  =>     gnd_vec 
	);
	
TX_RS232_FPGA <= UART_TXDATA;
UART_RXDATA   <= RX_RS232_FPGA;


ACCESS_VALID_output : process(CLK,Reset_n)
begin
	if Reset_n='0' then
		RAMB_ACCESS_VALID <= DEV_FREE;
		wait_state <= '0';
		RD_DATA_BUS <= (others=>'0');
	elsif CLK'event and CLK='1' then
		if RAMB_ACCESS_VALID = DEV_BUSY then
			if wait_state = '0' then
				wait_state <= '1';
			else
				-- RAM ensures DOUTB valid on previous falling edge
				RD_DATA_BUS <= RD;
				RAMB_ACCESS_VALID <= DEV_DATA_OK;
			end if;
		elsif CS_N_UART_REG='0' and RAMB_ACCESS_VALID /= DEV_DATA_OK then
			RAMB_ACCESS_VALID <= DEV_BUSY;
		elsif CS_N_UART_REG='1' then
			RAMB_ACCESS_VALID <= DEV_FREE;
			wait_state <= '0';
		end if;
	end if;
end process;


CS_REG: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				CS_N_UART_REG<= '1';
			elsif (CLK'event and CLK = '1') then
				CS_N_UART_REG <= CS_N_UART;
			end if;
		end process;


		

RAM_CTL: RAM  
    port map( 
		DINA 		=> data_out_uart,
		DOUTA 		=> RD_CTL,
		ADDRA		=> add_uart(5 downto 0),
		RWA			=> RnW,
		BLKA		=> CS_CTL,
		
		DINB 		=> ctl_data_in,
		DOUTB 		=> ctl_data_out,
		ADDRB		=> wr_pointer, 
		RWB			=> ctl_wen_in,
	
		BLKB		=> gnd,
		CLKAB		=> CLK,
        
		RESET		=> Reset_n	
		) ;

		
RAM_RW_MASTER: RAM16  
    port map( 
		DINA 		=> data_out_uart(15 downto 0),
		DOUTA 		=> RD_W,
		ADDRA		=> add_m(6 downto 0),
	 	RWA			=> RnW,
		BLKA		=> CS_W_FIX,
	
		
		DINB 		=> d_in_reg,
		DOUTB 		=> d_vme_m,
		ADDRB		=> ram_pointer,
		RWB			=> RnW_B,
		BLKB		=> blkb_m,
		
		CLKAB		=> CLK,
        RESET		=> Reset_n	
		) ;
		
		CS_W_FIX <= CS_W when blkb_m = '1' else '1';
		
		--RnW_B  <= not run_c_fix(3) or dtack_b_in_flt;
		RnW_B  <= not run_c_fix(3) or dtack_b_in_reg;
		add_m <= add_uart - X"00000040";
		
		REG_DATA_IN_P: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				d_in_reg <= (others=>'0');
			elsif (CLK'event and CLK = '1') then
				d_in_reg <= d_in;
			end if;
		end process;
		
		--DTACK EVENT
		
		REG_DTACK_IN: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				dtack_b_in_reg<= '1';
			elsif (CLK'event and CLK = '1') then
				dtack_b_in_reg <= dtack_b_in;
			end if;
		end process;
		
		dtack_b_in_flt <= not(not dtack_b_in and dtack_b_in_reg);
		
				
		REG_DTACK_OUT: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				dtack_b_out_reg<= '1';
			elsif (CLK'event and CLK = '1') then
				dtack_b_out_reg <= dtack_b_out_i;
			end if;
		end process;
		
		REG_DTACK_IN2: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				dtack_b_in_reg2<= '1';
			elsif (CLK'event and CLK = '1') then
				dtack_b_in_reg2 <= dtack_b_in_reg;
			end if;
		end process;
		
		dtack_b_in_flt2 <= not(not dtack_b_in_reg and dtack_b_in_reg2);
		
		dtack_b_out_flt <= not(not dtack_b_out_i and dtack_b_out_reg);
		
		dtack_event_flt <= not(dtack_b_out_flt and dtack_b_in_flt2);
		
		
		
		
	--DS EVENT
	
		REG_DS_IN: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				ds_b_in_reg<= "11";
			elsif (CLK'event and CLK = '1') then
				ds_b_in_reg <= ds_b_in;
			end if;
		end process;
		
		-- REG_DS_OUT: process (CLK, Reset_n)
		-- begin
			-- if (Reset_n = '0') then
				-- ds_b_out_reg<= "11";
			-- elsif (CLK'event and CLK = '1') then
				-- ds_b_out_reg <= ds_b_out_i;
			-- end if;
		-- end process;
		
		
		ds_event <= not (ds_b_out_i(0) and ds_b_out_i(1) and ds_b_in_reg(0) and ds_b_in_reg(1));
		
		REG_DS_EVENT: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				ds_event_reg<= '0';
			elsif (CLK'event and CLK = '1') then
				ds_event_reg <= ds_event;
			end if;
		end process;
		
		ds_event_flt <= ds_event and not ds_event_reg;
		
	--AS EVENT	
		
		-- REG_AS_OUT: process (CLK, Reset_n)
		-- begin
			-- if (Reset_n = '0') then
				-- as_b_out_reg<= '1';
			-- elsif (CLK'event and CLK = '1') then
				-- as_b_out_reg <= as_b_out_i;
			-- end if;
		-- end process;
		
		as_event <= not (as_b_out_i and as_b_in_reg);
		
		REG_AS_EVENT: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				as_event_reg<= '0';
			elsif (CLK'event and CLK = '1') then
				as_event_reg <= as_event;
			end if;
		end process;
		
		as_event_flt <= as_event and not as_event_reg;
	

	RAM_RW_SLAVE: RAM16  
    port map( 
		DINA 		=> data_out_uart(15 downto 0),
		DOUTA 		=> RD_R,
		BLKA		=> CS_R,
		ADDRA		=> add_sl(6 downto 0),
		RWA			=> RnW,
		
		DINB 		=> d_in_reg,
		DOUTB 		=> d_vme_sl,
		ADDRB		=> ram_pointer, 
		RWB			=> RnW_B_sl,
		BLKB		=> blkb_sl,
		
		CLKAB		=> CLK,
        RESET		=> Reset_n	
		) ;
		
		CS_R_FIX <= CS_R when blkb_sl = '1' else '1';

		
		add_sl <= add_uart - X"000000C0";
		ram_pointer  <= blt_pointer;
	
--DECODIFICATION UART ACCESS

	DECO_UART_ACCES_MUX: process (CLK, Reset_n)
   begin
		if (Reset_n = '0') then
    			RD <= X"00000000";	
				CS_CTL 	<= '1';
				CS_W 	<= '1';	
				CS_R 	<= '1';	
				CS_PN 	<= '1';	
   
		elsif (CLK'event and CLK = '1') then
			if add_uart(9 downto 6) = "000" then
				RD 		<= RD_CTL;	
				CS_CTL 	<= CS_N_UART;
				CS_W 	<= '1';	
				CS_R 	<= '1';	
				CS_PN 	<= '1';	
			
			elsif (add_uart(9 downto 6) = "001") or (add_uart(9 downto 6) = "010") then
				RD 		<= X"0000" & RD_W;	
				CS_CTL 	<= '1';
				CS_W 	<= CS_N_UART;	
				CS_R 	<= '1';	
				CS_PN 	<= '1';		
			
			elsif (add_uart(9 downto 6) = "011") or (add_uart(9 downto 6) = "100") then
				RD 	<=  X"0000" & RD_R;	
				CS_CTL 	<= '1';
				CS_W 	<= '1';	
				CS_R 	<= CS_N_UART;	
				CS_PN 	<= '1';		
			
			elsif add_uart(9 downto 6) = "101" then
				RD <= RD_PN;
				CS_CTL 	<= '1';
				CS_W 	<= '1';	
				CS_R 	<= '1';	
				CS_PN 	<= CS_N_UART;
			
			end if;
		end if;		
		 
   end process;
	
	
--FSM CONTROL CYCLES

SYNC_PROC_CYCLES: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
				state_cycle<= stc_wait;
			elsif (CLK'event and CLK = '1') then
				state_cycle <= next_state_cycle;
			end if;
		end process;
  
NEXT_STATE_DECODE_CYCLES: process (state_cycle, run_c_fix, end_int, tm_count, dtack_b_in_reg, Reset_n_p, blt_pointer, ds_b_in_reg, as_b_in_reg, reset_reg, tm_count_rst, tm_count_rst_a, 
	a_in_reg, slave_add_start, slave_add_end, DeviceWants_Bus, 
	am_in_reg, bus_grant, size_block, eos_width, tm_count_eos, 
	bblt_pointer, EBBLT)
   begin
      --declare default state for next_state to avoid latches
      next_state_cycle <= state_cycle;  --default is to stay in current state
     
     case (state_cycle) is
	 
	 	when stc_wait =>
				next_state_cycle <= stc_rst;
		
		when stc_wait_cmd =>
			if run_c_fix(1 downto 0) = "01" or  run_c_fix(1 downto 0) = "10" or run_c_fix(2) = '1' then 
				next_state_cycle <= stc_br;
			elsif (run_c_fix(4) = '1') and (as_b_in_reg = '0') and (am_in_reg = slave_add_start(29 downto 24)) and ((a_in_reg>=slave_add_start(23 downto 1)) 
			and (a_in_reg<=slave_add_end(23 downto 1))) then --add slave control 17/09/2014
				next_state_cycle <= stc_sl;
			-- elsif run_c_fix(2) = '1' then 
				-- next_state_cycle <= stc_prq;
			elsif reset_reg = X"FFFFFFFF" then 
				next_state_cycle <= stc_rst;
			elsif reset_reg = X"AAAAAAAA" or (Reset_n_p = '0') then --RESET FIX 09/12/2014
				next_state_cycle <= stc_rst_a;
			elsif run_c_fix(5) = '1' then --INT ADDED 05/11/2014
				next_state_cycle <= stc_int;
			 elsif DeviceWants_Bus = '1' and Int_Handler_Reg(12) = '1' then  --INT ADDED 09/12/2014
				 next_state_cycle <= stc_int_h;
			elsif run_c_fix(6) = '1' then --MUI 12/12/2014
				next_state_cycle <= stc_mui;	
			-- elsif eos_fq > (eos_width+2)	then 	--EOS FQ ADDED 30/10/2015
					-- next_state_cycle <= stc_mui_eos_f_A;
			end if;
					
		--MASTER BUS REQUEST 	
					
		when stc_br =>
			if bus_grant = '1' then 
				if run_c_fix(1 downto 0) = "01" or  run_c_fix(1 downto 0) = "10" then
					next_state_cycle <= stc_m;
				else 
					next_state_cycle <= stc_m_bblt;
				end if;
			elsif  tm_count >= TM_TIMOUT_ERROR then 
					next_state_cycle <= stc_error_br;
			end if;
			
		--PINOUT USER REQUEST	
		
		--when stc_prq =>
			--next_state_cycle <= stc_wait_cmd;
			
		
		--RESET USER REQUEST	
		
		when stc_rst =>
			if (tm_count_rst >= TM_RESET_MAX) and (reset_reg = X"00000000") then  
				next_state_cycle <= stc_wait_cmd;
			end if;
		
		--RESET A USER REQUEST	
		
		when stc_rst_a =>
			if (tm_count_rst_a >= TM_RESET_MAX) and (reset_reg = X"00000000") then  
				next_state_cycle <= stc_wait_cmd;
			end if;
		
		--MASTER RW/ S/BLT
		when stc_m =>
			if tm_count >= TM_AS then 
				next_state_cycle <= stc_m_addresing;
			end if;
		
			
			when stc_m_addresing =>
				if tm_count >= TM_DS then 
					next_state_cycle <= stc_m_dating;
				end if;
						
			when stc_m_dating =>
				if dtack_b_in_reg = '0' then 
					next_state_cycle <= stc_m_ending;
				elsif  tm_count >= TM_TIMOUT_ERROR then 
					next_state_cycle <= stc_error_m;
				end if;
				
			
			when stc_m_ending =>
				if dtack_b_in_reg = '1' then 
					if run_c_fix(1) = '1' then  
						if (blt_pointer >=size_block) then
							next_state_cycle <= stc_end_run;
						else		
							next_state_cycle <= stc_m_addresing;
						end if;
					else
						next_state_cycle <= stc_end_run;
					end if;
				elsif  tm_count >= TM_TIMOUT_ERROR then 
					next_state_cycle <= stc_error_m;
				end if;
		
		--MASTER RW/ BBLT
		when stc_m_bblt =>
			if tm_count >= TM_AS_BBLT then 
				next_state_cycle <= stc_m_addresing_bblt;
			end if;
		
			
			when stc_m_addresing_bblt =>
				if tm_count >= TM_DS then 
					next_state_cycle <= stc_m_dating_bblt;
				end if;
						
			when stc_m_dating_bblt =>
				if dtack_b_in_reg = '0' then 
					next_state_cycle <= stc_m_ending_bblt;
				elsif  tm_count >= TM_TIMOUT_ERROR then 
					next_state_cycle <= stc_error_m;
				end if;
				
			
			when stc_m_ending_bblt =>
				if dtack_b_in_reg = '1' then 
						if (blt_pointer >=size_block) then
							if (add_vme_bblt >= EBBLT) then
								next_state_cycle <= stc_end_run;
							else
								next_state_cycle <= stc_m_bblt;
							end if;
						else
							next_state_cycle <= stc_m_addresing_bblt;
						end if;
				elsif  tm_count >= TM_TIMOUT_ERROR then 
					next_state_cycle <= stc_error_m;
				end if;

	
	--	--SLAVE RW/ S/BLT
		when stc_sl =>
			if tm_count >= TM_DS then 
				next_state_cycle <= stc_sl_addresing;
			end if;
			
			when stc_sl_addresing =>
				if ds_b_in_reg(0) = '0' or ds_b_in_reg(1) = '0' then 
					next_state_cycle <= stc_sl_dating;
				elsif  tm_count >= TM_TIMOUT_ERROR then 
					next_state_cycle <= stc_error_sl;
				end if;
			
			when stc_sl_dating =>
				if (ds_b_in_reg= "11") then 
					next_state_cycle <= stc_sl_ending;
				elsif  tm_count >= TM_TIMOUT_ERROR then 
					next_state_cycle <= stc_error_sl;
				end if;
						
			
			when stc_sl_ending =>
				if (as_b_in_reg = '1') then 
					next_state_cycle <= stc_end_run;
				elsif ds_b_in_reg(0) = '0' or ds_b_in_reg(1) = '0' then 
					next_state_cycle <= stc_sl_addresing;
				elsif  tm_count >= TM_TIMOUT_ERROR then 
					next_state_cycle <= stc_error_sl;
				end if;
		
	
		--INT ADDED REQUEST 03/11/2014
		when stc_int =>
	
				if (end_int = '1') then  --INT ADDED 09/12/2014then 
					next_state_cycle <= stc_end_run;
				elsif tm_count >= TM_TIMOUT_ERROR then 	
					next_state_cycle <= stc_error_irq;
				end if;
		
		--INT ADDED HANDLER 09/12/2014
		when stc_int_h =>
	
				if (DeviceWants_Bus = '0') then  --INT ADDED 09/12/2014then 
					next_state_cycle <= stc_end_run;
				elsif tm_count >= 2000 then 	
						next_state_cycle <= stc_error_irq;
				end if;
				
		when stc_mui =>
				
				if tm_count >= eos_width	then 	
						next_state_cycle <= stc_end_run;
				end if;
				
		-- when stc_mui_eos_f_A => 		--EOS FQ ADDED 30/10/2015
				-- if eos_fq = X"00000000"	then 	
					-- next_state_cycle <= stc_end_run;
				-- elsif tm_count_eos >= eos_width	then 
					-- next_state_cycle <= stc_mui_eos_f_B;
				-- end if;		
		
		-- when stc_mui_eos_f_B => 		--EOS FQ ADDED 30/10/2015
				-- if eos_fq = X"00000000"	then 	
					-- next_state_cycle <= stc_end_run;
				-- elsif tm_count_eos >= eos_fq	then 
					-- next_state_cycle <= stc_mui_eos_f_A;
				--end if;				
	
		--END RUN CYCLES
		when stc_end_run =>
			if run_c_fix(2 downto 0) = "000" and run_c_fix(5) = '0' and run_c_fix(6) = '0' then 
				next_state_cycle <= stc_wait_cmd;
			elsif  tm_count >= TM_TIMOUT_ERROR then 
				next_state_cycle <= stc_error_m;
			end if;	
			
		--ERROR
		when stc_error_m =>
			next_state_cycle <= stc_end_run;	
		when stc_error_sl =>
			next_state_cycle <= stc_end_run;	
		when stc_error_irq =>
			next_state_cycle <= stc_end_run;
		when stc_error_br =>
			next_state_cycle <= stc_end_run;
		
		when others =>
			next_state_cycle <= stc_wait;	
			
	 end case;      
	 
   end process;
    
    OUPUT_DECODE_RESET_CYCLES: process (state_cycle, run_c_fix, dtack_b_in_reg, ds_b_in_reg, write_b_in, d_vme_sl, bus_grant, tm_count, 
	as_b_irq, ds_b_irq, a_b_irq, DeviceWants_Bus,
	tm_count_rst, tm_count_rst_a, d_vme_irq, dtack_b_irq, mui_reg, am_vme, access_dtb, add_vme, d_vme_m)
   begin
    case (state_cycle) is
	
		when stc_wait =>
		
		 ldoe     	<= '1';       -- Latch Outgoing VMEbus Data
		d_out_i       <= X"0000";   -- VMEbus Data
		 laoe       <= '1';       -- Latch Outgoing VMEbus Address
		a_out_i       <= (others => '0');   -- VMEbus Address
		 lamoe      <= '1';       -- Latch Outgoing VMEbus Address Modifier
		am_out_i     <= "000000";    -- VMEbus Address Modifier
		
		 ldsoe_i       <= '1';      -- VMEbus Data S O Enable
		--ldsoe_i       <= '0'; 
		as_b_out_i    <= '1';       -- VMEbus Address Strobe
		ds_b_out_i    <= "11";  	  -- VMEbus DataStrobe
		write_b_out_i <= '1';       -- VMEbus Data Direction
		lword_b_out_i <= '1';       -- VMEbus Long-Word
		time_cycle <= '0';
		end_run    <= '0';		
		start_blt <= '0';	
		rst_blt_pointer  <= '0';	
		rst_bblt_pointer  <= '0';
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '0';
		
		bus_rq <= '1';

		 lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '1';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';
		lirqo <= '1';		
		
		loe_i <= vcc;

	
		spare_A (11 downto 8) <= "ZZZZ"; 
		state_reg  <= X"BB000000";
       
		when stc_wait_cmd =>
		
		 ldoe     	<= '1';       -- Latch Outgoing VMEbus Data
		d_out_i       <= X"0000";   -- VMEbus Data
		 laoe       <= '1';       -- Latch Outgoing VMEbus Address
		a_out_i       <= (others => '0');   -- VMEbus Address
		 lamoe      <= '1';       -- Latch Outgoing VMEbus Address Modifier
		am_out_i     <= "000000";    -- VMEbus Address Modifier
		
		 ldsoe_i       <= '1';      -- VMEbus Data S O Enable
		--ldsoe_i       <= '0'; 
		as_b_out_i    <= '1';       -- VMEbus Address Strobe
		ds_b_out_i    <= "11";  	  -- VMEbus DataStrobe
		write_b_out_i <= '1';       -- VMEbus Data Direction
		lword_b_out_i <= '1';       -- VMEbus Long-Word
		time_cycle <= '0';
		end_run    <= '0';		
		start_blt <= '0';	
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '0';
		
		bus_rq <= '1';

		 lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';
		lirqo <= '1';		
		
		loe_i <= vcc;--gnd;
		
	
		spare_A (11 downto 8) <= "ZZZZ"; 
		state_reg  <= X"00000000";
		
		when stc_br =>
		
		 ldoe     	<= '1';       
		d_out_i       <= X"0000";   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		ldsoe_i       <= '1';     
		-- ldsoe_i       <= '0'; 
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';      
		lword_b_out_i <= '1';       
		
		if bus_grant = '1' then 		
			time_cycle <= '0';
		else
			time_cycle <= '1';
		end if;
					
		end_run    <= '0';		
		start_blt <= '0';	
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '0';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';
		lirqo <= '1';			
		
		loe_i <= vcc;--gnd;
		
	
		spare_A (11 downto 8) <= "ZZZZ"; 
		state_reg  <= X"00000001";		
			
--		when stc_prq =>
--		
--		 ldoe     	<= '1';      
--		d_out_i       <= X"0000"; 
--		 laoe       <= '1';       
--		a_out_i       <= (others => '0');  
--		 lamoe      <= '1';       
--		am_out_i     <= "000000";   
--		
--		ldsoe_i       <= '1';      
--		--ldsoe_i       <= '0'; 
--		as_b_out_i    <= '1';       
--		ds_b_out_i    <= "11";  	  
--		write_b_out_i <= '1';       
--		lword_b_out_i <= '1';       
--		time_cycle <= '1';
--		end_run    <= '1';		
--		start_blt <= '0';	
--		rst_blt_pointer  <= '0';	
-- 		rst_bblt_pointer  <= '0';
--		MSW_rst	 <= '1';
--		blkb_sl	<= '1';
--		blkb_m	<= '1';
--		RnW_B_sl  <=  '1';
--		
--		dtack_b_out_i <= '1';
--		LED_N(1) <= '1';
--		
--		bus_rq <= '1';	
--		
--		  lbroe    <= '1';
--		bbsy_b_out_i<= '1';
--		
--		status_error<= X"00000000"; 	
--		error_flag	<= '0';
--		time_rst	<= '0';
--		time_rst_a	<= '0';	
--		
--		IntRequest <= '0';
--		lirqo <= '1';		
--		
--		loe_i <= vcc;--gnd;
--
--	
--		spare_A (11 downto 8) <= "ZZZZ"; 	
--		state_reg  <= X"00000002";
--
		
		
		when stc_rst =>
		
		 ldoe     	<= '1';      
		d_out_i       <= X"0000"; 
		 laoe       <= '1';       
		a_out_i       <= (others => '0');  
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		ldsoe_i       <= '1';      
		-- ldsoe_i       <= '0'; 
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '0';
		
		if tm_count_rst >= TM_RESET_MAX then
			end_run    <= '1';
		else
			end_run    <= '0';
		end if;
		
		start_blt <= '0';	
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '1';	
		time_rst_a	<= '0';	
		
		IntRequest <= '0';
		lirqo <= '1';		

		loe_i <= vcc;--gnd;
		
	
		spare_A (11 downto 8) <= "ZZZZ"; 		
		state_reg  <= X"000000A2";		

		
		when stc_rst_a =>
		
		 ldoe     	<= '1';      
		d_out_i       <= X"0000"; 
		 laoe       <= '1';       
		a_out_i       <= (others => '0');  
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		ldsoe_i       <= '1';      
		-- ldsoe_i       <= '0'; 
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '0';
		
		if tm_count_rst_a >= TM_RESET_MAX then
			end_run    <= '1';
		else
			end_run    <= '0';
		end if;
		
		start_blt <= '0';	
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';	
		time_rst_a	<= '1';	
		
		IntRequest <= '0';	
		lirqo <= '1';	
		
		loe_i <= vcc;--gnd;
		
	
		spare_A (11 downto 8) <= "ZZZZ";
		state_reg  <= X"00000003";		
	
						
		--MASTER /RW BLT & SINGLE
		when stc_m =>
			 ldoe     	<= '1';       
			d_out_i       <= X"0000";   
			 laoe       <= '0';       
			a_out_i       <= add_vme(23 downto 1);   
			 lamoe      <= '0';      
			am_out_i     <= am_vme(5 downto 0); 
			
			 ldsoe_i       <= '0';     
			as_b_out_i    <= '1';      
			ds_b_out_i    <= "11";  	
			write_b_out_i <= '1';      
			lword_b_out_i <= '1';--am_vme(28);     
			time_cycle <= '1';
			end_run    <= '0';	
			start_blt <= '0';	
			rst_blt_pointer  <= '0';	
			rst_bblt_pointer  <= '0';
			MSW_rst	 <= '1';
			blkb_sl	<= '1';
			blkb_m	<= '0';	
			RnW_B_sl  <=  '1';
			
			dtack_b_out_i <= '1';
			LED_N(1) <= '1';
			
			bus_rq <= '1';	
			
			 lbroe    <= '0';
			bbsy_b_out_i<= '0';
			
			status_error<= X"00000000"; 	
			error_flag	<= '0';
			time_rst	<= '0';
			time_rst_a	<= '0';	
			
			IntRequest <= '0';	
			lirqo <= '1';	
			
			loe_i <= vcc;--gnd;
			
		
			spare_A (11 downto 8) <= "ZZZZ"; 
			state_reg  <= X"00000004";			
			
		
			
			when stc_m_addresing =>
				 ldoe     	<= run_c_fix(3); --READ/WRITE
				d_out_i       <= d_vme_m;   
				 
				 laoe       <= '0';       
				a_out_i       <= add_vme(23 downto 1);   
				 lamoe      <= '0';      
				am_out_i     <= am_vme(5 downto 0);   
				
				 ldsoe_i       <= '0';     
				as_b_out_i    <= '0';  
				
				-- if (run_c_fix(1)  = '1') then -- BLT MODE FIX ADDED 04/02/2014 
					-- ds_b_out_i    <= "00"; 
				-- else
					-- if am_vme(28) = '0' then --8 BITS MODE SINGLE
						-- ds_b_out_i    <= (add_vme(0) & not add_vme(0)); 
					-- else
						-- ds_b_out_i    <= "00"; 
					-- end if;
				-- end if;
				ds_b_out_i    <= "11"; 
				
				write_b_out_i <= run_c_fix(3); --READ/WRITE        
				lword_b_out_i <= '1';--am_vme(28);     
				time_cycle <= '1';		
				end_run    <= '0';	
				start_blt <= '0';
				rst_blt_pointer  <= '0';
				rst_bblt_pointer  <= '0';				
				MSW_rst	 <= '0';
				blkb_sl	<= '1';
				blkb_m	<= '0';	
				RnW_B_sl  <=  '1';

				dtack_b_out_i <= '1';
				LED_N(1) <= '1';
				
				bus_rq <= '1';	
				
				  lbroe    <= '0';
				bbsy_b_out_i<= '0';
				
				status_error<= X"00000000"; 	
				error_flag	<= '0';
				time_rst	<= '0';
				time_rst_a	<= '0';	
				
				IntRequest <= '0';	
				lirqo <= '1';

				loe_i <= vcc;--gnd;
				
	
				spare_A (11 downto 8) <= "ZZZZ"; 	
				state_reg  <= X"00000005";
		
			when stc_m_dating =>
				 ldoe     	<= run_c_fix(3); --READ/WRITE     
				d_out_i       <= d_vme_m;   
				 
				 laoe       <= '0';       
				a_out_i       <= add_vme(23 downto 1);   
				 lamoe      <= '0';      
				am_out_i     <= am_vme(5 downto 0);    
				
				 ldsoe_i       <= '0';     
				as_b_out_i    <= '0';  
				
				
				
				if (run_c_fix(1)  = '1') then -- BLT MODE FIX ADDED 04/02/2014 
					ds_b_out_i    <= "00"; 
				else
					if am_vme(28) = '0' then --8 BITS MODE SINGLE
						ds_b_out_i    <= (add_vme(0) & not add_vme(0)); 
					else
						ds_b_out_i    <= "00"; 
					end if;
				end if;
			
				write_b_out_i <= run_c_fix(3); --READ/WRITE   
				lword_b_out_i <= '1';--am_vme(28);     
				time_cycle <= '1';	
				end_run    <= '0';	
				start_blt <= '0';	
				rst_blt_pointer  <= '0';
				rst_bblt_pointer  <= '0';				
				MSW_rst	 <= '0';	
				blkb_sl	<= '1';
				blkb_m	<= '0';	
				RnW_B_sl  <=  '1';
				
				
				if run_c_fix(7) = '1' then
					if tm_count >= TM_BC then 
						dtack_b_out_i <= '0';--'1'; --BROAD CAST ADDED
					else
						dtack_b_out_i <= '1';		
					end if;	
				else
					dtack_b_out_i <= '1';				
				end if;
			
				LED_N(1) <= '1';
				
				bus_rq <= '1';	
				
				  lbroe    <= '0';
				bbsy_b_out_i<= '0';
				
				status_error<= X"00000000"; 	
				error_flag	<= '0';
				time_rst	<= '0';
				time_rst_a	<= '0';	
				
				IntRequest <= '0';	
				lirqo <= '1';

				loe_i <= vcc;--gnd;
				
	
				spare_A (11 downto 8) <= "ZZZZ"; 	
				state_reg  <= X"00000006";
			
			when stc_m_ending =>
				 ldoe     	<= run_c_fix(3); --READ/WRITE      
				d_out_i       <= d_vme_m;   
				 
				 laoe       <= '0';       
				a_out_i       <= add_vme(23 downto 1);   
				 lamoe      <= '0';      
				am_out_i     <= am_vme(5 downto 0);    
				
				 ldsoe_i       <= '0';     
				as_b_out_i    <= '0';      
				ds_b_out_i    <= "11";  	
				write_b_out_i <= run_c_fix(3); --READ/WRITE     
				lword_b_out_i <= '1';--am_vme(28);     
				
				end_run    <= '0';
								
				if (run_c_fix(2 downto 0)  = "010") and (dtack_b_in_reg = '1') then  
					start_blt <= '1';
				else
					start_blt <= '0';
				end if;
				
				if (run_c_fix(2 downto 0)  = "010") and (dtack_b_in_reg = '1') then  
					time_cycle <= '0';					
				else
					time_cycle <= '1';
				end if;
								
				rst_blt_pointer  <= '0';
				rst_bblt_pointer  <= '0';				
				MSW_rst	 <= '0';	
				blkb_sl	<= '1';
				blkb_m	<= '0';	
				RnW_B_sl  <=  '1';

				dtack_b_out_i <= '1';
				LED_N(1) <= '1';
				
				bus_rq <= '1';	
				
				  lbroe    <= '0';
				bbsy_b_out_i<= '0';
				
				status_error<= X"00000000"; 	
				error_flag	<= '0';
				time_rst	<= '0';
				time_rst_a	<= '0';	
				
				IntRequest <= '0';	
				lirqo <= '1';

				loe_i <= vcc;--gnd;
				
			
				spare_A (11 downto 8) <= "ZZZZ";
				state_reg  <= X"00000007";
	
	--MASTER /RW BBLT
		when stc_m_bblt =>
			 ldoe     	<= '1';       
			d_out_i       <= X"0000";   
			 laoe       <= '0';       
			a_out_i       <= add_vme_bblt(23 downto 1);   
			 lamoe      <= '0';      
			am_out_i     <= am_vme(5 downto 0); 
			
			 ldsoe_i       <= '0';     
			as_b_out_i    <= '1';      
			ds_b_out_i    <= "11";  	
			write_b_out_i <= '1';      
			lword_b_out_i <= '1';--am_vme(28);     
			time_cycle <= '1';
			end_run    <= '0';	
			start_blt <= '0';	
			rst_blt_pointer  <= '1';	
			rst_bblt_pointer <= '1';	
			MSW_rst	 <= '1';
			blkb_sl	<= '1';
			blkb_m	<= '0';	
			RnW_B_sl  <=  '1';
			
			dtack_b_out_i <= '1';
			LED_N(1) <= '1';
			
			bus_rq <= '1';	
			
			 lbroe    <= '0';
			bbsy_b_out_i<= '0';
			
			status_error<= X"00000000"; 	
			error_flag	<= '0';
			time_rst	<= '0';
			time_rst_a	<= '0';	
			
			IntRequest <= '0';	
			lirqo <= '1';	
			
			loe_i <= vcc;--gnd;
			
		
			spare_A (11 downto 8) <= "ZZZZ"; 
			state_reg  <= X"BB000004";			
			
		
			
			when stc_m_addresing_bblt =>
				 ldoe     	<= run_c_fix(3); --READ/WRITE
				d_out_i       <= d_vme_m_bblt;   
				 
				 laoe       <= '0';       
				a_out_i       <= add_vme_bblt(23 downto 1);   
				 lamoe      <= '0';      
				am_out_i     <= am_vme(5 downto 0);   
				
				 ldsoe_i       <= '0';     
				as_b_out_i    <= '0';  
				
				-- if (run_c_fix(1)  = '1') then -- BLT MODE FIX ADDED 04/02/2014 
					-- ds_b_out_i    <= "00"; 
				-- else
					-- if am_vme(28) = '0' then --8 BITS MODE SINGLE
						-- ds_b_out_i    <= (add_vme(0) & not add_vme(0)); 
					-- else
						-- ds_b_out_i    <= "00"; 
					-- end if;
				-- end if;
				ds_b_out_i    <= "11"; 
				
				write_b_out_i <= run_c_fix(3); --READ/WRITE        
				lword_b_out_i <= '1';--am_vme(28);     
				time_cycle <= '1';		
				end_run    <= '0';	
				start_blt <= '0';
				rst_blt_pointer   <= '0';	
				rst_bblt_pointer  <= '0';	
				MSW_rst	 <= '0';
				blkb_sl	<= '1';
				blkb_m	<= '0';	
				RnW_B_sl  <=  '1';

				dtack_b_out_i <= '1';
				LED_N(1) <= '1';
				
				bus_rq <= '1';	
				
				  lbroe    <= '0';
				bbsy_b_out_i<= '0';
				
				status_error<= X"00000000"; 	
				error_flag	<= '0';
				time_rst	<= '0';
				time_rst_a	<= '0';	
				
				IntRequest <= '0';	
				lirqo <= '1';

				loe_i <= vcc;--gnd;
				
	
				spare_A (11 downto 8) <= "ZZZZ"; 	
				state_reg  <= X"BB000005";
		
			when stc_m_dating_bblt =>
				 ldoe     	<= run_c_fix(3); --READ/WRITE     
				d_out_i       <= d_vme_m_bblt;   
				 
				 laoe       <= '0';       
				a_out_i       <= add_vme_bblt(23 downto 1);   
				 lamoe      <= '0';      
				am_out_i     <= am_vme(5 downto 0);    
				
				 ldsoe_i       <= '0';     
				as_b_out_i    <= '0';  
				
				
				
				-- if (run_c_fix(1)  = '1') then -- BLT MODE FIX ADDED 04/02/2014 
					-- ds_b_out_i    <= "00"; 
				-- else
					-- if am_vme(28) = '0' then --8 BITS MODE SINGLE
						-- ds_b_out_i    <= (add_vme(0) & not add_vme(0)); 
					-- else
						-- ds_b_out_i    <= "00"; 
					-- end if;
				-- end if;
				ds_b_out_i    <= "00"; 
			
				write_b_out_i <= run_c_fix(3); --READ/WRITE   
				lword_b_out_i <= '1';--am_vme(28);     
				time_cycle <= '1';	
				end_run    <= '0';	
				start_blt <= '0';	
				rst_blt_pointer   <= '0';	
				rst_bblt_pointer  <= '0';	
				MSW_rst	 <= '0';	
				blkb_sl	<= '1';
				blkb_m	<= '0';	
				RnW_B_sl  <=  '1';
				
				
				if run_c_fix(7) = '1' then
					if tm_count >= TM_BC then 
						dtack_b_out_i <= '0';--'1'; --BROAD CAST ADDED
					else
						dtack_b_out_i <= '1';		
					end if;	
				else
					dtack_b_out_i <= '1';				
				end if;
			
				LED_N(1) <= '1';
				
				bus_rq <= '1';	
				
				  lbroe    <= '0';
				bbsy_b_out_i<= '0';
				
				status_error<= X"00000000"; 	
				error_flag	<= '0';
				time_rst	<= '0';
				time_rst_a	<= '0';	
				
				IntRequest <= '0';	
				lirqo <= '1';

				loe_i <= vcc;--gnd;
				
	
				spare_A (11 downto 8) <= "ZZZZ"; 	
				state_reg  <= X"BB000006";
			
			when stc_m_ending_bblt =>
				 ldoe     	<= run_c_fix(3); --READ/WRITE      
				d_out_i       <= d_vme_m_bblt;   
				 
				 laoe       <= '0';       
				a_out_i       <= add_vme_bblt(23 downto 1);   
				 lamoe      <= '0';      
				am_out_i     <= am_vme(5 downto 0);    
				
				 ldsoe_i       <= '0';     
				as_b_out_i    <= '0';      
				ds_b_out_i    <= "11";  	
				write_b_out_i <= run_c_fix(3); --READ/WRITE     
				lword_b_out_i <= '1';--am_vme(28);     
				
				end_run    <= '0';
								
				if (dtack_b_in_reg = '1') then  
					start_blt <= '1';
					time_cycle <= '0';	
				else
					start_blt <= '0';
					time_cycle <= '1';
				end if;
					
				rst_blt_pointer   <= '0';	
				rst_bblt_pointer  <= '0';	
				MSW_rst	 <= '0';	
				blkb_sl	<= '1';
				blkb_m	<= '0';	
				RnW_B_sl  <=  '1';

				dtack_b_out_i <= '1';
				LED_N(1) <= '1';
				
				bus_rq <= '1';	
				
				  lbroe    <= '0';
				bbsy_b_out_i<= '0';
				
				status_error<= X"00000000"; 	
				error_flag	<= '0';
				time_rst	<= '0';
				time_rst_a	<= '0';	
				
				IntRequest <= '0';	
				lirqo <= '1';

				loe_i <= vcc;--gnd;
				
			
				spare_A (11 downto 8) <= "ZZZZ";
				state_reg  <= X"BB000007";
	
	--	--SLAVE
	when stc_sl =>
					
		 ldoe     	<= '1';       
		d_out_i       <= X"0000";   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		ldsoe_i       <= '1';  
-- ldsoe_i       <= '0'; 		
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '1';		
		start_blt <= '0';
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '1';
		blkb_sl	<= '0';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';	
		lirqo <= '1';

		loe_i <= gnd;
	
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"00000008";
		
			
		when stc_sl_addresing =>
		
		 ldoe     	<= not write_b_in;       
		d_out_i       <= d_vme_sl;  
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		 ldsoe_i       <= '1';     
-- ldsoe_i       <= '0'; 	
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '1';		
		start_blt <= '0';
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '0';
		blkb_sl	<= '0';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';	
		lirqo <= '1';	
		
		loe_i <= gnd;
		
	
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"00000009";
				
				
		when stc_sl_dating =>
		
		 ldoe     	<= not write_b_in;        
		d_out_i       <= d_vme_sl;   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		ldsoe_i       <= '1';    
-- ldsoe_i       <= '0'; 		
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '1';		
		start_blt <= '0';
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '0';
		blkb_sl	<= '0';
		blkb_m	<= '1';	
		RnW_B_sl  <= write_b_in or (ds_b_in_reg(1) and ds_b_in_reg(0));
		
		dtack_b_out_i <= '0';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';	
		lirqo <= '1';	
		
		loe_i <= gnd;
	
	
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"0000000A";
					
		when stc_sl_ending =>
		
		 ldoe     	<= not write_b_in;        
		d_out_i       <= d_vme_sl;     
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		 ldsoe_i       <= '1';   
-- ldsoe_i       <= '0'; 	
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '0';	
		end_run    <= '1';
		
		if (ds_b_in_reg(0) = '0' or ds_b_in_reg(1) = '0') then 		
			start_blt <= '1';
		else
			start_blt <= '0';
		end if;
		
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '0';
		blkb_sl	<= '0';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';	
		lirqo <= '1';	
		
		loe_i <= vcc;--gnd;
	
	
		spare_A (11 downto 8) <= "ZZZZ";
		state_reg  <= X"0000000B";		
	
	
	
	--	--INT
	when stc_int =>
	
		 ldoe     	<= not Access_DTB;       
		d_out_i       <= d_vme_irq; --d_vme_irq(7 downto 0) & d_vme_irq(15 downto 8);  
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		 ldsoe_i       <= '1';     
-- ldsoe_i       <= '0'; 	
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '0';		
		start_blt <= '0';
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '0';
		blkb_sl	<= '0';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= dtack_b_irq;
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest  <= '1';	
		lirqo <= '0';	
		
		--loe_i <= gnd; --MODIFICATION INT DTACK 25/01/2016
		loe_i <= vcc;
	
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"0000000C";
		
	--INT HANDLER
	when stc_int_h =>
	
		 ldoe     	<= '1';       
		d_out_i       <= (others => '0');  
		 laoe       <= '0';       
		a_out_i       <= "00000000000000000000"&a_b_irq;   
		 lamoe      <= '1';       
		am_out_i     <= (others => '0');   
		
		 ldsoe_i       <= '0';     
		-- ldsoe_i       <= '1'; 	
		as_b_out_i    <= as_b_irq;       
		ds_b_out_i    <= ds_b_irq;  	

		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '0';		
		start_blt <= '0';
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '0';
		blkb_sl	<= '0';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
	
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest  <= '0';	
		--lirqo <= '0';	
		lirqo <= '1';	--MODIFICATION INT DTACK 23/06/2016
		
		loe_i <= vcc;--gnd;
	
	
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"0000000D";
		
	--	MUI
	when stc_mui =>
	
		 ldoe     	<= '1';       
		d_out_i       <= (others => '0');  
		 laoe       <= '1';       
		a_out_i       <= (others => '0');    
		 lamoe      <= '1';       
		am_out_i     <= (others => '0');   
		
			 ldsoe_i       <= '1';     
-- ldsoe_i       <= '0'; 	
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '0';		
		start_blt <= '0';
		rst_blt_pointer  <= '0';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '0';
		blkb_sl	<= '0';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest  <= '0';	
		lirqo <= '1';

		loe_i <= gnd;
	
	
		spare_A (11 downto 8) <= mui_reg(5 downto 2); 
		state_reg  <= X"0000000E";		
	
	
	-- when stc_mui_eos_f_A => 	
	
	
	 -- ldoe     	<= '1';       
		-- d_out_i       <= (others => '0');  
		 -- laoe       <= '1';       
		-- a_out_i       <= (others => '0');    
		 -- lamoe      <= '1';       
		-- am_out_i     <= (others => '0');   
		
			 -- ldsoe_i       <= '1';     
----ldsoe_i       <= '0'; 	
		-- as_b_out_i    <= '1';       
		-- ds_b_out_i    <= "11";  	  
		-- write_b_out_i <= '1';       
		-- lword_b_out_i <= '1';       
		-- time_cycle <= '1';	
		-- end_run    <= '0';		
		-- start_blt <= '0';
		-- rst_blt_pointer  <= '0';
		-- rst_bblt_pointer  <= '0';		
		-- MSW_rst	 <= '0';
		-- blkb_sl	<= '0';
		-- blkb_m	<= '1';	
		-- RnW_B_sl  <=  '1';
		
		-- dtack_b_out_i <= '1';
		-- LED_N(1) <= '1';
		
		-- bus_rq <= '1';	
		
		  -- lbroe    <= '1';
		-- bbsy_b_out_i<= '1';
		
		-- status_error<= X"00000000"; 	
		-- error_flag	<= '0';
		-- time_rst	<= '0';
		-- time_rst_a	<= '0';	
		
		-- IntRequest  <= '0';	
		-- lirqo <= '1';

		-- loe_i <= gnd;
		
	
	
		-- spare_A (11 downto 8) <= mui_reg(5 downto 2); 
		-- state_reg  <= X"D000000E";		
		
	-- when stc_mui_eos_f_B => 	
	
	 -- ldoe     	<= '1';       
		-- d_out_i       <= (others => '0');  
		 -- laoe       <= '1';       
		-- a_out_i       <= (others => '0');    
		 -- lamoe      <= '1';       
		-- am_out_i     <= (others => '0');   
		
			 -- ldsoe_i       <= '1';     
----ldsoe_i       <= '0'; 	
		-- as_b_out_i    <= '1';       
		-- ds_b_out_i    <= "11";  	  
		-- write_b_out_i <= '1';       
		-- lword_b_out_i <= '1';       
		-- time_cycle <= '1';	
		-- end_run    <= '0';		
		-- start_blt <= '0';
		-- rst_blt_pointer  <= '0';
		-- rst_bblt_pointer  <= '0';		
		-- MSW_rst	 <= '0';
		-- blkb_sl	<= '0';
		-- blkb_m	<= '1';	
		-- RnW_B_sl  <=  '1';
		
		-- dtack_b_out_i <= '1';
		-- LED_N(1) <= '1';
		
		-- bus_rq <= '1';	
		
		  -- lbroe    <= '1';
		-- bbsy_b_out_i<= '1';
		
		-- status_error<= X"00000000"; 	
		-- error_flag	<= '0';
		-- time_rst	<= '0';
		-- time_rst_a	<= '0';	
		
		-- IntRequest  <= '0';	
		-- lirqo <= '1';

		-- loe_i <= gnd;
	
	
		-- spare_A (11 downto 8) <= mui_reg(5 downto 2); 
		-- state_reg  <= X"D000000E";		
	
	
	when stc_end_run =>
				
		 ldoe     	<= '1';       
		d_out_i       <= X"0000";   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		 ldsoe_i       <= '1';  
-- ldsoe_i       <= '0';     
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '1';		
		start_blt <= '0';
		rst_blt_pointer   <= '1';
		rst_bblt_pointer  <= '0';		
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"00000000"; 	
		error_flag	<= '0';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';
		lirqo <= '1';	

		loe_i <= vcc;--gnd;
		
	
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"0000000F";

			
		when stc_error_m =>
				
		 ldoe     	<= '1';       
		d_out_i       <= X"0000";   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		 ldsoe_i       <= '0';      
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '1';	
		start_blt <= '0';		
		rst_blt_pointer  <= '1';
		rst_bblt_pointer  <= '1';
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"A000000B"; 	
		error_flag	<= '1';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';
		lirqo <= '1';	

		loe_i <= vcc;--gnd;
	
	
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"00000011";

		
		when stc_error_sl =>
				
		 ldoe     	<= '1';       
		d_out_i       <= X"0000";   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		
		 ldsoe_i       <= '1';     
-- ldsoe_i       <= '0'; 	
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';	
		end_run    <= '1';	
		start_blt <= '0';		
		rst_blt_pointer  <= '1';
		rst_bblt_pointer  <= '1';
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
		
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"A000000C"; 	
		error_flag	<= '1';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';
		lirqo <= '1';	

		loe_i <= vcc;--gnd;
		
	
		spare_A (11 downto 8) <= "ZZZZ"; 
		state_reg  <= X"00000012";		

		when stc_error_irq =>
		
			
		 ldoe     	<= '1';       
		d_out_i       <= X"0000";   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		                          
		 ldsoe_i       <= '1';      
	-- ldsoe_i       <= '0'; 
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';		
		end_run    <= '1';		
		start_blt <= '0';
		rst_blt_pointer  <= '1';
		rst_bblt_pointer  <= '1';
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
				
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"A000000E"; 	
		error_flag	<= '1';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';	
		lirqo <= '1';	
		
		loe_i <= vcc;--gnd;
		
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"00000013";
		
		when stc_error_br =>
		
			
		 ldoe     	<= '1';       
		d_out_i       <= X"0000";   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		                          
		 ldsoe_i       <= '1';      
	-- ldsoe_i       <= '0'; 
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';		
		end_run    <= '1';		
		start_blt <= '0';
		rst_blt_pointer  <= '1';
		rst_bblt_pointer  <= '1';
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
				
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"A000000F"; 	
		error_flag	<= '1';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';	
		lirqo <= '1';	
		
		loe_i <= vcc;--gnd;
		
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"00000014";
		
		
		
		when others =>
		
			
		 ldoe     	<= '1';       
		d_out_i       <= X"0000";   
		 laoe       <= '1';       
		a_out_i       <= (others => '0');   
		 lamoe      <= '1';       
		am_out_i     <= "000000";   
		                          
		 ldsoe_i       <= '1';      
	-- ldsoe_i       <= '0'; 
		as_b_out_i    <= '1';       
		ds_b_out_i    <= "11";  	  
		write_b_out_i <= '1';       
		lword_b_out_i <= '1';       
		time_cycle <= '1';		
		end_run    <= '1';		
		start_blt <= '0';
		rst_blt_pointer  <= '1';
		rst_bblt_pointer  <= '1';
		MSW_rst	 <= '1';
		blkb_sl	<= '1';
		blkb_m	<= '1';	
		RnW_B_sl  <=  '1';
				
		dtack_b_out_i <= '1';
		LED_N(1) <= '1';
		
		bus_rq <= '1';	
		
		  lbroe    <= '1';
		bbsy_b_out_i<= '1';
		
		status_error<= X"A000000D"; 	
		error_flag	<= '1';
		time_rst	<= '0';
		time_rst_a	<= '0';	
		
		IntRequest <= '0';	
		lirqo <= '1';	
		
		loe_i <= vcc;--gnd;
	
	
		spare_A (11 downto 8) <= "ZZZZ"; 	
		state_reg  <= X"00000015";
		
	 end case;      
	 
   end process;
   
   
   
  ---**** EOS PULSE GENERATOR ****------- (07/04/2016) RFC
   
   SYNC_PROC: process (CLK)
  begin
     if (CLK'event and CLK = '1') then
        --if (rstin = '1') then
		if (Reset_n = '0') then
           state_eos <= st_eos_wait;
        else
           state_eos <= next_state_eos;
        -- assign other outputs to internal signals
		end if;        
     end if;
  end process;
	
  OUTPUT_DECODE: process (state_eos)
  begin
      case (state_eos) is
        when st_eos_wait =>
			eos_b_i			<= '1';	
			en_count_eos 	<= '0';
			eos_enable_buffer 	<= '1';
			       	
		when st_eos_active =>
			eos_b_i			<= '0';	
			en_count_eos 	<= '1';
			eos_enable_buffer 	<= '0';
			 
				
		when st_eos_noactive =>
			eos_b_i			<= '1';
			en_count_eos 	<= '1';		
			eos_enable_buffer 	<= '0';			
			
		when st_eos_error =>
			eos_b_i			<= '1';	
			en_count_eos 	<= '0';
			eos_enable_buffer 	<= '1';
				
		when others =>
			eos_b_i			<= '1';	
			en_count_eos 	<= '0';
			eos_enable_buffer 	<= '1';
		
          
     end case;      
  end process;		
	
		
	
	 NEXT_STATE_DECODE: process (state_eos, eos_fq, eos_width, tm_count_eos)
		begin
     --declare default state for next_state to avoid latches
     next_state_eos <= state_eos;  --default is to stay in current state
     --insert statements to decode next_state
     case (state_eos) is
        when st_eos_wait =>
			if eos_fq > (eos_width+2)	then 	
					next_state_eos <= st_eos_active;
			end if;
	    
	    when st_eos_active => 		
			if eos_fq = X"00000000"	then 	
				next_state_eos <= st_eos_wait;
			elsif tm_count_eos >= eos_width	then 
				next_state_eos <= st_eos_noactive;
			end if;		
		
		when st_eos_noactive => 		
			if eos_fq = X"00000000"	then 	
				next_state_eos <= st_eos_wait;
			elsif tm_count_eos >= eos_fq	then 
				next_state_eos <= st_eos_active;
			end if;				
			 
			
		when st_eos_error =>
           next_state_eos <= st_eos_wait;
	  
	  when others =>
		    next_state_eos <= st_eos_error;
          
     end case;      
    end process;		
   
   
   ---EOS_B COUNTER FQ
	
		EOS_B_COUNTER_P: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					tm_count_eos <= (others => '0');
			elsif (CLK'event and CLK = '1') then
				if tm_count_eos >= eos_fq then 
					tm_count_eos <= (others => '0');
				elsif en_count_eos = '1' then 
					tm_count_eos <= tm_count_eos + 1;
				end if;
			end if;
		end process;
  
  ---**** EOS PULSE GENERATOR ****------- 
   
   
   
   
   
   
   
  
   d_vme_bblt_calc: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
           	d_vme_m_bblt <= (OTHERS => '0');
    	elsif (CLK'event and CLK = '1') then
			if DBBLT(31)  = '1' then 
				--d_vme_m_bblt <= SBBLT(15 downto 0) + add_vme_bblt(15 downto 0) + (blt_pointer(6 downto 1) & '0');
				d_vme_m_bblt <= add_vme_bblt(15 downto 0) + (blt_pointer(6 downto 1) & '0');
			else
				d_vme_m_bblt <= DBBLT(15 downto 0);
           	end if;
    	end if;
	end process; 
  
   add_vme_bblt_calc: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
           	add_vme_bblt <= (OTHERS => '0');
    	elsif (CLK'event and CLK = '1') then
			if blkb_m = '0' then 
				if blkb_m_flt = '1' then 
					add_vme_bblt <= SBBLT;
           	 	elsif rst_bblt_pointer_fd = '1' then 
					if am_vme(28) = '0' then
						add_vme_bblt <= add_vme_bblt + (blt_pointer(6 downto 2) & "00");
					else 
						add_vme_bblt <= add_vme_bblt + (blt_pointer(6 downto 2) & "000");
					end if;
				end if;
			else
				add_vme_bblt <= (OTHERS => '0');
			end if;	
    	end if;
	end process; 
	
	reg_blkb_m_p: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
           	reg_blkb_m <= '1';
    	elsif (CLK'event and CLK = '1') then
			reg_blkb_m <= blkb_m;
    	end if;
	end process; 
	
	blkb_m_flt <= reg_blkb_m and not blkb_m;
	
	
	
	
	
	rst_bblt_pointer_fd <= rst_bblt_pointer and not rst_bblt_pointer_reg;
	
	 REG_ADD_VME_BLT: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
           	rst_bblt_pointer_reg <= '0';
    	elsif (CLK'event and CLK = '1') then
			rst_bblt_pointer_reg <= rst_bblt_pointer;
    	end if;
	end process; 
		
   	
	BLT_POINTER_COUNTER: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
           	blt_pointer <= (OTHERS => '0');
    	elsif (CLK'event and CLK = '1') then
			if rst_blt_pointer = '1' then 
				blt_pointer <= (OTHERS => '0');
            elsif start_blt_fd = '1' then 
				blt_pointer <= blt_pointer + 1;
			end if;
    	end if;
	end process; 
	
	start_blt_fd <= start_blt and not start_blt_reg;
	
	START_BLT_REG_P: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
           	start_blt_reg <='0';
    	elsif (CLK'event and CLK = '1') then
			start_blt_reg <= start_blt;
    	end if;
	end process; 
	
	CONTER_TIME_CYCLE: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					tm_count <= 0;
			elsif (CLK'event and CLK = '1') then
				if time_cycle = '1' then
					tm_count <= tm_count + 1;
				else
					tm_count <= 0;
				end if;
			end if;
		end process;	
		
--RUN_C REG CONTROL FIX RAM ISSUES	

	RUN_C_FIX_P: process (CLK, Reset_n)
	begin
		if (Reset_n = '0') then
				run_c_fix 			<= X"00000000";
				dct_reg(7 downto 0)	<= (OTHERS => '1');
				eos_width		    <= (OTHERS => '0');
			    eos_fq			    <= (OTHERS => '0');

		elsif (CLK'event and CLK = '1') then
			if (add_uart = X"00000002") and (RnW = '0') and (CS_CTL = '0')  then
				run_c_fix <= data_out_uart;
			elsif (add_uart = X"00000005") and (RnW = '0') and (CS_CTL = '0')  then
				dct_reg(7 downto 0) <= data_out_uart(7 downto 0);
			
			elsif (add_uart = X"0000000F") and (RnW = '0') and (CS_CTL = '0')  then
				eos_width <= data_out_uart;
			elsif (add_uart = X"00000010") and (RnW = '0') and (CS_CTL = '0')  then
				eos_fq    <= data_out_uart;
					
			elsif end_run = '1' then
				run_c_fix <= "000000000000000000000000000" & run_c_fix(4 downto 3) & "000";
			elsif reset_vme = '0'  then
				run_c_fix <= X"00000000";
			end if;
		end if;
	end process;	


	
--READ/WRITE CONTROL REGISTER: RAM CTL PORT B


--WRITE PROCESS

write_p: process (wr_pointer, end_run, state_reg, reset_vme) 
	begin

            CASE wr_pointer IS
				WHEN "000000" => 
						ctl_data_in <=  VERSION;
						ctl_wen_in  <=  '0';
				WHEN "000001" => 
						ctl_data_in <=  REVISON;
						ctl_wen_in  <=  '0';	
				WHEN "000101" => 
						ctl_data_in <=  dct_reg;
						ctl_wen_in  <=  '0';		
				WHEN "000111" => 
					if end_run = '1' then 
						ctl_data_in <=  X"00000000";
						ctl_wen_in  <=  '0';											
					else
						ctl_data_in <=  X"00000000";
						ctl_wen_in  <=  '1';
					end if;				
				WHEN "001101" => 
						ctl_data_in <=  state_reg;
						ctl_wen_in  <=  '0';
				WHEN "010101" => 
						ctl_data_in <=  status_id_irqh;
						ctl_wen_in  <=  '0';			
				
				WHEN OTHERS => 
					ctl_data_in <=  (OTHERS => '0');
					if reset_vme = '0'  then
						ctl_wen_in  <=  '0';
					else
						ctl_wen_in  <=  '1';
					end if;
					
				
				END CASE;
	end process; 


--READ PROCESS

read_p: process (CLK, Reset_n) 
	begin
		if (Reset_n = '0') then
			add_vme 		<= (OTHERS => '0');
			size_block 		<= (OTHERS => '0');
			brqg_reg 		<= (OTHERS => '1');
			reset_reg 		<= (OTHERS => '0');
			slave_add_start <= (OTHERS => '0');
			slave_add_end 	<= (OTHERS => '0');
			Int_Handler_Reg <= (OTHERS => '0');
			Interrupter_Reg <= (OTHERS => '0');
			mui_reg 		<= (OTHERS => '0');
			am_vme			<= (OTHERS => '0');
			-- eos_width		<= (OTHERS => '0');
			-- eos_fq			<= (OTHERS => '0');
			SBBLT			<= (OTHERS => '0');
			EBBLT			<= (OTHERS => '0');
			DBBLT			<= (OTHERS => '0');
			--dct_reg(7 downto 0)			<= (OTHERS => '1');
			
		elsif (CLK'event and CLK = '1') then
            CASE wr_pointer IS
				WHEN "000100" => --+1
						add_vme 		<=  ctl_data_out;
				WHEN "000101" => --+1
						size_block 		<=  ctl_data_out(6 downto 0);	
				-- WHEN "000110" => --+1
						-- dct_reg(7 downto 0) 		<=  ctl_data_out(7 downto 0);	
				WHEN "000111" => --+1
						brqg_reg 		<=  ctl_data_out;
				WHEN "001000" => --+1
						reset_reg 		<=  ctl_data_out;		
				WHEN "001001" => --+1
						slave_add_start <=  ctl_data_out;	
				WHEN "001010" => --+1
						slave_add_end	<=  ctl_data_out;	
				WHEN "001011" => --+1 --ADDED IRQ 03/11/2014
						Int_Handler_Reg	<=  ctl_data_out;	
				WHEN "001100" => --+1
						Interrupter_Reg	<=  ctl_data_out;	
				WHEN "001101" => --+1
						mui_reg			<=  ctl_data_out;	
				WHEN "001111" => --+1
						am_vme			<=  ctl_data_out;	
				-- WHEN "010000" => --+1
						-- eos_width		<=  ctl_data_out;
				-- WHEN "010001" => --+1
						-- eos_fq			<=  ctl_data_out;		

				WHEN "010011" => --+1
						SBBLT			<=  ctl_data_out;					
				WHEN "010100" => --+1
						EBBLT			<=  ctl_data_out;					
				WHEN "010101" => --+1
						DBBLT			<=  ctl_data_out;		
						
				WHEN OTHERS => NULL;	
			END CASE;
    	end if;
	end process; 
	
		
	wr_pointer_p: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
           	wr_pointer <= (OTHERS => '0');
    	elsif (CLK'event and CLK = '1') then
            if wr_pointer = "111111" then 
				wr_pointer <= (OTHERS => '0');
			--elsif wr_pointer /= add_uart_before then
			else
				wr_pointer <= wr_pointer + 1;
			end if;
    	end if;
	end process; 
		
	add_uart_before <= (add_uart(5 downto 0))-1;
	

--- PIN/OUT STATUS STORE CYCLES ---

--	--DATA REG 
--  d_in       : in std_logic_vector(15 downto 0);  	-- VMEbus Data
--	ds_b_in    : in std_logic_vector(1 downto 0);  		-- VMEbus DataStrobe
--	write_b_in : in std_logic;       					-- VMEbus Data Direction
--	lword_b_in : in std_logic;       					-- VMEbus Long-Word
--	dtack_b_in : in std_logic;       					-- VMEbus Data Transfer Acknowledge

--    
--	--ADD REG 
--	a_in_reg       : in std_logic_vector(23 downto 1);  	-- VMEbus Address
--  am_in_reg      : in std_logic_vector(5 downto 0);  		-- VMEbus Address Modifier
--	as_b_in_reg    : in std_logic;       					-- VMEbus Address Strobe
--	
--	--CONTROL REG 
--	irq_b_in   : in std_logic_vector(7 downto 1);  		-- VMEbus Int Request
--  berr_b_in  : in std_logic;       					-- VMEbus Error
--	sysfail_b_in	: in std_logic;       				-- VMEbus System Fail
--	bclr_b_in  : in std_logic;       					-- VMEbus Clear
--	bbsy_b_in  : in std_logic;       					-- VMEbus Busy
--	br_b_in    : in std_logic_vector(3 downto 0);  		-- VMEbus Request
--  helath_b   	: in    std_logic;       				-- VMEbus HELATH
--	tpfail_b   	: in    std_logic;       				-- VMEbus TP Fail
--	scon	    : in    std_logic;  					-- SCON
--	
--	
--	--SPARE REG 
--	spare_B	    : in std_logic_vector(15 downto 12);  	-- SPARE
--	user_in	    : in std_logic_vector(7 downto 0);  	-- SPARE
			

--LATCH PROCESS

--Lathing in ds event as event and dtack event
--OUT ADDED 06/10/2014

   flp_p: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
           	flp 	<= '0';
           	flp_reg <= '0';
    	elsif (CLK'event and CLK = '1') then
            flp 	<= run_c_fix(8);
			flp_reg	<= flp;
    	end if;
	end process; 
	
   flp_event <= flp and not flp_reg;

	pn_data_p_reg: process (CLK, Reset_n) 
		variable i :natural; 
	begin
	  if (Reset_n = '0') then
			for i in 2 downto 0	loop
				pn_data (i) <= (others=>'0');
			end loop; 
		elsif (CLK'event and CLK = '1') then
			if reset_reg = X"FFFFFFFF" then 
				for i in 2 downto 0	loop
					pn_data (i) <= (others=>'0');
				end loop; 
				else 
					if ds_event_flt = '1' then
								pn_data (0) <= "00000000000"&d_in_reg&ds_b_in_reg&write_b_in&lword_b_in&dtack_b_in_reg;
					elsif flp_event = '1' then
								pn_data (0) <= "00000000000"&d_in_reg&ds_b_in_reg&write_b_in&lword_b_in&dtack_b_in_reg;
					elsif as_event_flt = '1' then
								pn_data (1) <= "00000000000"&d_in_reg&ds_b_in_reg&write_b_in&lword_b_in&dtack_b_in_reg;
					elsif dtack_event_flt = '1' then
								pn_data (2) <= "00000000000"&d_in_reg&ds_b_in_reg&write_b_in&lword_b_in&dtack_b_in_reg;		
					end if;
			end if;
		end if;	
	end process; 
	
	pn_data_p_reg_out: process (CLK, Reset_n) 
		variable i :natural; 
	begin
	  if (Reset_n = '0') then
			for i in 2 downto 0	loop
				pn_data_out (i) <= (others=>'0');
			end loop; 
		elsif (CLK'event and CLK = '1') then
			if reset_reg = X"FFFFFFFF" then 
					for i in 2 downto 0	loop
						pn_data_out (i) <= (others=>'0');
					end loop; 
			else 
				if ds_event_flt = '1' then
					pn_data_out (0) <= "00000000000"&d_out_i&ds_b_out_i&write_b_out_i&lword_b_out_i&dtack_b_out_i;
				elsif flp_event = '1' then
					pn_data_out (0) <= "00000000000"&d_out_i&ds_b_out_i&write_b_out_i&lword_b_out_i&dtack_b_out_i;
				elsif as_event_flt = '1' then
					pn_data_out (1) <= "00000000000"&d_out_i&ds_b_out_i&write_b_out_i&lword_b_out_i&dtack_b_out_i;
				elsif dtack_event_flt = '1' then
					pn_data_out (2) <= "00000000000"&d_out_i&ds_b_out_i&write_b_out_i&lword_b_out_i&dtack_b_out_i;
				end if;
			end if;
		end if;
	end process; 
	
	d_out 			<= d_out_i;
	ds_b_out 		<= ds_b_out_i;
	write_b_out 	<= write_b_out_i;
	lword_b_out 	<= lword_b_out_i;
	

	pn_add_p_reg: process (CLK, Reset_n) 
		variable i :natural; 
	begin
	  if (Reset_n = '0') then
			for i in 2 downto 0	loop
				pn_add (i) <= (others=>'0');
			end loop; 
		elsif (CLK'event and CLK = '1') then
			if reset_reg = X"FFFFFFFF" then 
					for i in 2 downto 0	loop
						pn_add (i) <= (others=>'0');
					end loop; 
			else 
				if ds_event_flt = '1' then
					pn_add (0) <= "00"&a_in_reg&am_in_reg&as_b_in_reg;
				elsif flp_event = '1' then
					pn_add (0) <= "00"&a_in_reg&am_in_reg&as_b_in_reg;
				elsif as_event_flt = '1' then
					pn_add (1) <= "00"&a_in_reg&am_in_reg&as_b_in_reg;
				elsif dtack_event_flt = '1' then
					pn_add (2) <= "00"&a_in_reg&am_in_reg&as_b_in_reg;
				end if;
			end if;
		end if;
	end process; 
	
	
	pn_add_p_reg_out: process (CLK, Reset_n) 
		variable i :natural; 
	begin
	  if (Reset_n = '0') then
			for i in 2 downto 0	loop
				pn_add_out (i) <= (others=>'0');
			end loop; 
		elsif (CLK'event and CLK = '1') then
			if reset_reg = X"FFFFFFFF" then 
					for i in 2 downto 0	loop
						pn_add_out (i) <= (others=>'0');
					end loop; 
			else 
				if ds_event_flt = '1' then
					pn_add_out (0) <= "00"&a_out_i&am_out_i&as_b_out_i;
				elsif flp_event = '1' then
					pn_add_out (0) <= "00"&a_out_i&am_out_i&as_b_out_i;
				elsif as_event_flt = '1' then
					pn_add_out (1) <= "00"&a_out_i&am_out_i&as_b_out_i;
				elsif dtack_event_flt = '1' then
					pn_add_out (2) <= "00"&a_out_i&am_out_i&as_b_out_i;
				end if;
			end if;
		end if;
	end process; 
	
	a_out 		<= a_out_i;
	am_out 		<= am_out_i;
	as_b_out 	<= as_b_out_i;
	
	
	
	pn_ctrl_p_reg: process (CLK, Reset_n) 
		variable i :natural; 
	begin
	  if (Reset_n = '0') then
			for i in 2 downto 0	loop
				pn_ctrl (i) <= (others=>'0');
			end loop; 
		elsif (CLK'event and CLK = '1') then
			if reset_reg = X"FFFFFFFF" then 
					for i in 2 downto 0	loop
						pn_ctrl (i) <= (others=>'0');
					end loop; 
			else 
				if ds_event_flt = '1' then
					pn_ctrl (0) <= "0000000000000"&irq_b_in&berr_b_in&sysfail_b_in&bclr_b_in&bbsy_b_in&br_b_in&helath_b&tpfail_b&"00";
				elsif flp_event = '1' then
					pn_ctrl (0) <= "0000000000000"&irq_b_in&berr_b_in&sysfail_b_in&bclr_b_in&bbsy_b_in&br_b_in&helath_b&tpfail_b&"00";
				elsif as_event_flt = '1' then
					pn_ctrl (1) <= "0000000000000"&irq_b_in&berr_b_in&sysfail_b_in&bclr_b_in&bbsy_b_in&br_b_in&helath_b&tpfail_b&"00";
				elsif dtack_event_flt = '1' then
					pn_ctrl (2) <= "0000000000000"&irq_b_in&berr_b_in&sysfail_b_in&bclr_b_in&bbsy_b_in&br_b_in&helath_b&tpfail_b&"00";
				end if;
			end if;
		end if;
	end process; 
	
	pn_ctrl_p_reg_out: process (CLK, Reset_n) 
		variable i :natural; 
	begin
	  if (Reset_n = '0') then
			for i in 2 downto 0	loop
				pn_ctrl_out (i) <= (others=>'0');
			end loop; 
		elsif (CLK'event and CLK = '1') then
			if reset_reg = X"FFFFFFFF" then 
					for i in 2 downto 0	loop
						pn_ctrl_out (i) <= (others=>'0');
					end loop; 
			else 
				if ds_event_flt = '1' then
					pn_ctrl_out (0) <= "0000000000000"&irq_b_out_i&berr_b_out_i&sysfail_b_out_i&bclr_b_out_i&bbsy_b_out_i&br_b_out_i&helath_b&tpfail_b&"00";
				elsif flp_event = '1' then
					pn_ctrl_out (0) <= "0000000000000"&irq_b_out_i&berr_b_out_i&sysfail_b_out_i&bclr_b_out_i&bbsy_b_out_i&br_b_out_i&helath_b&tpfail_b&"00";
				elsif as_event_flt = '1' then
					pn_ctrl_out (1) <= "0000000000000"&irq_b_out_i&berr_b_out_i&sysfail_b_out_i&bclr_b_out_i&bbsy_b_out_i&br_b_out_i&helath_b&tpfail_b&"00";
				elsif dtack_event_flt = '1' then
					pn_ctrl_out (2) <= "0000000000000"&irq_b_out_i&berr_b_out_i&sysfail_b_out_i&bclr_b_out_i&bbsy_b_out_i&br_b_out_i&helath_b&tpfail_b&"00";
				end if;
			end if;
		end if;
	end process; 
	
	irq_b_out 		<= irq_b_out_i; 	
	sysfail_b_out	<= sysfail_b_out_i;	
	bclr_b_out		<= bclr_b_out_i;	
	bbsy_b_out		<= bbsy_b_out_i;	
	br_b_out 		<= br_b_out_i;
	
 	
	pn_spare_p_reg: process (CLK, Reset_n) 
		variable i :natural; 
	begin
	  if (Reset_n = '0') then
			for i in 2 downto 0	loop
				pn_spare (i) <= (others=>'0');
			end loop; 
		elsif (CLK'event and CLK = '1') then
			if ds_event_flt = '1' then
				pn_spare (0) <= "00000000000000000000"&spare_B&user_in;
			elsif flp_event = '1' then
				pn_spare (0) <= "00000000000000000000"&spare_B&user_in;
			elsif as_event_flt = '1' then
				pn_spare (1) <= "00000000000000000000"&spare_B&user_in;
			elsif dtack_event_flt = '1' then
				pn_spare (2) <= "00000000000000000000"&spare_B&user_in;
			end if;
		end if;
	end process; 
	
	
	PN_DATA_DECO: process (add_uart, error_reg, pn_data, pn_data_out, pn_add_out, pn_ctrl_out, pn_add, pn_ctrl, pn_spare) 
	begin

            CASE add_uart(4 downto 0) IS
				WHEN "00000" => 
						RD_PN <=  pn_data (0);
				WHEN "00001" => 
						RD_PN <=  pn_data (1);
				WHEN "00010" => 
						RD_PN <=  pn_data (2);
				WHEN "00011" => 
						RD_PN <=  pn_add (0);
				WHEN "00100" => 
						RD_PN <=  pn_add (1);
				WHEN "00101" => 
						RD_PN <=  pn_add (2);	
				WHEN "00110" => 
						RD_PN <=  pn_ctrl (0);
				WHEN "00111" => 
						RD_PN <=  pn_ctrl (1);
				WHEN "01000" => 
						RD_PN <=  pn_ctrl (2);	
				WHEN "01001" => 
						RD_PN <=  pn_spare (0);
				WHEN "01010" => 
						RD_PN <=  pn_spare (1);
				WHEN "01011" => 
						RD_PN <=  pn_spare (2);	
				WHEN "01111" => --ERROR REG
						RD_PN <= error_reg;	
				WHEN "10000" => 
						RD_PN <=  pn_data_out (0);
				WHEN "10001" => 
						RD_PN <=  pn_data_out (1);
				WHEN "10010" => 
						RD_PN <=  pn_data_out (2);
				WHEN "10011" => 
						RD_PN <=  pn_add_out (0);
				WHEN "10100" => 
						RD_PN <=  pn_add_out (1);
				WHEN "10101" => 
						RD_PN <=  pn_add_out (2);	
				WHEN "10110" => 
						RD_PN <=  pn_ctrl_out (0);
				WHEN "10111" => 
						RD_PN <=  pn_ctrl_out (1);
				WHEN "11000" => 
						RD_PN <=  pn_ctrl_out (2);	
				WHEN OTHERS => 
					RD_PN <=  (OTHERS => '0');
			END CASE;
	end process; 
	
	--REG ICK/BG INPUTS AND STORE // DAISY CHAIN TEST
	
	BG_ICK_REG: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
				bgin_b_i <= (others=>'1');
				iack_b_i <= '1';
		elsif (CLK'event and CLK = '1') then
			bgin_b_i <= bgin_b;
			iack_b_i <= iack_b;
		end if;
	end process; 
	
	dct_reg(31 downto 8) <= X"0000"&"000"&iack_b_i& bgin_b_i;
	
	
	--REG ERROR ADDED 10/09/2014
	error_reg_p: process (CLK, Reset_n) 
	begin
	  if (Reset_n = '0') then
				error_reg <= (others=>'0');
		elsif (CLK'event and CLK = '1') then
			if error_flag = '1' then
				error_reg <= status_error;
			end if;
		end if;
	end process; 
	
	--VME BUS SYS CONTROLLER INSTANCE
	--BUS REQUEST ARBITRATION
	
	s_con_p: process (CLK, Reset_n) 
	begin
	    if (Reset_n = '0') then
			scon_b_i <= '1';
		elsif (CLK'event and CLK = '1') then
			scon_b_i <= scon_b;
		end if;
	end process; 
	
	lbgdir_metro 	<= vcc;
	lbgdir 			<= gnd;
	lbgoe 			<= gnd;
	s_con 			<= brqg_reg(4) and scon_b_i;

	lbgoe_metro_p: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					lbgoe_metro <= '1';
			elsif (CLK'event and CLK = '1') then
				--if (bus_rq = '0' or DeviceWants_Bus = '1')  and s_con = '0' then
				if (bus_rq = '0')  and (s_con = '0') then				
					lbgoe_metro <= vcc;
				else
					lbgoe_metro <= gnd;
				end if;
			end if;
		end process;
		
			
	BUS_REQ_P_INTERNAL: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					br_b_out_i <= (others => '1');
			elsif (CLK'event and CLK = '1') then
				if bus_rq = '0' or DeviceWants_Bus = '1' then --ADDED IRQ 03/11/2014
				--if bus_rq = '0' then
					br_b_out_i <= brqg_reg(3 downto 0);
				else
					br_b_out_i <= (others => '1');
				end if;
			end if;
		end process;
		
		
		
	---ERROR BUS CONDITION WHEN SYS CONTROLLER	15/04/2015
	
		TIMOUT_COUNTER_P: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					to_counter <= (others => '0');
			elsif (CLK'event and CLK = '1') then
				if as_b_in_reg = '1' then 
					to_counter <= (others => '0');
				elsif s_con = '0' and as_b_in_reg = '0' then
					-- change 17-4-2015
					-- Original sentence: to_counter <= to_counter + 1;
					-- We add this if sentence:
					if to_counter < TO_MAX then  
						to_counter <= to_counter + 1;
					else
						to_counter <= TO_MAX;
					end if;
				end if;
			end if;
		end process;
	
	
	berr_b_out_i <= '0' when (to_counter >= TO_MAX) else '1';

	
	sys_controller_i: vme_arbiter 
	  port map (
		br_b_in    	 	=> br_b_in,
		int_br_b_in 	=> vcc_vec(3 downto 0),
		bgout_b     	=> bgin_b_metro_i,
		bclr_b_out  	=> bclr_b_out_i,
		bclr_b_oen  	=> open,
		bbsy_b_in   	=> bbsy_b_in,
		bbsy_set    	=> open,
		bbsy_rst    	=> open,
		scon_b     		=> s_con,
		arb_fail   		=> open,
		arcr        	=> gnd_vec(7 downto 0),
		clk         	=> CLK,
		rst       		=> Reset_n
		
	);
	
			
	BUS_GRANT_P: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					bus_grant <= '0';
			elsif (CLK'event and CLK = '1') then
				if s_con = '1' then 
					if (bus_rq = '0' or DeviceWants_Bus = '1') and (bgin_b_i = brqg_reg(3 downto 0)) and (bgin_b_i /= "1111")then 
					--if (bus_rq = '0') and (bgin_b_i = brqg_reg(3 downto 0)) and (bgin_b_i /= "1111")then 
						bus_grant <= '1';
					else
						bus_grant <= '0';
					end if;
				else
					if (bus_rq = '0' or DeviceWants_Bus = '1') then --and (bgin_b_metro_i = brqg_reg(3 downto 0)) and (bgin_b_metro_i /= "1111") then 
					--if (bus_rq = '0') then --and (bgin_b_metro_i = brqg_reg(3 downto 0)) and (bgin_b_metro_i /= "1111") then 
						bus_grant <= '1';
					else
						bus_grant <= '0';
					end if;
				end if;
			end if;
		end process;
		
		bgin_b_metro <= bgin_b_metro_i and dct_reg(3 downto 0);
		
		
	--SYS CLK 12.5 MhZ ADDED 10/09/2014	

	PLL_SYSCLK_P: PLL_SYSCLK  
		port map(
		POWERDOWN	=> brqg_reg(4), -- not scon 
		CLKA 		=> CLK_IN,
		LOCK		=> lock_pll,
		GLA 		=> xsysclk,
		OADIVRST 	=> gnd
		 ) ;
	
	
-- MANGEMENT RESET ADDED 12/09/2014
	PLL_CLKIN_P: PLL_CLKIN  
		port map(
		POWERDOWN	=> vcc, 
		CLKA 		=> CLK_IN,
		LOCK		=> lock_pll_clkin,
		GLA 		=> CLK,
		OADIVRST 	=> gnd
		 ) ;
	
	Reset_n <= lock_pll_clkin and Reset_n_p;
			
	reset_circuit_P: reset_circuit 
		port map( 
			rst_p	=> Reset_n_in,
			CLK		=> CLK,
			rst_n	=> Reset_n_p
			);	

	--Power On Reset ADDED 10/09/2014	

	sysreset_b <= reset_vme when reset_vme = '0' else 'Z';
	reset_vme <= not time_rst;
	
			
	CONTER_TIME_RESET: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					tm_count_rst <= 0;
			elsif (CLK'event and CLK = '1') then
				if time_rst = '1' then
					tm_count_rst <= tm_count_rst + 1;
				else
					tm_count_rst <= 0;
				end if;
			end if;
		end process;	


	--Reset a  ADDED 25/09/2014	

	reseta <= not time_rst_a and reset_vme;
		
	CONTER_TIME_RESET_A: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					tm_count_rst_a <= 0;
			elsif (CLK'event and CLK = '1') then
				if time_rst_a = '1' then
					tm_count_rst_a <= tm_count_rst_a + 1;
				else
					tm_count_rst_a <= 0;
				end if;
			end if;
		end process;	
			
		
	-- MANGEMENT IRQ ADDED 03/11/2014 
	
	-- INTERRUPTER 
	
	uut_Interrupter: Interrupter port map(
    CLK         => CLK,    
    Reset_N     => Reset_n,
    IACK_IN_N   => iack_b_i,
    Address     => a_in_reg(3 downto 1),
    AS_N        => as_b_in_reg,
    DS0_N       => ds_b_in_reg(0),
    DS1_N       => ds_b_in_reg(1),
    SelectBits  => SelectBits,
    BusRelease  => BusRelease,
    IntLevel_X  => IntLevel_X,
    IntRequest  => IntRequest_fl,
    IRQ_N       => irq_b_out_i,
    IACK_Out_N  => iack_b_int,
    DTACK_N     => dtack_b_irq,
    DataOut     => d_vme_irq,
    Access_DTB  => Access_DTB);	
		
	IntLevel_X <= Interrupter_Reg(2 downto 0);
	SelectBits <= Select8 when Int_Handler_Reg(0) = '0' else Select16;
	BusRelease <= ROAK when Int_Handler_Reg(8) = '0' else RORA;
	
	IntRequest_reg_p: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					IntRequest_reg <= '0';
			elsif (CLK'event and CLK = '1') then
					IntRequest_reg <= IntRequest;
			end if;
		end process;	
		
	IntRequest_fl <= IntRequest and not IntRequest_reg;
	
	
	Access_DTB_reg_p: process (CLK, Reset_n)
		begin
			if (Reset_n = '0') then
					Access_DTB_reg <= '0';
			elsif (CLK'event and CLK = '1') then
					Access_DTB_reg <= Access_DTB;
			end if;
		end process;	
		
	end_int <= Access_DTB_reg and not Access_DTB;

	
	-- INT HANDLER
	
	uut_Int_Hander: Int_Handler port map(
   CLK         => CLK,    
   Reset_N     => Reset_N,
   IRQ_N       => irq_b_in,
   IACK_IN_N   => iack_b_i,
   DTACK_N     => dtack_b_in_reg,
   BERR_N      => berr_b_in,
   DataIn      => d_in_reg,
   ValidInts   => ValidInts,
   DTB_Granted => DTB_Granted,
   SelectBits  => SelectBits,
   BusRelease  => BusRelease,
   IACK_N      => iack_b_int_h,
   IACK_Out_N  => iack_b_int_h_out,
   AS_N        => as_b_irq,
   DS0_N       => ds_b_irq(1),
   DS1_N       => ds_b_irq(0),
   DeviceWants_Bus=>DeviceWants_Bus,
   Address     		=> a_b_irq,
   StatusID_irqh 	=> status_id_irqh
   );	
	

	
	ValidInts  <= Int_Handler_Reg(7 downto 1);
	DTB_Granted <= bus_grant when DeviceWants_Bus = '1' else '0';
	
	iack_b_metro <= iack_b_int_h and (iack_b_int and dct_reg(4)) and iack_b_int_h_out;
	
	
	
	
	
	
end RTL;