--
--	APSC Controller AMC FW Simulation Package
--
--		07/11/2012 - drhoms
--

library ieee;
use ieee.std_logic_1164.all;
use work.text_util_common_pkg.all;

package sim_AMC_pkg is
		
--------------------------------------------------------------------------------
-- Simulate transmission of data on serial 422 line.
-- 'Faults' are simulated by inverting the 3 LSBs.
procedure serial_tx(
        signal txd :    out std_logic;      -- txd data line
        baud_period :   in time;            -- baud period
        fault :         in boolean;         -- true to simulate a comms error
        data :          in std_logic_vector(7 downto 0)
    );

--------------------------------------------------------------------------------
-- Simulate reception of data on serial 422 line.
-- 'Faults' are simulated by inverting the 3 LSBs.
procedure serial_rx(
        signal rxd :    in std_logic;      -- txd data line
        baud_period :   in time;           -- baud period
        fault :         out boolean;       -- true on comms error
        data :          out std_logic_vector(7 downto 0)
    );	
	
	
end package;

package body sim_AMC_pkg is

procedure serial_tx(
        signal txd :    out std_logic;
        baud_period :   in time;
        fault :         in boolean;
        data :          in std_logic_vector(7 downto 0)
    ) is
variable i : integer;
variable tx_data :  std_logic_vector(7 downto 0);
variable parity : std_logic := '0';
begin

    tx_data := data;

    -- transmit start bit
    txd <= '0';
    wait for baud_period;

    -- compute parity
    for i in 0 to 7 loop
        parity := parity xor tx_data(i);
    end loop;

    -- if we are to simulate a fault, invert the 3 LSBs
    if fault then
        tx_data := tx_data xor "00000111";
    end if;

    -- transmit data bits
    for i in 0 to 7 loop
        txd <= tx_data(i);
        wait for baud_period;
    end loop;

    -- transmit parity bit: for EVEN parity, just send the computed parity bit
    txd <= parity;
    wait for baud_period;

    --transmit 1 (hardcoded) stop bit
    txd <= '1';
    wait for baud_period*1;

end procedure serial_tx;


procedure serial_rx(
        signal rxd :    in std_logic;      -- txd data line
        baud_period :   in time;           -- baud period
        fault :         out boolean;       -- true on error
        data :          out std_logic_vector(7 downto 0)
    ) is
variable sr : std_logic_vector(7 downto 0);
variable i : integer;
variable parity, rx_parity : std_logic;
begin
    fault := false;
    parity := '0';
    
    -- make sure the line is high so we catch the first falling edge and
    -- synchronize to it.
    -- this is gonna block the parent process.
    if rxd/='1' then
        wait until rxd='1';
    end if;

    -- wait for start bit...
    wait until rxd'event and rxd='0';

    -- ...and get it
    wait for baud_period/2;
    if rxd /= '0' then
        -- spurious start bit; quit with error
        log_fault("OPBUS", "Spurious start bit");
        fault := true;
        return;
    end if;
    
    -- get incoming bits, LSB first
    for i in 0 to 7 loop
        wait for baud_period;
        sr(i) := rxd;
        parity := parity xor sr(i);
    end loop;
    
    -- get parity bit
    wait for baud_period;
    rx_parity := rxd;
    
    -- skip get stop bit
    wait for baud_period;
    if rxd /= '1' then
        -- spurious start bit; quit with error
        log_fault("OPBUS", "Stop bit is not high");
        fault := true;
        return;
    end if;

    -- if parity does not match, quit with error
    if rx_parity /= parity then
        log_fault("OPBUS", "Parity error");
        fault := true;
        return;
    end if;
    
    data := sr;
end serial_rx;

	
end package body;