-- ****************************************************
-- Created by: ACTgen 9.1.0.18
-- Date : Thu Jul 17 12:10:18 2014 
-- Parameters:
--   WIDTH:23
--   DIRECTION:UP
--   CLK_EDGE:RISE
--   CLR_POLARITY:1
--   PRE_POLARITY:2
--   EN_POLARITY:2
--   LD_POLARITY:2
--   TCNT_POLARITY:1
-- ****************************************************

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity Counter_behave is
  port(Clock : in std_logic;
       Q : out std_logic_vector(22 downto 0);
       Aclr : in std_logic;
       Tcnt : out std_logic);
end Counter_behave;

architecture behavioral of Counter_behave is

  signal Qaux : UNSIGNED(22 downto 0);

begin

  process(Clock, Aclr)
  begin

    if (Aclr = '1') then
      Qaux <= (others => '0');
    elsif (Clock'event and Clock = '1') then
      Qaux <= Qaux + 1;
    end if;

  end process;

  Q <= std_logic_vector(Qaux);

  process(Qaux)
    variable aux : std_logic;
  begin

    aux := '1';
    for I in 0 to 22 loop
      if (Qaux(I) = '0') then
        aux := '0';
      end if;
    end loop;

    Tcnt <= aux;

  end process;

end behavioral;
