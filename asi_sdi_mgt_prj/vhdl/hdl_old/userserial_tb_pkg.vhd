-- userserial_tb_pkg.vhd
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

use work.txt_util.all;

package userserial_tb_pkg is

	subTYPE slv_7dt0 IS std_logic_vector (7 downto 0);
	TYPE BYTE_vector IS ARRAY (NATURAL RANGE <>) OF slv_7dt0;
	
	--------------------------------------------------------------------------------
	-- Simulate transmission of data on serial 422 line.
	-- 'Faults' are simulated by inverting the 3 LSBs.
	procedure serial_tx(
			signal txd :    out std_logic;      -- txd data line
			baud_period :   in time;            -- baud period
			fault :         in boolean;         -- true to simulate a comms error
			data :          in std_logic_vector(7 downto 0)
		);
	
	--------------------------------------------------------------------------------
	-- Simulate reception of data on serial 422 line.
	-- 'Faults' are simulated by inverting the 3 LSBs.
	procedure serial_rx(
			signal rxd :    in std_logic;      -- txd data line
			baud_period :   in time;           -- baud period
			fault :         out boolean;       -- true on comms error
			data :          out std_logic_vector(7 downto 0)
		);
		
	-- converts a character into an ASCII character
	function char_to_ascii (ch : in character) return slv_7dt0;
    
	-- converts an integer into an control ASCII character representation
    function ctrl_ascii(int: integer) return string;

end userserial_tb_pkg;

package body userserial_tb_pkg is

	procedure serial_tx(
			signal txd :    out std_logic;
			baud_period :   in time;
			fault :         in boolean;
			data :          in std_logic_vector(7 downto 0)
		) is
	variable i : integer;
	variable tx_data :  std_logic_vector(7 downto 0);
	variable parity : std_logic := '0';
	begin
	
		tx_data := data;
	
		-- transmit start bit
		txd <= '0';
		wait for baud_period;
	
		-- compute parity
		for i in 0 to 7 loop
			parity := parity xor tx_data(i);
		end loop;
	
		-- if we are to simulate a fault, invert the 3 LSBs
		if fault then
			tx_data := tx_data xor "00000111";
		end if;
	
		-- transmit data bits
		for i in 0 to 7 loop
			txd <= tx_data(i);
			wait for baud_period;
		end loop;
	
		-- transmit parity bit: for EVEN parity, just send the computed parity bit
		txd <= parity;
		wait for baud_period;
	
		--transmit 1 (hardcoded) stop bit
		txd <= '1';
		wait for baud_period*1;
	
	end procedure serial_tx;
	
	
	procedure serial_rx(
			signal rxd :    in std_logic;      -- txd data line
			baud_period :   in time;           -- baud period
			fault :         out boolean;       -- true on error
			data :          out std_logic_vector(7 downto 0)
		) is
	variable sr : std_logic_vector(7 downto 0);
	variable i : integer;
	variable parity, rx_parity : std_logic;
	begin
		fault := false;
		parity := '0';
		
		-- make sure the line is high so we catch the first falling edge and
		-- synchronize to it.
		-- this is gonna block the parent process.
		if rxd/='1' then
			wait until rxd='1';
		end if;
	
		-- wait for start bit...
		wait until rxd'event and rxd='0';
	
		-- ...and get it
		wait for baud_period/2;
		if rxd /= '0' then
			-- spurious start bit; quit with error
			print("serial_rx : Spurious start bit");
			fault := true;
			return;
		end if;
		
		-- get incoming bits, LSB first
		for i in 0 to 7 loop
			wait for baud_period;
			sr(i) := rxd;
			parity := parity xor sr(i);
		end loop;
		
		-- get parity bit
		wait for baud_period;
		rx_parity := rxd;
		
		-- skip get stop bit
		wait for baud_period;
		if rxd /= '1' then
			-- spurious start bit; quit with error
			print("serial_rx : Stop bit is not high");
			fault := true;
			return;
		end if;
	
		-- if parity does not match, quit with error
		if rx_parity /= parity then
			print("serial_rx : Parity error");
			fault := true;
			return;
		end if;
		
		data := sr;
	end serial_rx;
	
	function char_to_ascii (
		ch :                in character
		) return slv_7dt0 is
	begin
		return std_logic_vector(to_unsigned(character'pos(ch),8));
	end function char_to_ascii;
	
   function ctrl_ascii(int: integer) return string is
    variable c: string(1 to 5);
   begin
        case int is
			when 0 => 	c:="'NUL'";
			when 1 => 	c:="'SOH'";
			when 2 => 	c:="'STX'";
			when 3 => 	c:="'ETX'";
			when 4 => 	c:="'EOT'";
			when 5 => 	c:="'ENQ'";
			when 6 => 	c:="'ACK'";
			when 7 => 	c:="'BEL'";
			when 8 => 	c:="'BS' ";
			when 9 => 	c:="'HT' ";
			when 10 => 	c:="'LF' ";
			when 11 => 	c:="'VT' ";
			when 12 => 	c:="'FF' ";
			when 13 => 	c:="'CR' ";
			when 14 => 	c:="'SO' ";
			when 15 => 	c:="'SI' ";
			when 16 => 	c:="'DLE'";
			when 17 => 	c:="'DC1'";
			when 18 => 	c:="'DC2'";
			when 19 => 	c:="'DC3'";
			when 20 => 	c:="'DC4'";
			when 21 => 	c:="'NAK'";
			when 22 => 	c:="'SYN'";
			when 23 => 	c:="'ETB'";
			when 24 => 	c:="'CAN'";
			when 25 => 	c:="'EM' ";
			when 26 => 	c:="'SUB'";
			when 27 => 	c:="'ESC'";
			when 28 => 	c:="'FSP'";
			when 29 => 	c:="'GSP'";
			when 30 => 	c:="'RSP'";
			when 31 => 	c:="'USP'";	
			when 127 => c:="'DEL'";
			when others => null;
        end case;
        return c;
    end ctrl_ascii;	

end userserial_tb_pkg;