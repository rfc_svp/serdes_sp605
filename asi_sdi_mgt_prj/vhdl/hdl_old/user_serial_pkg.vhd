-- user_serial_pkg.vhd
library IEEE;
use IEEE.std_logic_1164.all;

package user_serial_pkg is

	constant USER_UART_ClkFreq 		: INTEGER	:= 48000000;	-- clock freq (Hz)
	constant USER_UART_BaudRate		: INTEGER	:= 9600;		-- bps
	constant USER_UART_bit8			: std_logic	:= '1';     	-- byte = '1' 
	constant USER_UART_parity_en	: std_logic	:= '1';     	-- enable = '1' 
	constant USER_UART_odd_n_even	: std_logic	:= '0';     	-- odd = '1' even = '0'

	--
	type ACCESS_VALID_UART_type is (DEV_FREE,DEV_DATA_OK,DEV_DATA_FL,DEV_BUSY);
	
	-- ASCII TO HEX DECODE
	type useruart_ascii_char is (
		x0,x1,x2,x3,x4,x5,x6,x7,x8,x9,
		uA,uB,uC,uD,uE,uF,
		lA,lB,lC,lD,lE,lF,
		uR,lR,uW,lW,
		COMA,QUES,
		Y,N,T,
		LineFeed,CarriageReturn
	);
	
	type useruart_ascii_type is array (useruart_ascii_char'left to useruart_ascii_char'right) of  std_logic_vector (7 downto 0);
	
	constant useruart_ascii : useruart_ascii_type := (	
		x0	=> x"30", 	-- dec 48
		x1	=> x"31", 	-- dec 49
		x2	=> x"32", 	-- dec 50
		x3	=> x"33", 	-- dec 51
		x4	=> x"34", 	-- dec 52
		x5	=> x"35", 	-- dec 53
		x6	=> x"36", 	-- dec 54
		x7	=> x"37", 	-- dec 55
		x8	=> x"38", 	-- dec 56
		x9	=> x"39", 	-- dec 57
		-- uppercase letter
		uA	=> x"41", 	-- dec 65
		uB	=> x"42", 	-- dec 66
		uC	=> x"43", 	-- dec 67
		uD	=> x"44", 	-- dec 68
		uE	=> x"45", 	-- dec 69
		uF	=> x"46", 	-- dec 70
		-- lower case letter
		lA	=> x"61", 	-- dec 97
		lB	=> x"62", 	-- dec 98
		lC	=> x"63", 	-- dec 99
		lD	=> x"64", 	-- dec 100
		lE	=> x"65", 	-- dec 101
		lF	=> x"66", 	-- dec 102
		-- Operation symbol
		uR	=> x"52", 	-- dec 82
		lR	=> x"72", 	-- dec 114
		uW	=> x"57", 	-- dec 87
		lW	=> x"77", 	-- dec 119
		-- separator symbol
		COMA	=> x"2C", 	-- dec 44
		QUES	=> x"3F", 	-- dec 63
		-- validate symbol
		Y	=> x"59", 	-- dec 89
		N	=> x"4E", 	-- dec 78
		T	=> x"54", 	-- dec 84
		-- carrier Return and Line Feed
		LineFeed		=> x"0A", 	-- dec 10
		CarriageReturn	=> x"0D" 	-- dec 13
	);
	
	-- types
	TYPE nibble_vector IS ARRAY (NATURAL RANGE <>) OF std_logic_vector(3 downto 0);

end user_serial_pkg;

package body user_serial_pkg is

end user_serial_pkg;