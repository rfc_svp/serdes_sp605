--------------------------------------------------------------------------------
-- Company: Microsemi Corporation
--
-- File: counter.vhd
-- File history:
--      Initial version
--      
-- Description: 
--
-- A 9-bit counter that starts counting from a known state set by the reset generated from the POR/PPR circuit.
-- It validates that the counter is starting from the known state only because of pulse generated due to the POR/PPR circuit.
-- POR/PPR circuit also has been tested by setting more number of flip-flops to a known state to confirm that
-- all the flip-flops get reset to known state because of the POR/PPR pulse and not because of FFs coming-up in random state. 
--
-- Connect a weak pull-up resistor to the BIBUF IO PAD.
--
-- Targeted device: IGLOO M1AGL1000V2 484FPGA
--
-- Author: Application Engineering
--
--------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

entity mycounter is
port (
     CLK : in std_logic;
     Resetn_in: in std_logic;
     Resetn_out: out std_logic
);
end mycounter;

architecture mycounter_arch of mycounter is

   signal count : std_logic_vector(8 downto 0);

begin
   
   process(CLK,Resetn_in)
   begin
   if(Resetn_in = '0') then
   count <= "101000000";
   elsif (CLK'event and CLK='1') then
   if(count(8 downto 6) = "101") then
   count <= count + 1;
   end if;
   end if;
   end process;

   Resetn_out <= not count(6);   

end mycounter_arch;