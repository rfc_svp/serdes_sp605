         

#### refresh precompiled libraries

vmap mylib mylib
vmap work work

vlog -work mylib -refresh
vcom -work mylib -refresh
vlog -work work -refresh
vcom -work work -refresh

### compile test_bench files


vlog +acc  "testbench/bs_10_10/rot20.v"
vlog +acc  "testbench/bs_10_10/maskencoder.v"
vlog +acc  "testbench/bs_10_10/control.v"
vlog +acc  "testbench/prbs_chk_par_10.v"
vlog +acc  "testbench/prbsgen_ser.v"
vlog +acc  "testbench/deser_id_20.v"
vlog +acc  "testbench/bs_10_10/bshift10to10.v"
vlog +acc  "testbench/tb_sim_dru_jitter.v"
vlog +acc  "C:/Xilinx/10.1/ISE/verilog/src/glbl.v"


#### start simulation and add waves

vsim -t 1fs   -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -L secureip -lib work tb_sim_dru_jitter glbl

#### add waves

do {wave.do}
view wave
view structure
view signals

#### run simulation

run 600 us


