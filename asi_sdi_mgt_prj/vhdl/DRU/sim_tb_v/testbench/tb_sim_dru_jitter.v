///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2003 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.1
//  \   \         Application : Non Integer DRU
//  /   /         Filename: tb_sim_dru_jitter.v
// /___/   /\     Timestamp: May 1 2007
// \   \  /  \
//  \___\/\___\
//
//Device: Virtex 5 LXT/FXT/TXT
//Design Name: tb_sim_dru_jitter.v
//Purpose: This is the test bench to simulate the NIDRU at different rates.
//Authors: Paolo Novellini, Giovanni Guasti. 
//
// Fast Ethernet: 125Mbit/s
// 97.728 Mbit/s
// OC1: 51.84 Mbit/s
// OC3: 155.52 Mbit/s
// E4: 139.264 Mbit/s
// Proprietary: 510 Mbit/s
// Proprietary: 1000 Mbit/s
// Proprietary: 1250 Mbit/s
//    
///////////////////////////////////////////////////////////////////////////////

`timescale 1fs / 1fs

module tb_sim_dru_jitter;

	
	
	wire [36:0] CENTER_F;
	wire [4:0] G1;
	wire [4:0] G1_P;
	wire [4:0] G2;
	wire RT_ERR;
	
	
	reg RST;
  reg CLK_HF = 0;
  reg clk_dt = 0;
  wire clk_dt_del;
  reg rit = 0;
	
	wire [20:0] PH_OUT;
	wire [31:0] CTRL;
	wire [3:0] SAMV;
	wire [9:0] SAM;
	wire [9:0] DOUT10;
	wire [19:0] DT_OUT;
	wire CLK_OUT;
	wire DV10;
	

	dru uut (
		.DT_IN(DT_OUT), 
		.CENTER_F(CENTER_F), 
		.G1(G1), 
		.G1_P(G1_P), 
		.G2(G2), 
		.CLK(CLK_OUT), 
		.PH_OUT(PH_OUT), 
		.CTRL(CTRL), 
		.RST(RST), 
		.SAMV(SAMV), 
		.SAM(SAM)
	);

  prbsgen_ser prbsgen_ser_inst (
    .CLK(clk_dt_del), 
    .RST(RST), 
    .ERR(1'b0), 
    .PRBSOUT(PRBSOUT)
    ); 
    
  prbs_chk_par_10 prbs_chk_par_10_inst (
    .CLK(CLK_OUT), 
    .RST(RST), 
    .CHK_OKKO(), 
    .RT_ERR(RT_ERR), 
    .EN(DV10), 
    .RES_ALARM(1'b0), 
    .DT_IN(DOUT10)
    ); 
      
deser_id_20 deser_id_20_inst (
    .CLK(CLK_HF), 
    .RST(RST), 
    .CLK_OUT(CLK_OUT), 
    .DT_IN(PRBSOUT), 
    .DT_OUT(DT_OUT)
    );
    
bshift10to10 bshift10to10_inst (
    .CLK(CLK_OUT), 
    .RST(RST), 
    .DIN(SAM), 
    .DV(SAMV), 
    .DV10(DV10), 
    .DOUT10(DOUT10)
    );
    
    assign G1 = 5'b01011;
		assign G1_P = 5'b01011;
		assign G2 = 5'b01011;


	initial begin
	  
	  rit = 0;
    RST = 0;
		
		// Wait 100 ns for global reset to finish
		#100000000;
    RST = 1'b1;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    #2000000000;
    rit = 1;
   end
   
   assign clk_dt_del = (!clk_dt) ^ rit ;   // this clock generates data @ 125Mbit/s with a step in the phase. 

////////////////////////////////////////////////////////////////////////////////////////////
// Use these controls to use 125Mbit/s (Fast Ethernet) with 155.52MHz refclk
////////////////////////////////////////////////////////////////////////////////////////////
	          
 assign CENTER_F = 37'b0000011001101110000101110010110101001;
 always begin #160751 assign CLK_HF = ~(CLK_HF); end 		// 3.11G // Oversampling ratio: 24.88X
// always begin #4000000 assign clk_dt = ~(clk_dt); end		// The clock generating the data is nominal
 always begin #4000400 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -100ppm
// always begin #4000100 assign clk_dt = ~(clk_dt); end 	// The clock generating the data is -25ppm
// always begin #3999900 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +25ppm
// always begin #4000800 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -200ppm
// always begin #3999200 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +200ppm
// always begin #3999600 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +100ppm
 
 
   
 
////////////////////////////////////////////////////////////////////////////////////////////
// Use these controls to use 51.84Mbit/s (OC1) with 155.52MHz refclk
////////////////////////////////////////////////////////////////////////////////////////////
	          
//  assign CENTER_F = 37'b0000001010101010101010101010101010101;
//  always begin #160751 assign CLK_HF = ~(CLK_HF); end 		// 3.11G // Oversampling ratio: 60X
// always begin #9645100 assign clk_dt = ~(clk_dt); end		// The clock generating the data is nominal
// always begin #9646000 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -100ppm
// always begin #9645300 assign clk_dt = ~(clk_dt); end 	// The clock generating the data is -25ppm
// always begin #9647000 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -200ppm
// always begin #9643100 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +200ppm
// always begin #9644100 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +100ppm


////////////////////////////////////////////////////////////////////////////////////////////
// Use these controls to use 155.52Mbit/s (OC3) with 155.52MHz refclk
////////////////////////////////////////////////////////////////////////////////////////////
	          
//  assign CENTER_F = 37'b0000100000000000000000000000000000000;
//  always begin #160751 assign CLK_HF = ~(CLK_HF); end 		// 3.11G // Oversampling ratio: 20X
// always begin #3215000 assign clk_dt = ~(clk_dt); end		// The clock generating the data is nominal
// always begin #3215300 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -100ppm
// always begin #3215100 assign clk_dt = ~(clk_dt); end 	// The clock generating the data is -25ppm
// always begin #3215700 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -200ppm
// always begin #3214400 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +200ppm
// always begin #3214700 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +100ppm

  
////////////////////////////////////////////////////////////////////////////////////////////
// Use these controls to use 139.264Mbit/s (PDH E4-ITUTG703) with 155.52MHz refclk
////////////////////////////////////////////////////////////////////////////////////////////
	          
//  assign CENTER_F = 37'b0000011100101001111011011110000100111;
//  always begin #160751 assign CLK_HF = ~(CLK_HF); end 		// 3.11G // Oversampling ratio: 22.33X
// always begin #3590300 assign clk_dt = ~(clk_dt); end		// The clock generating the data is nominal
// always begin #3590600 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -100ppm
// always begin #3590400 assign clk_dt = ~(clk_dt); end 	// The clock generating the data is -25ppm
// always begin #3591000 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -200ppm
// always begin #3589600 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +200ppm
// always begin #3590000 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +100ppm
 
 
  
////////////////////////////////////////////////////////////////////////////////////////////
// Use these controls to use 510Mbit/s (Proprietary) with 155.52MHz refclk
////////////////////////////////////////////////////////////////////////////////////////////
	                        
// assign CENTER_F = 37'b0001101000111100000011001010010001011;
// always begin #160751 assign CLK_HF = ~(CLK_HF); end 		// 3.11G // Oversampling ratio: 6.1X
// always begin #0980392 assign clk_dt = ~(clk_dt); end		// The clock generating the data is nominal
// always begin #0980490 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -100ppm
// always begin #0980417 assign clk_dt = ~(clk_dt); end 	// The clock generating the data is -25ppm
// always begin #0980588 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -200ppm
// always begin #0980196 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +200ppm
// always begin #0980294 assign clk_dt = ~(clk_dt); end		// The clock generating the data is +100ppm



////////////////////////////////////////////////////////////////////////////////////////////
// Use these controls to use 1000Mbit/s (Proprietary) with 155.52MHz refclk
////////////////////////////////////////////////////////////////////////////////////////////
	          
//  assign CENTER_F = 37'b0011001101110000101110010110101001100;
//  always begin #160751 assign CLK_HF = ~(CLK_HF); end 		// 3.11G // Oversampling ratio: 3.11X
//  always begin #0500000 assign clk_dt = ~(clk_dt); end		// The clock generating the data is nominal

////////////////////////////////////////////////////////////////////////////////////////////
// Use these controls to use 1000Mbit/s (Proprietary) with 155.52MHz refclk
////////////////////////////////////////////////////////////////////////////////////////////
	          
//  assign CENTER_F = 37'b0010000001101000010010111101101000010;
//  always begin #160751 assign CLK_HF = ~(CLK_HF); end 		// 3.11G // Oversampling ratio: 3.11X
//  always begin #0793650 assign clk_dt = ~(clk_dt); end		// The clock generating the data is nominal




////////////////////////////////////////////////////////////////////////////////////////////
// Use these controls to use 1250Mbit/s (Proprietary) with 155.52MHz refclk
////////////////////////////////////////////////////////////////////////////////////////////
	                      
// assign CENTER_F = 37'b0100000001001100111001111100010100000;
// always begin #160751 assign CLK_HF = ~(CLK_HF); end 		// 3.11G // Oversampling ratio: 2.49X
// always begin #0400000 assign clk_dt = ~(clk_dt); end		// The clock generating the data is nominal
// always begin #0400004 assign clk_dt = ~(clk_dt); end		// The clock generating the data is -10ppm
// always begin #0399996 assign clk_dt = ~(clk_dt); end 	// The clock generating the data is +10ppm


endmodule

