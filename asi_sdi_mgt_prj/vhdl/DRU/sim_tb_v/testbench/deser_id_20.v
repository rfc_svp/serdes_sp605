///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: deser_id_20.v
// /___/   /\     Timestamp: May 1 2007
// \   \  /  \
//  \___\/\___\
//
//Device: Virtex 5 LXT/FXT/TXT
//Design Name: deser_id_20.v
//Purpose: Prbs generator with error insertion.
//Authors: Paolo Novellini, Giovanni Guasti. 
//
//    
///////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module deser_id_20 

(
    CLK,
    RST,
    CLK_OUT,
    DT_IN,
    DT_OUT
);
	

//***********************************Ports Declaration*******************************

    input           CLK;
    input           RST;
    input           DT_IN;
    output          CLK_OUT;
    output[19:0]     DT_OUT;
    
//************************** Register Declarations ****************************

   reg[19:0]   dt_aux_1;
   reg[19:0]   dt_aux_2;
   reg[5:0]    cnt;
        
//**************************** Wire Declarations ******************************

assign CLK_OUT = cnt[4];
assign DT_OUT = dt_aux_2;

always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
      begin
         dt_aux_1 <= 20'b00000000000000000000;
      end
      else 
      begin
         dt_aux_1[19] <= DT_IN;
         dt_aux_1 [18:0] <= dt_aux_1[19:1];
      end

always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
         dt_aux_2 <= 20'b00000000000000000000;
      else 
      begin
         dt_aux_2 <= dt_aux_1;
      end

always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
      begin
         cnt <= 6'b000000;
      end
      else
      begin 
        if (cnt == 6'b010011)
        begin
          cnt <= 6'b000000;
        end
        else  
        begin 
          cnt <= cnt + 6'b000001;
        end
       end 
endmodule