///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2003 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: prbsgen_ser.v
// /___/   /\     Timestamp: May 1 2007
// \   \  /  \
//  \___\/\___\
//
//Device: Virtex 5 LXT/FXT/TXT
//Design Name: prbsgen_ser.v
//Purpose: Prbs generator with error insertion.
//Authors: Paolo Novellini, Giovanni Guasti. 
//
//    
///////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module prbsgen_ser 

(
    CLK,
    RST,
    ERR,
    PRBSOUT
);
	

//***********************************Ports Declaration*******************************

    input           CLK;
    input           RST;
    input           ERR;
    output          PRBSOUT;
    
//************************** Register Declarations ****************************

   reg [31:0]  x;
   reg         err_r;
   reg         err_rr;
   reg         err_sync_r;
   reg         PRBSOUT;
        
//**************************** Wire Declarations ******************************

   wire err_sync;

always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
      begin
         PRBSOUT <= 1'b0;
      end
      else 
      begin
         PRBSOUT <= x[0] ^ err_sync_r;
      end

always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
         x <= 32'h55555555;
      else 
      begin
         x[0] <= x[14] ^ x[15];
         x[15:1] <= x[14:0];
      end

always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
      begin
         err_r <= 1'b0;
         err_rr <= 1'b0;
         err_sync_r <= 1'b0;
      end
      else
      begin 
         err_r <= ERR;
         err_rr <= err_r;
         err_sync_r <= err_sync;
      end

assign err_sync = err_r & !(err_rr);

endmodule

