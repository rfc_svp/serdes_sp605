onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /tb_sim_dru_jitter/RT_ERR
add wave -noupdate -format Logic /tb_sim_dru_jitter/PRBSOUT
add wave -noupdate -format Analog-Step -height 100 -offset 1000000.0 -radix decimal -scale 5.0000000000000002e-005 /tb_sim_dru_jitter/CTRL
add wave -noupdate -format Literal /tb_sim_dru_jitter/SAMV
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {16795746733 fs} 0}
configure wave -namecolwidth 269
configure wave -valuecolwidth 43
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {399252500 ps} {505302500 ps}
