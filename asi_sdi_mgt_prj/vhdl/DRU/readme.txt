*******************************************************************************
** �2009 Xilinx, Inc. All Rights Reserved.
** Confidential and proprietary information of Xilinx, Inc. 
*******************************************************************************
**   ____  ____ 
**  /   /\/   / 
** /___/  \  /   Vendor: Xilinx 
** \   \   \/    Version: 1.1
**  \   \        Filename:  
**  /   /        Date Last Modified: 12 - Oct - 2009 
** /___/   /\    Date Created: 13 - May - 2006
** \   \  /  \ 
**  \___\/\___\ 
** 
**  Device: 
**  Purpose: Non - Integer data recovery unit(NI-DRU) for Virtex 5.
**  Reference: 
**     v1.1 :  1 - The verilog testbench simulates correctly the phase reaquisition. 
**             2 - In paragraph "Selection REFCLK frequency" setup and hold has been replaced by metastability.
**             3 - Fig.3 has been updated.
**             4 - Note 1 in fig.3 was showing an incorrect refclk frequency.
**             5 - G3 has been replaced with the correct name, G1_P
**             6 - A bug in the verilog version of the Barrel Shifter has been fixed.
**             7 - A bug showing up for oversampling ratios between 4 and 5 has been identified and fixed.
**             8 - A bug in the Verilog version of the mask encoder has been fixed.
**     v1.0 :  Creation
**   
*******************************************************************************
**
**  Disclaimer: 
**
**      This disclaimer is not a license and does not grant any rights to the materials 
**      distributed herewith. Except as otherwise provided in a valid license issued to you 
**      by Xilinx, and to the maximum extent permitted by applicable law: 
**      (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, 
**      AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, 
**      INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR 
**      FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable (whether in contract 
**      or tort, including negligence, or under any other theory of liability) for any loss or damage 
**      of any kind or nature related to, arising under or in connection with these materials, 
**      including for any direct, or any indirect, special, incidental, or consequential loss 
**      or damage (including loss of data, profits, goodwill, or any type of loss or damage suffered 
**      as a result of any action brought by a third party) even if such damage or loss was 
**      reasonably foreseeable or Xilinx had been advised of the possibility of the same.


**  Critical Applications:
**
**		Xilinx products are not designed or intended to be fail-safe, or for use in any application 
**		requiring fail-safe performance, such as life-support or safety devices or systems, 
**		Class III medical devices, nuclear facilities, applications related to the deployment of airbags,
**		or any other applications that could lead to death, personal injury, or severe property or 
**		environmental damage (individually and collectively, "Critical Applications"). Customer assumes 
**		the sole risk and liability of any use of Xilinx products in Critical Applications, subject only 
**		to applicable laws and regulations governing limitations on product liability.

**  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.

*******************************************************************************

This readme describes how to use the files that come with XAPP875.

*******************************************************************************

** IMPORTANT NOTES **

1) Designed with Modelsim SE 6.2g

1) The file dru_20/dru.ngc uses 1 dsp slice, dru_20/dru_no_dsp.ngc uses no DSP slices. The fuctionality is the same. 

*******************************************************************************

To incorporate the insert name here module into an ISE design project:

Verilog flow:

1) To implement the NIDRU in your netlist, incorporate the dru.ngc file into your netlist and follow the ISE implementation flow. 
2) To simulate the NIDRU in modelsim, just use the precompiled libraries in sim_lib into the test bench tb_sim_dru_jitter.v.

VHDL flow:

1) To implement the DRU in your netlist, incorporate the dru.ngc file into your netlist and follow the ISE implementation flow. 
2) To simulate the NIDRU in modelsim, just use the precompiled libraries in sim_lib into the test bench tb_sim_dru_jitter.vhdl.

Directory content:

cspro_prj: chipscope pro project.
dru_20: the NI-DRU with wnd without DSP for integration.
scripts: in modelsim, use wave.do to configure the viewer
sim_lib: use these libraries to simulate the DRU with with Modelsim.
sim_tb_v: use this testbench and the submodules to simulate the NIDRU in verilog
sim_tb_vhdl: use this testbench and the submodules to simulate the NIDRU in vhdl
hw_demo_v: hw demo sources in verilog
hw_demo_vhdl: hw demo sources in vhdl

