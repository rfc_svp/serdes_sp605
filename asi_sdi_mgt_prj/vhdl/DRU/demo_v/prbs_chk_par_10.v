///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2003 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Parallel prbs checker
//  /   /         Filename: prbs_chk_par.v
// /___/   /\     Timestamp: May 1 2007
// \   \  /  \
//  \___\/\___\
//
//Device: Virtex 5 LXT/FXT/TXT
//Design Name: prbs_chk_par.v
//Purpose: It checks a prbs on deserialized data.
//Authors: Paolo Novellini, Giovanni Guasti. 
//
//    
///////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module prbs_chk_par_10 

(
    CLK,
    RST,
    CHK_OKKO,
    RT_ERR,
    EN,
    RES_ALARM,
    DT_IN
);
	

//***********************************Ports Declaration*******************************

    input           CLK;
    input           RST;
    output          CHK_OKKO;
    output          RT_ERR;
    input           EN;
    input           RES_ALARM;
    input [9:0]     DT_IN;
    
//************************** Register Declarations ****************************

   reg [15:0]  dt_r;
   reg [9:0]   dt_rr;
   reg         chk_okko_int;
        
//**************************** Wire Declarations ******************************

   wire okko_int;
   

always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
      begin
         dt_r <= 16'b0;
      end
      else
         if (EN == 1'b1) 
            begin
               dt_r [15]<= DT_IN [9];
               dt_r [14]<= DT_IN [8];
               dt_r [13]<= DT_IN [7];
               dt_r [12]<= DT_IN [6];
               dt_r [11]<= DT_IN [5];
               dt_r [10]<= DT_IN [4];
               dt_r [9]<= DT_IN [3];
               dt_r [8]<= DT_IN [2];
               dt_r [7]<= DT_IN [1];
               dt_r [6]<= DT_IN [0];
               dt_r [5]<= dt_r [15];
               dt_r [4]<= dt_r [14];
               dt_r [3]<= dt_r [13];
               dt_r [2]<= dt_r [12];
               dt_r [1]<= dt_r [11];
               dt_r [0]<= dt_r [10];   
            end


always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
      begin
         dt_rr <= 10'b0;
      end
      else
         if (EN == 1'b1)
         begin
            dt_rr[0] <= DT_IN[0] ^ dt_r[0] ^ dt_r[1];
		        dt_rr[1] <= DT_IN[1] ^ dt_r[1] ^ dt_r[2];
		        dt_rr[2] <= DT_IN[2] ^ dt_r[2] ^ dt_r[3];
		        dt_rr[3] <= DT_IN[3] ^ dt_r[3] ^ dt_r[4];
		        dt_rr[4] <= DT_IN[4] ^ dt_r[4] ^ dt_r[5];
		        dt_rr[5] <= DT_IN[5] ^ dt_r[5] ^ dt_r[6];
		        dt_rr[6] <= DT_IN[6] ^ dt_r[6] ^ dt_r[7];
		        dt_rr[7] <= DT_IN[7] ^ dt_r[7] ^ dt_r[8];
		        dt_rr[8] <= DT_IN[8] ^ dt_r[8] ^ dt_r[9];
		        dt_rr[9] <= DT_IN[9] ^ dt_r[9] ^ dt_r[10];    
         end


always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
      begin
         chk_okko_int <= 1'b0;
      end
      else
         if (EN == 1'b1) 
         begin
            chk_okko_int <= okko_int | ((chk_okko_int & !RES_ALARM));
         end

assign okko_int = dt_rr[0]|dt_rr[1]|dt_rr[2]|dt_rr[3]|dt_rr[4]|dt_rr[5]|dt_rr[6]|dt_rr[7]|dt_rr[8]|dt_rr[9];
assign CHK_OKKO = chk_okko_int;
assign RT_ERR = okko_int;

endmodule
