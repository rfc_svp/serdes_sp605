///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: rot32.v
// /___/   /\     Timestamp: October 20 2008
// \   \  /  \
//  \___\/\___\
//
///////////////////////////////////////////////////////////////////////////////
//Design Name: Non integer DRU
//Purpose: rotator for the Barrel Shifter.
//Authors: Paolo Novellini, Giovanni Guasti. 
///////////////////////////////////////////////////////////////////////////////

module rot32 (CLK, RST, HIN, HOUT, P);
    input          CLK;       
    input          RST;       
    input   [31:0] HIN;        
    output  [31:0] HOUT;     
    input   [4:0]  P;     

    wire [31:0] a;
    wire [31:0] b;
    wire [31:0] c;
    wire [31:0] d;
    wire [31:0] e;
    reg  [31:0] temp;

    assign a =  (P[0] == 1'b0) ?  HIN[31:0] : {HIN[30:0], HIN[31]};       
    assign b =  (P[1] == 1'b0) ?  a[31:0]   :   {a[29:0],   a[31:30]};       
    assign c =  (P[2] == 1'b0) ?  b[31:0]   :   {b[27:0],   b[31:28]};       
    assign d =  (P[3] == 1'b0) ?  c[31:0]   :   {c[23:0],   c[31:24]};       
    assign e =  (P[4] == 1'b0) ?  d[31:0]   :   {d[15:0],   d[31:16]};       

    always @(posedge CLK or negedge RST)
    if (RST == 1'b0)
        temp <= 32'b00000000000000000000000000000000;
    else
        temp <= e;
        
    assign HOUT = temp;

//------------------------------------------------------------------------------
//-- for Elisa
//------------------------------------------------------------------------------

endmodule
