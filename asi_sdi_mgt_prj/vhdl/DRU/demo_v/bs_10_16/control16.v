///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: control6.v
// /___/   /\     Timestamp: October 20 2008
// \   \  /  \
//  \___\/\___\
//
///////////////////////////////////////////////////////////////////////////////
//Design Name: Non integer DRU
//Purpose: control logic for the Barrel Shifter.
//Authors: Paolo Novellini, Giovanni Guasti. 
///////////////////////////////////////////////////////////////////////////////

module control16 (CLK, RST, DV, SHIFT, WRFLAG, VALID);
    input         CLK;       
    input         RST;       
    input   [3:0] DV;        
    output  [4:0] SHIFT;     
    output        WRFLAG;    
    output        VALID;     


    reg  [4:0] pointer;
    reg  flag_d;
    reg  wrflags;
    reg  valids;
    wire flag;
    wire [4:0] temp;

    // pointer
    assign temp = pointer + {1'b0, DV};
    always @(posedge CLK or negedge RST)
    if (RST == 1'b0)
        pointer <= 5'b00000;
    else
        pointer <= temp;


    assign SHIFT = pointer;                
    assign flag =  (pointer < 5'b10000) ?  1'b0 : 1'b1;       
    always @(posedge CLK or negedge RST)
    if (RST == 1'b0)
        flag_d <= 1'b0;
    else
        flag_d <= flag;

    always @(posedge CLK or negedge RST)
    if (RST == 1'b0)
        wrflags <= 1'b0;
    else
        begin
        wrflags <= flag_d;
        valids <= flag ^ flag_d;
        end
    
    assign WRFLAG = wrflags;
    assign VALID  = valids;


//------------------------------------------------------------------------------
// for Elisa
//------------------------------------------------------------------------------
endmodule                
