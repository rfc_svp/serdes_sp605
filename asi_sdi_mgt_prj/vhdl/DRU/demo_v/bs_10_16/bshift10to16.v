///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: bshift10to16.v
// /___/   /\     Timestamp: October 20 2008
// \   \  /  \
//  \___\/\___\
//
///////////////////////////////////////////////////////////////////////////////
//Design Name: Non integer DRU
//Purpose: barrel shifter for the DRU.
//Authors: Paolo Novellini, Giovanni Guasti. 
///////////////////////////////////////////////////////////////////////////////

module bshift10to16(CLK, RST, DIN, DV, DV16, DOUT16);
    input           CLK;
    input           RST;
    input  [9:0]    DIN;
    input  [3:0]    DV;
    output          DV16;
    output [15:0]   DOUT16;

    reg  [15:0] DOUT16;
    reg         DV16;
    reg  [31:0] reg32;
    reg  [4:0]  i;

    wire [31:0] dinext;
    wire [31:0] dinext_rot;
    wire [31:0] mask;
    wire [4:0]  pointer1;
    wire [15:0] regout;
    wire        wrflag;
    wire        valid;



    maskencoder32 I_Maskdec (
        .CLK(CLK),
        .RST(RST),
        .SHIFT(pointer1),
        .MASK(mask));


    control16 I_control (
        .CLK(CLK),
        .RST(RST),
        .DV(DV),
        .SHIFT(pointer1),
        .WRFLAG(wrflag),
        .VALID(valid));


    assign dinext = {22'b0000000000000000000000, DIN};

    rot32 Inst_data_bs (
        .CLK(CLK),
        .RST(RST),
        .HIN(dinext),
        .HOUT(dinext_rot),
        .P  (pointer1));





    always @(posedge CLK or negedge RST)
    if (RST == 1'b0)
        reg32 <= 32'b00000000000000000000000000000000;
    else
        begin
        for(i=0; i<=31; i=i+1)
            begin
                if (mask[i]==1'b1)
                    reg32[i] <= dinext_rot[i];
                else
                    reg32[i] <= reg32[i]; 
        end //for
        end //process begin

    assign regout =  (wrflag == 1'b0) ?  reg32[15:0] : reg32[31:16];       
    always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
          DOUT16 <= 16'b0000000000000000;
      else
          DOUT16 <= regout;

    always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
          DV16 <= 1'b0;
      else
          DV16 <= valid;

//-----------------------------------------------------------------------------
// for Elisa
//-----------------------------------------------------------------------------


endmodule