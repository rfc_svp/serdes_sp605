

///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2003 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: tb_hw_dru.v
// /___/   /\     Timestamp: May 1 2007
// \   \  /  \
//  \___\/\___\
//
//Device: Virtex 5 LXT/FXT/TXT
//Design Name: tb_sim_dru_jitter.v
//Purpose: This is the test bench to test the NIDRU in HW.
//Authors: Paolo Novellini, Giovanni Guasti. 
//
//    
///////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module tb_hw_dru 

(
    TILE0_REFCLK_PAD_N_IN,
    TILE0_REFCLK_PAD_P_IN,
    TILE1_REFCLK_PAD_N_IN,
    TILE1_REFCLK_PAD_P_IN,
    //GTXRESET_IN,
    TILE0_PLLLKDET_OUT,
    TILE1_PLLLKDET_OUT,
    RXN_IN,
    RXP_IN,
    TXN_OUT,
    TXP_OUT,
    xgi_sclk
);
	

//***********************************Ports Declaration*******************************

    input           TILE0_REFCLK_PAD_N_IN;
    input           TILE0_REFCLK_PAD_P_IN;
    input           TILE1_REFCLK_PAD_N_IN;
    input           TILE1_REFCLK_PAD_P_IN;
    //input           GTXRESET_IN;
    output          TILE0_PLLLKDET_OUT;
    output          TILE1_PLLLKDET_OUT;
    input   [3:0]   RXN_IN;
    input   [3:0]   RXP_IN;
    output  [3:0]   TXN_OUT;
    output  [3:0]   TXP_OUT;
    output  [8:0]   xgi_sclk;

    
//************************** Register Declarations ****************************

    reg     [84:0]  ila_in0_r;
    reg     [84:0]  ila_in1_r;
    reg             tile0_tx_resetdone0_r;
    reg             tile0_tx_resetdone0_r2;
    reg             tile0_rx_resetdone0_r;
    reg             tile0_rx_resetdone0_r2;
    reg             tile0_tx_resetdone1_r;
    reg             tile0_tx_resetdone1_r2;
    reg             tile0_rx_resetdone1_r;
    reg             tile0_rx_resetdone1_r2;
    reg             tile1_tx_resetdone0_r;
    reg             tile1_tx_resetdone0_r2;
    reg             tile1_rx_resetdone0_r;
    reg             tile1_rx_resetdone0_r2;
    reg             tile1_tx_resetdone1_r;
    reg             tile1_tx_resetdone1_r2;
    reg             tile1_rx_resetdone1_r;
    reg             tile1_rx_resetdone1_r2;
    reg             async_mux0_sel_i;
    reg             async_mux1_sel_i;
    
//**************************** Wire Declarations ******************************

    //------------------------ MGT Wrapper Wires ------------------------------
    

    //________________________________________________________________________
    //________________________________________________________________________
    //TILE0   (X0Y3)

    //---------------------- Loopback and Powerdown Ports ----------------------
    wire    [2:0]   tile0_loopback0_i;
    wire    [2:0]   tile0_loopback1_i;
    //----------------- Receive Ports - RX Data Path interface -----------------
    wire    [19:0]  tile0_rxdata0_i;
    wire    [19:0]  tile0_rxdata1_i;
    //----- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    wire            tile0_rxcdrreset0_i;
    wire            tile0_rxcdrreset1_i;
    //------ Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
    wire            tile0_rxbufreset0_i;
    wire            tile0_rxbufreset1_i;
    //--------------- Receive Ports - RX Polarity Control Ports ----------------
    wire            tile0_rxpolarity0_i;
    wire            tile0_rxpolarity1_i;
    //------------------- Shared Ports - Tile and PLL Ports --------------------
    wire            tile0_gtxreset_i;
    wire            tile0_plllkdet_i;
    wire            tile0_refclkout_i;
    wire            tile0_resetdone0_i;
    wire            tile0_resetdone1_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [19:0]  tile0_txdata0_i;
    wire    [19:0]  tile0_txdata1_i;
    //------------- Transmit Ports - TX Driver and OOB signalling --------------
    wire    [2:0]   tile0_txdiffctrl0_i;
    wire    [2:0]   tile0_txdiffctrl1_i;



    //________________________________________________________________________
    //________________________________________________________________________
    //TILE1   (X0Y4)

    //---------------------- Loopback and Powerdown Ports ----------------------
    wire    [2:0]   tile1_loopback0_i;
    wire    [2:0]   tile1_loopback1_i;
    //----------------- Receive Ports - RX Data Path interface -----------------
    wire    [19:0]  tile1_rxdata0_i;
    wire    [19:0]  tile1_rxdata1_i;
    //----- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    wire            tile1_rxcdrreset0_i;
    wire            tile1_rxcdrreset1_i;
    //------ Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
    wire            tile1_rxbufreset0_i;
    wire            tile1_rxbufreset1_i;
    //--------------- Receive Ports - RX Polarity Control Ports ----------------
    wire            tile1_rxpolarity0_i;
    wire            tile1_rxpolarity1_i;
    //------------------- Shared Ports - Tile and PLL Ports --------------------
    wire            tile1_gtxreset_i;
    wire            tile1_plllkdet_i;
    wire            tile1_refclkout_i;
    wire            tile1_resetdone0_i;
    wire            tile1_resetdone1_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [19:0]  tile1_txdata0_i;
    wire    [19:0]  tile1_txdata1_i;
    //------------- Transmit Ports - TX Driver and OOB signalling --------------
    wire    [2:0]   tile1_txdiffctrl0_i;
    wire    [2:0]   tile1_txdiffctrl1_i;


    //----------------------------- Global Signals -----------------------------
    wire            tile0_tx_system_reset0_c;
    wire            tile0_rx_system_reset0_c;
    wire            tile0_tx_system_reset1_c;
    wire            tile0_rx_system_reset1_c;
    wire            tile1_tx_system_reset0_c;
    wire            tile1_rx_system_reset0_c;
    wire            tile1_tx_system_reset1_c;
    wire            tile1_rx_system_reset1_c;
    wire            tied_to_ground_i;
    wire    [63:0]  tied_to_ground_vec_i;
    wire            tied_to_vcc_i;
    wire    [7:0]   tied_to_vcc_vec_i;
    wire            drp_clk_in_i;
    
    wire            tile0_refclkout_bufg_i;
    
    
    //--------------------------- User Clocks ---------------------------------
    wire            tile0_txusrclk0_i;
    wire            tile1_txusrclk0_i;
    wire            refclkout_pll0_locked_i;
    wire            refclkout_pll0_reset_i;
    wire            tile0_refclkout_to_cmt_i;
    wire            refclkout_pll1_locked_i;
    wire            refclkout_pll1_reset_i;
    wire            tile1_refclkout_to_cmt_i;


    //--------------------- Frame check/gen Module Signals --------------------
    wire            tile0_refclk_i;
    wire            tile0_matchn0_i;
    
    wire    [1:0]   tile0_txcharisk0_float_i;
    
    wire    [19:0]  tile0_txdata0_float_i;
    
    
    wire            tile0_block_sync0_reset_i;
    wire    [7:0]   tile0_error_count0_i;
    wire            tile0_frame_check0_reset_i;
    wire            tile0_inc_in0_i;
    wire            tile0_inc_out0_i;
    wire    [19:0]  tile0_unscrambled_data0_i;
    wire            tile0_matchn1_i;
    
    wire    [1:0]   tile0_txcharisk1_float_i;
    
    wire    [19:0]  tile0_txdata1_float_i;
    
    
    wire            tile0_block_sync1_reset_i;
    wire    [7:0]   tile0_error_count1_i;
    wire            tile0_frame_check1_reset_i;
    wire            tile0_inc_in1_i;
    wire            tile0_inc_out1_i;
    wire    [19:0]  tile0_unscrambled_data1_i;

    wire            tile1_refclk_i;
    wire            tile1_matchn0_i;
    
    wire    [1:0]   tile1_txcharisk0_float_i;
    
    wire    [19:0]  tile1_txdata0_float_i;
    
    
    wire            tile1_block_sync0_reset_i;
    wire    [7:0]   tile1_error_count0_i;
    wire            tile1_frame_check0_reset_i;
    wire            tile1_inc_in0_i;
    wire            tile1_inc_out0_i;
    wire    [19:0]  tile1_unscrambled_data0_i;
    wire            tile1_matchn1_i;
    
    wire    [1:0]   tile1_txcharisk1_float_i;
    
    wire    [19:0]  tile1_txdata1_float_i;
    
    
    wire            tile1_block_sync1_reset_i;
    wire    [7:0]   tile1_error_count1_i;
    wire            tile1_frame_check1_reset_i;
    wire            tile1_inc_in1_i;
    wire            tile1_inc_out1_i;
    wire    [19:0]  tile1_unscrambled_data1_i;

    wire            reset_on_data_error_i;

    
    
    wire    [35:0]  control;
    wire    [255:0] async_out;
    wire    [255:0] async_in;

    wire            gtxreset_i;
    wire            mux_sel_i;
    wire            user_tx_reset_i;
    wire            user_rx_reset_i;
    wire            ila_clk0_i;
    wire            ila_clk1_i;
    
  //  wire [9:0] dt_out_10_tile00          ;
  //  wire en_10_tile00                    ;
    wire [36:0] center_f_tile00          ;
    wire [4:0] g1_tile_00                ;
    wire [4:0] g1_p_tile00               ;
    wire [4:0] g2_tile00                 ;
    wire rst_dru_tile00                  ;
    wire [3:0] samv_tile00               ;
    wire [9:0] sam_tile00                ;
    wire dv_out_tile00                   ;
    wire [9:0] d_out_tile00              ;
    wire chk_okko_tile00                 ;
    wire rt_err_tile00                   ;
    wire res_alarm_tile00                ;
    
  //  wire [9:0] dt_out_10_tile01          ;
  //  wire en_10_tile01                    ;
    wire [36:0] center_f_tile01          ;
    wire [4:0] g1_tile_01                ;
    wire [4:0] g1_p_tile01               ;
    wire [4:0] g2_tile01                 ;
    wire rst_dru_tile01                  ;
    wire [3:0] samv_tile01               ;
    wire [9:0] sam_tile01                ;
    wire dv_out_tile01                   ;
    wire [9:0] d_out_tile01              ;
    wire chk_okko_tile01                 ;
    wire rt_err_tile01                   ;
    wire res_alarm_tile01                ;
    
   // wire [9:0] dt_out_10_tile10          ;
   // wire en_10_tile10                    ;
    wire [36:0] center_f_tile10          ;
    wire [4:0] g1_tile_10                ;
    wire [4:0] g1_p_tile10               ;
    wire [4:0] g2_tile10                 ;
    wire rst_dru_tile10                  ;
    wire [3:0] samv_tile10               ;
    wire [9:0] sam_tile10                ;
    wire dv_out_tile10                   ;
    wire [9:0] d_out_tile10              ;
    wire chk_okko_tile10                 ;
    wire rt_err_tile10                   ;
    wire res_alarm_tile10                ;
    
   // wire [9:0] dt_out_10_tile11          ;
   //  wire en_10_tile11                    ;
    wire [36:0] center_f_tile11          ;
    wire [4:0] g1_tile_11                ;
    wire [4:0] g1_p_tile11               ;
    wire [4:0] g2_tile11                 ;
    wire rst_dru_tile11                  ;
    wire [3:0] samv_tile11               ;
    wire [9:0] sam_tile11                ;
    wire dv_out_tile11                   ;
    wire [9:0] d_out_tile11              ;
    wire chk_okko_tile11                 ;
    wire rt_err_tile11                   ;
    wire res_alarm_tile11                ;
    
    wire prbs_00;
    wire prbs_01;
    wire prbs_10;
    wire prbs_11;

//**************************** Main Body of Code *******************************

    //  Static signal Assigments    
    assign tied_to_ground_i             = 1'b0;
    assign tied_to_ground_vec_i         = 64'h0000000000000000;
    assign tied_to_vcc_i                = 1'b1;
    assign tied_to_vcc_vec_i            = 8'hff;

//MGT_USRCLK_SOURCE_PLL #
//    (
//        .MULT                           (5),
//        .DIVIDE                         (2),
//        .CLK_PERIOD                     (3.22),
//        .OUT0_DIVIDE                    (5),
//        .OUT1_DIVIDE                    (1),
//        .OUT2_DIVIDE                    (1),
//        .OUT3_DIVIDE                    (1),
////        .SIMULATION_P                   (EXAMPLE_USE_CHIPSCOPE),
//        .LOCK_WAIT_COUNT                (16'b0111100101001111)
//    )
//    refclkout_pll1_i
//    (
//        .CLK0_OUT                       (tile1_txusrclk0_i),
//        .CLK1_OUT                       (),
//        .CLK2_OUT                       (),
//        .CLK3_OUT                       (),
//        .CLK_IN                         (tile1_refclkout_to_cmt_i),
//        .PLL_LOCKED_OUT                 (refclkout_pll1_locked_i),
//        .PLL_RESET_IN                   (refclkout_pll1_reset_i)
//    );
//

    dru Inst_dru_tile00 (
    .DT_IN(tile0_rxdata0_i), 
    .CENTER_F(center_f_tile00), 
    .G1(g1_tile_00), 
    .G1_P(g1_p_tile00), 
    .G2(g2_tile00), 
    .CLK(tile0_txusrclk0_i), 
    .PH_OUT(), 
    .CTRL(), 
    .RST(rst_dru_tile00), 
    .SAMV(samv_tile00), 
    .SAM(sam_tile00)
    );
  
 bshift10to10 Inst_bs_10_10_tile00 (
    .CLK(tile0_txusrclk0_i), 
    .RST(rst_dru_tile00), 
    .DIN(sam_tile00), 
    .DV(samv_tile00), 
    .DV10(dv_out_tile00), 
    .DOUT10(d_out_tile00)
    );
  
prbs_chk_par_10 Inst_prbs_chk_par_tile00 (
    .CLK(tile0_txusrclk0_i), 
    .RST(rst_dru_tile00), 
    .CHK_OKKO(chk_okko_tile00), 
    .RT_ERR(rt_err_tile00), 
    .EN(dv_out_tile00), 
    .RES_ALARM(res_alarm_tile00), 
    .DT_IN(d_out_tile00)
    );

assign async_in[1] =tile0_rxdata0_i[10];
assign async_in[2] =tile0_rxdata1_i[10];
assign async_in[3] =tile1_rxdata0_i[10];
assign async_in[4] =tile1_rxdata1_i[10];

assign async_in[5] =tile0_txdata0_i[10];
assign async_in[6] =tile0_txdata1_i[10];
assign async_in[7] =tile1_txdata0_i[10];
assign async_in[8] =tile1_txdata1_i[10];


assign center_f_tile00 = async_out [36:0];
assign g1_tile_00 = async_out [41:37];
assign g1_p_tile00 = async_out [46:42];
assign g2_tile00 = async_out [51:47];
assign rst_dru_tile00 = async_out [52];
assign res_alarm_tile00 = async_out [53];
assign async_in[0] = chk_okko_tile00;

  dru Inst_dru_tile01 (
    .DT_IN(tile0_rxdata1_i), 
    .CENTER_F(center_f_tile01), 
    .G1(g1_tile_01), 
    .G1_P(g1_p_tile01), 
    .G2(g2_tile01), 
    .CLK(tile0_txusrclk0_i), 
    .PH_OUT(), 
    .CTRL(), 
    .RST(rst_dru_tile01), 
    .SAMV(samv_tile01), 
    .SAM(sam_tile01)
    );
  
 bshift10to10 Inst_bs_10_10_tile01 (
    .CLK(tile0_txusrclk0_i), 
    .RST(rst_dru_tile01), 
    .DIN(sam_tile01), 
    .DV(samv_tile01), 
    .DV10(dv_out_tile01), 
    .DOUT10(d_out_tile01)
    );
  
prbs_chk_par_10 Inst_prbs_chk_par_tile01 (
    .CLK(tile0_txusrclk0_i), 
    .RST(rst_dru_tile01), 
    .CHK_OKKO(chk_okko_tile01), 
    .RT_ERR(rt_err_tile01), 
    .EN(dv_out_tile01), 
    .RES_ALARM(res_alarm_tile01), 
    .DT_IN(d_out_tile01)
    );



assign center_f_tile01 = async_out [100:64];
assign g1_tile_01 = async_out [105:101];
assign g1_p_tile01 = async_out [110:106];
assign g2_tile01 = async_out [115:111];
assign rst_dru_tile01 = async_out [116];
assign res_alarm_tile01 = async_out [117];
assign async_in[64] = chk_okko_tile01;

  dru Inst_dru_tile10 (
    .DT_IN(tile1_rxdata0_i), 
    .CENTER_F(center_f_tile10), 
    .G1(g1_tile_10), 
    .G1_P(g1_p_tile10), 
    .G2(g2_tile10), 
    .CLK(tile1_txusrclk0_i), 
    .PH_OUT(), 
    .CTRL(), 
    .RST(rst_dru_tile10), 
    .SAMV(samv_tile10), 
    .SAM(sam_tile10)
    );
  
 bshift10to10 Inst_bs_10_10_tile10 (
    .CLK(tile1_txusrclk0_i), 
    .RST(rst_dru_tile10), 
    .DIN(sam_tile10), 
    .DV(samv_tile10), 
    .DV10(dv_out_tile10), 
    .DOUT10(d_out_tile10)
    );
  
prbs_chk_par_10 Inst_prbs_chk_par_tile10 (
    .CLK(tile1_txusrclk0_i), 
    .RST(rst_dru_tile10), 
    .CHK_OKKO(chk_okko_tile10), 
    .RT_ERR(rt_err_tile10), 
    .EN(dv_out_tile10), 
    .RES_ALARM(res_alarm_tile10), 
    .DT_IN(d_out_tile10)
    );



assign center_f_tile10 = async_out [164:128];
assign g1_tile_10 = async_out [169:129];
assign g1_p_tile10 = async_out [174:170];
assign g2_tile10 = async_out [179:175];
assign rst_dru_tile10 = async_out [180];
assign res_alarm_tile10 = async_out [181];
assign async_in[128] = chk_okko_tile10;

assign xgi_sclk = async_out [190:182];

 dru Inst_dru_tile11 (
    .DT_IN(tile1_rxdata1_i), 
    .CENTER_F(center_f_tile11), 
    .G1(g1_tile_11), 
    .G1_P(g1_p_tile11), 
    .G2(g2_tile11), 
    .CLK(tile1_txusrclk0_i), 
    .PH_OUT(), 
    .CTRL(), 
    .RST(rst_dru_tile11), 
    .SAMV(samv_tile11), 
    .SAM(sam_tile11)
    );
  
 bshift10to10 Inst_bs_10_10_tile11 (
    .CLK(tile1_txusrclk0_i), 
    .RST(rst_dru_tile11), 
    .DIN(sam_tile11), 
    .DV(samv_tile11), 
    .DV10(dv_out_tile11), 
    .DOUT10(d_out_tile11)
    );
  
prbs_chk_par_10 Inst_prbs_chk_par_tile11 (
    .CLK(tile1_txusrclk0_i), 
    .RST(rst_dru_tile11), 
    .CHK_OKKO(chk_okko_tile11), 
    .RT_ERR(rt_err_tile11), 
    .EN(dv_out_tile11), 
    .RES_ALARM(res_alarm_tile11), 
    .DT_IN(d_out_tile11)
    );



assign center_f_tile11 = async_out [228:192];
assign g1_tile_11 = async_out [233:229];
assign g1_p_tile11 = async_out [238:234];
assign g2_tile11 = async_out [243:239];
assign rst_dru_tile11 = async_out [244];
assign res_alarm_tile11 = async_out [245];
assign async_in[192] = chk_okko_tile11;

//common

assign tile0_rxpolarity0_i = async_out [250];
assign tile0_rxpolarity1_i = async_out [251];
assign tile1_rxpolarity0_i = async_out [252];
assign tile1_rxpolarity1_i = async_out [253];
assign tile0_gtxreset_i = async_out[254];
assign tile1_gtxreset_i = async_out[254];  // the reset is shared in the CSPRO

assign async_in[254] = tile0_plllkdet_i;
assign async_in[255] = tile1_plllkdet_i;

prbsgen_ser Inst_prbsgen_ser_00 (
    .CLK(tile0_txusrclk0_i), 
    .RST(rst_dru_tile00), 
    .ERR(async_out[246]), 
    .PRBSOUT(prbs_00)
    );

prbsgen_ser Inst_prbsgen_ser_01 (
    .CLK(tile0_txusrclk0_i), 
    .RST(rst_dru_tile01), 
    .ERR(async_out[247]), 
    .PRBSOUT(prbs_01)
    );
    
prbsgen_ser Inst_prbsgen_ser_10 (
    .CLK(tile1_txusrclk0_i), 
    .RST(rst_dru_tile10), 
    .ERR(async_out[248]), 
    .PRBSOUT(prbs_10)
    );
    
prbsgen_ser Inst_prbsgen_ser_11 (
    .CLK(tile1_txusrclk0_i), 
    .RST(rst_dru_tile11), 
    .ERR(async_out[249]), 
    .PRBSOUT(prbs_11)
    );
    
    
    IBUFDS tile0_refclk_ibufds_i
    (
        .O                              (tile0_refclk_i), 
        .I                              (TILE0_REFCLK_PAD_P_IN),
        .IB                             (TILE0_REFCLK_PAD_N_IN)
    );

    IBUFDS tile1_refclk_ibufds_i
    (
        .O                              (tile1_refclk_i), 
        .I                              (TILE1_REFCLK_PAD_P_IN),
        .IB                             (TILE1_REFCLK_PAD_N_IN)
    );


 

    BUFG refclkout_pll0_bufg_i
    (
        .I                              (tile0_refclkout_i),
        .O                              (tile0_refclkout_to_cmt_i)
    );

    assign  refclkout_pll0_reset_i          =  !tile0_plllkdet_i;
    MGT_USRCLK_SOURCE_PLL #
    (
        .MULT                           (5),
        .DIVIDE                         (2),
        .CLK_PERIOD                     (3.22),
        .OUT0_DIVIDE                    (5),
        .OUT1_DIVIDE                    (1),
        .OUT2_DIVIDE                    (1),
        .OUT3_DIVIDE                    (1),
 //       .SIMULATION_P                   (EXAMPLE_USE_CHIPSCOPE),
        .LOCK_WAIT_COUNT                (16'b0111100101001111)
    )
    refclkout_pll0_i
    (
        .CLK0_OUT                       (tile0_txusrclk0_i),
        .CLK1_OUT                       (),
        .CLK2_OUT                       (),
        .CLK3_OUT                       (),
        .CLK_IN                         (tile0_refclkout_to_cmt_i),
        .PLL_LOCKED_OUT                 (refclkout_pll0_locked_i),
        .PLL_RESET_IN                   (refclkout_pll0_reset_i)
    );


    BUFG refclkout_pll1_bufg_i
    (
        .I                              (tile1_refclkout_i),
        .O                              (tile1_refclkout_to_cmt_i)
    );

    assign  refclkout_pll1_reset_i          =  !tile1_plllkdet_i;
    MGT_USRCLK_SOURCE_PLL #
    (
        .MULT                           (5),
        .DIVIDE                         (2),
        .CLK_PERIOD                     (3.22),
        .OUT0_DIVIDE                    (5),
        .OUT1_DIVIDE                    (1),
        .OUT2_DIVIDE                    (1),
        .OUT3_DIVIDE                    (1),
//        .SIMULATION_P                   (EXAMPLE_USE_CHIPSCOPE),
        .LOCK_WAIT_COUNT                (16'b0111100101001111)
    )
    refclkout_pll1_i
    (
        .CLK0_OUT                       (tile1_txusrclk0_i),
        .CLK1_OUT                       (),
        .CLK2_OUT                       (),
        .CLK3_OUT                       (),
        .CLK_IN                         (tile1_refclkout_to_cmt_i),
        .PLL_LOCKED_OUT                 (refclkout_pll1_locked_i),
        .PLL_RESET_IN                   (refclkout_pll1_reset_i)
    );






   
    assign TILE0_PLLLKDET_OUT = tile0_plllkdet_i;
    assign TILE1_PLLLKDET_OUT = tile1_plllkdet_i;


    assign tile0_txdata0_i = {prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00,prbs_00};
    assign tile0_txdata1_i = {prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01,prbs_01};
    assign tile1_txdata0_i = {prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10,prbs_10};
    assign tile1_txdata1_i = {prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11,prbs_11};

    ROCKETIO_WRAPPER 
    rocketio_wrapper_i
    (
    
  
 
        //_____________________________________________________________________
        //_____________________________________________________________________
        //TILE0  (X0Y3)

        //---------------------- Loopback and Powerdown Ports ----------------------
        .TILE0_LOOPBACK0_IN             (3'b000),
        .TILE0_LOOPBACK1_IN             (3'b000),
        //----------------- Receive Ports - RX Data Path interface -----------------
        .TILE0_RXDATA0_OUT              (tile0_rxdata0_i),
        .TILE0_RXDATA1_OUT              (tile0_rxdata1_i),
        .TILE0_RXRESET0_IN              (!refclkout_pll0_locked_i),
        .TILE0_RXRESET1_IN              (!refclkout_pll0_locked_i),
        .TILE0_RXUSRCLK0_IN             (tile0_txusrclk0_i),
        .TILE0_RXUSRCLK1_IN             (tile0_txusrclk0_i),
        .TILE0_RXUSRCLK20_IN            (tile0_txusrclk0_i),
        .TILE0_RXUSRCLK21_IN            (tile0_txusrclk0_i),
        //----- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        .TILE0_RXCDRRESET0_IN           (1'b0),
        .TILE0_RXCDRRESET1_IN           (1'b0),
        .TILE0_RXN0_IN                  (RXN_IN[0]),
        .TILE0_RXN1_IN                  (RXN_IN[1]),
        .TILE0_RXP0_IN                  (RXP_IN[0]),
        .TILE0_RXP1_IN                  (RXP_IN[1]),
        //------ Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
        .TILE0_RXBUFRESET0_IN           (1'b0),
        .TILE0_RXBUFRESET1_IN           (1'b0),
        //--------------- Receive Ports - RX Polarity Control Ports ----------------
        .TILE0_RXPOLARITY0_IN           (tile0_rxpolarity0_i),
        .TILE0_RXPOLARITY1_IN           (tile0_rxpolarity1_i),
        //------------------- Shared Ports - Tile and PLL Ports --------------------
        .TILE0_CLKIN_IN                 (tile0_refclk_i),
        .TILE0_GTXRESET_IN              (tile0_gtxreset_i),
        .TILE0_PLLLKDET_OUT             (tile0_plllkdet_i),
        .TILE0_REFCLKOUT_OUT            (tile0_refclkout_i),
        .TILE0_RESETDONE0_OUT           (tile0_resetdone0_i),
        .TILE0_RESETDONE1_OUT           (tile0_resetdone1_i),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .TILE0_TXDATA0_IN               (tile0_txdata0_i),
        .TILE0_TXDATA1_IN               (tile0_txdata1_i),
        .TILE0_TXRESET0_IN              (!refclkout_pll0_locked_i),
        .TILE0_TXRESET1_IN              (!refclkout_pll0_locked_i),
        .TILE0_TXUSRCLK0_IN             (tile0_txusrclk0_i),
        .TILE0_TXUSRCLK1_IN             (tile0_txusrclk0_i),
        .TILE0_TXUSRCLK20_IN            (tile0_txusrclk0_i),
        .TILE0_TXUSRCLK21_IN            (tile0_txusrclk0_i),
        //------------- Transmit Ports - TX Driver and OOB signalling --------------
        .TILE0_TXDIFFCTRL0_IN           (3'b000),
        .TILE0_TXDIFFCTRL1_IN           (3'b000),
        .TILE0_TXN0_OUT                 (TXN_OUT[0]),
        .TILE0_TXN1_OUT                 (TXN_OUT[1]),
        .TILE0_TXP0_OUT                 (TXP_OUT[0]),
        .TILE0_TXP1_OUT                 (TXP_OUT[1]),


    
 
 
 
 
 
 
 
 
        //_____________________________________________________________________
        //_____________________________________________________________________
        //TILE1  (X0Y4)

        //---------------------- Loopback and Powerdown Ports ----------------------
        .TILE1_LOOPBACK0_IN             (3'b000),
        .TILE1_LOOPBACK1_IN             (3'b000),
        //----------------- Receive Ports - RX Data Path interface -----------------
        .TILE1_RXDATA0_OUT              (tile1_rxdata0_i),
        .TILE1_RXDATA1_OUT              (tile1_rxdata1_i),
        .TILE1_RXRESET0_IN              (!refclkout_pll1_locked_i),
        .TILE1_RXRESET1_IN              (!refclkout_pll1_locked_i),
        .TILE1_RXUSRCLK0_IN             (tile1_txusrclk0_i),
        .TILE1_RXUSRCLK1_IN             (tile1_txusrclk0_i),
        .TILE1_RXUSRCLK20_IN            (tile1_txusrclk0_i),
        .TILE1_RXUSRCLK21_IN            (tile1_txusrclk0_i),
        //----- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        .TILE1_RXCDRRESET0_IN           (1'b0),
        .TILE1_RXCDRRESET1_IN           (1'b0),
        .TILE1_RXN0_IN                  (RXN_IN[2]),
        .TILE1_RXN1_IN                  (RXN_IN[3]),
        .TILE1_RXP0_IN                  (RXP_IN[2]),
        .TILE1_RXP1_IN                  (RXP_IN[3]),
        //------ Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
        .TILE1_RXBUFRESET0_IN           (1'b0),
        .TILE1_RXBUFRESET1_IN           (1'b0),
        //--------------- Receive Ports - RX Polarity Control Ports ----------------
        .TILE1_RXPOLARITY0_IN           (tile1_rxpolarity0_i),
        .TILE1_RXPOLARITY1_IN           (tile1_rxpolarity1_i),
        //------------------- Shared Ports - Tile and PLL Ports --------------------
        .TILE1_CLKIN_IN                 (tile1_refclk_i),
        .TILE1_GTXRESET_IN              (tile1_gtxreset_i),
        .TILE1_PLLLKDET_OUT             (tile1_plllkdet_i),
        .TILE1_REFCLKOUT_OUT            (tile1_refclkout_i),
        .TILE1_RESETDONE0_OUT           (tile1_resetdone0_i),
        .TILE1_RESETDONE1_OUT           (tile1_resetdone1_i),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .TILE1_TXDATA0_IN               (tile1_txdata0_i),
        .TILE1_TXDATA1_IN               (tile1_txdata1_i),
        .TILE1_TXRESET0_IN              (!refclkout_pll1_locked_i),
        .TILE1_TXRESET1_IN              (!refclkout_pll1_locked_i),
        .TILE1_TXUSRCLK0_IN             (tile1_txusrclk0_i),
        .TILE1_TXUSRCLK1_IN             (tile1_txusrclk0_i),
        .TILE1_TXUSRCLK20_IN            (tile1_txusrclk0_i),
        .TILE1_TXUSRCLK21_IN            (tile1_txusrclk0_i),
        //------------- Transmit Ports - TX Driver and OOB signalling --------------
        .TILE1_TXDIFFCTRL0_IN           (3'b000),
        .TILE1_TXDIFFCTRL1_IN           (3'b000),
        .TILE1_TXN0_OUT                 (TXN_OUT[2]),
        .TILE1_TXN1_OUT                 (TXN_OUT[3]),
        .TILE1_TXP0_OUT                 (TXP_OUT[2]),
        .TILE1_TXP1_OUT                 (TXP_OUT[3])


    );

chipscope_vio_v1_02_a vio_inst
(
  .CONTROL(control),
  .ASYNC_OUT(async_out),
  .ASYNC_IN(async_in)
);

chipscope_icon_v1_03_a icon_inst
(
  .CONTROL0(control)
);



endmodule

