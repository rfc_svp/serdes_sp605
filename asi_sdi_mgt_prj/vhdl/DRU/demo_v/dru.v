
///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2003 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: tb_hw_dru.v
// /___/   /\     Timestamp: May 1 2007
// \   \  /  \
//  \___\/\___\
//
//Device: Virtex 5 LXT/FXT/TXT
//Design Name: dru.v
//Purpose: The NIDRU wrapper.
//Authors: Paolo Novellini, Giovanni Guasti. 
//
//    
///////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module dru(
    input [19:0] DT_IN,
    input [36:0] CENTER_F,
    input [4:0] G1,
    input [4:0] G1_P,
    input [4:0] G2,
    input CLK,
    output [20:0] PH_OUT,
    output [31:0] CTRL,
    input RST,
    output [3:0] SAMV,
    output [9:0] SAM
    );


endmodule
