-------------------------------------------------------------------------------
-- Copyright (c) 2005 Xilinx, Inc.
-- This design is confidential and proprietary of Xilinx, All Rights Reserved.
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor: Xilinx
-- \   \   \/    Version: 1.0
--  \   \        Filename: tb_hw_dru.vhd
--  /   /        Date Last Modified:  May 1 2007
-- /___/   /\    Date Created: May 1 2007
-- \   \  /  \
--  \___\/\___\
-- 
--Device: Virtex-5 LXT/FXT/TXT
--Purpose: This is the test bench to test the NIDRU in HW
--Reference:
--    
--Revision History:
--    Rev 1.0 - First created, Paolo Novellini and Giovanni Guasti, May 1 2007.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;


entity tb_hw_dru is
port
(
    TILE0_REFCLK_PAD_N_IN                   : in   std_logic;
    TILE0_REFCLK_PAD_P_IN                   : in   std_logic;
    TILE1_REFCLK_PAD_N_IN                   : in   std_logic;
    TILE1_REFCLK_PAD_P_IN                   : in   std_logic;
    GTXRESET_IN                             : in   std_logic;
    TILE0_PLLLKDET_OUT                      : out  std_logic;
    TILE1_PLLLKDET_OUT                      : out  std_logic;
    RXN_IN                                  : in   std_logic_vector(3 downto 0);
    RXP_IN                                  : in   std_logic_vector(3 downto 0);
    TXN_OUT                                 : out  std_logic_vector(3 downto 0);
    TXP_OUT                                 : out  std_logic_vector(3 downto 0);
    xgi_sclk                                : out std_logic_vector(8 downto 0) 
    
);


end tb_hw_dru;
    
architecture RTL of tb_hw_dru is

COMPONENT dru
	PORT(
		DT_IN : IN std_logic_vector(19 downto 0);
		CENTER_F : IN std_logic_vector(36 downto 0);
		G1 : IN std_logic_vector(4 downto 0);
		G1_P : IN std_logic_vector(4 downto 0);
		G2 : IN std_logic_vector(4 downto 0);
		CLK : IN std_logic;
		RST : IN std_logic;          
		PH_OUT : OUT std_logic_vector(20 downto 0);
		CTRL : OUT std_logic_vector(31 downto 0);
		SAMV : OUT std_logic_vector(3 downto 0);
		SAM : OUT std_logic_vector(9 downto 0)
		);
END COMPONENT;

COMPONENT bshift10to10
	PORT(
		CLK : IN std_logic;
		RST : IN std_logic;
		DIN : IN std_logic_vector(9 downto 0);
		DV : IN std_logic_vector(3 downto 0);          
		DV10 : OUT std_logic;
		DOUT10 : OUT std_logic_vector(9 downto 0)
		);
END COMPONENT;
	
COMPONENT prbs_chk_par_10
	PORT(
		CLK : IN std_logic;
		RST : IN std_logic;
		EN : IN std_logic;
		RES_ALARM : IN std_logic;
		DT_IN : IN std_logic_vector(9 downto 0);          
		CHK_OKKO : OUT std_logic;
		RT_ERR : OUT std_logic
		);
END COMPONENT;

component chipscope_icon_v1_03_a
  PORT (
    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
end component;

component chipscope_vio_v1_02_a
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    ASYNC_IN : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    ASYNC_OUT : OUT STD_LOGIC_VECTOR(255 DOWNTO 0));

end component;

COMPONENT prbsgen_ser
	PORT(
		CLK : IN std_logic;
		RST : IN std_logic;
		ERR : IN std_logic;          
		PRBSOUT : OUT std_logic
		);
END COMPONENT;
	
component ROCKETIO_WRAPPER 
generic
(
    -- Simulation attributes
    WRAPPER_SIM_MODE                : string    := "FAST"; 
    WRAPPER_SIM_GTXRESET_SPEEDUP    : integer   := 0; 
    WRAPPER_SIM_PLL_PERDIV2         : bit_vector:= x"142" 
);
port
(
    
    --_________________________________________________________________________
    --_________________________________________________________________________
    --TILE0  (Location)

    ------------------------ Loopback and Powerdown Ports ----------------------
    TILE0_LOOPBACK0_IN                      : in   std_logic_vector(2 downto 0);
    TILE0_LOOPBACK1_IN                      : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - RX Data Path interface -----------------
    TILE0_RXDATA0_OUT                       : out  std_logic_vector(19 downto 0);
    TILE0_RXDATA1_OUT                       : out  std_logic_vector(19 downto 0);
    TILE0_RXRESET0_IN                       : in   std_logic;
    TILE0_RXRESET1_IN                       : in   std_logic;
    TILE0_RXUSRCLK0_IN                      : in   std_logic;
    TILE0_RXUSRCLK1_IN                      : in   std_logic;
    TILE0_RXUSRCLK20_IN                     : in   std_logic;
    TILE0_RXUSRCLK21_IN                     : in   std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    TILE0_RXCDRRESET0_IN                    : in   std_logic;
    TILE0_RXCDRRESET1_IN                    : in   std_logic;
    TILE0_RXN0_IN                           : in   std_logic;
    TILE0_RXN1_IN                           : in   std_logic;
    TILE0_RXP0_IN                           : in   std_logic;
    TILE0_RXP1_IN                           : in   std_logic;
    -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
    TILE0_RXBUFRESET0_IN                    : in   std_logic;
    TILE0_RXBUFRESET1_IN                    : in   std_logic;
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
    TILE0_RXPOLARITY0_IN                    : in   std_logic;
    TILE0_RXPOLARITY1_IN                    : in   std_logic;
    --------------------- Shared Ports - Tile and PLL Ports --------------------
    TILE0_CLKIN_IN                          : in   std_logic;
    TILE0_GTXRESET_IN                       : in   std_logic;
    TILE0_PLLLKDET_OUT                      : out  std_logic;
    TILE0_REFCLKOUT_OUT                     : out  std_logic;
    TILE0_RESETDONE0_OUT                    : out  std_logic;
    TILE0_RESETDONE1_OUT                    : out  std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    TILE0_TXDATA0_IN                        : in   std_logic_vector(19 downto 0);
    TILE0_TXDATA1_IN                        : in   std_logic_vector(19 downto 0);
    TILE0_TXRESET0_IN                       : in   std_logic;
    TILE0_TXRESET1_IN                       : in   std_logic;
    TILE0_TXUSRCLK0_IN                      : in   std_logic;
    TILE0_TXUSRCLK1_IN                      : in   std_logic;
    TILE0_TXUSRCLK20_IN                     : in   std_logic;
    TILE0_TXUSRCLK21_IN                     : in   std_logic;
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    TILE0_TXDIFFCTRL0_IN                    : in   std_logic_vector(2 downto 0);
    TILE0_TXDIFFCTRL1_IN                    : in   std_logic_vector(2 downto 0);
    TILE0_TXN0_OUT                          : out  std_logic;
    TILE0_TXN1_OUT                          : out  std_logic;
    TILE0_TXP0_OUT                          : out  std_logic;
    TILE0_TXP1_OUT                          : out  std_logic;


    
    --_________________________________________________________________________
    --_________________________________________________________________________
    --TILE1  (Location)

    ------------------------ Loopback and Powerdown Ports ----------------------
    TILE1_LOOPBACK0_IN                      : in   std_logic_vector(2 downto 0);
    TILE1_LOOPBACK1_IN                      : in   std_logic_vector(2 downto 0);
    ------------------- Receive Ports - RX Data Path interface -----------------
    TILE1_RXDATA0_OUT                       : out  std_logic_vector(19 downto 0);
    TILE1_RXDATA1_OUT                       : out  std_logic_vector(19 downto 0);
    TILE1_RXRESET0_IN                       : in   std_logic;
    TILE1_RXRESET1_IN                       : in   std_logic;
    TILE1_RXUSRCLK0_IN                      : in   std_logic;
    TILE1_RXUSRCLK1_IN                      : in   std_logic;
    TILE1_RXUSRCLK20_IN                     : in   std_logic;
    TILE1_RXUSRCLK21_IN                     : in   std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    TILE1_RXCDRRESET0_IN                    : in   std_logic;
    TILE1_RXCDRRESET1_IN                    : in   std_logic;
    TILE1_RXN0_IN                           : in   std_logic;
    TILE1_RXN1_IN                           : in   std_logic;
    TILE1_RXP0_IN                           : in   std_logic;
    TILE1_RXP1_IN                           : in   std_logic;
    -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
    TILE1_RXBUFRESET0_IN                    : in   std_logic;
    TILE1_RXBUFRESET1_IN                    : in   std_logic;
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
    TILE1_RXPOLARITY0_IN                    : in   std_logic;
    TILE1_RXPOLARITY1_IN                    : in   std_logic;
    --------------------- Shared Ports - Tile and PLL Ports --------------------
    TILE1_CLKIN_IN                          : in   std_logic;
    TILE1_GTXRESET_IN                       : in   std_logic;
    TILE1_PLLLKDET_OUT                      : out  std_logic;
    TILE1_REFCLKOUT_OUT                     : out  std_logic;
    TILE1_RESETDONE0_OUT                    : out  std_logic;
    TILE1_RESETDONE1_OUT                    : out  std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    TILE1_TXDATA0_IN                        : in   std_logic_vector(19 downto 0);
    TILE1_TXDATA1_IN                        : in   std_logic_vector(19 downto 0);
    TILE1_TXRESET0_IN                       : in   std_logic;
    TILE1_TXRESET1_IN                       : in   std_logic;
    TILE1_TXUSRCLK0_IN                      : in   std_logic;
    TILE1_TXUSRCLK1_IN                      : in   std_logic;
    TILE1_TXUSRCLK20_IN                     : in   std_logic;
    TILE1_TXUSRCLK21_IN                     : in   std_logic;
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    TILE1_TXDIFFCTRL0_IN                    : in   std_logic_vector(2 downto 0);
    TILE1_TXDIFFCTRL1_IN                    : in   std_logic_vector(2 downto 0);
    TILE1_TXN0_OUT                          : out  std_logic;
    TILE1_TXN1_OUT                          : out  std_logic;
    TILE1_TXP0_OUT                          : out  std_logic;
    TILE1_TXP1_OUT                          : out  std_logic


);
end component;


component MGT_USRCLK_SOURCE 
generic
(
    FREQUENCY_MODE   : string   := "LOW";    
    PERFORMANCE_MODE : string   := "MAX_SPEED"    
);
port
(
    DIV1_OUT                : out std_logic;
    DIV2_OUT                : out std_logic;
    DCM_LOCKED_OUT          : out std_logic;
    CLK_IN                  : in  std_logic;
    DCM_RESET_IN            : in  std_logic

);
end component;



component MGT_USRCLK_SOURCE_PLL 
generic
(
    MULT                 : integer          := 2;
    DIVIDE               : integer          := 2;    
    CLK_PERIOD           : real             := 3.22;    
    OUT0_DIVIDE          : integer          := 2;
    OUT1_DIVIDE          : integer          := 2;
    OUT2_DIVIDE          : integer          := 2;
    OUT3_DIVIDE          : integer          := 2;
    SIMULATION_P         : integer          := 1;
    LOCK_WAIT_COUNT      : std_logic_vector := "1000001000110101"  
);
port
( 
    CLK0_OUT                : out std_logic;
    CLK1_OUT                : out std_logic;
    CLK2_OUT                : out std_logic;
    CLK3_OUT                : out std_logic;
    CLK_IN                  : in  std_logic;
    PLL_LOCKED_OUT          : out std_logic;
    PLL_RESET_IN            : in  std_logic
);
end component;

    signal prbs_00, prbs_01, prbs_10, prbs_11: std_logic;
    
--************************** Register Declarations ****************************

    signal   tile0_tx_resetdone0_r           : std_logic;
    signal   tile0_tx_resetdone0_r2          : std_logic;
    signal   tile0_rx_resetdone0_r           : std_logic;
    signal   tile0_rx_resetdone0_r2          : std_logic;
    signal   tile0_tx_resetdone1_r           : std_logic;
    signal   tile0_tx_resetdone1_r2          : std_logic;
    signal   tile0_rx_resetdone1_r           : std_logic;
    signal   tile0_rx_resetdone1_r2          : std_logic;
    signal   tile1_tx_resetdone0_r           : std_logic;
    signal   tile1_tx_resetdone0_r2          : std_logic;
    signal   tile1_rx_resetdone0_r           : std_logic;
    signal   tile1_rx_resetdone0_r2          : std_logic;
    signal   tile1_tx_resetdone1_r           : std_logic;
    signal   tile1_tx_resetdone1_r2          : std_logic;
    signal   tile1_rx_resetdone1_r           : std_logic;
    signal   tile1_rx_resetdone1_r2          : std_logic;
    signal   async_mux0_sel_i                : std_logic;
    signal   not_async_mux0_sel_i            : std_logic;
    signal   async_mux1_sel_i                : std_logic;
    signal   not_async_mux1_sel_i            : std_logic;
  

--**************************** Wire Declarations ******************************

    -------------------------- MGT Wrapper Wires ------------------------------
    
    --________________________________________________________________________
    --________________________________________________________________________
    --TILE0   (X0Y3)

    ------------------------ Loopback and Powerdown Ports ----------------------
    signal  tile0_loopback0_i               : std_logic_vector(2 downto 0);
    signal  tile0_loopback1_i               : std_logic_vector(2 downto 0);
    ------------------- Receive Ports - RX Data Path interface -----------------
    signal  tile0_rxdata0_i                 : std_logic_vector(19 downto 0);
    signal  tile0_rxdata1_i                 : std_logic_vector(19 downto 0);
    signal  tile0_rxreset0_i                : std_logic;
    signal  tile0_rxreset1_i                : std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    signal  tile0_rxcdrreset0_i             : std_logic;
    signal  tile0_rxcdrreset1_i             : std_logic;
    -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
    signal  tile0_rxbufreset0_i             : std_logic;
    signal  tile0_rxbufreset1_i             : std_logic;
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
    signal  tile0_rxpolarity0_i             : std_logic;
    signal  tile0_rxpolarity1_i             : std_logic;
    --------------------- Shared Ports - Tile and PLL Ports --------------------
    signal  tile0_gtxreset_i                : std_logic;
    signal  tile0_plllkdet_i                : std_logic;
    signal  tile0_refclkout_i               : std_logic;
    signal  tile0_resetdone0_i              : std_logic;
    signal  tile0_resetdone1_i              : std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    signal  tile0_txdata0_i                 : std_logic_vector(19 downto 0);
    signal  tile0_txdata1_i                 : std_logic_vector(19 downto 0);
    signal  tile0_txreset0_i                : std_logic;
    signal  tile0_txreset1_i                : std_logic;
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    signal  tile0_txdiffctrl0_i             : std_logic_vector(2 downto 0);
    signal  tile0_txdiffctrl1_i             : std_logic_vector(2 downto 0);


    --________________________________________________________________________
    --________________________________________________________________________
    --TILE1   (X0Y4)

    ------------------------ Loopback and Powerdown Ports ----------------------
    signal  tile1_loopback0_i               : std_logic_vector(2 downto 0);
    signal  tile1_loopback1_i               : std_logic_vector(2 downto 0);
    ------------------- Receive Ports - RX Data Path interface -----------------
    signal  tile1_rxdata0_i                 : std_logic_vector(19 downto 0);
    signal  tile1_rxdata1_i                 : std_logic_vector(19 downto 0);
    signal  tile1_rxreset0_i                : std_logic;
    signal  tile1_rxreset1_i                : std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    signal  tile1_rxcdrreset0_i             : std_logic;
    signal  tile1_rxcdrreset1_i             : std_logic;
    -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
    signal  tile1_rxbufreset0_i             : std_logic;
    signal  tile1_rxbufreset1_i             : std_logic;
    ----------------- Receive Ports - RX Polarity Control Ports ----------------
    signal  tile1_rxpolarity0_i             : std_logic;
    signal  tile1_rxpolarity1_i             : std_logic;
    --------------------- Shared Ports - Tile and PLL Ports --------------------
    signal  tile1_gtxreset_i                : std_logic;
    signal  tile1_plllkdet_i                : std_logic;
    signal  tile1_refclkout_i               : std_logic;
    signal  tile1_resetdone0_i              : std_logic;
    signal  tile1_resetdone1_i              : std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    signal  tile1_txdata0_i                 : std_logic_vector(19 downto 0);
    signal  tile1_txdata1_i                 : std_logic_vector(19 downto 0);
    signal  tile1_txreset0_i                : std_logic;
    signal  tile1_txreset1_i                : std_logic;
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    signal  tile1_txdiffctrl0_i             : std_logic_vector(2 downto 0);
    signal  tile1_txdiffctrl1_i             : std_logic_vector(2 downto 0);


    ------------------------------- Global Signals -----------------------------
    signal  tile0_tx_system_reset0_c        : std_logic;
    signal  tile0_rx_system_reset0_c        : std_logic;
    signal  tile0_tx_system_reset1_c        : std_logic;
    signal  tile0_rx_system_reset1_c        : std_logic;
    signal  tile1_tx_system_reset0_c        : std_logic;
    signal  tile1_rx_system_reset0_c        : std_logic;
    signal  tile1_tx_system_reset1_c        : std_logic;
    signal  tile1_rx_system_reset1_c        : std_logic;
    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
    signal  tied_to_vcc_i                   : std_logic;
    signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);
    signal  drp_clk_in_i                    : std_logic;
    
    signal  tile0_refclkout_bufg_i          : std_logic;
    
    
    ----------------------------- User Clocks ---------------------------------
    signal  tile0_txusrclk0_i               : std_logic;
    signal  tile1_txusrclk0_i               : std_logic;
    signal  refclkout_pll0_locked_i         : std_logic;
    signal  refclkout_pll0_reset_i          : std_logic;
    signal  tile0_refclkout_to_cmt_i        : std_logic;
    signal  refclkout_pll1_locked_i         : std_logic;
    signal  refclkout_pll1_reset_i          : std_logic;
    signal  tile1_refclkout_to_cmt_i        : std_logic;


    ----------------------- Frame check/gen Module Signals --------------------
    signal  tile0_refclk_i                  : std_logic;
    signal  tile0_matchn0_i                 : std_logic;
     
    signal  tile0_txcharisk0_float_i        : std_logic_vector(1 downto 0);
    signal  tile0_txdata0_float_i           : std_logic_vector(19 downto 0);
    
    
    signal  tile0_block_sync0_i             : std_logic;
    signal  tile0_error_count0_i            : std_logic_vector(7 downto 0);
    signal  tile0_frame_check0_reset_i      : std_logic;
    signal  tile0_inc_in0_i                 : std_logic;
    signal  tile0_inc_out0_i                : std_logic;
    signal  tile0_unscrambled_data0_i       : std_logic_vector(19 downto 0);
    signal  tile0_matchn1_i                 : std_logic;
     
    signal  tile0_txcharisk1_float_i        : std_logic_vector(1 downto 0);
    signal  tile0_txdata1_float_i           : std_logic_vector(19 downto 0);
    
    
    signal  tile0_block_sync1_i             : std_logic;
    signal  tile0_error_count1_i            : std_logic_vector(7 downto 0);
    signal  tile0_frame_check1_reset_i      : std_logic;
    signal  tile0_inc_in1_i                 : std_logic;
    signal  tile0_inc_out1_i                : std_logic;
    signal  tile0_unscrambled_data1_i       : std_logic_vector(19 downto 0);

    signal  tile1_refclk_i                  : std_logic;
    signal  tile1_matchn0_i                 : std_logic;
     
    signal  tile1_txcharisk0_float_i        : std_logic_vector(1 downto 0);
    signal  tile1_txdata0_float_i           : std_logic_vector(19 downto 0);
    
    
    signal  tile1_block_sync0_i             : std_logic;
    signal  tile1_error_count0_i            : std_logic_vector(7 downto 0);
    signal  tile1_frame_check0_reset_i      : std_logic;
    signal  tile1_inc_in0_i                 : std_logic;
    signal  tile1_inc_out0_i                : std_logic;
    signal  tile1_unscrambled_data0_i       : std_logic_vector(19 downto 0);
    signal  tile1_matchn1_i                 : std_logic;
     
    signal  tile1_txcharisk1_float_i        : std_logic_vector(1 downto 0);
    signal  tile1_txdata1_float_i           : std_logic_vector(19 downto 0);
    
    
    signal  tile1_block_sync1_i             : std_logic;
    signal  tile1_error_count1_i            : std_logic_vector(7 downto 0);
    signal  tile1_frame_check1_reset_i      : std_logic;
    signal  tile1_inc_in1_i                 : std_logic;
    signal  tile1_inc_out1_i                : std_logic;
    signal  tile1_unscrambled_data1_i       : std_logic_vector(19 downto 0);

    signal  reset_on_data_error_i           : std_logic;


    ----------------------- Chipscope Signals ---------------------------------

    signal  control                         : std_logic_vector(35 downto 0);
    signal  async_in, async_out             : std_logic_vector(255 downto 0);
    

    signal  tile0_tx_data_vio_in0_i         : std_logic_vector(31 downto 0);
    signal  tile0_tx_data_vio_out0_i        : std_logic_vector(31 downto 0);
    signal  tile0_tx_data_vio_in1_i         : std_logic_vector(31 downto 0);
    signal  tile0_tx_data_vio_out1_i        : std_logic_vector(31 downto 0);
    signal  tile0_rx_data_vio_in0_i         : std_logic_vector(31 downto 0);
    signal  tile0_rx_data_vio_out0_i        : std_logic_vector(31 downto 0);
    signal  tile0_rx_data_vio_in1_i         : std_logic_vector(31 downto 0);
    signal  tile0_rx_data_vio_out1_i        : std_logic_vector(31 downto 0);
    signal  tile0_ila_in0_i                 : std_logic_vector(84 downto 0);
    signal  tile0_ila_in1_i                 : std_logic_vector(84 downto 0);

    signal  tile1_tx_data_vio_in0_i         : std_logic_vector(31 downto 0);
    signal  tile1_tx_data_vio_out0_i        : std_logic_vector(31 downto 0);
    signal  tile1_tx_data_vio_in1_i         : std_logic_vector(31 downto 0);
    signal  tile1_tx_data_vio_out1_i        : std_logic_vector(31 downto 0);
    signal  tile1_rx_data_vio_in0_i         : std_logic_vector(31 downto 0);
    signal  tile1_rx_data_vio_out0_i        : std_logic_vector(31 downto 0);
    signal  tile1_rx_data_vio_in1_i         : std_logic_vector(31 downto 0);
    signal  tile1_rx_data_vio_out1_i        : std_logic_vector(31 downto 0);
    signal  tile1_ila_in0_i                 : std_logic_vector(84 downto 0);
    signal  tile1_ila_in1_i                 : std_logic_vector(84 downto 0);


    signal  gtxreset_i                      : std_logic;
    signal  mux_sel_i                       : std_logic;
    signal  not_mux_sel_i                   : std_logic;    
    signal  user_tx_reset_i                 : std_logic;
    signal  user_rx_reset_i                 : std_logic;
    signal  ila_clk0_i                      : std_logic;
    signal  ila_clk1_i                      : std_logic;

    signal dt_out_10_tile00                 : std_logic_vector(9 downto 0);
    signal en_10_tile00                     : std_logic;
    signal center_f_tile00                  : std_logic_vector(36 downto 0);
    signal g1_tile_00                       : std_logic_vector(4 downto 0);
    signal g1_p_tile00                      : std_logic_vector(4 downto 0);
    signal g2_tile00                        : std_logic_vector(4 downto 0);
    signal rst_dru_tile00                   : std_logic;
    signal samv_tile00                      : std_logic_vector(3 downto 0);
    signal sam_tile00                       : std_logic_vector(9 downto 0);
		signal dv_out_tile00                    : std_logic;
    signal d_out_tile00                     : std_logic_vector(9 downto 0);
    signal chk_okko_tile00                  : std_logic;
    signal rt_err_tile00                    : std_logic;
    signal res_alarm_tile00                 : std_logic;
    
    signal dt_out_10_tile01                 : std_logic_vector(9 downto 0);
    signal en_10_tile01                     : std_logic;
    signal center_f_tile01                  : std_logic_vector(36 downto 0);
    signal g1_tile_01                       : std_logic_vector(4 downto 0);
    signal g1_p_tile01                      : std_logic_vector(4 downto 0);
    signal g2_tile01                        : std_logic_vector(4 downto 0);
    signal rst_dru_tile01                   : std_logic;
    signal samv_tile01                      : std_logic_vector(3 downto 0);
    signal sam_tile01                       : std_logic_vector(9 downto 0);
		signal dv_out_tile01                    : std_logic;
    signal d_out_tile01                     : std_logic_vector(9 downto 0);
    signal chk_okko_tile01                  : std_logic;
    signal rt_err_tile01                    : std_logic;
    signal res_alarm_tile01                 : std_logic;
    
    signal dt_out_10_tile10                 : std_logic_vector(9 downto 0);
    signal en_10_tile10                     : std_logic;
    signal center_f_tile10                  : std_logic_vector(36 downto 0);
    signal g1_tile_10                       : std_logic_vector(4 downto 0);
    signal g1_p_tile10                      : std_logic_vector(4 downto 0);
    signal g2_tile10                        : std_logic_vector(4 downto 0);
    signal rst_dru_tile10                   : std_logic;
    signal samv_tile10                      : std_logic_vector(3 downto 0);
    signal sam_tile10                       : std_logic_vector(9 downto 0);
		signal dv_out_tile10                    : std_logic;
    signal d_out_tile10                     : std_logic_vector(9 downto 0);
    signal chk_okko_tile10                  : std_logic;
    signal rt_err_tile10                    : std_logic;
    signal res_alarm_tile10                 : std_logic;
    
    signal dt_out_10_tile11                 : std_logic_vector(9 downto 0);
    signal en_10_tile11                     : std_logic;
    signal center_f_tile11                  : std_logic_vector(36 downto 0);
    signal g1_tile_11                       : std_logic_vector(4 downto 0);
    signal g1_p_tile11                      : std_logic_vector(4 downto 0);
    signal g2_tile11                        : std_logic_vector(4 downto 0);
    signal rst_dru_tile11                   : std_logic;
    signal samv_tile11                      : std_logic_vector(3 downto 0);
    signal sam_tile11                       : std_logic_vector(9 downto 0);
		signal dv_out_tile11                    : std_logic;
    signal d_out_tile11                     : std_logic_vector(9 downto 0);
    signal chk_okko_tile11                  : std_logic;
    signal rt_err_tile11                    : std_logic;
    signal res_alarm_tile11                 : std_logic;

--**************************** Main Body of Code *******************************
begin

    --  Static signal Assigments
    tied_to_ground_i                        <= '0';
    tied_to_ground_vec_i                    <= x"0000000000000000";
    tied_to_vcc_i                           <= '1';
    tied_to_vcc_vec_i                       <= x"ff";


    Inst_dru_tile00: dru 
    PORT MAP(
		  DT_IN => tile0_rxdata0_i,
		  CENTER_F => center_f_tile00,
		  G1 => g1_tile_00,
		  G1_P => g1_p_tile00,
		  G2 => g2_tile00,
		  CLK => tile0_txusrclk0_i,
		  PH_OUT => open,
		  CTRL => open,
		  RST => rst_dru_tile00,
		  SAMV => samv_tile00,
		  SAM => sam_tile00
	  );

  Inst_bs_10_10_tile00: bshift10to10 PORT MAP(
		CLK => tile0_txusrclk0_i,
		RST => rst_dru_tile00,
		DIN => sam_tile00 (9 downto 0),
		DV => samv_tile00 (3 downto 0),
		DV10 => dv_out_tile00,
		DOUT10 => d_out_tile00
	);

  Inst_prbs_chk_par_tile00: prbs_chk_par_10 
  PORT MAP(
		CLK => tile0_txusrclk0_i,
		RST => rst_dru_tile00,
		CHK_OKKO => chk_okko_tile00,
		RT_ERR => rt_err_tile00,
		EN => dv_out_tile00,
		RES_ALARM => res_alarm_tile00,
		DT_IN => d_out_tile00 
	);

center_f_tile00 <= async_out (36 downto 0);
g1_tile_00 <= async_out (41 downto 37);
g1_p_tile00 <= async_out (46 downto 42);
g2_tile00 <= async_out (51 downto 47);
rst_dru_tile00 <= async_out (52);
res_alarm_tile00 <= async_out (53);
async_in(0) <= chk_okko_tile00;

    Inst_dru_tile01: dru 
    PORT MAP(
		  DT_IN => tile0_rxdata1_i,
		  CENTER_F => center_f_tile01,
		  G1 => g1_tile_01,
		  G1_P => g1_p_tile01,
		  G2 => g2_tile01,
		  CLK => tile0_txusrclk0_i,
		  PH_OUT => open,
		  CTRL => open,
		  RST => rst_dru_tile01,
		  SAMV => samv_tile01,
		  SAM => sam_tile01 
	  );
	  
	  Inst_bs_10_10_tile01: bshift10to10 PORT MAP(
		CLK => tile0_txusrclk0_i,
		RST => rst_dru_tile01,
		DIN => sam_tile01 (9 downto 0),
		DV => samv_tile01 (3 downto 0),
		DV10 => dv_out_tile01,
		DOUT10 => d_out_tile01
	);

  Inst_prbs_chk_par_tile01: prbs_chk_par_10 
  PORT MAP(
		CLK => tile0_txusrclk0_i,
		RST => rst_dru_tile01,
		CHK_OKKO => chk_okko_tile01,
		RT_ERR => rt_err_tile01,
		EN => dv_out_tile01,
		RES_ALARM => res_alarm_tile01,
		DT_IN => d_out_tile01
	);
	
center_f_tile01 <= async_out (100 downto 64);
g1_tile_01 <= async_out (105 downto 101);
g1_p_tile01 <= async_out (110 downto 106);
g2_tile01 <= async_out (115 downto 111);
rst_dru_tile01 <= async_out (116);
res_alarm_tile01 <= async_out (117);
async_in(64) <= chk_okko_tile01;	
	
	  Inst_dru_tile10: dru 
    PORT MAP(
		  DT_IN => tile1_rxdata0_i,
		  CENTER_F => center_f_tile10,
		  G1 => g1_tile_10,
		  G1_P => g1_p_tile10,
		  G2 => g2_tile10,
		  CLK => tile1_txusrclk0_i,
		  PH_OUT => open,
		  CTRL => open,
		  RST => rst_dru_tile10,
		  SAMV => samv_tile10,
		  SAM => sam_tile10 
	  );
	  
	  Inst_bs_10_10_tile10: bshift10to10 PORT MAP(
		CLK => tile1_txusrclk0_i,
		RST => rst_dru_tile10,
		DIN => sam_tile10 (9 downto 0),
		DV => samv_tile10 (3 downto 0),
		DV10 => dv_out_tile10,
		DOUT10 => d_out_tile10
	);

  Inst_prbs_chk_par_tile10: prbs_chk_par_10 
  PORT MAP(
		CLK => tile1_txusrclk0_i,
		RST => rst_dru_tile10,
		CHK_OKKO => chk_okko_tile10,
		RT_ERR => rt_err_tile10,
		EN => dv_out_tile10,
		RES_ALARM => res_alarm_tile10,
		DT_IN => d_out_tile10
	);
	  
center_f_tile10 <= async_out (164 downto 128);
g1_tile_10 <= async_out (169 downto 165);
g1_p_tile10 <= async_out (174 downto 170);
g2_tile10 <= async_out (179 downto 175);
rst_dru_tile10 <= async_out (180);
res_alarm_tile10 <= async_out (181); 
async_in(128) <= chk_okko_tile10; 

xgi_sclk <= async_out (190 downto 182);

	  Inst_dru_tile11: dru 
    PORT MAP(
		  DT_IN => tile1_rxdata1_i,
		  CENTER_F => center_f_tile11,
		  G1 => g1_tile_11,
		  G1_P => g1_p_tile11,
		  G2 => g2_tile11,
		  CLK => tile1_txusrclk0_i,
		  PH_OUT => open,
		  CTRL => open,
		  RST => rst_dru_tile11,
		  SAMV => samv_tile11,
		  SAM => sam_tile11 
	  );
    
    Inst_bs_10_10_tile11: bshift10to10 PORT MAP(
		CLK => tile1_txusrclk0_i,
		RST => rst_dru_tile11,
		DIN => sam_tile11 (9 downto 0),
		DV => samv_tile11 (3 downto 0),
		DV10 => dv_out_tile11,
		DOUT10 => d_out_tile11
	);

  Inst_prbs_chk_par_tile11: prbs_chk_par_10 
  PORT MAP(
		CLK => tile1_txusrclk0_i,
		RST => rst_dru_tile11,
		CHK_OKKO => chk_okko_tile11,
		RT_ERR => rt_err_tile11,
		EN => dv_out_tile11,
		RES_ALARM => res_alarm_tile11,
		DT_IN => d_out_tile11
	);

center_f_tile11 <= async_out (228 downto 192);
g1_tile_11 <= async_out (233 downto 229);
g1_p_tile11 <= async_out (238 downto 234);
g2_tile11 <= async_out (243 downto 239);
rst_dru_tile11 <= async_out (244);
res_alarm_tile11 <= async_out (245); 
async_in(192) <= chk_okko_tile11; 

-- common
tile0_rxpolarity0_i <= async_out (250);
tile0_rxpolarity1_i <= async_out (251);
tile1_rxpolarity0_i <= async_out (252);
tile1_rxpolarity1_i <= async_out (253);
tile0_gtxreset_i <= async_out(254);
tile1_gtxreset_i <= async_out(254);  -- the reset is shared in the CSPRO

async_in(254) <= tile0_plllkdet_i;
async_in(255) <= tile1_plllkdet_i;
    
    
  Inst_prbsgen_ser_00: prbsgen_ser PORT MAP(
		CLK => tile0_txusrclk0_i,
		RST => rst_dru_tile00,
		ERR => async_out(246),
		PRBSOUT => prbs_00
	);
    
  Inst_prbsgen_ser_01: prbsgen_ser PORT MAP(
		CLK => tile0_txusrclk0_i,
		RST => rst_dru_tile01,
		ERR => async_out(247),
		PRBSOUT => prbs_01
	);
	
	Inst_prbsgen_ser_10: prbsgen_ser PORT MAP(
		CLK => tile1_txusrclk0_i,
		RST => rst_dru_tile10,
		ERR => async_out(248),
		PRBSOUT => prbs_10
	);
	
	Inst_prbsgen_ser_11: prbsgen_ser PORT MAP(
		CLK => tile1_txusrclk0_i,
		RST => rst_dru_tile11,
		ERR => async_out(249),
		PRBSOUT => prbs_11
	);
	
    tile0_refclk_ibufds_i : IBUFDS
    port map
    (
        O                               =>      tile0_refclk_i,
        I                               =>      TILE0_REFCLK_PAD_P_IN,
        IB                              =>      TILE0_REFCLK_PAD_N_IN
    );

    tile1_refclk_ibufds_i : IBUFDS
    port map
    (
        O                               =>      tile1_refclk_i,
        I                               =>      TILE1_REFCLK_PAD_P_IN,
        IB                              =>      TILE1_REFCLK_PAD_N_IN
    );

    refclkout_pll0_bufg_i : BUFG
    port map
    (
        I                               =>      tile0_refclkout_i,
        O                               =>      tile0_refclkout_to_cmt_i
    );

    refclkout_pll0_reset_i                  <= not tile0_plllkdet_i;
    
    refclkout_pll0_i : MGT_USRCLK_SOURCE_PLL
    generic map
    (
        MULT                            =>      5,
        DIVIDE                          =>      2,
        CLK_PERIOD                      =>      3.22,
        OUT0_DIVIDE                     =>      5,
        OUT1_DIVIDE                     =>      1,
        OUT2_DIVIDE                     =>      1,
        OUT3_DIVIDE                     =>      1,
--        SIMULATION_P                    =>      EXAMPLE_USE_CHIPSCOPE,
        LOCK_WAIT_COUNT                 =>      "0111100101001111"
    )
    port map
    (
        CLK0_OUT                        =>      tile0_txusrclk0_i,
        CLK1_OUT                        =>      open,
        CLK2_OUT                        =>      open,
        CLK3_OUT                        =>      open,
        CLK_IN                          =>      tile0_refclkout_to_cmt_i,
        PLL_LOCKED_OUT                  =>      refclkout_pll0_locked_i,
        PLL_RESET_IN                    =>      refclkout_pll0_reset_i
    );


    refclkout_pll1_bufg_i : BUFG
    port map
    (
        I                               =>      tile1_refclkout_i,
        O                               =>      tile1_refclkout_to_cmt_i
    );

    refclkout_pll1_reset_i                  <= not tile1_plllkdet_i;
    refclkout_pll1_i : MGT_USRCLK_SOURCE_PLL
    generic map
    (
        MULT                            =>      5,
        DIVIDE                          =>      2,
        CLK_PERIOD                      =>      3.22,
        OUT0_DIVIDE                     =>      5,
        OUT1_DIVIDE                     =>      1,
        OUT2_DIVIDE                     =>      1,
        OUT3_DIVIDE                     =>      1,
--        SIMULATION_P                    =>      EXAMPLE_USE_CHIPSCOPE,
        LOCK_WAIT_COUNT                 =>      "0111100101001111"
    )
    port map
    (
        CLK0_OUT                        =>      tile1_txusrclk0_i,
        CLK1_OUT                        =>      open,
        CLK2_OUT                        =>      open,
        CLK3_OUT                        =>      open,
        CLK_IN                          =>      tile1_refclkout_to_cmt_i,
        PLL_LOCKED_OUT                  =>      refclkout_pll1_locked_i,
        PLL_RESET_IN                    =>      refclkout_pll1_reset_i
    );


    -- Wire all PLLLKDET signals to the top level as output ports
    TILE0_PLLLKDET_OUT                      <= tile0_plllkdet_i;
    TILE1_PLLLKDET_OUT                      <= tile1_plllkdet_i;

    -- Hold the TX in reset till the TX user clocks are stable
  
    tile0_txreset0_i                    <= not refclkout_pll0_locked_i;
  
    tile0_txreset1_i                    <= not refclkout_pll0_locked_i;
  
    tile1_txreset0_i                    <= not refclkout_pll1_locked_i;
  
    tile1_txreset1_i                    <= not refclkout_pll1_locked_i;

    -- Hold the RX in reset till the RX user clocks are stable
  
    tile0_rxreset0_i                    <= not refclkout_pll0_locked_i;
  
    tile0_rxreset1_i                    <= not refclkout_pll0_locked_i;
  
    tile1_rxreset0_i                    <= not refclkout_pll1_locked_i;
  
    tile1_rxreset1_i                    <= not refclkout_pll1_locked_i;
 
    tile0_txdata0_i <= (others=>prbs_00);
    tile0_txdata1_i <= (others=>prbs_01);
    tile1_txdata0_i <= (others=>prbs_10);
    tile1_txdata1_i <= (others=>prbs_11);

    rocketio_wrapper_i : ROCKETIO_WRAPPER
--    generic map
--    (
--        WRAPPER_SIM_MODE                =>      EXAMPLE_SIM_MODE,
--        WRAPPER_SIM_GTXRESET_SPEEDUP    =>      EXAMPLE_SIM_GTXRESET_SPEEDUP,
--        WRAPPER_SIM_PLL_PERDIV2         =>      EXAMPLE_SIM_PLL_PERDIV2
--    )
    port map
    (
    

 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --TILE0  (X0Y3)

        ------------------------ Loopback and Powerdown Ports ----------------------
        TILE0_LOOPBACK0_IN              =>      tile0_loopback0_i,
        TILE0_LOOPBACK1_IN              =>      tile0_loopback1_i,
        ------------------- Receive Ports - RX Data Path interface -----------------
        TILE0_RXDATA0_OUT               =>      tile0_rxdata0_i,
        TILE0_RXDATA1_OUT               =>      tile0_rxdata1_i,
        TILE0_RXRESET0_IN               =>      tile0_rxreset0_i,
        TILE0_RXRESET1_IN               =>      tile0_rxreset1_i,
        TILE0_RXUSRCLK0_IN              =>      tile0_txusrclk0_i,
        TILE0_RXUSRCLK1_IN              =>      tile0_txusrclk0_i,
        TILE0_RXUSRCLK20_IN             =>      tile0_txusrclk0_i,
        TILE0_RXUSRCLK21_IN             =>      tile0_txusrclk0_i,
        ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        TILE0_RXCDRRESET0_IN            =>      tile0_rxcdrreset0_i,
        TILE0_RXCDRRESET1_IN            =>      tile0_rxcdrreset1_i,
        TILE0_RXN0_IN                   =>      RXN_IN(0),
        TILE0_RXN1_IN                   =>      RXN_IN(1),
        TILE0_RXP0_IN                   =>      RXP_IN(0),
        TILE0_RXP1_IN                   =>      RXP_IN(1),
        -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
        TILE0_RXBUFRESET0_IN            =>      tile0_rxbufreset0_i,
        TILE0_RXBUFRESET1_IN            =>      tile0_rxbufreset1_i,
        ----------------- Receive Ports - RX Polarity Control Ports ----------------
        TILE0_RXPOLARITY0_IN            =>      tile0_rxpolarity0_i,
        TILE0_RXPOLARITY1_IN            =>      tile0_rxpolarity1_i,
        --------------------- Shared Ports - Tile and PLL Ports --------------------
        TILE0_CLKIN_IN                  =>      tile0_refclk_i,
        TILE0_GTXRESET_IN               =>      tile0_gtxreset_i,
        TILE0_PLLLKDET_OUT              =>      tile0_plllkdet_i,
        TILE0_REFCLKOUT_OUT             =>      tile0_refclkout_i,
        TILE0_RESETDONE0_OUT            =>      tile0_resetdone0_i,
        TILE0_RESETDONE1_OUT            =>      tile0_resetdone1_i,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        TILE0_TXDATA0_IN                =>      tile0_txdata0_i,
        TILE0_TXDATA1_IN                =>      tile0_txdata1_i,
        TILE0_TXRESET0_IN               =>      tile0_txreset0_i,
        TILE0_TXRESET1_IN               =>      tile0_txreset1_i,
        TILE0_TXUSRCLK0_IN              =>      tile0_txusrclk0_i,
        TILE0_TXUSRCLK1_IN              =>      tile0_txusrclk0_i,
        TILE0_TXUSRCLK20_IN             =>      tile0_txusrclk0_i,
        TILE0_TXUSRCLK21_IN             =>      tile0_txusrclk0_i,
        --------------- Transmit Ports - TX Driver and OOB signalling --------------
        TILE0_TXDIFFCTRL0_IN            =>      "000", --tile0_txdiffctrl0_i,
        TILE0_TXDIFFCTRL1_IN            =>      "000", --tile0_txdiffctrl1_i,
        TILE0_TXN0_OUT                  =>      TXN_OUT(0),
        TILE0_TXN1_OUT                  =>      TXN_OUT(1),
        TILE0_TXP0_OUT                  =>      TXP_OUT(0),
        TILE0_TXP1_OUT                  =>      TXP_OUT(1),
 
 
        --_____________________________________________________________________
        --_____________________________________________________________________
        --TILE1  (X0Y4)

        ------------------------ Loopback and Powerdown Ports ----------------------
        TILE1_LOOPBACK0_IN              =>      tile1_loopback0_i,
        TILE1_LOOPBACK1_IN              =>      tile1_loopback1_i,
        ------------------- Receive Ports - RX Data Path interface -----------------
        TILE1_RXDATA0_OUT               =>      tile1_rxdata0_i,
        TILE1_RXDATA1_OUT               =>      tile1_rxdata1_i,
        TILE1_RXRESET0_IN               =>      tile1_rxreset0_i,
        TILE1_RXRESET1_IN               =>      tile1_rxreset1_i,
        TILE1_RXUSRCLK0_IN              =>      tile1_txusrclk0_i,
        TILE1_RXUSRCLK1_IN              =>      tile1_txusrclk0_i,
        TILE1_RXUSRCLK20_IN             =>      tile1_txusrclk0_i,
        TILE1_RXUSRCLK21_IN             =>      tile1_txusrclk0_i,
        ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        TILE1_RXCDRRESET0_IN            =>      tile1_rxcdrreset0_i,
        TILE1_RXCDRRESET1_IN            =>      tile1_rxcdrreset1_i,
        TILE1_RXN0_IN                   =>      RXN_IN(2),
        TILE1_RXN1_IN                   =>      RXN_IN(3),
        TILE1_RXP0_IN                   =>      RXP_IN(2),
        TILE1_RXP1_IN                   =>      RXP_IN(3),
        -------- Receive Ports - RX Elastic Buffer and Phase Alignment Ports -------
        TILE1_RXBUFRESET0_IN            =>      tile1_rxbufreset0_i,
        TILE1_RXBUFRESET1_IN            =>      tile1_rxbufreset1_i,
        ----------------- Receive Ports - RX Polarity Control Ports ----------------
        TILE1_RXPOLARITY0_IN            =>      tile1_rxpolarity0_i,
        TILE1_RXPOLARITY1_IN            =>      tile1_rxpolarity1_i,
        --------------------- Shared Ports - Tile and PLL Ports --------------------
        TILE1_CLKIN_IN                  =>      tile1_refclk_i,
        TILE1_GTXRESET_IN               =>      tile1_gtxreset_i,
        TILE1_PLLLKDET_OUT              =>      tile1_plllkdet_i,
        TILE1_REFCLKOUT_OUT             =>      tile1_refclkout_i,
        TILE1_RESETDONE0_OUT            =>      tile1_resetdone0_i,
        TILE1_RESETDONE1_OUT            =>      tile1_resetdone1_i,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        TILE1_TXDATA0_IN                =>      tile1_txdata0_i,
        TILE1_TXDATA1_IN                =>      tile1_txdata1_i,
        TILE1_TXRESET0_IN               =>      tile1_txreset0_i,
        TILE1_TXRESET1_IN               =>      tile1_txreset1_i,
        TILE1_TXUSRCLK0_IN              =>      tile1_txusrclk0_i,
        TILE1_TXUSRCLK1_IN              =>      tile1_txusrclk0_i,
        TILE1_TXUSRCLK20_IN             =>      tile1_txusrclk0_i,
        TILE1_TXUSRCLK21_IN             =>      tile1_txusrclk0_i,
        --------------- Transmit Ports - TX Driver and OOB signalling --------------
        TILE1_TXDIFFCTRL0_IN            =>      tile1_txdiffctrl0_i,
        TILE1_TXDIFFCTRL1_IN            =>      tile1_txdiffctrl1_i,
        TILE1_TXN0_OUT                  =>      TXN_OUT(2),
        TILE1_TXN1_OUT                  =>      TXN_OUT(3),
        TILE1_TXP0_OUT                  =>      TXP_OUT(2),
        TILE1_TXP1_OUT                  =>      TXP_OUT(3)


    );

vio_inst : chipscope_vio_v1_02_a
  port map (
    CONTROL => control,
    ASYNC_IN => async_in,
    ASYNC_OUT => async_out);

icon_inst : chipscope_icon_v1_03_a
  port map (
    CONTROL0 => control);
    
    
    
end RTL;


