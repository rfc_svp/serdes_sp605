         

#### refresh precompiled libraries

vmap mylib mylib
vmap work work

vlog -work mylib -refresh
vcom -work mylib -refresh
vlog -work work -refresh
vcom -work work -refresh

### compile test_bench files

vcom -explicit  -93 "testbench/bs_10_10/rot20.vhd"
vcom -explicit  -93 "testbench/bs_10_10/maskencoder.vhd"
vcom -explicit  -93 "testbench/bs_10_10/control.vhd"
vcom -explicit  -93 "testbench/bs_10_10/bshift10to10.vhd"
vcom -explicit  -93 "testbench/prbs_chk_par_10.vhd"
vcom -explicit  -93 "testbench/prbsgen_ser.vhd"
vcom -explicit  -93 "testbench/deser_id_20.vhd"
vcom -explicit  -93 "testbench/tb_sim_dru_jitter.vhd"

#### start simulation and add waves

vsim -t 1fs   -lib work tb_sim_dru_jitter

#### add waves

do {wave.do}
view wave
view structure
view signals

#### run simulation

run 600 us


