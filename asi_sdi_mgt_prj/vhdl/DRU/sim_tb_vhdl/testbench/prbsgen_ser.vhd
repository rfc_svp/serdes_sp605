-------------------------------------------------------------------------------
-- Copyright (c) 2005 Xilinx, Inc.
-- This design is confidential and proprietary of Xilinx, All Rights Reserved.
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor: Xilinx
-- \   \   \/    Version: 1.0
--  \   \        Filename: prbsgen_ser.vhd
--  /   /        Date Last Modified:  May 1 2007
-- /___/   /\    Date Created: May 1 2007
-- \   \  /  \
--  \___\/\___\
-- 
--Device: Virtex-5
--Purpose: Simple PRBS 15 generator.
--Reference:
--    
--Revision History:
--    Rev 1.0 - First created, Paolo Novellini and Giovanni Guasti, May 1 2007.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity prbsgen_ser is
  Port (  
    CLK           : in STD_LOGIC; 
    RST	          : in STD_LOGIC;
    ERR           : in std_logic;
    PRBSOUT       : out STD_LOGIC
  );
end prbsgen_ser;

architecture Behavioral of prbsgen_ser is

SIGNAL x                                    : STD_LOGIC_VECTOR(31 downto 0);
SIGNAL err_r, err_rr, err_sync, err_sync_r  : std_logic;

begin

PROCESS(CLK,RST)
begin
  if RST='0' then    
    PRBSOUT <= '0';
  elsif CLK='1' and CLK'event then
    PRBSOUT <= x(0) XOR err_sync_r;
  end if;
end PROCESS;

--PRBSOUT <= x(0) XOR err_sync_r;

PROCESS(CLK,RST)
begin
  if RST='0' then    
    x <=x"55555555";
  elsif CLK='1' and CLK'event then
    x(0) <= x(15) XOR x(14);     -- Uncomment this line for PRBS data
    -- x(0) <= x(15);            -- Uncomment this line for max toggling data
    x(15 downto 1) <= x(14 downto 0);		
  end if;
end PROCESS;

PROCESS(CLK,RST)
begin
  if RST='0' then    
    err_r <= '0';
    err_rr <= '0';
    err_sync_r <= '0';
  elsif CLK='1' and CLK'event then
    err_r <= ERR;
    err_rr <= err_r;
    err_sync_r <= err_sync;
  end if;
end PROCESS;

err_sync <= err_r AND NOT(err_rr);

end Behavioral;

