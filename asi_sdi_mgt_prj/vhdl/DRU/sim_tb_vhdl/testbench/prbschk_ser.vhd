-------------------------------------------------------------------------------
-- Copyright (c) 2005 Xilinx, Inc.
-- This design is confidential and proprietary of Xilinx, All Rights Reserved.
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor: Xilinx
-- \   \   \/    Version: 1.0
--  \   \        Filename: prbsgen_ser.vhd
--  /   /        Date Last Modified:  May 1 2007
-- /___/   /\    Date Created: May 1 2007
-- \   \  /  \
--  \___\/\___\
-- 
--Device: Virtex-5
--Purpose: Serial PRBS checker.
--Reference:
--    
--Revision History:
--    Rev 1.0 - First created, Paolo Novellini and Giovanni Guasti, May 1 2007.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity prbschk_ser is
    Port (  
    	CLK2M	: in	STD_LOGIC; --CLK=2MHz
    	EN      : in STD_LOGIC;
	RST	: in	STD_LOGIC;
	ERR	: out 	STD_LOGIC;
	ERR_RST	: in 	STD_LOGIC;
	RT_ERR : out STD_LOGIC;
	PRBSIN	: in	std_logic
	);
end prbschk_ser;

architecture Behavioral of prbschk_ser is

SIGNAL	x		: 	STD_LOGIC_VECTOR(31 downto 0);
SIGNAL	cmp, err_int    :	STD_LOGIC;

begin

cmp<=x(15) XOR x(14);
ERR<=err_int;

PROCESS(CLK2M,RST)
begin
if RST='0' then
		x <=x"55555555";
elsif CLK2M='1' and CLK2M'event then
	if EN='1' then 
		x(0)<=PRBSIN;
		x(15 downto 1)<=x(14 downto 0);
	else null;
	end if;
end if;
end PROCESS;


PROCESS(CLK2M, RST)
begin

	if RST='0' then
		err_int <='0';
	elsif CLK2M='1' and EN='1' and CLK2M'event then
	
		if ERR_RST='1' then
			err_int<='0';
			RT_ERR<='0';
		else 
			err_int<=err_int OR (PRBSIN XOR cmp);
			RT_ERR<=PRBSIN XOR cmp;
		end if;		
	end if;
end PROCESS;





end Behavioral;

