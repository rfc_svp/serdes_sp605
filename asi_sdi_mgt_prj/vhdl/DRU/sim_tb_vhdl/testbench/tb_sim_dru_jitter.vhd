-------------------------------------------------------------------------------
-- Copyright (c) 2005 Xilinx, Inc.
-- This design is confidential and proprietary of Xilinx, All Rights Reserved.
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor: Xilinx
-- \   \   \/    Version: 1.1
--  \   \        Filename: tb_sim_dru.vhd
--  /   /        Date Last Modified:  May 1 2007
-- /___/   /\    Date Created: May 1 2007
-- \   \  /  \
--  \___\/\___\
-- 
--Device: Virtex-5
--Purpose: This is the test bench to simulate the NIDRU at different rates.
--
--
-- Fast Ethernet: 125Mbit/s
-- T3: 44.736Mbit/s
-- 97.728 Mbit/s
-- OC1: 51.84 Mbit/s
-- OC3: 155.52 Mbit/s
-- E4: 139.264 Mbit/s
-- Proprietary: 510 Mbit/s
-- Proprietary: 1000 Mbit/s
-- Proprietary: 1250 Mbit/s
--         
--Revision History:
--    Rev 1.0 - First created, Paolo Novellini and Giovanni Guasti, May 1 2007.
------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;

ENTITY tb_sim_dru_jitter IS
END tb_sim_dru_jitter;

ARCHITECTURE behavior OF tb_sim_dru_jitter IS 
  
  COMPONENT dru
    PORT(
      DT_IN    : IN std_logic_vector(19 downto 0);
      CLK      : IN std_logic;
      CENTER_F : IN std_logic_vector (36 downto 0);  
      G1       : in STD_LOGIC_VECTOR(4 downto 0);
      G2       : in STD_LOGIC_VECTOR(4 downto 0);
      G1_P     : in STD_LOGIC_VECTOR(4 downto 0);
      RST      : IN std_logic;
      SAMV     : OUT std_logic_vector(3 downto 0);
      SAM      : OUT std_logic_vector(9 downto 0);
      CTRL     : out std_logic_vector (31 downto 0)
    );
  END COMPONENT;

  COMPONENT prbsgen_ser
    PORT(
      CLK     : IN std_logic;
      RST     : IN std_logic; 
      ERR     : IN std_logic;         
      PRBSOUT : OUT std_logic
    );
  END COMPONENT;
 
  COMPONENT prbs_chk_par_10
    PORT(
      CLK       : IN std_logic;
      RST       : IN std_logic;
      EN        : IN std_logic;
      RES_ALARM : IN std_logic;
      DT_IN     : IN std_logic_vector(9 downto 0);          
      CHK_OKKO  : OUT std_logic;
      RT_ERR    : OUT std_logic
    );
  END COMPONENT;

  COMPONENT deser_id_20
    PORT(
      CLK       : IN std_logic;
      RST       : IN std_logic;
      DT_IN     : IN std_logic;     
      CLK_OUT   : OUT std_logic;     
      DT_OUT    : OUT std_logic_vector(19 downto 0)
    );
  END COMPONENT;

  COMPONENT bshift10to10
    PORT(
      CLK       : IN std_logic;
      RST       : IN std_logic;
      DIN       : IN std_logic_vector(9 downto 0);
      DV        : IN std_logic_vector(3 downto 0);          
      DV10      : OUT std_logic;
      DOUT10    : OUT std_logic_vector(9 downto 0)
    );
  END COMPONENT;

  SIGNAL CLK_HF                : std_logic                    :='0';
  SIGNAL dt_in                 : std_logic                    := '0';
  SIGNAL clk_dt                : std_logic                    := '1';
  SIGNAL clk_dt_del            : std_logic;
  SIGNAL rit                   : std_logic; 
  SIGNAL RST                   : std_logic                    := '0';
  SIGNAL PH_MOVE               : std_logic_vector(5 downto 0) := (others=>'0');
  SIGNAL CENTER_F              : std_logic_vector(36 downto 0):= (others=>'0');
  SIGNAL dt_out_10             : std_logic_vector(9 downto 0);
  SIGNAL en_10                 : std_logic;
  SIGNAL CLK                   : std_logic;
  SIGNAL dt_out_ds             : std_logic_vector(19 downto 0);
  SIGNAL samv                  : std_logic_vector (3 downto 0);
  SIGNAL sam                   : std_logic_vector (9 downto 0);
  SIGNAL g1                    : std_logic_vector(4 downto 0);
  SIGNAL g2, g1_p              : std_logic_vector(4 downto 0);
  SIGNAL ctrl                  : std_logic_vector(31 downto 0);
  SIGNAL rt_err                : std_logic;
  
BEGIN

  uut: dru PORT MAP(
    DT_IN => dt_out_ds,
    CENTER_F => CENTER_F,  
    G1 => g1,
    G2 => g2,
    G1_P => g1_p,
    CLK => CLK,
    CTRL => ctrl,
    RST => RST,
    SAMV => samv,
    SAM => sam
  );


  Inst_prbsgen_ser: prbsgen_ser PORT MAP(
    CLK => clk_dt_del,
    RST => RST,
    ERR => '0',
    PRBSOUT => dt_in
  );

  Inst_bs_10_10: bshift10to10 PORT MAP(
    CLK => CLK,
    RST => RST,
    DIN => sam (9 downto 0),
    DV => samv (3 downto 0),
    DV10 => en_10,
    DOUT10 => dt_out_10
  );

  Inst_prbs_chk_par_10: prbs_chk_par_10 PORT MAP(
    CLK => CLK,
    RST => RST,
    CHK_OKKO => open,
    RT_ERR => rt_err,
    EN => en_10,
    RES_ALARM => '0',
    DT_IN => dt_out_10
  );
  
  Inst_deser_id_20: deser_id_20 PORT MAP(
    CLK => CLK_HF,
    RST => RST,
    DT_IN => dt_in,     
    DT_OUT => dt_out_ds,
    CLK_OUT => CLK
  );

  tb : PROCESS
    BEGIN
	
     g1 <= "01011";
     g1_p <= "01011";
     g2 <= "01011";
     rit <= '0';
     RST <= '0';
     wait for 100 ns;
     RST <='0';
     wait for 100 ns;
     RST<='1';
     wait for 200 us ;
     rit <= '1';  -- instantaneus step in the data phase to force a bit error.
     wait;

  END PROCESS;

  clk_dt_del <= clk_dt XOR rit ;   -- this clock generates data with a step in the phase.
  
---------------------------------------------------------------------------------------------
-- Use these controls to use 125Mbit/s (Fast Ethernet) with 155.52MHz refclk
---------------------------------------------------------------------------------------------
	          
  CENTER_F <= b"0000011001101110000101110010110101001";
  CLK_HF <= NOT(CLK_HF) after 160.751 ps; 		-- 3.11G --> Oversampling ratio: 24.88X
-- clk_dt <= NOT(clk_dt) after 4.0000 ns;		-- The clock generating the data nominal
  clk_dt <= NOT(clk_dt) after 4.0004 ns;		-- The clock generating the data is -100ppm
-- clk_dt <= NOT(clk_dt) after 4.0001 ns;		-- The clock generating the data is -25ppm
-- clk_dt <= NOT(clk_dt) after 3.9999 ns;		-- The clock generating the data is +25ppm
-- clk_dt <= NOT(clk_dt) after 4.0008 ns;		-- The clock generating the data is -200ppm
-- clk_dt <= NOT(clk_dt) after 3.9992 ns;		-- The clock generating the data is +200ppm
-- clk_dt <= NOT(clk_dt) after 3.9996 ns;		-- The clock generating the data is +100ppm

   
      
-------------------------------------------------------------------------------------
-- Use these controls to use 51.84Mbit/s (OC1) with 155.52MHz refclk
-------------------------------------------------------------------------------------  
  
-- CENTER_F <= b"0000001010101010101010101010101010101";  -- 3.11G --> Oversampling ratio: 60X
-- CLK_HF <= NOT(CLK_HF) after 160.751 ps; 		  -- This is the local artificial reference clock
-- clk_dt <= NOT(clk_dt) after 9.6451 ns;		-- The clock generating the data nominal
-- clk_dt <= NOT(clk_dt) after 9.6460 ns;		-- The clock generating the data is -100ppm
-- clk_dt <= NOT(clk_dt) after 9.6453 ns;		-- The clock generating the data is -25ppm
-- clk_dt <= NOT(clk_dt) after 9.6470 ns;		-- The clock generating the data is -200ppm
-- clk_dt <= NOT(clk_dt) after 9.6431 ns;		-- The clock generating the data is +200ppm
-- clk_dt <= NOT(clk_dt) after 9.6441 ns;		-- The clock generating the data is +100ppm



--------------------------------------------------------------------------------------
-- Use these controls to use 155.52Mbit/s (OC3-ITUTG703) with 155.2MHz refclk
--------------------------------------------------------------------------------------   
  
-- CENTER_F <= b"0000100000000000000000000000000000000";   -- 3.11G --> Oversampling ratio: 20X
-- CLK_HF <= NOT(CLK_HF) after 160.751 ps; 		  -- This is the local artificial reference clock
-- clk_dt <= NOT(clk_dt) after 3.2150 ns;		-- The clock generating the data nominal
-- clk_dt <= NOT(clk_dt) after 3.2153 ns;		-- The clock generating the data is -100ppm
-- clk_dt <= NOT(clk_dt) after 3.2151 ns;		-- The clock generating the data is -25ppm
-- clk_dt <= NOT(clk_dt) after 3.2157 ns;		-- The clock generating the data is -200ppm
-- clk_dt <= NOT(clk_dt) after 3.2144 ns;		-- The clock generating the data is +200ppm
-- clk_dt <= NOT(clk_dt) after 3.2147 ns;		-- The clock generating the data is +100ppm



------------------------------------------------------------------------------------------
-- Use these controls to use 139.264Mbit/s (PDH E4-ITUTG703) with 155.52MHz refclk
------------------------------------------------------------------------------------------   
  
-- CENTER_F <= b"0000011100101001111011011110000100111";  -- 3.11GG --> Oversampling ratio: 22.33X
-- CLK_HF <= NOT(CLK_HF) after 160.751 ps; 		  -- This is the local artificial reference clock
-- clk_dt <= NOT(clk_dt) after 3.5903 ns;		-- The clock generating the data nominal
-- clk_dt <= NOT(clk_dt) after 3.5906 ns;		-- The clock generating the data is -100ppm
-- clk_dt <= NOT(clk_dt) after 3.5904 ns;		-- The clock generating the data is -25ppm
-- clk_dt <= NOT(clk_dt) after 3.5910 ns;		-- The clock generating the data is -200ppm
-- clk_dt <= NOT(clk_dt) after 3.5896 ns;		-- The clock generating the data is +200ppm
-- clk_dt <= NOT(clk_dt) after 3.5900 ns;		-- The clock generating the data is +100ppm



---------------------------------------------------------------------------------------
-- Use these controls to use 510.0Mbit/s (Proprietary) with 155.52MHz refclk
---------------------------------------------------------------------------------------   
               
-- CENTER_F <= b"0001101000111100000011001010010001011";  -- 3.11G --> Oversampling ratio: 6.1X
-- CLK_HF <= NOT(CLK_HF) after 160.751 ps; 		  -- This is the local artificial reference clock
-- clk_dt <= NOT(clk_dt) after 0.980392 ns;		-- The clock generating the data nominal
-- clk_dt <= NOT(clk_dt) after 0.980490 ns;		-- The clock generating the data is -100ppm
-- clk_dt <= NOT(clk_dt) after 0.980417 ns;		-- The clock generating the data is -25ppm
-- clk_dt <= NOT(clk_dt) after 0.9805882 ns;		-- The clock generating the data is -200ppm
-- clk_dt <= NOT(clk_dt) after 0.980196 ns;		-- The clock generating the data is +200ppm
-- clk_dt <= NOT(clk_dt) after 0.980294 ns;		-- The clock generating the data is +100ppm

---------------------------------------------------------------------------------------
-- Use these controls to use 630Mbit/s (Proprietary) with 155.52MHz refclk
---------------------------------------------------------------------------------------   
               
-- CENTER_F <= b"0010000001101000010010111101101000010";  -- 3.11G --> Oversampling ratio: 3.11X
-- CLK_HF <= NOT(CLK_HF) after 160.751 ps; 		  -- This is the local artificial reference clock
-- clk_dt <= NOT(clk_dt) after 793.65 ps;		-- The clock generating the data nominal

---------------------------------------------------------------------------------------
-- Use these controls to use 1000.0Mbit/s (Proprietary) with 155.52MHz refclk
---------------------------------------------------------------------------------------   
               
-- CENTER_F <= b"0011001101110000101110010110101001100";  -- 3.11G --> Oversampling ratio: 3.11X
-- CLK_HF <= NOT(CLK_HF) after 160.751 ps; 		  -- This is the local artificial reference clock
-- clk_dt <= NOT(clk_dt) after 0.5 ns;		-- The clock generating the data nominal

 
 
---------------------------------------------------------------------------------------
-- Use these controls to use 1250.0Mbit/s (Proprietary) with 155.52MHz refclk
---------------------------------------------------------------------------------------   
               
-- CENTER_F <= b"0100000001001100111001111100010100000";  -- 3.11G --> Oversampling ratio: 2.49X
-- CLK_HF <= NOT(CLK_HF) after 160.751 ps; 		  -- This is the local artificial reference clock
-- clk_dt <= NOT(clk_dt) after 0.4 ns;		-- The clock generating the data nominal
-- clk_dt <= NOT(clk_dt) after 0.400004 ns;		-- The clock generating the data is -10ppm
-- clk_dt <= NOT(clk_dt) after 0.399996 ns;		-- The clock generating the data is +10ppm

  
  
END;
