-------------------------------------------------------------------------------
-- Copyright (c) 2005 Xilinx, Inc.
-- This design is confidential and proprietary of Xilinx, All Rights Reserved.
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor: Xilinx
-- \   \   \/    Version: 1.0
--  \   \        Filename: tran_detect.vhd
--  /   /        Date Last Modified:  Thu Jan 17 2008
-- /___/   /\    Date Created: Wed May 2 2007
-- \   \  /  \
--  \___\/\___\
-- 
--Device: Virtex-5
--Purpose: Parallel (10) PRBS checker. 
--         
--         
--Revision History:
--    Rev 1.0 - First created, P. Novellini and G. Guasti, Wed May 2 2007.
------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity prbs_chk_par_10 is

Port ( 
      CLK : in std_logic;
      RST : in std_logic;
      CHK_OKKO: out std_logic;
      RT_ERR : out STD_LOGIC;
      EN : in STD_LOGIC;
      RES_ALARM: in std_logic;
      DT_IN : in std_logic_vector(9 downto 0)	
     ); 


end prbs_chk_par_10;

architecture Behavioral of prbs_chk_par_10 is

-- signal definition
	signal dt_r : std_logic_vector(15 downto 0);
	signal dt_rr : std_logic_vector(9 downto 0);
	signal okko_int: std_logic;
	signal chk_okko_int : std_logic;

begin

PROCESS (RST,CLK)
BEGIN
  if RST ='0' then
    dt_r <=(others=>'0');
  elsif CLK='1' and CLK'event and EN='1' then
    dt_r (15)<= DT_IN (9);
    dt_r (14)<= DT_IN (8);
    dt_r (13)<= DT_IN (7);
    dt_r (12)<= DT_IN (6);
    dt_r (11)<= DT_IN (5);
    dt_r (10)<= DT_IN (4);
    dt_r (9)<= DT_IN (3);
    dt_r (8)<= DT_IN (2);
    dt_r (7)<= DT_IN (1);
    dt_r (6)<= DT_IN (0);
    dt_r (5)<= dt_r (15);
    dt_r (4)<= dt_r (14);
    dt_r (3)<= dt_r (13);
    dt_r (2)<= dt_r (12);
    dt_r (1)<= dt_r (11);
    dt_r (0)<= dt_r (10);
  end if;
END PROCESS;

PROCESS (RST,CLK)
BEGIN
	if RST ='0' then
		dt_rr <=(others=>'0');
	elsif CLK='1' and CLK'event and EN='1' then
		dt_rr(0)<=DT_IN(0) XOR dt_r(0) XOR dt_r(1);
		dt_rr(1)<=DT_IN(1) XOR dt_r(1) XOR dt_r(2);
		dt_rr(2)<=DT_IN(2) XOR dt_r(2) XOR dt_r(3);
		dt_rr(3)<=DT_IN(3) XOR dt_r(3) XOR dt_r(4);
		dt_rr(4)<=DT_IN(4) XOR dt_r(4) XOR dt_r(5);
		dt_rr(5)<=DT_IN(5) XOR dt_r(5) XOR dt_r(6);
		dt_rr(6)<=DT_IN(6) XOR dt_r(6) XOR dt_r(7);
		dt_rr(7)<=DT_IN(7) XOR dt_r(7) XOR dt_r(8);
		dt_rr(8)<=DT_IN(8) XOR dt_r(8) XOR dt_r(9);
		dt_rr(9)<=DT_IN(9) XOR dt_r(9) XOR dt_r(10);
	end if;
END PROCESS;
	
PROCESS (RST,CLK)
BEGIN
	if RST ='0' then
		chk_okko_int <='0';
	elsif CLK='1' and CLK'event and EN='1' then
		chk_okko_int<=okko_int OR ((chk_okko_int AND NOT(RES_ALARM))); 					
	end if;
END PROCESS;

okko_int <= (dt_rr(0) OR dt_rr(1) OR dt_rr(2) OR dt_rr(3) OR dt_rr(4) OR dt_rr(5) OR dt_rr(6) OR dt_rr(7) OR dt_rr(8) OR dt_rr(9));
CHK_OKKO<=chk_okko_int;
RT_ERR <= okko_int;

end Behavioral;
