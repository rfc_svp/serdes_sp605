-------------------------------------------------------------------------------
-- Copyright (c) 2005 Xilinx, Inc.
-- This design is confidential and proprietary of Xilinx, All Rights Reserved.
-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /   Vendor: Xilinx
-- \   \   \/    Version: 1.0
--  \   \        Filename: prbsgen_ser.vhd
--  /   /        Date Last Modified:  May 1 2007
-- /___/   /\    Date Created: May 1 2007
-- \   \  /  \
--  \___\/\___\
-- 
--Device: Virtex-5
--Purpose: Ideal by 10 deserializer.
--Reference:
--    
--Revision History:
--    Rev 1.0 - First created, Paolo Novellini and Giovanni Guasti, May 1 2007.
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity deser_id_20 is
    Port ( CLK : in  STD_LOGIC;
    	     RST : in std_logic;
    	     CLK_OUT : OUT std_logic;
           DT_IN : in  STD_LOGIC;
           DT_OUT : out  STD_LOGIC_VECTOR (19 downto 0));
end deser_id_20;

architecture Behavioral of deser_id_20 is

SIGNAL dt_aux_1, dt_aux_2 	: std_logic_vector (19 downto 0);
SIGNAL cnt			: std_logic_vector (5 downto 0);

begin

PROCESS (RST,CLK)
BEGIN
	if RST ='0' then
		dt_aux_1 <= (others=>'0');
	elsif CLK='1' and CLK'event then
		dt_aux_1 (19) <= DT_IN;
		dt_aux_1 (18 downto 0) <= dt_aux_1 (19 downto 1);
	end if;
END PROCESS;

PROCESS (RST,CLK)
BEGIN
	if RST ='0' then
		dt_aux_2 <= (others=>'0');
	elsif CLK='1' and CLK'event and cnt = b"010011" then
		dt_aux_2  <= dt_aux_1;
	end if;
END PROCESS;

PROCESS (RST,CLK)
BEGIN
	if RST ='0' then
		cnt <= (others=>'0');
	elsif CLK='1' and CLK'event then
		if cnt = b"010011" then
			cnt <= b"000000";
		else
			cnt <= cnt + '1';
		end if;
	end if;
END PROCESS;

clk_OUT <= cnt(4);
DT_OUT <= dt_aux_2;

end Behavioral;

