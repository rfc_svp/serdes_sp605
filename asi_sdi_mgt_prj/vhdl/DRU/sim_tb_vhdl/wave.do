onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /tb_sim_dru_jitter/inst_prbs_chk_par_10/rt_err
add wave -noupdate -format Logic /tb_sim_dru_jitter/inst_prbsgen_ser/prbsout
add wave -noupdate -format Analog-Step -height 100 -offset 1000000.0 -radix decimal -scale 5.0000000000000002e-005 /tb_sim_dru_jitter/ctrl
add wave -noupdate -format Literal -radix unsigned /tb_sim_dru_jitter/samv
TreeUpdate [SetDefaultTree]
configure wave -namecolwidth 269
configure wave -valuecolwidth 43
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {0 fs} {320617283951 fs}
