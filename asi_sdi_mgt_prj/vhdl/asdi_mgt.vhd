---======================================================================
--
-- VHDL COMPONENT     : asdi_mgt.vhd
--
-- COMPONENT TYPE     : asdi_mgt | RTL
--
-- CSC0 NAME          : 
--
-- CSC0 CODE          : C�digo asignado en el Family Tree
--
-- PROJECT            : SVP MGT
--
-- DEPARTMENT         : 
--
-- DESCR0PT0ON        :
--   
--
-- REFERENCE          :		
--======================================================================
--
-- OWNER : INDRA
--
--======================================================================
--======================================================================

------------------------------------------------------------------------
--                     DEVELOPMENT HisTORY
------------------------------------------------------------------------
--
-- ACT0V0TY  /  DATE   /   AUTHOR     /  VERS0ON  /    REASON
--
-- MODFIED            /              /           /
--
-- CREATED    17-07-18 / Ra�l Fern�ndez Cardenal     /   V 1.0   / initial Version
------------------------------------------------------------------------

-------------------------------------------------------
--! @file asdi_mgt.vhd
--! @brief ASI SDI S6 MGT
--! @author Ra�l Fern�ndez Cardenal (SVP)
--! @email rfcardenal@indra.es
--! @date 208-18-17
-------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use work.utils_uart_pkg.all;
--use work.asdi_mgt_pkg.all;


--***********************************Ent0ty Declarat0on************************

entity asdi_mgt is
generic(
   EXAMPLE_SIM_GTPRESET_SPEEDUP  : integer  :=   1;
   EXAMPLE_USE_CHIPSCOPE         : integer  :=   1;
   EXAMPLE_SIMULATION            : integer  :=   0 
);
port
(
   
    --*******************SDI**************************************
  TILE0_GTP1_REFCLK_PAD_N_IN:   in  std_logic;              -- GTP
  TILE0_GTP1_REFCLK_PAD_P_IN:   in  std_logic;          
  USER_SMA_GPIO_P:              out std_logic;          
  USER_SMA_GPIO_N:              out std_logic;          
  GPIO_SWITCH_0:                in  std_logic;          
  GPIO_SWITCH_1:                in  std_logic;          
  GPIO_SWITCH_2:                in  std_logic;          
  GPIO_SWITCH_3:                in  std_logic;          
  GPIO_BUTTON0:                 in  std_logic;          
  GPIO_BUTTON1:                 in  std_logic;          
  GPIO_BUTTON2:                 in  std_logic;          
  GPIO_BUTTON3:                 in  std_logic;          
  CPU_RESET:                    in  std_logic;          
  GPIO_HEADER_0_LS:             out std_logic;          
  GPIO_HEADER_1_LS:             out std_logic;          
  GPIO_HEADER_2_LS:             out std_logic;          
  GPIO_HEADER_3_LS:             out std_logic;          
  GPIO_LED_0:                   out std_logic;          
  GPIO_LED_1:                   out std_logic;          
  GPIO_LED_2:                   out std_logic;          
  GPIO_LED_3:                   out std_logic;          
  FPGA_AWAKE:                   out std_logic;          
  RXN_IN:                       in  std_logic_vector (1 downto 0);   
  RXP_IN:                       in  std_logic_vector (1 downto 0);   
  TXN_OUT:                      out std_logic_vector (1 downto 0);   
  TXP_OUT:                      out std_logic_vector (1 downto 0);  


	--*******************uart**************************************
		
 USER_CLOCK:           in  std_logic;                -- 27 MHz X0	
 UART_RXDATA            : in   std_logic;
 UART_TXDATA            : out   std_logic	
	
);

   
end asdi_mgt;
    
architecture RTL of asdi_mgt is

--**************************Component Declarat0ons*****************************

component SVP_SDI 
generic(
   EXAMPLE_SIM_GTPRESET_SPEEDUP  : integer  :=   1;
   EXAMPLE_USE_CHIPSCOPE         : integer  :=   1;
   EXAMPLE_SIMULATION            : integer  :=   0 
);
port(
  TILE0_GTP1_REFCLK_PAD_N_IN:   in  std_logic;              -- GTP
  TILE0_GTP1_REFCLK_PAD_P_IN:   in  std_logic;          
  USER_SMA_GPIO_P:              out std_logic;          
  USER_SMA_GPIO_N:              out std_logic;          
  GPIO_SWITCH_0:                in  std_logic;          
  GPIO_SWITCH_1:                in  std_logic;          
  GPIO_SWITCH_2:                in  std_logic;          
  GPIO_SWITCH_3:                in  std_logic;          
  GPIO_BUTTON0:                 in  std_logic;          
  GPIO_BUTTON1:                 in  std_logic;          
  GPIO_BUTTON2:                 in  std_logic;          
  GPIO_BUTTON3:                 in  std_logic;          
  CPU_RESET:                    in  std_logic;          
  GPIO_HEADER_0_LS:             out std_logic;          
  GPIO_HEADER_1_LS:             out std_logic;          
  GPIO_HEADER_2_LS:             out std_logic;          
  GPIO_HEADER_3_LS:             out std_logic;          
  GPIO_LED_0:                   out std_logic;          
  GPIO_LED_1:                   out std_logic;          
  GPIO_LED_2:                   out std_logic;          
  GPIO_LED_3:                   out std_logic;          
  FPGA_AWAKE:                   out std_logic;          
  RXN_IN:                       in  std_logic_vector (1 downto 0);   
  RXP_IN:                       in  std_logic_vector (1 downto 0);   
  TXN_OUT:                      out std_logic_vector (1 downto 0);   
  TXP_OUT:                      out std_logic_vector (1 downto 0);   
    
-- ************************** FMC connector *****************************
  clk_27M_in:           in  std_logic
);
end component;

component USER_UART  
	generic (
		ClkFreq				: inTEGER:=40000000;	-- clock freq (Hz)
		BaudRate			: inTEGER:=9600;		-- bps
		bit8				: std_logic:='1';		-- byte = '1' 
		parity_en			: std_logic:='1';		-- enable = '1' 
		odd_n_even			: std_logic:='1'		-- odd = '1' even = '0'
	);
	port (
		n_RESET        		: in        std_logic;
		sys_clk            	: in        std_logic;
		-- 0/f CORE 0P
		RXDATA         		: in        std_logic;
		TXDATA         		: out       std_logic;
		-- 0/f MASTER BUS
		ADDRESS_UART   		:   out		std_logic_vector (15 downto 0); -- Address bus
		RD_DATA_BUS_UART    :   in		std_logic_vector (15 downto 0); -- input data bus
		WD_DATA_BUS_UART  	:   out		std_logic_vector (15 downto 0); -- Output data bus
		RD_nWD_EN_UART		:   out		std_logic;                      -- Read(act0ve h0gh)/wr0te(act0ve low) enable
		nCS_UART			:	out		std_logic;						-- Operat0on request(act0ve low)
		ACCESS_VALID_UART	:	in		ACCESS_VALID_UART_type;			-- Access val0d acknowledge
		WAIT_UP_ADC_DATA	:	in		std_logic;						-- Ready data in the ADC
		--	error flags
		UART_BIT_I_S		:	out	std_logic;						-- module in inval0d state
		UART_BIT_W_D		:	out	std_logic;						-- module is STOPED
		-- debug outputs
		RXRDY_o 			:	out	std_logic;
		TXRDY_o 			:	out	std_logic
	);
end component;

 COMPONENT blk_mem_gen_v6_3 IS
  PORT (
      --Port A
  
    WEA        : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    ADDRA      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  
    DINA       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
  
    DOUTA      : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);

  
    CLKA       : IN STD_LOGIC;

  
      --Port B
  
    WEB        : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    ADDRB      : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
  
    DINB       : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    DOUTB      : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    CLKB       : IN STD_LOGIC


  );
  END COMPONENT;
  
  
--***********************************Parameter Declarations********************

    constant DLY : time := 1 ns;

    
--************************** Register Declarations ****************************
	
	 ----------------------------- UART signals ---------------------------------
	
	signal data_out_uart: std_logic_vector(15 downto 0);
	signal data_in_uart: std_logic_vector(15 downto 0);
	signal add_uart: std_logic_vector(15 downto 0);
	
	signal  RnW : std_logic;
	signal  CS_N_UART : std_logic;
	signal  RESET_N : std_logic;
	
	
  SIGNAL iUSER_CLOCK  : STD_LOGIC;

	 ----------------------------- Others signals ---------------------------------
	
	
    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
    signal  tied_to_vcc_i                   : std_logic;
    signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);
	
	
	signal wea        :  std_logic_vector(0 downto 0);
	signal web        :  std_logic_vector(0 downto 0);
    signal addra      :  std_logic_vector(3 downto 0);
    signal addrb      :  std_logic_vector(3 downto 0);
  
    signal dinb       :  std_logic_vector(15 downto 0);
    signal doutb      :  std_logic_vector(15 downto 0);

	   
--**************************** Main Body of Code *******************************
begin

    --  Stat0c signal Ass0gments
    tied_to_ground_i                        <= '0';
    tied_to_ground_vec_i                    <= x"0000000000000000";
    tied_to_vcc_i                           <= '1';
    tied_to_vcc_vec_i                       <= x"ff";
	
	
	RESET_N <= not CPU_RESET;
   
--**************************** sdi *******************************

	
	--**************************** UART RAM WRITE/READ CONTROL *******************************	

	wea(0) <= not RnW;
	addra <= add_uart(3 downto 0);
	addrb <= tied_to_ground_vec_i(3 downto 0);
	
	gUSER_CLOCK : BUFG
    PORT MAP (
     I => USER_CLOCK,
     O => iUSER_CLOCK
     );

   
  bmg0 : blk_mem_gen_v6_3
    PORT MAP (
      --Port A
  
      WEA        => wea,
      ADDRA      => addra,
     
	  DINA       => data_out_uart,
      DOUTA      => data_in_uart,
      CLKA       => iUSER_CLOCK,

  
      --Port B
  
      WEB        => web,
      ADDRB      => addrb,
  
      DINB       => dinb,
      DOUTB      => doutb,
      CLKB       => iUSER_CLOCK

    );
	
	web     <= "0";
	dinb 	<= X"0000";
	addrb   <= "0000";

	
	--**************************** UART CONTROL *******************************	
	
	
	
USER_UART_P: USER_UART  
	generic map (
		ClkFreq				  =>      27000000,	-- clock freq (Hz)
		BaudRate			  =>      9600,		--9600 bps -to simulate set to 1000000
		bit8				  =>      '1',		-- byte = '1' 
		parity_en			  =>      '0',		-- enable = '1' 
		odd_n_even			  =>      '1'		-- odd = '1' even = '0'
	)
	port map (
		n_RESET        		  =>     RESET_N,
		sys_clk               =>     iUSER_CLOCK,
		-- 0/f CORE 0P
		RXDATA         		  =>     UART_RXDATA,
		TXDATA         		  =>     UART_TXDATA,
		-- 0/f MASTER BUS
		ADDRESS_UART   		  =>     add_uart, 			    -- Address bus
		RD_DATA_BUS_UART      =>     data_in_uart, 			-- input data bus
		WD_DATA_BUS_UART  	  =>     data_out_uart,			-- Output data bus
		RD_nWD_EN_UART		  =>     RnW,              		-- Read(act0ve h0gh)/wr0te(act0ve low) enable
		nCS_UART			  =>     CS_N_UART,				-- Operat0on request(act0ve low)
		ACCESS_VALID_UART	  =>     DEV_DATA_OK,		-- Access val0d acknowledge
		WAIT_UP_ADC_DATA	  =>     tied_to_vcc_i,			-- Ready data in the ADC
		--	error flags
		UART_BIT_I_S		  =>     open,					-- module in inval0d state
		UART_BIT_W_D		  =>     open,					-- module is STOPED
		-- debug outputs
		RXRDY_o 			  =>     open,
		TXRDY_o 			  =>     open 
	);
	
 --**************************** SDI *******************************	

 SVP_SDI_P: SVP_SDI 
generic map(
   EXAMPLE_SIM_GTPRESET_SPEEDUP  => EXAMPLE_SIM_GTPRESET_SPEEDUP,
   EXAMPLE_USE_CHIPSCOPE         => EXAMPLE_USE_CHIPSCOPE,
   EXAMPLE_SIMULATION            => EXAMPLE_SIMULATION
)
port map(
  TILE0_GTP1_REFCLK_PAD_N_IN    =>  TILE0_GTP1_REFCLK_PAD_N_IN,           
  TILE0_GTP1_REFCLK_PAD_P_IN	=>  TILE0_GTP1_REFCLK_PAD_P_IN,      
  USER_SMA_GPIO_P				=>  USER_SMA_GPIO_P,			        
  USER_SMA_GPIO_N				=>  USER_SMA_GPIO_N,			    
  GPIO_SWITCH_0					=>  GPIO_SWITCH_0,				        
  GPIO_SWITCH_1					=>  GPIO_SWITCH_1,				       
  GPIO_SWITCH_2					=>  GPIO_SWITCH_2,				       
  GPIO_SWITCH_3					=>  GPIO_SWITCH_3,				        
  GPIO_BUTTON0					=>  GPIO_BUTTON0,				  
  GPIO_BUTTON1					=>  GPIO_BUTTON1,				       
  GPIO_BUTTON2					=>  GPIO_BUTTON2,				       
  GPIO_BUTTON3					=>  GPIO_BUTTON3,				       
  CPU_RESET						=>  CPU_RESET,					        
  GPIO_HEADER_0_LS				=>  GPIO_HEADER_0_LS,			       
  GPIO_HEADER_1_LS				=>  GPIO_HEADER_1_LS,			       
  GPIO_HEADER_2_LS				=>  GPIO_HEADER_2_LS,			        
  GPIO_HEADER_3_LS				=>  GPIO_HEADER_3_LS,			       
  GPIO_LED_0					=>  GPIO_LED_0,				      
  GPIO_LED_1					=>  GPIO_LED_1,				     
  GPIO_LED_2					=>  GPIO_LED_2,				      
  GPIO_LED_3					=>  GPIO_LED_3,				     
  FPGA_AWAKE					=>  FPGA_AWAKE,				       
  RXN_IN						=>  RXN_IN,					
  RXP_IN						=>  RXP_IN,					
  TXN_OUT						=>  TXN_OUT,					 
  TXP_OUT						=>  TXP_OUT,					
    
-- ************************** FMC connector *****************************
  clk_27M_in			=> iUSER_CLOCK
);


	

end RTL;

