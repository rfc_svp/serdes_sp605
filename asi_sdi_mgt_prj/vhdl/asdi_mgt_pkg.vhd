---======================================================================
--
-- VHDL COMPONENT     : asdi_mgt_pkg.vhd
--
-- COMPONENT TYPE     : asdi_mgt_pkg | RTL
--
-- CSC0 NAME          : 
--
-- CSC0 CODE          : C�digo asignado en el Family Tree
--
-- PROJECT            : SVP MGT
--
-- DEPARTMENT         : 
--
-- DESCR0PT0ON        :
--   
--
-- REFERENCE          :		
--======================================================================
--
-- OWNER : INDRA
--
--======================================================================
--======================================================================

------------------------------------------------------------------------
--                     DEVELOPMENT HisTORY
------------------------------------------------------------------------
--
-- ACT0V0TY  /  DATE   /   AUTHOR     /  VERS0ON  /    REASON
--
-- MODFIED            /              /           /
--
-- CREATED    17-07-18 / Ra�l Fern�ndez Cardenal     /   V 1.0   / initial Version
------------------------------------------------------------------------

-------------------------------------------------------
--! @file asdi_mgt_pkg.vhd
--! @brief ASI SDI S6 MGT
--! @author Ra�l Fern�ndez Cardenal (SVP)
--! @email rfcardenal@indra.es
--! @date 208-18-17
-------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package asdi_mgt_pkg is

-- main subtypes
subtype HALFBYTE is std_logic_vector(3 downto 0);
subtype BYTE is std_logic_vector(7 downto 0);
subtype WORD is std_logic_vector(15 downto 0);
subtype DWORD is std_logic_vector(31 downto 0);

-- main arrays
TYPE HALFBYTE_vector IS ARRAY (NATURAL RANGE <>) OF HALFBYTE;
TYPE BYTE_vector IS ARRAY (NATURAL RANGE <>) OF BYTE;
TYPE WORD_vector IS ARRAY (NATURAL RANGE <>) OF WORD;
TYPE DWORD_vector IS ARRAY (NATURAL RANGE <>) OF DWORD;

type data_m is array (11 downto 0) of std_logic_vector(15 downto 0);
type addr_m is array (11 downto 0) of std_logic_vector(11 downto 0);


-- base-2 logarithm
function log2 (Taps : integer) return integer;

-- bit reverse
function REVERSE(X : in std_logic_vector) return std_logic_vector;



end asdi_mgt_pkg;

package body asdi_mgt_pkg is

function log2 (Taps : integer) return integer is
   variable i: integer;
begin
   
   if Taps>1 then
	   for i in 0 to Taps - 1 loop
		  if 2**i >= Taps then
			return i;
		  end if;
	   end loop;
   else
		return 1;
   end if;   
   
end log2;

---------------------------------------------------------------------------
function REVERSE(X : in std_logic_vector) return std_logic_vector is
  variable Y : std_logic_vector(X'range);
  variable low : integer;
  variable high : integer;
begin
  if (X'left = X'low) then
    for i in 0 to X'length - 1 loop
      Y(X'low + X'length - i - 1) := X(i + X'low);
    end loop;
  else
    for i in 0 to X'length - 1 loop
      Y(X'low + i) := X(X'low + X'length - 1 - i);
    end loop;
  end if;
  return Y;
end REVERSE;


end asdi_mgt_pkg;