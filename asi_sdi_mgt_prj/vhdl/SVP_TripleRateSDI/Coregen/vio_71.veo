///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor     : Xilinx
// \   \   \/     Version    : 1.0
//  \   \         Application: Xilinx CORE Generator
//  /   /         Filename   : vio_71.veo
// /___/   /\     Timestamp  : Tue Feb 16 15:52:50 Mountain Standard Time 2010
// \   \  /  \
//  \___\/\___\
//
// Design Name: ISE Instantiation template
///////////////////////////////////////////////////////////////////////////////

// The following must be inserted into your Verilog file for this
// core to be instantiated. Change the instance name and port connections
// (in parentheses) to your own signal names.

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
vio_71 YourInstanceName (
    .CONTROL(CONTROL), // INOUT BUS [35:0]
    .ASYNC_IN(ASYNC_IN) // IN BUS [70:0]
);

// INST_TAG_END ------ End INSTANTIATION Template ---------

