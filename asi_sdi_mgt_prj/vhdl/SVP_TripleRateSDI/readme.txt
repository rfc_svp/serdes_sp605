*******************************************************************************
** � Copyright 2010�2011 Xilinx, Inc. All rights reserved.
** This file contains confidential and proprietary information of Xilinx, Inc. and 
** is protected under U.S. and international copyright and other intellectual property laws.
*******************************************************************************
**   ____  ____ 
**  /   /\/   / 
** /___/  \  /   Vendor: Xilinx 
** \   \   \/    
**  \   \        readme.txt Version: 1.2  
**  /   /        Date Last Modified: February 10, 2011 
** /___/   /\    Date Created: November 17, 2010
** \   \  /  \   Associated Filename: xapp1076_S6GTP_TripleRateSDI.zip
**  \___\/\___\ 
** 
**  Device: Spartan-6 LXT
**  Purpose: Triple-Rate SDI Reference Design
**  Reference: XAPP1076
**  Revision History: 
**     November 17, 2010
**     Initial release.
**
**     December 23, 2010 
**     Fixed problem with pathological pattern for 720p 50 Hz.
**     The following files were modified:
**        Demo Files/SP605_triple_rate_sdi.bit
**        Verilog/multigenHD_horz.v
**        VHDL/multigenHD_horz.vhd
**    
**     February 10, 2011
**     Changes to improve SDI TX data eye on demo platform of SP605 with
**     CTXIL671 (AVB FMC card).
**     Changed the files wiz1_4_20b_tile.v and wiz1_4_20b_tile.vhd to apply a
**     value of 7 to the TXPREEMPHSIS1 port on the GTP instance. 
**     Also replaced the demo bit file SP605_triple_rate_SDI_demo.bit which
**     incorporated the changes for an improved TX data eye on the demo platform.
**     The following files were modified:
**        Demo Files/SP605_triple_rate_SDI_demo.bit
**        Verilog/wiz1_4_20b_tile.v
**        VHDL/wiz1_4_20b_tile.vhd
**   
*******************************************************************************
**
**  Disclaimer: 
**
**		This disclaimer is not a license and does not grant any rights to the materials 
**              distributed herewith. Except as otherwise provided in a valid license issued to you 
**              by Xilinx, and to the maximum extent permitted by applicable law: 
**              (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, 
**              AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, 
**              INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR 
**              FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable (whether in contract 
**              or tort, including negligence, or under any other theory of liability) for any loss or damage 
**              of any kind or nature related to, arising under or in connection with these materials, 
**              including for any direct, or any indirect, special, incidental, or consequential loss 
**              or damage (including loss of data, profits, goodwill, or any type of loss or damage suffered 
**              as a result of any action brought by a third party) even if such damage or loss was 
**              reasonably foreseeable or Xilinx had been advised of the possibility of the same.


**  Critical Applications:
**
**		Xilinx products are not designed or intended to be fail-safe, or for use in any application 
**		requiring fail-safe performance, such as life-support or safety devices or systems, 
**		Class III medical devices, nuclear facilities, applications related to the deployment of airbags,
**		or any other applications that could lead to death, personal injury, or severe property or 
**		environmental damage (individually and collectively, "Critical Applications"). Customer assumes 
**		the sole risk and liability of any use of Xilinx products in Critical Applications, subject only 
**		to applicable laws and regulations governing limitations on product liability.

**  THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.

*******************************************************************************

This readme describes how to use the files that come with XAPP1076.

*******************************************************************************
Reference Design Description:

This is a triple-rate SDI reference design for Spartan-6 LXT devices.  This contains a
Triple-rate SDI transmitter with selectable video formats, and a Triple-rate SDI receiver.
The source files for the reference design of the Triple-Rate SDI Reciever, and 
Triple-Rate SDI Transmitter are included in both VHDL and Verilog. 

Demos:

There are also two demo designs which incorporate both RX and TX.  The baseline 
demo contains an independent RX and TX.  The other is a pass-throgh demo incorporating 
genlock. Some of the files for the genlock pass-through demo are in Verilog only. 
*******************************************************************************

** IMPORTANT NOTES **

1) There are two demos both of which require the SP605 board with AVB FMC daughter card.

2) The genlock pass-through demo also requires a clock module in the "Clock Module L" location. 

*******************************************************************************

The top level demo designs are:

	RX and TX demo:
		Verilog\sp605_demo_20b.v 
		VHDL\sp605_demo_20b.vhd 

	Genlock Pass-Through demo:
		verilog\sp605_passthru.v

The top level constraints files are:

	RX and TX demo:
		Verilog\sp605_demo_FMC.ucf

	Genlock Pass-Through demo:
		verilog\sp605_passthru.ucf

*******************************************************************************

To incorporate the sp605_demo_20b module into an ISE design project:

Verilog flow:

1) Use the file file SP605_triple_rate_SDI_demo.xise to establish the project settings.
2) Add Verilog\sp605_demo_20b.v as the top level file.
3) Add other source files from the "verilog" directory for all of the required modules.
4) Copy the files from the "ngc" directory to the project directory.
5) Add verilog\sp605_demo_FMC.ucf as the constraints file.

VHDL flow:

1) Use the file SP605_triple_rate_SDI_demo.xise to establish the project settings.
2) Add vhdl\sp605_demo_20b.vhd as the top level file.
3) Add other source files from the "vhdl" directory for all of the required modules.
4) Copy the files from the "ngc" directory to the project directory.
5) Add verilog\sp605_demo_FMC.ucf as the constraints file.
------------------------------------------------------------------------------
Files included in this release:

Coregen folder    Coregen source files(.xco) and output files produced by core generation: 
  	icon_2.ngc
	icon_2.v
	icon_2.veo
	icon_2.vhd
	icon_2.vho
	icon_2.xco
	ILA_67.ngc
	ILA_67.v
	ILA_67.veo
	ILA_67.vhd
	ILA_67.vho
	ILA_67.xco
	vio_71.ngc
	vio_71.v
	vio_71.veo
	vio_71.vhd
	vio_71.vho
	vio_71.xco

Demo Files folder:
	sp605_passthru_genlock_SDI_demo.bit  Demo bit file for the pass-through genlock 
					demo described in XAPP1076 Appendix B.

	SP605_passthru_genlock_SDI_demo.xise ISE project file for the pass-through genlock 
					demo described in XAPP1076 Appendix B.

	SP605_triple_rate_Chipscope.cpj Chipscope config file for viewing RX status and data.
					In conjunction with the RX and TX demo 
					of SP605_triple_rate_SDI_demo.bit as described in 
					XAPP1076 Appendix A.

	SP605_triple_rate_SDI_demo.bit	Demo bit file for the RX and TX demo described in 
					XAPP1076 Appendix A.

	SP605_triple_rate_SDI_demo.xise ISE project file for the RX and TX demo described in 
					XAPP1076 Appendix A.


ngc folder
	dru.ngc            Compiled NIDRU for data recovery in SD-SDI mode.
                           This file should be placed in the ISE project directory
	icon_2.ngc	   Chipscope control module
	ILA_67.ngc	   Integrated logic analyzer module with 67 synchronous inputs
	vio_71.ngc         Virtual IO module for Chipscope with 71 asyncrhronous inputs


Verilog folder    Verilog source files and top level .ucf constraints file:
	anc_rx.v                                
	autodetect.v 
	AVBCM.v
	AVBSPI.v 
	bshift10to10.v 
	cm_avb_control.v                         
	control.v
	dru.v
	edh_crc16.v                             
        edh_crc.v                               
        edh_errcnt.v                            
	edh_flags.v                             
	edh_loc.v                               
	edh_processor.v                         
	edh_rx.v                                
	edh_tx.v                                
	fly_field.v                             
	fly_fsm.v                               
	fly_horz.v                              
	fly_vert.v                              
	flywheel.v 
	genlock_control.v                             
	gtp_interface_pll.v
	gtp_interface_pll_gl.v
	hdsdi_crc2.v                            
	hdsdi_insert_crc.v                      
	hdsdi_insert_ln.v 
	hdsdi_rx_crc.v
	kcpsm3.v
	main_avb_control.v 
	maskencoder.v
	multi_sdi_decoder.v                      
	multi_sdi_encoder.v                     
        multi_sdi_framer.v                      
	multigenHD.v                            
	multigenHD_horz.v                       
	multigenHD_output.v                     
	multigenHD_vert.v 
	mux12_wide.v 
	Osc9.v
	rot20.v
	s6_sdi_rx_light_20b.v
	s6gtp_sdi_control.v
	s6gtp_sdi_drp_control.v
	s6gtp_sdi_rate_detect.v
	s6gtp_sdi_rx_reset.v                           
	sdi_bitrep_20b.v
        Si5324_fsel_lookup.v 
	SMPTE352_vpid_capture.v                       
	SMPTE352_vpid_insert.v 
	SMPTE425_B_demux2.v                 
	smpte_encoder.v 
	sp605_demo_20b.v                Top level Verilog file for RX, TX demo
	sp605_demo_FMC.ucf      ** include only one ucf file**  Top level constraints file for RX, TX demo
	sp605_passthru.ucf      ** include only one ucf file ** Top level constraints file for pass-through genlock demo
	sp605_passthru.v		Top level Verilog file for the pass-through genlock demo.
	sync_one_shot.v  
	triple_sdi_autodetect_ln.v 
	triple_sdi_rx_autorate.v          
	triple_sdi_tx_output_20b.v              
	triple_sdi_vpid_insert.v                
	trs_detect.v 
	usrclk_pll.v
	usrclk_pll_gl.v 
	video_decode.v                          
	vidgen_ntsc.v                           
	vidgen_pal.v                            
	wide_SRLC16E.v 
	wizv1_4_20b.v               GTP wrapper file          
	wizv1_4_20b_tile.v	    GTP tile instance

VHDL folder    VHDL source files and packages:
	anc_edh_pkg.vhd
	anc_rx.vhd                                
	autodetect.vhd 
	AVBSPI.vhd 
	bshift10to10.vhd                          
	control.vhd
	dru.vhd
	edh_crc16.vhd                             
        edh_crc.vhd                               
        edh_errcnt.vhd                            
	edh_flags.vhd                             
	edh_loc.vhd                               
	edh_processor.vhd                         
	edh_rx.vhd                                
	edh_tx.vhd                                
	fly_field.vhd                             
	fly_fsm.vhd                               
	fly_horz.vhd                              
	fly_vert.vhd                              
	flywheel.vhd                              
	gtp_interface_pll.vhd
	hdsdi_crc2.vhd                            
	hdsdi_insert_crc.vhd                      
	hdsdi_insert_ln.vhd 
	hdsdi_pkg.vhd
	hdsdi_rx_crc.vhd
	kcpsm3.vhd
	main_avb_control.vhd 
	maskencoder.vhd
	multi_sdi_decoder.vhd                      
	multi_sdi_encoder.vhd                     
        multi_sdi_framer.vhd                      
	multigenHD.vhd                            
	multigenHD_horz.vhd                       
	multigenHD_pkg.vhd
	multigenHD_output.vhd                     
	multigenHD_vert.vhd 
	mux12_wide.vhd 
	Osc9.vhd
	rot20.vhd
	s6_sdi_rx_light_20b.vhd
	s6gtp_sdi_control.vhd
	s6gtp_sdi_drp_control.vhd
	s6gtp_sdi_rate_detect.vhd
	s6gtp_sdi_rx_reset.vhd                           
	sdi_bitrep_20b.vhd
        Si5324_fsel_lookup.vhd 
	SMPTE352_vpid_capture.vhd                       
	SMPTE352_vpid_insert.vhd 
	SMPTE425_B_demux2.vhd                 
	smpte_encoder.vhd 
	sp605_demo_20b.vhd              Top level VHDL file
	sync_one_shot.vhd  
	triple_sdi_autodetect_ln.vhd 
	triple_sdi_rx_autorate.vhd          
	triple_sdi_tx_output_20b.vhd              
	triple_sdi_vpid_insert.vhd                
	trs_detect.vhd 
	usrclk_pll.vhd 
	video_decode.vhd                          
	vidgen_ntsc.vhd                           
	vidgen_pal.vhd                            
	wide_SRLC16E.vhd 
	wizv1_4_20b.vhd             GTP wrapper file          
	wizv1_4_20b_tile.vhd	    GTP tile instance

--------------------------------------------------------------------------------

