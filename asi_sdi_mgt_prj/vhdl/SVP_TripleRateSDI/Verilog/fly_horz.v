//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: fly_horz.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:37:08-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: fly_horz.v,rcs $
// Revision 1.1  2004-12-15 11:37:08-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module implements the horizontal logic for the video flywheel.

The module contains the horizontal counter. This counter keeps track of the
current horizontal position of the video. The module also generates the H 
signal. The H signal is asserted during the inactive portion of each scan line.

This module has the following inputs:

clk: clock input

rst: asynchronous reset

ce: clock enable

clr_hcnt: When this input is asserted, the horizontal counter is cleared.

resync_hcnt: When this input is asserted, the horizontal counter is reloaded
with the position of the EAV symbol. This happens during synchronous switches.

std: The video standard input code.

The module generates the following outputs:

hcnt: This is the value of the horizontal counter and indicates the current
horizontal positon of the video.

eav_next: Asserted the clock cycle before it is time for the flywheel to
generate the first word of an EAV symbol.

sav_next: Asserted the clock cycle before it is time for the flywheel to 
generate the first word of an SAV symbol.

h: This is the horizontal blanking bit.

trs_word: A 2-bit code indicating which word of the TRS symbol should be
generated by the flywheel.

fly_trs: Asserted during the first word of a flywheel generated TRS symbol.

fly_eav: Asserted during the XYZ word of a flywheel generated EAV symbol.

fly_sav: Asserted during the XYZ word of a flywheel generated SAV symbol.
*/

`timescale 1ns / 1 ns

module  fly_horz (
    // inputs
    clk,            // clock input
    rst,            // async reset input
    ce,             // clock enable
    clr_hcnt,       // clears the horizontal counter
    resync_hcnt,    // resynchronized horizontal counter during sync switch
    std,            // indicates current video standard
 
    // outputs
    hcnt,           // horizontal count
    eav_next,       // asserted when next word is first word of EAV symbol
    sav_next,       // asserted when next word is first word of SAV symbol
    h,              // horizontal blanking bit
    trs_word,       // indicates which word of the TRS symbol is being generated
    fly_trs,        // asserted during first word of a flywheel generated TRS
    fly_eav,        // asserted during xyz word of a flywheel generated EAV
    fly_sav         // asserted during xyz word of a flywheel generated SAV
);

//-----------------------------------------------------------------------------
// Parameter definitions
//

//
// This group of parameters defines the bit widths of various fields in the
// module. 
//
parameter HCNT_WIDTH    = 12;                   // Width of hcnt
parameter HCNT_MSB      = HCNT_WIDTH - 1;       // MS bit # of hcnt

//
// This group of parameters defines the starting position of the EAV symbol
// for the various supported video standards.
//
parameter EAV_LOC_NTSC_422          = 1440;
parameter EAV_LOC_NTSC_COMPOSITE    = 790;
parameter EAV_LOC_NTSC_422_WIDE     = 1920;
parameter EAV_LOC_NTSC_4444         = 2880;
parameter EAV_LOC_PAL_422           = 1440;
parameter EAV_LOC_PAL_COMPOSITE     = 972;
parameter EAV_LOC_PAL_422_WIDE      = 1920;
parameter EAV_LOC_PAL_4444          = 2880;

//
// This group of parameters defines the starting position of the SAV symbol
// for the various supported video standards.
//
parameter SAV_LOC_NTSC_422          = 1712;
parameter SAV_LOC_NTSC_COMPOSITE    = 790;
parameter SAV_LOC_NTSC_422_WIDE     = 2284;
parameter SAV_LOC_NTSC_4444         = 3428;
parameter SAV_LOC_PAL_422           = 1724;
parameter SAV_LOC_PAL_COMPOSITE     = 972;
parameter SAV_LOC_PAL_422_WIDE      = 2300;
parameter SAV_LOC_PAL_4444          = 3452;

//
// This group of parameters defines the encoding for the video standards output
// code.
//
parameter [2:0]
    NTSC_422        = 3'b000,
    NTSC_INVALID    = 3'b001,
    NTSC_422_WIDE   = 3'b010,
    NTSC_4444       = 3'b011,
    PAL_422         = 3'b100,
    PAL_INVALID     = 3'b101,
    PAL_422_WIDE    = 3'b110,
    PAL_4444        = 3'b111;

//-----------------------------------------------------------------------------
// Signal definitions
//

// IO definitions
input                   clk;
input                   rst;
input                   ce;
input                   clr_hcnt;
input                   resync_hcnt;
input   [2:0]           std;
output  [HCNT_MSB:0]    hcnt;
output                  eav_next;
output                  sav_next;                   
output                  h;
output  [1:0]           trs_word;
output                  fly_trs;
output                  fly_eav;
output                  fly_sav;

reg                     h;
reg     [1:0]           trs_word;       

// internal signals
reg     [HCNT_MSB:0]    hcount;         // horizontal counter
wire                    trs_next;       // TRS symbol starts on next count
reg                     trs;            // internal version of fly_trs signal
reg                     fly_xyz;        // asserted during flywheel generated XYZ word
reg     [HCNT_MSB:0]    eav_loc;        // EAV location
reg     [HCNT_MSB:0]    sav_loc;        // SAV location
reg     [HCNT_MSB:0]    resync_val;     // value to load on resync_hcnt

//
// hcount: horizontal counter
//
// The horizontal counter increments every clock cycle to keep track of the
// current horizontal position. If clr_hcnt is asserted by the FSM, hcnt is
// reloaded with a value of 1. A value of 1 is used because of the latency
// involved in detected the TRS symbol and deciding whether to clear hcnt or
// not. If resync_hcnt is asserted, the horizontal coutner is loaded with
// resync_val, a value derived from the EAV position. This happens during
// synchronous switches. 
//
always @ (posedge clk or posedge rst)
    if (rst)
        hcount <= 1;
    else if (ce)
        begin
            if (resync_hcnt)
                hcount <= resync_val;
            else if (clr_hcnt)
                hcount <= 1;
            else if (fly_sav)
                hcount <= 0;
            else
                hcount <= hcount + 1;
        end

//
// TRS word counter
//
// The TRS word counter is used to count out the words of a TRS symbol. A
// TRS symbol for component video is four words long.
//
// During the TRS symbol the trs signal is asserted. During the XYZ word of
// a component video signal fly_xyz is asserted and one of fly_sav or fly_eav.
//
always @ (posedge clk or posedge rst)
    if (rst)
        trs_word <= 0;
    else if (ce)
        begin
            if (trs_next)
                trs_word <= 0;
            else
                trs_word <= trs_word + 1;
        end

always @ (posedge clk or posedge rst)
    if (rst)
        trs <= 1'b0;
    else if (ce)
        begin
            if (clr_hcnt | fly_xyz | resync_hcnt)
                trs <= 1'b0;
            else if (trs_next)
                trs <= 1'b1;
        end
        
always @ (posedge clk or posedge rst)
    if (rst)
        fly_xyz <= 1'b0;
    else if (ce)
        begin
            if (clr_hcnt)
                fly_xyz <= 1'b0;
            else if (trs && (trs_word == 2'b10))
                fly_xyz <= 1'b1;
            else
                fly_xyz <= 1'b0;
        end
        
assign fly_eav = fly_xyz & h;
assign fly_sav = fly_xyz & ~h;

//
// TRS location detection
//
// This block of code generates the eav_next and sav_next signals. These signals
// are asserted the state before the flywheel will generate the first word of
// the EAV or SAV TRS symbols.
//
always @ (std)
    case (std)
        NTSC_422:
            begin
                eav_loc = EAV_LOC_NTSC_422 - 1;
                sav_loc = SAV_LOC_NTSC_422 - 1;
                resync_val = EAV_LOC_NTSC_422 + 2;
            end

        NTSC_422_WIDE:
            begin
                eav_loc = EAV_LOC_NTSC_422_WIDE - 1;
                sav_loc = SAV_LOC_NTSC_422_WIDE - 1;
                resync_val = EAV_LOC_NTSC_422_WIDE + 2;
            end

        NTSC_4444:
            begin
                eav_loc = EAV_LOC_NTSC_4444 - 1;
                sav_loc = SAV_LOC_NTSC_4444 - 1;
                resync_val = EAV_LOC_NTSC_4444 + 2;
            end

        PAL_422:
            begin
                eav_loc = EAV_LOC_PAL_422 - 1;
                sav_loc = SAV_LOC_PAL_422 - 1;
                resync_val = EAV_LOC_PAL_422 + 2;
            end

        PAL_422_WIDE:
            begin
                eav_loc = EAV_LOC_PAL_422_WIDE - 1;
                sav_loc = SAV_LOC_PAL_422_WIDE - 1;
                resync_val = EAV_LOC_PAL_422_WIDE + 2;
            end

        PAL_4444:
            begin
                eav_loc = EAV_LOC_PAL_4444 - 1;
                sav_loc = SAV_LOC_PAL_4444 - 1;
                resync_val = EAV_LOC_PAL_4444 + 2;
            end

        default:
            begin
                eav_loc = EAV_LOC_NTSC_422 - 1;
                sav_loc = SAV_LOC_NTSC_422 - 1;
                resync_val = EAV_LOC_NTSC_422 + 2;
            end

    endcase

assign eav_next = (hcount == eav_loc);
assign sav_next = (hcount == sav_loc);
assign trs_next = eav_next | sav_next;

//
// h
//
// This logic generates the H bit for the TRS XYZ word. The H bit becomes
// asserted at the start of EAV and is negated at the start of SAV. Note that
// the h_blank output from the flywheel module is similar to the H bit, but 
// remains asserted until after the last word of the SAV.
//
always @ (posedge clk or posedge rst)
    if (rst)
        h <= 1'b0;
    else if (ce)
        begin
            if (eav_next | resync_hcnt)
                h <= 1'b1;
            else if (sav_next| clr_hcnt)
                h <= 1'b0;
        end

//
// output assignments
//
assign fly_trs = trs;
assign hcnt = hcount;

endmodule