///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: rot20.v
// /___/   /\     Timestamp: October 20 2008
// \   \  /  \
//  \___\/\___\
//
///////////////////////////////////////////////////////////////////////////////
//Design Name: Non integer DRU
//Purpose: rotator for the Barrel Shifter.
//Authors: Paolo Novellini, Giovanni Guasti. 
///////////////////////////////////////////////////////////////////////////////

module rot20 (CLK, RST, HIN, HOUT, P);
    input          CLK;       
    input          RST;       
    input   [19:0] HIN;        
    output  [19:0] HOUT;     
    input   [4:0]  P;     

    wire [19:0] a;
    wire [19:0] b;
    wire [19:0] c;
    wire [19:0] d;
    wire [19:0] e;
    reg  [19:0] temp;
    
    assign a =  (P[0] == 1'b0) ?  HIN[19:0] : {HIN[18:0], HIN[19]};       
    assign b =  (P[1] == 1'b0) ?  a[19:0]   : {a[17:0], a[19:18]};       
    assign c =  (P[2] == 1'b0) ?  b[19:0]   : {b[15:0], b[19:16]};       
    assign d =  (P[3] == 1'b0) ?  c[19:0]   : {c[11:0], c[19:12]};       
    assign e =  (P[4] == 1'b0) ?  d[19:0]   : {d[3:0],  d[19:4]};       

    always @(posedge CLK or negedge RST)
    if (RST == 1'b0)
        temp <= 20'b00000000000000000000;
    else
        temp <= e;
        
    assign HOUT = temp;

//------------------------------------------------------------------------------
//-- for Elisa
//------------------------------------------------------------------------------

endmodule

