//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: edh_tx.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:31:15-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: edh_tx.v,rcs $
// Revision 1.1  2004-12-15 11:31:15-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This packet generates a new EDH packet and inserts it into the output video
stream.

The module is controlled by a finite state machine. The FSM waits for the
edh_next signal to be asserted by the edh_loc module. This signal indicates
that the next word is beginning of the area where an EDH packet should be
inserted.

The FSM then generates all the words of the EDH packet, assembling the payload
of the packet from the CRC and error flag inputs. The three sets of error flags
enter the module sequentially on the flags_in port. The module generates three
outputs (ap_flag_word, ff_flag_word, and anc_flag_word) to indicate which flag
set it needs on the flags_in port.

The module generates an output signal, edh_packet, that is asserted during all
the entire time that a generated EDH packet is being inserted into the video
stream. This signal is used by various other modules to determine when an EDH
packet has been sent.
*/

`timescale 1ns / 1 ns

module  edh_tx (
    // inputs
    clk,            // clock input
    ce,             // clock enable
    rst,            // async reset input
    vid_in,         // input video data
    edh_next,       // asserted when next word begins generated EDH packet
    edh_missing,    // received EDH packet is missing
    ap_crc_valid,   // active picture CRC valid
    ap_crc,         // active picture CRC
    ff_crc_valid,   // full field CRC valid
    ff_crc,         // full field CRC
    flags_in,       // bus that carries AP, FF, and ANC flags

    // outputs
    ap_flag_word,   // asserted during AP flag word in EDH packet
    ff_flag_word,   // asserted during FF flag word in EDH packet
    anc_flag_word,  // asserted during ANC flag word in EDH packet
    edh_packet,     // asserted during all words of EDH packet
    edh_vid         // generated EDH packet data
);


//-----------------------------------------------------------------------------
// Parameter definitions
//      

//
// This group of parameters defines the fixed values of some of the words in
// the EDH packet.
//
parameter EDH_ADF1          = 10'h000;
parameter EDH_ADF2          = 10'h3ff;
parameter EDH_ADF3          = 10'h3ff;
parameter EDH_DID           = 10'h1f4;
parameter EDH_DBN           = 10'h200;
parameter EDH_DC            = 10'h110;
parameter EDH_RSVD          = 10'h200;

//
// This group of parameters defines the states of the EDH generator state
// machine.
//
parameter STATE_WIDTH   = 5;
parameter STATE_MSB     = STATE_WIDTH - 1;

parameter [STATE_WIDTH-1:0]
    S_WAIT   = 0,
    S_ADF1   = 1,
    S_ADF2   = 2,
    S_ADF3   = 3,
    S_DID    = 4,
    S_DBN    = 5,
    S_DC     = 6,
    S_AP1    = 7,
    S_AP2    = 8,
    S_AP3    = 9,
    S_FF1    = 10,
    S_FF2    = 11,
    S_FF3    = 12,
    S_ANCFLG = 13,
    S_APFLG  = 14,
    S_FFFLG  = 15,
    S_RSV1   = 16,
    S_RSV2   = 17,
    S_RSV3   = 18,
    S_RSV4   = 19,
    S_RSV5   = 20,
    S_RSV6   = 21,
    S_RSV7   = 22,
    S_CHK    = 23;

//-----------------------------------------------------------------------------
// Port definitions
//
input                   clk;
input                   ce;
input                   rst;
input   [9:0]           vid_in;
input                   edh_next;
input                   edh_missing;
input                   ap_crc_valid;
input   [15:0]          ap_crc;
input                   ff_crc_valid;
input   [15:0]          ff_crc;
input   [4:0]           flags_in;
output                  ap_flag_word;
output                  ff_flag_word;
output                  anc_flag_word;
output                  edh_packet;
output  [9:0]           edh_vid;

reg                     ap_flag_word;
reg                     ff_flag_word;
reg                     anc_flag_word;
reg                     edh_packet;

//-----------------------------------------------------------------------------
// Signal definitions
//
reg     [STATE_MSB:0]   current_state;  // FSM current state register
reg     [STATE_MSB:0]   next_state;     // FSM next state value
wire                    parity;         // used to generate parity bit for EDH packet words
reg     [8:0]           checksum;       // used to calculated EDH packet CS word
reg                     clr_checksum;   // clears the checksum register
reg     [9:0]           vid;            // internal version of edh_vid output port
reg                     end_packet;     // FSM output that clears the edh_packet signal


//
// FSM: current_state register
//
// This code implements the current state register. It loads with the HSYNC1
// state on reset and the next_state value with each rising clock edge.
//
always @ (posedge clk or posedge rst)
    if (rst)
        current_state <= S_WAIT;
    else if (ce)
        current_state <= next_state;

//
// FSM: next_state logic
//
// This case statement generates the next_state value for the FSM based on
// the current_state and the various FSM inputs.
//
always @ (current_state or edh_next)
    case(current_state)
        S_WAIT:     if (edh_next)
                        next_state = S_ADF1;
                    else
                        next_state = S_WAIT;
                
        S_ADF1:     next_state = S_ADF2;

        S_ADF2:     next_state = S_ADF3;

        S_ADF3:     next_state = S_DID;

        S_DID:      next_state = S_DBN;

        S_DBN:      next_state = S_DC;

        S_DC:       next_state = S_AP1;

        S_AP1:      next_state = S_AP2;

        S_AP2:      next_state = S_AP3;

        S_AP3:      next_state = S_FF1;

        S_FF1:      next_state = S_FF2;

        S_FF2:      next_state = S_FF3;

        S_FF3:      next_state = S_ANCFLG;

        S_ANCFLG:   next_state = S_APFLG;

        S_APFLG:    next_state = S_FFFLG;
                    
        S_FFFLG:    next_state = S_RSV1;

        S_RSV1:     next_state = S_RSV2;

        S_RSV2:     next_state = S_RSV3;

        S_RSV3:     next_state = S_RSV4;

        S_RSV4:     next_state = S_RSV5;

        S_RSV5:     next_state = S_RSV6;

        S_RSV6:     next_state = S_RSV7;

        S_RSV7:     next_state = S_CHK;

        S_CHK:      next_state = S_WAIT;

        default: next_state = S_WAIT;

    endcase
        
//
// FSM: outputs
//
// This block decodes the current state to generate the various outputs of the
// FSM.
//
always @ (current_state or parity or ap_crc or ff_crc or ap_crc_valid or
          ff_crc_valid or flags_in or checksum or vid_in)
begin
    // Unless specifically assigned in the case statement, all FSM outputs
    // default to the values below.
    vid             = vid_in;
    clr_checksum    = 1'b0;
    end_packet      = 1'b0;
    ap_flag_word    = 1'b0;
    ff_flag_word    = 1'b0;
    anc_flag_word   = 1'b0;
                                
    case(current_state)     
        S_ADF1:     vid = EDH_ADF1;

        S_ADF2:     vid = EDH_ADF2;

        S_ADF3:     begin
                        vid = EDH_ADF3;
                        clr_checksum = 1'b1;
                    end

        S_DID:      vid = EDH_DID;

        S_DBN:      vid = EDH_DBN;

        S_DC:       vid = EDH_DC;

        S_AP1:      vid = {~parity, parity, ap_crc[5:0], 2'b00};

        S_AP2:      vid = {~parity, parity, ap_crc[11:6], 2'b00};

        S_AP3:      vid = {~parity, parity, ap_crc_valid, 1'b0, 
                            ap_crc[15:12], 2'b00};

        S_FF1:      vid = {~parity, parity, ff_crc[5:0], 2'b00};

        S_FF2:      vid = {~parity, parity, ff_crc[11:6], 2'b00};

        S_FF3:      vid = {~parity, parity, ff_crc_valid, 1'b0, 
                            ff_crc[15:12], 2'b00};

        S_ANCFLG:   begin
                        vid = {~parity, parity, 1'b0, flags_in, 2'b00};
                        anc_flag_word = 1'b1;
                    end

        S_APFLG:    begin
                        vid = {~parity, parity, 1'b0, flags_in, 2'b00};
                        ap_flag_word = 1'b1;
                    end

        S_FFFLG:    begin
                        vid = {~parity, parity, 1'b0, flags_in, 2'b00};
                        ff_flag_word = 1'b1;
                    end

        S_RSV1:     vid = EDH_RSVD;

        S_RSV2:     vid = EDH_RSVD;

        S_RSV3:     vid = EDH_RSVD;

        S_RSV4:     vid = EDH_RSVD;

        S_RSV5:     vid = EDH_RSVD;

        S_RSV6:     vid = EDH_RSVD;

        S_RSV7:     vid = EDH_RSVD;

        S_CHK:      begin
                        vid = {~checksum[8], checksum};
                        end_packet = 1'b1;
                    end
    endcase
end

//
// parity bit generation
//
// This code calculates the parity of bits 7:0 of the video word. The parity
// bit is inserted into bit 8 of parity protected words of the EDH packet. The
// complement of the parity bit is inserted into bit 9 of those same words.
//
assign parity = vid[7] ^ vid[6] ^ vid[5] ^ vid[4] ^
                vid[3] ^ vid[2] ^ vid[1] ^ vid[0];


//
// checksum calculator
//
// This code generates a checksum for the EDH packet. The checksum is cleared
// to zero prior to beginning the checksum calculation by the FSM asserting the
// clr_checksum signal. The vid_in word is added to the current checksum when
// the FSM asserts the do_checksum signal. The checksum is a 9-bit value and
// is computed by summing all but the MSB of the vid_in word with the current
// checksum value and ignoring any carry bits.
//
always @ (posedge clk or posedge rst)
    if (rst)
        checksum <= 0;
    else
        if (ce)
            begin
                if (clr_checksum)
                    checksum <= 0;
                else 
                    checksum <= checksum + vid[8:0];
            end

//
// edh_packet signal
//
// The edh_packet signal becomes asserted at the beginning of an EDH packet
// and remains asserted through the last word of the EDH packet.
//
always @ (posedge clk or posedge rst)
    if (rst)
        edh_packet <= 1'b0;
    else
        if (ce)
            begin
                if (edh_next)
                    edh_packet <= 1'b1;
                else if (end_packet)
                    edh_packet <= 1'b0;
            end

//
// output assignments
//
assign edh_vid = vid;

endmodule