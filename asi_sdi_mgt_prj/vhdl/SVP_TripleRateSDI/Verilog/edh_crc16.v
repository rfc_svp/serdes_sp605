//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: edh_crc16.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:25:30-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: edh_crc16.v,rcs $
// Revision 1.1  2004-12-15 11:25:30-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 

/*
This module does a 16-bit CRC calculation.

The calculation is a standard CRC16 calculation with a polynomial of x^16 + x^12
+ x^5 + 1. The function considers the LSB of the video data as the first bit
shifted into the CRC generator, although the implementation given here is a
fully parallel CRC, calculating all 16 CRC bits from the 10-bit video data in
one clock cycle.  

The assignment statements have all be optimized to use 4-input XOR gates
wherever possible to fit efficiently in the Xilinx FPGA architecture.

There are two input ports: c and d. The 16-bit c port must be connected to the
CRC "accumulation" register hold the last calculated CRC value. The 10-bit d
port is connected to the video stream.

The output port, crc, must be connected to the input of CRC "accumulation"
register.
*/

`timescale 1ns / 1 ns

module  edh_crc16 (
    c,      // current CRC value
    d,      // input data word
    crc     // new calculated CRC value
);


//-----------------------------------------------------------------------------
// Port definitions
//
input   [15:0]          c;
input   [9:0]           d;
output  [15:0]          crc;

//-----------------------------------------------------------------------------
// Signal definitions
//
wire    t1;         // intermediate product term used several times


assign t1 = d[4] ^ c[4] ^ d[0] ^ c[0];

assign crc[0]  = c[10] ^ crc[12];
assign crc[1]  = c[11] ^ d[0] ^ c[0] ^ crc[13];
assign crc[2]  = c[12] ^ d[1] ^ c[1] ^ crc[14];
assign crc[3]  = c[13] ^ d[2] ^ c[2] ^ crc[15];
assign crc[4]  = c[14] ^ d[3] ^ c[3];
assign crc[5]  = c[15] ^ t1;
assign crc[6]  = d[0] ^ c[0] ^ crc[11];
assign crc[7]  = d[1] ^ c[1] ^ crc[12];
assign crc[8]  = d[2] ^ c[2] ^ crc[13];
assign crc[9]  = d[3] ^ c[3] ^ crc[14];
assign crc[10] = t1 ^ crc[15];
assign crc[11] = d[5] ^ c[5] ^ d[1] ^ c[1];
assign crc[12] = d[6] ^ c[6] ^ d[2] ^ c[2];
assign crc[13] = d[7] ^ c[7] ^ d[3] ^ c[3];
assign crc[14] = d[8] ^ c[8] ^ t1;
assign crc[15] = d[9] ^ c[9] ^ crc[11];

endmodule
