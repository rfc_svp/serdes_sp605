//------------------------------------------------------------------------------ 
// Copyright (c) 2007 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: Reed P.Tidwell, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: sync_one_shot.v,v $
//  /   /        Date Last Modified:  $Date: 2010-02-23 16:45:58-07 $
// /___/   /\    Date Created: June 12, 2007
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: sync_one_shot.v,v $
// Revision 1.2  2010-02-23 16:45:58-07  reedt
// Feb. 2010 full release.
//
// Revision 1.1  2010-01-22 10:57:52-07  reedt
// Modifications for S6 architecture.
//
// Revision 1.0  2009-12-08 12:15:46-07  reedt
// Initial revision
//
// Revision 1.5  2007-10-25 15:27:45-06  reedt
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
//
//  This module creates a pulse that is one ce cycle long, synchronized to clk 
//  and occurs shortly after the rising edge of the "in" input. This can 
//  accommodate an extremely wide range of pulse widths on the input.  The 
//  minimum width of the pulse is just long enough to be recognized as a 
//  high level by the SR latch.  There is no maximum width; however the input 
//  must remain low for at least four clock cycles in order for the next rising 
//  edge to be recognized as a distinct pulse.
//
//------------------------------------------------------------------------------

`timescale 1 ps	/ 1ps

module sync_one_shot(	
  input  wire       clk,
  input  wire       rst,
  input  wire       ce,
  input  wire       in1,
  
  output wire       out1
  );

  reg               sr_out;   //output of SR Flip-Flop
  reg               sr_del1;  // SR FF delayed one enabled state
  reg               sr_del2;  // SR FF delayed two
  reg               sr_del3;  // SR FF delayed three
  wire              reg_reset;
  
  assign out1 = sr_del2 && !sr_del3;  // combinatorial output
  
// delay registers  
  always @ (posedge clk) begin
    if (rst) begin
      sr_del1 <= 0;
      sr_del2 <= 0;
      sr_del3 <= 0;
    end
    else if (ce) begin
      sr_del1 <= sr_out;
      sr_del2 <= sr_del1;
      sr_del3 <= sr_del2;
    end
  end //always
  
assign reg_reset = rst | sr_del3;
  
// define SR flip-flop  
  always @ (posedge in1 or posedge reg_reset)  
    if (reg_reset) 
      sr_out <= 0;
    else 
      sr_out <= 1;
  
endmodule
