///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: maskencoder.v
// /___/   /\     Timestamp: October 20 2008
// \   \  /  \
//  \___\/\___\
//
///////////////////////////////////////////////////////////////////////////////
//Design Name: Non integer DRU
//Purpose: mask encoder for the barrel shifter.
//Authors: Paolo Novellini, Giovanni Guasti. 
///////////////////////////////////////////////////////////////////////////////


module maskencoder(CLK, RST, SHIFT, MASK);
    input         CLK;
    input         RST;
    input  [4:0]  SHIFT;
    output [19:0] MASK;
    
    reg [19:0] maschera;

    assign MASK = maschera;

    always @(posedge CLK)
    begin
        case (SHIFT)
            5'b00000: maschera <= 20'b00000000001111111111;
            5'b00001: maschera <= 20'b00000000011111111110;
            5'b00010: maschera <= 20'b00000000111111111100;
            5'b00011: maschera <= 20'b00000001111111111000;
            5'b00100: maschera <= 20'b00000011111111110000;
			5'b00101: maschera <= 20'b00000111111111100000;
            5'b00110: maschera <= 20'b00001111111111000000;
            5'b00111: maschera <= 20'b00011111111110000000;
            5'b01000: maschera <= 20'b00111111111100000000;
            5'b01001: maschera <= 20'b01111111111000000000;
            5'b01010: maschera <= 20'b11111111110000000000;
            5'b01011: maschera <= 20'b11111111100000000001;
            5'b01100: maschera <= 20'b11111111000000000011;
            5'b01101: maschera <= 20'b11111110000000000111;
            5'b01110: maschera <= 20'b11111100000000001111;
            5'b01111: maschera <= 20'b11111000000000011111;
            5'b10000: maschera <= 20'b11110000000000111111;
            5'b10001: maschera <= 20'b11100000000001111111;
            5'b10010: maschera <= 20'b11000000000011111111;
            5'b10011: maschera <= 20'b10000000000111111111;
            default:  maschera <= 20'b00000000001111111111;
        endcase
    end

//          5'b00101: maschera <= 20'b00001111111111000000;

//----------------------------------------------------------------------------
// for Elisa
//----------------------------------------------------------------------------

endmodule