//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: video_decode.v,rcs $
//  /   /        Date Last Modified:  $Date: 2006-07-12 08:18:45-06 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: video_decode.v,rcs $
// Revision 1.2  2006-07-12 08:18:45-06  jsnow
// Small modification to support the new early V bit modification
// in the flywheel module.
//
// Revision 1.1  2004-12-15 11:32:09-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module instances and interconnects the three modules that make up the
digital video decoder: the TRS Detector, the Automatic Video Standard Detector,
and the Video Flywheel.

Together, these three modules will examine a video stream and determine the
format of the video from one of the six supported video standards. The flywheel
then synchronizes to the video stream to provide horizontal and vertical
counts so other modules can determine the location of data that occurs in
regular fixed locations, like the EDH packets. The flywheel will also 
regenerate TRS symbols and insert them into the video stream so that the video
contains valid TRS symbols even if the input video is noisy or stops 
altogether.

This module has the following inputs:

clk: clock input

ce: clock enable

rst: asynchronous reset input

vid_in: input video stream

reacquire: forces the autodetect unit to reacquire the video standard

en_sync_switch: enables support for synchronous video switching

en_trs_blank: enable TRS blanking

The module has the following outputs:

std: 3-bit video standard code from the autodetect module

std_locked: asserted when std is valid

trs: asserted during the four words when vid_out contains the TRS symbol words

vid_out: output video stream

field: indicates the current video field

v_blank: vertical blanking interval indicator

h_blank: horizontal blanking interval indicator

horz_count: the horizontal position of the word present on vid_out

vert_count: the vertical position of the word present on vid_out

sync_switch: asserted during the synchronous switching interval

locked: asserted when the flywheel is synchronized with the input video stream

eav_next: asserted the clock cycle before the first word of an EAV appears on
vid_out

sav_next: asserted the clock sycle before the first word of an SAV appears on 
vid_out

xyz_word: asserted when vid_out contains the XYZ word of a TRS symbol

anc_next: asserted the clock cycle before the first word of the ADF of an ANC
packet appears on vid_out

edh_next: asserted the clock cycle before the first word of the ADF of an EDH
packet appears on vid_out
*/

`timescale 1ns / 1 ns

module  video_decode (
    // inputs
    clk,            // clock input
    ce,             // clock enable
    rst,            // async reset input
    vid_in,         // input video
    reacquire,      // forces autodetect to reacquire the video standard
    en_sync_switch, // enables synchronous switching
    en_trs_blank,   // enables TRS blanking when asserted

    //outputs
    std,            // video standard code
    std_locked,     // autodetect ciruit is locked when this output is asserted
    trs,            // asserted during flywheel generated TRS symbol
    vid_out,        // TRS symbol data
    field,          // field indicator
    v_blank,        // vertical blanking bit
    h_blank,        // horizontal blanking bit
    horz_count,     // current horizontal count
    vert_count,     // current vertical count
    sync_switch,    // asserted on lines where synchronous switching is allowed
    locked,         // asserted when flywheel is synchronized to video
    eav_next,       // next word is first word of EAV
    sav_next,       // next word is first word of SAV
    xyz_word,       // current word is the XYZ word of a TRS
    anc_next,       // next word is first word of a received ANC
    edh_next        // next word is first word of a received EDH
);

//
// This group of parameters defines the bit widths of various fields in the
// module. 
//
parameter HCNT_WIDTH    = 12;                   // Width of hcnt
parameter VCNT_WIDTH    = 10;                   // Width of vcnt
 
parameter HCNT_MSB      = HCNT_WIDTH - 1;       // MS bit # of hcnt
parameter VCNT_MSB      = VCNT_WIDTH - 1;       // MS bit # of vcnt


//
// This group of parameters defines the encoding for the video standards output
// code.
//
parameter [2:0]
    NTSC_422        = 3'b000,
    NTSC_INVALID    = 3'b001,
    NTSC_422_WIDE   = 3'b010,
    NTSC_4444       = 3'b011,
    PAL_422         = 3'b100,
    PAL_INVALID     = 3'b101,
    PAL_422_WIDE    = 3'b110,
    PAL_4444        = 3'b111;

//-----------------------------------------------------------------------------
// Signal definitions
//

// IO definitions
input                   clk;
input                   ce;
input                   rst;
input   [9:0]           vid_in;
input                   reacquire;
input                   en_sync_switch;
input                   en_trs_blank;
output  [2:0]           std;
output                  std_locked;
output                  trs;
output  [9:0]           vid_out;
output                  field;
output                  v_blank;
output                  h_blank;
output  [HCNT_MSB:0]    horz_count;
output  [VCNT_MSB:0]    vert_count;
output                  sync_switch;
output                  locked;
output                  eav_next;
output                  sav_next;
output                  xyz_word;
output                  anc_next;
output                  edh_next;

// internal signals
wire                    td_xyz_err;         // trs_detect rx_xyz_err output
wire                    td_xyz_err_4444;    // trs_detect rx_xyz_err_4444 output
wire    [9:0]           td_vid;             // video stream from trs_detect
wire                    td_trs;             // trs_detect rx_trs output
wire                    td_xyz;             // trs_detect rx_xyz output
wire                    td_f;               // trs_detect rx_f output
wire                    td_v;               // trs_detect rx_v output
wire                    td_h;               // trs_detect rx_h output
wire                    td_anc;             // trs_detect rx_anc output
wire                    td_edh;             // trs_detect rx_edh output
wire                    td_eav;             // trs_detect rx_eav output
wire                    ad_s4444;           // autodetect s4444 output
wire                    ad_xyz_err;         // autodetect xyz_err output

//
// Instantiate the TRS detector module
//
trs_detect TD (
    .clk                (clk),
    .ce                 (ce),
    .rst                (rst),
    .vid_in             (vid_in),
    .vid_out            (td_vid),
    .rx_trs             (td_trs),
    .rx_eav             (td_eav),
    .rx_sav             (),
    .rx_f               (td_f),
    .rx_v               (td_v),
    .rx_h               (td_h),
    .rx_xyz             (td_xyz),
    .rx_xyz_err         (td_xyz_err),
    .rx_xyz_err_4444    (td_xyz_err_4444),
    .rx_anc             (td_anc),
    .rx_edh             (td_edh)
);

//
// Instantiate the video standard autodetect module
//
defparam AD.HCNT_WIDTH  = HCNT_WIDTH;
defparam AD.NTSC_422        = NTSC_422;
defparam AD.NTSC_INVALID    = NTSC_INVALID;
defparam AD.NTSC_422_WIDE   = NTSC_422_WIDE;
defparam AD.NTSC_4444       = NTSC_4444;
defparam AD.PAL_422     = PAL_422;
defparam AD.PAL_INVALID = PAL_INVALID;
defparam AD.PAL_422_WIDE    = PAL_422_WIDE;
defparam AD.PAL_4444        = PAL_4444;

autodetect AD (
    .clk                (clk),
    .ce                 (ce),
    .rst                (rst),
    .reacquire          (reacquire),
    .vid_in             (td_vid),
    .rx_trs             (td_trs),
    .rx_xyz             (td_xyz),
    .rx_xyz_err         (td_xyz_err),
    .rx_xyz_err_4444    (td_xyz_err_4444),
    .vid_std            (std),
    .locked             (std_locked),
    .xyz_err            (ad_xyz_err),
    .s4444              (ad_s4444)
);


//
// Instantiate the flywheel module
//

defparam FLY.VCNT_WIDTH     = VCNT_WIDTH;
defparam FLY.HCNT_WIDTH     = HCNT_WIDTH;
defparam FLY.NTSC_422       = NTSC_422;
defparam FLY.NTSC_INVALID   = NTSC_INVALID;
defparam FLY.NTSC_422_WIDE  = NTSC_422_WIDE;
defparam FLY.NTSC_4444      = NTSC_4444;
defparam FLY.PAL_422        = PAL_422;
defparam FLY.PAL_INVALID    = PAL_INVALID;
defparam FLY.PAL_422_WIDE   = PAL_422_WIDE;
defparam FLY.PAL_4444       = PAL_4444;

flywheel FLY (
    .clk            (clk),
    .ce             (ce),
    .rst            (rst),
    .rx_xyz_in      (td_xyz),
    .rx_trs_in      (td_trs),
    .rx_eav_first_in(td_eav),
    .rx_f_in        (td_f),
    .rx_v_in        (td_v),
    .rx_h_in        (td_h),
    .std_locked     (std_locked),
    .std_in         (std),
    .rx_xyz_err_in  (ad_xyz_err),
    .rx_vid_in      (td_vid),
    .rx_s4444_in    (ad_s4444),
    .rx_anc_in      (td_anc),
    .rx_edh_in      (td_edh),
    .en_sync_switch (en_sync_switch),
    .en_trs_blank   (en_trs_blank),
    .trs            (trs),
    .vid_out        (vid_out),
    .field          (field),
    .v_blank        (v_blank),
    .h_blank        (h_blank),
    .horz_count     (horz_count),
    .vert_count     (vert_count),
    .sync_switch    (sync_switch),
    .locked         (locked),
    .eav_next       (eav_next),
    .sav_next       (sav_next),
    .xyz_word       (xyz_word),
    .anc_next       (anc_next),
    .edh_next       (edh_next)
);
endmodule