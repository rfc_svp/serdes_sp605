//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: fly_vert.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:37:47-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: fly_vert.v,rcs $
// Revision 1.1  2004-12-15 11:37:47-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module implements the vertical functions of the video flywheel.

This module contains the vertical counter. This counter keeps track of the
current video scan line. The module also generates the V signal. This signal
is asserted during the vertical blanking interval of each field.

This module has the following inputs:

clk: clock input

rst: asynchronous reset input

ce: clock enable

ntsc: Asserted when the input video stream is NTSC.

ld_vcnt: This input causes the vertical counter to load the value of the first
line of the current field.

fsm_inc_vcnt: This input is asserted by the FSM to force the vertical counter
to increment during a failed synchronous switch.

eav_next: Asserted the clock cycle before the first word of a flywheel generated
EAV symbol.

clr_switch: Causes the switch_interval output to be negated.

rx_f: This signal carries the F bit from the input video stream during XYZ 
words.

f: This is the flywheel generated F bit.

fly_sav: Asserted during the XYZ word of a flywheel generated SAV.

fly_eav: Asserted during the XYZ word of a flywheel generated EAV.

rx_eav_first: Asserted during the first word of an EAV in the input video 
stream.

lock: Asserted when the flywheel is locked.

This module generates the following outputs:

vcnt: This is the value of the vertical counter indicating the current video
line number.

v: This is the vertical blanking bit asserted during the vertical blanking
interval.

sloppy_v: This signal is asserted on those lines where the V bit may fall early.

inc_f: Toggles the F bit when asserted.

switch_interval: Asserted when the current line contains the synchronous
switching interval.
*/

`timescale 1ns / 1 ns

module  fly_vert (
    // inputs
    clk,            // clock input
    rst,            // async reset input
    ce,             // clock enable input
    ntsc,           // 1 = NTSC, 0 = PAL
    ld_vcnt,        // causes vert counter to load
    fsm_inc_vcnt,   // forces vert counter to increment during failed sync switch
    eav_next,       // asserted when next word is first word of a flywheel EAV
    clr_switch,     // clears the switch_interval signal
    rx_f,           // received F bit
    f,              // flywheel generated field bit
    fly_sav,        // asserted during first word of flywheel generated SAV
    fly_eav,        // asserted during first word of flywheel generated EAV
    rx_eav_first,   // asserted during first word of received EAV
    lock,           // asserted when flywheel is locked

    // outputs
    vcnt,           // vertical counter
    v,              // vertical blanking bit indicator
    sloppy_v,       // asserted when FSM should ignore V bit in XYZ comparison
    inc_f,          // toggles the F bit when asserted
    switch_interval // asserted when current line is a sync switching line
);

//-----------------------------------------------------------------------------
// Parameter definitions
//

//
// This group of parameters defines the bit widths of various fields in the
// module. 
//
parameter VCNT_WIDTH    = 10;                   // Width of vcnt
parameter VCNT_MSB      = VCNT_WIDTH - 1;       // MS bit # of vcnt

//
// This group of parameters defines the synchronous switching interval lines.
//
parameter NTSC_FLD1_SWITCH          = 10;
parameter NTSC_FLD2_SWITCH          = 273;
parameter PAL_FLD1_SWITCH           = 6;
parameter PAL_FLD2_SWITCH           = 319;
    
//
// This group of parameters defines the ending positions of the fields for
// NTSC and PAL.
//
parameter NTSC_FLD1_END             = 265;
parameter NTSC_FLD2_END             = 3;
parameter PAL_FLD1_END              = 312;
parameter PAL_FLD2_END              = 625;
parameter NTSC_V_TOTAL              = 525;
parameter PAL_V_TOTAL               = 625;
    
//
// This group of parameters defines the starting and ending active portions of
// of the fields.
//
parameter NTSC_FLD1_ACT_START       = 20;
parameter NTSC_FLD1_ACT_END         = 263;
parameter NTSC_FLD2_ACT_START       = 283;
parameter NTSC_FLD2_ACT_END         = 525;
parameter PAL_FLD1_ACT_START        = 23;
parameter PAL_FLD1_ACT_END          = 310;
parameter PAL_FLD2_ACT_START        = 336;
parameter PAL_FLD2_ACT_END          = 623;
         
//
// This group of parameters defines the starting lines on which it is possible
// for the V bit to change early. This is due to previous versions of the
// specifications that allowed for an early transition from 1 to 0 on the V
// bit. This only occurs in the NTSC specifications. The period of ambiguity
// on the V bit ends with the first active video line of each field as
// defined above.
//
parameter SLOPPY_V_START_FLD1       = 10;
parameter SLOPPY_V_START_FLD2       = 273;


//-----------------------------------------------------------------------------
// Signal definitions
//

// IO definitions
input                   clk;
input                   rst;
input                   ce;
input                   ntsc;
input                   ld_vcnt;
input                   fsm_inc_vcnt;
input                   eav_next;
input                   clr_switch;
input                   rx_f;
input                   f;
input                   fly_sav;
input                   fly_eav; 
input                   rx_eav_first;
input                   lock;
output  [VCNT_MSB:0]    vcnt;
output                  v;
output                  sloppy_v;
output                  inc_f;
output                  switch_interval;

reg                     v;
reg                     switch_interval;
reg                     sloppy_v;

// internal signals
reg     [VCNT_MSB:0]    vcount;         // vertical counter
wire                    clr_vcnt;       // clears the vertical counter
reg     [VCNT_MSB:0]    new_vcnt;       // new value to load into vcount                
reg     [VCNT_MSB:0]    fld1_switch;    // synchronous switching line for field 1
reg     [VCNT_MSB:0]    fld2_switch;    // synchronous switching line for field 2
wire    [VCNT_MSB:0]    fld_switch;     // synchronous switching line for current field
wire                    switch_line;    // asserted when vcnt == fld_switch
wire    [VCNT_MSB:0]    v_total;        // total vertical lines for this video standard
reg     [VCNT_MSB:0]    fld1_act_start; // starting line of active video in field 1
reg     [VCNT_MSB:0]    fld1_act_end;   // ending line of active video in field 1
reg     [VCNT_MSB:0]    fld2_act_start; // starting line of active video in field 2
reg     [VCNT_MSB:0]    fld2_act_end;   // ending line of active video in field 2
wire    [VCNT_MSB:0]    fld_act_start;  // starting line of active video in current field
wire    [VCNT_MSB:0]    fld_act_end;    // ending line of active video in current field
wire                    act_start;      // result of comparing vcnt and fld_act_start
reg     [VCNT_MSB:0]    fld1_end;       // line count for end of field 1
reg     [VCNT_MSB:0]    fld2_end;       // line count for end of field 2
wire    [VCNT_MSB:0]    fld_end;        // line count for end of current field
wire    [VCNT_MSB:0]    sloppy_start;   // starting position of V bit ambiguity period

//
// vcnt: vertical counter
//
// The vertical counter increments once per line to keep track of the current
// vertical position. If clr_vcnt is asserted, vcnt is loaded with a value of
// 1. If ld_vcnt is asserted, the new_vcnt value is loaded into vcnt. If the
// state machine asserts the fsm_inc_vcnt signal indicating a synchronous
// switch event, then the vcnt must be forced to increment since the received
// EAV came before the flywheel's generated EAV, causing the hcnt to be updated
// to a position after the EAV and thus skipping the normal inc_vcnt signal
// that comes with the flywheel's EAV.
//
always @ (posedge clk or posedge rst)
    if (rst)
        vcount <= 1;
    else if (ce)
        begin
            if (ld_vcnt)
                vcount <= new_vcnt;
            else if (fsm_inc_vcnt | 
                     ((lock & switch_interval) ? rx_eav_first : eav_next))
                begin
                    if (clr_vcnt)
                        vcount <= 1;
                    else
                        vcount <= vcount + 1;
                end
        end

assign v_total = ntsc ? NTSC_V_TOTAL : PAL_V_TOTAL;
assign clr_vcnt = (vcount == v_total);
assign vcnt = vcount;

always @ (ntsc or rx_f)
    if (ntsc)
        begin
            if (rx_f)
                new_vcnt = NTSC_FLD1_END + 1;
            else
                new_vcnt = NTSC_FLD2_END + 1;
        end
    else
        begin
            if (rx_f)
                new_vcnt = PAL_FLD1_END + 1;
            else
                new_vcnt = 1;
        end


//
// synchronous switching line detector
//
// This code determines when the current line is a line during which
// it is permitted to switch between synchronous video sources. These sources
// may have a small amount of offset. The flywheel will immediately 
// resynchronize to the new signal on the synchronous switching lines without
// the usual flywheel induced delay.
//
always @ (ntsc)
    if (ntsc)
        begin
            fld1_switch <= NTSC_FLD1_SWITCH;
            fld2_switch <= NTSC_FLD2_SWITCH;
        end
    else
        begin
            fld1_switch <= PAL_FLD1_SWITCH;
            fld2_switch <= PAL_FLD2_SWITCH;
        end

assign fld_switch = f ? fld2_switch : fld1_switch;

assign switch_line = (vcount == fld_switch);

always @ (posedge clk or posedge rst)
    if (rst)
        switch_interval <= 1'b0;
    else if (ce)
        begin
            if (switch_interval ? clr_switch : fly_eav)
                switch_interval <= 1'b0;
            else if (fly_sav)
                switch_interval <= switch_line;
        end

//
// v
//
// This logic generates the V bit for the TRS XYZ word. The V bit is asserted
// in the TRS symbols of all lines in the vertical blanking interval. It is
// generated by comparing the vcnt starting and ending positions of the
// current field at the beginning of the EAV symbol. Whenever the state 
// machine reloads the field counter by asserted ld_f, the v flag should be
// set because the field counter is only reloaded in the vertical blanking
// interval.
//
always @ (ntsc)
    if (ntsc)
        begin
            fld1_act_start = NTSC_FLD1_ACT_START - 1;
            fld1_act_end   = NTSC_FLD1_ACT_END;
            fld2_act_start = NTSC_FLD2_ACT_START - 1;
            fld2_act_end   = NTSC_FLD2_ACT_END;
        end
    else
        begin
            fld1_act_start = PAL_FLD1_ACT_START - 1;
            fld1_act_end   = PAL_FLD1_ACT_END;
            fld2_act_start = PAL_FLD2_ACT_START - 1;
            fld2_act_end   = PAL_FLD2_ACT_END;
        end

assign fld_act_start = f ? fld2_act_start : fld1_act_start;
assign fld_act_end   = f ? fld2_act_end   : fld1_act_end;
assign act_start = vcnt == fld_act_start;

always @ (posedge clk or posedge rst)
    if (rst)
        v <= 1'b0;
    else if (ce)
        begin
            if (ld_vcnt)
                v <= 1'b1;
            else if (eav_next)
                begin
                    if (vcnt == fld_act_start)
                        v <= 1'b0;
                    else if (vcnt == fld_act_end)
                        v <= 1'b1;
                end
        end

//
// inc_f
//
// This logic determines when to toggle the F bit.
//
always @ (ntsc)
    if (ntsc)
        begin
            fld1_end = NTSC_FLD1_END;
            fld2_end = NTSC_FLD2_END;
        end
    else
        begin
            fld1_end = PAL_FLD1_END;
            fld2_end = PAL_FLD2_END;
        end

assign fld_end = f ? fld2_end : fld1_end;
assign inc_f = (vcnt == fld_end);

//
// sloppy_v
//
// This signal is asserted during the interval when the V bit should be
// ignored in XYZ comparisons due to ambiguity in earlier versions of the
// NTSC digital video specifications.
//
always @ (posedge clk or posedge rst)
    if (rst)
        sloppy_v <= 1'b0;
    else if (ce)
        begin
            if (ld_vcnt | ~ntsc)
                sloppy_v <= 1'b0;
            else
                begin
                    if (vcnt == sloppy_start)
                        sloppy_v <= 1'b1;
                    else if (eav_next & act_start)
                        sloppy_v <= 1'b0;
                end
        end

assign sloppy_start = f ? SLOPPY_V_START_FLD2 : SLOPPY_V_START_FLD1;

endmodule