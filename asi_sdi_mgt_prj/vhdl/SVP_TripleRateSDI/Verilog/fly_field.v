//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: fly_field.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:34:52-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: fly_field.v,rcs $
// Revision 1.1  2004-12-15 11:34:52-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module implements the field related functions for the video flywheel.
There are two main field related functions included in this module. The first
is the F bit. This bit indicates the field that is currently active. The other
function is the received field transition detector. This function determines
when the received video transition from one field to the next.

The inputs to this module are:

clk: clock input

rst: asynchronous reset input

ce: clock enable

ld_f: When this input is asserted, the F flip-flop is loaded with the 
current field value.

inc_f: When this input is asserted the F flip-flop is toggled.

eav_next: Must be asserted the clock cycle before the first word of an EAV 
symbol is processed by the flywheel.

rx_field: This is the F bit from the XYZ word of the input video stream. This
input is only valied when rx_xyz is asserted.

rx_xyz: Asserted when the flywheel is processing the XYZ word of a TRS symbol.

The outputs of this module are:

f: Current field bit

new_rx_field: Asserted for when a field transition is detected. This signal
will be asserted for the entire duration of the first line of a new field.
*/

`timescale 1ns / 1 ns

module  fly_field (
    // inputs
    clk,            // clock input
    rst,            // async reset input
    ce,             // clock enable
    ld_f,           // loads the F bit
    inc_f,          // toggles the F bit
    eav_next,       // asserted when next word is first word of EAV
    rx_field,       // F bit from received XYZ word
    rx_xyz,         // asserted during XYZ word of received TRS symbol

    // outputs
    f,              // field bit
    new_rx_field    // asserted when received field changes
);

//-----------------------------------------------------------------------------
// Signal definitions
//

// IO definitions
input                   clk;
input                   rst;
input                   ce;
input                   ld_f;
input                   inc_f;
input                   eav_next;
input                   rx_field;
input                   rx_xyz;
output                  f;
output                  new_rx_field;

reg                     f;

// internal signals
reg                     rx_f_now;   // holds F bit from most recent XYZ word
reg                     rx_f_prev;  // holds F bit from previous XYZ word

//
// field bit
//                                  
// The field bit keep track of the current field (even or odd). It loads from
// the rx_f_now value when ld_f is asserted during the time the flywheel is
// synchronizing with the incoming video. Otherwise, it toggles at the
// beginning of each field.
//
always @ (posedge clk or posedge rst)
    if (rst)
        f <= 0;
    else if (ce)
        begin
            if (ld_f) 
                f <= rx_f_now;
            else if (eav_next & inc_f)
                f <= ~f;
        end

                    

//
// received video new field detection
//
// The rx_f_now register holds the field value for the current field.
// The rx_f_prev register holds the field value from the previous field. If
// there is a difference between these two registers, the new_rx_field signal
// is asserted. This informs the FSM that the received video has transitioned
// from one field to the next.
//
always @ (posedge clk or posedge rst)
    if (rst)
        begin
            rx_f_now  <= 1'b0;
            rx_f_prev <= 1'b0;
        end
    else if (ce)
        if (rx_xyz)
            begin
                rx_f_now  <= rx_field;
                rx_f_prev <= rx_f_now;
            end

assign new_rx_field = rx_f_now ^ rx_f_prev;

endmodule