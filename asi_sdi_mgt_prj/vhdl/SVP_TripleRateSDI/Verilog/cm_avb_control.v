//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow
//  \   \        Filename: $RCSfile: cm_avb_control.v,v $
//  /   /        Date Last Modified:  $Date: 2010-04-08 15:17:14-06 $
// /___/   /\    Date Created: July 14, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: cm_avb_control.v,v $
// Revision 1.0  2010-04-08 15:17:14-06  reedt
// Initial revision
//
// Revision 1.2  2010-02-10 11:25:40-07  jsnow
// Added dynamic Si5324 bandwidth selection capability.
//
// Revision 1.1  2010-01-11 10:21:52-07  jsnow
// Expanded the Si5324 output frequency select ports to 4 bits.
//
// Revision 1.0  2009-11-19 14:55:46-07  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module for FMC carrier boards provides access to the control & status for 
the a clock module on the Xilinx AVB FMC card. One of these modules is required
for each clock module installed on the AVB FMC card.

*/

`timescale 1ns / 1 ns

module cm_avb_control 
( 
// Master clock
    input   wire            clk,                    // 27 MHz clock from the AVB FMC card
    input   wire            rst,
    input   wire            ga,                     // must be 0 for CML and 1 for CMH

// SPI signals
    output  wire            sck,                    // SPI SCK
    output  wire            mosi,                   // master-out slave-in serial data
    input   wire            miso,                   // master-in slave-out serial data
    output  wire            ss,                     // slave select -- asserted low
    
// Module identification
    output  reg [15:0]      module_type = 0,        // Clock module type
    output  reg [15:0]      module_rev = 0,         // Clock module revision
    output  reg             module_type_valid =1'b0,// 1 = module type & rev have been read
    output  reg             module_type_error =1'b0,// 1 = error reading module type & rev

// General control
    input   wire            clkin5_src_sel,         // Clock module CLKIN 5 source
                                                    // 0 = 27 MHz, 1 = from FMC connector
// GPIO direction signals
// These control the direction of signals between the FPGA on the AVB FMC card
// and the clock module. A value of 0 indicates an FPGA output to the clock
// module. A value of 1 indicates an input to the FPGA from the clock module.
//
    input   wire [7:0]      gpio_dir_0,             // GPIO signals [7:0]
    input   wire [7:0]      gpio_dir_1,             // GPIO signals [15:8]
    input   wire [7:0]      gpio_dir_2,             // GPIO signals [23:16]

// General purpose output values
// These control the of the GPIO signals when they are outputs from the FPGA
// on the AVB FMC card to the clock module.
    input   wire [7:0]      gp_out_value_0,         // GPIO signals [7:0]
    input   wire [7:0]      gp_out_value_1,         // GPIO signals [15:8]
    input   wire [7:0]      gp_out_value_2,         // GPIO signals [23:16]

// General purpose input values
// The ports reflect the values of the GPIO signals when they are inputs to the
// FPGA on the AVB FMC card from the clock clock module.
    output  reg  [7:0]      gp_in_value_0 = 0,      // GPIO signals [7:0]
    output  reg  [7:0]      gp_in_value_1 = 0,      // GPIO signals [15:8]
    output  reg  [7:0]      gp_in_value_2 = 0,      // GPIO signals [23_16]
    output  reg  [3:0]      gp_in = 0,              // GPIN signals [3:0]

// I2C bus register peek/poke control
// These ports provide peek/poke capability to devices connected to the
// I2C bus on the clock module. To write a value to a device register, set the
// the slave address, register address, and data to be written and then pulse
// i2c_reg_wr high for one cycle of the 27 MHz clock. The i2c_reg_rdy signal
// will go low on the rising edge of the clock when i2c_reg_wr is high and
// will stay low until the write is completed. To read a register, setup the
// slave address and register address then pulse i2c_reg_rd high for one cycle
// of the clock. Again, i2c_reg_rdy will go low until the read cycle is completed.
// When i2c_reg_rdy goes high, the data read from the register will be present
// on i2c_reg_dat_rd.
//
    input   wire [7:0]      i2c_slave_adr,          // I2C device slave address
    input   wire [7:0]      i2c_reg_adr,            // I2C device register address
    input   wire [7:0]      i2c_reg_dat_wr,         // Data to be written to device
    input   wire            i2c_reg_wr,             // Write request
    input   wire            i2c_reg_rd,             // Read request
    output  reg  [7:0]      i2c_reg_dat_rd = 0,     // Data read from I2C device
    output  reg             i2c_reg_rdy = 1'b0,     // 1 = peek/poke completed      
    output  reg             i2c_reg_error = 1'b0,   // 1 = NACK occurred during I2C peek/poke

// Si5324 module signals
//
// These ports are only valid if the Si5324 clock module is installed on the
// AVB FMC card. There are 3 identical sets of ports, one set for each of the
// three Si5324 parts on the clock module. The out_fsel and in_fsel ports
// set the predefined frequency synthesis options as follows:
//
//      Si5324_X_in_fsel[4:0] select the input frequency:
//          0x00: 480i (NTSC) HSYNC
//          0x01: 480p HSYNC
//          0x02: 576i (PAL) HSYNC
//          0x03: 576p HSYNC
//          0x04: 720p 24 Hz HSYNC
//          0x05: 720p 23.98 Hz HSYNC
//          0x06: 720p 25 Hz HSYNC
//          0x07: 720p 30 Hz HSYNC
//          0x08: 720p 29.97 Hz HSYNC
//          0x09: 720p 50 Hz HSYNC
//          0x0A: 720p 60 Hz HSYNC
//          0x0B: 720p 59.94 Hz HSYNC
//          0x0C: 1080i 50 Hz HSYNC
//          0x0D: 1080i 60 Hz HSYNC
//          0x0E: 1080i 59.94 Hz HSYNC
//          0x0F: 1080p 24 Hz HSYNC
//          0x10: 1080p 23.98 Hz HSYNC
//          0x11: 1080p 25 Hz HSYNC
//          0x12: 1080p 30 Hz HSYNC
//          0x13: 1080p 29.97 Hz HSYNC
//          0x14: 1080p 50 Hz HSYNC
//          0x15: 1080p 60 Hz HSYNC
//          0x16: 1080p 59.94 Hz HSYNC
//          0x17: 27 MHz
//          0x18: 74.25 MHz
//          0x19: 74.25/1.001 MHz
//          0x1A: 148.5 MHz
//          0x1B: 148.5/1.001 MHz
//
//      Si5324_out_fsel[3:0] select the output frequency:
//          0x0: 27 MHz
//          0x1: 74.25 MHz
//          0x2: 74.25/1.001 MHz
//          0x3: 148.5 MHz
//          0x4: 148.5/1.001 MHz
//          0x5: 24.576 MHz
//          0x6: 148.5/1.0005 MHz
//          0x7: Invalid
//          0x8: 297 MHz
//          0x9: 297/1.001 MHz
//
// Note that any HSYNC frequency can only be converted to 27 MHz. Choosing any
// output frequency except 27 MHz when the input selection is 0x00 through 0x16
// will result in an error. Any input frequency selected by 0x17 through 0x1B
// can be converted to any output frequency, with the exception that the
// 74.25/1.001 and 148.5/1.001 MHz input frequencies can't be converted to 
// 24.576 MHz.
//
// The Si5324_x_bw_sel port select the bandwidth of the Si5324. The selected
// bandwidth MUST be a legal value for the input/output frequency combinations.
//
    input   wire            Si5324_A_clkin_sel,     // Selects input clock, 0=CKIN1, 1=CKIN2
    input   wire [3:0]      Si5324_A_out_fsel,      // Selects the output frequency
    input   wire [4:0]      Si5324_A_in_fsel,       // Selects the input frequency
    input   wire [3:0]      Si5324_A_bw_sel,        // Selects the device bandwidth
    input   wire            Si5324_A_DHOLD,         // 1 puts device in digital hold mode
    output  reg             Si5324_A_FOS2 = 1'b0,   // 1=frequency offset alarm for CKIN2
    output  reg             Si5324_A_FOS1 = 1'b0,   // 1=frequency offset alarm for CKIN1
    output  reg             Si5324_A_LOL = 1'b0,    // 0=PLL locked, 1=PLL unlocked

    input   wire            Si5324_B_clkin_sel,     // Selects input clock, 0=CKIN1, 1=CKIN2
    input   wire [3:0]      Si5324_B_out_fsel,      // Selects the output frequency
    input   wire [4:0]      Si5324_B_in_fsel,       // Selects the input frequency
    input   wire [3:0]      Si5324_B_bw_sel,        // Selects the device bandwidth
    input   wire            Si5324_B_DHOLD,         // 1 puts device in digital hold mode
    output  reg             Si5324_B_FOS2 = 1'b0,   // 1=frequency offset alarm for CKIN2
    output  reg             Si5324_B_FOS1 = 1'b0,   // 1=frequency offset alarm for CKIN1
    output  reg             Si5324_B_LOL = 1'b0,    // 0=PLL locked, 1=PLL unlocked

    input   wire            Si5324_C_clkin_sel,     // Selects input clock, 0=CKIN1, 1=CKIN2
    input   wire [3:0]      Si5324_C_out_fsel,      // Selects the output frequency
    input   wire [4:0]      Si5324_C_in_fsel,       // Selects the input frequency
    input   wire [3:0]      Si5324_C_bw_sel,        // Selects the device bandwidth
    input   wire            Si5324_C_DHOLD,         // 1 puts device in digital hold mode
    output  reg             Si5324_C_FOS2 = 1'b0,   // 1=frequency offset alarm for CKIN2
    output  reg             Si5324_C_FOS1 = 1'b0,   // 1=frequency offset alarm for CKIN1
    output  reg             Si5324_C_LOL = 1'b0     // 0=PLL locked, 1=PLL unlocked
);

//
// Internal signal definitions
//
wire    [7:0]       port_id;            // PicoBlaze port ID output
wire                write_strobe;       // PicoBlaze write strobe
wire                read_strobe;        // PicoBlaze read strobe
wire    [7:0]       output_port;        // PicoBlaze output port
reg     [7:0]       input_port = 0;     // PicoBlaze input port
reg     [2:0]       spi_reg = 3'b100;   // [SS,SCK,MOSI] control bits from pBlaze
wire    [9:0]       address;
wire    [17:0]      instruction;

reg                 i2c_rd_req = 1'b0;
reg                 i2c_wr_req = 1'b0;

wire [7:0]          Si5324_A_fsel;
wire [7:0]          Si5324_B_fsel;
wire [7:0]          Si5324_C_fsel;

assign ss   = spi_reg[2];
assign sck  = spi_reg[1];
assign mosi = spi_reg[0];

avbcm CM_SPI_CODEROM (
    .address            (address),
    .instruction        (instruction),
    .clk                (clk));

//
// PicoBlaze processor
//
kcpsm3 CM_SPI_PICO (
    .address            (address),
    .instruction        (instruction),
    .port_id            (port_id),
    .write_strobe       (write_strobe),
    .out_port           (output_port),
    .read_strobe        (read_strobe),
    .in_port            (input_port),
    .interrupt          (1'b0),
    .interrupt_ack      (),
    .reset              (rst),
    .clk                (clk));

//
// PicoBlaze output port registers
//
// SPI control register
//
always @ (posedge clk)
    if (rst)
        spi_reg <= 3'b100;
    else if (write_strobe & ~port_id[7] & ~port_id[6])
        spi_reg <= output_port[2:0];

always @ (posedge clk)
    if (write_strobe & ~port_id[7] & port_id[6] & (port_id[2:0]  == 3'b000))
        module_type[15:8] <= output_port;

always @ (posedge clk)
    if (write_strobe & ~port_id[7] & port_id[6] & (port_id[2:0]  == 3'b001))
        module_type[7:0] <= output_port;

always @ (posedge clk)
    if (write_strobe & ~port_id[7] & port_id[6] & (port_id[2:0]  == 3'b010))
        module_rev[15:8] <= output_port;

always @ (posedge clk)
    if (write_strobe & ~port_id[7] & port_id[6] & (port_id[2:0]  == 3'b011))
        module_rev[7:0] <= output_port;

always @ (posedge clk)
    if (write_strobe & ~port_id[7] & port_id[6] & port_id[2])
    begin
        module_type_valid <= output_port[0];
        module_type_error <= output_port[1];
    end

always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'h0))
        gp_in <= output_port[3:0];

always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'h3))
        gp_in_value_0 <= output_port;

always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'h4))
        gp_in_value_1 <= output_port;

always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'h5))
        gp_in_value_2 <= output_port;

always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'h8))
        i2c_reg_dat_rd <= output_port;

always @ (posedge clk)
    if (i2c_reg_wr)
        i2c_wr_req <= 1'b1;
    else if (read_strobe & (port_id[3:0] == 4'h9))
        i2c_wr_req <= 1'b0;

always @ (posedge clk)
    if (i2c_reg_rd)
        i2c_rd_req <= 1'b1;
    else if (read_strobe & (port_id[3:0] == 4'h9))
        i2c_rd_req <= 1'b0;

always @ (posedge clk)
    if (i2c_reg_wr | i2c_reg_rd)
        i2c_reg_rdy <= 1'b0;
    else if (write_strobe & port_id[7] & (port_id[3:0] == 4'h7))
        i2c_reg_rdy <= 1'b1;

always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'h7))
        i2c_reg_error <= output_port[6];

always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'hA))
    begin
        Si5324_A_FOS2 <= output_port[2];
        Si5324_A_FOS1 <= output_port[1];
        Si5324_A_LOL  <= output_port[0];
    end
    
always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'hC))
    begin
        Si5324_B_FOS2 <= output_port[2];
        Si5324_B_FOS1 <= output_port[1];
        Si5324_B_LOL  <= output_port[0];
    end
    
always @ (posedge clk)
    if (write_strobe & port_id[7] & (port_id[3:0] == 4'hE))
    begin
        Si5324_C_FOS2 <= output_port[2];
        Si5324_C_FOS1 <= output_port[1];
        Si5324_C_LOL  <= output_port[0];
    end
        
//
// PicoBlaze Input Port Mux
//
always @ (posedge clk)
    if (~port_id[7])
        input_port <= {ga, 6'b0, miso};
    else 
        case(port_id[4:0])
            5'b00000:   input_port <= gpio_dir_0;
            5'b00001:   input_port <= gpio_dir_1;
            5'b00010:   input_port <= gpio_dir_2;
            5'b00011:   input_port <= gp_out_value_0;
            5'b00100:   input_port <= gp_out_value_1;
            5'b00101:   input_port <= gp_out_value_2;
            5'b00110:   input_port <= i2c_slave_adr;
            5'b00111:   input_port <= i2c_reg_adr;
            5'b01000:   input_port <= i2c_reg_dat_wr;
            5'b01001:   input_port <= {6'b0, i2c_rd_req, i2c_wr_req};
            5'b01010:   input_port <= {3'b0, Si5324_A_DHOLD, 3'b0, Si5324_A_clkin_sel};
            5'b01011:   input_port <= Si5324_A_fsel;
            5'b01100:   input_port <= {3'b0, Si5324_B_DHOLD, 3'b0, Si5324_B_clkin_sel};
            5'b01101:   input_port <= Si5324_B_fsel;
            5'b01110:   input_port <= {3'b0, Si5324_C_DHOLD, 3'b0, Si5324_C_clkin_sel};
            5'b01111:   input_port <= Si5324_C_fsel;
            5'b10000:   input_port <= {7'b0, clkin5_src_sel};
            5'b10001:   input_port <= {4'b0, Si5324_A_bw_sel};
            5'b10010:   input_port <= {4'b0, Si5324_B_bw_sel};
            5'b10011:   input_port <= {4'b0, Si5324_C_bw_sel};
            default:    input_port <= 8'h00;
        endcase

//
// Si5324 frequency select mapping
//
// For all output frequencies selected by Si5324_out_fsel codes 0 through 7,
// the mapping is simply the concatenation of the LS 3 bits of Si5324_out_fsel
// with the 5 bits of Si5324_in_fsel, just as was previously done when
// Si53234_out_fsel was just 3 bits. However, if bit 3 of Si5324_out_fsel is 1,
// use a mapping looking up ROM.
//

Si5324_fsel_lookup FSELA (
    .clk            (clk),
    .in_fsel        (Si5324_A_in_fsel),
    .out_fsel       (Si5324_A_out_fsel),
    .fsel           (Si5324_A_fsel));

Si5324_fsel_lookup FSELB (
    .clk            (clk),
    .in_fsel        (Si5324_B_in_fsel),
    .out_fsel       (Si5324_B_out_fsel),
    .fsel           (Si5324_B_fsel));

Si5324_fsel_lookup FSELC (
    .clk            (clk),
    .in_fsel        (Si5324_C_in_fsel),
    .out_fsel       (Si5324_C_out_fsel),
    .fsel           (Si5324_C_fsel));

endmodule

