//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: edh_errcnt.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:27:22-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: edh_errcnt.v,rcs $
// Revision 1.1  2004-12-15 11:27:22-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module keeps a running count of the number of video fields that contain
an EDH error. By default, the counter is a 24-bit counter, but the counter
width can be modified by changing the ERRFLD_WIDTH parameter.

A 16-bit wide error flag input vector, flags, allows up to sixteen different 
error flags to be monitored by the error counter. Each of the 16 error flags
has an associated flag_enable signal. If a flag_enable signal is low, the
corresponding error flag is ignored by the counter. If any enabled error flag
is asserted at the next EDH packet (edh_next asserted), the error counter is
incremented. There is no latching mechanism on the error flags -- they must
remain asserted until edh_next is asserted in order to increment the counter.

The error counter will saturate and will not roll over when it reaches the
maximum count. The counter is cleared on reset and when clr_errcnt is asserted.

A count enable input, count_en, is also provided to enable and disable the
error counter. This can be used to disable the counter when the video decoder
is not synchronized to the video stream. 
*/

`timescale 1ns / 1 ns

module  edh_errcnt (
    // inputs
    clk,            // clock input
    ce,             // clock enable
    rst,            // async reset input
    clr_errcnt,     // clears the error counter
    count_en,       // enables error counter when high
    flag_enables,   // specifies which error flags cause the counter to increment
    flags,          // error flag inputs
    edh_next,       // counter increments on edh_next asserted

    // outputs
    errcnt          // error counter
);


//-----------------------------------------------------------------------------
// Parameter definitions
//

//
// This group of parameters defines the bit widths of various fields in the
// module. 
//
parameter ERRFLD_WIDTH  = 24;                   // Width of error counter
parameter ERRFLD_MSB    = ERRFLD_WIDTH - 1;     // MS bit # of error counter

parameter FLAGS_WIDTH   = 16;                   // Width of error flag field
parameter FLAGS_MSB     = FLAGS_WIDTH - 1;      // MS bit # of error flag field
    
         
//-----------------------------------------------------------------------------
// Port definitions
//
input                   clk;
input                   ce;
input                   rst;
input                   clr_errcnt;
input                   count_en;
input   [FLAGS_MSB:0]   flag_enables;
input   [FLAGS_MSB:0]   flags;
input                   edh_next;
output  [ERRFLD_MSB:0]  errcnt;

//-----------------------------------------------------------------------------
// Signal definitions
//
wire    [FLAGS_MSB:0]   enabled_flags;  // error flags after ANDing with enables
wire                    err_in_field;   // OR of all enabled error flags
reg                     errcnt_tc;      // asserted when errcnt reaches terminal count
reg     [ERRFLD_MSB:0]  next_count;     // current error count + 1
reg     [ERRFLD_MSB:0]  cntr;           // actual error counter

//
// flag enabling logic
//
assign enabled_flags = flags & flag_enables;
assign err_in_field = |enabled_flags;

//
// error counter
//
always @ (cntr)
    begin
        next_count = cntr + 1;
        if (next_count == 0)
            errcnt_tc = 1'b1;
        else
            errcnt_tc = 1'b0;
    end
    
always @ (posedge clk or posedge rst)
    if (rst)
        cntr <= 0;
    else if (ce)
        begin
            if (clr_errcnt)
                cntr <= 0;
            else if (edh_next & ~errcnt_tc & err_in_field & count_en)
                cntr <= next_count;
        end
        
//
// output assignment
//
assign errcnt = cntr;
             
endmodule
