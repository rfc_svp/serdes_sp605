//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow
//  \   \        Filename: $RCSfile: Si5324_fsel_lookup.v,rcs $
//  /   /        Date Last Modified:  $Date: 2010-01-11 10:22:40-07 $
// /___/   /\    Date Created: December 3, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: Si5324_fsel_lookup.v,rcs $
// Revision 1.0  2010-01-11 10:22:40-07  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module converts the Si5324 input and output frequency select values into
an 8-bit frequency select number that is sent to the AVB FMC card to select
the programming for the Si5324.

For out_fsel values of 7 or less, the mapping is just a concatenation of
{out_fsel[2:0], in_fsel} which corresponds to the original 8-bit direct mapping
implemented in early versions of the AVB FMC code. If out_fsel[3] is 1, then
there is a mapping process that goes on to fit these other mapping values into
unused code spaces in the sparse 256-entry programming lookup table.

When the concatenation method is used, the select signals are delayed by one
clock cycle to correspond to the 1 clock cycle latency through the mapping path.
*/

`timescale 1ns / 1 ns

module Si5324_fsel_lookup
( 
    input   wire            clk,             // clock input
    input   wire [3:0]      out_fsel,        // selects the output frequency
    input   wire [4:0]      in_fsel,         // selects the input frequency
    output  wire [7:0]      fsel);           // frequency select value


reg  [7:0]  lookup_rom = 0;
reg  [3:0]  out_fsel_reg = 4'b0000;
reg  [4:0]  in_fsel_reg = 5'b00000;

always @ (posedge clk)
begin
    out_fsel_reg <= out_fsel;
    in_fsel_reg  <= in_fsel;
end

always @ (posedge clk)
    if (out_fsel[2:0] == 3'b000)        // corresponds to 297 MHz (out_fsel = 1000)
        case(in_fsel)
            5'd23:      lookup_rom <= 8'd247;
            5'd24:      lookup_rom <= 8'd248;
            5'd25:      lookup_rom <= 8'd249;
            5'd26:      lookup_rom <= 8'd250;
            5'd27:      lookup_rom <= 8'd251;
            default:    lookup_rom <= 8'd252;
        endcase
    else if (out_fsel[2:0] == 3'b001)   // corresponds to 297/1.001 MHz (out_fsel = 1001)
        case(in_fsel)
            5'd23:      lookup_rom <= 8'd210;
            5'd24:      lookup_rom <= 8'd211;
            5'd25:      lookup_rom <= 8'd212;
            5'd26:      lookup_rom <= 8'd213;
            5'd27:      lookup_rom <= 8'd214;
            default:    lookup_rom <= 8'd215;
        endcase
    else
        lookup_rom <= 8'd0;

assign fsel = out_fsel_reg[3] ? lookup_rom : {out_fsel_reg[2:0], in_fsel_reg};

endmodule

