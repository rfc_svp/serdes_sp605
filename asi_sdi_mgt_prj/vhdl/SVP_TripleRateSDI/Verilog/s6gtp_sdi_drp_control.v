//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: Reed P. Tidwell, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: s6gtp_sdi_drp_control.v,v $
//  /   /        Date Last Modified:  $Date: 2010-02-23 16:45:59-07 $
// /___/   /\    Date Created: May 27,2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: s6gtp_sdi_drp_control.v,v $
// Revision 1.4  2010-02-23 16:45:59-07  reedt
// Feb. 2010 full release.
//
// Revision 1.3  2010-01-27 15:45:00-07  reedt
// Deleted obsolete code.
//
// Revision 1.2  2009-12-08 12:15:42-07  reedt
// <>
//
// Revision 1.1  2009-11-18 14:29:01-07  reedt
// Resets and rate detect working for triple-rate RX
//
// Revision 1.0  2009-11-12 15:32:54-07  reedt
// Initial revision
//
// Revision 1.0  2009-07-01 16:13:45-06  reedt
// Initial revision
//

//------------------------------------------------------------------------------ 
// This file contains confidential and proprietary information of Xilinx, Inc. 
// and is protected under U.S. and international copyright and other 
// intellectual property laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any rights to the 
// materials distributed herewith. Except as otherwise provided in a valid 
// license issued to you by Xilinx, and to the maximum extent permitted by 
// applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL 
// FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, 
// IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF 
// MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; 
// and (2) Xilinx shall not be liable (whether in contract or tort, including 
// negligence, or under any other theory of liability) for any loss or damage of 
// any kind or nature related to, arising under or in connection with these 
// materials, including for any direct, or any indirect, special, incidental, 
// or consequential loss or damage (including loss of data, profits, goodwill, 
// or any type of loss or damage suffered as a result of any action brought by 
// a third party) even if such damage or loss was reasonably foreseeable or 
// Xilinx had been advised of the possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-safe, or for use in 
// any application requiring fail-safe performance, such as life-support or 
// safety devices or systems, Class III medical devices, nuclear facilities, 
// applications related to the deployment of airbags, or any other applications 
// that could lead to death, personal injury, or severe property or 
// environmental damage (individually and collectively, "Critical Applications").
// Customer assumes the sole risk and liability of any use of Xilinx products in
// Critical Applications, subject only to applicable laws and regulations 
// governing limitations on product liability. 
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT
// ALL TIMES.
//
//------------------------------------------------------------------------------ 
/*
Module Description:

This module connects to the DRP of the GTP and modifies attributes in the GTP
tile in response to changes on its input control signals. This module is
specifically designed to support triple-rate SDI interfaces implemented in the
GTP tile. It can independently select SD-SDI, HD-SDI, or 3G-SDI mode for each
of the two Rx and two Tx units in the tile. 

On initial startup and after a reset, the module will initialize all attributes
that it controls to match the input selections. After that it will selectively 
update attributes in response to changes on the control inputs.
*/

`timescale 1ns / 1 ns

module s6gtp_sdi_drp_control 
#(
    parameter PMA_RX_CFG_HD         = 25'h05ce059,  
    parameter PMA_RX_CFG_SD         = 25'h05ce000,
    parameter PMA_RX_CFG_3G         = 25'h05ce089,
    
    parameter PLL_RXDIVSEL_OUT_HD   = 2'b01,
    parameter PLL_RXDIVSEL_OUT_SD   = 2'b01,
    parameter PLL_RXDIVSEL_OUT_3G   = 2'b00,

    parameter PLL_TXDIVSEL_OUT_HD   = 2'b01,
    parameter PLL_TXDIVSEL_OUT_SD   = 2'b00,
    parameter PLL_TXDIVSEL_OUT_3G   = 2'b00)
(
    input   wire        clk,            // DRP DCLK
    input   wire        rst,            // async reset
    input   wire [1:0]  tx0_mode,       // TX0 mode select: 00=HD, 01=SD, 10=3G
    input   wire [1:0]  tx1_mode,       // TX1 mode select: 00=HD, 01=SD, 10=3G
    input   wire [1:0]  rx0_mode,       // RX0 mode select: 00=HD, 01=SD, 10=3G
    input   wire [1:0]  rx1_mode,       // RX1 mode select: 00=HD, 01=SD, 10=3G
    input   wire [15:0] do,             // connect to DO port on GTP
    input   wire        drdy,           // connect to DRDY port on GTP
    output  reg  [7:0]  daddr = 0,      // connect to DADDR port on GTP
    output  wire [15:0] di,             // connect to DI port on GTP
    output  reg         den,            // connect to DEN port on GTP
    output  reg         dwe,            // connect to DWE port on GTP
    output  wire        tx0_mode_change,// 1 for one clock cycle when Tx0 mode changes
    output  wire        tx1_mode_change,// 1 for one clock cycle when Tx1 mode changes
    output  wire        rx0_mode_change,// 1 for one clock cycle when Rx0 mode changes
    output  wire        rx1_mode_change // 1 for one clock cycle when Rx1 mode changes
);
             
//
// This group of constants defines the states of the master state machine.
// 
localparam MSTR_STATE_WIDTH = 5;
localparam MSTR_STATE_MSB   = MSTR_STATE_WIDTH - 1;

localparam [MSTR_STATE_MSB:0]
    MSTR_START          = 5'd0,
    MSTR_DO_REF         = 5'd1,
    MSTR_WAIT_REF       = 5'd2,
    MSTR_DO_RX0         = 5'd3,
    MSTR_WAIT_RX0       = 5'd4,
    MSTR_DO_RX0_B       = 5'd5,
    MSTR_WAIT_RX0_B     = 5'd6,
    MSTR_DO_RX0_C       = 5'd7,
    MSTR_WAIT_RX0_C     = 5'd8,
    MSTR_DO_RX1         = 5'd9,
    MSTR_WAIT_RX1       = 5'd10,
    MSTR_DO_RX1_B       = 5'd11,
    MSTR_WAIT_RX1_B     = 5'd12,
    MSTR_DO_RX1_C       = 5'd13,
    MSTR_WAIT_RX1_C     = 5'd14,
    MSTR_DO_TX0         = 5'd17,
    MSTR_WAIT_TX0       = 5'd18,
    MSTR_DO_TX1         = 5'd21,
    MSTR_WAIT_TX1       = 5'd22,
    MSTR_RST_0          = 5'd23,
    MSTR_RST_1          = 5'd24;

//
// This group of parameters defines the states of the DRP state machine.
//
localparam DRP_STATE_WIDTH = 3;
localparam DRP_STATE_MSB = DRP_STATE_WIDTH - 1;

localparam [DRP_STATE_MSB:0]
    DRP_STATE_WAIT =    3'b000,
    DRP_STATE_RD1 =     3'b001,
    DRP_STATE_RD2 =     3'b011,
    DRP_STATE_WR1 =     3'b010,
    DRP_STATE_WR2 =     3'b110;

//
// This group of parameters defines the values for the what_changed encoder.
//   
localparam CHANGED_MSB = 3;

localparam [CHANGED_MSB:0]
    NOTHING_CHANGED = 3'b000,
    TX0_CHANGED     = 3'b001,
    TX1_CHANGED     = 3'b011,
    RX0_CHANGED     = 3'b100,
    RX1_CHANGED     = 3'b101;

//
// Local signal declarations
//
(* ASYNC_REG = "TRUE" *)
reg  [1:0]              tx0_in_reg = 2'b00;                 // Sync & changed registers
(* ASYNC_REG = "TRUE" *)
reg  [1:0]              tx0_sync_reg = 2'b00;               // for TX0 mode select
reg  [1:0]              tx0_last_reg = 2'b00;
reg                     tx0_changed = 1'b1;
reg                     tx0_mc = 1'b1;

(* ASYNC_REG = "TRUE" *)
reg  [1:0]              tx1_in_reg = 2'b00;                 // Sync & changed registers
(* ASYNC_REG = "TRUE" *)
reg  [1:0]              tx1_sync_reg = 2'b00;               // for TX1 mode select
reg  [1:0]              tx1_last_reg = 2'b00;
reg                     tx1_changed = 1'b1;
reg                     tx1_mc = 1'b1;

(* ASYNC_REG = "TRUE" *)
reg  [1:0]              rx0_in_reg = 2'b00;                 // Sync & changed registers
(* ASYNC_REG = "TRUE" *)
reg  [1:0]              rx0_sync_reg = 2'b00;               // for RX0 mode select
reg  [1:0]              rx0_last_reg = 2'b00;
reg                     rx0_changed  = 1'b1;
reg                     rx0_mc = 1'b1;

(* ASYNC_REG = "TRUE" *)
reg  [1:0]              rx1_in_reg = 2'b00;                 // Sync & changed registers
(* ASYNC_REG = "TRUE" *)
reg  [1:0]              rx1_sync_reg = 2'b00;               // for RX1 mode select
reg  [1:0]              rx1_last_reg = 2'b00;
reg                     rx1_changed = 1'b1;
reg                     rx1_mc = 1'b1;

reg [CHANGED_MSB:0]     what_changed = NOTHING_CHANGED;     // indicates what value changed
reg [MSTR_STATE_MSB:0]  mstr_current_state = MSTR_START;    // master FSM current state
reg [MSTR_STATE_MSB:0]  mstr_next_state;                    // master FSM next state
reg [DRP_STATE_MSB:0]   drp_current_state = DRP_STATE_WAIT; // DRP FSM current state
reg [DRP_STATE_MSB:0]   drp_next_state;                     // DRP FSM next state

reg                     ld_changed = 1'b1;                  // 1 = load sync & changed registers
reg                     ld_changed_x;
reg                     clr_changed;                        // 1 = clear changed registers
reg                     drp_go;                             // Go signal from master FSM to DRP FSM
reg                     drp_rdy;                            // Ready signal from DRP FSM to master FSM
reg                     ld_capture;                         // 1 = load capture register

reg [15:0]              capture = 0;                        // Holds data from DRP read cycle
reg [15:0]              mask;                               // Masks bits not to be modified
reg [15:0]              mask_reg = 0;                       // Holds mask value
reg [15:0]              new_data;                           // New data to be ORed into read data
reg [15:0]              new_data_reg = 0;                   // Holds new_data value
reg                     ld_mask_nd;                         // Loads the mask & new_data registers
reg [7:0]               drp_daddr;                          // DRP address to be accessed
reg [24:0]              rx0_pma_cfg;                        // Holds new PMA_RX_CFG0 value
reg [24:0]              rx1_pma_cfg;                        // Holds new PMA_RX_CFG1 value
reg [1:0]               rx0_pll_div;                        // Holds new PLL_RXDIVSEL_OUT0 value
reg [1:0]               rx1_pll_div;                        // Holds new PLL_RXDIVSEL_OUT1 value
reg [1:0]               tx0_pll_div;                        // Holds new PLL_TXDIVSEL_OUT0 value
reg [1:0]               tx1_pll_div;                        // Holds new PLL_TXDIVSEL_OUT1 value

wire                    drp_timeout;                        // 1 = DRP access timeout
reg [9:0]               drp_to_counter = 0;                 // DRP timeout counter
reg                     clr_drp_to;                         // 1 = clear DRP timeout

//------------------------------------------------------------------------------
// Input change detectors
//
// For input signal there is an input register and a sync register, forming a
// dual-rank synchronizer to synchronize the signal to the DRP controller clock
// domain. This is followed by a "last" register hold the previous value. The
// sync register and the last register are compared and the changed FF is set
// if the values differ. The changed FF is cleared by a command from the master
// state machine when it is done processing the change request. All registers
// and the changed FF only clock when the ld_changed signal from the master
// state machine is asserted High (the exception is the input register). This
// is to prevent a change on a input data set from being missed should it 
// happen right when the master state machine is servicing a previous change
// on the same input data set.
//
always @ (posedge clk)
begin
    tx0_in_reg   <= tx0_mode;
    if (ld_changed) begin
        tx0_sync_reg <= tx0_in_reg;
        tx0_last_reg <= tx0_sync_reg;
    end
end

always @ (posedge clk or posedge rst)
    if (rst)
        tx0_changed <= 1'b1;
    else if (ld_changed && ((tx0_last_reg ^ tx0_sync_reg) != 0))
        tx0_changed <= 1'b1;
    else if (clr_changed && (what_changed == TX0_CHANGED))
        tx0_changed <= 1'b0;

always @ (posedge clk)
    tx0_mc <= tx0_changed;

assign tx0_mode_change = tx0_mc & ~tx0_changed;

always @ (posedge clk)
begin
    tx1_in_reg   <= tx1_mode;
    if (ld_changed) begin
        tx1_sync_reg <= tx1_in_reg;
        tx1_last_reg <= tx1_sync_reg;
    end
end

always @ (posedge clk or posedge rst)
    if (rst)
        tx1_changed <= 1'b1;
    else if (ld_changed && ((tx1_last_reg ^ tx1_sync_reg) != 0))
        tx1_changed <= 1'b1;
    else if (clr_changed && (what_changed == TX1_CHANGED))
        tx1_changed <= 1'b0;

always @ (posedge clk)
    tx1_mc <= tx1_changed;

assign tx1_mode_change = tx1_mc & ~tx1_changed;


always @ (posedge clk)
begin
    rx0_in_reg   <= rx0_mode;
    if (ld_changed) begin
        rx0_sync_reg <= rx0_in_reg;
        rx0_last_reg <= rx0_sync_reg;
    end
end

always @ (posedge clk or posedge rst)
    if (rst)
        rx0_changed <= 1'b1;
    else if (ld_changed && ((rx0_last_reg ^ rx0_sync_reg) != 0))
        rx0_changed <= 1'b1;
    else if (clr_changed && (what_changed == RX0_CHANGED))
        rx0_changed <= 1'b0;

always @ (posedge clk)
    rx0_mc <= rx0_changed;

assign rx0_mode_change = rx0_mc & ~rx0_changed;


always @ (posedge clk)
begin
    rx1_in_reg   <= rx1_mode;
    if (ld_changed) begin
        rx1_sync_reg <= rx1_in_reg;
        rx1_last_reg <= rx1_sync_reg;
    end
end

always @ (posedge clk or posedge rst)
    if (rst)
        rx1_changed <= 1'b1;
    else if (ld_changed && ((rx1_last_reg ^ rx1_sync_reg) != 0))
        rx1_changed <= 1'b1;
    else if (clr_changed && (what_changed == RX1_CHANGED))
        rx1_changed <= 1'b0;

always @ (posedge clk)
    rx1_mc <= rx1_changed;

assign rx1_mode_change = rx1_mc & ~rx1_changed;


//
// Create values used for the new data word

always @ *
    case(rx0_sync_reg)
        2'b01:   rx0_pma_cfg = PMA_RX_CFG_SD;
        2'b10:   rx0_pma_cfg = PMA_RX_CFG_3G;
        default: rx0_pma_cfg = PMA_RX_CFG_HD;
    endcase

always @ *
    case(rx1_sync_reg)
        2'b01:   rx1_pma_cfg = PMA_RX_CFG_SD;
        2'b10:   rx1_pma_cfg = PMA_RX_CFG_3G;
        default: rx1_pma_cfg = PMA_RX_CFG_HD;
    endcase

always @ *
    case(rx0_sync_reg)
        2'b01:   rx0_pll_div = PLL_RXDIVSEL_OUT_SD;
        2'b10:   rx0_pll_div = PLL_RXDIVSEL_OUT_3G;
        default: rx0_pll_div = PLL_RXDIVSEL_OUT_HD;
    endcase

always @ *
    case(rx1_sync_reg)
        2'b01:   rx1_pll_div = PLL_RXDIVSEL_OUT_SD;
        2'b10:   rx1_pll_div = PLL_RXDIVSEL_OUT_3G;
        default: rx1_pll_div = PLL_RXDIVSEL_OUT_HD;
    endcase

always @ *
    case(tx0_sync_reg)
        2'b01:   tx0_pll_div = PLL_TXDIVSEL_OUT_SD;
        2'b10:   tx0_pll_div = PLL_TXDIVSEL_OUT_3G;
        default: tx0_pll_div = PLL_TXDIVSEL_OUT_HD;
    endcase

always @ *
    case(tx1_sync_reg)
        2'b01:   tx1_pll_div = PLL_TXDIVSEL_OUT_SD;
        2'b10:   tx1_pll_div = PLL_TXDIVSEL_OUT_3G;
        default: tx1_pll_div = PLL_TXDIVSEL_OUT_HD;
    endcase

//------------------------------------------------------------------------------
// What Changed priority encoder
//
// This code looks at all the change requests and selects the current highest
// priority request to load into the what_changed register.
//
always @ (posedge clk)
    if (clr_changed)
        what_changed <= NOTHING_CHANGED;
    else if (ld_changed && (what_changed == NOTHING_CHANGED)) begin
        if (rx0_changed)
            what_changed <= RX0_CHANGED;
        else if (rx1_changed)
            what_changed <= RX1_CHANGED;
        else if (tx0_changed)
            what_changed <= TX0_CHANGED;
        else if (tx1_changed)
            what_changed <= TX1_CHANGED;
        else
            what_changed <= NOTHING_CHANGED;
    end

//------------------------------------------------------------------------------        
// Master state machine
//
// The master FSM examines the what_changed register and then initiates one
// or more RMW cycles to the DRP to modify the correct attributes for that
// particular change request.
//
// The actual DRP RMW cycles are handled by a separate FSM, the DRP FSM. The
// master FSM provides a DRP address, mask value, and new data words to the
// DRP FSM and asserts a drp_go signal. The DRP FSM does the actual RMW cycle
// and responds with a drp_rdy signal when the cycle is complete.
//

//
// Current state register
// 
always @ (posedge clk or posedge rst)
    if (rst)
    begin
        mstr_current_state <= MSTR_START;
        ld_changed <= 1'b1;
    end
    else
    begin
        mstr_current_state <= mstr_next_state;
        ld_changed  <= ld_changed_x;
    end

//
// Next state logic
//
always @ *
begin
    ld_changed_x = 1'b0;
    
    case(mstr_current_state)
        MSTR_START:
            if (what_changed == RX0_CHANGED)
                mstr_next_state = MSTR_DO_RX0;
            else if (what_changed == RX1_CHANGED)
                mstr_next_state = MSTR_DO_RX1;
            else if (what_changed == TX0_CHANGED)
                mstr_next_state = MSTR_DO_TX0;
            else if (what_changed == TX1_CHANGED)
                mstr_next_state = MSTR_DO_TX1;
            else
            begin
                mstr_next_state = MSTR_START;
                ld_changed_x = 1'b1;
            end

        MSTR_DO_REF:
            mstr_next_state = MSTR_WAIT_REF;

        MSTR_WAIT_REF:
            if (drp_rdy)
            begin
                mstr_next_state = MSTR_START;
                ld_changed_x = 1'b1;
            end
            else
                mstr_next_state = MSTR_WAIT_REF;

        MSTR_DO_RX0:
            mstr_next_state = MSTR_WAIT_RX0;

        MSTR_WAIT_RX0:
            if (drp_rdy)
                mstr_next_state = MSTR_DO_RX0_B;
            else
                mstr_next_state = MSTR_WAIT_RX0;

        MSTR_DO_RX0_B:
            mstr_next_state = MSTR_WAIT_RX0_B;

        MSTR_WAIT_RX0_B:
            if (drp_rdy)
                mstr_next_state = MSTR_DO_RX0_C;
            else
                mstr_next_state = MSTR_WAIT_RX0_B;

        MSTR_DO_RX0_C:
            mstr_next_state = MSTR_WAIT_RX0_C;

        MSTR_WAIT_RX0_C:
            if (drp_rdy)
            begin
                mstr_next_state = MSTR_START;
                ld_changed_x = 1'b1;
            end
            else
                mstr_next_state = MSTR_WAIT_RX0_C;

        MSTR_DO_RX1:
            mstr_next_state = MSTR_WAIT_RX1;

        MSTR_WAIT_RX1:
            if (drp_rdy)
                mstr_next_state = MSTR_DO_RX1_B;
            else
                mstr_next_state = MSTR_WAIT_RX1;

        MSTR_DO_RX1_B:
            mstr_next_state = MSTR_WAIT_RX1_B;

        MSTR_WAIT_RX1_B:
            if (drp_rdy)
                mstr_next_state = MSTR_DO_RX1_C;
            else
                mstr_next_state = MSTR_WAIT_RX1_B;

        MSTR_DO_RX1_C:
            mstr_next_state = MSTR_WAIT_RX1_C;

        MSTR_WAIT_RX1_C:
            if (drp_rdy)
            begin
                mstr_next_state = MSTR_START;
                ld_changed_x = 1'b1;
            end
            else
                mstr_next_state = MSTR_WAIT_RX1_C;

        MSTR_DO_TX0:
            mstr_next_state = MSTR_WAIT_TX0;

        MSTR_WAIT_TX0:
            if (drp_rdy)
            begin
                mstr_next_state = MSTR_START;
                ld_changed_x = 1'b1;
            end
            else
                mstr_next_state = MSTR_WAIT_TX0;

        MSTR_DO_TX1:
            mstr_next_state = MSTR_WAIT_TX1;

        MSTR_WAIT_TX1:
            if (drp_rdy)
            begin
                mstr_next_state = MSTR_START;
                ld_changed_x = 1'b1;
            end
            else
                mstr_next_state = MSTR_WAIT_TX1;
            
        MSTR_RST_0:
            begin
                mstr_next_state = MSTR_RST_1;
                ld_changed_x = 1'b1;
            end

        MSTR_RST_1:
            begin
                mstr_next_state = MSTR_START;
                ld_changed_x = 1'b1;
            end

        default:
            begin
                mstr_next_state = MSTR_RST_0;
                ld_changed_x = 1'b1;
            end

    endcase
end

//
// Output logic
//
always @ *
begin
    clr_changed = 1'b0;
    drp_go = 1'b0;
    mask = 0;
    new_data = 0;
    ld_mask_nd = 1'b0;
    drp_daddr = 0;

    case(mstr_current_state)
        MSTR_DO_REF:    begin                // Ref clock switching 
                            drp_go = 1'b1;
                            mask = 16'hffff; // Ref clock switching disabled by mask
                            new_data = 16'b0;  
                            ld_mask_nd = 1'b1;
                        end

        MSTR_WAIT_REF:  clr_changed = 1'b1;

        MSTR_DO_RX0:    begin                // RX0_PMA_CFG first part
                            drp_go = 1'b1;
                            drp_daddr = 8'h36;
                            mask = 16'h0000;
                            new_data = rx0_pma_cfg[15:0];
                            ld_mask_nd = 1'b1;
                        end

        MSTR_DO_RX0_B:  begin                // RX0_PMA_CFG second part
                            drp_go = 1'b1;
                            drp_daddr = 8'h37;
                            mask = 16'hfe00;
                            new_data = {7'h00, rx0_pma_cfg[24:16]};
                            ld_mask_nd = 1'b1;
                        end

        MSTR_DO_RX0_C:  begin              // RX0_PLL_DIV
                            drp_go = 1'b1;
                            drp_daddr = 8'h34;
                            mask = 16'hfcff;
                            new_data = {6'b0, rx0_pll_div[1:0], 8'b0}; 
                            ld_mask_nd = 1'b1;
                        end
                    
        MSTR_WAIT_RX0_C:clr_changed = 1'b1;
            
        MSTR_DO_RX1:    begin                // RX1_PMA_CFG first part
                            drp_go = 1'b1;
                            drp_daddr = 8'h76;
                            mask = 16'h0000;
                            new_data = rx1_pma_cfg[15:0];
                            ld_mask_nd = 1'b1;
                        end

        MSTR_DO_RX1_B:  begin              // RX1_PMA_CFG second part
                            drp_go = 1'b1;
                            drp_daddr = 8'h77;
                            mask = 16'hfe00;
                            new_data = {7'b0, rx1_pma_cfg[24:16]};
                            ld_mask_nd = 1'b1;
                        end

        MSTR_DO_RX1_C:  begin          // RX1_PLL_DIV       
                            drp_go = 1'b1;
                            drp_daddr = 8'h74;
                            mask = 16'hfcff;
                            new_data = {6'b0,rx1_pll_div[1:0], 8'b0};
                            ld_mask_nd = 1'b1;
                        end
        
        MSTR_WAIT_RX1_C:clr_changed = 1'b1;

        MSTR_DO_TX0:    begin            // TX0_PLL_DIV 
                            drp_go = 1'b1;
                            drp_daddr = 8'h34;
                            mask = 16'hf3ff;
                            new_data = {4'b0,tx0_pll_div[1:0], 10'b0};
                            ld_mask_nd = 1'b1;
                        end
    
        MSTR_WAIT_TX0:clr_changed = 1'b1;

        MSTR_DO_TX1:    begin               // TX1_PLL_DIV
                            drp_go = 1'b1;
                            drp_daddr = 8'h74;
                            mask = 16'hf3ff;
                            new_data = {4'b0, tx1_pll_div[1:0], 10'b0};
                            ld_mask_nd = 1'b1;
                        end

        MSTR_WAIT_TX1:  clr_changed = 1'b1;

    endcase
end

//------------------------------------------------------------------------------
// DRP state machine
//
// The DRP state machine performs the RMW cycle on the DRP at the request of the
// master FSM. The master FSM provides the DRP address, a 16-bit mask indicating
// which bits are to be modified (bits set to 0), and a 16-bit new data value
// containing the new value. When the drp_go signal from the master FSM is
// asserted, the DRP FSM will execute the RMW cycle. The DRP FSM asserts the
// drp_rdy signal when it is ready to execute a RMW cycle and negates it for
// the duration of the RMW cycle.
//
// A timeout timer is used to timeout a DRP access should the DRP fail to
// respond with a DRDY signal within a reasonable amount of time.
//

//
// Current state register
//
always @ (posedge clk or posedge rst)
    if (rst)
        drp_current_state <= DRP_STATE_WAIT;
    else if (drp_timeout)
        drp_current_state <= DRP_STATE_WAIT;
    else
        drp_current_state <= drp_next_state;

//
// Next state logic
//
always @ *
    case(drp_current_state)
        DRP_STATE_WAIT:
            if (drp_go)
                drp_next_state = DRP_STATE_RD1;
            else
                drp_next_state = DRP_STATE_WAIT;

        DRP_STATE_RD1:
            drp_next_state = DRP_STATE_RD2;

        DRP_STATE_RD2:
            if (drdy)
                drp_next_state = DRP_STATE_WR1;
            else
                drp_next_state = DRP_STATE_RD2;

        DRP_STATE_WR1:
            drp_next_state = DRP_STATE_WR2;

        DRP_STATE_WR2:
            if (drdy)
                drp_next_state = DRP_STATE_WAIT;
            else
                drp_next_state = DRP_STATE_WR2;

        default: drp_next_state = DRP_STATE_WAIT;
    endcase

always @ (posedge clk)
    if (ld_mask_nd) begin
        mask_reg <= mask;
        new_data_reg <= new_data;
        daddr <= drp_daddr;
    end

//
// Output logic
//
always @ *
begin
    den = 0;
    dwe = 0;
    ld_capture = 0;
    drp_rdy = 0;
    clr_drp_to = 0;

    case(drp_current_state)
        DRP_STATE_WAIT: begin
                            drp_rdy = 1'b1;
                            clr_drp_to = 1'b1;
                        end

        DRP_STATE_RD1:  den = 1'b1;

        DRP_STATE_RD2:  ld_capture = 1'b1;

        DRP_STATE_WR1:  begin
                            den = 1'b1;
                            dwe = 1'b1;
                            clr_drp_to = 1'b1;
                        end
    endcase
end

//
// A timeout counter for DRP accesses. If the timeout counter reaches its
// terminal count, the DRP state machine aborts the transfer.
//
always @ (posedge clk)
    if (clr_drp_to)
        drp_to_counter <= 0;
    else
        drp_to_counter <= drp_to_counter + 1;

assign drp_timeout = &drp_to_counter;

//
// DRP di capture register
//
always @ (posedge clk)
    if (ld_capture)
        capture <= do;

assign di = (capture & mask_reg) | (new_data_reg & ~mask_reg);

endmodule

