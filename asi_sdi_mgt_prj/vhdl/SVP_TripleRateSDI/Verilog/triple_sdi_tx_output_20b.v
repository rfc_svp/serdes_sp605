//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow
//  \   \        Filename: $RCSfile: triple_sdi_tx_output_20b.v,rcs $
//  /   /        Date Last Modified:  $Date: 2009-03-10 15:48:17-06 $
// /___/   /\    Date Created: January 6, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: triple_sdi_tx_output_20b.v,rcs $
// Revision 1.0  2009-03-10 15:48:17-06  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This is the output module for a triple-rate SD/HD/3G-SDI transmitter. It
inserts EDH packets for SD and CRC & LN words for HD and 3G. It scrambles the
data for transmission. For SD, it implements 11X bit replication. For HD and
3G, it converts the data to a 10-bit data stream for connection to a 20-bit
TXDATA port on the serializer.

The clk frequency is normally 74.25 MHz for HD-SDI and 148.5 MHz for 3G-SDI and
SD-SDI. The clock enable must be 1 always for HD-SDI and 3G-SDI, unless for some
reason, the clock frequency is twice as much as normal). For SD-SDI, it must 
average 27 MHz, by asserting it at a 5/6/5/6 clock cycle cadence. For 
level B 3G-SDI, all four input data streams are active and the actual data rate 
is 74.25 MHz, even though the clock frequency is 148.5 MHz. In this case, 
din_rdy must be asserted every other clock cycle to indicate on which clock 
cycle the input data should be taken by the module. For all other modes, 
din_rdy should always be High. For dual link HD-SDI with 1080p 60 Hz or 50 Hz
video, the clock frequency will typically be 148.5 MHz, but the data rate is
74.25 MHz and ce is asserted every other clock cycle with din_rdy always High.
*/

module triple_sdi_tx_output_20b (
    input  wire             clk,            // 74.25 MHz (HD) or 148.5 MHz (SD/3G)
    input  wire             din_rdy,        // input data ready
    input  wire [1:0]       ce,             // runs at scrambler data rate: 27 MHz, 74.25 MHz, or 148.5 MHz
    input  wire             rst,            // async reset input
    input  wire [1:0]       mode,           // data path mode: 00 = HD or 3GA, 01 = SD, 10 = 3GB
    input  wire [9:0]       ds1a,           // SD Y/C input, HD Y input, 3G Y input, dual link A_Y input
    input  wire [9:0]       ds2a,           // HD C input, 3G C input, dual link A_C input
    input  wire [9:0]       ds1b,           // dual link B_Y input
    input  wire [9:0]       ds2b,           // dual link B_C input
    input  wire             insert_crc,     // 1 = insert CRC for HD and 3G
    input  wire             insert_ln,      // 1 = insert LN for HD and 3G
    input  wire             insert_edh,     // 1 = generate and insert EDH packets in SD
    input  wire [10:0]      ln_a,           // HD/3G line number for link A
    input  wire [10:0]      ln_b,           // HD/3G line number for link B
    input  wire             eav,            // HD/3G EAV (asserted on EAV XYZ word)
    input  wire             sav,            // HD/3G SAV (asserted on SAV XYZ word)
    output wire [19:0]      txdata,         // output data stream
    output wire             ce_align_err);  // 1 if ce 5/6/5/6 cadence is broken


//
// Internal signals
//
reg  [9:0]      ds1a_reg = 0;               // input registers
reg  [9:0]      ds2a_reg = 0;
reg  [9:0]      ds1b_reg = 0;
reg  [9:0]      ds2b_reg = 0;
reg  [10:0]     ln_a_reg = 0;
reg  [10:0]     ln_b_reg = 0;
reg  [1:0]      mode_reg = 0;
reg             eav_reg = 0;
reg             sav_reg = 0;
reg             ins_crc_reg = 0;
reg             ins_ln_reg = 0;
reg             ins_edh_reg = 0;
reg  [3:0]      eav_dly = 0;                // generates timing signals based on EAV
wire [9:0]      edh_out;                    // EDH processor video output
wire [9:0]      edh_mux;                    // EDH processor bypass mux
wire [9:0]      ds1a_edh_mux;               // chooses SD or HD/3G data steam 1 A
wire [9:0]      ln_out_ds1a;                // data stream 1 A out of LN insert
wire [9:0]      ln_out_ds2a;                // data stream 2 A out of LN insert
wire [9:0]      ln_out_ds1b;                // data stream 1 B out of LN insert
wire [9:0]      ln_out_ds2b;                // data stream 2 B out of LN insert
wire [17:0]     crc_ds1a;                   // calculated CRC for data stream 1 A
wire [17:0]     crc_ds2a;                   // calculated CRC for data stream 2 A
wire [17:0]     crc_ds1b;                   // calculated CRC for data stream 1 B
wire [17:0]     crc_ds2b;                   // calculated CRC for data stream 2 B
wire [9:0]      crc_out_ds1a;               // data stream 1 A out of CRC insert
wire [9:0]      crc_out_ds2a;               // data stream 2 A out of CRC insert
wire [9:0]      crc_out_ds1b;               // data stream 1 B out of CRC insert
wire [9:0]      crc_out_ds2b;               // data stream 2 B out of CRC insert
wire [9:0]      scram_in_ds1;               // scrambler ds1 input
wire [9:0]      scram_in_ds2;               // scrambler ds2 input
wire [19:0]     scram_out;                  // scrambler registered output
wire [19:0]     sd_bit_rep_out;             // output of SD 11X bit replicate
reg             crc_en = 1'b0;              // CRC control signal
reg             clr_crc = 1'b0;             // CRC control signal
wire            mode_SD;                    // asserted when mode = 01
wire            mode_3G_B;                  // asserted when mode = 10
reg  [19:0]     txdata_reg = 0;
(* equivalent_register_removal = "no" *)
reg  [2:0]      rst_reg = 0;
wire            align_err;


always @ (posedge clk)
    if (ce[0])
        rst_reg <= {3 {rst}};

//
// Input registers
//
always @ (posedge clk)
    if (ce[0])
        if (din_rdy)
            begin
                ds1a_reg    <= ds1a;
                ds2a_reg    <= ds2a;
                ds1b_reg    <= ds1b;
                ds2b_reg    <= ds2b;
                ln_a_reg    <= ln_a;
                ln_b_reg    <= ln_b;
                mode_reg    <= mode;
                eav_reg     <= eav;
                sav_reg     <= sav;
                ins_crc_reg <= insert_crc;
                ins_ln_reg  <= insert_ln;
                ins_edh_reg <= insert_edh;
            end

assign mode_SD = mode_reg == 2'b01;
assign mode_3G_B = mode_reg == 2'b10;

//
// EAV delay register
//
// Generates timing control signals for line number insertion and CRC generation
// and insertion.
//
always @ (posedge clk or posedge rst_reg[0])
    if (rst_reg[0])
        eav_dly <= 0;
    else if (ce[0])
        if (din_rdy)
            eav_dly <= {eav_dly[2:0], eav_reg};

//
// Instantiate the line number formatting and insertion modules
//
hdsdi_insert_ln INSLNA (
    .insert_ln  (ins_ln_reg),
    .ln_word0   (eav_dly[0]),
    .ln_word1   (eav_dly[1]),
    .c_in       (ds2a_reg),
    .y_in       (ds1a_reg),
    .ln         (ln_a_reg),
    .c_out      (ln_out_ds2a),
    .y_out      (ln_out_ds1a));
        
hdsdi_insert_ln INSLNB (
    .insert_ln  (ins_ln_reg),
    .ln_word0   (eav_dly[0]),
    .ln_word1   (eav_dly[1]),
    .c_in       (ds2b_reg),
    .y_in       (ds1b_reg),
    .ln         (ln_b_reg),
    .c_out      (ln_out_ds2b),
    .y_out      (ln_out_ds1b));

//
// Generate timing control signals for the CRC calculators.
//
// The crc_en signal determines which words are included into the CRC 
// calculation. All words that enter the hdsdi_crc module when crc_en is high
// are included in the calculation. To meet the HD-SDI spec, the CRC calculation
// must being with the first word after the SAV and end after the second line
// number word after the EAV.
//
// The clr_crc signal clears the internal registers of the hdsdi_crc modules to
// cause a new CRC calculation to begin. The crc_en signal is asserted during
// the XYZ word of the SAV since the next word after the SAV XYZ word is the
// first word to be included into the new CRC calculation.
//
always @ (posedge clk or posedge rst_reg[0])
    if (rst_reg[0])
        crc_en <= 1'b0;
    else if (ce[0])
        if (din_rdy)
            begin
                if (sav_reg)
                    crc_en <= 1'b1;
                else if (eav_dly[1])
                    crc_en <= 1'b0;
            end

always @ (posedge clk or posedge rst_reg[0])
    if (rst_reg[0])
        clr_crc <= 1'b0;
    else if (ce[0])
        if (din_rdy)
            clr_crc <= sav_reg;

//
// Instantiate the CRC generators
//
hdsdi_crc2 CRC1A (
    .clk        (clk),
    .ce         (ce[0]),
    .en         (din_rdy & crc_en),
    .rst        (rst_reg[0]),
    .clr        (clr_crc),
    .d          (ln_out_ds1a),
    .crc_out    (crc_ds1a)
);

hdsdi_crc2 CRC2A (
    .clk        (clk),
    .ce         (ce[0]),
    .en         (din_rdy & crc_en),
    .rst        (rst_reg[0]),
    .clr        (clr_crc),
    .d          (ln_out_ds2a),
    .crc_out    (crc_ds2a)
);

hdsdi_crc2 CRC1B (
    .clk        (clk),
    .ce         (ce[0]),
    .en         (din_rdy & crc_en),
    .rst        (rst_reg[0]),
    .clr        (clr_crc),
    .d          (ln_out_ds1b),
    .crc_out    (crc_ds1b)
);

hdsdi_crc2 CRC2B (
    .clk        (clk),
    .ce         (ce[0]),
    .en         (din_rdy & crc_en),
    .rst        (rst_reg[0]),
    .clr        (clr_crc),
    .d          (ln_out_ds2b),
    .crc_out    (crc_ds2b)
);

//
// Insert the CRC values into the data streams. The CRC values are inserted
// after the line number words after the EAV.
//
hdsdi_insert_crc CRCA (
    .insert_crc (ins_crc_reg),
    .crc_word0  (eav_dly[2]),
    .crc_word1  (eav_dly[3]),
    .y_in       (ln_out_ds1a),
    .c_in       (ln_out_ds2a),
    .y_crc      (crc_ds1a),
    .c_crc      (crc_ds2a),
    .y_out      (crc_out_ds1a),
    .c_out      (crc_out_ds2a));

hdsdi_insert_crc CRCB (
    .insert_crc (ins_crc_reg),
    .crc_word0  (eav_dly[2]),
    .crc_word1  (eav_dly[3]),
    .y_in       (ln_out_ds1b),
    .c_in       (ln_out_ds2b),
    .y_crc      (crc_ds1b),
    .c_crc      (crc_ds2b),
    .y_out      (crc_out_ds1b),
    .c_out      (crc_out_ds2b));

//
// EDH Processor for SD-SDI
//

edh_processor EDH (
    .clk             (clk),
    .ce              (ce[1]),
    .rst             (rst_reg[1]),
    .vid_in          (ds1a_reg),
    .reacquire       (1'b0),
    .en_sync_switch  (1'b0),
    .en_trs_blank    (1'b0),
    .anc_idh_local   (1'b0),
    .anc_ues_local   (1'b0),
    .ap_idh_local    (1'b0),
    .ff_idh_local    (1'b0),
    .errcnt_flg_en   (16'b0),
    .clr_errcnt      (1'b0),
    .receive_mode    (1'b0),

    .vid_out         (edh_out),
    .std             (),
    .std_locked      (),
    .trs             (),
    .field           (),
    .v_blank         (),
    .h_blank         (),
    .horz_count      (),
    .vert_count      (),
    .sync_switch     (),
    .locked          (),
    .eav_next        (),
    .sav_next        (),
    .xyz_word        (),
    .anc_next        (),
    .edh_next        (),
    .rx_ap_flags     (),
    .rx_ff_flags     (),
    .rx_anc_flags    (),
    .ap_flags        (),
    .ff_flags        (),
    .anc_flags       (),
    .packet_flags    (),
    .errcnt          (),
    .edh_packet      ());

//
// This mux bypasses the EDH inserter if insert_edh is 0.
//
assign edh_mux = ins_edh_reg ? edh_out : ds1a_reg;

//
// These muxes select the inputs for the scrambler. In SD, HD, and 3G level A
// modes, they simply pass ds1a and ds2a through. In 3G level B mode, they
// interleave data streams 1 and 2 of link A onto the Y input of the scrambler
// and data streams 1 and 2 of link B onto the C input.
//
assign scram_in_ds1 = mode_3G_B ? (din_rdy ? crc_out_ds1a : crc_out_ds2a) : crc_out_ds1a;
assign scram_in_ds2 = mode_3G_B ? (din_rdy ? crc_out_ds1b : crc_out_ds2b) : crc_out_ds2a;

//
// This mux selects the SD path or the HD/3G path for data stream 1.
//
assign ds1a_edh_mux = mode_SD ? edh_mux : scram_in_ds1;

//
// SDI scrambler
//
// In SD mode, this module scrambles just 10 bits on the Y channel. In HD and
// 3G modes, this modules scrambles 20 bits. In HD mode, the scrambler is
// enabled by ce AND din_rdy in order to support both regular HD-SDI and dual-
// link HD-SDI. In 3G-SDI mode, the scrambler is controlled by just ce.
//

multi_sdi_encoder SCRAM (
    .clk        (clk),
    .rst        (1'b0),
    .ce         (ce[0]),
    .hd_sd      (mode_SD),
    .nrzi       (1'b1),
    .scram      (1'b1),
    .c          (scram_in_ds2),
    .y          (ds1a_edh_mux),
    .q          (scram_out));

//
// SD 11X bit replicater
//
sdi_bitrep_20b BITREP (
    .clk        (clk),
    .rst        (rst_reg[2]),
    .ce         (ce[0]),
    .d          (scram_out[19:10]),
    .q          (sd_bit_rep_out),
    .align_err  (align_err));

assign ce_align_err = align_err & mode_SD;

//
// Output register
//
always @ (posedge clk)
    if (mode_SD)
        txdata_reg <= sd_bit_rep_out;
    else if (ce[0])
        txdata_reg <= scram_out;

assign txdata = txdata_reg;

endmodule