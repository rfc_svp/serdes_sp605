
///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2003 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU	Optimized for SD-SDI
//  /   /         Filename: tb_hw_dru.v
// /___/   /\     Timestamp: May 1 2007
// \   \  /  \
//  \___\/\___\
//
//Device: Virtex 5 LXT/FXT/TXT
//Design Name: dru.v
//Purpose: The NIDRU wrapper.
//Authors: Paolo Novellini, Giovanni Guasti. 
//
//    
///////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module dru(
    input   [19:0] 	DT_IN,
    input   [36:0]  CENTER_F,
    input   [4:0] 	G1,
    input   [4:0] 	G1_P,
    input   [4:0] 	G2,
    input   			    CLK,
    input   			    RST,
    input          RST_FREQ,
    output [7:0]   VER,
    input          EN,
    output [31:0]  INTEG,
    output [31:0]  DIRECT,
    output [31:0]  CTRL,
    output [20:0] 	PH_OUT,
    output [19:0] 	RECCLK,
    output [3:0] 	SAMV,
    output [9:0] 	SAM
    );


endmodule
