////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Author : Reed P. Tidwell
//  \   \         Filename: $RCSfile: sp605_passthru.v,v $
//  /   /         Date Last Modified:  $Date: 2010-11-05 13:10:37-06 $
// /___/   /\     Date Created: March 2, 2010
// \   \  /  \ 
//  \___\/\___\
//
// Revision History: 
// $Log: sp605_passthru.v,v $
// Revision 1.0  2010-11-05 13:10:37-06  reedt
// Initial revision
//
// Revision 1.5  2010-04-09 08:32:15-06  reedt
// April 2010 release.
//
//
// (c) Copyright 2010 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of,
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES. 
//------------------------------------------------------------------------------ 
//
// Module Description:
//
// This module is a triple-rate SDI transmitter with signal generator, and receiver 
// with data recovery, error checking, and line standard identification. It 
// supports SD-SDI, HD-SDI, and level A 3G-SDI (1080p 50, 59.94, or 60 Hz only). 
// It runs on  SP605 boards with the AVB FMC daughter card.
// 
//------------------------------------------------------------------------------ 


`timescale 1ns / 1ps
`default_nettype none


//***********************************Entity Declaration************************

module sp605_passthru #
(
    parameter EXAMPLE_SIM_GTPRESET_SPEEDUP              =   1,   // simulation setting for GTP SecureIP model
    parameter EXAMPLE_USE_CHIPSCOPE                     =   1,    // Set to 1 to use Chipscope to drive resets
    parameter EXAMPLE_SIMULATION                        =   0,    // Set to 1 in testbench for simulation
    parameter RATE_REFCLK_FREQ                          =  32000000,   // Frequency of the System ACE clock
    parameter CLOCK_SEL_PERIOD                          =   8,         // period for changing the GTP reference clock in quarter seconds
    parameter CLKSEL_PERIOD_MAXCNT                      =  RATE_REFCLK_FREQ / 4 * CLOCK_SEL_PERIOD -1
)
(
    input  wire          TILE0_GTP0_REFCLK_PAD_N_IN,    // GTP
    input  wire          TILE0_GTP0_REFCLK_PAD_P_IN,
    output wire          USER_SMA_GPIO_P,
    output wire          USER_SMA_GPIO_N,
    input  wire          GPIO_SWITCH_0,
    input  wire          GPIO_SWITCH_1,
    input  wire          GPIO_SWITCH_2,
    input  wire          GPIO_SWITCH_3,
    input  wire          GPIO_BUTTON0,
    input  wire          GPIO_BUTTON1,
    input  wire          GPIO_BUTTON2,
    input  wire          GPIO_BUTTON3,
    input  wire          CPU_RESET,
    output wire          GPIO_HEADER_0_LS,
    output wire          GPIO_HEADER_1_LS,
    output wire          GPIO_HEADER_2_LS,
    output wire          GPIO_HEADER_3_LS,
    output wire          GPIO_LED_0,
    output wire          GPIO_LED_1,
    output wire          GPIO_LED_2,
    output wire          GPIO_LED_3,
    output wire          FPGA_AWAKE,
    input  wire  [1:0]   RXN_IN,
    input  wire  [1:0]   RXP_IN,
    output wire  [1:0]   TXN_OUT,
    output wire  [1:0]   TXP_OUT, 
    
    // ************************** FMC connector *****************************
    input  wire           PAD_Si5324_ckout1_p,  // CKOUT1 from Si5324 on FMC
    input  wire           PAD_Si5324_ckout1_n,  // CKOUT1 from Si5324 on FMC
    input  wire           PAD_fmc_xbar1_out2_p,  // OUT2 from XBAR1 on FMC
    input  wire           PAD_fmc_xbar1_out2_n,  // OUT2 from XBAR1 on FMC
    output wire           PAD_Si5324_B_ckin2_p, // CKIN2 on Si5324 B on clock mod. L
    output wire           PAD_Si5324_B_ckin2_n, // CKIN2 on Si5324 B on clock mod. L
    output wire           PAD_si5324_A_rst_n,   // active low reset for chip A
    output wire           PAD_si5324_B_rst_n,   // active low reset for chip A
    output wire           PAD_si5324_C_rst_n,   // active low reset for chip A

    input  wire           PAD_clk_fmc_27M_in,   // 27 MHz X0
    output wire           PAD_main_spi_sck,       	// main SPI interface  SCK
    output wire           PAD_main_spi_mosi,       // main SPI interface MOSI
    input  wire           PAD_main_spi_miso,       // main SPI interface MISO
    output wire           PAD_main_spi_ss,         // main SPI interface SS
    
    output wire           PAD_cml_spi_sck,    	// clock module L SPI interface  SCK
    output wire           PAD_cml_spi_mosi,    // clock module L SPI interface MOSI
    input  wire           PAD_cml_spi_miso,    // clock module L SPI interface MISO
    output wire           PAD_cml_spi_ss,      // clock module L SPI interface SS

    input  wire           PAD_fmc_hsync_in,
    // GTP connections
    input  wire           PAD_gtp123_1_refclk_p,   // FMC gtp refclk
    input  wire           PAD_gtp123_1_refclk_n,  
    output wire           PAD_gtp123_1_txp,        // FMC gtp Tx
    output wire           PAD_gtp123_1_txn,
    input  wire           PAD_gtp123_1_rxp,        // FMC gtp Rx
    input  wire           PAD_gtp123_1_rxn
    
);                                                 
                                                  	
    
//************************** Register Declarations ****************************

    reg     [84:0]  ila_in0_r;
    reg     [84:0]  ila_in1_r;
    reg             tile0_resetdone0_r;
    reg             tile0_resetdone0_r2;
    reg             tile0_resetdone1_r;
    reg             tile0_resetdone1_r2;
    

//**************************** Wire Declarations ******************************

    //------------------------ MGT Wrapper Wires ------------------------------
    //---------------------- Loopback and Powerdown Ports ----------------------
    wire    [2:0]   tile0_loopback0_i;
    wire    [2:0]   tile0_loopback1_i;
    //------------------------------- PLL Ports --------------------------------
    wire            tile0_gtpreset0_i;
    wire            tile0_gtpreset1_i;
    wire            tile0_plllkdet0_i;
    wire            tile0_plllkdet1_i;
    wire            tile0_resetdone0_i;
    wire            tile0_resetdone1_i;
    //----------------- Receive Ports - RX Data Path interface -----------------
    wire    [19:0]  tile0_rxdata0_i;
    wire    [19:0]  rxdata1_20_gtp;
    wire    [19:0]  rxdata1_20;
    wire            tile0_rxrecclk0_i;
    wire            tile0_rxrecclk1_i;
    wire            rxrecclk1_buf;
    //----- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    wire            tile0_rxcdrreset0_i;
    wire            tile0_rxcdrreset1_i;
    wire            auto_rxcdrreset;
    //--------- Receive Ports - RX Elastic Buffer and Phase Alignment ----------
    wire            tile0_rxbufreset0_i;
    wire            tile0_rxbufreset1_i;
    wire            auto_rxbufreset;
    wire    [2:0]   tile0_rxbufstatus0_i;
    wire    [2:0]   tile0_rxbufstatus1_i;
    //-------------------------- TX/RX Datapath Ports --------------------------
    wire    [1:0]   tile0_gtpclkout0_i;
    wire    [1:0]   tile0_gtpclkout1_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [19:0]  tile0_txdata0_i;
    wire    [19:0]  txdata1_20;
    wire    [19:0]  txdata1_20_gtp;
    wire            tile0_txoutclk0_i;
    wire            tile0_txoutclk1_i;
    wire            tx_ce_align_err;


    //----------------------------- Global Signals -----------------------------
    wire            drp_clk_in_i;
    wire            tile0_refclkout_bufg_i;
    
    
    //--------------------------- User Clocks ---------------------------------
    wire            txusrclk;
    wire            txusrclk2;
    wire            txpipeclk;
    wire            rxusrclk;
    wire            rxusrclk2;
    wire            rxpipeclk; 
    wire            gtpclkout1_pll_locked;
    wire            gtpclkout1_pll_reset;
    wire            gtpclkout1_bufio;
    wire            pll2_fb_out_i;

    //--------------------- Frame check/gen Module Signals --------------------
    wire            tile0_gtp0_refclk_i;

    wire            tile0_matchn0_i;
                                                           
    wire    [1:0]   tile0_txcharisk0_float_i;
    wire    [19:0]  tile0_txdata0_float_i;
    wire            tile0_track_data0_i;
    wire    [7:0]   tile0_error_count0_i;
    wire            tile0_frame_check0_reset_i;
    wire            tile0_inc_in0_i;
    wire            tile0_inc_out0_i;
    wire            tile0_matchn1_i;
    
    wire    [1:0]   tile0_txcharisk1_float_i;
    wire    [19:0]  tile0_txdata1_float_i;
    wire            tile0_track_data1_i;
    wire    [7:0]   tile0_error_count1_i;
    wire            tile0_frame_check1_reset_i;
    wire            tile0_inc_in1_i;
    wire            tile0_inc_out1_i;

    wire            reset_on_data_error_i;
    wire            track_data_out_i;
    
    //----------------------- Sync Module Signals -----------------------------
    wire            tile0_rx_sync_done0_i;
    wire            tile0_rx_sync_done1_i;
    wire            tile0_tx_sync_done0_i;
    wire            tile0_tx_sync_done1_i;
    
    //---------------------- AVB FMC and Clock Module Signals -----------------
    wire            clk_fmc_27M_in;      // 27 MHz clock from input buffer
    wire            clk_fmc_27M;        // clock from BUFG
    wire            gtp123_1_refclk;   // GTP reference clock after IBUFDS
    wire [1:0]	     fmc_tx1_red_led;
    wire [1:0]	     fmc_tx1_grn_led;
    wire [1:0]	     fmc_tx2_red_led;
    wire [1:0]	     fmc_tx2_grn_led;
    wire [1:0]	     fmc_tx3_red_led;
    wire [1:0]	     fmc_tx3_grn_led;
    wire [1:0]	     fmc_tx4_red_led;
    wire [1:0]	     fmc_tx4_grn_led;
    wire [1:0]	     fmc_sync_red_led;
    wire [1:0]	     fmc_sync_grn_led;
    wire		          fmc_sync_err;
    wire [2:0]	     fmc_sync_rate;
    wire		          fmc_sync_m;
    wire [10:0]	    fmc_sync_format;
    wire [7:0]	     fmc_sdi_eq_cd_n;
    wire [4:0]	     fmc_sdi_eq_cli;
    wire [7:0]	     fmc_sdi_drv_hd_sd;
    wire [7:0]	     fmc_sdi_drv_enable;
    wire [7:0]	     fmc_sdi_drv_fault_n;
    wire [7:0]	     fmc_fpga_rev;
    wire		          fmc_exp_brd_prsnt;
    wire		          fmc_Si5324_LOL;
    wire            tx1_slew;
    wire		          genlock_enable;
    wire [4:0]	     Si5324_in_fsel;
    wire [3:0]	     Si5324_bw_sel;
    wire [1:0]	     Si5324_clkin_sel;
    wire            cml_Si5324_LOL_B;
    wire            cml_Si5324_LOL_C;
    wire            cml_Si5324_clkin_sel;
    wire            glk_Si5324_DHOLD;
    wire  [1:0]     fmc_gtp_clk_sel;
    reg  [31:0]     clksel_period_counter;
    wire            clksel_reset;
    wire            clksel_halt;
    reg             refclk_rate;
    wire            rec_rate;
    wire            glk_Si5324_locked;
    wire            glk_sync_missing;
    wire            glk_sync_invalid;   
    wire            glk_local_lock;
    wire            glk_genlock;
    wire            Si5324_ckout1;
    wire            gtp_refclk_copy;
    wire [4:0]      cml_in_fsel;
    //--------------------- Chipscope Signals ---------------------------------
   (* KEEP = "TRUE" *)
    wire    [35:0]  mode_test_vio_control_i;
   (* KEEP = "TRUE" *)
    wire    [35:0]  shared_vio_control_i;
   (* KEEP = "TRUE" *)
    wire    [35:0]  data_vio_control_i;
   (* KEEP = "TRUE" *)
    wire    [35:0]  ila0_control;
   (* KEEP = "TRUE" *)
    wire    [35:0]  ila1_control;
    wire    [35:0]  null_vio_4_i;
    wire    [35:0]  null_vio_5_i;
    wire    [35:0]  null_vio_6_i;
    wire    [35:0]  null_vio_7_i;
    wire    [35:0]  null_vio_8_i;
    wire    [35:0]  null_vio_9_i;
    wire    [35:0]  null_vio_10_i;
    wire    [35:0]  null_vio_11_i;
    wire    [35:0]  null_vio_12_i;
    wire    [31:0]  shared_vio_in_i;
    wire    [31:0]  shared_vio_out_i;

    wire    [139:0] data_vio_in_i;
    wire    [139:0] data_vio_out_i;
    wire    [84:0]  ila0_dat;
    wire    [84:0]  ila1_dat;
            
    wire            gtpreset0_i;
    wire            gtpreset1_i;
    wire            gtp_reset0_in;
    wire            gtp_reset1_in;
    wire            user_tx1_reset_i;
    wire            user_rx_reset_i;
    
    reg  [3:0]      dip_switch_in;
    reg  [3:0]      dip_switch_sync;
    wire [3:0]      push_button_bus;
    reg  [9:0]      push_button_count;
    reg  [3:0]      push_button_1;
    reg  [3:0]      push_button_2;
    reg  [3:0]      push_button_3;
    reg  [3:0]      push_button_4;
    reg  [3:0]      push_button_5;
    reg  [3:0]      push_button_6;   
    reg  [3:0]      push_button_7;   
    reg  [3:0]      push_button_8;   
    reg  [3:0]      push_button_stretch;
    wire [3:0]      push_button_sync;
    // Tx signals:
    wire [1:0]      tile0_txbufstatus0;
    wire [1:0]      tile0_txbufstatus1;
    wire            rx_sd_mode;
    wire  [2:0]     tx_format;
    wire            tx_bitrate;
    reg             tx_bitrate_1;
    reg             tx_bitrate_2;
    reg             tx_bitrate_3;
    reg             tx_bitrate_4;
    reg             tx_bitrate_5;
    reg             tx_bitrate_6;
    reg             tx_bitrate_7;
    reg             tx_bitrate_8;
    reg             tx_rate_change;
    reg             tx_rate_change_del;
    wire [9:0]      tx_y_in;
    wire [9:0]      tx_c_in;
    wire [9:0]      tx_ds1a;
    wire [9:0]      tx_ds2a;
    wire [9:0]      tx_ds1b;
    wire [9:0]      tx_ds2b;                      
    wire            tx_eav;
    wire            tx_sav;
    wire [1:0]      tx_out_mode;
    wire            txreset1;
    wire            auto_txreset1;
    wire            tx1_fabric_reset;
    wire            auto_tx1_fabric_reset;
    wire [7:0]      gtp101_daddr;
    wire [15:0]     gtp101_di;
    wire [15:0]     gtp101_drpo;
    wire            gtp101_den;
    wire            gtp101_dwe;
    wire            gtp101_drdy;

    // Clock enables

(* equivalent_register_removal = "no" *)
(* KEEP = "TRUE" *)
reg [4:0]       tx_ce = 5'b11111;

wire            sd_ce;

(* equivalent_register_removal = "no" *)
(* KEEP = "TRUE" *)
reg  [10:0]     gen_sd_ce = 11'b00001000001;

wire            ce_mux;
    
//////////////////////////////////////////////////////////////////////////////
// SDI Receiver signals 
//////////////////////////////////////////////////////////////////////////////
localparam RX1_NUM_CE = 2;
localparam RX1_NUM_B_DRDY = 2;
(* KEEP = "TRUE" *)
wire [RX1_NUM_CE-1:0]       rx1_ce;
wire [RX1_NUM_B_DRDY-1:0]   rx1_lvlb_drdy;
// Clock signals

wire            rx1_gtp_recclk;             // recovered clock from GTX Rx #1
wire            rx1_recclk;                 // recovered clock buffered from DCM
wire            rx1_usrclk;                 // 297/148.5 MHz Rx user clock
wire            refclk_out;                 // 148.5 MHz reference clock from GTX
wire            refclk_out_buf;                 // 148.5 MHz reference clock from GTX
wire            rx1_clk_mux_sel;            // control BUFGMUX driving rx1_usrclk
wire  [3:0]     clkdiv_tx_clk;                 // output clocks produced by the clock divider

wire [19:0]     rx1_gtp_data;               // GTX RXDATA port
wire [1:0]      rx1_mode;                   // operating mode (3G/HD/SD)
wire            rx1_mode_HD;
wire            rx1_mode_SD;
wire            rx1_mode_3G;
wire            rx1_mode_locked;
wire            rx1_rate;
wire [3:0]      rx1_format;
wire            rx1_locked;
wire            rx1_crc_err;
wire [10:0]     rx1_ln_a;
wire [10:0]     rx1_ln_b;
wire [31:0]     rx1_a_vpid;
wire            rx1_a_vpid_valid;
wire [31:0]     rx1_b_vpid;
wire            rx1_b_vpid_valid;
wire [9:0]      rx1_a_y;
wire [9:0]      rx1_a_c;
wire            rx1_trs;
wire            rx1_eav;
wire            rx1_sav;
wire [9:0]      rx1_b_y;
wire [9:0]      rx1_b_c;
wire            rx1_crc_err2;
wire            rx1_level_b;
(* use_clock_enable = "yes" *)
reg             rx1_crc_err_ff = 1'b0;


// GTP reset 
wire [2:0]      gtp101_rxbufstatus1;
wire            gtp101_rxbufreset1;
wire            gtp101_resetdone1;
wire            gtp101_rxcdrreset1;
wire            rxreset1;
wire            auto_rxreset;

wire            gtp_reset_in;
wire            rx1_fabric_reset;
wire            auto_rx1_fabric_reset;

wire [23:0]     edh_errcnt;
wire            clr_errs;
wire            rx1_err;
wire            edh_err;
wire   [2:0]    std;
wire            sd_locked;
wire            locked;
reg             rx1_ce_1;
  
//////////////////////////////////////////////////////////////////////////////
// end SDI Receiver signals
//////////////////////////////////////////////////////////////////////////////
reg             gpioLED1;
reg             gpioLED2;


//**************************** Main Body of Code *******************************

//****************************************************************   
    // Temporary work around for GTP startup issue
    // Not needed for production silicon
    //
    wire clk_out_bufg;
    wire ringclk_i;

    BUFG mybufg                                
    (                                                 
        .I                          (ringclk_i),         
        .O                          (clk_out_bufg)        
    );                                                
    
    Osc9 Iring ( .out_clk(ringclk_i), .enable (1'b1));
    
    STARTUP_SPARTAN6     Istartup (
        .GSR	( 1'b0		), 
        .CFGCLK	(	), 
    	.CFGMCLK( ), 
    	.EOS	( ), 
	.CLK	(clk_out_bufg),
	.GTS	( 1'b0		),
	.KEYCLEARB(1'b1		));

    //---------------------Dedicated GTP Reference Clock Inputs ---------------
    // The dedicated reference clock inputs you selected in the GUI are implemented using
    // IBUFDS instances.
    // This network is the highest performace (lowest jitter) option for providing clocks
    // to the GTP transceivers.
    
IBUFDS #(
	.IOSTANDARD	("LVDS_25"),
	.DIFF_TERM	("TRUE"))
ibufds_gtp123_1(
	.I			 (PAD_gtp123_1_refclk_p),
	.IB			(PAD_gtp123_1_refclk_n),
	.O			(gtp123_1_refclk));

    //--------------------------------- User Clocks ---------------------------
    
    // The clock resources in this section were added based on userclk source selections on
    // the Latency, Buffering, and Clocking page of the GUI. A few notes about user clocks:
    // * The userclk and userclk2 for each GTP datapath (TX and RX) must be phase aligned to 
    //   avoid data errors in the fabric interface whenever the datapath is wider than 10 bits
    // * To minimize clock resources, you can share clocks between GTPs. GTPs using the same frequency
    //   or multiples of the same frequency can be accomadated using DCMs and PLLs. Use caution when
    //   using the recovered clock(gtpclkout[1]) as a clock source, however - these clocks can 
    //   typically only be shared if all the channels using the clock are receiving data 
    //   from TX channels that share a reference clock source with each other.
    //

    BUFIO2 #
    (
        .DIVIDE                         (1),
        .DIVIDE_BYPASS                  ("TRUE")
    )
    gtpclkout1_txpll_bufio2_i
    (
        .I                              (tile0_gtpclkout1_i[0]),
        .DIVCLK                         (gtpclkout1_bufio),
        .IOCLK                          (),
        .SERDESSTROBE                   ()
    );
    
//  27 MHz clock from  Si5324  CKOUT1 on FMC card
   IBUFGDS #(
      .DIFF_TERM("FALSE"),    // Differential Termination
      .IOSTANDARD("DEFAULT")  // Specify the input I/O standard
   ) IBUFGDS_Si5324_ckout1 (
      .O(Si5324_ckout1),  // Clock buffer output
      .I(PAD_Si5324_ckout1_p),  // Diff_p clock buffer input (connect directly to top-level port)
      .IB(PAD_Si5324_ckout1_n) // Diff_n clock buffer input (connect directly to top-level port)
   );
   
// 27 MHz clock out to CKIN2 on Si5324 B on clock module L  
   OBUFDS #(
      .IOSTANDARD("DEFAULT") // Specify the output I/O standard
   ) OBUFDS_inst (
      .O(PAD_Si5324_B_ckin2_p),     // Diff_p output (connect directly to top-level port)
      .OB(PAD_Si5324_B_ckin2_n),   // Diff_n output (connect directly to top-level port)
      .I(Si5324_ckout1)      // Buffer input 
   );

// Clock buffer for copy of GTP reference clock
   IBUFGDS #(
      .DIFF_TERM("FALSE"),    // Differential Termination
      .IOSTANDARD("DEFAULT") // Specifies the I/O standard for this buffer
   ) IBUFGDS_inst (
      .O(gtp_refclk_copy),  // Clock buffer output
      .I(PAD_fmc_xbar1_out2_p),  // Clock buffer input p
      .IB(PAD_fmc_xbar1_out2_n)  // Clock buffer input n
   );
 
//------------------------------------------------------------------------------
// Generate clock enables
//
// In SD-SDI mode, sd_ce is based on the rx1_ce but only half as long, since the 
// tx clock period is half or the rx clock period.  The sd_ce is timed to be coincident with 
// the second half of each rx1_ce pulse.
// sd_ce runs at an average of 27 MHz and is asserted at a 6/6/6/4 cadence 
// and sycnronized to the 3/3/3/2 cademce from the  RX DRU.
// tx_ce is always 1 for 3G-SDI and HD-SDI and equal to sd_ce for SD-SDI
//
//always @ (posedge txpipeclk) begin
//    rx1_ce_1 <= rx1_ce[0];
//end	 
//assign    sd_ce = rx1_ce[0] & !rx1_ce_1;
    
assign ce_mux = rx1_mode == 2'b01 ? sd_ce : 1'b1;

always @ (posedge txpipeclk)
    tx_ce <= {5 {ce_mux}};
           
always @ (posedge txpipeclk) begin
  tx_bitrate_1 <= tx_bitrate;
  tx_bitrate_2 <= tx_bitrate_1;
  tx_bitrate_3 <= tx_bitrate_2;
  tx_bitrate_4 <= tx_bitrate_3; 
  tx_bitrate_5 <= tx_bitrate_4; 
  tx_bitrate_6 <= tx_bitrate_5; 
  tx_bitrate_7 <= tx_bitrate_6; 
  tx_bitrate_8 <= tx_bitrate_7; 
  tx_rate_change  <= !(tx_bitrate_7 == tx_bitrate_2);
  tx_rate_change_del  <= !(tx_bitrate_8 == tx_bitrate_7);
end
   
assign  gtp_reset0_in = gtpreset0_i || tx_rate_change_del || cml_Si5324_LOL_B ||
        cml_Si5324_LOL_C;
assign  gtp_reset1_in = gtpreset1_i || tx_rate_change_del || cml_Si5324_LOL_B ||
        cml_Si5324_LOL_C;
// resetCDR after  a TX rate change
 assign tile0_rxcdrreset1_i  = auto_rxcdrreset;   
 assign rxreset1             = auto_rxreset;   
 assign tile0_rxbufreset1_i  = auto_rxbufreset;
 assign rx1_fabric_reset     = auto_rx1_fabric_reset;
 assign txreset1             = auto_txreset1;   
 assign tx1_fabric_reset     = auto_tx1_fabric_reset;
       
    //--------------------------- The GTP Wrapper -----------------------------
    
    // Use the instantiation template in the examples directory to add the GTP wrapper to your design.
    // In this example, the wrapper is wired up for basic operation with a frame generator and frame 
    // checker. The GTPs will reset, then attempt to align and transmit data. If channel bonding is 
    // enabled, bonding should occur after alignment.


    // Wire all PLLLKDET signals to the top level as output ports
//    assign TILE0_GTP0_PLLLKDET_OUT = tile0_plllkdet0_i;
//    assign TILE0_GTP1_PLLLKDET_OUT = tile0_plllkdet1_i;

 
    WIZ1_4_20B#
    (
        .WRAPPER_SIM_GTPRESET_SPEEDUP           (EXAMPLE_SIM_GTPRESET_SPEEDUP),
        .WRAPPER_SIMULATION                     (EXAMPLE_SIMULATION)
    )
    wiz1_4_20b_i
    (
 
 
 
 
 
 
        //_____________________________________________________________________
        //_____________________________________________________________________
        //TILE0  (X1_Y0)

        //---------------------- Loopback and Powerdown Ports ----------------------
        .TILE0_LOOPBACK0_IN             (tile0_loopback0_i),
        .TILE0_LOOPBACK1_IN             (tile0_loopback1_i),
        //------------------------------- PLL Ports --------------------------------
        .TILE0_CLK00_IN                 (gtp123_1_refclk),
        .TILE0_CLK01_IN                 (gtp123_1_refclk),
        .TILE0_GTPRESET0_IN             (tile0_gtpreset0_i),
        .TILE0_GTPRESET1_IN             (tile0_gtpreset1_i),
        .TILE0_PLLLKDET0_OUT            (tile0_plllkdet0_i),
        .TILE0_PLLLKDET1_OUT            (tile0_plllkdet1_i),
        .TILE0_RESETDONE0_OUT           (tile0_resetdone0_i),
        .TILE0_RESETDONE1_OUT           (tile0_resetdone1_i),
        //----------------- Receive Ports - RX Data Path interface -----------------
        .TILE0_RXDATA0_OUT              (tile0_rxdata0_i),
        .TILE0_RXDATA1_OUT              (rxdata1_20_gtp),
        .TILE0_RXRECCLK0_OUT            (tile0_rxrecclk0_i),
        .TILE0_RXRECCLK1_OUT            (tile0_rxrecclk1_i),
        .TILE0_RXRESET0_IN              (1'b0),
        .TILE0_RXRESET1_IN              (rxreset1),
        .TILE0_RXUSRCLK0_IN             (rxusrclk), 
        .TILE0_RXUSRCLK1_IN             (rxusrclk), 
        .TILE0_RXUSRCLK20_IN            (rxusrclk2),
        .TILE0_RXUSRCLK21_IN            (rxusrclk2),
        //----- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        .TILE0_RXCDRRESET0_IN           (tile0_rxcdrreset0_i),
        .TILE0_RXCDRRESET1_IN           (tile0_rxcdrreset1_i),
        .TILE0_RXN0_IN                  (),
        .TILE0_RXN1_IN                  (PAD_gtp123_1_rxn),
        .TILE0_RXP0_IN                  (),
        .TILE0_RXP1_IN                  (PAD_gtp123_1_rxp),
        //--------- Receive Ports - RX Elastic Buffer and Phase Alignment ----------
        .TILE0_RXBUFRESET0_IN           (tile0_rxbufreset0_i),
        .TILE0_RXBUFRESET1_IN           (tile0_rxbufreset1_i),
        .TILE0_RXBUFSTATUS0_OUT         (tile0_rxbufstatus0_i),
        .TILE0_RXBUFSTATUS1_OUT         (tile0_rxbufstatus1_i),
        //----------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
        .TILE0_DADDR_IN                 (gtp101_daddr),
        .TILE0_DCLK_IN                  (clk_fmc_27M),
        .TILE0_DEN_IN                   (gtp101_den),
        .TILE0_DI_IN                    (gtp101_di),
        .TILE0_DRDY_OUT                 (gtp101_drdy),
        .TILE0_DRPDO_OUT                (gtp101_drpo),
        .TILE0_DWE_IN                   (gtp101_dwe),
        //-------------------------- TX/RX Datapath Ports --------------------------
        .TILE0_GTPCLKOUT0_OUT           (tile0_gtpclkout0_i),
        .TILE0_GTPCLKOUT1_OUT           (tile0_gtpclkout1_i),
        //------------- Transmit Ports - TX Buffer and Phase Alignment -------------
        .TILE0_TXBUFSTATUS0_OUT         (tile0_txbufstatus0),
        .TILE0_TXBUFSTATUS1_OUT         (tile0_txbufstatus1),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .TILE0_TXDATA0_IN               (tile0_txdata0_i),
        .TILE0_TXDATA1_IN               (txdata1_20_gtp),
        .TILE0_TXOUTCLK0_OUT            (tile0_txoutclk0_i),
        .TILE0_TXOUTCLK1_OUT            (tile0_txoutclk1_i),     //  non-clock-specific routing
        .TILE0_TXRESET0_IN              (),
        .TILE0_TXRESET1_IN              (txreset1),
        .TILE0_TXUSRCLK0_IN             (txusrclk),
        .TILE0_TXUSRCLK1_IN             (txusrclk),
        .TILE0_TXUSRCLK20_IN            (txusrclk2),
        .TILE0_TXUSRCLK21_IN            (txusrclk2),
        //------------- Transmit Ports - TX Driver and OOB signalling --------------
        .TILE0_TXN0_OUT                 (),
        .TILE0_TXN1_OUT                 (PAD_gtp123_1_txn),
        .TILE0_TXP0_OUT                 (),
        .TILE0_TXP1_OUT                 (PAD_gtp123_1_txp)
    );
 
assign rx_sd_mode = rx1_mode == 2'b01;   // rx1_mode changes during lock-in 
assign gtpclkout1_pll_reset = !tile0_plllkdet0_i; 

// GTP  interface for genlock.
  gtp_interface_pll_gl  gtp_interface_pll_gl_i (
    .outclk          (tile0_txoutclk1_i),        
    .gtpoutclk       (gtpclkout1_bufio),      
    .pll_reset_in    (gtpclkout1_pll_reset), 
    .rx_data_in      (rxdata1_20_gtp),        
    .tx_data_in      (txdata1_20), 
    .sd_mode         (rx_sd_mode),
    .rx_ce           (rx1_ce[0]), 
    
    .rx_data_out     (rxdata1_20),
    .tx_data_out     (txdata1_20_gtp),
    .rx_usrclk       (rxusrclk),         
    .rx_usrclk2      (rxusrclk2),        
    .tx_usrclk       (txusrclk),         
    .tx_usrclk2      (txusrclk2),        
    .tx_pipe_clk     (txpipeclk),       
    .rx_pipe_clk     (rxpipeclk),       
    .pll_locked_out  (gtpclkout1_pll_locked),
    .pre_tx_ce       (sd_ce)
  );
  assign tx_format = rx1_format;
  assign tx_bitrate = rx1_rate;
  
    //------------------------ User Module Resets -----------------------------
    // All the User Modules i.e. FRAME_GEN, FRAME_CHECK and the sync modules
    // are held in reset till the RESETDONE goes high. 
    // The RESETDONE is registered a couple of times on USRCLK2 and connected 
    // to the reset of the modules
    
    always @(posedge txpipeclk or negedge tile0_resetdone0_i)
    begin
        if (!tile0_resetdone0_i )
        begin
            tile0_resetdone0_r    <=   1'b0;
            tile0_resetdone0_r2   <=   1'b0;
        end
        else
        begin
            tile0_resetdone0_r    <=   tile0_resetdone0_i;
            tile0_resetdone0_r2   <=   tile0_resetdone0_r;
        end
    end
    always @(posedge txpipeclk or negedge tile0_resetdone1_i)
    begin
        if (!tile0_resetdone1_i )
        begin
            tile0_resetdone1_r    <=    1'b0;
            tile0_resetdone1_r2   <=    1'b0;
        end
        else
        begin
            tile0_resetdone1_r    <=    tile0_resetdone1_i;
            tile0_resetdone1_r2   <=    tile0_resetdone1_r;
        end
    end

//
// GTP SDI control module for GTP tile 123
//




s6gtp_sdi_control #(
    .RATE_REFCLK_FREQ       (27000000))
GTPCTRL101 (
    .dclk               (clk_fmc_27M),
    .rate_refclk        (clk_fmc_27M),
    .rx0_usrclk         (1'b0),
    .rx1_usrclk         (rxpipeclk),
    .tx0_usrclk         (1'b0),
    .tx1_usrclk         (txpipeclk),   
    .drst               (1'b0),

    .rx0_mode           (2'b00),            
    .rx1_mode           (rx1_mode),
    .tx0_mode           (2'b00),
    .tx1_mode           (rx1_mode),

    .tx0_slew           (),
    .tx1_slew           (tx1_slew), 

    .rx0_rate           (),
    .rx1_rate           (rx1_rate),

    .gtp_reset0_in      (gtp_reset0_in),
    .gtp_reset1_in      (gtp_reset1_in),
    .clocks_stable      (1'b1),

    .rx0_pcs_reset      (1'b0),
    .rx0_cdr_reset      (1'b0),
    .rx0_fabric_reset   (),

    .rx1_pcs_reset      (1'b0),
    .rx1_cdr_reset      (1'b0),
    .rx1_fabric_reset   (auto_rx1_fabric_reset),

    .tx0_reset          (1'b0),
    .tx0_fabric_reset   (),
    .tx1_reset          (user_tx1_reset_i),
    .tx1_fabric_reset   (auto_tx1_fabric_reset),   
    
    .daddr              (gtp101_daddr),
    .den                (gtp101_den),
    .di                 (gtp101_di),
    .drpo               (gtp101_drpo),
    .drdy               (gtp101_drdy),
    .dwe                (gtp101_dwe),

    .txbufstatus0_b1    (tile0_txbufstatus0[1]),
    .txbufstatus1_b1    (tile0_txbufstatus1[1]),
    .rxbufstatus0_b2    (tile0_rxbufstatus0_i[2]),
    .rxbufstatus1_b2    (tile0_rxbufstatus1_i[2]),

    .gtpreset0          (tile0_gtpreset0_i),
    .gtpreset1          (tile0_gtpreset1_i),

    .resetdone0         (tile0_resetdone0_i),
    .rxreset0           (),
    .rxbufreset0        (),
    .rxcdrreset0        (),

    .resetdone1         (tile0_resetdone1_i),
    .rxreset1           (auto_rxreset),
    .rxbufreset1        (auto_rxbufreset),
    .rxcdrreset1        (auto_rxcdrreset),

    .txreset0           (),
    .txreset1           (auto_txreset1)
   );


always @ (posedge txpipeclk)
    if (tx_ce[0])
        dip_switch_in <= {GPIO_SWITCH_3, GPIO_SWITCH_2,
                          GPIO_SWITCH_1, GPIO_SWITCH_0};
always @ (posedge txpipeclk)
    if (tx_ce[0])
        dip_switch_sync   <= dip_switch_in;

// debounce push buttons
 assign push_button_bus = {GPIO_BUTTON3, GPIO_BUTTON2, GPIO_BUTTON1, GPIO_BUTTON0};
 always @ (posedge clk_fmc_27M)  begin
   push_button_count <= push_button_count +1;
   if (push_button_count == 0) begin
     push_button_1 <= push_button_bus;
     push_button_2 <= push_button_1;
     push_button_3 <= push_button_2;
     push_button_4 <= push_button_3;
     push_button_5 <= push_button_4;
     push_button_6 <= push_button_5;
     push_button_7 <= push_button_6;
     push_button_8 <= push_button_7;
     push_button_stretch <= push_button_8 &push_button_7 &push_button_6  
     & push_button_5 & push_button_4  & push_button_3 & push_button_2 
     & push_button_1& push_button_bus;
   end 
 end


  // synchroninze push button
  sync_one_shot sync_one_shot_pb0(	
    .clk   (txpipeclk),
    .rst   (1'b0),
    .ce    (tx_ce[0]),
    .in1   (push_button_stretch[0]),
    .out1  (push_button_sync[0])
  );
  
          
  // synchroninze push button
  sync_one_shot sync_one_shot_pb1(	
    .clk   (txpipeclk),
    .rst   (1'b0),
    .ce    (tx_ce[0]),
    .in1   (push_button_stretch[2]),
    .out1  (push_button_sync[2])
  );

  assign gtp_reset_in = CPU_RESET;      

//
// Generate the SMPTE 352M VPID byte 2 for 3G-SDI based on the tx_format and
// bit rate.
//
//
// Triple-rate SDI SMPTE 352M VPID packet insertion.
//
// In this demo, VPID packets are only inserted for 3G-SDI mode.
//
triple_sdi_vpid_insert VPIDINS (
    .clk            (txpipeclk),
    .ce             (tx_ce[1]),
    .din_rdy        (rx1_lvlb_drdy),
    .rst            (tx1_fabric_reset),
    .sdi_mode       (rx1_mode),
    .level          (rx1_level_b),             // always level A
    .enable         (0),   // enabled on 3G only
    .overwrite      (0),
    .byte1          (0),            // 1080-line 3G-SDI level A
    .byte2          (0),
    .byte3          (0),
    .byte4a         (0),
    .byte4b         (0),
    .ln_a           (0),
    .ln_b           (0),
    .line_f1        (0),
    .line_f2        (0),
    .line_f2_en     (1'b0),
    .a_y_in         (rx1_a_y),
    .a_c_in         (rx1_a_c),
    .b_y_in         (rx1_b_y),
    .b_c_in         (rx1_b_c),
    .ds1a_out       (tx_ds1a),
    .ds2a_out       (tx_ds2a),
    .ds1b_out       (tx_ds1b),
    .ds2b_out       (tx_ds2b),
    .eav_out        (tx_eav),
    .sav_out        (tx_sav),
    .out_mode       (tx_out_mode)
  );

// Triple-rate SDI Tx output module.
triple_sdi_tx_output_20b TXOUTPUT (
    .clk            (txpipeclk),
    .ce             (tx_ce[3:2]),
    .din_rdy        (rx1_lvlb_drdy),
    .rst            (tx1_fabric_reset),
    .mode           (tx_out_mode),
    .ds1a           (tx_ds1a),
    .ds2a           (tx_ds2a),
    .ds1b           (tx_ds1b),
    .ds2b           (tx_ds2b),
    .insert_crc     (1'b1),
    .insert_ln      (1'b1),
    .insert_edh     (1'b1),
    .ln_a           (rx1_ln_a),
    .ln_b           (rx1_ln_b),
    .eav            (tx_eav),
    .sav            (tx_sav),
    .txdata         (txdata1_20),
    .ce_align_err   (tx_ce_align_err));

//////////////////////////////////////////////////////////////////////////////
// SDI Receiver 
/////////////////////////////////////////////////////////////////////////////
//
// Triple-rate SDI RX data path
//

s6_sdi_rx_light_20b#(
    .NUM_SD_CE              (RX1_NUM_CE),
    .NUM_3G_DRDY            (RX1_NUM_B_DRDY))
SDIRX1 (
    .clk                    (rxpipeclk),
    .rst                    (rx1_fabric_reset),
    .data_in                (rxdata1_20),
    .frame_en               (1'b1),
    .mode                   (rx1_mode),
    .mode_HD                (rx1_mode_HD),
    .mode_SD                (rx1_mode_SD),
    .mode_3G                (rx1_mode_3G),
    .mode_locked            (rx1_mode_locked),
    .rx_locked              (rx1_locked),
    .t_format               (rx1_format),
    .level_b_3G             (rx1_level_b),
    .ce_sd                  (rx1_ce),
    .nsp                    (),
    .ln_a                   (rx1_ln_a),
    .a_vpid                 (rx1_a_vpid),
    .a_vpid_valid           (rx1_a_vpid_valid),
    .b_vpid                 (rx1_b_vpid),
    .b_vpid_valid           (rx1_b_vpid_valid),
    .crc_err_a              (rx1_crc_err),
    .ds1_a                  (rx1_a_y),
    .ds2_a                  (rx1_a_c),
    .eav                    (rx1_eav),
    .sav                    (rx1_sav),
    .trs                    (rx1_trs),
    .ln_b                   (rx1_ln_b),
    .dout_rdy_3G            (rx1_lvlb_drdy),
    .crc_err_b              (rx1_crc_err2),
    .ds1_b                  (rx1_b_y),
    .ds2_b                  (rx1_b_c));

//
// EDH processor to check for SD-SDI errors.
//
edh_processor EDH (
    .clk                    (rxpipeclk),
    .ce                     (rx1_ce[1]),
    .rst                    (rx1_mode != 2'b01),
    .vid_in                 (rx1_a_y),
    .reacquire              (1'b0),
    .en_sync_switch         (1'b1),
    .en_trs_blank           (1'b0),
    .anc_idh_local          (1'b0),
    .anc_ues_local          (1'b0),
    .ap_idh_local           (1'b0),
    .ff_idh_local           (1'b0),
    .errcnt_flg_en          (16'b0_00001_00001_00000),
    .clr_errcnt             (clr_errs),
    .receive_mode           (1'b1),                   
    .vid_out                (),
    .std                    (std),
    .std_locked             (sd_locked),
    .trs                    (),
    .field                  (),
    .v_blank                (),
    .h_blank                (),
    .horz_count             (),
    .vert_count             (),
    .sync_switch            (),
    .locked                 (),
    .eav_next               (),
    .sav_next               (),
    .xyz_word               (),
    .anc_next               (),
    .edh_next               (),
    .rx_ap_flags            (),
    .rx_ff_flags            (),
    .rx_anc_flags           (),
    .ap_flags               (),
    .ff_flags               (),
    .anc_flags              (),
    .packet_flags           (),
    .errcnt                 (edh_errcnt),
    .edh_packet             ());

assign edh_err = |edh_errcnt;

assign locked =  rx1_mode == 2'b01 ? sd_locked : rx1_locked;


//
// Capture any CRC error and cause an LED to light until cleared by pushing
// PB3 push button.
//
assign clr_errs = push_button_stretch[1] || !locked;

always @ (posedge rxpipeclk or posedge clr_errs)
    if (clr_errs)
        rx1_crc_err_ff <= 1'b0;
    else if (rx1_ce[0] & (rx1_crc_err | rx1_crc_err2))
        rx1_crc_err_ff <= 1'b1;

assign rx1_err = rx1_mode_SD ? edh_err : rx1_crc_err_ff;

//////////////////////////////////////////////////////////////////////////////
// end SDI Receiver code for FMC
//////////////////////////////////////////////////////////////////////////////

//  GPIO LED Controls
  always @ (posedge rxpipeclk) begin
     gpioLED1 <= rx1_mode[0];
     gpioLED2 <= rx1_mode[1];
  end
     
  assign rec_rate = rx_sd_mode? 0 : rx1_rate;
  assign GPIO_LED_0 = rec_rate;
  assign GPIO_LED_1 = gpioLED1;
  assign GPIO_LED_2 = gpioLED2;
  assign GPIO_LED_3 = rx1_err;
  assign FPGA_AWAKE = locked;


//////////////////////////////////////////////////////////////////////////////
//              AVB FMC card interface
//////////////////////////////////////////////////////////////////////////////


//------------------------------------------------------------------------------
// AVB FMC card controller
//
IBUFG #(
	.IOSTANDARD	("LVCMOS25"),
	.IBUF_DELAY_VALUE	("0"))
HB06IBUF (
	.I			(PAD_clk_fmc_27M_in),
	.O			(clk_fmc_27M_in));

AUTOBUF #(
	.BUFFER_TYPE	("BUFG"))
BUF27M (
	.I			(clk_fmc_27M_in),
	.O			(clk_fmc_27M));

main_avb_control main_avb_control (
	.clk				(clk_fmc_27M),
	.rst				(1'b0),

// SPI interface to AVB FMC card
	.sck				(PAD_main_spi_sck ),
	.mosi			(PAD_main_spi_mosi),
	.miso			(PAD_main_spi_miso),
	.ss					(PAD_main_spi_ss  ),

// General status signals
	.fpga_rev			(fmc_fpga_rev),
	.exp_brd_prsnt		(fmc_exp_brd_prsnt),
	.board_options		(),

// Clock XBAR control signals
//
// For XBAR 1, each output can be driven by any of the four inputs as follows:
// 		00 selects clock from Si5324
//		01 selects clock module L CLK OUT 1
//		10 selects clock module L CLK OUT 2
//		11 selects OUT 0 of XBAR 3
// 
	.xbar1_out0_sel		(fmc_gtp_clk_sel),  // GTP refclk on FMC connector
	.xbar1_out1_sel		(2'b00),
	.xbar1_out2_sel 	(2'b00),
	.xbar1_out3_sel		(2'b00),

//
// For XBAR 2, each output can be driven by any of the four inuts as follows:
//		00 selects OUT 3 of XBAR 3
//		01 selects clock module H CLK OUT 1
//		10 selects clock module H CLK OUT 2
//		11 selects clock module H CLK OUT 3
//
	.xbar2_out0_sel		(2'b00),
	.xbar2_out1_sel		(2'b00),
	.xbar2_out2_sel		(2'b00),
	.xbar2_out3_sel		(2'b00),

//
// For XBAR 3, each output can be driven by any of the four inputs as follows:
//		00 selects FMC HA19
//		01 selects FMC LA22
//		10 selects FMC DP0 (LPC compatible MGT)
//		11 selects FMC DP1 (HPC compatible MGT)
//
	.xbar3_out0_sel		(2'b00),                                   
	.xbar3_out1_sel		(2'b10),
	.xbar3_out2_sel		(2'b10),
	.xbar3_out3_sel		(2'b01),

// Si5324 Status & Control
//
// The Si5324_clkin_sel port controls the clock input selection for the Si5324.
// There are three possible clock sources: 27 MHz XO, FPGA signal, and the HSYNC
// signal from the clock separator. If the HSYNC signal is chosen, the device can be
// put into auto frequency select mode where the controller automatically determines
// the external HSYNC frequency and selects the proper frequency synthesis
// settings to produce 27 MHz out of the Si5324. If Si5324_clkin_sel is anything
// other than 01 (auto HSYNC mode), the frequency synthesis of the Si5324 is
// controlled by the Si5324_in_fsel and Si5324_out_fsel ports as follows:
//
//		Si5324_in_fsel[4:0] select the input frequency:
//			0x00: 480i (NTSC) HSYNC
//			0x01: 480p HSYNC
//			0x02: 576i (PAL) HSYNC
//			0x03: 576p HSYNC
//			0x04: 720p 24 Hz HSYNC
//			0x05: 720p 23.98 Hz HSYNC
//			0x06: 720p 25 Hz HSYNC
//			0x07: 720p 30 Hz HSYNC
//			0x08: 720p 29.97 Hz HSYNC
//			0x09: 720p 50 Hz HSYNC
//			0x0A: 720p 60 Hz HSYNC
//			0x0B: 720p 59.94 Hz HSYNC
//			0x0C: 1080i 50 Hz HSYNC
//			0x0D: 1080i 60 Hz HSYNC
//			0x0E: 1080i 59.94 Hz HSYNC
//			0x0F: 1080p 24 Hz HSYNC
//			0x10: 1080p 23.98 Hz HSYNC
//			0x11: 1080p 25 Hz HSYNC
//			0x12: 1080p 30 Hz HSYNC
//			0x13: 1080p 29.97 Hz HSYNC
//			0x14: 1080p 50 Hz HSYNC
//			0x15: 1080p 60 Hz HSYNC
//			0x16: 1080p 59.94 Hz HSYNC
//			0x17: 27 MHz
//			0x18: 74.25 MHz
//			0x19: 74.25/1.001 MHz
//			0x1A: 148.5 MHz
//			0x1B: 148.5/1.001 MHz
//
//      Si5324_out_fsel[3:0] select the output frequency:
//          0x0: 27 MHz
//          0x1: 74.25 MHz
//          0x2: 74.25/1.001 MHz
//          0x3: 148.5 MHz
//          0x4: 148.5/1.001 MHz
//          0x5: 24.576 MHz
//          0x6: 148.5/1.0005 MHz
//          0x7: Invalid
//          0x8: 297 MHz
//          0x9: 297/1.001 MHz
//
// Note that any HSYNC frequency can only be converted to 27 MHz. Choosing any
// output frequency except 27 MHz when the input selection is 0x00 through 0x16
// will result in an error. Any input frequency selected by 0x17 through 0x1B
// can be converted to any output frequency, with the exception that the
// 74.25/1.001 and 148.5/1.001 MHz input frequencies can't be converted to 
// 24.576 MHz.
//
// For custom frequency synthesis, use the Si5324 register peek/poke facility
// to modify individual registers on a custom basis.
//
	.Si5324_reset		(1'b0),				// 1 resets Si5324
	.Si5324_clkin_sel	(Si5324_clkin_sel),	// Control input clock source selection for Si5324
											// 00=27 MHz, 01=sync sep HSYNC (auto fsel mode)
											// 10=FMC LA29, 11=sync sep HSYNC (manual fsel mode)
	.Si5324_out_fsel	(4'h0),		// selects the output frequency 0 = 27 MHz
	.Si5324_in_fsel		(Si5324_in_fsel), 	// selects the input frequency
 .Si5324_bw_sel   (Si5324_bw_sel),   // bandwidth select
 .Si5324_DHOLD (0),      // 1 puts the Si5324 in digital hold mode
	.Si5324_FOS2		(),					// 1=frequency offset alarm for CKIN2
	.Si5324_FOS1		(),					// 1=frequency offset alram for CKIN1
	.Si5324_LOL			(fmc_Si5324_LOL),	// 0=PLL locked, 1=PLL unlocked

// Si5324 register peek/poke control
	.Si5324_reg_adr		  (8'h00),			// Si5324 peek/poke register address (8-bit)
	.Si5324_reg_wr_dat	(8'h00),			// Si5324 peek/poke register write data	(8-bi)
	.Si5324_reg_rd_dat	(), 				// Si5324 peek/poke register read data (8-bit)
	.Si5324_reg_wr		(1'b0),				// Si5324 poke request, assert High for one clk
	.Si5324_reg_rd		(1'b0),				// Si5324 peek request, assert High for one clk
	.Si5324_reg_rdy		(),					// Si5324 peek/poke cycle done when 1
	.Si5324_error		(),					// Si5324 peek/poke error when 1 (transfer was NACKed on I2C bus)

//
// These ports are associated with the LMH1981 sync separator.  Note that the
// actual sync signals are available directly to the FPGA via FMC signals. The
// sync_video_frame value is a count of the number of lines in a field or frame
// as captured directly by the LMH1981. The sync_m and sync_frame_rate indicate
// the frame rate of the video signal as shown below. 
//
//  	sync_frame_rate		Frame Rate		sync_m
//				000			23.98 Hz			1
//				001			24 Hz				0
//				010			25 Hz				0
//				011			29.97 Hz			1
//				100			30 Hz				0
//				101			50 Hz				0
//				110			59.94 Hz			1
//				111			60 Hz				0
//
	.sync_video_fmt		(fmc_sync_format),	// count of lines per field/frame (11-bit)
	.sync_updating		(),					// sync_video_frame only valid when this port is 0
	.sync_frame_rate	(fmc_sync_rate),	// frame rate indicator (3-bit)
	.sync_m				(fmc_sync_m),		// 1 = frame rate is 1000/1001
	.sync_err			(fmc_sync_err),		// 1 = error detected frame rate

//
// LED control ports
//

// The eight two-color LEDs associated with the SDI RX connectors are controlled
// by 2 bits each as follows:
//		00 = off
//		01 = green
//		10 = red
//		11 = controlled by cable EQ CD signal (green when carrier detected, else red)
//
	.sdi_rx1_led		(2'b11),			// controls the SDI RX1 LED
	.sdi_rx2_led		(2'b00),			// controls the SDI RX2 LED
	.sdi_rx3_led		(2'b00),			// controls the SDI RX3 LED
	.sdi_rx4_led		(2'b00),			// controls the SDI RX4 LED
	.sdi_rx5_led		(2'b00),			// controls the SDI RX5 LED
	.sdi_rx6_led		(2'b00),			// controls the SDI RX6 LED
	.sdi_rx7_led		(2'b00),			// controls the SDI RX7 LED
	.sdi_rx8_led		(2'b00),			// controls the SDI RX8 LED

// All other LEDs have separate 2-bit control ports for both the red and green LEDs
// so that the red and green sides of the LED are independently controlled like this:
//		00 = off
//		01 = on
//		10 = flash slowly
//		11 = flash quickly
//
	.sdi_tx1_red_led	(fmc_tx1_red_led),	// controls the SDI TX1 red LED
	.sdi_tx1_grn_led	(2'b01),	// controls the SDI TX1	green LED
	.sdi_tx2_red_led	(fmc_tx2_red_led),	// controls the SDI TX2 red LED
	.sdi_tx2_grn_led	(2'b01),	// controls the SDI TX2	green LED
	.sdi_tx3_red_led	(fmc_tx3_red_led), 	// controls the SDI TX3 red LED
	.sdi_tx3_grn_led	(fmc_tx3_grn_led), 	// controls the SDI TX3	green LED
	.sdi_tx4_red_led	(fmc_tx4_red_led),	// controls the SDI TX4 red LED
	.sdi_tx4_grn_led	(fmc_tx4_grn_led),	// controls the SDI TX4	green LED
	.sdi_tx5_red_led	(2'b00),			// controls the SDI TX5 red LED
	.sdi_tx5_grn_led	(2'b00),			// controls the SDI TX5	green LED
	.sdi_tx6_red_led	(2'b00),			// controls the SDI TX6 red LED
	.sdi_tx6_grn_led	(2'b00),			// controls the SDI TX6	green LED
	.sdi_tx7_red_led	(2'b00),			// controls the SDI TX7 red LED
	.sdi_tx7_grn_led	(2'b00),			// controls the SDI TX7	green LED
	.sdi_tx8_red_led	(2'b00),			// controls the SDI TX8 red LED
	.sdi_tx8_grn_led	(2'b00),			// controls the SDI TX8	green LED

	.aes_rx1_red_led	(rx1_format[2]),			// controls the AES3 RX1 red LED
	.aes_rx1_grn_led	(rx1_format[2]),			// controls the AES3 RX1 green LED
	.aes_rx2_red_led	(rx1_format[3]),			// controls the AES3 RX2 red LED
	.aes_rx2_grn_led	(rx1_format[3]),			// controls the AES3 RX2 green LED
	.aes_tx1_red_led	(rx1_format[0]),  	// controls the AES3 TX1 red LED
	.aes_tx1_grn_led	(rx1_format[0]),			// controls the AES3 TX1 green LED
	.aes_tx2_red_led	(rx1_format[1]),			// controls the AES3 TX2 red LED
	.aes_tx2_grn_led	(rx1_format[1]),			// controls the AES3 TX2 green LED
	.madi_rx_red_led	(2'b00),			// controls the MADI RX red LED
	.madi_rx_grn_led	(2'b00),			// controls the MADI RX green LED
	.madi_tx_red_led	(2'b00),			// controls the MADI TX red LED
	.madi_tx_grn_led	(2'b00),			// controls the MADI TX green LED
	.sync_red_led		  (fmc_sync_red_led),  	// controls the external sync red LED
	.sync_grn_led		  (fmc_sync_grn_led),	// controls the external sync green LED
	
// SDI Cable EQ control & status
//
// In the first two ports, there is one bit for each possible cable EQ device with
// bit 0 for SDI RX1 and bit 7 for SDI RX8.
//
	.sdi_eq_cd_n		(fmc_sdi_eq_cd_n),	// carrier detects from cable drivers, asserted low
	.sdi_eq_ext_3G_reach(8'b00000000),		// Enable bits for extended 3G reach mode, 1=enable, 0=disable
	.sdi_eq_select		(3'b000),			// selects which EQ's status signals drive port below
	.sdi_eq_cli			(fmc_sdi_eq_cli),  	// cable length indicator

// SDI Cable Driver control & status
//
// For these ports, there is one bit for each possible cable driver device with
// bit 0 for SDI TX1 and bit 7 for SDI TX8.
//
	.sdi_drv_hd_sd		(fmc_sdi_drv_hd_sd),// Sets slew rate of each cable driver, 1=SD, 0=HD/3G
	.sdi_drv_enable		(fmc_sdi_drv_enable),// 1 enables the driver, 0 powers driver down
	.sdi_drv_fault_n	(fmc_sdi_drv_fault_n)// 1 = normal operation, 0 = fault
);
assign fmc_sdi_drv_hd_sd = {6'h00, {2{tx1_slew}}};
assign fmc_sdi_drv_enable = 8'h0F;

assign fmc_sync_grn_led = {1'b0, ~fmc_sync_err};
assign fmc_sync_red_led = {1'b0, fmc_sync_err};

//////////////////////////////////////////////////////////////////////////////
//            Clock Module Control
//////////////////////////////////////////////////////////////////////////////

assign  PAD_si5324_A_rst_n = 1;
assign  PAD_si5324_B_rst_n = 1;
assign  PAD_si5324_C_rst_n = 1;
assign  cml_in_fsel =  glk_Si5324_locked ? 5'h17: 5'h00;


//Control clock select for GTP reference clock:
assign clksel_reset = clksel_period_counter == CLKSEL_PERIOD_MAXCNT;
assign clksel_halt  = locked && !(rx1_mode == 2'b01 && refclk_rate);
assign fmc_gtp_clk_sel = refclk_rate? 2 : 1;   // 2= C chip,148.35 MHz; 1= B chip, 148.5 MHz 
  always @ (posedge clk_fmc_27M) 
    if (clksel_reset) begin
      clksel_period_counter <= 0;
      refclk_rate = !refclk_rate;
    end    
    else if (!clksel_halt)                       // stop toggle when locked
      clksel_period_counter <= clksel_period_counter + 1;
   

cm_avb_control cm_avb_control_i 
( 
// Master clock
    .clk      (clk_fmc_27M),         // 27 MHz clock from the AVB FMC card
    .rst      (0),
    .ga       (0),         // must be 0 for CML and 1 for CMH

// SPI signals
    .sck       (PAD_cml_spi_sck),       // SPI SCK
    .mosi      (PAD_cml_spi_mosi),      // master-out slave-in serial data
    .miso      (PAD_cml_spi_miso),      // master-in slave-out serial data
    .ss        (PAD_cml_spi_ss),        // slave select -- asserted low
    
// Module identification
    .module_type       (), // Clock module type
    .module_rev        (), // Clock module revision
    .module_type_valid (), // 1 = module type & rev have been read
    .module_type_error (), // 1 = error reading module type & rev

// General control
    .clkin5_src_sel    (0), // Clock module CLKIN 5 source
                                                    // 0 = 27 MHz, 1 = from FMC connector
// GPIO direction signals
// These control the direction of signals between the FPGA on the AVB FMC card
// and the clock module. A value of 0 indicates an FPGA output to the clock
// module. A value of 1 indicates an input to the FPGA from the clock module.
//
    .gpio_dir_0       (),   // GPIO signals [7:0]
    .gpio_dir_1       (),   // GPIO signals [15:8]
    .gpio_dir_2       (),   // GPIO signals [23:16]

// General purpose output values
// These control the of the GPIO signals when they are outputs from the FPGA
// on the AVB FMC card to the clock module.
    .gp_out_value_0   (),   // GPIO signals [7:0]
    .gp_out_value_1   (),   // GPIO signals [15:8]
    .gp_out_value_2   (),   // GPIO signals [23:16]

// General purpose input values
// The ports reflect the values of the GPIO signals when they are inputs to the
// FPGA on the AVB FMC card from the clock clock module.
    .gp_in_value_0    (),   // GPIO signals [7:0]
    .gp_in_value_1    (),   // GPIO signals [15:8]
    .gp_in_value_2    (),   // GPIO signals [23_16]
    .gp_in            (),   // GPIN signals [3:0]

// I2C bus register peek/poke control
// These ports provide peek/poke capability to devices connected to the
// I2C bus on the clock module. To write a value to a device register, set the
// the slave address, register address, and data to be written and then pulse
// i2c_reg_wr high for one cycle of the 27 MHz clock. The i2c_reg_rdy signal
// will go low on the rising edge of the clock when i2c_reg_wr is high and
// will stay low until the write is completed. To read a register, setup the
// slave address and register address then pulse i2c_reg_rd high for one cycle
// of the clock. Again, i2c_reg_rdy will go low until the read cycle is completed.
// When i2c_reg_rdy goes high, the data read from the register will be present
// on i2c_reg_dat_rd.
//
    .i2c_slave_adr    (),   // [7:0] I2C device slave address
    .i2c_reg_adr      (),   // [7:0] I2C device register address
    .i2c_reg_dat_wr   (),   // [7:0] Data to be written to device
    .i2c_reg_wr       (),   //       Write request
    .i2c_reg_rd       (),   //       Read request
    .i2c_reg_dat_rd   (),   // [7:0] Data read from I2C device
    .i2c_reg_rdy      (),   //       1 = peek/poke completed      
    .i2c_reg_error    (),   //       1 = NACK occurred during I2C peek/poke

// Si5324 module signals
//
// These ports are only valid if the Si5324 clock module is installed on the
// AVB FMC card. There are 3 identical sets of ports, one set for each of the
// three Si5324 parts on the clock module. The out_fsel and in_fsel ports
// set the predefined frequency synthesis options as follows:
//
//      Si5324_X_in_fsel[4:0] select the input frequency:
//          0x00: 480i (NTSC) HSYNC
//          0x01: 480p HSYNC
//          0x02: 576i (PAL) HSYNC
//          0x03: 576p HSYNC
//          0x04: 720p 24 Hz HSYNC
//          0x05: 720p 23.98 Hz HSYNC
//          0x06: 720p 25 Hz HSYNC
//          0x07: 720p 30 Hz HSYNC
//          0x08: 720p 29.97 Hz HSYNC
//          0x09: 720p 50 Hz HSYNC
//          0x0A: 720p 60 Hz HSYNC
//          0x0B: 720p 59.94 Hz HSYNC
//          0x0C: 1080i 50 Hz HSYNC
//          0x0D: 1080i 60 Hz HSYNC
//          0x0E: 1080i 59.94 Hz HSYNC
//          0x0F: 1080p 24 Hz HSYNC
//          0x10: 1080p 23.98 Hz HSYNC
//          0x11: 1080p 25 Hz HSYNC
//          0x12: 1080p 30 Hz HSYNC
//          0x13: 1080p 29.97 Hz HSYNC
//          0x14: 1080p 50 Hz HSYNC
//          0x15: 1080p 60 Hz HSYNC
//          0x16: 1080p 59.94 Hz HSYNC
//          0x17: 27 MHz
//          0x18: 74.25 MHz
//          0x19: 74.25/1.001 MHz
//          0x1A: 148.5 MHz
//          0x1B: 148.5/1.001 MHz
//
//      Si5324_X_out_fsel[2:0] select the output frequency:
//          0x0: 27 MHz
//          0x1: 74.25 MHz
//          0x2: 74.25/1.001 MHz
//          0x3: 148.5 MHz
//          0x4: 148.5/1.001 MHz
//          0x5: 24.576 MHz
//
// Note that any HSYNC frequency can only be converted to 27 MHz. Choosing any
// output frequency except 27 MHz when the input selection is 0x00 through 0x16
// will result in an error. Any input frequency selected by 0x17 through 0x1B
// can be converted to any output frequency, with the exception that the
// 74.25/1.001 and 148.5/1.001 MHz input frequencies can't be converted to 
// 24.576 MHz.
//
    .Si5324_A_clkin_sel  (),   // Selects input clock, 0=CKIN1, 1=CKIN2
    .Si5324_A_out_fsel   (),   // Selects the output frequency
    .Si5324_A_in_fsel    (),   // Selects the input frequency
	   .Si5324_A_DHOLD			   (),   // 1 puts device in digital hold mode
    .Si5324_A_FOS2       (),   // 1=frequency offset alarm for CKIN2
    .Si5324_A_FOS1       (),   // 1=frequency offset alarm for CKIN1
    .Si5324_A_LOL        (),   // 0=PLL locked, 1=PLL unlocked

    .Si5324_B_clkin_sel  (1'b1),   // Selects input clock, 0=CKIN1, 1=CKIN2
    .Si5324_B_out_fsel   (3'h3),   // Selects the output frequency  3= 148.5 MHz
    .Si5324_B_in_fsel    (cml_in_fsel),   // Selects the input frequency  0x17= 27 MHz
   	.Si5324_B_bw_sel	    (4'd4),//(4'd10),
	   .Si5324_B_DHOLD			   (glk_Si5324_DHOLD),   // 1 puts device in digital hold mode
    .Si5324_B_FOS2       (),   // 1=frequency offset alarm for CKIN2
    .Si5324_B_FOS1       (),   // 1=frequency offset alarm for CKIN1
    .Si5324_B_LOL        (cml_Si5324_LOL_B),   // 0=PLL locked, 1=PLL unlocked

    .Si5324_C_clkin_sel  (1'b0),   // Selects input clock, 0=CKIN1, 1=CKIN2
    .Si5324_C_out_fsel   (3'h4),   // Selects the output frequency  4= 148.35 MHz
    .Si5324_C_in_fsel    (cml_in_fsel),   // Selects the input frequency  0x17= 27 MHz
	   .Si5324_C_bw_sel	    (4'd2),//(4'd7),
	   .Si5324_C_DHOLD			   (glk_Si5324_DHOLD),   // 1 puts device in digital hold mode
    .Si5324_C_FOS2       (),   // 1=frequency offset alarm for CKIN2
    .Si5324_C_FOS1       (),   // 1=frequency offset alarm for CKIN1
    .Si5324_C_LOL        (cml_Si5324_LOL_C)   // 0=PLL locked, 1=PLL unlocked
);

//
// Genlock Control.
//
assign genlock_enable = 1;
assign Si5324_clkin_sel = {cml_Si5324_clkin_sel, cml_Si5324_clkin_sel};
genlock_control  
#(
     .GENLOCK_SI5324_BW_SEL (4'b0001)
)
genlock_control_i(
   .clk               (clk_fmc_27M),         
   .hsync_in          (PAD_fmc_hsync_in),         
   .genlock_enable    (genlock_enable),         
   .default_code      (5'b10111),  // 23 = 27 MHz input     
   .default_bw_sel    (4'd6),      // 10=6Hz, 9=14Hz, 8=28Hz, 7=56Hz, 6=113Hz, 5= 230Hz, 4=474Hz   
   .sync_video_fmt    (fmc_sync_format),         
   .sync_frame_rate   (fmc_sync_rate),         
   .Si5324_LOL_0      (fmc_Si5324_LOL),         
   .Si5324_LOL_1      (cml_Si5324_LOL_B),         
   .Si5324_LOL_2      (cml_Si5324_LOL_C),         

   .Si5324_clk_sel    (cml_Si5324_clkin_sel),   
   .in_freq_sel       (Si5324_in_fsel),     
   .Si5324_bw_sel     (Si5324_bw_sel),     
   .Si5324_locked     (glk_Si5324_locked),  
   .sync_missing      (glk_sync_missing),         
   .sync_invalid      (glk_sync_invalid),         
   .local_lock        (glk_local_lock),  
   .genlock           (glk_genlock),  
   .Si5324_DHOLD      (glk_Si5324_DHOLD)   
);

endmodule

