//------------------------------------------------------------------------------ 
// Copyright (c) 2008 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: hdsdi_crc2.v,rcs $
//  /   /        Date Last Modified:  $Date: 2008-11-14 09:15:40-07 $
// /___/   /\    Date Created: June 8, 2008
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: hdsdi_crc2.v,rcs $
// Revision 1.1  2008-11-14 09:15:40-07  jsnow
// Added register initializers.
//
// Revision 1.0  2008-04-21 15:20:03-06  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module does a 18-bit CRC calculation.

The calculation is the SMPTE 292M defined CRC18 calculation with a polynomial of 
x^18 + x^5 + x^4 + 1. The function considers the LSB of the video data as the 
first bit shifted into the CRC generator, although the implementation given here
is a fully parallel CRC, calculating all 18 CRC bits from the 10-bit video data
in one clock cycle.  

The clr input must be asserted coincident with the first input data word of
a new CRC calculation. The clr input forces the old CRC value stored in the
module's crc_reg to be discarded and a new calculation begins as if the old CRC
value had been cleared to zero.

This module is the same as hdsdi_crc, but adds an enable input in addition to
the clock enable.
*/


module  hdsdi_crc2 (
    input  wire         clk,    // clock input
    input  wire         ce,     // clock enable
    input  wire         en,
    input  wire         rst,    // async reset input
    input  wire         clr,    // forces the old CRC value to zero to start new calculation
    input  wire [9:0]   d,      // input data word
    output wire [17:0]  crc_out // new calculated CRC value
);

//-----------------------------------------------------------------------------
// Signal definitions
//
wire                    x10;
wire                    x9;
wire                    x8;
wire                    x7;
wire                    x6;
wire                    x5;
wire                    x4;
wire                    x3;
wire                    x2;
wire                    x1;
wire    [17:0]          newcrc;     // input to CRC register            
wire    [17:0]          crc;        // output of crc_reg, unless clr is asserted
reg     [17:0]          crc_reg = 0;// internal CRC register


//
// The previous CRC value is represented by the variable crc. This value is
// combined with the new data word to form the new CRC value. Normally, crc is
// equal to the contents of the crc_reg. However, if the clr input is asserted,
// the crc value is set to all zeros.
//
assign crc = clr ? 0 : crc_reg;

//
// The x variables are intermediate terms used in the new CRC calculation.
//                             
assign x10 = d[9] ^ crc[9];
assign x9  = d[8] ^ crc[8];
assign x8  = d[7] ^ crc[7];
assign x7  = d[6] ^ crc[6];
assign x6  = d[5] ^ crc[5];
assign x5  = d[4] ^ crc[4];
assign x4  = d[3] ^ crc[3];
assign x3  = d[2] ^ crc[2];
assign x2  = d[1] ^ crc[1];
assign x1  = d[0] ^ crc[0];

//
// These assignments generate the new CRC value.
//
assign newcrc[0]  = crc[10];
assign newcrc[1]  = crc[11];
assign newcrc[2]  = crc[12];
assign newcrc[3]  = x1  ^ crc[13];
assign newcrc[4]  = x2  ^ x1 ^ crc[14];
assign newcrc[5]  = x3  ^ x2 ^ crc[15];
assign newcrc[6]  = x4  ^ x3 ^ crc[16];
assign newcrc[7]  = x5  ^ x4 ^ crc[17];
assign newcrc[8]  = x6  ^ x5 ^ x1;
assign newcrc[9]  = x7  ^ x6 ^ x2;
assign newcrc[10] = x8  ^ x7 ^ x3;
assign newcrc[11] = x9  ^ x8 ^ x4;
assign newcrc[12] = x10 ^ x9 ^ x5;
assign newcrc[13] = x10 ^ x6;
assign newcrc[14] = x7;
assign newcrc[15] = x8;
assign newcrc[16] = x9;
assign newcrc[17] = x10;

//
// This is the crc_reg. On each clock cycle when ce is asserted, it loads the
// newcrc value. The module's crc_out vector is always assigned to the contents
// of the crc_reg.
//
always @ (posedge clk or posedge rst)
    if (rst)
        crc_reg <= 0;
    else if (ce)
        if (en)
            crc_reg <= newcrc;

assign crc_out = crc_reg;

endmodule
