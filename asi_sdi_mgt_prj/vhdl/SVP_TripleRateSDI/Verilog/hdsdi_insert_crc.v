//------------------------------------------------------------------------------
// hdsdi_insert_crc.v
//
// Inserts CRC values into the HD-SDI video stream.
//
//
//
//                  Author: John F. Snow
//                  Staff Applications Engineer
//
//                  Video Applications
//                  Advanced Products Group
//                  Xilinx, Inc.
//
//                  Copyright (c) 2003 Xilinx, Inc.
//                  All rights reserved
//
//                  RESTRICTED RIGHTS LEGEND
//
//      This software has not been published by the author, and 
//      has been disclosed to others for the purpose of enhancing 
//      and promoting design productivity in Xilinx products.
//
//      Therefore use, duplication or disclosure, now and in the 
//      future should give consideration to the productivity 
//      enhancements afforded the user of this code by the author's 
//      efforts.  Thank you for using our products !
//
// Disclaimer:  THESE DESIGNS ARE PROVIDED "AS IS" WITH NO WARRANTY 
//              WHATSOEVER AND XILINX SPECIFICALLY DISCLAIMS ANY 
//              IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
//              A PARTICULAR PURPOSE, OR AGAINST INFRINGEMENT.
//
// $Log: hdsdi_insert_crc.v,rcs $
// Revision 1.0  2004-05-21 13:25:56-06  jsnow
// Initial revision
//
//
// Other modules instanced in this design:
//          none
//
//------------------------------------------------------------------------------
/*
This formats the 18-bit CRC values for each channel into two 10-bit video words
and inserts them into the appropriate places immediately after the line number
words in the EAV.

An 18-bit CRC value is formatted into two 10-bit words that are inserted after
the EAV and line number words. The format of the CRC words is shown below:
 
         b9     b8     b7     b6     b5     b4     b3     b2     b1     b0
      +------+------+------+------+------+------+------+------+------+------+
CRC0: |~crc8 | crc8 | crc7 | crc6 | crc5 | crc4 | crc3 | crc2 | crc1 | crc0 |
      +------+------+------+------+------+------+------+------+------+------+
CRC1: |~crc17| crc16| crc15| crc14| crc13| crc12| crc11| crc10| crc9 | crc8 |
      +------+------+------+------+------+------+------+------+------+------+

This module is purely combinatorial and contains no delay registers.
*/


module hdsdi_insert_crc (
    insert_crc,         // CRC values will be inserted when this input is high
    crc_word0,          // input asserted during time for first CRC word in EAV 
    crc_word1,          // input asserted during time for second CRC word in EAV
    c_in,               // C channel video input
    y_in,               // Y channel video input
    c_crc,              // C channel CRC value input
    y_crc,              // Y channel CRC value input
    c_out,              // C channel video output
    y_out               // Y channel video output
);

//-----------------------------------------------------------------------------
// Parameter definitions
//

//
// The VID_WIDTH parameter determines the width of the video input bus. For
// HD-SDI, this is normally always 10. Note that the code in this module
// may produce an error if VID_WIDTH is not 10.
//
parameter VID_WIDTH     = 10;                   // Width of video components
parameter VID_MSB       = VID_WIDTH - 1;        // MS bit # of video data path

//-----------------------------------------------------------------------------
// Port definitions
//
input                   insert_crc;
input                   crc_word0;
input                   crc_word1;
input   [VID_MSB:0]     c_in;
input   [VID_MSB:0]     y_in;
input   [17:0]          c_crc;
input   [17:0]          y_crc;
output  [VID_MSB:0]     c_out;
output  [VID_MSB:0]     y_out;

reg     [VID_MSB:0]     c_out;
reg     [VID_MSB:0]     y_out;

always @ (insert_crc or c_crc or c_in or crc_word0 or crc_word1)
    if (insert_crc & crc_word0)
        c_out <= {~c_crc[8], c_crc[8:0]};
    else if (insert_crc & crc_word1)
        c_out <= {~c_crc[17], c_crc[17:9]};
    else
        c_out <= c_in;

always @ (insert_crc or y_crc or y_in or crc_word0 or crc_word1)
    if (insert_crc & crc_word0)
        y_out <= {~y_crc[8], y_crc[8:0]};
    else if (insert_crc & crc_word1)
        y_out <= {~y_crc[17], y_crc[17:9]};
    else
        y_out <= y_in;

endmodule
