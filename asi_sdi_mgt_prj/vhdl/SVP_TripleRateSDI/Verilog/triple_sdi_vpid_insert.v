//------------------------------------------------------------------------------ 
// Copyright (c) 2008 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: triple_sdi_vpid_insert.v,v $
//  /   /        Date Last Modified:  $Date: 2010-11-05 13:08:07-06 $
// /___/   /\    Date Created: Feb 8, 2008
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: triple_sdi_vpid_insert.v,v $
// Revision 1.0  2010-11-05 13:08:07-06  reedt
// Initial revision
//
// Revision 1.0  2010-09-09 13:17:42-06  reedt
// Initial revision
//
// Revision 1.2  2008-07-31 13:32:08-06  jsnow
// Added vpidins_ce internal signal.
//
// Revision 1.1  2008-03-13 11:15:48-06  jsnow
// Added second line number input to support vertically unsynchronized
// dual HD-SDI signals in 3G-SDI level B.
//
// Revision 1.0  2008-02-25 14:08:31-07  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module inserts SMPTE 352M video payload ID packets into SD-SDI, HD-SDI, or
3G-SDI data streams. The module works with triple-rate SDI TX datapaths 
designed for 10-bit SERDES interfaces or 20-bit SERDES interfaces. The
difference is simply the clock, clock enable frequencies, and din_rdy
frequencies.

When used with 10-bit SERDES interfaces, such as the Virtex-5 GTP configured
with a 10-bit TXDATA port, the module works this way:

For SD-SDI, it accepts one 10-bit multiplexed Y/C data stream. The clock rate
must be 297 MHz and ce must be asserted 1 out of 11 clock cycles for a 27 MHz
data rate. The din_rdy input must be High always.

For HD-SDI, it accepts two 10-bit data streams, Y and C,. The clock rate is
148.5 MHz and ce must be asserted every other clock cycle for an input data
rate of 74.25 MHz. The din_rdy input must be High always.

For 3G-SDI level A, it accepts two 10-bit data streams. These can either be
the Y and C channels of 1080p 50 or 60 Hz video, or they can be pre-formatted
3G-SDI level A data streams. The clock frequency is 297 MHz and ce must be
asserted every other clock cycle for an input data rate of 148.5 MHz. The
din_rdy input must be High always.

For 3G-SDI level B, it accepts four 10-bit data streams. These can either be
a SMPTE 372M dual link pair or they can be two indpenedent, but synchronized,
HD-SDI signals. The clock frequency is 297 MHz, ce runs at 148.5 MHz, and
din_rdy is asserted one out of four clock cycles for an input data rate of
74.25 MHz. Input data is only accepted when din_rdy and ce are both High.
Because din_rdy is also used to mux the four data streams down to two data
streams on the output of the module, it must have a 50% duty cycle -- High for
two clock cycles and low for two clock cycles.

When used with 20-bit SERDES interfaces, such as the Virtex-5 GTX configured
with a 20-bit TXDATA port, the module works this way:

For SD-SDI, it accepts one 10-bit multiplexed Y/C data stream. The clock rate
must be 148.5 MHz and ce must be asserted with a 5/6/5/6 clock cycle cadence
giving a 27 MHz	data rate. The din_rdy input must be High always.

For HD-SDI, it accepts two 10-bit data streams, Y and C,. The clock rate is
74.25 MHz and ce and din_rdy inputs must always be High.

For 3G-SDI level A, it accepts two 10-bit data streams. These can either be
the Y and C channels of 1080p 50 or 60 Hz video, or they can be pre-formatted
3G-SDI level A data streams. The clock frequency is 148.5 MHz. The ce and the
din_rdy inputs must be High always.

For 3G-SDI level B, it accepts four 10-bit data streams. These can either be
a SMPTE 372M dual link pair or they can be two indpenedent, but synchronized,
HD-SDI signals. The clock frequency is 148.5 MHz. The ce input should be High
always. The din_rdy input must be asserted every other clock cycle giving an
input data rate of 74.25 MHz. Input data is only accepted when din_rdy and ce 
are both High. Because din_rdy is also used to mux the four data streams down to two data
streams on the output of the module, it must have a 50% duty cycle -- High for
one clock cycle and low for one clock cycle.

*/

`timescale 1ns / 1 ns

module  triple_sdi_vpid_insert (
    input  wire             clk,            // clock input
    input  wire             ce,             // clock enable
    input  wire             din_rdy,        // input data ready for level B, ..
                                            // .. must be 1 for all other modes
    input  wire             rst,            // async reset input
    input  wire [1:0]       sdi_mode,       // 00 = HD, 01 = SD, 10 = 3G
    input  wire             level,          // 0 = level A, 1 = level B
    input  wire             enable,         // 0 = disable insertion
    input  wire             overwrite,      // 1 = overwrite existing VPID packets
    input  wire [7:0]       byte1,          // VPID byte 1
    input  wire [7:0]       byte2,          // VPID byte 2
    input  wire [7:0]       byte3,          // VPID byte 3
    input  wire [7:0]       byte4a,         // VPID byte 4 for link A
    input  wire [7:0]       byte4b,         // VPID byte 4 for link B
    input  wire [10:0]      ln_a,           // current line number for link A
    input  wire [10:0]      ln_b,           // current line number for link B
    input  wire [10:0]      line_f1,        // VPID line for field 1
    input  wire [10:0]      line_f2,        // VPID line for field 2
    input  wire             line_f2_en,     // enable VPID insertion on line_f2
    input  wire [9:0]       a_y_in,         // SD in, HD & 3GA Y in, 3GB A Y in
    input  wire [9:0]       a_c_in,         // HD & 3GA C in, 3GB A C in
    input  wire [9:0]       b_y_in,         // 3GB only, B Y in
    input  wire [9:0]       b_c_in,         // 3GB only, B C in
    output reg  [9:0]       ds1a_out,       // data stream 1, link A out
    output reg  [9:0]       ds2a_out,       // data stream 2, link A out
    output reg  [9:0]       ds1b_out,       // data stream 1, link B out
    output reg  [9:0]       ds2b_out,       // data stream 2, link B out
    output reg              eav_out,        // asserted on XYZ word of EAV
    output reg              sav_out,        // asserted on XYZ word of SAV
    output reg  [1:0]       out_mode        // connect to mode port of the
                                            // triple_sdi_tx_output module
);

wire    [9:0]   ds2_in;
wire    [9:0]   ds1_y;
wire    [9:0]   ds1_c;
wire    [9:0]   ds2_y;
wire    [9:0]   ds2_c;
wire            eav;
wire            sav;
wire    [10:0]  ds2_ln;

reg     [1:0]   sdi_mode_reg = 2'b00;
wire            mode_SD;
wire            mode_3G_A;
wire            mode_3G_B;
reg             level_reg = 1'b0;
wire            vpidins_ce;

//
// Register timing critical signals
//
always @ (posedge clk)
    if (ce)
        sdi_mode_reg <= sdi_mode;

always @ (posedge clk)
    if (ce)
        level_reg <= level;

assign mode_SD = sdi_mode_reg == 2'b01;
assign mode_3G_A = (sdi_mode_reg == 2'b10) & ~level_reg;
assign mode_3G_B = (sdi_mode_reg == 2'b10) & level_reg;

//
// Insert VPID packets on both data streams
//
// The SMPTE352_vpid_insert module only inserts VPID packets into the Y data
// stream, so two of them are used to insert packets into each data stream.
//
assign vpidins_ce = ce & din_rdy;

SMPTE352_vpid_insert VPIDINS1 (
    .clk            (clk),
    .ce             (vpidins_ce),
    .rst            (rst),
    .hd_sd          (mode_SD),
    .level_b        (level_reg),
    .enable         (enable),
    .overwrite      (overwrite),
    .line           (ln_a),
    .line_a         (line_f1),
    .line_b         (line_f2),
    .line_b_en      (line_f2_en),
    .byte1          (byte1),
    .byte2          (byte2),
    .byte3          (byte3),
    .byte4          (byte4a),
    .y_in           (a_y_in),
    .c_in           (a_c_in),
    .y_out          (ds1_y),
    .c_out          (ds1_c),
    .eav_out        (eav),
    .sav_out        (sav));

assign ds2_in = mode_3G_A ? a_c_in : b_y_in;
assign ds2_ln = mode_3G_B ? ln_b : ln_a;

SMPTE352_vpid_insert VPIDINS2 (
    .clk            (clk),
    .ce             (vpidins_ce),
    .rst            (rst),
    .hd_sd          (mode_SD),
    .level_b        (level_reg),
    .enable         (enable),
    .overwrite      (overwrite),
    .line           (ds2_ln),
    .line_a         (line_f1),
    .line_b         (line_f2),
    .line_b_en      (line_f2_en),
    .byte1          (byte1),
    .byte2          (byte2),
    .byte3          (byte3),
    .byte4          (byte4b),
    .y_in           (ds2_in),
    .c_in           (b_c_in),
    .y_out          (ds2_y),
    .c_out          (ds2_c),
    .eav_out        (),
    .sav_out        ());

//
// Output muxes  Registered for S6 timing
//
always @ (posedge clk) 
    if (ce) begin
       ds2a_out <= mode_3G_A ? ds2_y : ds1_c;
       ds1b_out <= ds2_y;
       ds1a_out <= ds1_y;
       ds2b_out <= ds2_c;
       eav_out  <= eav;
       sav_out  <= sav;
     end

always @ (*)
    if (mode_SD)
        out_mode = 2'b01;
    else if (mode_3G_B)
        out_mode = 2'b10;
    else
        out_mode = 2'b00;
         
endmodule

