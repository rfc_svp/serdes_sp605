//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: Reed P. Tidwell
//  \   \        Filename: $RCSfile: s6_sdi_rx_light_20b.v,v $
//  /   /        Date Last Modified:  $Date: 2010-02-23 16:45:58-07 $
// /___/   /\    Date Created: September 24, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: s6_sdi_rx_light_20b.v,v $
// Revision 1.1  2010-02-23 16:45:58-07  reedt
// Feb. 2010 full release.
//
// Revision 1.0  2010-01-27 15:39:41-07  reedt
// Initial revision
//
// Revision 1.1  2009-12-08 12:01:14-07  reedt
// December '09 Pre-release.
//
// Revision 1.0  2009-11-18 14:29:05-07  reedt
// Initial revision
//
// Revision 1.0  2009-10-27 08:50:47-06  reedt
// Initial Checkin.  Works for HD, SD, and 3G with Vasu's DRU
//
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This is the triple-rate SDI (SD/HD/3G) receiver data path. It is designed to 
support GTP and GTX SERDES with 20-bit RXDATA interfaces, and the S6 DRU

--------------------------------------------------------------------------------
*/

module s6_sdi_rx_light_20b #(
    parameter NUM_SD_CE         = 2,        // number of SD-SDI clock enable outputs
    parameter NUM_3G_DRDY       = 2,        // number of dout_rdy_3G outputs
    parameter ERRCNT_WIDTH      = 4,        // width of counter tracking lines with errors
    parameter MAX_ERRS_LOCKED   = 15,       // max number of consecutive lines with errors
    parameter MAX_ERRS_UNLOCKED = 2)        // max number of lines with errors during search
(
    // inputs
    input  wire                     clk,                // rxusrclk input
    input  wire                     rst,                // async reset input
    input  wire [19:0]              data_in,            // raw data from GTx RXDATA port
    input  wire                     frame_en,           // 1 = enable framer position update

    // outputs
    output wire [1:0]               mode,               // 00=HD, 01=SD, 10=3G
    output wire                     mode_HD,            // 1 = HD mode      
    output wire                     mode_SD,            // 1 = SD mode
    output wire                     mode_3G,            // 1 = 3G mode
    output wire                     mode_locked,        // auto mode detection locked
    output wire                     rx_locked,          // receiver locked
    output wire [3:0]               t_format,           // transport format for 3G and HD
    output wire                     level_b_3G,         // 0 = level A, 1 = level B
    output wire [NUM_SD_CE-1:0]     ce_sd,              // clock enable for SD, always 1 for HD & 3G
    output wire                     nsp,                // framer new start position
    output wire [10:0]              ln_a,               // line number for HD & 3G (link A for level B)
    output wire [31:0]              a_vpid,             // video payload ID packet ds1 for 3G or HD-SDI
    output wire                     a_vpid_valid,       // 1 = a_vpid is valid
    output wire [31:0]              b_vpid,             // video payload ID packet data from data stream 2
    output wire                     b_vpid_valid,       // 1 = b_vpid is valid
    output wire                     crc_err_a,          // CRC error for HD & 3G
    output wire [9:0]               ds1_a,              // SD=Y/C, HD=Y, 3GA=ds1, 3GB=Y link A
    output wire [9:0]               ds2_a,              // HD=C, 3GA=ds2, 3GB=C link A
    output wire                     eav,                // EAV
    output wire                     sav,                // SAV
    output wire                     trs,                // TRS

    // outputs valid for 3G level B mode only
    output wire [10:0]              ln_b,               // line number of 3G level B link B
    output wire [NUM_3G_DRDY-1:0]   dout_rdy_3G,        // 1 for level A, asserted every other clk for level B
    output wire                     crc_err_b,          // CRC error for ds2 (level B only)
    output wire [9:0]               ds1_b,              // 3G level B only = Y link B
    output wire [9:0]               ds2_b               // 3G level B only = C link B
);

//
// Internal signal declarations
//

// Clock enables
localparam NUM_INT_CE = 5;          // Number of internal clock enables used
localparam NUM_INT_LVLB_CE = 4;


(* equivalent_register_removal = "no" *)
(* KEEP = "TRUE" *)
reg [NUM_INT_CE-1:0]        ce_int = 0;         // internal SD clock enable FFs

(* equivalent_register_removal = "no" *)
(* KEEP = "TRUE" *)
reg [NUM_INT_LVLB_CE-1:0]   ce_lvlb_int = 0;    // internal ce's correct for all modes

(* equivalent_register_removal = "no" *)
(* KEEP = "TRUE" *)
reg [NUM_SD_CE-1:0]         ce_sd_ff = 0;       // external SD clock enable FFs

(* equivalent_register_removal = "no" *)
(* KEEP = "TRUE" *)
reg [NUM_3G_DRDY-1:0]       dout_rdy_3G_ff = 0; // dout_rdy signals

(* KEEP = "TRUE" *)
wire                        dru_drdy;           // data ready signal from DRU


// Other internal signals
wire [9:0]      samv;
wire [9:0]      sam;
wire [9:0]      dru_dout;
reg             lvlb_drdy = 1'b0;
reg  [19:0]     rxdata = 0;
reg  [9:0]      sd_rxdata = 0;
wire [1:0]      mode_int;
wire            mode_locked_int;
reg             mode_HD_int;
reg             mode_SD_int;
reg             mode_3G_int;
wire [19:0]     descrambler_in;
wire [19:0]     descrambler_out;
wire [9:0]      framer_ds1;
wire [9:0]      framer_ds2;
wire            framer_eav;
wire            framer_sav;
wire            framer_trs;
wire            framer_xyz;
wire            framer_trs_err;
wire            framer_nsp;
wire            level_b;
wire            level_a;
wire [31:0]     a_vpid_int;
wire [31:0]     b_vpid_int;
wire [9:0]      vpid_b_in;
wire            a_vpid_valid_int;
wire            b_vpid_valid_int;
wire [9:0]      lvlb_a_y;
wire [9:0]      lvlb_a_c;
wire [9:0]      lvlb_b_y;
wire [9:0]      lvlb_b_c;
wire            lvlb_trs;
wire            lvlb_eav;
wire            lvlb_sav;
wire            lvlb_xyz;
wire            lvlb_dout_rdy_gen;
wire            lvlb_sav_err;
reg             autodetect_sav = 1'b0;
reg             autodetect_trs_err = 1'b0;
wire [3:0]      ad_format;
wire            ad_locked;
reg             eav_int = 1'b0;
reg             sav_int = 1'b0;
reg             trs_int = 1'b0;
reg  [9:0]      ds1_a_int = 0;
reg  [9:0]      ds2_a_int = 0;
reg  [9:0]      ds1_b_int = 0;
reg  [9:0]      ds2_b_int = 0;
wire            ds1_a_crc_err;
wire            ds2_a_crc_err;
wire            ds1_b_crc_err;
wire            ds2_b_crc_err;


//------------------------------------------------------------------------------
// Clock enable generation
//

//
// The internal clock enables and the external SD clock enables are copies of
// the DRU's drdy output in SD mode and are always High in HD and 3G modes.
//
always @ (posedge clk)
    if (mode == 2'b01)
        ce_int <= {NUM_INT_CE {dru_drdy}};
    else
        ce_int <= {NUM_INT_CE {1'b1}};

always @ (posedge clk)
    if (mode == 2'b01)
        ce_sd_ff <= {NUM_SD_CE {dru_drdy}};
    else
        ce_sd_ff <= {NUM_SD_CE {1'b1}};
        
assign ce_sd = ce_sd_ff;

//
// The lvlb_drdy clock enable is High all of the time except when running in
// 3G-SDI level B mode. In that mode, it is generate by control signals in the
// SMPTE425_B_demux module. The lvlb_drdy signal is fed back into the 
// SMPTE425_B_demux module to control its timing. The signal is also used to
// produce the external dout_rdy_3G signals. The lvlb_drdy and the dout_rdy_3G
// signals are asserted at a 74.25 MHz rate in 3G-SDI level B mode to control
// the 40-bit data path.
//
always @ (posedge clk)
    if (lvlb_dout_rdy_gen | ~(mode_3G_int & level_b))
        lvlb_drdy <= 1'b1;
    else
        lvlb_drdy <= ~lvlb_drdy;

always @ (posedge clk)
    if (lvlb_dout_rdy_gen | ~(mode_3G_int & level_b))
        dout_rdy_3G_ff <= {NUM_3G_DRDY {1'b1}};
    else
        dout_rdy_3G_ff <= {NUM_3G_DRDY {~lvlb_drdy}};

assign dout_rdy_3G = dout_rdy_3G_ff;

// 
// The ce_lvlb_int signal is a clock enable that is correct for all three
// operating modes. It is equivalent to the data ready signal from the DRU in
// SD-SDI mode. It is High for HD-SDI and 3G-SDI level A modes. It is asserted
// every other clock cycle in 3G-SDI level B mode.
//
always @ (posedge clk)
    if (mode == 2'b01)
        ce_lvlb_int <= {NUM_INT_LVLB_CE {dru_drdy}};
    else if (mode == 2'b00)
        ce_lvlb_int <= {NUM_INT_LVLB_CE {1'b1}};
    else
        ce_lvlb_int <= {NUM_INT_LVLB_CE {lvlb_drdy}};

//------------------------------------------------------------------------------
// Input register
//
always @ (posedge clk)
    rxdata <= data_in;

//------------------------------------------------------------------------------
// Oversampling data recovery unit
//
// This module recovers the SD-SDI data from the oversampled raw data output
// by the RocketIO transceiver. It produces a clock enable output whenever a
// 10-bit data word is ready on the output.
//
dru DRU(
     .DT_IN  (rxdata),
     .CENTER_F  (37'b0001110100011010111111101001011110110),
     .G1        (5'b00110),
     .G1_P      (5'b10000),
     .G2        (5'b00111),
     .CLK       (clk),
     .RST       (1'b1),
     .RST_FREQ  (1'b1),
     .EN        (1'b1),
     .CTRL      (),
     .DIRECT    (),
     .VER       (),
     .INTEG     (),
     .PH_OUT    (),
     .RECCLK    (), 
     .SAMV      (samv),
     .SAM       (sam)
  );
  
bshift10to10 DRUBSHIFT (
    .CLK        (clk),
    .RST        (1'b1),
    .DIN        (sam),
    .DV         (samv),
    .DV10       (dru_drdy),
    .DOUT10     (dru_dout));
    
always @ (posedge clk)
    if (dru_drdy)
        sd_rxdata <= dru_dout;
  
//
// SDI descrambler and framer
//
// The output of the framer is valid for HD or SD data.
//
assign descrambler_in = mode_SD_int ? {sd_rxdata, 10'b0} : rxdata;

multi_sdi_decoder DEC (
    .clk            (clk),
    .rst            (1'b0),
    .ce             (ce_int[0]),
    .hd_sd          (mode_SD_int),
    .d              (descrambler_in),
    .q              (descrambler_out));

multi_sdi_framer FRM (
    .clk            (clk),
    .rst            (1'b0),
    .ce             (ce_int[1]),
    .d              (descrambler_out),
    .frame_en       (frame_en),
    .hd_sd          (mode_SD_int),
    .c              (framer_ds2),
    .y              (framer_ds1),
    .trs            (framer_trs),
    .xyz            (framer_xyz),
    .eav            (framer_eav),
    .sav            (framer_sav),
    .trs_err        (framer_trs_err),
    .nsp            (framer_nsp));

assign nsp = framer_nsp;

//
// Triple-rate mode detection
//
always @ (posedge clk)
    if (ce_int[2])
        if (mode_3G_int & level_b)
        begin
            autodetect_sav <= lvlb_sav;
            autodetect_trs_err <= lvlb_sav_err;
        end else begin
            autodetect_sav <= framer_sav;
            autodetect_trs_err <= framer_trs_err;
        end

        
triple_sdi_rx_autorate #(
    .ERRCNT_WIDTH       (ERRCNT_WIDTH),
    .MAX_ERRS_LOCKED    (MAX_ERRS_LOCKED),
    .MAX_ERRS_UNLOCKED  (MAX_ERRS_UNLOCKED))
AUTORATE (
    .clk                (clk),
    .ce                 (ce_int[2]),
    .rst                (rst),
    .sav                (autodetect_sav),
    .trs_err            (autodetect_trs_err),
    .mode_enable        (3'b111),
    .mode               (mode_int),
    .locked             (mode_locked_int)
    );

always @ (*)
begin
    mode_HD_int = 1'b0;
    mode_SD_int = 1'b0;
    mode_3G_int = 1'b0;

    case(mode)
        2'b01:   mode_SD_int = 1'b1;
        2'b10:   mode_3G_int = 1'b1;
        default: mode_HD_int = 1'b1;
    endcase
end

assign mode_HD     = mode_HD_int & mode_locked_int;
assign mode_SD     = mode_SD_int & mode_locked_int;
assign mode_3G     = mode_3G_int & mode_locked_int;
assign mode        = mode_int;
assign mode_locked = mode_locked_int;

//
// 3G-SDI level B demux
//
SMPTE425_B_demux2 BDMUX (
    .clk            (clk),
    .ce             (ce_int[3]),
    .drdy_in        (lvlb_drdy),
    .rst            (rst),
    .ds1            (framer_ds1),
    .ds2            (framer_ds2),
    .trs_in         (framer_trs),
    .level_b        (level_b),
    .c0             (lvlb_a_c),
    .y0             (lvlb_a_y),
    .c1             (lvlb_b_c),
    .y1             (lvlb_b_y),
    .trs            (lvlb_trs),
    .eav            (lvlb_eav),
    .sav            (lvlb_sav),
    .xyz            (lvlb_xyz),
    .dout_rdy_gen   (lvlb_dout_rdy_gen),
    .line_num       ());

assign lvlb_sav_err = lvlb_sav & (
                       (lvlb_a_y[5] ^ lvlb_a_y[6] ^ lvlb_a_y[7]) |
                       (lvlb_a_y[4] ^ lvlb_a_y[8] ^ lvlb_a_y[6]) |
                       (lvlb_a_y[3] ^ lvlb_a_y[8] ^ lvlb_a_y[7]) |
                       (lvlb_a_y[2] ^ lvlb_a_y[8] ^ lvlb_a_y[7] ^ lvlb_a_y[6]) |
                       ~lvlb_a_y[9] | lvlb_a_y[1] | lvlb_a_y[0]);

//
// These pipelined muxes select between the framer output and the output of the
// level B data path. They also implement a pipeline delay to improve timing
// to the downstream logic.
//
always @ (posedge clk)
    if (ce_int[0])
    begin
        eav_int   <= (mode_3G_int & level_b) ? lvlb_eav : framer_eav;
        sav_int   <= (mode_3G_int & level_b) ? lvlb_sav : framer_sav;
        trs_int   <= (mode_3G_int & level_b) ? lvlb_trs : framer_trs;
        ds1_a_int <= (mode_3G_int & level_b) ? lvlb_a_y : framer_ds1;
        ds2_a_int <= (mode_3G_int & level_b) ? lvlb_a_c : framer_ds2;
        ds1_b_int <= lvlb_b_y;
        ds2_b_int <= lvlb_b_c;
    end

assign ds1_a = ds1_a_int;
assign ds2_a = ds2_a_int;
assign ds1_b = ds1_b_int;
assign ds2_b = ds2_b_int;
assign eav   = eav_int;
assign sav   = sav_int;
assign trs   = trs_int;
assign level_b_3G = mode_3G_int & level_b;
assign level_a    = mode_3G_int & ~level_b;

//
// Video timing detection module for HD and 3G
//
triple_sdi_autodetect_ln AD (
    .clk        (clk),
    .rst        (rst),
    .ce         (ce_lvlb_int[0]),
    .vid_in     (ds1_a_int[8:7]),
    .eav        (eav_int),
    .sav        (sav_int),
    .reacquire  (1'b0),
    .a3g        (level_a),
    .std        (ad_format),
    .locked     (ad_locked),
    .ln         (),
    .ln_valid   ());

assign t_format = ad_format;
assign rx_locked = (mode_SD_int) ? mode_locked_int : ad_locked;

//
// CRC checking
//
hdsdi_rx_crc RXCRC1 (
    .clk        (clk),
    .rst        (1'b0),
    .ce         (ce_lvlb_int[1]), 
    .c_video    (ds2_a_int),
    .y_video    (ds1_a_int),
    .trs        (trs_int),
    .c_crc_err  (ds2_a_crc_err),
    .y_crc_err  (ds1_a_crc_err),
    .c_line_num (),
    .y_line_num (ln_a));

hdsdi_rx_crc RXCRC2 (
    .clk        (clk),
    .rst        (1'b0),
    .ce         (ce_lvlb_int[1]), 
    .c_video    (ds2_b_int),
    .y_video    (ds1_b_int),
    .trs        (trs_int),
    .c_crc_err  (ds2_b_crc_err),
    .y_crc_err  (ds1_b_crc_err),
    .c_line_num (),
    .y_line_num (ln_b));

assign crc_err_a = ds2_a_crc_err | ds1_a_crc_err;
assign crc_err_b = ds2_b_crc_err | ds1_b_crc_err;

//
// Video payload ID capture
//
SMPTE352_vpid_capture PLOD1 (
    .clk            (clk),
    .ce             (ce_lvlb_int[2]),
    .rst            (rst),
    .sav            (sav_int),
    .vid_in         (ds1_a_int),
    .payload        (a_vpid),
    .valid          (a_vpid_valid));

assign vpid_b_in = (mode_3G_int & level_b) ? ds1_b_int : ds2_a_int;

SMPTE352_vpid_capture PLOD2 (
    .clk            (clk),
    .ce             (ce_lvlb_int[2]),
    .rst            (rst),
    .sav            (sav_int),
    .vid_in         (vpid_b_in),
    .payload        (b_vpid),
    .valid          (b_vpid_valid));

endmodule

