//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: Reed P. Tidwell
//  \   \        Filename: $RCSfile: mux12_wide.v,v $
//  /   /        Date Last Modified:  $Date: 2010-02-23 16:45:59-07 $
// /___/   /\    Date Created: August 10, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: mux12_wide.v,v $
// Revision 1.1  2010-02-23 16:45:59-07  reedt
// Feb. 2010 full release.
//
// Revision 1.0  2009-12-08 12:15:46-07  reedt
// Initial revision
//
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module implements a simple 12:1 MUX.  This is generic, rather than optimized
in order to work with multiple device families. The width of the MUX is set by 
the parameter WIDTH.
*/

`timescale 1ns / 1 ns

module mux12_wide  #(
    parameter               WIDTH = 10)
(
    input  wire [WIDTH-1:0] d0,             // input vector 0
    input  wire [WIDTH-1:0] d1,             // input vector 1
    input  wire [WIDTH-1:0] d2,             // input vector 2
    input  wire [WIDTH-1:0] d3,             // input vector 3
    input  wire [WIDTH-1:0] d4,             // input vector 4
    input  wire [WIDTH-1:0] d5,             // input vector 5
    input  wire [WIDTH-1:0] d6,             // input vector 6
    input  wire [WIDTH-1:0] d7,             // input vector 7
    input  wire [WIDTH-1:0] d8,             // input vector 8
    input  wire [WIDTH-1:0] d9,             // input vector 9
    input  wire [WIDTH-1:0] d10,            // input vector 10
    input  wire [WIDTH-1:0] d11,            // input vector 11
    input  wire [3:0]       sel,            // select inputs
    output reg  [WIDTH-1:0] y               // output port
);

always @ *    begin
  case (sel)
    0: y = d0;
    1: y = d1;
    2: y = d2;
    3: y = d3;
    4: y = d4;
    5: y = d5;
    6: y = d6;
    7: y = d7;
    8: y = d8;
    9: y = d9;
    10: y = d10;
    11: y = d11;
    default: y = d0;  
  endcase
end

            
endmodule