//------------------------------------------------------------------------------ 
// Copyright (c) 2008 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: SMPTE425_B_demux2.v,rcs $
//  /   /        Date Last Modified:  $Date: 2008-11-14 16:31:47-07 $
// /___/   /\    Date Created: April 21, 2008
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: SMPTE425_B_demux2.v,rcs $
// Revision 1.0  2008-11-14 16:31:47-07  jsnow
// Added register initializers.
//
// Revision 1.2  2008-06-02 13:40:57-06  jsnow
// Minor changes to clock enables.
//
// Revision 1.1  2008-05-07 14:47:11-06  jsnow
// Comment changes only.
//
// Revision 1.0  2008-04-22 16:43:00-06  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This is the SMPTE 425M 3G-SDI receiver demux for level B only. This
module takes two 10-bit streams at 148.5 MHz and converts them into two
streams each with two 10-bit components (Y and C) at 74.25 MHz. Typically,
the two 10-bit input streams to this module come directly from the C (ds1) 
and Y (ds2) outputs of a hdsdi_framer module.

The module also generates correct timing signals for the video including
TRS, XYZ, EAV, and SAV signals and line number information captured from the
data stream.

The module also creates an output clock enable signal, dout_rdy, that is
asserted when valid data is present on the outputs. If the input clock rate
is 148.5 MHz (with ce asserted high always), the dout_rdy will be asserted
every other clock cycle with a 50% duty cycle. If the input clock rate is
297 MHz (with ce asserted every other clock cycle), then dout_rdy will be
asserted one cycle out of every four with a 25% duty cycle.

Note: If ce input is used (not wired to 1), then dout_rdy will be asserted for
multiple clock cycles and will only change when the ce input is 1. Thus,
downstream devices should not treat dout_rdy as a clock enable, but as a
data ready signal that must be qualified with the clock enable.

This version of the file is identical to SMPTE425_B_demux except that the
dout_rdy output has been replaced with a dout_rdy_gen signal and a drdy_in
input has been added. This allows the dout_rdy signal to be generated externally
to this module, under control of the dout_rdy_gen, and fed back in on drdy_in.
This allows for better timing when generating the dout_rdy signal.
*/
//------------------------------------------------------------------------------

module SMPTE425_B_demux2 (
    input   wire        clk,            // word-rate clock
    input   wire        ce,             // input clock enable (always 1 if clk = 148.5 MHz)
    input   wire        drdy_in,
    input   wire        rst,            // async reset input
    input   wire [9:0]  ds1,            // connect to Y output of hdsdi_framer
    input   wire [9:0]  ds2,            // connect to C output of hdsdi_framer
    input   wire        trs_in,         // input TRS signal from hdsdi_framer
    output  reg         level_b,        // 1 if data is SMPTE 424M Level B
    output  reg  [9:0]  c0 = 0,         // channel 0 data stream C output
    output  wire [9:0]  y0,             // channel 0 data stream Y output
    output  reg  [9:0]  c1 = 0,         // channel 1 data stream C output
    output  wire [9:0]  y1,             // channel 1 data stream Y output
    output  wire        trs,            // asserted during all 4 words of EAV & SAV
    output  wire        eav,            // asserted during XYZ word of EAV
    output  wire        sav,            // asserted during XYZ word of SAV
    output  wire        xyz,            // asserted during XYZ word
    output  wire        dout_rdy_gen,
    output  reg [10:0]  line_num = 0    // line number
);

//
// Internal signals
//
reg     [9:0]       c0_int = 0;         // internal capture reg for c0
reg     [9:0]       y0_int = 0;         // internal capture reg for y0
reg     [9:0]       c1_int = 0;         // internal capture reg for c1
reg     [9:0]       y1_int = 0;         // internal capture reg for y1
reg     [4:0]       trs_dly = 0;        // TRS timing delay shift register
reg     [6:0]       ln_ls = 0;          // LS bits of line number capture
reg                 trs_rise = 0;       // TRS rising edge detect
wire                all_ones;           // Y channel is all ones
wire                all_zeros;          // Y channel is all zeros
reg     [2:0]       zeros = 0;          // all zeros delay shift register
reg     [4:0]       ones = 0;           // all ones delay shift register
reg                 level_b_detect = 0; // level_b detect signal
reg                 trs_rise_dly = 0;

//
// Clock enable logic
//
// First detect the rising edge of the trs input signal. The dout_rdy_gen signal
// is set to one the cycle after the rising edge of trs. 
//
always @ (posedge clk)
    if (ce) begin
        if (trs_in & ~trs_dly[0])
            trs_rise <= 1'b1;
        else
            trs_rise <= 1'b0;
    end

always @ (posedge clk)
    if (ce)
        trs_rise_dly <= trs_rise;

assign dout_rdy_gen = trs_rise & ~trs_rise_dly;

//
// Capture registers
//
// The capture registers convert the two 10-bit data streams into two 20-bit
// data streams. The C components are captured first and stored in temporary
// registers. The temporary C component registers and the incoming Y components
// are then captured in the final capture registers and output from the module
// as y0, c0, y1, and c1.
//
always @ (posedge clk)
    if (ce) 
        if (~drdy_in) 
        begin
            c0_int <= ds1;
            c1_int <= ds2;
        end

always @ (posedge clk)
    if (ce)
        if (drdy_in) begin
            y0_int <= ds1;
            c0     <= c0_int;
            y1_int <= ds2;
            c1     <= c1_int;
        end

assign y0 = y0_int;
assign y1 = y1_int;

//
// TRS timing
//
// This logic generates the trs, xyz, eav, and sav timing signals, all derived
// from the trs_in signal.
//
always @ (posedge clk or posedge rst)
    if (rst)
        trs_dly <= 0;
    else if (ce)
        if (drdy_in)
            trs_dly <= {trs_dly[3:0], trs_in};

assign trs = |trs_dly[2:0] | (&trs_dly[3:2]);
assign xyz = trs_dly[3] & ~trs_dly[4];
assign eav = xyz & y0_int[6];
assign sav = xyz & ~y0_int[6];

//
// Line number capture
//
// This logic captures the line number information that is embedded in the y0
// data stream.
//
reg [1:0] eav_dly = 0;

always @ (posedge clk)
    if (ce)
        if (drdy_in)
            eav_dly <= {eav_dly[0], eav};

always @ (posedge clk)
    if (ce)
        if (drdy_in & eav_dly[0])
            ln_ls <= y0_int[8:2];

always @ (posedge clk)
    if (ce)
        if (drdy_in & eav_dly[1])
            line_num <= {y0_int[5:2], ln_ls};

//
// Level B detector
//
// This logic determines whether the input data streams are carrying level A
// or level B encoded data. This determination is not dependent upon SMPTE
// 352M video payload ID packets. The determination is made by examining the
// pattern of words with all 1's and all 0's at each TRS. The pattern is 
// different between level A and level B.
//
assign all_ones = &ds1;
assign all_zeros = ~|ds1;

always @ (posedge clk)
    if (ce)
        ones <= {ones[3:0], all_ones};

always @ (posedge clk)
    if (ce)
        zeros <= {zeros[1:0], all_zeros};

always @ (posedge clk)
    if (ce)
        if (drdy_in)
            level_b_detect <= &ones[4:3] & &zeros[2:0] & all_zeros;

always @ (posedge clk)
    if (ce)
        if (drdy_in & trs_dly[2] & trs_dly[1])
            level_b <= level_b_detect;


endmodule

