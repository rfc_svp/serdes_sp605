//------------------------------------------------------------------------------ 
// Copyright (c) 2007 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: SMPTE352_vpid_capture.v,rcs $
//  /   /        Date Last Modified:  $Date: 2007-08-08 13:39:42-06 $
// /___/   /\    Date Created: April 26, 2007
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: SMPTE352_vpid_capture.v,rcs $
// Revision 1.0  2007-08-08 13:39:42-06  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs. Xilinx and its licensors make 
// and you receive no warranties or conditions, express, implied, statutory or 
// otherwise, and Xilinx specifically disclaims any implied warranties of 
// merchantability, non-infringement, or fitness for a particular purpose. 
// Xilinx does not warrant that the functions contained in these designs will 
// meet your requirements, or that the operation of these designs will be 
// uninterrupted or error free, or that defects in the Designs will be 
// corrected. Furthermore, Xilinx does not warrant or make any representations 
// regarding use or the results of the use of the designs in terms of 
// correctness, accuracy, reliability, or otherwise. The designs are not 
// covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module captures the SMPTE 352M video payload ID packet. The payload
output port is only updated when the packet does not have a checksum error. 
The vpid_valid output is asserted as long at least one valid packet has 
been detected in the last VPID_TIMEOUT_VBLANKS vertical blanking intervals.
*/

`timescale 1ns / 1 ns

module SMPTE352_vpid_capture #(  
    parameter VPID_TIMEOUT_VBLANKS = 4)
( 
    // inputs
    input  wire         clk,            // clock input
    input  wire         ce,             // clock enable
    input  wire         rst,            // async reset input
    input  wire         sav,            // asserted on XYZ word of SAV
    input  wire [9:0]   vid_in,         // video data input
        
    // outputs
    output reg  [31:0]  payload = 0,    // {byte 4, byte 3, byte 2, byte 1}
    output reg          valid = 0       // 1 when payload is valid
);

//-----------------------------------------------------------------------------
// Parameter definitions
//      

//
// This group of parameters defines the states of the finite state machine.
//
localparam STATE_WIDTH   = 4;
localparam STATE_MSB     = STATE_WIDTH - 1;

localparam [STATE_WIDTH-1:0]
    STATE_START     = 0,
    STATE_ADF2      = 1,
    STATE_ADF3      = 2,
    STATE_DID       = 3,
    STATE_SDID      = 4,
    STATE_DC        = 5,
    STATE_UDW0      = 6,
    STATE_UDW1      = 7,
    STATE_UDW2      = 8,
    STATE_UDW3      = 9,
    STATE_CS        = 10;

localparam MUXSEL_MSB = 2;

localparam [MUXSEL_MSB:0]
    MUX_SEL_000     = 0,
    MUX_SEL_3FF     = 1,
    MUX_SEL_DID     = 2,
    MUX_SEL_SDID    = 3,
    MUX_SEL_DC      = 4,
    MUX_SEL_CS      = 5;

localparam SR_MSB = VPID_TIMEOUT_VBLANKS - 1;

reg  [STATE_MSB:0]  current_state = STATE_START;
reg  [STATE_MSB:0]  next_state;
reg  [8:0]          checksum = 0;
reg                 old_v = 0;
reg                 v = 0;
wire                v_fall;
wire                v_rise;
reg                 packet_rx = 0;
reg [SR_MSB:0]      packet_det = 0;
reg [7:0]           byte1 = 0;
reg [7:0]           byte2 = 0;
reg [7:0]           byte3 = 0;
reg [7:0]           byte4 = 0;
reg                 ld_byte1;
reg                 ld_byte2;
reg                 ld_byte3;
reg                 ld_byte4;
reg                 ld_cs_err;
reg                 clr_cs;
reg [MUXSEL_MSB:0]  cmp_mux_sel;
reg [9:0]           cmp_mux;
wire                cmp_equal;
reg                 packet_ok = 1'b0;


//
// FSM: current_state register
//
// This code implements the current state register. 
//
always @ (posedge clk or posedge rst)
    if (rst)
        current_state <= STATE_START;
    else if (ce)
        current_state <= next_state;

//
// FSM: next_state logic
//
// This case statement generates the next_state value for the FSM based on
// the current_state and the various FSM inputs.
//
always @ *
    case(current_state)
        STATE_START:
            if (cmp_equal)
                next_state = STATE_ADF2;
            else
                next_state = STATE_START;
                
        STATE_ADF2:
            if (cmp_equal)
                next_state = STATE_ADF3;
            else
                next_state = STATE_START;

        STATE_ADF3:
            if (cmp_equal)
                next_state = STATE_DID;
            else
                next_state = STATE_START;

        STATE_DID:
            if (cmp_equal)
                next_state = STATE_SDID;
            else
                next_state = STATE_START;

        STATE_SDID:
            if (cmp_equal)
                next_state = STATE_DC;
            else
                next_state = STATE_START;

        STATE_DC:
            if (cmp_equal)
                next_state = STATE_UDW0;
            else
                next_state = STATE_START;

        STATE_UDW0:
            next_state = STATE_UDW1;

        STATE_UDW1:
            next_state = STATE_UDW2;

        STATE_UDW2:
            next_state = STATE_UDW3;

        STATE_UDW3:
            next_state = STATE_CS;

        STATE_CS:
            next_state = STATE_START;

        default:    next_state = STATE_START;
    endcase
        
//
// FSM: outputs
//
// This block decodes the current state to generate the various outputs of the
// FSM.
//
always @ *
    begin
        // Unless specifically assigned in the case statement, all FSM outputs
        // are given the values assigned here.
        ld_byte1    = 1'b0;
        ld_byte2    = 1'b0;
        ld_byte3    = 1'b0;
        ld_byte4    = 1'b0;
        ld_cs_err   = 1'b0;
        clr_cs      = 1'b0;
        cmp_mux_sel = MUX_SEL_000;
                                
        case(current_state) 

            STATE_START:    clr_cs = 1'b1;

            STATE_ADF2:     begin
                                cmp_mux_sel = MUX_SEL_3FF;
                                clr_cs = 1'b1;
                            end

            STATE_ADF3:     begin
                                cmp_mux_sel = MUX_SEL_3FF;
                                clr_cs = 1'b1;
                            end

            STATE_DID:      cmp_mux_sel = MUX_SEL_DID;

            STATE_SDID:     cmp_mux_sel = MUX_SEL_SDID;

            STATE_DC:       cmp_mux_sel = MUX_SEL_DC;

            STATE_UDW0:     ld_byte1 = 1'b1;

            STATE_UDW1:     ld_byte2 = 1'b1;

            STATE_UDW2:     ld_byte3 = 1'b1;

            STATE_UDW3:     ld_byte4 = 1'b1;

            STATE_CS:       begin
                                cmp_mux_sel = MUX_SEL_CS;
                                ld_cs_err = 1'b1;
                            end
        endcase
    end

//
// Comparator
//
// Compares the expected value of each word, except the user data words, to the
// received value.
//
always @ *
    case(cmp_mux_sel)
        MUX_SEL_000:    cmp_mux = 10'h000;
        MUX_SEL_3FF:    cmp_mux = 10'h3ff;
        MUX_SEL_DID:    cmp_mux = 10'h241;
        MUX_SEL_SDID:   cmp_mux = 10'h101;
        MUX_SEL_DC:     cmp_mux = 10'h104;
        MUX_SEL_CS:     cmp_mux = {~checksum[8], checksum};
        default:        cmp_mux = 10'h000;
    endcase

assign cmp_equal = cmp_mux == vid_in;

//
// User data word registers
//
always @ (posedge clk)
    if (ce & ld_byte1)
        byte1 <= vid_in[7:0];

always @ (posedge clk)
    if (ce & ld_byte2)
        byte2 <= vid_in[7:0];

always @ (posedge clk)
    if (ce & ld_byte3)
        byte3 <= vid_in[7:0];

always @ (posedge clk)
    if (ce & ld_byte4)
        byte4 <= vid_in[7:0];

//
// Checksum generation and error flag
//
always @ (posedge clk)
    if (ce) begin
        if (clr_cs)
            checksum <= 0;
        else
            checksum <= checksum + vid_in[8:0];
    end
    
always @ (posedge clk)
    if (ce) 
        packet_ok <= ld_cs_err & cmp_equal;

//
// Packet valid signal generation
//
// The valid output is updated immediatly if a packet is received. Once a
// packet has been detected in any of the last VPID_TIMEOUT_VBLANKS blanking 
// intervals, the valid output will be asserted.
//
always @ (posedge clk)
    if (ce & sav) begin
        v <= vid_in[7];
        old_v <= v;
    end
    
assign v_fall = old_v & ~v;
assign v_rise = ~old_v & v;

always @ (posedge clk)
    if (ce) begin
        if (packet_ok)
            packet_rx <= 1'b1;
        else if (v_rise)
            packet_rx <= 1'b0;
    end

always @ (posedge clk)
    if (ce & v_fall)
        packet_det <= {packet_det[SR_MSB - 1: 0], packet_rx};

always @ (posedge clk) 
    if (ce)
        valid <= packet_rx | (|packet_det);
         
//
// Output registers
//
// The payload register is loaded from the captured bytes at the same time that
// packet_rx is set -- when packet_ok is asserted.
//
always @ (posedge clk)
    if (ce & packet_ok)
        payload <= {byte4, byte3, byte2, byte1};

endmodule
