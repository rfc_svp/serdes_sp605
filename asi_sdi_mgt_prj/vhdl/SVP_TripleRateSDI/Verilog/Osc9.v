// **************************************************************//
// Module Name: Osc9.v 
// Description : freq  ~ 50 Mhz 
// **************************************************************//

 module Osc9 ( out_clk, enable );

   output out_clk;
   input  enable;
   
   wire invout1;
   wire invout2;
   wire invout3;
   wire invout4;
   wire invout5;
   wire invout6;
   wire invout7;
   wire invout8;
   

   XORCY Ixor1 ( .CI (out_clk), .LI (enable), .O(invout1));
   XORCY Iinv2 ( .CI (invout1), .LI (1'b1), .O(invout2));
   XORCY Iinv3 ( .CI (invout2), .LI (1'b1), .O(invout3));
   XORCY Iinv4 ( .CI (invout3), .LI (1'b1), .O(invout4));
   XORCY Iinv5 ( .CI (invout4), .LI (1'b1), .O(invout5));
   XORCY Iinv6 ( .CI (invout5), .LI (1'b1), .O(invout6));
   XORCY Iinv7 ( .CI (invout6), .LI (1'b1), .O(invout7));
   XORCY Iinv8 ( .CI (invout7), .LI (1'b1), .O(invout8));
   XORCY Iinv9 ( .CI (invout8), .LI (1'b1), .O(out_clk));

//synthesis attribute LOC of Ixor1 is SLICE_X0Y10;
//synthesis attribute LOC of Iinv2 is SLICE_X0Y11;
//synthesis attribute LOC of Iinv3 is SLICE_X0Y12;
//synthesis attribute LOC of Iinv4 is SLICE_X0Y13;
//synthesis attribute LOC of Iinv5 is SLICE_X0Y14;
//synthesis attribute LOC of Iinv6 is SLICE_X0Y15;
//synthesis attribute LOC of Iinv7 is SLICE_X0Y16;
//synthesis attribute LOC of Iinv8 is SLICE_X0Y17;
//synthesis attribute LOC of Iinv9 is SLICE_X0Y18;


 endmodule // Osc9
