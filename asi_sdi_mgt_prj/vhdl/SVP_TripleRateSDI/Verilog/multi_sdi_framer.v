//------------------------------------------------------------------------------ 
// Copyright (c) 2008 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow
//  \   \        Filename: $RCSfile: multi_sdi_framer.v,v $
//  /   /        Date Last Modified:  $Date: 2010-02-23 16:46:00-07 $
// /___/   /\    Date Created: May 28, 2004
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: multi_sdi_framer.v,v $
// Revision 1.2  2010-02-23 16:46:00-07  reedt
// Feb. 2010 full release.
//
// Revision 1.1  2009-12-08 12:15:43-07  reedt
// <>
//
// Revision 1.0  2009-10-19 14:20:19-06  reedt
// Added register stage.  Fixes S6 timing.
//
// Revision 1.7  2008-11-17 16:04:20-07  jsnow
// Added register initializers.
//
// Revision 1.6  2008-10-01 09:36:00-06  jsnow
// Added KEEP constraints to some critical SD path signals so that
// they can be used as TPTHRU points for constraining the SD
// path to the slower SD clock rate.
//
// Revision 1.5  2006-01-10 10:10:14-07  jsnow
// Fixed error in HD mode which caused false TRS detection on
// some input patterns.
//
// Revision 1.4  2005-03-29 15:05:40-07  jsnow
// in_vector assignment for SD mode was incorrect in version 1.3
//
// Revision 1.3  2005-03-29 14:09:00-07  jsnow
// Fixed error in SD mode which caused false TRS detection on
// some input patterns.
//
// Revision 1.2  2004-10-18 14:14:52-06  jsnow
// Force LS 10-bits of d to zero in SD mode.
//
// Revision 1.1  2004-08-23 13:05:46-06  jsnow
// Comment changes only.
//
// Revision 1.0  2004-05-21 14:02:37-06  jsnow
// Initial Revision
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

SMPTE 292M-1998 HD-SDI is a standard for transmitting high-definition digital 
video over a serial link.  SMPTE 259M (SD-SDI) is an equivalent standard for 
standard-definition video. This module performs the framing function on the 
decoded data from the multi-rate decoder for both SD-SDI and HD-SDI

This module accepts 20-bit "unframed" data words in HD-SDI mode and
10-bit data in SD-SDI mode. It examines the video stream for the 30-bit TRS 
preamble. Once a TRS is found, the framer then knows the bit boundary of all 
subsequent 10-bit characters in the video stream and uses this offset to 
generate properly framed video.

The d input port is 20-bits wide to accommodate the 20-bit HD-SDI data word
from the decoder. In SD-SDI mode, only the 10 most significant bits (19:10)
are used.

The module has the following control inputs:

ce: The clock enable input controls loading of all registers in the module. It
must be asserted whenever a new 10-bit word is to be loaded into the module. By
providing a clock enable, this module can use a clock that is running at the
bit rate of the SDI bit stream if ce is asserted once every ten clock cycles.

hd_sd: Controls whether the framer runs in HD-SDI mode (0) or SD-SDI mode (1).

frame_en: This input controls whether the framer resynchronize to new character
offsets when out-of-phase TRS symbols are detected. When this input is high,
out-of-phase TRS symbols will cause the framer to resynchronize.

The module generates the following outputs:

c: This port contains the framed 10-bit C component for HD-SDI. It is unused
for SD-SDI.

y: This port contains the framed 10-bit Y component for HD-SDI or the 10-bit
framed video word for SD-SDI.

trs: (timing reference signal) This output is asserted when the y and c outputs
have any of the four words of a TRS.

xyz: This output is asserted when the XYZ word of a TRS is output.

eav: This output is asserted when the XYZ word of a EAV is output.

sav: This output is asserted when the XYZ word of a SAV is output.

trs_err: This output is asserted during the XYZ word if an error is detected
by examining the protection bits.

nsp: (new start position) If frame_en is low and a TRS is detected that does not
match the current character offset, this signal will be asserted high. The nsp
signal will remain asserted until the offset error has been corrected..

There are normally three ways to use the frame_en input:

frame_en tied high: When frame_en is tied high, the framer will resynchronize
on every TRS detected. 

frame_en tied to nsp: When in this mode, the framer implements TRS filtering.
If a TRS is detected that is out of phase with the existing character offset,
nsp will be asserted, but the framer will not resynchronize. If the next TRS
received is in phase with the current character offset, nsp will go low and the
will not resynchronize. If the next TRS arrives out of phase with the current
character offset, then the new character offset will be loaded and nsp will be
deasserted. Single erroneous TRS  are ignored in this mode, but if they persist,
the decoder will adjust.

frame_en tied low: The automatic framing function is disabled when frame_en is
tied low. If data is being sent across the interface that does not comply with
the SDI standard and may contain data that looks like TRS symbols, the framing
function can be disabled in this manner.
--------------------------------------------------------------------------------
*/

`timescale 1ns / 1ns

module multi_sdi_framer (
    input  wire         clk,        // input clock
    input  wire         rst,        // reset signal
    input  wire         ce,         // clock enable
    input  wire [19:0]  d,          // input data
    input  wire         frame_en,   // enables resynchronization when high
    input  wire         hd_sd,
    output wire [9:0]   c,          // chroma channel output data
    output wire [9:0]   y,          // luma channel output data
    output reg          trs = 1'b0, // asserted when out reg contains a TRS symbol
    output wire         xyz,        // asserted during XYZ word of TRS symbol
    output wire         eav,        // asserted during XYZ word of EAV symbol
    output wire         sav,        // asserted during XYZ word of SAV symbol
    output wire         trs_err,    // asserted if error detected in XYZ word
    output reg          nsp = 1'b1  // new start position detected
);

//------------------------------------------------------------------------------
// Internal signals
//
reg     [19:0]      in_reg = 0;         // input register
reg     [19:0]      dly_reg = 0;        // pipeline delay register
reg     [19:0]      dly_reg2 = 0;       // pipeline delay register
reg     [19:0]      dly_reg3 = 0;       // pipeline delay register
wire    [4:0]       offset_val;         // HD/SD offset value mux output
wire                trs_detected;       // HD/SD TRS detected mux output
wire                hd_trs_err;
reg     [4:0]       offset_reg = 0;     // offset register
reg     [3:0]       trs_out = 0;        // used to generate the trs output signal
wire    [38:0]      hd_in_0;            // input vector for zeros detector
wire    [38:0]      hd_in_1;            // input vector for ones detector
reg     [19:0]      hd_ones_in;         // ones detector result vector 
reg     [19:0]      hd_ones_reg;        // ones detector result register 
reg     [19:0]      hd_zeros_in;        // zeros detector result vector
reg     [19:0]      hd_zeros_reg;       // zeros detector result register
reg     [19:0]      hd_zeros_dly = 0;   // zeros detector result vector delayed
wire    [19:0]      hd_trs_match;       // TRS detector result vector
wire                hd_trs_detected;    // asserted when TRS symbol is detected
wire    [4:0]       hd_offset_val;      // calculated offset value to load into offset_reg
reg     [34:0]      bs_1_out;           // output of first level of barrel shifter
reg     [22:0]      bs_2_out;           // output of second level of barrel shifter
reg     [38:0]      barrel_in = 0;      // barrel shifter input register
reg     [19:0]      barrel_out;         // output of barrel shifter
wire                new_offset;         // mismatch between offset_val and offset_reg
wire    [50:0]      bs_in;              // input vector to barrel shifter first level
wire                bs_sel_1;           // barrel shifter first level select bit
wire    [1:0]       bs_sel_2;           // barrel shifter second level select bits
wire    [1:0]       bs_sel_3;           // barrel shifter third level select bits
reg     [9:0]       c_int = 0;          // internal version of c output
reg     [9:0]       y_int = 0;          // internal version of y output
reg                 xyz_int = 1'b0;     // internal flip-flop for XYZ output
integer             i,j,k;              // barrel shifter loop variables
integer             l,m;                // TRS detect for loop variables
wire    [38:0]      sd_in_vector;       // concatenation of the four input registers
reg     [9:0]       sd_trs_match1;      // which offsets in in_vector[18:0] match 0x3ff
reg     [9:0]       sd_trs_match2;      // which offsets in in_vector[28:10] match 0x000
reg     [9:0]       sd_trs_match3;      // which offsets in in_vector[38:29] match 0x000
wire    [9:0]       sd_trs_match_all;   // which offsets match complete 30-bit TRS symbol
reg     [15:0]      sd_trs_match1_l1;   // intermediate level of gate outputs in TRS detector
reg     [15:0]      sd_trs_match2_l1;   // intermediate level of gate outputs in TRS detector
reg     [15:0]      sd_trs_match3_l1;   // intermediate level of gate outputs in TRS detector
(* KEEP = "TRUE" *)
wire                sd_trs_detected;    // asserted when TRS symbol is detected
(* KEEP = "TRUE" *)
reg                 sd_trs_err;         // more than one offset matched the TRS symbol
wire    [3:0]       sd_offset_val;      // calculated offset value to load into offset_reg
      
//------------------------------------------------------------------------------
// Input and pipeline delay registers
//

//
// input register
//
always @ (posedge clk or posedge rst)
    if (rst)
        in_reg <= 0;
    else if (ce)
        in_reg <= d;
    
//
// delay register
//
always @ (posedge clk or posedge rst)
    if (rst)
        dly_reg <= 0;
    else if (ce)
        dly_reg <= in_reg;

//
// delay register 2
//
always @ (posedge clk or posedge rst)
    if (rst)
        dly_reg2 <= 0;
    else if (ce)
        dly_reg2 <= dly_reg;
//
// delay register 3
//
always @ (posedge clk or posedge rst)
    if (rst)
        dly_reg3 <= 0;
    else if (ce)
        dly_reg3 <= dly_reg2;

//------------------------------------------------------------------------------
// HD TRS detector and offset encoder
//
// The HD TRS detector identifies the 60-bit TRS sequence consisting of 20 '1'
// bits followed by 40 '0' bits. The first level of the TRS detector consists
// of a ones detector and a zeros detector. The ones detector looks for a run
// of 20 consecutive ones in the hd_in_1 vector. The hd_in_1 vector is a 39-bit
// vector made up of the contents of dly_reg2 and the 19 LSBs of dly_reg. The
// zeros detector looks for a run of 20 consecutive '0' bits in the hd_in_0 
// vector. The hd_in_0 vector is 39-bits wide and is made up of the contents of 
// in_reg and the 19 LSBs of the d input port. The output of the zeros detector 
// is stored in the hd_zeros_dly register so that the zeros detector can be used
// twice to find two consecutive runs of 20 zeros. The output of the zeros 
// detector (both hd_zeros_in and hd_zeros_dly) and the ones detector 
// (hd_ones_in) are 20-bit vectors with a bit for each possible starting 
// position of the 20-bit run.
//
// A vector called trs_match is created by ORing the hd_ones_in, hd_zeros_in, 
// and hd_zeros_dly values together. The 20-bit trs_match vector will have a 
// single bit set indicating the starting position of a TRS if one is present in
// the input vector. The trs_detected signal, asserted when a TRS is detected, 
// can then be created by ORing all of the bits of trs_match together. And the
// offset_val, which is a 4-bit binary value indicating the starting position
// of the TRS to the barrel shifter, can be generated from the trs_match vector.
// 
assign hd_in_0 = {d[18:0], in_reg};
assign hd_in_1 = {dly_reg[18:0], dly_reg2};


//
// zeros and ones detectors
//
always @ *
    for (l = 0; l < 20; l = l + 1)
        hd_zeros_in[l] <= ~(hd_in_0[l+19] | hd_in_0[l+18] | hd_in_0[l+17] | 
                            hd_in_0[l+16] | hd_in_0[l+15] | hd_in_0[l+14] | 
                            hd_in_0[l+13] | hd_in_0[l+12] | hd_in_0[l+11] | 
                            hd_in_0[l+10] | hd_in_0[l+ 9] | hd_in_0[l+ 8] |
                            hd_in_0[l+ 7] | hd_in_0[l+ 6] | hd_in_0[l+ 5] | 
                            hd_in_0[l+ 4] | hd_in_0[l+ 3] | hd_in_0[l+ 2] | 
                            hd_in_0[l+ 1] | hd_in_0[l+ 0]);

always @ *
    for (m = 0; m < 20; m = m + 1)
        hd_ones_in[m] <= hd_in_1[m+19] & hd_in_1[m+18] & hd_in_1[m+17] & 
                         hd_in_1[m+16] & hd_in_1[m+15] & hd_in_1[m+14] & 
                         hd_in_1[m+13] & hd_in_1[m+12] & hd_in_1[m+11] & 
                         hd_in_1[m+10] & hd_in_1[m+ 9] & hd_in_1[m+ 8] & 
                         hd_in_1[m+ 7] & hd_in_1[m+ 6] & hd_in_1[m+ 5] & 
                         hd_in_1[m+ 4] & hd_in_1[m+ 3] & hd_in_1[m+ 2] & 
                         hd_in_1[m+ 1] & hd_in_1[m+ 0];
                         
always @(posedge clk) begin
  hd_zeros_reg <= hd_zeros_in;
  hd_ones_reg  <= hd_ones_in;
end

// delay reg for hd_zeros_reg
always @ (posedge clk or posedge rst)
    if (rst)
        hd_zeros_dly <= 0;
    else if (ce)
        hd_zeros_dly <= hd_zeros_reg;

// TRS match vector generation
assign hd_trs_match = hd_zeros_reg & hd_zeros_dly & hd_ones_reg;

// trs_detected signal
assign hd_trs_detected = |hd_trs_match;


//
// The following assignments encode the hd_trs_match vector into a binary
// offset code.
//
assign hd_offset_val[0] = hd_trs_match[1]  | hd_trs_match[3]  | hd_trs_match[5]  |
                          hd_trs_match[7]  | hd_trs_match[9]  | hd_trs_match[11] |
                          hd_trs_match[13] | hd_trs_match[15] | hd_trs_match[17] |
                          hd_trs_match[19];

assign hd_offset_val[1] = hd_trs_match[2]  | hd_trs_match[3]  | hd_trs_match[6]  |
                          hd_trs_match[7]  | hd_trs_match[10] | hd_trs_match[11] |
                          hd_trs_match[14] | hd_trs_match[15] | hd_trs_match[18] |
                          hd_trs_match[19];

assign hd_offset_val[2] = hd_trs_match[4]  | hd_trs_match[5]  | hd_trs_match[6]  |
                          hd_trs_match[7]  | hd_trs_match[12] | hd_trs_match[13] |
                          hd_trs_match[14] | hd_trs_match[15];

assign hd_offset_val[3] = hd_trs_match[8]  | hd_trs_match[9]  | hd_trs_match[10] |
                          hd_trs_match[11] | hd_trs_match[12] | hd_trs_match[13] |
                          hd_trs_match[14] | hd_trs_match[15];

assign hd_offset_val[4] = hd_trs_match[16] | hd_trs_match[17] | hd_trs_match[18] |
                          hd_trs_match[19];


//------------------------------------------------------------------------------
// SD TRS detector
//

//
// TRS detector and offset encoder
//
// The TRS detector finds 30-bit TRS preambles (0x3ff, 0x000, 0x000) in the
// input data stream. The TRS detector scans a 39-bit input vector
// consisting of all the bits from the three input registers plus the LS
// 9 bits of the d input data.
//
// The detector consists two main parts. 
//
// The first part is a series 10-bit AND and NOR gates that examine each
// possible bit location in the 39 input vector for the TRS preamble. These
// 10-bit wide AND and NOR gates have been coded here as two levels of
// 3 and 4 input gates because this results in a more compact implementation
// in most synthesis engines. 
//
// The outputs of these gates are assigned to the vectors trs_match1, 2, 
// and 3. These three vectors each contain 10 unary bits which indicate which 
// offset(s) matched the pattern being detected. ANDing these three vectors
// together generates another 10-bit vector called trs_match_all whose bits
// indicate which offset(s) matches the entire 30-bit TRS preamble.
//
// After the starting position of the TRS preamble has been detected, it must
// be encoded into an offset value which can drive the barrel shifter. The
// TRS encoder generates a 4-bit offset_val which contains the bit offset of
// the TRS preamble. It also generates a trs_error signal which is asserted if
// more than one start position is detected simultaneously.
// 
assign sd_in_vector = {d[18:10], in_reg[19:10], dly_reg[19:10], dly_reg2[19:10]};

// first level of gates

always @ (sd_in_vector)
    begin
        sd_trs_match1_l1[ 0] <=  &sd_in_vector[ 3: 0];
        sd_trs_match1_l1[ 1] <=  &sd_in_vector[ 4: 1];
        sd_trs_match1_l1[ 2] <=  &sd_in_vector[ 5: 2];
        sd_trs_match1_l1[ 3] <=  &sd_in_vector[ 6: 3];
        sd_trs_match1_l1[ 4] <=  &sd_in_vector[ 7: 4];
        sd_trs_match1_l1[ 5] <=  &sd_in_vector[ 8: 5];
        sd_trs_match1_l1[ 6] <=  &sd_in_vector[ 9: 6];
        sd_trs_match1_l1[ 7] <=  &sd_in_vector[10: 7];
        sd_trs_match1_l1[ 8] <=  &sd_in_vector[11: 8];
        sd_trs_match1_l1[ 9] <=  &sd_in_vector[12: 9];
        sd_trs_match1_l1[10] <=  &sd_in_vector[13:10];
        sd_trs_match1_l1[11] <=  &sd_in_vector[14:11];
        sd_trs_match1_l1[12] <=  &sd_in_vector[15:12];
        sd_trs_match1_l1[13] <=  &sd_in_vector[16:13];
        sd_trs_match1_l1[14] <=  &sd_in_vector[17:14];
        sd_trs_match1_l1[15] <=  &sd_in_vector[18:15];

        sd_trs_match2_l1[ 0] <= ~|sd_in_vector[13:10];
        sd_trs_match2_l1[ 1] <= ~|sd_in_vector[14:11];
        sd_trs_match2_l1[ 2] <= ~|sd_in_vector[15:12];
        sd_trs_match2_l1[ 3] <= ~|sd_in_vector[16:13];
        sd_trs_match2_l1[ 4] <= ~|sd_in_vector[17:14];
        sd_trs_match2_l1[ 5] <= ~|sd_in_vector[18:15];
        sd_trs_match2_l1[ 6] <= ~|sd_in_vector[19:16];
        sd_trs_match2_l1[ 7] <= ~|sd_in_vector[20:17];
        sd_trs_match2_l1[ 8] <= ~|sd_in_vector[21:18];
        sd_trs_match2_l1[ 9] <= ~|sd_in_vector[22:19];
        sd_trs_match2_l1[10] <= ~|sd_in_vector[23:20];
        sd_trs_match2_l1[11] <= ~|sd_in_vector[24:21];
        sd_trs_match2_l1[12] <= ~|sd_in_vector[25:22];
        sd_trs_match2_l1[13] <= ~|sd_in_vector[26:23];
        sd_trs_match2_l1[14] <= ~|sd_in_vector[27:24];
        sd_trs_match2_l1[15] <= ~|sd_in_vector[28:25];

        sd_trs_match3_l1[ 0] <= ~|sd_in_vector[23:20];
        sd_trs_match3_l1[ 1] <= ~|sd_in_vector[24:21];
        sd_trs_match3_l1[ 2] <= ~|sd_in_vector[25:22];
        sd_trs_match3_l1[ 3] <= ~|sd_in_vector[26:23];
        sd_trs_match3_l1[ 4] <= ~|sd_in_vector[27:24];
        sd_trs_match3_l1[ 5] <= ~|sd_in_vector[28:25];
        sd_trs_match3_l1[ 6] <= ~|sd_in_vector[29:26];
        sd_trs_match3_l1[ 7] <= ~|sd_in_vector[30:27];
        sd_trs_match3_l1[ 8] <= ~|sd_in_vector[31:28];
        sd_trs_match3_l1[ 9] <= ~|sd_in_vector[32:29];
        sd_trs_match3_l1[10] <= ~|sd_in_vector[33:30];
        sd_trs_match3_l1[11] <= ~|sd_in_vector[34:31];
        sd_trs_match3_l1[12] <= ~|sd_in_vector[35:32];
        sd_trs_match3_l1[13] <= ~|sd_in_vector[36:33];
        sd_trs_match3_l1[14] <= ~|sd_in_vector[37:34];
        sd_trs_match3_l1[15] <= ~|sd_in_vector[38:35];
    end

// second level of gates

always @ (sd_trs_match1_l1)
    begin
        sd_trs_match1[0] <= sd_trs_match1_l1[ 0] & sd_trs_match1_l1[ 4] & 
                            sd_trs_match1_l1[ 6];
        sd_trs_match1[1] <= sd_trs_match1_l1[ 1] & sd_trs_match1_l1[ 5] & 
                            sd_trs_match1_l1[ 7];
        sd_trs_match1[2] <= sd_trs_match1_l1[ 2] & sd_trs_match1_l1[ 6] & 
                            sd_trs_match1_l1[ 8];
        sd_trs_match1[3] <= sd_trs_match1_l1[ 3] & sd_trs_match1_l1[ 7] & 
                            sd_trs_match1_l1[ 9];
        sd_trs_match1[4] <= sd_trs_match1_l1[ 4] & sd_trs_match1_l1[ 8] & 
                            sd_trs_match1_l1[10];
        sd_trs_match1[5] <= sd_trs_match1_l1[ 5] & sd_trs_match1_l1[ 9] & 
                            sd_trs_match1_l1[11];
        sd_trs_match1[6] <= sd_trs_match1_l1[ 6] & sd_trs_match1_l1[10] & 
                            sd_trs_match1_l1[12];
        sd_trs_match1[7] <= sd_trs_match1_l1[ 7] & sd_trs_match1_l1[11] & 
                            sd_trs_match1_l1[13];
        sd_trs_match1[8] <= sd_trs_match1_l1[ 8] & sd_trs_match1_l1[12] & 
                            sd_trs_match1_l1[14];
        sd_trs_match1[9] <= sd_trs_match1_l1[ 9] & sd_trs_match1_l1[13] & 
                            sd_trs_match1_l1[15];
    end

always @ (sd_trs_match2_l1)
    begin
        sd_trs_match2[0] <= sd_trs_match2_l1[ 0] & sd_trs_match2_l1[ 4] & 
                            sd_trs_match2_l1[ 6];
        sd_trs_match2[1] <= sd_trs_match2_l1[ 1] & sd_trs_match2_l1[ 5] & 
                            sd_trs_match2_l1[ 7];
        sd_trs_match2[2] <= sd_trs_match2_l1[ 2] & sd_trs_match2_l1[ 6] & 
                            sd_trs_match2_l1[ 8];
        sd_trs_match2[3] <= sd_trs_match2_l1[ 3] & sd_trs_match2_l1[ 7] & 
                            sd_trs_match2_l1[ 9];
        sd_trs_match2[4] <= sd_trs_match2_l1[ 4] & sd_trs_match2_l1[ 8] & 
                            sd_trs_match2_l1[10];
        sd_trs_match2[5] <= sd_trs_match2_l1[ 5] & sd_trs_match2_l1[ 9] & 
                            sd_trs_match2_l1[11];
        sd_trs_match2[6] <= sd_trs_match2_l1[ 6] & sd_trs_match2_l1[10] & 
                            sd_trs_match2_l1[12];
        sd_trs_match2[7] <= sd_trs_match2_l1[ 7] & sd_trs_match2_l1[11] & 
                            sd_trs_match2_l1[13];
        sd_trs_match2[8] <= sd_trs_match2_l1[ 8] & sd_trs_match2_l1[12] & 
                            sd_trs_match2_l1[14];
        sd_trs_match2[9] <= sd_trs_match2_l1[ 9] & sd_trs_match2_l1[13] & 
                            sd_trs_match2_l1[15];
    end

always @ (sd_trs_match3_l1)
    begin
        sd_trs_match3[0] <= sd_trs_match3_l1[ 0] & sd_trs_match3_l1[ 4] & 
                            sd_trs_match3_l1[ 6];
        sd_trs_match3[1] <= sd_trs_match3_l1[ 1] & sd_trs_match3_l1[ 5] & 
                            sd_trs_match3_l1[ 7];
        sd_trs_match3[2] <= sd_trs_match3_l1[ 2] & sd_trs_match3_l1[ 6] & 
                            sd_trs_match3_l1[ 8];
        sd_trs_match3[3] <= sd_trs_match3_l1[ 3] & sd_trs_match3_l1[ 7] & 
                            sd_trs_match3_l1[ 9];
        sd_trs_match3[4] <= sd_trs_match3_l1[ 4] & sd_trs_match3_l1[ 8] & 
                            sd_trs_match3_l1[10];
        sd_trs_match3[5] <= sd_trs_match3_l1[ 5] & sd_trs_match3_l1[ 9] & 
                            sd_trs_match3_l1[11];
        sd_trs_match3[6] <= sd_trs_match3_l1[ 6] & sd_trs_match3_l1[10] & 
                            sd_trs_match3_l1[12];
        sd_trs_match3[7] <= sd_trs_match3_l1[ 7] & sd_trs_match3_l1[11] & 
                            sd_trs_match3_l1[13];
        sd_trs_match3[8] <= sd_trs_match3_l1[ 8] & sd_trs_match3_l1[12] & 
                            sd_trs_match3_l1[14];
        sd_trs_match3[9] <= sd_trs_match3_l1[ 9] & sd_trs_match3_l1[13] & 
                            sd_trs_match3_l1[15];
    end

//
// third level of gates generates a unary bit pattern indicating which offsets
// contain valid TRS symbols
//
assign sd_trs_match_all = sd_trs_match1 & sd_trs_match2 & sd_trs_match3;

//
// If any of the bits in sd_trs_match_all are asserted, the assert trs_detected        
//
assign sd_trs_detected = |sd_trs_match_all;

//
// The following asserts trs_error if more than one bit is set in 
// sd_trs_match_all
//
always @ (sd_trs_match_all)
    case (sd_trs_match_all)
        10'b00_0000_0000: sd_trs_err  <= 0;
        10'b00_0000_0001: sd_trs_err  <= 0;
        10'b00_0000_0010: sd_trs_err  <= 0;
        10'b00_0000_0100: sd_trs_err  <= 0;
        10'b00_0000_1000: sd_trs_err  <= 0;
        10'b00_0001_0000: sd_trs_err  <= 0;
        10'b00_0010_0000: sd_trs_err  <= 0;
        10'b00_0100_0000: sd_trs_err  <= 0;
        10'b00_1000_0000: sd_trs_err  <= 0;
        10'b01_0000_0000: sd_trs_err  <= 0;
        10'b10_0000_0000: sd_trs_err  <= 0;
        default:          sd_trs_err  <= 1;
    endcase

//
// The following assignments encode the sd_trs_match_all vector into a binary
// offset code.
//
assign sd_offset_val[0] = sd_trs_match_all[1] | sd_trs_match_all[3] | 
                          sd_trs_match_all[5] | sd_trs_match_all[7] | 
                          sd_trs_match_all[9];

assign sd_offset_val[1] = sd_trs_match_all[2] | sd_trs_match_all[3] | 
                          sd_trs_match_all[6] |
                          sd_trs_match_all[7];

assign sd_offset_val[2] = sd_trs_match_all[4] | sd_trs_match_all[5] | 
                          sd_trs_match_all[6] | sd_trs_match_all[7];

assign sd_offset_val[3] = sd_trs_match_all[8] | sd_trs_match_all[9];

//------------------------------------------------------------------------------
// Offset register & new start position detection
//

//
// HD/SD muxes for the trs_detected and offset_val signals
//
assign trs_detected = hd_sd ? sd_trs_detected : hd_trs_detected;
assign offset_val = hd_sd ? {1'b0, sd_offset_val} : hd_offset_val;

//
// offset_reg: barrel shifter offset register
//
// The offset_reg loads the offset_val whenever trs_detected is
// asserted and frame_en is asserted.
//
always @ (posedge clk or posedge rst)
    if (rst)
        offset_reg <= 0;
    else if (ce) 
        if (trs_detected & frame_en)
            offset_reg <= offset_val;

//
// New start position detector
// 
// A comparison between offset_val and offset_reg determines if
// the new offset is different than the current one. If there is
// a mismatch and frame_en is not asserted, then the nsp output
// will be asserted.
//
assign new_offset = offset_val != offset_reg;

always @ (posedge clk or posedge rst)
    if (rst)
        nsp <= 1;
    else if (ce)        
        if (trs_detected)
            nsp <= ~frame_en & new_offset;

//------------------------------------------------------------------------------
// Barrel shifter
//

//
// barrel shifter input register
//
always @ (posedge clk or posedge rst)
    if (rst)
        barrel_in <= 0;
    else if (ce)
        barrel_in <= hd_sd ? 
                     {dly_reg[18:0], 1'b0, dly_reg[18:10], dly_reg2[19:10]} : 
                     {dly_reg2[18:0], dly_reg3};  // 

//
// barrel shifter
//
// The barrel shifter extracts a 20-bit field from the bs_in vector.
// The bits extracted depend on the value of the offset_reg. 
//
assign bs_in = {12'b0000_0000_0000, barrel_in};
assign bs_sel_1 = offset_reg[4];
assign bs_sel_2 = offset_reg[3:2];
assign bs_sel_3 = offset_reg[1:0];

always @ (bs_in or bs_sel_1)
    for (i = 0; i < 35; i = i + 1)
        if (bs_sel_1)
            bs_1_out[i] <= bs_in[i + 16];
        else
            bs_1_out[i] <= bs_in[i];

always @ (bs_1_out or bs_sel_2)
    for (j = 0; j < 23; j = j + 1)
        case (bs_sel_2)
            2'b00: bs_2_out[j] <= bs_1_out[j];
            2'b01: bs_2_out[j] <= bs_1_out[j + 4];
            2'b10: bs_2_out[j] <= bs_1_out[j + 8]; 
            2'b11: bs_2_out[j] <= bs_1_out[j + 12];
        endcase

always @ (bs_2_out or bs_sel_3)
    for (k = 0; k < 20; k = k + 1)
        case (bs_sel_3)
            2'b00: barrel_out[k] <= bs_2_out[k];
            2'b01: barrel_out[k] <= bs_2_out[k + 1];
            2'b10: barrel_out[k] <= bs_2_out[k + 2];
            2'b11: barrel_out[k] <= bs_2_out[k + 3];
        endcase

//
// Output registers
//
always @ (posedge clk or posedge rst)
    if (rst)
        begin
            c_int <= 0;
            y_int <= 0;
        end
    else if (ce)
        begin
            c_int <= barrel_out[9:0];
            y_int <= hd_sd ? barrel_out[9:0] : barrel_out[19:10];
        end

assign c = c_int;
assign y = y_int;

//
// trs: trs output generation logic
//
// The trs_out register is a 4-bit shift register which shifts every time
// the bit_cntr[0] bit is asserted. The trs output signal is the OR of
// the four bits in this register so it becomes asserted when the first
// character of the TRS symbol is output and remains asserted for the
// following three characters of the TRS symbol.
//
always @ (posedge clk or posedge rst)
    if (rst)
        trs_out <= 0;
    else if (ce)
        trs_out <= {trs_detected, trs_out[3:1]};

always @ (posedge clk or posedge rst)
    if (rst)
        trs <= 0;
    else if (ce)
        trs <= |trs_out;

always @ (posedge clk or posedge rst)
    if (rst)
        xyz_int <= 1'b0;
    else if (ce)
        xyz_int <= trs_out[0];

assign xyz = xyz_int;
assign eav = xyz_int & y_int[6];
assign sav = xyz_int & ~y_int[6];

//
// TRS error detector
//
// This code examines the protection bits in the XYZ word and asserts the 
// trs_err output if an error is detected.
//
assign hd_trs_err = xyz_int & (
                    (y_int[5] ^ y_int[6] ^ y_int[7]) |
                    (y_int[4] ^ y_int[8] ^ y_int[6]) |
                    (y_int[3] ^ y_int[8] ^ y_int[7]) |
                    (y_int[2] ^ y_int[8] ^ y_int[7] ^ y_int[6]) |
                    ~y_int[9] | y_int[1] | y_int[0]);

assign trs_err = hd_sd ? sd_trs_err : hd_trs_err;

endmodule
