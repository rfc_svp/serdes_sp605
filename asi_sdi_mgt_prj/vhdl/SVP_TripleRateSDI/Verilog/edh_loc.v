//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: edh_loc.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:29:24-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: edh_loc.v,rcs $
// Revision 1.1  2004-12-15 11:29:24-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module examines the vcnt and hcnt values to determine when it is time for
an EDH packet to appear in the video stream. The signal edh_next is asserted
during the sample before the first location of the first ADF word of the
EDH packet.

The output of this module is used to determine if EDH packets are missing from
the input video stream and to determine when to insert EDH packets into the
output video stream.
*/

`timescale 1ns / 1 ns

module  edh_loc (
    // inputs
    clk,            // clock input
    ce,             // clock enable
    rst,            // async reset input
    f,              // field bit
    vcnt,           // vertical line count
    hcnt,           // horizontal sample count
    std,            // indicates the video standard

    // outputs
    edh_next        // EDH packet should begin on next sample
);


//-----------------------------------------------------------------------------
// Parameter definitions
//

//
// This group of parameters defines the bit widths of various fields in the
// module. 
//
parameter HCNT_WIDTH    = 12;                   // Width of hcnt
parameter VCNT_WIDTH    = 10;                   // Width of vcnt
 
parameter HCNT_MSB      = HCNT_WIDTH - 1;       // MS bit # of hcnt
parameter VCNT_MSB      = VCNT_WIDTH - 1;       // MS bit # of vcnt


//
// This group of parameters defines the encoding for the video standards output
// code.
//
parameter [2:0]
    NTSC_422        = 3'b000,
    NTSC_INVALID    = 3'b001,
    NTSC_422_WIDE   = 3'b010,
    NTSC_4444       = 3'b011,
    PAL_422         = 3'b100,
    PAL_INVALID     = 3'b101,
    PAL_422_WIDE    = 3'b110,
    PAL_4444        = 3'b111;


//
// This group of parameters defines the line numbers where the EDH packet is
// located.
//
parameter NTSC_FLD1_EDH_LINE = 272;
parameter NTSC_FLD2_EDH_LINE =   9;
parameter PAL_FLD1_EDH_LINE  = 318;
parameter PAL_FLD2_EDH_LINE  =   5;
         
//
// This group of parameters defines the word count two words before the
// start of the EDH packet for each different supported video standard. First,
// the position of the SAV is defined, then the EDH packet position is defined
// relative to the SAV. A point two words counts before the start of the EDH
// packet is used because the edh_next must be asserted the count before the
// EDH plus there is one cycle of clock latency.
//

parameter SAV_NTSC_422      = 1712;
parameter SAV_NTSC_422_WIDE = 2284;
parameter SAV_NTSC_4444     = 3428;
parameter SAV_PAL_422       = 1724;
parameter SAV_PAL_422_WIDE  = 2300;
parameter SAV_PAL_4444      = 3452;

parameter EDH_PACKET_LENGTH = 23;

parameter EDH_NTSC_422      = SAV_NTSC_422 - EDH_PACKET_LENGTH - 2;
parameter EDH_NTSC_422_WIDE = SAV_NTSC_422_WIDE - EDH_PACKET_LENGTH - 2;
parameter EDH_NTSC_4444     = SAV_NTSC_4444 - EDH_PACKET_LENGTH - 2;
parameter EDH_PAL_422       = SAV_PAL_422 - EDH_PACKET_LENGTH - 2;
parameter EDH_PAL_422_WIDE  = SAV_PAL_422_WIDE - EDH_PACKET_LENGTH - 2;
parameter EDH_PAL_4444      = SAV_PAL_4444 - EDH_PACKET_LENGTH - 2;
        


//-----------------------------------------------------------------------------
// Port definitions
//
input                   clk;
input                   ce;
input                   rst;
input                   f;
input   [VCNT_MSB:0]    vcnt;
input   [HCNT_MSB:0]    hcnt;
input   [2:0]           std;
output                  edh_next;

reg                     edh_next;

//-----------------------------------------------------------------------------
// Signal definitions
//
wire                    ntsc;           // 1 = NTSC, 0 = PAL
reg     [VCNT_MSB:0]    edh_line_num;   // EDH occurs on this line number
wire                    edh_line;       // asserted when vcnt == edh_line_num
reg     [HCNT_MSB:0]    edh_hcnt;       // EDH begins sample after this value
wire                    edh_next_d;     // asserted when next sample begins EDH

//
// EDH vertical position detector
// 
// The following code determines when the current video line number (vcnt)
// matches the line where the next EDH packet location occurs. The line numbers
// for the EDH packets are different for NTSC and PAL video standards. Also,
// there is one EDH per field, so the field bit (f) is used to determine the
// line number of the next EDH packet.
//
assign ntsc = (std == NTSC_422) || (std == NTSC_INVALID) ||
              (std == NTSC_422_WIDE) || (std == NTSC_4444);

always @ (ntsc or f)
    if (ntsc)
        begin
            if (~f)
                edh_line_num = NTSC_FLD2_EDH_LINE;
            else
                edh_line_num = NTSC_FLD1_EDH_LINE;
        end
    else
        begin
            if (~f)
                edh_line_num = PAL_FLD2_EDH_LINE;
            else
                edh_line_num = PAL_FLD1_EDH_LINE;
        end
            
assign edh_line = vcnt == edh_line_num;

//
// EDH horizontal position detector
//
// This code matches the current horizontal count (hcnt) with the word count
// of the next EDH location. The location of the EDH packet is immediately 
// before the SAV. edh_next_d is asserted when both the vcnt and hcnt match
// the EDH packet location.
//
always @ (std)
    case(std)
        NTSC_422:       edh_hcnt = EDH_NTSC_422;
        NTSC_422_WIDE:  edh_hcnt = EDH_NTSC_422_WIDE;
        NTSC_4444:      edh_hcnt = EDH_NTSC_4444;
        PAL_422:        edh_hcnt = EDH_PAL_422;
        PAL_422_WIDE:   edh_hcnt = EDH_PAL_422_WIDE;
        PAL_4444:       edh_hcnt = EDH_PAL_4444;
        default:        edh_hcnt = EDH_NTSC_422;
    endcase

assign edh_next_d = edh_line & (edh_hcnt == hcnt);

//
// output register
//
always @ (posedge clk or posedge rst)
    if (rst)
        edh_next <= 1'b0;
    else if (ce)
        edh_next <= edh_next_d;
                            
endmodule