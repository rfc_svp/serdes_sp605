//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow
//  \   \        Filename: $RCSfile: main_avb_control.v,rcs $
//  /   /        Date Last Modified:  $Date: 2010-02-10 11:27:14-07 $
// /___/   /\    Date Created: July 13, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: main_avb_control.v,rcs $
// Revision 1.2  2010-02-10 11:27:14-07  jsnow
// Added dynamic Si5324 bandwidth selection capability.
//
// Revision 1.1  2010-01-11 10:22:24-07  jsnow
// Expanded the Si5324 output frequency select port to 4 bits.
//
// Revision 1.0  2009-11-19 14:56:08-07  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module for FMC carrier boards provides the main control & status for the
Xilinx AVB FMC card.

*/

`timescale 1ns / 1 ns

module main_avb_control 
( 
// Master clock
    input   wire            clk,                    // 27 MHz clock from the AVB FMC card
    input   wire            rst,

// Main SPI interface to FMC card
    output  wire            sck,                    // SPI SCK
    output  wire            mosi,                   // master-out slave-in serial data
    input   wire            miso,                   // master-in slave-out serial data
    output  wire            ss,                     // slave select -- asserted low
    
// General status signals
    output  reg [7:0]       fpga_rev = 0,           // AVB FPGA revision
    output  reg             exp_brd_prsnt = 1'b0,   // 1 if expansion board is present
    output  reg [7:0]       board_options = 0,      // Indicates board strapping options

// Clock XBAR control signals
//
// For XBAR 1, each output can be driven by any of the four inputs as follows:
//      00 selects clock from Si5324
//      01 selects clock module L CLK OUT 1
//      10 selects clock module L CLK OUT 2
//      11 selects OUT 0 of XBAR 3
// 
    input   wire [1:0]      xbar1_out0_sel,         // Drives FMC GBTCLK0_M2C       
    input   wire [1:0]      xbar1_out1_sel,         // Drives FMC CLK2_M2C  
    input   wire [1:0]      xbar1_out2_sel,         // Drives FMC CLK1_M2C
    input   wire [1:0]      xbar1_out3_sel,         // Drives clock module L CLK IN 4
//
// For XBAR 2, each output can be driven by any of the four inuts as follows:
//      00 selects OUT 3 of XBAR 3
//      01 selects clock module H CLK OUT 1
//      10 selects clock module H CLK OUT 2
//      11 selects clock module H CLK OUT 3
//
    input   wire [1:0]      xbar2_out0_sel,         // Drives FMC GBTCLK1_M2C
    input   wire [1:0]      xbar2_out1_sel,         // Drives FMC HB17
    input   wire [1:0]      xbar2_out2_sel,         // Drives FMC HA17
    input   wire [1:0]      xbar2_out3_sel,         // Drives clock module H CLK IN 4
//
// For XBAR 3, each output can be driven by any of the four inputs as follows:
//      00 selects FMC HA19
//      01 selects FMC LA22
//      10 selects FMC DP0 (LPC compatible MGT)
//      11 selects FMC DP1 (HPC compatible MGT)
//
    input   wire [1:0]      xbar3_out0_sel,         // Drives IN 3 of XBAR 1
    input   wire [1:0]      xbar3_out1_sel,         // Drives SDI TX1 cable driver
    input   wire [1:0]      xbar3_out2_sel,         // Drives SDI TX2 cable driver
    input   wire [1:0]      xbar3_out3_sel,         // Drives IN 0 of XBAR 2

// Si5324 Status & Control
//
// The Si5324_clkin_sel port controls the clock input selection for the Si5324.
// There are three possible clock sources: 27 MHz XO, FPGA signal, and the HSYNC
// signal from the clock separator. If the HSYNC signal is chosen, the device can be
// put into auto frequency select mode where the controller automatically determines
// the external HSYNC frequency and selects the proper frequency synthesis
// settings to produce 27 MHz out of the Si5324. If Si5324_clkin_sel is anything
// other than 01 (auto HSYNC mode), the frequency synthesis of the Si5324 is
// controlled by the Si5324_in_fsel and Si5324_out_fsel ports as follows:
//
//      Si5324_in_fsel[4:0] select the input frequency:
//          0x00: 480i (NTSC) HSYNC
//          0x01: 480p HSYNC
//          0x02: 576i (PAL) HSYNC
//          0x03: 576p HSYNC
//          0x04: 720p 24 Hz HSYNC
//          0x05: 720p 23.98 Hz HSYNC
//          0x06: 720p 25 Hz HSYNC
//          0x07: 720p 30 Hz HSYNC
//          0x08: 720p 29.97 Hz HSYNC
//          0x09: 720p 50 Hz HSYNC
//          0x0A: 720p 60 Hz HSYNC
//          0x0B: 720p 59.94 Hz HSYNC
//          0x0C: 1080i 50 Hz HSYNC
//          0x0D: 1080i 60 Hz HSYNC
//          0x0E: 1080i 59.94 Hz HSYNC
//          0x0F: 1080p 24 Hz HSYNC
//          0x10: 1080p 23.98 Hz HSYNC
//          0x11: 1080p 25 Hz HSYNC
//          0x12: 1080p 30 Hz HSYNC
//          0x13: 1080p 29.97 Hz HSYNC
//          0x14: 1080p 50 Hz HSYNC
//          0x15: 1080p 60 Hz HSYNC
//          0x16: 1080p 59.94 Hz HSYNC
//          0x17: 27 MHz
//          0x18: 74.25 MHz
//          0x19: 74.25/1.001 MHz
//          0x1A: 148.5 MHz
//          0x1B: 148.5/1.001 MHz
//
//      Si5324_out_fsel[3:0] select the output frequency:
//          0x0: 27 MHz
//          0x1: 74.25 MHz
//          0x2: 74.25/1.001 MHz
//          0x3: 148.5 MHz
//          0x4: 148.5/1.001 MHz
//          0x5: 24.576 MHz
//          0x6: 148.5/1.0005 MHz
//          0x7: Invalid
//          0x8: 297 MHz
//          0x9: 297/1.001 MHz
//
// Note that any HSYNC frequency can only be converted to 27 MHz. Choosing any
// output frequency except 27 MHz when the input selection is 0x00 through 0x16
// will result in an error. Any input frequency selected by 0x17 through 0x1B
// can be converted to any output frequency, with the exception that the
// 74.25/1.001 and 148.5/1.001 MHz input frequencies can't be converted to 
// 24.576 MHz.
//
// For custom frequency synthesis, use the Si5324 register peek/poke facility
// to modify individual registers on a custom basis.
//
// The Si5324_bw_sel port selects the bandwidth of the Si5324 device. The
// bandwidth selection must be a legal bandwidth value for the specified
// input/output frequency combination.
//
    input   wire            Si5324_reset,           // 1 resets Si5324
    input   wire [1:0]      Si5324_clkin_sel,       // Control input clock source selection for Si5324
                                                    // 00=27 MHz, 01=sync sep HSYNC (auto fsel mode)
                                                    // 10=FMC LA29, 11=sync sep HSYNC (manual fsel mode)
    input   wire [3:0]      Si5324_out_fsel,        // selects the output frequency
    input   wire [4:0]      Si5324_in_fsel,         // selects the input frequency
    input   wire [3:0]      Si5324_bw_sel,          // bandwidth select
    input   wire            Si5324_DHOLD,           // 1 puts the Si5324 in digital hold mode
    output  reg             Si5324_FOS2 = 1'b0,     // 1=frequency offset alarm for CKIN2
    output  reg             Si5324_FOS1 = 1'b0,     // 1=frequency offset alram for CKIN1
    output  reg             Si5324_LOL = 1'b0,      // 0=PLL locked, 1=PLL unlocked

    input   wire [7:0]      Si5324_reg_adr,         // Si5324 peek/poke register address
    input   wire [7:0]      Si5324_reg_wr_dat,      // Si5324 peek/poke register write data
    output  reg  [7:0]      Si5324_reg_rd_dat = 0,  // Si5324 peek/poke register read data
    input   wire            Si5324_reg_wr,          // Si5324 poke request, assert High for one clk
    input   wire            Si5324_reg_rd,          // Si5324 peek request, assert High for one clk
    output  reg             Si5324_reg_rdy = 1'b0,  // Si5324 peek/poke cycle done when 1
    output  reg             Si5324_error = 1'b0,    // Si5324 peek/poke error when 1 (transfer was NACKed on I2C bus)

//
// These ports are associated with the LMH1981 sync separator.  Note that the
// actual sync signals are available directly to the FPGA via FMC signals. The
// sync_video_frame value is a count of the number of lines in a field or frame
// as captured directly by the LMH1981. The sync_m and sync_frame_rate indicate
// the frame rate of the video signal as shown below. 
//
//      sync_frame_rate     Frame Rate      sync_m
//              000         23.98 Hz            1
//              001         24 Hz               0
//              010         25 Hz               0
//              011         29.97 Hz            1
//              100         30 Hz               0
//              101         50 Hz               0
//              110         59.94 Hz            1
//              111         60 Hz               0
//
    output  reg  [10:0]     sync_video_fmt = 0,     // count of lines per field/frame
    output  reg             sync_updating = 1'b0,   // sync_video_frame only valid when this port is 0
    output  reg  [2:0]      sync_frame_rate = 0,    // frame rate indicator
    output  reg             sync_m = 1'b0,          // 1 = frame rate is 1000/1001
    output  reg             sync_err = 1'b0,        // 1 = error detected frame rate

//
// LED control ports
//

// The eight two-color LEDs associated with the SDI RX connectors are controlled
// by 2 bits each as follows:
//      00 = off
//      01 = green
//      10 = red
//      11 = controlled by cable EQ CD signal (green when carrier detected, else red)
//
    input   wire [1:0]      sdi_rx1_led,            // controls the SDI RX1 LED
    input   wire [1:0]      sdi_rx2_led,            // controls the SDI RX2 LED
    input   wire [1:0]      sdi_rx3_led,            // controls the SDI RX3 LED
    input   wire [1:0]      sdi_rx4_led,            // controls the SDI RX4 LED
    input   wire [1:0]      sdi_rx5_led,            // controls the SDI RX5 LED
    input   wire [1:0]      sdi_rx6_led,            // controls the SDI RX6 LED
    input   wire [1:0]      sdi_rx7_led,            // controls the SDI RX7 LED
    input   wire [1:0]      sdi_rx8_led,            // controls the SDI RX8 LED

// All other LEDs have separate 2-bit control ports for both the red and green LEDs
// so that the red and green sides of the LED are independently controlled like this:
//      00 = off
//      01 = on
//      10 = flash slowly
//      11 = flash quickly
//
    input   wire [1:0]      sdi_tx1_red_led,        // controls the SDI TX1 red LED
    input   wire [1:0]      sdi_tx1_grn_led,        // controls the SDI TX1 green LED
    input   wire [1:0]      sdi_tx2_red_led,        // controls the SDI TX2 red LED
    input   wire [1:0]      sdi_tx2_grn_led,        // controls the SDI TX2 green LED
    input   wire [1:0]      sdi_tx3_red_led,        // controls the SDI TX3 red LED
    input   wire [1:0]      sdi_tx3_grn_led,        // controls the SDI TX3 green LED
    input   wire [1:0]      sdi_tx4_red_led,        // controls the SDI TX4 red LED
    input   wire [1:0]      sdi_tx4_grn_led,        // controls the SDI TX4 green LED
    input   wire [1:0]      sdi_tx5_red_led,        // controls the SDI TX5 red LED
    input   wire [1:0]      sdi_tx5_grn_led,        // controls the SDI TX5 green LED
    input   wire [1:0]      sdi_tx6_red_led,        // controls the SDI TX6 red LED
    input   wire [1:0]      sdi_tx6_grn_led,        // controls the SDI TX6 green LED
    input   wire [1:0]      sdi_tx7_red_led,        // controls the SDI TX7 red LED
    input   wire [1:0]      sdi_tx7_grn_led,        // controls the SDI TX7 green LED
    input   wire [1:0]      sdi_tx8_red_led,        // controls the SDI TX8 red LED
    input   wire [1:0]      sdi_tx8_grn_led,        // controls the SDI TX8 green LED

    input   wire [1:0]      aes_rx1_red_led,        // controls the AES3 RX1 red LED
    input   wire [1:0]      aes_rx1_grn_led,        // controls the AES3 RX1 green LED
    input   wire [1:0]      aes_rx2_red_led,        // controls the AES3 RX2 red LED
    input   wire [1:0]      aes_rx2_grn_led,        // controls the AES3 RX2 green LED
    input   wire [1:0]      aes_tx1_red_led,        // controls the AES3 TX1 red LED
    input   wire [1:0]      aes_tx1_grn_led,        // controls the AES3 TX1 green LED
    input   wire [1:0]      aes_tx2_red_led,        // controls the AES3 TX2 red LED
    input   wire [1:0]      aes_tx2_grn_led,        // controls the AES3 TX2 green LED
    input   wire [1:0]      madi_rx_red_led,        // controls the MADI RX red LED
    input   wire [1:0]      madi_rx_grn_led,        // controls the MADI RX green LED
    input   wire [1:0]      madi_tx_red_led,        // controls the MADI TX red LED
    input   wire [1:0]      madi_tx_grn_led,        // controls the MADI TX green LED
    input   wire [1:0]      sync_red_led,           // controls the external sync red LED
    input   wire [1:0]      sync_grn_led,           // controls the external sync green LED
    
// SDI Cable EQ control & status
//
// In the first two ports, there is one bit for each possible cable EQ device with
// bit 0 for SDI RX1 and bit 7 for SDI RX8.
//
    output  reg  [7:0]      sdi_eq_cd_n = 0,        // carrier detects from cable drivers, asserted low
    input   wire [7:0]      sdi_eq_ext_3G_reach,    // Enable bits for extended 3G reach mode, 1=enable, 0=disable
    input   wire [2:0]      sdi_eq_select,          // selects which EQ's status signals drive port below
    output  reg  [4:0]      sdi_eq_cli = 0,         // cable length indicator

// SDI Cable Driver control & status
//
// For these ports, there is one bit for each possible cable driver device with
// bit 0 for SDI TX1 and bit 7 for SDI TX8.
//
    input   wire [7:0]      sdi_drv_hd_sd,          // Sets slew rate of each cable driver, 1=SD, 0=HD/3G
    input   wire [7:0]      sdi_drv_enable,         // 1 enables the driver, 0 powers driver down
    output  reg  [7:0]      sdi_drv_fault_n = 0     // 1 = normal operation, 0 = fault
);

//
// Internal signal definitions
//
wire    [7:0]       port_id;            // PicoBlaze port ID output
wire                write_strobe;       // PicoBlaze write strobe
wire                read_strobe;        // PicoBlaze read strobe
wire    [7:0]       output_port;        // PicoBlaze output port
reg     [7:0]       input_port = 0;     // PicoBlaze input port
reg     [2:0]       spi_reg = 3'b100;   // [SS,SCK,MOSI] control bits from pBlaze
wire    [9:0]       address;
wire    [17:0]      instruction;

reg                 Si5324_reg_wr_req = 1'b0;
reg                 Si5324_reg_rd_req = 1'b0;
wire [7:0]          Si5324_fsel;

assign ss   = spi_reg[2];
assign sck  = spi_reg[1];
assign mosi = spi_reg[0];

avbspi MAIN_SPI_CODEROM (
    .address            (address),
    .instruction        (instruction),
    .clk                (clk));

//
// PicoBlaze processor
//
kcpsm3 MAIN_SPI_PICO (
    .address            (address),
    .instruction        (instruction),
    .port_id            (port_id),
    .write_strobe       (write_strobe),
    .out_port           (output_port),
    .read_strobe        (read_strobe),
    .in_port            (input_port),
    .interrupt          (1'b0),
    .interrupt_ack      (),
    .reset              (rst),
    .clk                (clk));

//
// PicoBlaze output port registers
//
// SPI control register
//
always @ (posedge clk)
    if (rst)
        spi_reg <= 3'b100;
    else if (write_strobe & ~port_id[7])
        spi_reg <= output_port[2:0];

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h80))
        fpga_rev <= output_port;

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h81))
        exp_brd_prsnt <= output_port[0];

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h82))
        board_options <= output_port;

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h83))
    begin
        Si5324_FOS2 <= output_port[2];
        Si5324_FOS1 <= output_port[1];
        Si5324_LOL  <= output_port[0];
    end

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h84))
        sync_video_fmt[7:0] <= output_port;

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h85))
    begin
        sync_video_fmt[10:8] <= output_port[2:0];
        sync_updating        <= output_port[7];
    end

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h86))
    begin
        sync_frame_rate <= output_port[2:0];
        sync_m          <= output_port[6];
        sync_err        <= output_port[7];
    end

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h87))
        sdi_eq_cd_n <= output_port;
    
always @ (posedge clk)
    if (write_strobe & (port_id == 8'h88))
        sdi_eq_cli <= output_port[4:0];

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h89))
        sdi_drv_fault_n <= output_port;

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h94))
        Si5324_reg_rd_dat <= output_port;

always @ (posedge clk)
    if (Si5324_reg_wr)
        Si5324_reg_wr_req <= 1'b1;
    else if (read_strobe & (port_id == 8'h95))
        Si5324_reg_wr_req <= 1'b0;

always @ (posedge clk)
    if (Si5324_reg_rd)
        Si5324_reg_rd_req <= 1'b1;
    else if (read_strobe & (port_id == 8'h95))
        Si5324_reg_rd_req <= 1'b0;

always @ (posedge clk)
    if (Si5324_reg_rd | Si5324_reg_wr)
        Si5324_reg_rdy <= 1'b0;
    else if (write_strobe & (port_id == 8'h96))
        Si5324_reg_rdy <= 1'b1;

always @ (posedge clk)
    if (write_strobe & (port_id == 8'h96))
        Si5324_error <= output_port[6];

//
// PicoBlaze Input Port Mux
//
always @ (posedge clk)
    if (~port_id[7])
        input_port <= {7'b0, miso};
    else 
        case(port_id[4:0])
            5'b00000:   input_port <= {xbar1_out3_sel, xbar1_out2_sel, xbar1_out1_sel, xbar1_out0_sel};
            5'b00001:   input_port <= {xbar2_out3_sel, xbar2_out2_sel, xbar2_out1_sel, xbar2_out0_sel};
            5'b00010:   input_port <= {xbar3_out3_sel, xbar3_out2_sel, xbar3_out1_sel, xbar3_out0_sel};
            5'b00011:   input_port <= {3'b0, Si5324_DHOLD, 1'b0, Si5324_reset, Si5324_clkin_sel};
            5'b00100:   input_port <= Si5324_fsel;
            5'b00101:   input_port <= {sdi_rx4_led, sdi_rx3_led, sdi_rx2_led, sdi_rx1_led};
            5'b00110:   input_port <= {sdi_rx8_led, sdi_rx7_led, sdi_rx6_led, sdi_rx5_led};
            5'b00111:   input_port <= {sdi_tx2_red_led, sdi_tx2_grn_led, sdi_tx1_red_led, sdi_tx1_grn_led};
            5'b01000:   input_port <= {sdi_tx4_red_led, sdi_tx4_grn_led, sdi_tx3_red_led, sdi_tx3_grn_led};
            5'b01001:   input_port <= {sdi_tx6_red_led, sdi_tx6_grn_led, sdi_tx5_red_led, sdi_tx5_grn_led};
            5'b01010:   input_port <= {sdi_tx8_red_led, sdi_tx8_grn_led, sdi_tx7_red_led, sdi_tx7_grn_led};
            5'b01011:   input_port <= {aes_rx2_red_led, aes_rx2_grn_led, aes_rx1_red_led, aes_rx1_grn_led};
            5'b01100:   input_port <= {aes_tx2_red_led, aes_tx2_grn_led, aes_tx1_red_led, aes_tx1_grn_led};
            5'b01101:   input_port <= {madi_tx_red_led, madi_tx_grn_led, madi_rx_red_led, madi_rx_grn_led};
            5'b01110:   input_port <= {4'b0, sync_red_led, sync_grn_led};
            5'b01111:   input_port <= sdi_eq_ext_3G_reach;
            5'b10000:   input_port <= {5'b0, sdi_eq_select};
            5'b10001:   input_port <= sdi_drv_hd_sd;
            5'b10010:   input_port <= sdi_drv_enable;
            5'b10011:   input_port <= Si5324_reg_adr;
            5'b10100:   input_port <= Si5324_reg_wr_dat;
            5'b10101:   input_port <= {6'b0, Si5324_reg_rd_req, Si5324_reg_wr_req};
            5'b10110:   input_port <= {4'b0, Si5324_bw_sel};
        endcase

//
// Si5324 frequency select mapping
//
// For all output frequencies selected by Si5324_out_fsel codes 0 through 7,
// the mapping is simply the concatenation of the LS 3 bits of Si5324_out_fsel
// with the 5 bits of Si5324_in_fsel, just as was previously done when
// Si53234_out_fsel was just 3 bits. However, if bit 3 of Si5324_out_fsel is 1,
// use a mapping looking up ROM.
//

Si5324_fsel_lookup FSEL (
    .clk            (clk),
    .in_fsel        (Si5324_in_fsel),
    .out_fsel       (Si5324_out_fsel),
    .fsel           (Si5324_fsel));

endmodule

