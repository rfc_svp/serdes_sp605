//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: Reed P. Tidwell
//  \   \        Filename: $RCSfile: s6gtp_sdi_control.v,v $
//  /   /        Date Last Modified:  $Date: 2010-02-23 16:45:59-07 $
// /___/   /\    Date Created: May 27.2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: s6gtp_sdi_control.v,v $
// Revision 1.5  2010-02-23 16:45:59-07  reedt
// Feb. 2010 full release.
//
// Revision 1.4  2010-01-20 16:57:51-07  reedt
// Changed TXDIVSEL value for HD to support 20-bit GTP TX interface.
//
// Revision 1.3  2010-01-13 18:17:53-07  reedt
// Removed refclk selection ports.  Prior to going to 20-big GTP interface.
//
// Revision 1.2  2009-12-08 12:15:42-07  reedt
// <>
//
// Revision 1.1  2009-11-18 14:29:01-07  reedt
// Resets and rate detect working for triple-rate RX
//
// Revision 1.0  2009-11-12 15:32:54-07  reedt
// Initial revision
//
// Revision 1.0  2009-07-01 16:13:12-06  reedt
// Initial revision
//
//------------------------------------------------------------------------------ 
// This file contains confidential and proprietary information of Xilinx, Inc. 
// and is protected under U.S. and international copyright and other 
// intellectual property laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any rights to the 
// materials distributed herewith. Except as otherwise provided in a valid 
// license issued to you by Xilinx, and to the maximum extent permitted by 
// applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL 
// FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, 
// IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF 
// MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; 
// and (2) Xilinx shall not be liable (whether in contract or tort, including 
// negligence, or under any other theory of liability) for any loss or damage of 
// any kind or nature related to, arising under or in connection with these 
// materials, including for any direct, or any indirect, special, incidental, 
// or consequential loss or damage (including loss of data, profits, goodwill, 
// or any type of loss or damage suffered as a result of any action brought by 
// a third party) even if such damage or loss was reasonably foreseeable or 
// Xilinx had been advised of the possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-safe, or for use in 
// any application requiring fail-safe performance, such as life-support or 
// safety devices or systems, Class III medical devices, nuclear facilities, 
// applications related to the deployment of airbags, or any other applications 
// that could lead to death, personal injury, or severe property or 
// environmental damage (individually and collectively, "Critical Applications").
// Customer assumes the sole risk and liability of any use of Xilinx products in
// Critical Applications, subject only to applicable laws and regulations 
// governing limitations on product liability. 
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT
// ALL TIMES.
//
//------------------------------------------------------------------------------ 
/*
Module Description:

This module provides several functions required when using the Spartan-6 GTP
to receiver and/or transmit the SMPTE SDI protocols: SD-SDI, HD-SDI, and
3G-SDI. The functions provided are;

1. A DRP controller that controls the mode of operation and the selection 
and routing of the GTP's reference clocks.

2. Reset logic.

3. Rx bit rate detection.
--------------------------------------------------------------------------------
*/

`timescale 1ns / 1ps

module s6gtp_sdi_control #(
    //
    // The GTP_RESET_CNTR_SIZE parameter specifies the size (number of bits) of 
    // the GTP reset delay counter. This counter is reset to 0 when gtp_reset_in
    // is 1. When gtp_reset_in goes low, the counter will be counting and the 
    // gtpreset output signal will be 1 until the terminal count is reached. The
    // terminal  count occurs when the MSB of the counter becomes 1. The reset 
    // counter is clocked by the dclk clock input, so the delay count is dclk 
    // frequency / (2^GTP_RESET_CNTR_SIZE). The CBL_DISCONNECT_RESET_CNTR_SIZE 
    // parameter specifies the size of the cable disconnect reset duration 
    // counter in terms of DCLK cycles.
    //
    parameter GTP_RESET_CNTR_SIZE           = 17,
    parameter CBL_DISCONNECT_RST_CNTR_SIZE  = 12,

    //
    // This group of parameters specifies the PMA_RX_CFG to be used for each of
    // the three SDI modes of operation: HD, SD, and 3G. Normally, the default
    // values given here should be used.
    //
    parameter PMA_RX_CFG_HD                 = 25'h05ce059, // 59
    parameter PMA_RX_CFG_SD                 = 25'h05ce000,
    parameter PMA_RX_CFG_3G                 = 25'h05ce089,

    //
    // This group of parameters specifies the RXDIVSEL_OUT divider value to be
    // used for each of the three SDI modes of operation. The default values
    // given here should normally be used. They default to divide by 2 for HD
    // (1.485 Gb/s line rate. 148.5 refclk) ,  SD (1.485 Gb/s line rate,
    // 27MHz MHz refclk) and 3G (2.97 Gb/s line rate, 297 MHz refclk).
    //
    //  
    parameter PLL_RXDIVSEL_OUT_HD           = 2'b01,
    parameter PLL_RXDIVSEL_OUT_SD           = 2'b01,
    parameter PLL_RXDIVSEL_OUT_3G           = 2'b00,

    //
    // This group of parameters specifies the TXDIVSEL_OUT divider value to be
    // used for each of the three SDI modes of operation. The default value
    // given here should normally be used. The default for all line rates is 
    // divide by 1.  This results in a 2.97 Gb/s line rate with a 148.5 MHz 
    // refclk.  This is the normal 3G rate.   HD is sent in an oversampled 
    // fashion with 2X oversampling (1.485 Gb/s data rete).  SD is oversampled
    // at 11X (270 MHz data rate).
    //  
    parameter PLL_TXDIVSEL_OUT_HD           = 2'b01,
    parameter PLL_TXDIVSEL_OUT_SD           = 2'b00,
    parameter PLL_TXDIVSEL_OUT_3G           = 2'b00, 

    // 
    // This parameter specifies the frequency of the reference clock used to
    // detect the HD and 3G bit rates. The default value is 148.5 MHz.
    //
    parameter RATE_REFCLK_FREQ              = 33333333)
(
    input   wire        dclk,           // DRP clock
    input   wire        rate_refclk,    // rate detection reference clock
    input   wire        rx0_usrclk,     // RXUSRCLK for Rx0
    input   wire        rx1_usrclk,     // RXUSRCLK for Rx1
    input   wire        tx0_usrclk,     // TXUSRCLK for Tx0
    input   wire        tx1_usrclk,     // TXUSRCLK for Tx1
    input   wire        drst,           // async reset for DRP controller

//
// These inputs control the reference clock selection and reference clock 
// routing through the GTP.
//
    input   wire [1:0]  rx0_mode,       // RX0 mode select: 00=HD, 01=SD, 10=3G
    input   wire [1:0]  rx1_mode,       // RX1 mode select: 00=HD, 01=SD, 10=3G
    input   wire [1:0]  tx0_mode,       // TX0 mode select: 00=HD, 01=SD, 10=3G
    input   wire [1:0]  tx1_mode,       // TX1 mode select: 00=HD, 01=SD, 10=3G

    output  wire        tx0_slew,       // controls slew rate of Tx0 cable driver
    output  wire        tx1_slew,       // controls slew rate of Tx1 cable driver
    
// 
// Rate detection outputs
//
    output  wire        rx0_rate,       // 1 = /1.001 rate
    output  wire        rx1_rate,       // 1 = /1.001 rate

// 
// These signals are for the reset logic.
//
    // Entire GTP reset signals
    input   wire        gtp_reset0_in, // reset the entire GTP tile
    input   wire        gtp_reset1_in, // reset the entire GTP tile
    input   wire        clocks_stable,  // 1 when reference clocks are stable

    // Rx0 reset signals
    input   wire        rx0_pcs_reset,  // 1 = force reset of Rx0 PCS
    input   wire        rx0_cdr_reset,  // 1 = force reset of Rx0 CDR & PCS
    output  wire        rx0_fabric_reset,// reset synchronous with rx0_usrclk

    // Rx1 reset signals
    input   wire        rx1_pcs_reset,  // 1 = force reset of Rx0 PCS
    input   wire        rx1_cdr_reset,  // 1 = force reset of Rx0 CDR & PCS
    output  wire        rx1_fabric_reset,// reset synchronous with rx1_usrclk

    // Tx0 reset signals
    input   wire        tx0_reset,      // 1 = force reset of Tx0
    output  wire        tx0_fabric_reset,// reset synchronous with tx0_usrclk

    // Tx1 reset signals
    input   wire        tx1_reset,      // 1 = force reset of Tx1
    output  wire        tx1_fabric_reset,// reset synchronous with tx1_usrclk

//
// These signals must be connected to the GTP wrapper module created by the
// RocketIO wizard.
//
    output  wire  [7:0] daddr,          // connect to DADDR port on GTP
    output  wire        den,            // connect to DEN port on GTP
    output  wire [15:0] di,             // connect to DI port on GTP
    input   wire [15:0] drpo,           // connect to DO port on GTP
    input   wire        drdy,           // connect to DRDY port on GTP
    output  wire        dwe,            // connect to DWE port on GTP

    input   wire        txbufstatus0_b1,// connect to bit 1 of TXBUFSTATUS0
    input   wire        txbufstatus1_b1,// connect to bit 1 of TXBUFSTATUS1
    input   wire        rxbufstatus0_b2,// connect to bit 2 of RXBUFSTATUS0
    input   wire        rxbufstatus1_b2,// connect to bit 2 of RXBUFSTATUS1

    output  wire        gtpreset0,      // connect to GTPRESET0 input of GTP
    output  wire        gtpreset1,      // connect to GTPRESET1 input of GTP

    input   wire        resetdone0,     // connect to RESETDONE0 of GTP
    output  wire        rxreset0,       // connect to RXRESET0 of GTP
    output  wire        rxbufreset0,    // connect to RXBUFRESET0 of GTP
    output  wire        rxcdrreset0,    // connect to RXCDRRESET0 of GTP

    input   wire        resetdone1,     // connect to RESETDONE1 of GTP
    output  wire        rxreset1,       // connect to RXRESET1 of GTP
    output  wire        rxbufreset1,    // connect to RXBUFRESET1 of GTP
    output  wire        rxcdrreset1,    // connect to RXCDRRESET1 of GTP,

    output  wire        txreset0,       // connect to TXRESET0 of GTP
    output  wire        txreset1       // connect to TXRESET1 of GTP
    
);

localparam GTPRST_CTR_MSB = GTP_RESET_CNTR_SIZE - 1;

//
// Internal signals
//
(* ASYNC_REG = "TRUE" *)
reg  [1:0]              drst_sync = 0;      // synchronizes rst input to dclk

reg  [GTPRST_CTR_MSB:0] gtprst0_cntr = 0;    // GTP reset counter
wire                    gtprst0_cntr_tc;     // GTP reset counter terminal count
reg                     gtprst0_ff = 1'b1;   // internal version of gtpreset out
reg  [GTPRST_CTR_MSB:0] gtprst1_cntr = 0;    // GTP reset counter
wire                    gtprst1_cntr_tc;     // GTP reset counter terminal count
reg                     gtprst1_ff = 1'b1;   // internal version of gtpreset out

reg  [1:0]              rx0_rate_detect_std = 0;
wire                    rx0_rate_chg_det;
wire                    rx0_rate_change;
wire                    rx0_mode_change;

reg  [1:0]              rx1_rate_detect_std = 0;
wire                    rx1_rate_chg_det;
wire                    rx1_rate_change;
wire                    rx1_mode_change;

wire                    txreset0_int;
(* ASYNC_REG = "TRUE" *)
reg                     tx0_rst_sync_reg = 1'b1;
(* ASYNC_REG = "TRUE" *)
reg                     tx0_fabric_reset_int = 1'b1;
wire                    tx0_mode_change;

wire                    txreset1_int;
(* ASYNC_REG = "TRUE" *)
reg                     tx1_rst_sync_reg = 1'b1;
(* ASYNC_REG = "TRUE" *)
reg                     tx1_fabric_reset_int = 1'b1;
wire                    tx1_mode_change;  

//------------------------------------------------------------------------------
// DRP controller
//
// The DRP controller dynamically changes the GTP attributes for reference
// clock selection, CDR mode, and PLL dividers to change the clock routing
// and operating mode of the GTP Rx and Tx units.
//
always @ (posedge dclk)
    drst_sync <= {drst_sync[0], drst};

s6gtp_sdi_drp_control #(

     .PMA_RX_CFG_HD          (PMA_RX_CFG_HD),      
     .PMA_RX_CFG_SD          (PMA_RX_CFG_SD),
     .PMA_RX_CFG_3G          (PMA_RX_CFG_3G),
 
     .PLL_RXDIVSEL_OUT_HD    (PLL_RXDIVSEL_OUT_HD),
     .PLL_RXDIVSEL_OUT_SD    (PLL_RXDIVSEL_OUT_SD),
     .PLL_RXDIVSEL_OUT_3G    (PLL_RXDIVSEL_OUT_3G),
     
     .PLL_TXDIVSEL_OUT_HD    (PLL_TXDIVSEL_OUT_HD),
     .PLL_TXDIVSEL_OUT_SD    (PLL_TXDIVSEL_OUT_SD),
     .PLL_TXDIVSEL_OUT_3G    (PLL_TXDIVSEL_OUT_3G)
)
DRPCTRL (
    .clk                    (dclk),
    .rst                    (drst_sync[1]),
    .tx0_mode               (tx0_mode),
    .tx1_mode               (tx1_mode),
    .rx0_mode               (rx0_mode),
    .rx1_mode               (rx1_mode),
    .do                     (drpo),
    .drdy                   (drdy),
    .daddr                  (daddr),
    .di                     (di),
    .den                    (den),
    .dwe                    (dwe),
    .tx0_mode_change        (tx0_mode_change),
    .tx1_mode_change        (tx1_mode_change),
    .rx0_mode_change        (rx0_mode_change),
    .rx1_mode_change        (rx1_mode_change));

//
// The cable driver slew rate control is determined by the Tx mode.
//
assign tx0_slew = tx0_mode[0];
assign tx1_slew = tx1_mode[0];

//------------------------------------------------------------------------------
// GTP reset circuit
//
// Hold the entire GTP in reset for some period of time until all clocks are
// stable.
//
// gtpreset0
always @ (posedge dclk or posedge gtp_reset0_in)
    if (gtp_reset0_in)
        gtprst0_cntr = 0;
    else if (~gtprst0_cntr_tc)
        gtprst0_cntr = gtprst0_cntr + 1;

assign gtprst0_cntr_tc = gtprst0_cntr[GTPRST_CTR_MSB];

always @ (posedge dclk or posedge gtp_reset0_in)
    if (gtp_reset0_in)
        gtprst0_ff = 1'b1;
    else if (gtprst0_cntr_tc)
        gtprst0_ff = 1'b0;

assign gtpreset0 = gtprst0_ff | ~clocks_stable;

// gtpreset1
always @ (posedge dclk or posedge gtp_reset1_in)
    if (gtp_reset1_in)
        gtprst1_cntr = 0;
    else if (~gtprst1_cntr_tc)
        gtprst1_cntr = gtprst1_cntr + 1;

assign gtprst1_cntr_tc = gtprst1_cntr[GTPRST_CTR_MSB];

always @ (posedge dclk or posedge gtp_reset1_in)
    if (gtp_reset1_in)
        gtprst1_ff = 1'b1;
    else if (gtprst1_cntr_tc)
        gtprst1_ff = 1'b0;

assign gtpreset1 = gtprst1_ff | ~clocks_stable;
//
// Reset logic for Rx0
//

assign rx0_rate_change = (rx0_mode == 2'b01)?  0: rx0_rate_chg_det;
assign rx1_rate_change = (rx1_mode == 2'b01)?  0: rx1_rate_chg_det;

s6gtp_sdi_rx_reset #(
    .CBL_DISCONNECT_RST_CNTR_SIZE   (CBL_DISCONNECT_RST_CNTR_SIZE))
RX0RST (
    .free_clk           (dclk),
    .usr_clk            (rx0_usrclk),
    .gtp_reset          (gtpreset0),
    .pcs_reset          (rx0_pcs_reset),
    .cdr_reset          (rx0_cdr_reset),
    .eq_cd_n            (1'b0),
    .rate_change        (rx0_rate_change),
    .mode_change        (rx0_mode_change),
    .buffer_error       (rxbufstatus0_b2),
    .reset_done         (resetdone0),
    .gtp_rx_reset       (rxreset0),
    .gtp_rx_buf_reset   (rxbufreset0),
    .gtp_rx_cdr_reset   (rxcdrreset0),
    .fabric_reset       (rx0_fabric_reset),
    .clk_mux_sel        ());

//
// Reset logic for Rx1
//
s6gtp_sdi_rx_reset #(
    .CBL_DISCONNECT_RST_CNTR_SIZE   (CBL_DISCONNECT_RST_CNTR_SIZE))
RX1RST (
    .free_clk           (dclk),
    .usr_clk            (rx1_usrclk),
    .gtp_reset          (gtpreset1),
    .pcs_reset          (rx1_pcs_reset),
    .cdr_reset          (rx1_cdr_reset),
    .eq_cd_n            (1'b0),
    .rate_change        (rx1_rate_change),
    .mode_change        (rx1_mode_change),
    .buffer_error       (rxbufstatus1_b2),
    .reset_done         (resetdone1),
    .gtp_rx_reset       (rxreset1),
    .gtp_rx_buf_reset   (rxbufreset1),
    .gtp_rx_cdr_reset   (rxcdrreset1),
    .fabric_reset       (rx1_fabric_reset),
    .clk_mux_sel        ());

//------------------------------------------------------------------------------
// Reset logic for Tx0
//
// The transmitter is reset on a mode change, when a Tx buffer under/over run
// occurs, or when the tx0_reset input to this module is asserted. This reset
// resets the PCS section of the GTP Tx. Also asserted at the same time is
// the tx0_fabric_reset to reset the fabric portion of the transmitter.
//
assign txreset0_int = tx0_reset | tx0_mode_change | txbufstatus0_b1;

always @ (posedge tx0_usrclk or posedge txreset0_int)
    if (txreset0_int)
        begin
            tx0_rst_sync_reg <= 1'b1;
            tx0_fabric_reset_int <= 1'b1;
        end
    else 
        begin
            tx0_rst_sync_reg <= 1'b0;
            tx0_fabric_reset_int <= tx0_rst_sync_reg;
        end

assign txreset0 = txreset0_int;
assign tx0_fabric_reset = tx0_fabric_reset_int;

// -----------------------------------------------------------------------------
// Reset logic for Tx1
//
// The transmitter is reset on a mode change, when a Tx buffer under/over run
// occurs, or when the tx1_reset input to this module is asserted. This reset
// resets the PCS section of the GTP Tx. Also asserted at the same time is
// the tx1_fabric_reset to reset the fabric portion of the transmitter.
//
assign txreset1_int = tx1_reset | tx1_mode_change | txbufstatus1_b1;

always @ (posedge tx1_usrclk or posedge txreset1_int)
    if (txreset1_int)
        begin
            tx1_rst_sync_reg <= 1'b1;
            tx1_fabric_reset_int <= 1'b1;
        end
    else 
        begin
            tx1_rst_sync_reg <= 1'b0;
            tx1_fabric_reset_int <= tx1_rst_sync_reg;
        end

assign txreset1 = txreset1_int;
assign tx1_fabric_reset = tx1_fabric_reset_int;


//------------------------------------------------------------------------------
// Rate detection for Rx0
//
always @ (posedge rx0_usrclk)
    rx0_rate_detect_std <= {rx0_rate_detect_std[0], rx0_mode[1] | rx0_mode[0]};

s6gtp_sdi_rate_detect #(
    .REFCLK_FREQ     (RATE_REFCLK_FREQ))
RATE0 (
    .rst        (1'b0),
    .refclk     (rate_refclk),
    .recvclk    (rx0_usrclk),
    .std        (rx0_rate_detect_std[1]),
    .reset_out  (rx0_rate_chg_det),
    .enable     (resetdone0),
    .drift      (),
    .rate       (rx0_rate));


//------------------------------------------------------------------------------
// Rate detection for Rx1
//
always @ (posedge rx1_usrclk)
    rx1_rate_detect_std <= {rx1_rate_detect_std[0], rx1_mode[1] | rx1_mode[0]};

s6gtp_sdi_rate_detect #(
    .REFCLK_FREQ     (RATE_REFCLK_FREQ))
RATE1 (
    .rst        (1'b0),
    .refclk     (rate_refclk),
    .recvclk    (rx1_usrclk),
    .std        (rx1_rate_detect_std[1]),
    .reset_out  (rx1_rate_chg_det),
    .enable     (resetdone1),
    .drift      (),
    .rate       (rx1_rate));
 
endmodule


