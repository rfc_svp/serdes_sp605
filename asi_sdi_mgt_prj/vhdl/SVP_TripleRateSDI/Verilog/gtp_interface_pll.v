////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Author : Reed P. Tidwell
//  \   \         Filename: $RCSfile: gtp_interface_pll.v,v $
//  /   /         Date Last Modified:  $Date: 2010-02-23 16:45:58-07 $
// /___/   /\     Date Created: January 21, 2010
// \   \  /  \ 
//  \___\/\___\
//
// Revision History: 
// $Log: gtp_interface_pll.v,v $
// Revision 1.2  2010-02-23 16:45:58-07  reedt
// Feb. 2010 full release.
//
// Revision 1.1  2010-01-27 15:43:32-07  reedt
// Added data path pass thru.
//
// Revision 1.0  2010-01-21 09:01:33-07  reedt
// Initial Checkin. Supports 20-bit GTP  interface.
//
// (c) Copyright 2010 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of,
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES. 


`timescale 1ns / 1ps
`default_nettype none


module gtp_interface_pll 
(
  input wire        outclk,           // txoutclk  or recovered clock on fabric routing not used by PLL
  input wire        gtpoutclk,        //input clock to PLL on dedicated clock routing
  input wire        pll_reset_in,
  input wire [19:0] data_in,            // data passes through, allowing for synchronization 
                                       // if required for alternate clocking schemes.
  output wire[19:0] data_out,
  output wire       usrclk,           // output user clock at input clock rate
  output wire       usrclk2,          // output user clock at input clock /4
  output wire       pipe_clk,         // output clock for SDI pipeline
  output wire       pll_locked_out 
);                                                 
                                                  	
    
//************************** Signal Declarations ****************************
wire            pll_fb;

assign data_out = data_in;
assign pipe_clk = usrclk2;

//   PLL    
    usrclk_pll #
      (
          .MULT                           (3),
          .DIVIDE                         (1),
          .CLK_PERIOD                     (6.734006),//(3.367003),
          .OUT0_DIVIDE                    (3),
          .OUT1_DIVIDE                    (3),
          .OUT2_DIVIDE                    (6),
          .OUT3_DIVIDE                    (66)
      )
      usrclk_pll
      (
          .CLK0_OUT                       (usrclk),      //  divide by 1  
          .CLK1_OUT                       (),    
          .CLK2_OUT                       (usrclk2),     //  divide by 2               
          .CLK3_OUT                       (),
          .CLK1_SEL                       (1'b0),         
          .CLK_IN                         (gtpoutclk),    //  297 MHz; 
          .CLKFB_IN                       (pll_fb),                
          .CLKFB_OUT                      (pll_fb),
          .PLL_LOCKED_OUT                 (pll_locked_out),  
          .PLL_RESET_IN                   (pll_reset_in)
      );

endmodule
