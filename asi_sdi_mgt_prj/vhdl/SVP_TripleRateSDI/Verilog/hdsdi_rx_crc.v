//------------------------------------------------------------------------------ 
// Copyright (c) 2008 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Solutions Development Group, Xilinx, Inc.
//  \   \        Filename: $RCSfile: hdsdi_rx_crc.v,rcs $
//  /   /        Date Last Modified:  $Date: 2008-11-14 09:17:00-07 $
// /___/   /\    Date Created: May 21, 2004
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: hdsdi_rx_crc.v,rcs $
// Revision 1.2  2008-11-14 09:17:00-07  jsnow
// Added register initializers. Replaced hdsdi_crc module with hdsdi_crc2.
//
// Revision 1.1  2004-08-23 13:06:27-06  jsnow
// Comment changes only.
//
// Revision 1.0  2004-05-21 13:47:25-06  jsnow
// Initial Revision
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module calculates the CRC value for a line and compares it to the received
CRC value. The module does this for both the Y and C channels. If a CRC error
is detected, the corresponding CRC error output is asserted high. This output
remains asserted for one video line time, until the next CRC check is made.

The module also captures the line number values for the two channels and 
outputs them. The line number values are valid for the entire line time. 

--------------------------------------------------------------------------------
*/

//`timescale 1ns / 1ns

module hdsdi_rx_crc (
    input  wire         clk,                // receiver clock
    input  wire         rst,                // reset signal
    input  wire         ce,                 // clock enable input
    input  wire [9:0]   c_video,            // C channel video input port
    input  wire [9:0]   y_video,            // Y channel video input port
    input  wire         trs,                // TRS signal asserted during all 4 words of TRS
    output reg          c_crc_err = 1'b0,   // C channel CRC error detected
    output reg          y_crc_err = 1'b0,   // Y channel CRC error detected
    output reg  [10:0]  c_line_num = 0,     // C channel received line number
    output reg  [10:0]  y_line_num = 0      // Y channel received line number
);

// Internal wires
reg     [17:0]      c_rx_crc = 0;
reg     [17:0]      y_rx_crc = 0;
wire    [17:0]      c_calc_crc;
wire    [17:0]      y_calc_crc;
reg     [7:0]       trslncrc = 0;
reg                 crc_clr = 0;
reg                 crc_en = 0;
reg     [6:0]       c_line_num_int = 0;
reg     [6:0]       y_line_num_int = 0;

//
// CRC generator modules
//
hdsdi_crc2 crc_C (
    .clk            (clk),
    .ce             (ce),
    .en             (crc_en),
    .rst            (rst),
    .clr            (crc_clr),
    .d              (c_video),
    .crc_out        (c_calc_crc)
);

hdsdi_crc2 crc_Y (
    .clk            (clk),
    .ce             (ce),
    .en             (crc_en),
    .rst            (rst),
    .clr            (crc_clr),
    .d              (y_video),
    .crc_out        (y_calc_crc)
);


//
// trslncrc generator
//
// This code generates timing signals indicating where the CRC and LN words
// are located in the EAV symbol.
//
always @ (posedge clk or posedge rst)
    if (rst)
        trslncrc <= 0;
    else if (ce)
        begin
            if (trs & ~trslncrc[0] & ~trslncrc[1] & ~trslncrc[2])
                trslncrc[0] <= 1'b1;
            else
                trslncrc[0] <= 1'b0;
            trslncrc[7:1] <= {trslncrc[6:3], trslncrc[2] & y_video[6], trslncrc[1:0]};
        end

//
// crc_clr signal
//
// The crc_clr signal controls when the CRC generator's accumulation register
// gets reset to begin calculating the CRC for a new line.
//
always @ (posedge clk or posedge rst)
    if (rst)
        crc_clr <= 1'b0;
    else if (ce)
        begin
            if (trslncrc[2] & ~y_video[6])
                crc_clr <= 1'b1;
            else
                crc_clr <= 1'b0;
        end
        
//
// crc_en signal
//
// The crc_en signal controls which words are included in the CRC calculation.
//
always @ (posedge clk or posedge rst)
    if (rst)
        crc_en <= 1'b0;
    else if (ce)
        begin
            if (trslncrc[2] & ~y_video[6])
                crc_en <= 1'b1;
            else if (trslncrc[4])
                crc_en <= 1'b0;
        end
        
//
// received CRC registers
//
// These registers hold the received CRC words from the input video stream.
//
always @ (posedge clk or posedge rst)
    if (rst)
        begin
            c_rx_crc <= 0;
            y_rx_crc <= 0;
        end
    else if (ce)
        begin
            if (trslncrc[5])
                begin
                    c_rx_crc[8:0] <= c_video[8:0];
                    y_rx_crc[8:0] <= y_video[8:0];
                end
            else if (trslncrc[6])
                begin
                    c_rx_crc[17:9] <= c_video[8:0];
                    y_rx_crc[17:9] <= y_video[8:0];
                end
        end

//
// CRC comparators
//
// Compare the received CRC values against the calculated CRCs.
//
always @ (posedge clk or posedge rst)
    if (rst)
        begin
            c_crc_err <= 1'b0;
            y_crc_err <= 1'b0;
        end
    else if (ce & trslncrc[7])
        begin
            if (c_rx_crc == c_calc_crc)
                c_crc_err <= 1'b0;
            else
                c_crc_err <= 1'b1;

            if (y_rx_crc == y_calc_crc)
                y_crc_err <= 1'b0;
            else
                y_crc_err <= 1'b1;
        end

//
// line number registers
//
// These registers hold the line number values from the input video stream.
//
always @ (posedge clk or posedge rst)
    if (rst)
        begin
            c_line_num_int <= 0;
            y_line_num_int <= 0;
            c_line_num <= 0;
            y_line_num <= 0;
        end
    else if (ce)
        begin
            if (trslncrc[3])
                begin
                    c_line_num_int <= c_video[8:2];
                    y_line_num_int <= y_video[8:2];
                end
            else if (trslncrc[4])
                begin
                    c_line_num <= {c_video[5:2], c_line_num_int};
                    y_line_num <= {y_video[5:2], y_line_num_int};
                end
        end

endmodule
