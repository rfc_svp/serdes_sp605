//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: Reed P. Tidwell
//  \   \        Filename: $RCSfile: s6gtp_sdi_rx_reset.v,v $
//  /   /        Date Last Modified:  $Date: 2010-02-23 16:46:00-07 $
// /___/   /\    Date Created: May 27, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: s6gtp_sdi_rx_reset.v,v $
// Revision 1.2  2010-02-23 16:46:00-07  reedt
// Feb. 2010 full release.
//
// Revision 1.1  2009-12-08 12:02:09-07  reedt
// December '09 Pre-release.
//
// Revision 1.0  2009-11-18 14:29:05-07  reedt
// Initial revision
//
// Revision 1.0  2009-07-01 16:12:04-06  reedt
// Initial revision
//
//------------------------------------------------------------------------------ 
// This file contains confidential and proprietary information of Xilinx, Inc. 
// and is protected under U.S. and international copyright and other 
// intellectual property laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any rights to the 
// materials distributed herewith. Except as otherwise provided in a valid 
// license issued to you by Xilinx, and to the maximum extent permitted by 
// applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL 
// FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, 
// IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF 
// MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; 
// and (2) Xilinx shall not be liable (whether in contract or tort, including 
// negligence, or under any other theory of liability) for any loss or damage of 
// any kind or nature related to, arising under or in connection with these 
// materials, including for any direct, or any indirect, special, incidental, 
// or consequential loss or damage (including loss of data, profits, goodwill, 
// or any type of loss or damage suffered as a result of any action brought by 
// a third party) even if such damage or loss was reasonably foreseeable or 
// Xilinx had been advised of the possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-safe, or for use in 
// any application requiring fail-safe performance, such as life-support or 
// safety devices or systems, Class III medical devices, nuclear facilities, 
// applications related to the deployment of airbags, or any other applications 
// that could lead to death, personal injury, or severe property or 
// environmental damage (individually and collectively, "Critical Applications").
// Customer assumes the sole risk and liability of any use of Xilinx products in
// Critical Applications, subject only to applicable laws and regulations 
// governing limitations on product liability. 
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT
// ALL TIMES.
//
//------------------------------------------------------------------------------ 
/*
Module Description:

This module implements the reset circuits for a GTP receiver for SDI.
--------------------------------------------------------------------------------
*/

`timescale 1ns / 1ps

module s6gtp_sdi_rx_reset
#(
//
// This parameters specifies the size of the cable disconnect counter. The size
// of this counter should be set so that it generates a cable disconnect reset
// pulse of about 60 microseconds when clocked by the free_clk.
//
    parameter CBL_DISCONNECT_RST_CNTR_SIZE  = 12
)
(                                           
    input   wire        free_clk,           // free running clock (usually DRP DCLK)
    input   wire        usr_clk,            // RXUSRCLK
    input   wire        gtp_reset,          // GTPRESET
    input   wire        pcs_reset,          // 1 = reset PCS of the GTP Rx
    input   wire        cdr_reset,          // 1 = reset CDR & PCS of the GTP Rx
    input   wire        eq_cd_n,            // from cable EQ carrier detect pin
    input   wire        rate_change,        // 1 = bit rate change
    input   wire        mode_change,        // 1 = mode change
    input   wire        buffer_error,       // 1 = Rx buffer error
    input   wire        reset_done,         // 1 = GTP internal reset complete

    output  wire        gtp_rx_reset,       // RXRESET
    output  wire        gtp_rx_buf_reset,   // RXBUFRESET
    output  wire        gtp_rx_cdr_reset,   // RXCDRRESET
    output  wire        fabric_reset,       // synchronous reset to fabric
    output  reg         clk_mux_sel         // BUFGMUX clock select
);

localparam CBLDIS_CNTR_MSB = CBL_DISCONNECT_RST_CNTR_SIZE - 1;

//
// Internal signals
//
wire                        pcs_rst_trigger;
(* ASYNC_REG = "TRUE" *)
reg  [1:0]                  fabric_rst_sync = 2'b11;
(* ASYNC_REG = "TRUE" *)
reg  [3:0]                  eq_cd_sync = 0;
reg                         cdr_rst_capture = 1'b1;
(* ASYNC_REG = "TRUE" *)
reg  [1:0]                  cdr_rst_sync = 0;
reg                         rate_change_capture = 1'b1;
(* ASYNC_REG = "TRUE" *)
reg  [1:0]                  rate_change_sync = 0;
reg  [CBLDIS_CNTR_MSB:0]    disconnect_cntr = 0;
reg                         disconnect_ff = 1'b0;
wire                        cdr_rst_int;
reg  [1:0]                  reset_done_sync = 0;
(* ASYNC_REG = "TRUE" *)
reg  [1:0]                  gtp_reset_sync_reg = 0;
wire                        gtp_reset_sync;
reg                         cdr_rst_disable = 1'b1;
reg                         gtp_reset_capture = 1'b0;
wire                        gtp_reset_done;
reg                         buffer_error_ff = 1'b0;


//
// This logic disables the CDR reset logic on initial power up or whenever a
// GTP reset occurs. The GTP Rx doesn't seem to recover correctly if CDR
// reset happens during the initial GTP reset after power on. 
//
always @ (posedge free_clk)
    gtp_reset_sync_reg = {gtp_reset_sync_reg[0], gtp_reset};

assign gtp_reset_sync = gtp_reset_sync_reg[1];

always @ (posedge free_clk)
    gtp_reset_capture = gtp_reset_sync;

assign gtp_reset_done = gtp_reset_capture & ~gtp_reset_sync;

always @ (posedge free_clk)
    if (gtp_reset_done)
        cdr_rst_disable = 1'b0;

// 
// gtp_rx_reset
//
// The pcs_reset input directly controls the gtp_rx_reset output
//
assign gtp_rx_reset = pcs_reset;

//
// gtp_rx_buf_reset
//
// The buffer_error signal directly controls the gtp_rx_buf_reset output.
// The buffer_error signal goes through a FF in order to reduce the length of
// the timing path.
//
always @ (posedge free_clk)
    buffer_error_ff <= buffer_error;

assign gtp_rx_buf_reset = buffer_error_ff;

//
// fabric_reset
//
// When pcs_reset, buffer_error, or cdr_rst_int signals are asserted, assert
// the fabric_reset output. This output is asserted asynchronously to any clock
// but its falling edge (negation) is synchronous with the rising edge of the 
// usr_clk.
//
assign pcs_rst_trigger = pcs_reset | buffer_error_ff | cdr_rst_int;

always @ (posedge usr_clk or posedge pcs_rst_trigger)
    if (pcs_rst_trigger)
        fabric_rst_sync = 2'b11;
    else
        fabric_rst_sync = {fabric_rst_sync[0], 1'b0};

assign fabric_reset = fabric_rst_sync[1];

//
// Cable disconnect glitch filter and synchronizer
//
// This logic synchronizes the eq_cd_n signal (carrier detect -- asserted low
// when cable is connected and signal is good) and removes short duration 
// glitches. The signal must be High (cable disconnected) for three consecutive
// cycles of free_clk before cable_disconnect will be asserted High.
//
always @ (posedge free_clk)
    eq_cd_sync = {eq_cd_sync[2:0], eq_cd_n};

assign cable_disconnect = &eq_cd_sync[3:1];

//
// CDR reset synchronizer and pulse stretcher
//
// This logic synchronizes the cdr_reset input to free_clk. It is edge
// triggered on the rising edge of cdr_reset so that input pulses shorter than
// the period of free_clk are detected and result in output pulses that are
// multiple free_clk cycles long. The output signal will remain asserted until
// several clock cycles after cdr_reset goes low.
//
always @ (posedge free_clk or posedge cdr_reset)
    if (cdr_reset)
        cdr_rst_capture = 1'b1;
    else if (cdr_rst_sync[1] | gtp_reset_sync)
        cdr_rst_capture = 1'b0;

always @ (posedge free_clk)
    if (gtp_reset_sync)
        cdr_rst_sync = 0;
    else
        cdr_rst_sync = {cdr_rst_sync[0], cdr_rst_capture};

//
// Rate change synchronizer and pulse stretcher
//
// This logic synchronizes the rate_change input to free_clk. It is edge
// triggered on the rising edge of rate_change so that input pules shorter than
// the period of free_clk are detected and result in output pulses that are
// multiple free_clk cycles long. The output signal will remain asserted until
// several clock cycles after rate_change goes low.
//
always @ (posedge free_clk or posedge rate_change)
    if (rate_change)
        rate_change_capture = 1'b1;
    else if (rate_change_sync[1] | gtp_reset_sync)
        rate_change_capture = 1'b0;

always @ (posedge free_clk)
    if (gtp_reset_sync)
        rate_change_sync = 0;
    else
        rate_change_sync = {rate_change_sync[0], rate_change_capture};

//
// Cable disconnect timer
//
// This counter generates an approximately 60 usec long reset pulse whenever
// the cable_disconnect from the cable EQ carrier detect synchronizer and
// glitch filter is asserted. The output signal is asserted as long as
// cable_disconnect is asserted and will remain asserted for an additonal 60 us
// after cable_disconnect is negated.
//
always @ (posedge free_clk)
    if (cable_disconnect)
        disconnect_cntr = 0;
    else if (disconnect_ff)
        disconnect_cntr = disconnect_cntr + 1;

always @ (posedge free_clk)
    if (cable_disconnect)
        disconnect_ff = 1'b1;
    else if (disconnect_cntr[CBLDIS_CNTR_MSB] | gtp_reset_sync)
        disconnect_ff = 1'b0;

//
// CDR reset
//
// The gtp_rx_cdr_reset output and its internal equivalent, cdr_rst_int, are
// asserted whenever the disconnect_ff is High or when any of the other
// reset sources (cdr_rst_sync[1], rate_change_sync[1], or mode_change) are
// High. The cdr_rst_int signal also causes the fabric_reset signal to be
// asserted. The GTP's RXCDRRESET input, driven by the gtp_rx_cdr_reset signal
// resets both the CDR and PCS sections of the GTP Rx. So, this signal resets
// the entire Rx, but does not reset the common PLL section of the GTP.
//
assign cdr_rst_int = (disconnect_ff | cdr_rst_sync[1] | rate_change_sync[1] |
                     mode_change) & ~cdr_rst_disable;
                     
assign gtp_rx_cdr_reset = cdr_rst_int;

//
// BUFGMUX control
//
// The clk_mux_sel signal is intended to be used with a BUFGCTRL primitive to
// switch the RXUSRCLK from the RXRECCLK recovered clock to a reference clock
// whenever the CDR reset is asserted. This is because during CDR resets
// (including the entire time the cable is disconnected) the RXRECCLK is 
// stopped. Many applications require a continuous clock, even if the the input
// video bit stream is stopped.
//
always @ (posedge free_clk or posedge cdr_rst_int)
    if (cdr_rst_int)
        reset_done_sync = 0;
    else
        reset_done_sync = {reset_done_sync[0], reset_done};

always @ (posedge free_clk or posedge cdr_rst_int)
    if (cdr_rst_int)
        clk_mux_sel = 1'b0;
    else if (reset_done_sync[1])
        clk_mux_sel = 1'b1;


endmodule