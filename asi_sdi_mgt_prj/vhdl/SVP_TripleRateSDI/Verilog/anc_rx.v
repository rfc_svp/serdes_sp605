//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: anc_rx.v,rcs $
//  /   /        Date Last Modified:  $Date: 2008-01-29 12:05:08-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: anc_rx.v,rcs $
// Revision 1.2  2008-01-29 12:05:08-07  jsnow
// Fixed next_state sensitivity list.
//
// Revision 1.1  2004-12-15 11:22:55-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module checks the parity bits and checksums of all ANC packets (except EDH
packets) on the video stream.. If any errors are detected in ANC packets during
a field, the module will assert the anc_edh_local signal. This signal is used 
by the edh_gen module to assert the edh flag in the ANC flag set of the next 
EDH packet it generates. The anc_edh_local signal remains asserted until the
EDH packet has been sent (as indicated the edh_packet input being asserted then
negated).

The module will not do any checking after reset until the video decoder's locked
signal is asserted for the first time.
*/

`timescale 1ns / 1 ns

module  anc_rx (
    // inputs
    clk,            // clock input
    ce,             // clock enable
    rst,            // async reset input
    locked,         // video decoder locked signal
    rx_anc_next,    // indicates the next word is the first word of a received ANC packet
    rx_edh_next,    // indicates the next word is the first word of a received EDH packet
    edh_packet,     // indicates an EDH packet is being generated
    vid_in,         // video data

    // outputs
    anc_edh_local   // ANC edh flag
);


//-----------------------------------------------------------------------------
// Parameter definitions
//      

//
// This group of parameters defines the states of the EDH processor state
// machine.
//
parameter STATE_WIDTH   = 4;
parameter STATE_MSB     = STATE_WIDTH - 1;

parameter [STATE_WIDTH-1:0]
    S_WAIT   = 0,
    S_ADF1   = 1,
    S_ADF2   = 2,
    S_ADF3   = 3,
    S_DID    = 4,
    S_DBN    = 5,
    S_DC     = 6,
    S_UDW    = 7,
    S_CHK    = 8,
    S_EDH1   = 9,
    S_EDH2   = 10,
    S_EDH3   = 11;

//-----------------------------------------------------------------------------
// Port definitions
//
input                   clk;
input                   ce;
input                   rst;
input                   locked;
input                   rx_anc_next;
input                   rx_edh_next;
input                   edh_packet;
input   [9:0]           vid_in;
output                  anc_edh_local;
reg                     anc_edh_local;

//-----------------------------------------------------------------------------
// Signal definitions
//
reg     [STATE_MSB:0]   current_state;  // FSM current state
reg     [STATE_MSB:0]   next_state;     // FSM next state
wire                    parity;         // used to generate parity_err signal
wire                    parity_err;     // asserted on parity error
reg                     check_parity;   // asserted when parity should be checked
reg     [8:0]           checksum;       // checksum generator for ANC packet
reg                     clr_checksum;   // asserted to clear the checksum
reg                     check_checksum; // asserted when checksum is to be tested
reg                     clr_edh_flag;   // asserted to clear the edh flag
reg                     checksum_err;   // asserted when checksum error in EDH packet is detected
reg     [7:0]           udw_cntr;       // user data word counter
reg                     udwcntr_eq_0;   // asserted when output of UDW in MUX is zero
wire    [7:0]           udw_mux;        // UDW counter input MUX
reg                     ld_udw_cntr;    // loads the UDW counter when asserted
reg                     enable;         // generated from locked input

//
// enable signal
//
// This signal enables checking of the parity and checksum. It is negated on
// reset and remains negated until locked is asserted for the first time.
//
always @ (posedge clk or posedge rst)
    if (rst)
        enable <= 1'b0;
    else if (ce)
        if (locked)
            enable <= 1'b1;
                                
//
// FSM: current_state register
//
// This code implements the current state register. 
//
always @ (posedge clk or posedge rst)
    if (rst)
        current_state <= S_WAIT;
    else if (ce)
        current_state <= next_state;

//
// FSM: next_state logic
//
// This case statement generates the next_state value for the FSM based on
// the current_state and the various FSM inputs.
//
always @ (*)
    case(current_state)
        S_WAIT:     if (~enable)
                        next_state = S_WAIT;
                    else if (rx_anc_next & ~rx_edh_next)
                        next_state = S_ADF1;
                    else if (edh_packet)
                        next_state = S_EDH1;
                    else
                        next_state = S_WAIT;
                
        S_ADF1:     next_state = S_ADF2;

        S_ADF2:     next_state = S_ADF3;

        S_ADF3:     next_state = S_DID;

        S_DID:      if (parity_err)
                        next_state = S_WAIT;
                    else
                        next_state = S_DBN;

        S_DBN:      if (parity_err)
                        next_state = S_WAIT;
                    else
                        next_state = S_DC;

        S_DC:       if (parity_err)
                        next_state = S_WAIT;
                    else if (udwcntr_eq_0)
                        next_state = S_CHK;
                    else
                        next_state = S_UDW;

        S_UDW:      if (udwcntr_eq_0)
                        next_state = S_CHK;
                    else
                        next_state = S_UDW;

        S_CHK:      next_state = S_WAIT;

        S_EDH1:     if (~edh_packet)
                        next_state = S_EDH1;
                    else
                        next_state = S_EDH2;

        S_EDH2:     if (edh_packet)
                        next_state = S_EDH2;
                    else
                        next_state = S_EDH3;

        S_EDH3:     next_state = S_WAIT;

        default:    next_state = S_WAIT;

    endcase
        
//
// FSM: outputs
//
// This block decodes the current state to generate the various outputs of the
// FSM.
//
always @ (*)
begin
    // Unless specifically assigned in the case statement, all FSM outputs
    // default to the values given here.
    clr_checksum    = 1'b0;
    clr_edh_flag    = 1'b0;
    check_parity    = 1'b0;
    ld_udw_cntr     = 1'b0;
    check_checksum  = 1'b0;
                            
    case(current_state)     
        S_EDH3:     clr_edh_flag = 1'b1;

        S_ADF3:     clr_checksum = 1'b1;

        S_DID:      check_parity = 1'b1;

        S_DBN:      check_parity = 1'b1;

        S_DC:       begin
                        ld_udw_cntr = 1'b1;
                        check_parity = 1'b1;
                    end

        S_CHK:      check_checksum = 1'b1;

    endcase
end

//
// parity error detection
//
// This code calculates the parity of bits 7:0 of the video word. The calculated
// parity bit is compared to bit 8 and the complement of bit 9 to determine if
// a parity error has occured. If a parity error is detected, the parity_err
// signal is asserted. Parity is only valid on the payload portion of the
// EDH packet (user data words).
//
assign parity = vid_in[7] ^ vid_in[6] ^ vid_in[5] ^ vid_in[4] ^
                vid_in[3] ^ vid_in[2] ^ vid_in[1] ^ vid_in[0];

assign parity_err = (parity ^ vid_in[8]) | (parity ^ ~vid_in[9]);


//
// checksum calculator
//
// This code generates a checksum for the EDH packet. The checksum is cleared
// to zero prior to beginning the checksum calculation by the FSM asserting the
// clr_checksum signal. The vid_in word is added to the current checksum when
// the FSM asserts the do_checksum signal. The checksum is a 9-bit value and
// is computed by summing all but the MSB of the vid_in word with the current
// checksum value and ignoring any carry bits.
//
always @ (posedge clk or posedge rst)
    if (rst)
        checksum <= 0;
    else
        if (ce)
            begin
                if (clr_checksum)
                    checksum <= 0;
                else
                    checksum <= checksum + vid_in[8:0];
            end

//
// checksum tester
//
// This logic asserts the checksum_err signal if the calculated and received
// checksum are not the same.
//
always @ (checksum or vid_in)
    if (checksum == vid_in[8:0] && checksum[8] == ~vid_in[9])
        checksum_err = 1'b0;
    else
        checksum_err = 1'b1;

//
// UDW counter, input MUX, and comparator
//
// The UDW counter is designed to count the number of user data words in the
// ANC packet so that the FSM knows when the payload portion of the ANC
// packet is over.
//
// The ld_udw_cntr signal controls a MUX. When this signal is asserted, the
// MUX outputs the vid_in data word. Otherwise, the MUX outputs the contents of
// the UDW counter. The output of the MUX is decremented by one and loaded into
// the UDW counter. The output of the MUX is also tested to see if it equals
// zero and the udwcntr_eq_0 signal is asserted if so.
//
assign udw_mux = ld_udw_cntr ? vid_in[7:0] : udw_cntr;

always @ (*)
    if (udw_mux == 8'b00000000)
        udwcntr_eq_0 = 1'b1;
    else
        udwcntr_eq_0 = 1'b0;
        
always @ (posedge clk or posedge rst)
    if (rst)
        udw_cntr <= 0;
    else if (ce)
        udw_cntr <= udw_mux - 1;
        
//
// anc_edh_local flag
//
// This flag is reset whenever an EDH packet is generated. The flag is set
// if a parity error or checksum error is detected during a field.
//
always @ (posedge clk or posedge rst)
    if (rst)
        anc_edh_local <= 1'b0;
    else if (ce)
        begin
            if (clr_edh_flag)
                anc_edh_local <= 1'b0;
            else if (parity_err & check_parity)
                anc_edh_local <= 1'b1;
            else if (checksum_err & check_checksum)
                anc_edh_local <= 1'b1;
        end
                            
endmodule