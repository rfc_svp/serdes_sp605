//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: edh_crc.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:23:55-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: edh_crc.v,rcs $
// Revision 1.1  2004-12-15 11:23:55-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module calculates the active picture and full-frame CRC values. The ITU-R
BT.1304 and SMPTE RP 165-1994 standards define how the two CRC values are to be
calculated.

The module uses the vertical line count (vcnt) input, the field bit (f), the
horizontal blanking interval bit (h), and the eav_next, sav_next, and xyz_word
inputs to determine which samples to include in the two CRC calculations.

The calculation is a standard CRC16 calculation with a polynomial of x^16 + x^12
+ x^5 + 1. The function considers the LSB of the video data as the first bit
shifted into the CRC generator, although the implementation given here is a
fully parallel CRC, calculating all 16 CRC bits from the 10-bit video data in
one clock cycle.  The CRC calculation is done is the edh_crc16 module. It is 
instanced twice, once for the full-frame calculation and once for the active-
picture calculation.    

For each CRC calculation, a valid bit is also generated. After reset the valid
bits will be negated until the locked input from the video decoder is asserted.
The valid bits remain asserted even if locked is negated. However, the valid
bits will be negated for one filed if the locked signal rises during a CRC
calculation, indicating that the video decoder has re-synchronized.
*/

`timescale 1ns / 1 ns

module  edh_crc (
    // inputs
    clk,            // clock input
    ce,             // clock enable
    rst,            // async reset input
    f,              // field bit
    h,              // horizontal blanking bit
    eav_next,       // asserted when next sample begins EAV symbol
    xyz_word,       // asserted when current word is the XYZ word of a TRS
    vid_in,         // video data
    vcnt,           // vertical line count
    std,            // indicates the video standard
    locked,         // asserted when flywheel is locked

    // outputs
    ap_crc,         // calculated active picture CRC
    ap_crc_valid,   // asserted when CRC is valid
    ff_crc,         // calculated full-frame CRC
    ff_crc_valid    // asserted when CRC is valid
);


//-----------------------------------------------------------------------------
// Parameter definitions
//

//
// This group of parameters defines the bit widths of various fields in the
// module. 
//
parameter VCNT_WIDTH    = 10;                   // Width of vcnt
parameter VCNT_MSB      = VCNT_WIDTH - 1;       // MS bit # of vcnt


//
// This group of parameters defines the encoding for the video standards output
// code.
//
parameter [2:0]
    NTSC_422        = 3'b000,
    NTSC_INVALID    = 3'b001,
    NTSC_422_WIDE   = 3'b010,
    NTSC_4444       = 3'b011,
    PAL_422         = 3'b100,
    PAL_INVALID     = 3'b101,
    PAL_422_WIDE    = 3'b110,
    PAL_4444        = 3'b111;


//
// This group of parameters defines the line numbers that begin and end the
// two CRC intervals. Values are given for both fields and for both NTSC and
// PAL.
//
parameter NTSC_FLD1_AP_FIRST    =  21;
parameter NTSC_FLD1_AP_LAST     = 262;
parameter NTSC_FLD1_FF_FIRST    =  12;
parameter NTSC_FLD1_FF_LAST     = 271;
    
parameter NTSC_FLD2_AP_FIRST    = 284;
parameter NTSC_FLD2_AP_LAST     = 525;
parameter NTSC_FLD2_FF_FIRST    = 275;
parameter NTSC_FLD2_FF_LAST     =   8;

parameter PAL_FLD1_AP_FIRST     =  24;
parameter PAL_FLD1_AP_LAST      = 310;
parameter PAL_FLD1_FF_FIRST     =   8;
parameter PAL_FLD1_FF_LAST      = 317;

parameter PAL_FLD2_AP_FIRST     = 336;
parameter PAL_FLD2_AP_LAST      = 622;
parameter PAL_FLD2_FF_FIRST     = 321;
parameter PAL_FLD2_FF_LAST      =   4;
    
         
//-----------------------------------------------------------------------------
// Port definitions
//
input                   clk;
input                   ce;
input                   rst;
input                   f;
input                   h;
input                   eav_next;
input                   xyz_word;
input   [9:0]           vid_in;
input   [VCNT_MSB:0]    vcnt;
input   [2:0]           std;
input                   locked;
output  [15:0]          ap_crc;
output                  ap_crc_valid;
output  [15:0]          ff_crc;
output                  ff_crc_valid;


//-----------------------------------------------------------------------------
// Signal defintions
//
wire                    ntsc;           // 1 = NTSC, 0 = PAL
reg     [15:0]          ap_crc_reg;     // active picture CRC register
reg     [15:0]          ff_crc_reg;     // full field cRC register
wire    [15:0]          ap_crc16;       // active picture CRC calc output
wire    [15:0]          ff_crc16;       // full field CRC calc output
reg                     ap_region;      // asserted during active picture CRC interval
reg                     ff_region;      // asserted during full field CRC interval
reg     [VCNT_MSB:0]    ap_start_line;  // active picture interval start line
reg     [VCNT_MSB:0]    ap_end_line;    // active picture interval end line
reg     [VCNT_MSB:0]    ff_start_line;  // full field interval start line
reg     [VCNT_MSB:0]    ff_end_line;    // full field interval end line
wire                    ap_start;       // result of comparing ap_start_line with vcnt
wire                    ap_end;         // result of comparing ap_end_line with vcnt
wire                    ff_start;       // result of comparing ff_start_line with vcnt
wire                    ff_end;         // result of comparing ff_end_line with vcnt
wire                    sav;            // asserted during XYZ word of SAV symbol
wire                    eav;            // asserted during XYZ word of EAV symbol
wire                    ap_crc_clr;     // clears the active picture CRC register
wire                    ff_crc_clr;     // clears the full field CRC register
reg     [9:0]           clipped_vid;    // output of video clipper function
reg                     ap_valid;       // ap_crc_valid internal signal
reg                     ff_valid;       // ff_crc_valid internal signal
reg                     prev_locked;    // locked input signal delayed once clock
wire                    locked_rise;    // asserted on rising edge of locked

//
// video clipper
//
// The SMPTE and ITU specifications require that the video data values used
// by the CRC calculation have the 2 LSBs both be ones if the 8 MSBs are all
// ones.
//
always @ (vid_in)
    begin
        clipped_vid[9:2] = vid_in[9:2];
        if (&vid_in[9:2])
            clipped_vid[1:0] = 2'b11;
        else
            clipped_vid[1:0] = vid_in[1:0];
    end

//
// decoding
//
// These assignments generate the ntsc, eav, and sav signals.
//
assign ntsc = (std == NTSC_422) || (std == NTSC_INVALID) ||
              (std == NTSC_422_WIDE) || (std == NTSC_4444);
assign sav = ~vid_in[6] & xyz_word;
assign eav = vid_in[6] & xyz_word;

//
// ap_region and ff_region generation
// 
// This code determines when the current video signal is within the active
// picture and full field CRC regions. Note that since the F bit changes before
// the end of the EDH full-field time period, the ff_end_line value is set
// to the opposite field value in the assignments below. That is, if F is low,
// normally indicating Field 1, the ff_end_line is assigned to xxx_FLD2_FF_LAST,
// not xxx_FLD1_FF_LAST as might be expected.
//

// This section looks up the starting and ending line numbers of the two CRC
// regions based on the current field and video standard.
always @ (ntsc or f)
    if (ntsc)
        begin
            if (~f)
                begin
                    ap_start_line = NTSC_FLD1_AP_FIRST;
                    ap_end_line =   NTSC_FLD1_AP_LAST;
                    ff_start_line = NTSC_FLD1_FF_FIRST;
                    ff_end_line =   NTSC_FLD2_FF_LAST;
                end
            else
                begin
                    ap_start_line = NTSC_FLD2_AP_FIRST;
                    ap_end_line =   NTSC_FLD2_AP_LAST;
                    ff_start_line = NTSC_FLD2_FF_FIRST;
                    ff_end_line =   NTSC_FLD1_FF_LAST;
                end
        end
    else
        begin
            if (~f)
                begin
                    ap_start_line = PAL_FLD1_AP_FIRST;
                    ap_end_line =   PAL_FLD1_AP_LAST;
                    ff_start_line = PAL_FLD1_FF_FIRST;
                    ff_end_line =   PAL_FLD2_FF_LAST;
                end
            else
                begin
                    ap_start_line = PAL_FLD2_AP_FIRST;
                    ap_end_line =   PAL_FLD2_AP_LAST;
                    ff_start_line = PAL_FLD2_FF_FIRST;
                    ff_end_line =   PAL_FLD1_FF_LAST;
                end
        end

// These four statements compare the current vcnt value to the starting and
// ending line numbers of the two CRC regions.          
assign ap_start = vcnt == ap_start_line;
assign ap_end =   vcnt == ap_end_line;
assign ff_start = vcnt == ff_start_line;
assign ff_end =   vcnt == ff_end_line;

// This code block generates the ap_region signal indicating when the current
// position is in the active-picture CRC region.
assign ap_crc_clr = ap_start & xyz_word & sav;

always @ (posedge clk or posedge rst)
    if (rst)
        ap_region <= 1'b0;
    else if (ce)
        begin
            if (ap_crc_clr)
                ap_region <= 1'b1;
            else if (ap_end & eav_next)
                ap_region <= 1'b0;
        end


// This code block generates teh ff_region signal indicating when the current
// position is in the full-field CRC region.
assign ff_crc_clr = ff_start & xyz_word & eav;

always @ (posedge clk or posedge rst)
    if (rst)
        ff_region <= 1'b0;
    else if (ce)
        begin
            if (ff_crc_clr)
                ff_region <= 1'b1;
            else if (ff_end & eav_next)
                ff_region <= 1'b0;
        end

//
// Valid bit generation
//
// This code generates the two CRC valid bits.
//
always @ (posedge clk or posedge rst)
    if (rst)
        prev_locked <= 1'b0;
    else if (ce)
        prev_locked <= locked;

assign locked_rise = ~prev_locked & locked;

always @ (posedge clk or posedge rst)
    if (rst)
        ap_valid <= 1'b0;
    else if (ce)
        begin
            if (locked_rise)
                ap_valid <= 1'b0;
            else if (locked & ap_crc_clr)
                ap_valid <= 1'b1;
        end

always @ (posedge clk or posedge rst)
    if (rst)
        ff_valid <= 1'b0;
    else if (ce)
        begin
            if (locked_rise)
                ff_valid <= 1'b0;
            else if (locked & ff_crc_clr)
                ff_valid <= 1'b1;
        end

//
// CRC calculations and registers
//
// Each CRC is calculated separately by an edh_crc16 module. Associted with
// each is a register. The register acts as an accumulation register and is
// fed back into the edh_crc16 module to be combined with the next video
// word. Enable logic for the registers determines which words are accumulated
// into the CRC value by controlling the load enables to the two registers.
//

// Active-picture CRC calculator
edh_crc16 apcrc16 (
    .c      (ap_crc_reg),
    .d      (clipped_vid),
    .crc    (ap_crc16)
);

// Active-picture CRC register
always @ (posedge clk or posedge rst)
    if (rst)
        ap_crc_reg <= 0;
    else if (ce)
        begin
            if (ap_crc_clr)
                ap_crc_reg <= 0;
            else if (ap_region & ~h)
                ap_crc_reg <= ap_crc16;
        end
        
// Full-field CRC calculator
edh_crc16 ffcrc16 (
    .c      (ff_crc_reg),
    .d      (clipped_vid),
    .crc    (ff_crc16)
);

// Full-field CRC register
always @ (posedge clk or posedge rst)
    if (rst)
        ff_crc_reg <= 0;
    else if (ce)
        begin
            if (ff_crc_clr)
                ff_crc_reg <= 0;
            else if (ff_region)
                ff_crc_reg <= ff_crc16;
        end
        
//
// Output assignments
//
assign ap_crc = ap_crc_reg;
assign ap_crc_valid = ap_valid;
assign ff_crc = ff_crc_reg;
assign ff_crc_valid = ff_valid;
                        
endmodule
