//------------------------------------------------------------------------------
// hdsdi_insert_ln.v
//
// This module inserts the line numbers into their appropriate places after
// the EAV.
//
//
//
//                  Author: John F. Snow
//                  Staff Applications Engineer
//
//                  Video Applications
//                  Advanced Products Group
//                  Xilinx, Inc.
//
//                  Copyright (c) 2003 Xilinx, Inc.
//                  All rights reserved
//
//                  RESTRICTED RIGHTS LEGEND
//
//      This software has not been published by the author, and 
//      has been disclosed to others for the purpose of enhancing 
//      and promoting design productivity in Xilinx products.
//
//      Therefore use, duplication or disclosure, now and in the 
//      future should give consideration to the productivity 
//      enhancements afforded the user of this code by the author's 
//      efforts.  Thank you for using our products !
//
// Disclaimer:  THESE DESIGNS ARE PROVIDED "AS IS" WITH NO WARRANTY 
//              WHATSOEVER AND XILINX SPECIFICALLY DISCLAIMS ANY 
//              IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
//              A PARTICULAR PURPOSE, OR AGAINST INFRINGEMENT.
//
// $Log: hdsdi_insert_ln.v,rcs $
// Revision 1.0  2004-05-21 13:27:53-06  jsnow
// Initial revision
//
// 
// Other modules instanced in this design:
//
//          None.
//------------------------------------------------------------------------------
/*
This module formats the 11-bit line number value into two 10-bit words and 
inserts them into their proper places immediately after the EAV word. The
insert_ln input can disable the insertion of line numbers. The same line
number value is inserted into both video channels. 

In the SMPTE 292M standard, the 11-bit line numbers must be formatted into two
10-bit words with the format of each word as follows:

        b9    b8    b7    b6    b5    b4    b3    b2    b1    b0
     +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
LN0: | ~ln6| ln6 | ln5 | ln4 | ln3 | ln2 | ln1 | ln0 |  0  |  0  |
     +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
LN1: |  1  |  0  |  0  |  0  | ln10| ln9 | ln8 | ln7 |  0  |  0  |
     +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
      

This module is purely combinatorial and has no delay registers.
*/

module hdsdi_insert_ln (
    insert_ln,      // enables insertion of line numbers when high
    ln_word0,       // asserted during first word of line number
    ln_word1,       // asserted during second word of line number
    c_in,           // C channel video input
    y_in,           // Y channel video input
    ln,             // 11-bit line number input
    c_out,          // C channel video output
    y_out           // Y channel video output
);

//-----------------------------------------------------------------------------
// Parameter definitions
//

//
// The VID_WIDTH parameter determines the width of the video input bus. For
// HD-SDI, this is normally always 10. Note that the code in this module
// may produce an error if VID_WIDTH is not 10.
//
parameter VID_WIDTH     = 10;                   // Width of video components
parameter VID_MSB       = VID_WIDTH - 1;        // MS bit # of video data path

//-----------------------------------------------------------------------------
// Signal definitions
//
// IO definitions
input                   insert_ln;
input                   ln_word0;
input                   ln_word1;
input   [VID_MSB:0]     c_in;
input   [VID_MSB:0]     y_in;
input   [10:0]          ln;
output  [VID_MSB:0]     c_out;
output  [VID_MSB:0]     y_out;

reg     [VID_MSB:0]     c_out;
reg     [VID_MSB:0]     y_out;

always @ (ln or insert_ln or c_in or ln_word0 or ln_word1)
    if (insert_ln & ln_word0)
        c_out <= {~ln[6], ln[6:0], 2'b00};
    else if (insert_ln & ln_word1)
        c_out <= {4'b1000, ln[10:7], 2'b00};
    else
        c_out <= c_in;

always @ (ln or insert_ln or y_in or ln_word0 or ln_word1)
    if (insert_ln & ln_word0)
        y_out <= {~ln[6], ln[6:0], 2'b00};
    else if (insert_ln & ln_word1)
        y_out <= {4'b1000, ln[10:7], 2'b00};
    else
        y_out <= y_in;

endmodule

