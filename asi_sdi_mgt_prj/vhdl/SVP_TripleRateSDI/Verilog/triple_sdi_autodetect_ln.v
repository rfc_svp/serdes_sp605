//------------------------------------------------------------------------------ 
// Copyright (c) 2008 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Solutions Development Group, Xilinx, Inc.
//  \   \        Filename: $RCSfile: triple_sdi_autodetect_ln.v,rcs $
//  /   /        Date Last Modified:  $Date: 2008-11-14 16:32:58-07 $
// /___/   /\    Date Created: June 25, 2008
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: triple_sdi_autodetect_ln.v,rcs $
// Revision 1.2  2008-11-14 16:32:58-07  jsnow
// Added register initializers.
//
// Revision 1.1  2008-11-10 08:37:21-07  jsnow
// Comment changes only.
//
// Revision 1.0  2008-07-31 14:03:47-06  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*

This module will examine a HD data stream and detect the transport format. It
detects all the video standards currently supported by SMPTE 292M-2006 (SMPTE 
260M, SMPTE 274M, and SMPTE 296M) plus the 1080PsF video formats described in
SMPTE RP 211 and the legacy SMPTE 295M format. The module can also be used with
a 3G-SDI receiver and recognizes all video formats supported by SMPTE 425M-2007, 
including the SMPTE 428 digital cinema format.

Note that this module detects transport timing and not necessarily  the actual 
video format. The module counts words and lines to  determine the video 
standard. It does not depend on the inclusion of ANC packets identifying the
video standard.

This module also produces a line number value indicating the current line number.
This line number value changes on the rising edge of clock following the XYZ
word of the EAV so that is valid for insertion into the LN field of an HD-SDI
stream.

The module requires as input only one of the channels of the video stream,
either Y or C. It also requires as input the decoded signals eav and sav. These
inputs must be asserted only during the XYZ word of the EAV or SAV, 
respectively..

The a3g input must be 1 for 3G-SDI level A and 0 for all other modes. This 
allows it to use an alternate set of word counts to correctly identify the 
various video formats in 3G-SDI level A mode where the clock will be twice as
fast as it should be except for the 1080p 50 Hz / 60 Hz standards. It also 
allows the module to differentiate between 1080p 50 Hz and 1080p 25 Hz and 
between 1080p 60 Hz and 1080p 30 Hz.

Normally, when the input video standard changes, this module will wait for
some number of video frames (determine by MAX_ERRCNT) before beginning the 
process of identifying and locking to the new video format. This is to prevent
a few errors in the video from causing the module to lose lock. However, it also
increases the latency for the module to lock to a new standard when the input
video standard is deliberately changed. If some logic external to this module
knows that a deliberate input video standard change has been done, it can assert
this module's reacquire input for one clock cycle to force the module to 
immediately begin the process of identifying and locking to the new video
standard.

The module generates the following outputs:

locked: Indicates when the module has locked to the incoming video standard. The
std and ln outputs are only valid when locked is a 1.

std: A 4-bit code indicating which transport format has been detected encoded
as follows (rates are frame rate):
    
    0000: SMPTE 260M 1035i           30Hz
    0001: SMPTE 295M 1080i           25Hz
    0010: SMPTE 274M 1080i or 1080sF 30Hz
    0011: SMPTE 274M 1080i or 1080sF 25Hz
    0100: SMPTE 274M 1080p           30Hz   
    0101: SMPTE 274M 1080p           25Hz   
    0110: SMPTE 274M 1080p           24Hz
    0111: SMPTE 296M 720p            60Hz
    1000: SMPTE 274M 1080sF          24Hz
    1001: SMPTE 296M 720p            50Hz
    1010: SMPTE 296M 720p            30Hz
    1011: SMPTE 296M 720p            25Hz
    1100: SMPTE 296M 720p            24Hz
    1101: SMPTE 296M 1080p           60Hz   (3G-SDI level A only)
    1110: SMPTE 296M 1080p           50Hz   (3G-SDI level A only)

ln: An 11-bit line number code indicating the current line number. This code
changes on the rising edge of the clock when both xyz and eav are asserted. This
allows the ln code to be available just in time for encoding and insertion into
the two words that immediately follow the EAV. However, care must be taken to
insure that this path meets timing.
 
ln_valid: Asserted whenever the locked output is asserted and the line number
generator has started generating valid line numbers.

Note that the std code does not distinguish between the /1 and /M transport
formats. So, the code 0010 can represent either a true 30Hz signal or 30Hz/M.
Also note that this module is unable to distinguish between the SMPTE 274M
1080i standards and the corresponding 1080sF standards since they both have
exactly the same video format in terms of number of lines per frame and words
per line.

This module will detect the SMPTE 428 digital cinema 2048x1080 24p or 24sF
as SMPTE 274M 1080p 24Hz or 2080p 24sF, respectively, because these are the
transport protocols that carry the container file for the digital cinema
format.
*/

module triple_sdi_autodetect_ln #(
    parameter                       VCNT_WIDTH = 11,
    parameter                       HCNT_WIDTH = 13)
(
    input  wire                     clk,            // 74.25MHz or 74.25MHz/1.001 video clock input
    input  wire                     rst,            // async reset input
    input  wire                     ce,             // clock enable input
    input  wire [8:7]               vid_in,         // C or Y channel video input (only bits 8:7 are needed)
    input  wire                     eav,            // XYZ word of EAV
    input  wire                     sav,            // XYZ word of SAV
    input  wire                     reacquire,      // force module to decode & lock to new standard more quickly
    input  wire                     a3g,            // 1 = 3G-SDI level A
    output wire [3:0]               std,            // video standard code output
    output wire                     locked,         // asserted when locked to video
    output wire [VCNT_WIDTH-1:0]    ln,             // line number output
    output wire                     ln_valid        // asserted when ln is valid
);

//-----------------------------------------------------------------------------
// Parameter definitions
//

// The HCNT_MSB parameter controls the width of the word counter. It is set to 
// be one bit more than required for the maximum horizontal line width because, 
// in 3G-SDI level A mode, the word counter increments twice per sample for the
// 74.25 MHz sampled video formats.
//
localparam HCNT_MSB     = HCNT_WIDTH;           // MS bit # of horizontal counter
localparam VCNT_MSB     = VCNT_WIDTH - 1;       // MS bit # of vertical counter

//
// Video standard parameters
//
localparam [3:0]
    VSTD_260M           = 0,    // SMPTE 260M 1035i  30Hz
    VSTD_295M           = 1,    // SMPTE 295M 1080i  25Hz
    VSTD_274M_30i       = 2,    // SMPTE 274M 1080i  30Hz or 1080sF 30Hz
    VSTD_274M_25i       = 3,    // SMPTE 274M 1080i  25Hz or 1080sF 25Hz
    VSTD_274M_30p       = 4,    // SMPTE 274M 1080p  30Hz
    VSTD_274M_25p       = 5,    // SMPTE 274M 1080p  25Hz
    VSTD_274M_24p       = 6,    // SMPTE 274M 1080p  24Hz
    VSTD_296M           = 7,    // SMPTE 296M 720p   60Hz
    VSTD_RP211_24sF     = 8,    // SMPTE 274M 1080sF 24Hz
    VSTD_296M_50        = 9,    // SMPTE 296M 720p   50Hz
    VSTD_296M_30        = 10,   // SMPTE 296M 720p   30Hz
    VSTD_296M_25        = 11,   // SMPTE 296M 720p   25Hz
    VSTD_296M_24        = 12,   // SMPTE 296M 720p   24Hz
    VSTD_274M_60p       = 13,   // SMPTE 296M 1080p  60Hz (3G-SDI level A only)
    VSTD_274M_50p       = 14;   // SMPTE 296M 1080p  50Hz (3G-SDI level A only)

//
// The following parameters must be equal to the value of the last video format
// code in the table above. It is used to indicate the terminal count value of
// a counter used to search for valid video formats.
//
localparam LAST_VIDEO_FORMAT_CODE_NORMAL = VSTD_296M_24;
localparam LAST_VIDEO_FORMAT_CODE_3GA    = VSTD_274M_50p;

//
// State machine state assignments
//
localparam [3:0]
    ACQ0    = 4'd0,
    ACQ1    = 4'd1,
    ACQ2    = 4'd2,
    ACQ3    = 4'd3,
    ACQ4    = 4'd4,
    ACQ5    = 4'd5,
    ACQ6    = 4'd6,
    ACQ7    = 4'd7,
    LCK0    = 4'd8,
    LCK1    = 4'd9,
    LCK2    = 4'd10,
    LCK3    = 4'd11,
    LCK4    = 4'd12,
    ERR     = 4'd13;

//
// The MAX_ERRCNT parameter indicates the maximum number of consecutive frames 
// that do not match the currently locked standard's parameters. If MAX_ERRCNT
// is exceeded the locked signal will be negated and the state machine will
// attempt to match the new video standard.
//
// Increasing the MAX_ERRCNT value improves the tolerance to errors in the
// video stream. However, it also increases the latency for the module to
// lock to a new standard when the input standard changes. If some external
// logic knows that the input video standard has changed, the state machine
// can be forced to reacquire the new standard more quickly by asserted the
// reacquire input for one clock cycle. This forces the state machine to start
// the process of identifying the new standard without having to wait for
// it to reach the MAX_ERRCNT number of errored frames before starting this
// process.
//
parameter [2:0]
    MAX_ERRCNT          = 3'b010;       // max # of errored frames before unlocking

//-----------------------------------------------------------------------------
// Signal definitions
//


// Internal signals
reg     [3:0]           std_int = 0;        // internal video std output code
reg     [HCNT_MSB:0]    word_counter = 0;   // counts words per line
reg     [HCNT_MSB:0]    trs_to_counter = 0; // TRS timeout counter
reg     [VCNT_MSB:0]    line_counter = 0;   // counts lines per field or frame
reg     [3:0]           current_state = ACQ0;// FSM current state
reg     [3:0]           next_state;         // FSM next state
reg                     en_wcnt;            // enables word counter
reg                     en_lcnt;            // enables line counter
reg                     clr_wcnt;           // clears word counter
reg                     clr_lcnt;           // clears line counter
reg                     set_locked;         // asserts the locked signal
reg                     clr_locked;         // clears the locked signal
reg                     clr_errcnt;         // clears the error counter
reg                     inc_errcnt;         // increments the error counter
reg     [3:0]           loops = 0;          // iteration loop counter used by FSM
reg                     clr_loops;          // clears loop counter
reg                     inc_loops;          // increments loop counter
wire                    loops_tc;           // asserted when loop counter equals 8
reg                     ld_std;             // load std register
reg     [2:0]           errcnt = 0;         // error counter
wire                    maxerrs;            // asserted when error counter reaches max allowed
wire                    match;              // asserted when video standard match is found
reg                     match_words = 1'b0; // word counter matches video standard
reg                     match_lines = 1'b0; // line counter matches video standard
reg                     compare_sel;        // controls comparator input MUX
wire    [3:0]           cmp_mux;            // comparator input MUX
wire    [HCNT_MSB-1:0]  wpl;                // calculated words per line (corrected for 3GA mode)
reg     [HCNT_MSB-1:0]  cmp_wcnt;           // word count comparison value
reg     [VCNT_MSB:0]    cmp_lcnt;           // line count comparison value
reg                     first_act = 1'b0;   // asserted on first active line
reg                     last_v = 1'b0;      // registered version of V bit from last line
wire                    v;                  // vertical blanking interval indicator (V bit)
wire                    f;                  // field indicator (F bit)
wire                    trs_timeout;        // timed out waiting for TRS
wire                    first_timeout;      // timed out waiting for first line
wire                    timeout;            // timeout condition
reg                     locked_q = 1'b0;    // locked flip-flop
reg                     ln_valid_q = 1'b0;  // ln_valid flip-flop
reg     [7:0]           reset_delay = 0;    // delay register for reset signal
wire                    reset;              // module reset
reg     [VCNT_MSB:0]    ln_counter = 0;     // counter for the ln generator
reg     [VCNT_MSB:0]    ln_init;            // init value for the counter
reg     [VCNT_MSB:0]    ln_max;             // max ln value for the current std
wire                    ln_tc;              // asserted when ln_counter = ln_max
wire                    ln_load;            // loads the ln_counter with ln_init
reg     [3:0]           std_reg = 0;        // holds internal copy of std for ln generation logic
reg                     reacquire_sync = 1'b0;
reg                     reacquire_q = 1'b0;
wire    [3:0]           last_code;
reg                     a3g_reg = 1'b0;

always @ (posedge clk)
    a3g_reg <= a3g;

//
// reacquire synchronizer
//
// Since reacquire is a direct input to the FSM from parts unknown, make sure
// it is synchronous to the local clock before feeding it to the state machine.
//
always @ (posedge clk or posedge reset)
    if (reset)
        reacquire_q <= 1'b0;
    else if (ce)
        reacquire_q <= reacquire;

always @ (posedge clk or posedge reset)
    if (reset)
        reacquire_sync <= 1'b0;
    else if (ce)
        reacquire_sync <= reacquire_q;

//
// word counter
//
// The word counter counts the number of words detected during a video line.
//
always @ (posedge clk or posedge reset)
    if (reset)
        word_counter <= 0;
    else if (ce)
        begin
            if (clr_wcnt)
                word_counter <= 0;
            else if (en_wcnt)
                word_counter <= word_counter + 1;
        end

//
// trs timeout generator
//
// This timer will timeout if TRS symbols are not received on a periodic
// basis, causing the trs_timeout signal to be asserted.
//
always @ (posedge clk or posedge reset)
    if (reset)
        trs_to_counter <= 0;
    else if (ce)
        begin
            if (eav | sav)
                trs_to_counter <= 0;
            else
                trs_to_counter <= trs_to_counter + 1;
        end

assign trs_timeout = &trs_to_counter;

//
// line counter
//
// The line counter counts the number of lines in a field or frame. The 
// first_timeout signal will be asserted if the line counter reaches terminal
// count before the FSM clears the counter at the beginning of a new field or
// frame.
// 
always @ (posedge clk or posedge reset)
    if (reset)
        line_counter <= 0;
    else if (ce)
        begin
            if (clr_lcnt)
                line_counter <= 0;
            else if (en_lcnt & eav)
                line_counter <= line_counter + 1;
        end

assign first_timeout = &line_counter;

//
// The timeout signal will be asserted if either trs_timeout or first_timeout
// are asserted.
//
assign timeout = trs_timeout | first_timeout;

//
// error counter
//
// The error counter is incremented by the FSM when an error is detected. When
// the error counter reaches MAX_ERRCNT, the maxerrs signal will be asserted.
//
always @ (posedge clk or posedge reset)
    if (reset)
        errcnt <= 0;
    else if (ce)
        begin
            if (clr_errcnt)
                errcnt <= 0;
            else if (inc_errcnt)
                errcnt <= errcnt + 1;
        end

assign maxerrs = errcnt == MAX_ERRCNT;

//
// loop counter
//
// The loop counter is a 4-bit binary up counter used by the FSM. It is used to
// cycle through the word & line count values for each of the supported video
// standards so that they can be compared to the values found in the input
// video stream. The loops_tc signal is asserted when the loop counter reaches
// its terminal count.
//
always @ (posedge clk or posedge reset)
    if (reset)
        loops <= 0;
    else if (ce)
        begin
            if (clr_loops)
                loops <= 0;
            else if (inc_loops)
                loops <= loops + 1;
        end

assign last_code = a3g_reg ? LAST_VIDEO_FORMAT_CODE_3GA : LAST_VIDEO_FORMAT_CODE_NORMAL;
assign loops_tc = loops == last_code;


//
// std_int register
//
// This register holds the detected video standard code.
//
always @ (posedge clk or posedge reset)
    if (reset)
        std_int <= 0;
    else if (ce)
        if (ld_std)
            std_int <= loops;

assign std = std_int;

//
// video timing logic
//
// The following logic generates various video timing signals:
//
// v is the vertical blanking indicator bit and is only valid when eav or sav
// are asserted. v is really just the vid_in[7] bit, but is reassigned to the
// more descriptive v signal.
//
// f is the field indicator bit and is only valid when eav or sav are asserted.
// f is really just the vid_in[8] bit, but is reassigned to the more descriptive
// f signal.
//
// last_v is a register that holds the value of v from the last line. This
// register loads whenever eav is asserted. It is used to detect the rising
// edge of the V signal to generate the first_act signal.
//
// first_act indicates the first active line of a field or frame.  
//
assign v = vid_in[7];
assign f = vid_in[8];

always @ (posedge clk or posedge reset)
    if (reset)
        last_v <= 1'b0;
    else if (ce)
        if (eav)
            last_v <= v;

always @ (posedge clk or posedge reset)
    if (reset)
        first_act <= 1'b0;
    else if (ce)
        if (eav)
            first_act <= last_v & ~v;

//
// locked flip-flop
//
// This flip flop is controlled by the finite state machine.
//
always @ (posedge clk or posedge reset)
    if (reset)
        locked_q <= 1'b0;
    else if (ce)
        begin
            if (clr_locked)
                locked_q <= 1'b0;
            else if (set_locked)
                locked_q <= 1'b1;
        end

assign locked = locked_q;

//
// comparison logic
//
// The comparison logic is used to compare the word and line counts found in 
// the video stream against the known values for the various video standards.
// To reduce the size of the implementation, the word and line counts are
// compared sequentially against the known word and line counts using one
// comparator for the words and one for the lines, rather than doing a parallel
// comparison against all known values. The FSM controls this sequential search.
// When running in 3G-SDI level A mode, the actual words counts will be double
// the values given here. In that mode, the word counter value is divided by
// 2 before being compared to the cmp_wcnt value. Thus the values given in the
// case statement for 1080p 50 Hz and 60 Hz are half as much as the actual
// values.
//
always @ (*)
    case (cmp_mux)
        4'b0000:     cmp_wcnt <= 2200;
        4'b0001:     cmp_wcnt <= 2376;
        4'b0010:     cmp_wcnt <= 2200;
        4'b0011:     cmp_wcnt <= 2640;
        4'b0100:     cmp_wcnt <= 2200;
        4'b0101:     cmp_wcnt <= 2640;
        4'b0110:     cmp_wcnt <= 2750;
        4'b0111:     cmp_wcnt <= 1650;
        4'b1000:     cmp_wcnt <= 2750;
        4'b1001:     cmp_wcnt <= 1980;
        4'b1010:     cmp_wcnt <= 3300;
        4'b1011:     cmp_wcnt <= 3960;
        4'b1100:     cmp_wcnt <= 4125;
        4'b1101:     cmp_wcnt <= 1100;      // 2200 / 2 for 1080p 60 Hz
        4'b1110:     cmp_wcnt <= 1320;      // 2640 / 2 for 1080p 50 Hz
        default:     cmp_wcnt <= 2200;
    endcase

always @ (*)
    case (cmp_mux)
        4'b0000:     cmp_lcnt <= 517;
        4'b0001:     cmp_lcnt <= 540;
        4'b0010:     cmp_lcnt <= 540;
        4'b0011:     cmp_lcnt <= 540;
        4'b0100:     cmp_lcnt <= 1080;
        4'b0101:     cmp_lcnt <= 1080;
        4'b0110:     cmp_lcnt <= 1080;
        4'b0111:     cmp_lcnt <= 720;
        4'b1000:     cmp_lcnt <= 540;
        4'b1001:     cmp_lcnt <= 720;
        4'b1010:     cmp_lcnt <= 720;
        4'b1011:     cmp_lcnt <= 720;
        4'b1100:     cmp_lcnt <= 720;
        4'b1101:     cmp_lcnt <= 1080;
        4'b1110:     cmp_lcnt <= 1080;
        default:     cmp_lcnt <= 540;
    endcase

assign cmp_mux = compare_sel ? std_int : loops;
assign wpl = a3g_reg ? word_counter[HCNT_MSB:1] : word_counter[HCNT_MSB-1:0];

always @ (posedge clk)
    if (ce)
    begin
        match_words <= wpl == cmp_wcnt;
        match_lines <= line_counter == cmp_lcnt;
    end

assign match = match_words & match_lines;

//
// Finite state machine
//
always @ (posedge clk or posedge reset)
    if (reset)
        current_state <= ACQ0;
    else if (ce)
        if (reacquire_sync)
            current_state <= ACQ0;
        else
            current_state <= next_state;

always @ (*)
    case (current_state)
        ACQ0:   if (sav & first_act)
                    next_state <= ACQ1;
                else
                    next_state <= ACQ0;

        ACQ1:   if (sav)
                    next_state <= ACQ2;
                else
                    next_state <= ACQ1;

        ACQ2:   if (sav & v)
                    next_state <= ACQ3;
                else
                    next_state <= ACQ2;

        ACQ3:   next_state <= ACQ4;

        ACQ4:   next_state <= ACQ5;

        ACQ5:   if (match)
                    next_state <= ACQ7;
                else
                    next_state <= ACQ6;

        ACQ6:   if (loops_tc)
                    next_state <= ACQ0;
                else
                    next_state <= ACQ4;

        ACQ7:   next_state <= LCK0;

        LCK0:   if (timeout)
                    next_state <= ERR;
                else if (sav & first_act & ~f)
                    next_state <= LCK1;
                else
                    next_state <= LCK0;

        LCK1:   if (timeout)
                    next_state <= ERR;
                else if (sav)
                    next_state <= LCK2;
                else 
                    next_state <= LCK1;

        LCK2:   if (timeout)
                    next_state <= ERR;
                else if (sav & v)
                    next_state <= LCK3;
                else
                    next_state <= LCK2;

        LCK3:   next_state <= LCK4;

        LCK4:   if (match)
                    next_state <= LCK0;
                else
                    next_state <= ERR;

        ERR:    if (maxerrs)
                    next_state <= ACQ0;
                else
                    next_state <= LCK0;

        default:
                next_state <= ACQ0;
    endcase

always @ (*)
begin
    en_wcnt         <= 1'b0;
    en_lcnt         <= 1'b0;
    clr_wcnt        <= 1'b0;
    clr_lcnt        <= 1'b0;
    set_locked      <= 1'b0;
    clr_locked      <= 1'b0;
    clr_errcnt      <= 1'b0;
    inc_errcnt      <= 1'b0;
    clr_loops       <= 1'b0;
    inc_loops       <= 1'b0;
    ld_std          <= 1'b0;
    compare_sel     <= 1'b0;

    case (current_state)
        
        ACQ0:   begin
                    clr_errcnt <= 1'b1;
                    clr_locked <= 1'b1;
                    clr_wcnt <= 1'b1;
                    clr_lcnt <= 1'b1;
                end

        ACQ1:   begin
                    en_wcnt <= 1'b1;
                    en_lcnt <= 1'b1;
                end

        ACQ2:   en_lcnt <= 1'b1;

        ACQ3:   clr_loops <= 1'b1;

        ACQ6:   inc_loops <= 1'b1;

        ACQ7:   begin
                    ld_std <= 1'b1;
                    clr_wcnt <= 1'b1;
                    set_locked <= 1'b1;
                end

        LCK0:   begin
                    en_wcnt <= 1'b1;
                    clr_lcnt <= 1'b1;
                    compare_sel <= 1'b1;
                    if (sav)
                        clr_wcnt <= 1'b1;
                end

        LCK1:   begin
                    en_wcnt <= 1'b1;
                    en_lcnt <= 1'b1;
                    compare_sel <= 1'b1;
                end
        
        LCK2:   begin
                    en_lcnt <= 1'b1;
                    compare_sel <= 1'b1;
                end
        
        LCK3:   begin
                    compare_sel <= 1'b1;
                end
        
        LCK4:   begin
                    compare_sel <= 1'b1;
                    clr_wcnt <= 1'b1;
                    if (match) 
                        clr_errcnt <= 1'b1;
                end

        ERR:    begin
                    inc_errcnt <= 1'b1;
                    clr_wcnt <= 1'b1;
                    compare_sel <= 1'b1;
                end
    endcase
end

//
// Reset logic
//
// The reset signal is maintained to the logic in this module for a few
// clock cycles after the global reset signal is removed.
//
assign reset = rst | ~reset_delay[7];

always @ (posedge clk or posedge rst)
    if (rst)
        reset_delay <= 0;
    else
        reset_delay <= {reset_delay[6:0], 1'b1};

//
// ln generator
//
// This code implements the line number generator. The line number generator
// is a counter that increments on the XYZ word of each EAV. It is initialized
// on the first active line of either  field. The initial value depends on
// the current standard and the field bit. The counter wraps back to zero when 
// it reaches the maximum line count for the current standard.
//
always @ (posedge clk or posedge rst)
    if (rst)
        std_reg <= 4'b0000;
    else if (ce)
        if (set_locked)
            std_reg <= std_int;


assign ln_load = last_v & ~v;


always @ (posedge clk or posedge rst)
    if (rst)
        ln_valid_q <= 1'b0;
    else if (ce)
        if (~locked_q)
            ln_valid_q <= 1'b0;
        else if (eav & ln_load)
            ln_valid_q <= 1'b1;

assign ln_valid = ln_valid_q;

always @ (std_reg or f)
    case({std_reg, f})
        // SMPTE 260M 1035i 30Hz
        5'b0000_0: ln_init <= 11'd41;
        5'b0000_1: ln_init <= 11'd603;
    
        // SMPTE 295M 1080i 25Hz
        5'b0001_0: ln_init <= 11'd81;
        5'b0001_1: ln_init <= 11'd706;
    
        // SMPTE 274M 1080i or 1080sF 30Hz
        5'b0010_0: ln_init <= 11'd21;
        5'b0010_1: ln_init <= 11'd584;
    
        // SMPTE 274M 1080i or 1080sF 25 Hz
        5'b0011_0: ln_init <= 11'd21;
        5'b0011_1: ln_init <= 11'd584;
    
        // SMPTE 274M 1080p 30Hz
        5'b0100_0: ln_init <= 11'd42;
        5'b0100_1: ln_init <= 11'd42;
    
        // SMPTE 274M 1080p 25Hz
        5'b0101_0: ln_init <= 11'd42;
        5'b0101_1: ln_init <= 11'd42;
    
        // SMPTE 274M 1080p 24Hz
        5'b0110_0: ln_init <= 11'd42;
        5'b0110_1: ln_init <= 11'd42;
    
        // SMPTE 296M 720p all rates
        5'b0111_0: ln_init <= 11'd26;
        5'b0111_1: ln_init <= 11'd26;
        5'b1001_0: ln_init <= 11'd26;
        5'b1001_1: ln_init <= 11'd26;
        5'b1010_0: ln_init <= 11'd26;
        5'b1010_1: ln_init <= 11'd26;
        5'b1011_0: ln_init <= 11'd26;
        5'b1011_1: ln_init <= 11'd26;
        5'b1100_0: ln_init <= 11'd26;
        5'b1100_1: ln_init <= 11'd26;
    
        // SMPTE 274M 1080sF 24Hz
        5'b1000_0: ln_init <= 11'd21;
        5'b1000_1: ln_init <= 11'd584;

        default: ln_init <= 11'd21;     
    endcase                    

always @ (std_reg)
    case(std_reg)
        4'b0000: ln_max <= 11'd1125;
        4'b0001: ln_max <= 11'd1250;
        4'b0010: ln_max <= 11'd1125;
        4'b0011: ln_max <= 11'd1125;
        4'b0100: ln_max <= 11'd1125;
        4'b0101: ln_max <= 11'd1125;
        4'b0110: ln_max <= 11'd1125;
        4'b0111: ln_max <= 11'd750;
        4'b1000: ln_max <= 11'd1125;
        4'b1001: ln_max <= 11'd750;
        4'b1010: ln_max <= 11'd750;
        4'b1011: ln_max <= 11'd750;
        4'b1100: ln_max <= 11'd750;
        4'b1101: ln_max <= 11'd1125;
        4'b1110: ln_max <= 11'd1125;
        default: ln_max <= 11'd1125;
    endcase

assign ln_tc = ln_counter == ln_max;

always @ (posedge clk or posedge rst)
    if (rst)
        ln_counter <= 11'd1;
    else if (ce & eav) 
        begin
            if (ln_load)
                ln_counter <= ln_init;
            else if (ln_tc)
                ln_counter <= 11'd1;
            else
                ln_counter <= ln_counter + 1;
        end

assign ln = ln_counter;

endmodule