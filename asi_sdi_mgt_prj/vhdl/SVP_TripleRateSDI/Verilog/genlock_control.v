//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow
//  \   \        Filename: $RCSfile: genlock_control.v,v $
//  /   /        Date Last Modified:  $Date: 2010-04-08 15:12:11-06 $
// /___/   /\    Date Created: November 23, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: genlock_control.v,v $
// Revision 1.0  2010-04-08 15:12:11-06  reedt
// Initial revision
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module controls the Si5324 devices to implement a genlock clock generator.
The module is designed to work with 3 Si5324 devices. The first device takes
in either the local 27 MHz clock or horizontal sync from the sync separator. It
always outputs 27 MHz. The other two Si5324 devices receive the 27 MHz clock
from the first Si5324 and generate 148.5 MHz and 148.35 MHz reference clocks.

Altenatively, the module can also work with 2 Si5324 devices, one creating the
master 27 MHz clock and the other taking this 27 MHz clock and creating one
MGT reference clock. In this case, the Si5324_LOL_2 input should be wired low.
*/

`timescale 1ns / 1 ns

module genlock_control 
#(
    parameter GENLOCK_SI5324_BW_SEL = 4'b0001)
(
    input   wire        clk,                        // must be the AVB FMC interface clock (27 MHz)
    input   wire        hsync_in,                   // connect to HSYNC signal from sync separator
    input   wire        genlock_enable,             // 1 enables genlock mode
    input   wire [4:0]  default_code,               // in_freq_sel code to output in non-genlock mode
    input   wire [3:0]  default_bw_sel,             // bandwidth to use in non-genlock mode
    input   wire [10:0] sync_video_fmt,             // 11-bit code from LMH1981
    input   wire [2:0]  sync_frame_rate,            // 3-bit code indicating frame rate
    input   wire        Si5324_LOL_0,               // LOL signal from main Si5324
    input   wire        Si5324_LOL_1,               // LOL signal from secondary Si5324 1
    input   wire        Si5324_LOL_2,               // LOL signal from secondary Si5324 2
    output  reg         Si5324_clk_sel = 1'b0,      // Main Si5324 clock input select
    output  reg  [4:0]  in_freq_sel = 0,            // Main Si5324 input frequency select code
    output  reg  [3:0]  Si5324_bw_sel = 0,          // Main Si5324 bandwidth select
    output  reg         Si5324_locked = 1'b0,       // Indicates when the main Si5324 is locked for the first time
    output  wire        sync_missing,               // 1 = video sync error
    output  wire        sync_invalid,               // 1 = unsupported sync frequency       
    output  reg         local_lock = 1'b0,          // 1 = locked to local XO
    output  reg         genlock = 1'b0,             // 1 = locked to external video sync
    output  reg         Si5324_DHOLD = 1'b0         // connect to DHOLD inputs of the secondary Si5324 devices
);

localparam [2:0]
    MODE_NTSC   = 3'b000,
    MODE_PAL    = 3'b001,
    MODE_720p   = 3'b010,
    MODE_1080i  = 3'b100,
    MODE_1080p  = 3'b110,
    MODE_ERROR  = 3'b111;
             
localparam [5:0] FREQ_SEL_ERR = 6'b100000;          // Specifies encoding of freq_sel when error occurs
localparam       LOCK_CNT_MSB = 22;                 // Sets the width of the lock timer counter
localparam       SHORT_DELAY_BIT = 15;              // Sets the DHOLD start delay length

//
// Finite state machine state definitions
//
localparam [3:0]
    STATE_START         = 4'd0,
    STATE_INIT1         = 4'd1,
    STATE_INIT2         = 4'd2,
    STATE_INIT3         = 4'd3,
    STATE_LOCAL_LOCK    = 4'd4,
    STATE_CHANGE1       = 4'd5,
    STATE_CHANGE2       = 4'd6,
    STATE_CHANGE3L      = 4'd7,
    STATE_CHANGE4       = 4'd8,
    STATE_CHANGE5       = 4'd9,
    STATE_CHANGE6       = 4'd10,
    STATE_CHANGE7       = 4'd11,
    STATE_GENLOCK       = 4'd12,
    STATE_CHANGE3G      = 4'd13,
    STATE_CHANGEH       = 4'd14;

//
// Signal definitions
//
reg  [3:0]              current_state = STATE_START;
reg  [3:0]              next_state;
reg  [2:0]              mode = 0;
reg  [5:0]              freq_sel = 0;
reg  [1:0]              Si5324_LOL_0_sync = 0;
reg  [1:0]              Si5324_LOL_12_sync = 0;
reg  [LOCK_CNT_MSB:0]   lock_counter = 0;
wire                    lock_counter_tc;
wire                    short_delay_tc;
reg                     clr_lock_counter;
reg                     en_auto_clr_main;
reg                     en_auto_clr_second;
reg                     set_dhold;
reg                     clr_dhold;
reg  [1:0]              genlock_enable_sync = 0;
reg                     genlock_enable_change = 1'b0;
reg                     load_genlock;
reg                     set_default_freq;
reg                     set_hsync_freq;
wire                    sync_OK;
reg                     sync_missing_reg = 1'b0;
reg                     sync_invalid_reg = 1'b0;
reg                     genlock_reg = 1'b0;
reg [12:0]              hsync_timer = 0;
reg [2:0]               hsync_sync = 3'b0;
wire                    hsync_leading_edge;
wire                    hsync_timeout;
reg                     clr_lock;
reg                     set_genlock;
reg                     set_local_lock;
reg                     set_Si5324_locked;
wire                    hsync_freq_change;
reg [2:0]               prev_mode;
reg [5:0]               prev_freq_sel;

//------------------------------------------------------------------------------
// Horizontal sync activity indicator
//
// This code serves as a watchdog to determine if there is activity on the
// horizontal sync signal. Without this watchdog, there is no way for this
// module to determine if the external sync input is running or stopped.
//
always @ (posedge clk)
    hsync_sync <= {hsync_sync[1:0], hsync_in};

assign hsync_leading_edge = hsync_sync[2] & ~hsync_sync[1];

always @ (posedge clk)
    if (hsync_leading_edge)
        hsync_timer <= 0;
    else if (~hsync_timeout)
        hsync_timer <= hsync_timer + 1;

assign hsync_timeout = hsync_timer[12];

always @ (posedge clk)
    sync_missing_reg <= hsync_timeout;

assign sync_missing = sync_missing_reg;

//------------------------------------------------------------------------------
// Sync frequency validation & Si5324 input frequency selection
//
// Based on the video format counts from the sync separator, determine the 
// video mode and calculate an appropriate input frequency select setting
// for the main Si5324. Also generate a bandwidth select value for the downstream
// Si5324 devices and a clock input select signal from the main Si5324.
//
// Generate a sync_OK signal if the video sync is valid. This is an input to the
// FSM.
//
// Finally, based on control signals from the FSM, set the in_freq_sel and 
// Si5324_clk_sel output ports either to the default mode for non-genlocked mode
// or to the appropriate horizontal sync frequency when in genlock mode.
// 
always @ (posedge clk)
    case(sync_video_fmt)
        11'd259:    mode <= MODE_NTSC;
        11'd260:    mode <= MODE_NTSC;
        11'd309:    mode <= MODE_PAL;
        11'd310:    mode <= MODE_PAL;
        11'd747:    mode <= MODE_720p;
        11'd559:    mode <= MODE_1080i;
        11'd560:    mode <= MODE_1080i;
        11'd1122:   mode <= MODE_1080p;
        default:    mode <= MODE_ERROR;
    endcase

always @ (posedge clk)
    prev_mode <= mode;

always @ (posedge clk)
    case(mode)
        MODE_NTSC:  freq_sel <= 6'h00;
        MODE_PAL:   freq_sel <= 6'h02;
        MODE_720p:
            case(sync_frame_rate)
                3'b000:     freq_sel <= 6'h05;  // 23.98 Hz
                3'b001:     freq_sel <= 6'h04;  // 24 Hz
                3'b010:     freq_sel <= 6'h06;  // 25 Hz
                3'b011:     freq_sel <= 6'h08;  // 29.97 Hz
                3'b100:     freq_sel <= 6'h07;  // 30 Hz
                3'b101:     freq_sel <= 6'h09;  // 50 Hz
                3'b110:     freq_sel <= 6'h0b;  // 59.94 Hz
                default:    freq_sel <= 6'h0a;  // 60 Hz
            endcase
        MODE_1080i:
            case(sync_frame_rate)
                3'b010:     freq_sel <= 6'h0c;  // 25 Hz
                3'b011:     freq_sel <= 6'h0e;  // 29.97 Hz
                3'b100:     freq_sel <= 6'h0d;  // 30 Hz
                default:    freq_sel <= FREQ_SEL_ERR;   // error
            endcase
    
        MODE_1080p:
            case(sync_frame_rate)
                3'b000:     freq_sel <= 6'h10;  // 23.98 Hz
                3'b001:     freq_sel <= 6'h0f;  // 24 Hz
                3'b010:     freq_sel <= 6'h11;  // 25 Hz
                3'b011:     freq_sel <= 6'h13;  // 29.97 Hz
                3'b100:     freq_sel <= 6'h12;  // 30 Hz
                3'b101:     freq_sel <= 6'h14;  // 50 Hz
                3'b110:     freq_sel <= 6'h16;  // 59.94 Hz
                default:    freq_sel <= 6'h15;  // 60 Hz
            endcase
        default:    freq_sel <= FREQ_SEL_ERR;
    endcase

always @ (posedge clk)
    prev_freq_sel <= freq_sel;

assign hsync_freq_change = (prev_freq_sel != freq_sel) || (prev_mode != mode);

always @ (posedge clk)
    sync_invalid_reg <= (freq_sel == FREQ_SEL_ERR || mode == MODE_ERROR);

assign sync_invalid = sync_invalid_reg;

assign sync_OK = ~sync_invalid_reg & ~sync_missing_reg;

always @ (posedge clk)
    if (set_default_freq)
    begin
        in_freq_sel    <= default_code;
        Si5324_bw_sel  <= default_bw_sel;
        Si5324_clk_sel <= 1'b0;
    end
    else if (set_hsync_freq)
    begin
        in_freq_sel    <= freq_sel[4:0];
        Si5324_bw_sel  <= GENLOCK_SI5324_BW_SEL;
        Si5324_clk_sel <= 1'b1;
    end

//------------------------------------------------------------------------------
// Input signal synchronization
//
// Synchronize the LOL of signals from the Si5324 to the local clock.
// Synchronize the genlock_enable signal to the local clock and create a
// signal indicating when it changes values. This is an input to the FSM. 
// Under control of the FSM, load the new genlock mode into the genlock_reg.
//
always @ (posedge clk)
    Si5324_LOL_0_sync <= {Si5324_LOL_0_sync[0], Si5324_LOL_0};

always @ (posedge clk)
    Si5324_LOL_12_sync <= {Si5324_LOL_12_sync[0], Si5324_LOL_1 | Si5324_LOL_2};

always @ (posedge clk)
    genlock_enable_sync <= {genlock_enable_sync[0], genlock_enable};

always @ (posedge clk)
    if (load_genlock)
        genlock_enable_change <= 1'b0;
    else
        genlock_enable_change <= genlock_enable_sync[1] ^ genlock_reg;

always @ (posedge clk)
    if (load_genlock)
        genlock_reg <= genlock_enable_sync[1];
    else if (clr_lock)
        genlock_reg <= 1'b0;

//------------------------------------------------------------------------------
// Lock timer
//
// Controls the various timing delays implemented by the FSM. The timer is
// cleared under FSM control and, when enabled by the FSM, directly by LOL
// signals from the Si5324 devices. This allows the timer to implement a delay
// that extends for X amount of time after the LOL signal is negated (where X
// is the terminal count of the timer).
//
always @ (posedge clk)
    if (clr_lock_counter | (en_auto_clr_main & Si5324_LOL_0_sync[1]) | (en_auto_clr_second & Si5324_LOL_12_sync[1]))
        lock_counter <= 0;
    else
        lock_counter <= lock_counter + 1;

assign lock_counter_tc = lock_counter[LOCK_CNT_MSB];
assign short_delay_tc = lock_counter[SHORT_DELAY_BIT];

//------------------------------------------------------------------------------
// Finite State Machine
//

//
// Current state register
// 
always @ (posedge clk)
    current_state <= next_state;

//
// Next state logic
//
always @ (*)
begin
    case(current_state)
        STATE_START:
            next_state <= STATE_INIT1;

        STATE_INIT1:
            if (lock_counter_tc)
                next_state <= STATE_INIT2;
            else
                next_state <= STATE_INIT1;

        STATE_INIT2:
            next_state <= STATE_INIT3;

        STATE_INIT3:
            if (lock_counter_tc)
                next_state <= STATE_LOCAL_LOCK;
            else
                next_state <= STATE_INIT3;

        STATE_LOCAL_LOCK:
            if (Si5324_LOL_0_sync[1])
                next_state <= STATE_START;
            else if (genlock_enable_change & (~genlock_enable_sync[1] | (genlock_enable_sync[1] & sync_OK)))
                next_state <= STATE_CHANGE1;
            else
                next_state <= STATE_LOCAL_LOCK;
            
        STATE_CHANGE1:
            next_state <= STATE_CHANGE2;

        STATE_CHANGE2:
            if (short_delay_tc)
            begin
                if (~genlock_reg)
                    next_state <= STATE_CHANGE3L;
                else
                    next_state <= STATE_CHANGE3G;
            end
            else
                next_state <= STATE_CHANGE2;

        STATE_CHANGE3L:
            next_state <= STATE_CHANGE4;

        STATE_CHANGE3G:
            next_state <= STATE_CHANGE4;

        STATE_CHANGE4:
            if (~Si5324_LOL_0_sync[1])
                next_state <= STATE_CHANGE5;
            else
                next_state <= STATE_CHANGE4;

        STATE_CHANGE5:
            if (lock_counter_tc)
                next_state <= STATE_CHANGE6;
            else
                next_state <= STATE_CHANGE5;

        STATE_CHANGE6:
            next_state <= STATE_CHANGE7;

        STATE_CHANGE7:
            if (lock_counter_tc)
            begin
                if (genlock_reg)
                    next_state <= STATE_GENLOCK;
                else
                    next_state <= STATE_LOCAL_LOCK;
            end
            else
                next_state <= STATE_CHANGE7;

        STATE_GENLOCK:
            if (genlock_enable_change)
                next_state <= STATE_CHANGE1;
            else if (hsync_freq_change)
                next_state <= STATE_CHANGEH;
            else if (Si5324_LOL_0_sync[1])
                next_state <= STATE_START;
            else
                next_state <= STATE_GENLOCK;
                
        STATE_CHANGEH:
            if (lock_counter_tc)
            begin
                if (sync_OK)
                    next_state <= STATE_CHANGE3G;
                else
                    next_state <= STATE_START;
            end
            else
                next_state <= STATE_CHANGEH;

        default:
            next_state <= STATE_START;
    endcase
end

//
// Output logic
//
always @ (*)
begin
    clr_lock_counter = 1'b0;
    en_auto_clr_main = 1'b0;
    en_auto_clr_second = 1'b0;
    set_dhold = 1'b0;
    clr_dhold = 1'b0;
    load_genlock = 1'b0;
    set_default_freq = 1'b0;
    set_hsync_freq = 1'b0;
    clr_lock = 1'b0;
    set_genlock = 1'b0;
    set_local_lock = 1'b0;
    set_Si5324_locked = 1'b0;

    case(current_state)
        STATE_START:
            begin
                clr_lock = 1'b1;
                clr_lock_counter = 1'b1;
                set_default_freq = 1'b1;
                clr_dhold = 1'b1;
            end

        STATE_INIT1:
            en_auto_clr_main = 1'b1;

        STATE_INIT2:
            begin
                clr_lock_counter = 1'b1;
                set_Si5324_locked = 1'b1;
            end

        STATE_INIT3:
            en_auto_clr_second = 1'b1;

        STATE_LOCAL_LOCK:
            set_local_lock = 1'b1;

        STATE_CHANGE1:
            begin
                set_dhold = 1'b1;
                clr_lock_counter = 1'b1;
                clr_lock = 1'b1;
                load_genlock = 1'b1;
            end

        STATE_CHANGE3L:
            begin
                set_default_freq = 1'b1;
                clr_lock_counter = 1'b1;
            end

        STATE_CHANGE3G:
            begin
                set_hsync_freq = 1'b1;
                clr_lock_counter = 1'b1;
            end

        STATE_CHANGE4:
            clr_lock_counter = 1'b1;

        STATE_CHANGE5:
            en_auto_clr_main = 1'b1;

        STATE_CHANGE6:
            begin
                clr_lock_counter = 1'b1;
                clr_dhold = 1'b1;
            end

        STATE_CHANGE7:
            en_auto_clr_second = 1'b1;

        STATE_GENLOCK:
            begin
                set_genlock = 1'b1;
                clr_lock_counter = 1'b1;
            end

        STATE_CHANGEH:
            set_dhold = 1'b1;

    endcase
end

//------------------------------------------------------------------------------
// DHOLD bit -- puts secondary Si5324 devices into digital holdover mode during
// genlock mode change. This greatly reduces the lock time of these devices.
//
always @ (posedge clk)
    if (set_dhold)
        Si5324_DHOLD <= 1'b1;
    else if (clr_dhold)
        Si5324_DHOLD <= 1'b0;

//------------------------------------------------------------------------------
// Lock indicators
//
// This code implements the genlock, local_lock, and Si5324_locked output signals.
// This signals are all controlled by the FSM.
//
always @ (posedge clk)
    if (clr_lock)
        genlock <= 1'b0;
    else if (set_genlock)
        genlock <= 1'b1;

always @ (posedge clk)
    if (clr_lock)
        local_lock <= 1'b0;
    else if (set_local_lock)
        local_lock <= 1'b1;

always @ (posedge clk)
    if (set_Si5324_locked)
        Si5324_locked <= 1'b1;

endmodule