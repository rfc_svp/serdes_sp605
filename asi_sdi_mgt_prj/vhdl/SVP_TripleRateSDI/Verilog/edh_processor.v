//------------------------------------------------------------------------------ 
// Copyright (c) 2004 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: edh_processor.v,rcs $
//  /   /        Date Last Modified:  $Date: 2004-12-15 11:21:47-07 $
// /___/   /\    Date Created: 2002
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: edh_processor.v,rcs $
// Revision 1.1  2004-12-15 11:21:47-07  jsnow
// Header update.
//
//------------------------------------------------------------------------------ 
//
//     XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"
//     SOLELY FOR USE IN DEVELOPING PROGRAMS AND SOLUTIONS FOR
//     XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION
//     AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE, APPLICATION
//     OR STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS
//     IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,
//     AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE
//     FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY
//     WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE
//     IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR
//     REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF
//     INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//     FOR A PARTICULAR PURPOSE.
//
//------------------------------------------------------------------------------ 
/* 
This module instances and interconnects the various modules that make up the
error detection and handling (EDH) packet processor. This processor includes
an ANC packet checksum checker, but does not include any ANC packet mux or
demux functions.

EDH packets for digital component video are defined by the standards 
ITU-R BT.1304 and SMPTE RP 165-1994. The documents define a standard method
of generating and inserting checkwords into the video stream. These checkwords
are not used for error correction. They are used to determine if the video
data is being corrupted somewhere in the chain of video equipment processing
the data. The nature of the EDH packets allows the malfunctioning piece of
equipment to be quickly located.

Two checkwords are defined, one for the field of active picture (AP) video data
words and the other for the full field (FF) of video data. Three sets of flags
are defined to feed forward information regarding detected errors. One of flags
is associated with the AP checkword, one set with the FF checkword. The third
set of flags identify errors detected in the ancillary data checksums within
the field. Implementation of this third set is optional in the standards.

The two checkwords and three sets of flags for each field are combined into an
ancillary data packet, commonly called the EDH packet. The EDH packet occurs
at a fixed location, always immediately before the SAV symbol on the line before
the synchronous switching line. The synchronous switching lines for NTSC are
lines 10 and 273. For 625-line PAL they are lines 6 and 319.

Three sets of error flags outputs are provided. One set consists of the 12
error flags received in the last EDH packet in the input video stream. The
second set consists of the twelve flags sent in the last EDH packet in the
output video stream. A third set contains error flags related to the processing
of the received EDH packet such as packet_missing errors.

*/

`timescale 1ns / 1 ns

module  edh_processor (
    // inputs
    clk,            // clock input
    ce,             // clock enable
    rst,            // async reset input
    vid_in,         // input video
    reacquire,      // forces autodetect to reacquire the video standard
    en_sync_switch, // enables synchronous switching
    en_trs_blank,   // enables TRS blanking when asserted
    anc_idh_local,  // ANC IDH flag input
    anc_ues_local,  // ANC UES flag input
    ap_idh_local,   // AP IDH flag input
    ff_idh_local,   // FF IDH flag input
    errcnt_flg_en,  // selects which error flags increment the error counter
    clr_errcnt,     // clears the error counter
    receive_mode,   // 1 enables receiver, 0 for generate only
        
    //outputs
    vid_out,        // output video stream with EDH packets inserted
    std,            // video standard code
    std_locked,     // video standard detector is locked
    trs,            // asserted during flywheel generated TRS symbol
    field,          // field indicator
    v_blank,        // vertical blanking indicator
    h_blank,        // horizontal blanking indicator
    horz_count,     // horizontal position
    vert_count,     // vertical position
    sync_switch,    // asserted on lines where synchronous switching is allowed
    locked,         // asserted when flywheel is synchronized to video
    eav_next,       // next word is first word of EAV
    sav_next,       // next word is first word of SAV
    xyz_word,       // current word is the XYZ word of a TRS
    anc_next,       // next word is first word of a received ANC packet
    edh_next,       // next word is first word of a received EDH packet
    rx_ap_flags,    // received AP error flags from last EDH packet
    rx_ff_flags,    // received FF error flags from last EDH packet
    rx_anc_flags,   // received ANC error flags from last EDH packet
    ap_flags,       // AP error flags from last field
    ff_flags,       // FF error flags from last field
    anc_flags,      // ANC error flags from last field
    packet_flags,   // error flags related to the received packet processing
    errcnt,         // errored fields counter
    edh_packet      // asserted during all words of a generated EDH packet
);

//-----------------------------------------------------------------------------
// Parameter definitions
//

//
// This group of parameters defines the bit widths of various fields in the
// module. 
//
parameter HCNT_WIDTH    = 12;                   // Width of hcnt
parameter VCNT_WIDTH    = 10;                   // Width of vcnt
parameter ERRFLD_WIDTH  = 24;                   // Width of errored fields count
parameter FLAGS_WIDTH   = 16;                   // Width of error flag enable field
 
parameter HCNT_MSB      = HCNT_WIDTH - 1;       // MS bit # of hcnt
parameter VCNT_MSB      = VCNT_WIDTH - 1;       // MS bit # of vcnt
parameter ERRFLD_MSB    = ERRFLD_WIDTH - 1;     // MS bit of errcnt
parameter FLAGS_MSB     = FLAGS_WIDTH - 1;      // MS bit of flag enable field

//
// This group of parameters defines the encoding for the video standards output
// code.
//
parameter [2:0]
    NTSC_422        = 3'b000,
    NTSC_INVALID    = 3'b001,
    NTSC_422_WIDE   = 3'b010,
    NTSC_4444       = 3'b011,
    PAL_422         = 3'b100,
    PAL_INVALID     = 3'b101,
    PAL_422_WIDE    = 3'b110,
    PAL_4444        = 3'b111;

//-----------------------------------------------------------------------------
// Port definitions
//
input                   clk;
input                   ce;
input                   rst;
input   [9:0]           vid_in;
input                   reacquire;
input                   en_sync_switch;
input                   en_trs_blank;
input                   anc_idh_local;
input                   anc_ues_local;
input                   ap_idh_local;
input                   ff_idh_local;
input   [FLAGS_MSB:0]   errcnt_flg_en;
input                   clr_errcnt;
input                   receive_mode;
output  [9:0]           vid_out;
output  [2:0]           std;
output                  std_locked;
output                  trs;
output                  field;
output                  v_blank;
output                  h_blank;
output  [HCNT_MSB:0]    horz_count;
output  [VCNT_MSB:0]    vert_count;
output                  sync_switch;
output                  locked;
output                  eav_next;
output                  sav_next;
output                  xyz_word;
output                  anc_next;
output                  edh_next;
output  [4:0]           rx_ap_flags;
output  [4:0]           rx_ff_flags;
output  [4:0]           rx_anc_flags;
output  [4:0]           ap_flags;
output  [4:0]           ff_flags;
output  [4:0]           anc_flags;
output  [3:0]           packet_flags;
output  [ERRFLD_MSB:0]  errcnt;
output                  edh_packet;

reg     [9:0]           vid_out;
reg     [2:0]           std;
reg                     std_locked;
reg                     trs;
reg                     field;
reg                     v_blank;
reg                     h_blank;
reg     [HCNT_MSB:0]    horz_count;
reg     [VCNT_MSB:0]    vert_count;
reg                     sync_switch;
reg                     locked;
reg                     eav_next;
reg                     sav_next;
reg                     xyz_word;
reg                     anc_next;
reg                     edh_next;
reg                     edh_packet;

//-----------------------------------------------------------------------------
// Signal definitions
//
wire    [2:0]           dec_std;        // video_decode std output
wire                    dec_std_locked; // video_decode std locked output
wire    [9:0]           dec_vid;        // video_decode video output
wire                    dec_trs;        // video_decode trs output
wire                    dec_f;          // video_decode field output
wire                    dec_v;          // video_decode v_blank output
wire                    dec_h;          // video_decode h_blank output
wire    [HCNT_MSB:0]    dec_hcnt;       // video_decode horz_count output
wire    [VCNT_MSB:0]    dec_vcnt;       // video_decode vert_count output
wire                    dec_sync_switch;// video_decode sync_switch output
wire                    dec_locked;     // video_decode locked output
wire                    dec_eav_next;   // video_decode eav_next output
wire                    dec_sav_next;   // video_decode sav_next output
wire                    dec_xyz_word;   // video_decode xyz_word output
wire                    dec_anc_next;   // video_decode anc_next output
wire                    dec_edh_next;   // video_decode edh_next output
wire    [15:0]          ap_crc;         // calculated active pic CRC
wire                    ap_crc_valid;   // calculated active pic CRC valid signal
wire    [15:0]          ff_crc;         // calculated full field CRC
wire                    ff_crc_valid;   // calculated full field CRC valid signal
wire                    edh_missing;    // EDH packet missing error flag
wire                    edh_parity_err; // EDH packet parity error flag
wire                    edh_chksum_err; // EDH packet checksum error flag
wire                    edh_format_err; // EDH packet format error flag
wire                    tx_edh_next;    // generated EDH packet begins on next word
wire    [4:0]           flag_bus;       // flag bus between EDH_FLAGS and EDH_TX
wire                    ap_flag_word;   // selects AP flags for flag bus
wire                    ff_flag_word;   // selects FF flags for flag bus
wire                    anc_flag_word;  // selects ANC flags for flag bus
wire                    rx_ap_crc_valid;// received active pic CRC valid signal
wire    [15:0]          rx_ap_crc;      // received active pic CRC
wire                    rx_ff_crc_valid;// received full field CRC valid signal
wire    [15:0]          rx_ff_crc;      // received full field CRC
wire    [4:0]           in_ap_flags;    // received active pic flags to edh_flags
wire    [4:0]           in_ff_flags;    // received full field flags to edh_flags
wire    [4:0]           in_anc_flags;   // received ANC flags to edh_flags
reg                     errcnt_en;      // enables error counter
wire                    anc_edh_local;  // ANC EDH signal
wire    [9:0]           tx_vid_out;     // video out of edh_tx
wire                    tx_edh_packet;  // asserted when edh packet is to be generated


//
// Video decoder module from XAPP625
//
defparam DEC.VCNT_WIDTH     = VCNT_WIDTH;
defparam DEC.HCNT_WIDTH     = HCNT_WIDTH;
defparam DEC.NTSC_422       = NTSC_422;
defparam DEC.NTSC_INVALID   = NTSC_INVALID;
defparam DEC.NTSC_422_WIDE  = NTSC_422_WIDE;
defparam DEC.NTSC_4444      = NTSC_4444;
defparam DEC.PAL_422        = PAL_422;
defparam DEC.PAL_INVALID    = PAL_INVALID;
defparam DEC.PAL_422_WIDE   = PAL_422_WIDE;
defparam DEC.PAL_4444       = PAL_4444;

video_decode DEC (
    .clk            (clk),
    .ce             (ce),
    .rst            (rst),
    .vid_in         (vid_in),
    .reacquire      (reacquire),
    .en_sync_switch (en_sync_switch),
    .en_trs_blank   (en_trs_blank),
    .std            (dec_std),
    .std_locked     (dec_std_locked),
    .trs            (dec_trs),
    .vid_out        (dec_vid),
    .field          (dec_f),
    .v_blank        (dec_v),
    .h_blank        (dec_h),
    .horz_count     (dec_hcnt),
    .vert_count     (dec_vcnt),
    .sync_switch    (dec_sync_switch),
    .locked         (dec_locked),
    .eav_next       (dec_eav_next),
    .sav_next       (dec_sav_next),
    .xyz_word       (dec_xyz_word),
    .anc_next       (dec_anc_next),
    .edh_next       (dec_edh_next)
);

//
// edh_crc module
//
// This module computes the CRC values for the incoming video stream, vid_in.
// Also, the module generates valid signals for both CRC values based on the
// locked signal. If locked rises during a field, the CRC is considered to be
// invalid.
defparam EDH_CRC.VCNT_WIDTH     = VCNT_WIDTH;
defparam EDH_CRC.NTSC_422       = NTSC_422;
defparam EDH_CRC.NTSC_INVALID   = NTSC_INVALID;
defparam EDH_CRC.NTSC_422_WIDE  = NTSC_422_WIDE;
defparam EDH_CRC.NTSC_4444      = NTSC_4444;
defparam EDH_CRC.PAL_422        = PAL_422;
defparam EDH_CRC.PAL_INVALID    = PAL_INVALID;
defparam EDH_CRC.PAL_422_WIDE   = PAL_422_WIDE;
defparam EDH_CRC.PAL_4444       = PAL_4444;

edh_crc EDH_CRC (
    .clk            (clk),
    .ce             (ce),
    .rst            (rst),
    .f              (dec_f),
    .h              (dec_h),
    .eav_next       (dec_eav_next),
    .xyz_word       (dec_xyz_word),
    .vid_in         (dec_vid),
    .vcnt           (dec_vcnt),
    .std            (dec_std),
    .locked         (dec_locked),
    .ap_crc         (ap_crc),
    .ap_crc_valid   (ap_crc_valid),
    .ff_crc         (ff_crc),
    .ff_crc_valid   (ff_crc_valid)
);

//
// edh_rx module
//
// This module processes EDH packets found in the incoming video stream. The
// CRC words and valid flags are captured from the packet. Various error flags
// related to errors found in the packet are generated.
//
edh_rx EDH_RX (
    .clk            (clk),
    .ce             (ce),
    .rst            (rst),
    .rx_edh_next    (dec_edh_next),
    .vid_in         (dec_vid),
    .edh_next       (tx_edh_next),
    .reg_flags      (1'b0),
    .ap_crc_valid   (rx_ap_crc_valid),
    .ap_crc         (rx_ap_crc),
    .ff_crc_valid   (rx_ff_crc_valid),
    .ff_crc         (rx_ff_crc),
    .edh_missing    (edh_missing),
    .edh_parity_err (edh_parity_err),
    .edh_chksum_err (edh_chksum_err),
    .edh_format_err (edh_format_err),
    .in_ap_flags    (in_ap_flags),
    .in_ff_flags    (in_ff_flags),
    .in_anc_flags   (in_anc_flags),
    .rx_ap_flags    (rx_ap_flags),
    .rx_ff_flags    (rx_ff_flags),
    .rx_anc_flags   (rx_anc_flags)
);

//
// edh_loc module
//
// This module locates the beginning of an EDH packet in the incoming video
// stream. It asserts the tx_edh_next siganl the sample before the EDH packet
// begins on vid_in.
//
defparam EDH_LOC.HCNT_WIDTH     = HCNT_WIDTH;
defparam EDH_LOC.VCNT_WIDTH     = VCNT_WIDTH;
defparam EDH_LOC.NTSC_422       = NTSC_422;
defparam EDH_LOC.NTSC_INVALID   = NTSC_INVALID;
defparam EDH_LOC.NTSC_422_WIDE  = NTSC_422_WIDE;
defparam EDH_LOC.NTSC_4444      = NTSC_4444;
defparam EDH_LOC.PAL_422        = PAL_422;
defparam EDH_LOC.PAL_INVALID    = PAL_INVALID;
defparam EDH_LOC.PAL_422_WIDE   = PAL_422_WIDE;
defparam EDH_LOC.PAL_4444       = PAL_4444;

edh_loc EDH_LOC (
    .clk            (clk),
    .ce             (ce),
    .rst            (rst),
    .f              (dec_f),
    .vcnt           (dec_vcnt),
    .hcnt           (dec_hcnt),
    .std            (dec_std),
    .edh_next       (tx_edh_next)
);

//
// anc_rx module
//
// This module calculates checksums for every ANC packet in the input video
// stream and compares the calculated checksums against the CS word of each
// packet. It also checks the parity bits of all parity protected words in the
// ANC packets. An error in any ANC packet will assert the anc_edh_local signal.
// This output will remain asserted until after the next EDH packet is sent in
// the output video stream.
//
anc_rx ANC_RC (
    .clk            (clk),
    .ce             (ce),
    .rst            (rst),
    .locked         (dec_locked),
    .rx_anc_next    (dec_anc_next),
    .rx_edh_next    (dec_edh_next),
    .edh_packet     (tx_edh_packet),
    .vid_in         (dec_vid),
    .anc_edh_local  (anc_edh_local)
);

//
// edh_tx module
//
// This module generates a new EDH packet based on the calculated CRC words
// and the incoming and local flags.
//
edh_tx EDH_TX (
    .clk            (clk),
    .ce             (ce),
    .rst            (rst),
    .vid_in         (dec_vid),
    .edh_next       (tx_edh_next),
    .edh_missing    (edh_missing),
    .ap_crc_valid   (ap_crc_valid),
    .ap_crc         (ap_crc),
    .ff_crc_valid   (ff_crc_valid),
    .ff_crc         (ff_crc),
    .flags_in       (flag_bus),
    .ap_flag_word   (ap_flag_word),
    .ff_flag_word   (ff_flag_word),
    .anc_flag_word  (anc_flag_word),
    .edh_packet     (tx_edh_packet),
    .edh_vid        (tx_vid_out)
);

//
// edh_flags module
//
// This module creates the error flags that are included in the new
// EDH packet created by the GEN module. It also captures those flags until the
// next EDH packet and provides them as outputs.
//
edh_flags EDH_FLAGS (
    .clk                (clk),
    .ce                 (ce),
    .rst                (rst),
    .receive_mode       (receive_mode),
    .ap_flag_word       (ap_flag_word),
    .ff_flag_word       (ff_flag_word),
    .anc_flag_word      (anc_flag_word),
    .edh_missing        (edh_missing),
    .edh_parity_err     (edh_parity_err),
    .edh_format_err     (edh_format_err),
    .rx_ap_crc_valid    (rx_ap_crc_valid),
    .rx_ap_crc          (rx_ap_crc),
    .rx_ff_crc_valid    (rx_ff_crc_valid),
    .rx_ff_crc          (rx_ff_crc),
    .rx_ap_flags        (in_ap_flags),
    .rx_ff_flags        (in_ff_flags),
    .rx_anc_flags       (in_anc_flags),
    .anc_edh_local      (anc_edh_local),
    .anc_idh_local      (anc_idh_local),
    .anc_ues_local      (anc_ues_local),
    .ap_idh_local       (ap_idh_local),
    .ff_idh_local       (ff_idh_local),
    .calc_ap_crc_valid  (ap_crc_valid),
    .calc_ap_crc        (ap_crc),
    .calc_ff_crc_valid  (ff_crc_valid),
    .calc_ff_crc        (ff_crc),
    .flags              (flag_bus),
    .ap_flags           (ap_flags),
    .ff_flags           (ff_flags),
    .anc_flags          (anc_flags)
);

//
// edh_errcnt module
//
// This counter increments once for every field that contains an enabled error.
// The error counter is disabled until after the video decoder is locked to the
// video stream for the first time and the first EDH packet has been received.
//
defparam EDH_ERRCNT.ERRFLD_WIDTH    = ERRFLD_WIDTH;
defparam EDH_ERRCNT.FLAGS_WIDTH     = FLAGS_WIDTH;

edh_errcnt EDH_ERRCNT (
    .clk                (clk),
    .ce                 (ce),
    .rst                (rst),
    .clr_errcnt         (clr_errcnt),
    .count_en           (errcnt_en),
    .flag_enables       (errcnt_flg_en),
    .flags              ({edh_chksum_err, ap_flags, ff_flags, anc_flags}),
    .edh_next           (tx_edh_next),
    .errcnt             (errcnt)
);

always @ (posedge clk or posedge rst)
    if (rst)
        errcnt_en <= 1'b0;
    else if (ce)
        if (locked & dec_edh_next)
            errcnt_en <= 1'b1;

//
// packet_flags
//
// This statement combines the four EDH packet flags into a vector.
//
assign packet_flags = {edh_format_err, edh_chksum_err, edh_parity_err, edh_missing};

//
// output registers
//
// This code implements an output register for the video path and all video
// timing signals.
//
always @ (posedge clk or posedge rst)
    if (rst)
        begin
            vid_out <= 0;
            std <= 0;
            std_locked <= 0;
            trs <= 0;
            field <= 0;
            v_blank <= 0;
            h_blank <= 0;
            horz_count <= 0;
            vert_count <= 0;
            sync_switch <= 0;
            locked <= 0;
            eav_next <= 0;
            sav_next <= 0;
            xyz_word <= 0;
            anc_next <= 0;
            edh_next <= 0;
            edh_packet <= 0;
        end
    else if (ce)
        begin
            vid_out <= tx_vid_out;
            std <= dec_std;
            std_locked <= dec_std_locked;
            trs <= dec_trs;
            field <= dec_f;
            v_blank <= dec_v;
            h_blank <= dec_h;
            horz_count <= dec_hcnt;
            vert_count <= dec_vcnt;
            sync_switch <= dec_sync_switch;
            locked <= dec_locked;
            eav_next <= dec_eav_next;
            sav_next <= dec_sav_next;
            xyz_word <= dec_xyz_word;
            anc_next <= dec_anc_next;
            edh_next <= dec_edh_next;
            edh_packet <= tx_edh_packet;
        end

endmodule