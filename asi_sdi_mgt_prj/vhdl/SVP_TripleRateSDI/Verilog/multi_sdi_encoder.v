//------------------------------------------------------------------------------ 
// Copyright (c) 2008 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Solutions Development Group, Xilinx, Inc.
//  \   \        Filename: $RCSfile: multi_sdi_encoder.v,rcs $
//  /   /        Date Last Modified:  $Date: 2008-11-07 11:34:00-07 $
// /___/   /\    
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// $Log: multi_sdi_encoder.v,rcs $
// Revision 1.2  2008-11-07 11:34:00-07  jsnow
// Added register initializers to eliminate unknowns in simulation.
//
// Revision 1.1  2004-12-15 11:41:05-07  jsnow
// Header update.
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module is the top-level module of the multi-rate HD/SD-SDI encoder. For 
HD-SDI, this module encodes 20 bits of data, 10 bits of chroma (C) and 10 bits 
of luma (Y), per clock cycle. For SD-SDI, 10 bits are encoded per clock cycle.

This module instantiates the smpte_encoder module twice, with one module 
encoding the C data and the other the Y data. The two modules are cross 
connected so that the results from one encoder affects the encoding of the bits
in the other encoder, as required by the HD-SDI encoding scheme. When encoding
SD-SDI, only the Y channel SMPTE encoder is used

The q output is a 20-bit encoded value. Note that this value must be bit-swapped
before it can be connected to the 20-bit input of the RocketIO transmitter. For
SD-SDI, only the LS 10-bits of the output are valid.

Note that this module does not make multiple copies of each encoded bit for
SD-SDI as required to run the RocketIO MGT in oversampled mode for the slow
SD-SDI bit rates. This bit replication must be done externally to this module.
--------------------------------------------------------------------------------
*/

module multi_sdi_encoder (
    clk,                // input clock
    rst,                // reset signal
    ce,                 // input register load signal
    hd_sd,              // 0 = HD, 1 = SD
    nrzi,               // enables NRZ-to-NRZI conversion when high
    scram,              // enables SDI scrambler when high
    c,                  // C channel input data (not used for SD-SDI)
    y,                  // Y channel input data 
    q                   // encoded output data
);

// IO definitions   
input               clk;
input               rst;
input               ce;
input               hd_sd;
input               nrzi;
input               scram; 
input   [9:0]       c;
input   [9:0]       y;
output  [19:0]      q;

// Internal signals
reg     [9:0]       c_in_reg = 0;   // C channel input register
reg     [9:0]       y_in_reg = 0;   // Y channel input register
wire    [8:0]       c_i_scram;      // C channel intermediate scrambled data
wire    [8:0]       y_i_scram_q;    // Y channel intermediate scrambled data
wire                c_i_nrzi;       // C channel intermediate nrzi data
wire    [9:0]       c_out;          // output of C scrambler
wire    [9:0]       y_out;          // output of Y scrambler
wire    [8:0]       y_p_scram_mux;  // p_scram input MUX for Y encoder
wire                y_p_nrzi_mux;   // p_nrzi input MUX for Y encoder

//
// Scrambler modules for both C and Y channels
//
smpte_encoder C_scram (
    .clk        (clk),
    .rst        (rst),
    .ce         (~hd_sd & ce),
    .nrzi       (nrzi),
    .scram      (scram),
    .d          (c_in_reg),
    .p_scram    (y_i_scram_q),
    .p_nrzi     (y_out[9]),
    .q          (c_out),
    .i_scram    (c_i_scram),
    .i_scram_q  (),
    .i_nrzi     (c_i_nrzi)
);

smpte_encoder Y_scram (
    .clk        (clk),
    .rst        (rst),
    .ce         (ce),
    .nrzi       (nrzi),
    .scram      (scram),
    .d          (y_in_reg),
    .p_scram    (y_p_scram_mux),
    .p_nrzi     (y_p_nrzi_mux),
    .q          (y_out),
    .i_scram    (),
    .i_scram_q  (y_i_scram_q),
    .i_nrzi     ()
);

//
// These MUXes control whether the two smpte_scrambler modules are configured
// for HD-SDI or SD-SDI. In HD-SDI, the C and Y channel scramblers are
// cross connected to encode a 20-bit word every clock cycle. In SD-SDI mode,
// only the Y channel scrambler is used and it's output is feedback to its
// inputs to allow the sequential scrambling of the data 10-bits at a time.
//
assign y_p_scram_mux = hd_sd ? y_i_scram_q : c_i_scram;
assign y_p_nrzi_mux = hd_sd ? y_out[9] : c_i_nrzi;


//
// Input registers
//
always @ (posedge clk or posedge rst)
    if (rst)
        y_in_reg <= 0;
    else if (ce)
        y_in_reg <= y;

always @ (posedge clk or posedge rst)
    if (rst)
        c_in_reg <= 0;
    else if (ce & ~hd_sd)
        c_in_reg <= c;

//
// Output assignment
//
assign q = {y_out, c_out};

endmodule
