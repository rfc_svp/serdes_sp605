//------------------------------------------------------------------------------ 
// Copyright (c) 2009 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow
//  \   \        Filename: $RCSfile: sdi_bitrep_20b.v,rcs $
//  /   /        Date Last Modified:  $Date: 2009-03-10 15:45:13-06 $
// /___/   /\    Date Created: January 5, 2009
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: sdi_bitrep_20b.v,rcs $
// Revision 1.0  2009-03-10 15:45:13-06  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

Description of module:

This module performs the bit replication of the incoming data, 11 times and 
sends out 20 bits on every clock cycle. This module requires an alternating
cadence of 5/6/5/6 on the clock enable (ce) input. The state machine 
automatically aligns itself regardless of whether the first step of the 
cadence is 5 or 6 when it starts up. If the 5/6/5/6 cadence gets out of step,
the state machine will realign itself and will also assert the align_err
output for one clock cycle.

--------------------------------------------------------------------------------
*/

`timescale 1ns / 1ns

module sdi_bitrep_20b (
    input  wire             clk,                // clock input
    input  wire             rst,                // async reset 
    input  wire             ce,                 // clock enable 
    input  wire [9:0]       d,                  // input data
    output reg  [19:0]      q = 0,              // output data
    output reg              align_err = 1'b0);  // ce alignment error



//-------------------------------------------------------------------
// Parameter definitions
//

parameter STATE_WIDTH = 4;
parameter STATE_MSB   = STATE_WIDTH - 1;

parameter [STATE_MSB:0] 
    START = 4'b1111,
    S0    = 4'b0000,
    S1    = 4'b0001,
    S2    = 4'b0010,
    S3    = 4'b0011,
    S4    = 4'b0100,
    S5    = 4'b0101,
    S6    = 4'b0110,
    S7    = 4'b0111,
    S8    = 4'b1000,
    S9    = 4'b1001,
    S10   = 4'b1010,
    S5X   = 4'b1011;
  
//--------------------------------------------------------------------
// Signal definitions
//

(* fsm_encoding = "USER" *)
reg  [STATE_MSB:0]  current_state = START;
(* fsm_encoding = "USER" *)
reg  [STATE_MSB:0]  next_state;
reg  [9:0]          in_reg = 0;
reg  [9:0]          d_reg = 0;
reg                 b9_save = 1'b0;
reg                 ce_dly = 1'b0;
wire [19:0]         q_int;

//
// Input registers
//
always @ (posedge clk)
    if (ce)
        in_reg <= d;
        
always @ (posedge clk)
    ce_dly <= ce;
    
always @ (posedge clk)
    if (ce_dly)
        d_reg <= in_reg;                

always @ (posedge clk)
    if (ce_dly)
        b9_save <= d_reg[9];

//
// FSM: current_state register
//
// This code implements the current state register. It loads with the S0
// state on reset and the next_state value with each rising clock edge.
//
always @ (posedge clk or posedge rst)
    if (rst)
        current_state <= START;
    else 
        current_state <= next_state;
        

// FSM: next_state logic
//
// This case statement generates the next_state value for the FSM based on
// the current_state and the various FSM inputs.
//        
always@ *
    case(current_state)
        START:  if (ce_dly)
                    next_state = S0;
                else
                    next_state = START;
        
        S0:     next_state = S1;
        
        S1:     next_state = S2;
        
        S2:     next_state = S3;
        
        S3:     next_state = S4;
        
        S4:     if (ce_dly) 
                    next_state = S5;
                else
                    next_state = S5X;
        
        S5:     next_state = S6;            // Two different state 5's depending
                                            // on when the occurred
        S5X:    next_state = S6;
        
        S6:     next_state = S7;
        
        S7:     next_state = S8;
        
        S8:     next_state = S9;
        
        S9:     next_state = S10;
        
        S10:    if (ce_dly) 
                    next_state = S0; 
                else 
                    next_state = START;
        
        default: next_state = START; 
    endcase 

//
// Output mux
//
// Use the current state encoding to select the output bits.
//

mux12_wide #(
    .WIDTH  (20)) 
OUTMUX (
    .d0     ({ {9{d_reg[1]}}, {11{d_reg[0]}}}),                 // state S0
    .d1     ({ {7{d_reg[3]}}, {11{d_reg[2]}}, {2{d_reg[1]}}}),  // state S1
    .d2     ({ {5{d_reg[5]}}, {11{d_reg[4]}}, {4{d_reg[3]}}}),  // state S2
    .d3     ({ {3{d_reg[7]}}, {11{d_reg[6]}}, {6{d_reg[5]}}}),  // state S3
    .d4     ({    d_reg[9],   {11{d_reg[8]}}, {8{d_reg[7]}}}),  // state S4
    .d5     ({{10{d_reg[0]}}, {10{b9_save}}}),                  // state S5
    .d6     ({ {8{d_reg[2]}}, {11{d_reg[1]}},    d_reg[0]}),    // state S6
    .d7     ({ {6{d_reg[4]}}, {11{d_reg[3]}}, {3{d_reg[2]}}}),  // state S7
    .d8     ({ {4{d_reg[6]}}, {11{d_reg[5]}}, {5{d_reg[4]}}}),  // state S8
    .d9     ({ {2{d_reg[8]}}, {11{d_reg[7]}}, {7{d_reg[6]}}}),  // state S9
    .d10    ({{11{d_reg[9]}}, { 9{d_reg[8]}}}),                 // state S10
    .d11    ({{10{in_reg[0]}},{10{d_reg[9]}}}),                 // state S5X
    .sel    (current_state),
    .y      (q_int));

always @ (posedge clk)
    q <= q_int;
        
always @ (posedge clk)
    align_err <= ((current_state == S10) || (current_state == S5X)) & ~ce_dly;

endmodule
