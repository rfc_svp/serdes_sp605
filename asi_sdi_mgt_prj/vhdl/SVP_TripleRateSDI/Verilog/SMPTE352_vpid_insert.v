//------------------------------------------------------------------------------ 
// Copyright (c) 2007 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Advanced Product Division, Xilinx, Inc.
//  \   \        Filename: $RCSfile: SMPTE352_vpid_insert.v,rcs $
//  /   /        Date Last Modified:  $Date: 2008-11-14 13:21:15-07 $
// /___/   /\    Date Created: April 27, 2007
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: SMPTE352_vpid_insert.v,rcs $
// Revision 1.2  2008-11-14 13:21:15-07  jsnow
// Added register initializer to in_reg.
//
// Revision 1.1  2007-10-23 15:41:39-06  jsnow
// Changed level B VPID insertion to match SMPTE 425-2007 changes.
//
// Revision 1.0  2007-08-08 13:40:41-06  jsnow
// Initial release.
//
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs. Xilinx and its licensors make 
// and you receive no warranties or conditions, express, implied, statutory or 
// otherwise, and Xilinx specifically disclaims any implied warranties of 
// merchantability, non-infringement, or fitness for a particular purpose. 
// Xilinx does not warrant that the functions contained in these designs will 
// meet your requirements, or that the operation of these designs will be 
// uninterrupted or error free, or that defects in the Designs will be 
// corrected. Furthermore, Xilinx does not warrant or make any representations 
// regarding use or the results of the use of the designs in terms of 
// correctness, accuracy, reliability, or otherwise. The designs are not 
// covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module inserts SMPTE 352M video payload ID packets into a video stream.
The stream may be either HD or SD, as indicated by the hd_sd input signal.
The module will overwrite an existing VPID packet if the overwrite input
is asserted, otherwise if a VPID packet exists in the HANC space, it will
not be overwritten and a new packet will not be inserted.

The module does not create the user data words of the VPID packet. Those
are generated externally and enter the module on the byte1, byte2, byte3,
and byte4 ports.

The module requires an interface line number on its input. This line number
must be valid for the new line one clock cycle before the start of the
HANC space -- that is during the second CRC word following the EAV.

If the overwrite input is 1, this module will also deleted any VPID packets
that occur elsewhere in any HANC space. These packets will be marked as
deleted packets.

When the level_b input is 1, then the module works a little bit differently.
It will always overwrite the first data word of the VPID packet with the value
present on the byte1 input port, even if overwrite is 0. This is because
conversions from dual link to level B 3G-SDI require the first byte to be
modified. The checksum is recalculated and inserted.

This module is compliant with the 2007 revision of SMPTE 425M for inserting
SMPTE 352M VPID packets in level B streams.
*/

`timescale 1ns / 1 ns

module  SMPTE352_vpid_insert (
    input  wire             clk,            // clock input
    input  wire             ce,             // clock enable
    input  wire             rst,            // async reset input
    input  wire             hd_sd,          // 0 = HD, 1 = SD
    input  wire             level_b,        // 1 = SMPTE 425M Level B
    input  wire             enable,         // 0 = disable insertion
    input  wire             overwrite,      // 1 = overwrite existing packets
    input  wire [10:0]      line,           // current video line
    input  wire [10:0]      line_a,         // field 1 line for packet insertion
    input  wire [10:0]      line_b,         // field 2 line for packet insertion
    input  wire             line_b_en,      // 1 = use line_b, 0 = ignore line_b
    input  wire [7:0]       byte1,          // first byte of VPID data
    input  wire [7:0]       byte2,          // second byte of VPID data
    input  wire [7:0]       byte3,          // third byte of VPID data
    input  wire [7:0]       byte4,          // fourth byte of VPID data
    input  wire [9:0]       y_in,           // Y data stream in
    input  wire [9:0]       c_in,           // C data stream in
    output reg  [9:0]       y_out = 0,      // Y data stream out
    output wire [9:0]       c_out,          // C data stream out
    output reg              eav_out = 0,    // asserted on XYZ word of EAV
    output reg              sav_out = 0     // asserted on XYZ word of SAV
);

localparam STATE_WIDTH   = 6;
localparam STATE_MSB     = STATE_WIDTH - 1;

localparam [STATE_WIDTH-1:0]
    STATE_WAIT      = 6'd0,
    STATE_ADF0      = 6'd1,
    STATE_ADF1      = 6'd2,
    STATE_ADF2      = 6'd3,
    STATE_DID       = 6'd4,
    STATE_SDID      = 6'd5,
    STATE_DC        = 6'd6,
    STATE_B0        = 6'd7,
    STATE_B1        = 6'd8,
    STATE_B2        = 6'd9,
    STATE_B3        = 6'd10,
    STATE_CS        = 6'd11,
    STATE_DID2      = 6'd12,
    STATE_SDID2     = 6'd13,
    STATE_DC2       = 6'd14,
    STATE_UDW       = 6'd15,
    STATE_CS2       = 6'd16,
    STATE_INS_ADF0  = 6'd17,
    STATE_INS_ADF1  = 6'd18,
    STATE_INS_ADF2  = 6'd19,
    STATE_INS_DID   = 6'd20,
    STATE_INS_SDID  = 6'd21,
    STATE_INS_DC    = 6'd22,
    STATE_INS_B0    = 6'd23,
    STATE_INS_B1    = 6'd24,
    STATE_INS_B2    = 6'd25,
    STATE_INS_B3    = 6'd26,
    STATE_ADF0_X    = 6'd27,
    STATE_ADF1_X    = 6'd28,
    STATE_ADF2_X    = 6'd29,
    STATE_DID_X     = 6'd30,
    STATE_SDID_X    = 6'd31,
    STATE_DC_X      = 6'd32,
    STATE_UDW_X     = 6'd33,
    STATE_CS_X      = 6'd34;
        
localparam [3:0]
    MUX_SEL_000     = 4'd0,
    MUX_SEL_3FF     = 4'd1,
    MUX_SEL_DID     = 4'd2,
    MUX_SEL_SDID    = 4'd3,
    MUX_SEL_DC      = 4'd4,
    MUX_SEL_UDW     = 4'd5,
    MUX_SEL_CS      = 4'd6,
    MUX_SEL_DEL     = 4'd7,
    MUX_SEL_VID     = 4'd8;

// internal signals
reg     [9:0]   vid_reg0 = 0;           // video pipeline register
reg     [9:0]   vid_reg1 = 0;           // video pipeline register
reg     [9:0]   vid_reg2 = 0;           // video pipeline register
reg     [9:0]   vid_dly = 0;            // last stage of video pipeline
wire            all_ones_in;            // asserted when in_reg is all ones
wire            all_zeros_in;           // asserted when in_reg is all zeros
reg     [2:0]   all_zeros_pipe = 0;     // delay pipe for all zeros
reg     [2:0]   all_ones_pipe = 0;      // delay pipe for all ones
wire            xyz;                    // current word is the XYZ word
wire            eav_next;               // 1 = next word is first word of EAV
wire            sav_next;               // 1 = next word is first word of SAV
wire            anc_next;               // 1 = next word is first word of ANC
wire            hanc_start_next;        // 1 = next word is first word of HANC
reg     [3:0]   hanc_dly;               // delay value from xyz to hanc_start_next
reg     [9:0]   in_reg = 0;             // input register
reg     [9:0]   vid_out = 0;            // internal version of y_out
wire            line_match_a;           // output of line_a comparitor
wire            line_match_b;           // output of line_b comparitor
reg             vpid_line = 0;          // 1 = insert VPID packet on this line
wire            vpid_pkt;               // 1 = ANC packet is a VPID
wire            del_pkt_ok;             // 1 = ANC act is deleted packet with
reg     [7:0]   udw_cntr = 0;           // user data word counter
wire    [7:0]   udw_cntr_mux;           // mux on input of udw_cntr
reg             ld_udw_cntr;            // 1 = load udw_cntr
wire            udw_cntr_tc;            // 1 = udw_cntr == 0
reg     [8:0]   cs_reg = 0;             // checksum generation register
reg             clr_cs_reg;             // 1 = clear cs_reg to 0
reg     [7:0]   vpid_mux;               // selects the VPID byte to be output 
reg     [1:0]   vpid_mux_sel;           // controls vpid_mux
reg     [3:0]   out_mux_sel;            // controls the vid_out data mux
wire            parity;                 // parity calculation
reg     [3:0]   sav_timing = 0;         // shift register for generating sav_out
reg     [3:0]   eav_timing = 0;         // shift register for generating eav_out

reg     [STATE_MSB:0]   current_state = STATE_WAIT;     // FSM current state
reg     [STATE_MSB:0]   next_state;                     // FSM next state

reg     [7:0]   byte1_reg = 0;
reg     [7:0]   byte2_reg = 0;
reg     [7:0]   byte3_reg = 0;
reg     [7:0]   byte4_reg = 0;

//
// Input registers and video pipeline registers
//
always @ (posedge clk)
    if (ce) begin
        in_reg    <= y_in;
        vid_reg0  <= in_reg;
        vid_reg1  <= vid_reg0;
        vid_reg2  <= vid_reg1;
        vid_dly   <= vid_reg2;
        byte1_reg <= byte1;
        byte2_reg <= byte2;
        byte3_reg <= byte3;
        byte4_reg <= byte4;
    end

//
// all ones and all zeros detectors
//
assign all_ones_in = &in_reg;
assign all_zeros_in = ~|in_reg;

always @ (posedge clk)
    if (rst)
        all_zeros_pipe <= 0;
    else if (ce)
        all_zeros_pipe <= {all_zeros_pipe[1:0], all_zeros_in};

always @ (posedge clk)
    if (rst)
        all_ones_pipe <= 0;
    else if (ce)
        all_ones_pipe <= {all_ones_pipe[1:0], all_ones_in};

//
// EAV, SAV, and ADF detection
//
assign xyz = all_ones_pipe[2] & all_zeros_pipe[1] & all_zeros_pipe[0];

assign eav_next = xyz & in_reg[6];
assign sav_next = xyz & ~in_reg[6];
assign anc_next = (~hd_sd & level_b) ?
                  all_zeros_pipe[1] & all_ones_pipe[0] & all_ones_in :
                  all_zeros_pipe[2] & all_ones_pipe[1] & all_ones_pipe[0];

//
// This SRL16 is used to generate the hanc_start_next signal. The input to the
// shift register is eav_next. The depth of the shift register depends on 
// whether the video is HD or SD.
//
always @ *
    if (hd_sd)
        hanc_dly <= 4'd3;
    else
        hanc_dly <= 4'd7;

SRLC16E #(
    .INIT       (16'h0000))
EAVDLY (
    .Q          (hanc_start_next),
    .Q15        (),
    .A0         (hanc_dly[0]),
    .A1         (hanc_dly[1]),
    .A2         (hanc_dly[2]),
    .A3         (hanc_dly[3]),
    .CE         (ce),
    .CLK        (clk),
    .D          (eav_next));
          
//
// Line number comparison
//
// Two comparators are used to determine if the current line number matches
// either of the two lines where the VPID packets are located. The second
// line can be disabled for progressive video by setting line_b_en low.
//
assign line_match_a = line == line_a;
assign line_match_b = line == line_b;

always @ (posedge clk)
    if (ce)
        vpid_line = line_match_a | (line_match_b & line_b_en);

//
// DID/SDID match
//
// The vpid_pkt signal is asserted when the next two words in the video delay
// pipeline indicate a video payload ID packet. The del_pkt_ok signal is
// asserted when the data in the video delay pipeline indicates that a deleted
// ANC packet is present with a data count of at least 4.
//
assign vpid_pkt = vid_reg2[7:0] == 8'h41 && vid_reg1[7:0] == 8'h01;
assign del_pkt_ok = vid_reg2[7:0] == 8'h80 && vid_reg0[7:0] >= 8'h04;

//
// UDW counter
//
// This counter is used to cycle through the user data words of non-VPID ANC 
// packets that may be encountered before empty HANC space is found.
//
assign udw_cntr_mux = ld_udw_cntr ? vid_dly[7:0] : udw_cntr;
assign udw_cntr_tc = udw_cntr_mux == 8'h00;

always @ (posedge clk)
    if (ce)
        udw_cntr <= udw_cntr_mux - 1;

//
// Checksum generation
//
always @ (posedge clk)
    if (ce) begin
        if (clr_cs_reg)
            cs_reg <= 0;
        else
            cs_reg <= cs_reg + vid_out[8:0];
    end

//
// Video data path
//
always @ *
    case(vpid_mux_sel)
        2'b00:   vpid_mux <= byte1_reg;
        2'b01:   vpid_mux <= byte2_reg;
        2'b10:   vpid_mux <= byte3_reg;
        default: vpid_mux <= byte4_reg;
    endcase

assign parity = ^vpid_mux;

always @ *
    case(out_mux_sel)
        MUX_SEL_000:  vid_out <= 10'h000;
        MUX_SEL_3FF:  vid_out <= 10'h3ff;
        MUX_SEL_DID:  vid_out <= 10'h241;   // DID
        MUX_SEL_SDID: vid_out <= 10'h101;   // SDID
        MUX_SEL_DC:   vid_out <= 10'h104;   // DC
        MUX_SEL_UDW:  vid_out <= {~parity, parity, vpid_mux};
        MUX_SEL_CS:   vid_out <= {~cs_reg[8], cs_reg};
        MUX_SEL_DEL:  vid_out <= 10'h180;   // deleted pkt DID
        default:      vid_out <= vid_dly;
    endcase

always @ (posedge clk)
    if (ce)
        y_out <= vid_out;

//
// Delay the C data stream by 6 clock cycles to match the Y data stream delay.
//
wide_SRLC16E #(
    .WIDTH      (10))
CDLY (
    .clk        (clk),
    .ce         (ce),
    .d          (c_in),
    .a          (4'd5),
    .q          (c_out));

//
// EAV & SAV output generation
//
always @ (posedge clk)
    if (ce)
        eav_timing <= {eav_timing[2:0], eav_next};
        
always @ (posedge clk)
    if (ce)
        eav_out <= eav_timing[3];

always @ (posedge clk)
    if (ce)
        sav_timing = {sav_timing[2:0], sav_next};

always @ (posedge clk)
    if (ce)
        sav_out <= sav_timing[3];

//
// FSM: current_state register
//
// This code implements the current state register. 
//
always @ (posedge clk or posedge rst)
    if (rst)
        current_state <= STATE_WAIT;
    else if (ce) begin
        if (sav_next)
            current_state <= STATE_WAIT;
        else
            current_state <= next_state;
        end

//
// FSM: next_state logic
//
// This case statement generates the next_state value for the FSM based on
// the current_state and the various FSM inputs.
//
always @ *
    case(current_state)
        STATE_WAIT:
            if (enable & vpid_line & hanc_start_next) begin
                if (anc_next)
                    next_state = STATE_ADF0;
                else
                    next_state = STATE_INS_ADF0;
            end else if (enable & ~vpid_line & anc_next & overwrite)
                next_state = STATE_ADF0_X;
            else    
                next_state = STATE_WAIT;
                
        STATE_ADF0:
            next_state = STATE_ADF1;

        STATE_ADF1:
            next_state = STATE_ADF2;

        STATE_ADF2:
            if (vpid_pkt)
                next_state = STATE_DID;
            else if (del_pkt_ok)
                next_state = STATE_INS_DID;
            else
                next_state = STATE_DID2;

        STATE_DID:
            next_state = STATE_SDID;

        STATE_SDID:
            if (overwrite)
                next_state = STATE_INS_DC;
            else
                next_state = STATE_DC;

        STATE_DC:
            next_state = STATE_B0;

        STATE_B0:
            next_state = STATE_B1;

        STATE_B1:
            next_state = STATE_B2;

        STATE_B2:
            next_state = STATE_B3;

        STATE_B3:
            next_state = STATE_CS;

        STATE_CS:
            next_state = STATE_WAIT;

        STATE_DID2:
            next_state = STATE_SDID2;

        STATE_SDID2:
            next_state = STATE_DC2;

        STATE_DC2:
            if (udw_cntr_tc)
                next_state = STATE_CS2;
            else
                next_state = STATE_UDW;

        STATE_UDW:
            if (udw_cntr_tc)
                next_state = STATE_CS2;
            else
                next_state = STATE_UDW;

        STATE_CS2:
            if (anc_next)
                next_state = STATE_ADF0;
            else
                next_state = STATE_INS_ADF0;

        STATE_INS_ADF0:
            next_state = STATE_INS_ADF1;

        STATE_INS_ADF1:
            next_state = STATE_INS_ADF2;

        STATE_INS_ADF2:
            next_state = STATE_INS_DID;

        STATE_INS_DID:
            next_state = STATE_INS_SDID;

        STATE_INS_SDID:
            next_state = STATE_INS_DC;

        STATE_INS_DC:
            next_state = STATE_INS_B0;

        STATE_INS_B0:
            next_state = STATE_INS_B1;

        STATE_INS_B1:
            next_state = STATE_INS_B2;

        STATE_INS_B2:
            next_state = STATE_INS_B3;

        STATE_INS_B3:   
            next_state = STATE_CS;

        STATE_ADF0_X:
            next_state = STATE_ADF1_X;

        STATE_ADF1_X:
            next_state = STATE_ADF2_X;

        STATE_ADF2_X:
            if (vpid_pkt)
                next_state = STATE_DID_X;
            else
                next_state = STATE_WAIT;

        STATE_DID_X:
            next_state = STATE_SDID_X;

        STATE_SDID_X:
            next_state = STATE_DC_X;

        STATE_DC_X:
            if (udw_cntr_tc)
                next_state = STATE_CS_X;
            else
                next_state = STATE_UDW_X;

        STATE_UDW_X:
            if (udw_cntr_tc)
                next_state = STATE_CS_X;
            else
                next_state = STATE_UDW_X;

        STATE_CS_X:
            if (anc_next)
                next_state = STATE_ADF0_X;
            else
                next_state = STATE_WAIT;

        default:    next_state = STATE_WAIT;
    endcase
        
//
// FSM: outputs
//
// This block decodes the current state to generate the various outputs of the
// FSM.
//
always @ *
    begin
        // Unless specifically assigned in the case statement, all FSM outputs
        // are given the values assigned here.
        ld_udw_cntr     = 1'b0;
        clr_cs_reg      = 1'b0;
        vpid_mux_sel    = 2'b00;
        out_mux_sel     = MUX_SEL_VID;
                                
        case(current_state) 

            STATE_ADF2:     clr_cs_reg = 1'b1;

            STATE_B0:       begin
                                out_mux_sel = level_b ? MUX_SEL_UDW : MUX_SEL_VID;
                                vpid_mux_sel = 2'b00;
                            end
        
            STATE_CS:       out_mux_sel = MUX_SEL_CS;

            STATE_DC2:      ld_udw_cntr = 1'b1;

            STATE_INS_ADF0: out_mux_sel = MUX_SEL_000;

            STATE_INS_ADF1: out_mux_sel = MUX_SEL_3FF;

            STATE_INS_ADF2: begin
                                out_mux_sel = MUX_SEL_3FF;
                                clr_cs_reg = 1'b1;
                            end

            STATE_INS_DID:  out_mux_sel = MUX_SEL_DID;

            STATE_INS_SDID: out_mux_sel = MUX_SEL_SDID;

            STATE_INS_DC:   out_mux_sel = MUX_SEL_DC;

            STATE_INS_B0:   begin
                                out_mux_sel = MUX_SEL_UDW;
                                vpid_mux_sel = 2'b00;
                            end
        
            STATE_INS_B1:   begin
                                out_mux_sel = MUX_SEL_UDW;
                                vpid_mux_sel = 2'b01;
                            end
        
            STATE_INS_B2:   begin
                                out_mux_sel = MUX_SEL_UDW;
                                vpid_mux_sel = 2'b10;
                            end
        
            STATE_INS_B3:   begin
                                out_mux_sel = MUX_SEL_UDW;
                                vpid_mux_sel = 2'b11;
                            end

            STATE_ADF2_X:   clr_cs_reg = 1'b1;

            STATE_DID_X:    out_mux_sel = MUX_SEL_DEL;

            STATE_DC_X:     ld_udw_cntr = 1'b1;

            STATE_CS_X:     out_mux_sel = MUX_SEL_CS;

        endcase
    end

endmodule
