//------------------------------------------------------------------------------ 
// Copyright (c) 2008 Xilinx, Inc. 
// All Rights Reserved 
//------------------------------------------------------------------------------ 
//   ____  ____ 
//  /   /\/   / 
// /___/  \  /   Vendor: Xilinx 
// \   \   \/    Author: John F. Snow, Solutions Development Group, Xilinx, Inc.
//  \   \        Filename: $RCSfile: smpte_encoder.v,rcs $
//  /   /        Date Last Modified:  $Date: 2008-11-07 11:33:48-07 $
// /___/   /\    Date Created: May 21, 2004
// \   \  /  \ 
//  \___\/\___\ 
// 
//
// Revision History: 
// $Log: smpte_encoder.v,rcs $
// Revision 1.4  2008-11-07 11:33:48-07  jsnow
// Added register initializers to eliminate unknowns in simulation.
//
// Revision 1.3  2004-12-09 13:15:43-07  jsnow
// Comment changes only.
//
// Revision 1.2  2004-10-15 14:59:39-06  jsnow
// Changed p_scram to 9 bits from 10.
//
// Revision 1.1  2004-10-15 13:02:29-06  jsnow
// Header updated.
//------------------------------------------------------------------------------ 
//
// LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
// as a template to make your own working designs exclusively with Xilinx
// products. Xilinx and its licensors make and you receive no warranties or 
// conditions, express, implied, statutory or otherwise, and Xilinx specifically
// disclaims any implied warranties of merchantability, non-infringement, or 
// fitness for a particular purpose. Xilinx does not warrant that the functions
// contained in these designs will meet your requirements, or that the operation
// of these designs will be uninterrupted or error free, or that defects in the 
// Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
// representations regarding use or the results of the use of the designs in 
// terms of correctness, accuracy, reliability, or otherwise. The designs are 
// not covered by any other agreement that you may have with Xilinx. 
//
// LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
// for any damages, including without limitation direct, indirect, incidental, 
// special, reliance or consequential damages arising from the use or operation 
// of the designs or accompanying documentation, however caused and on any 
// theory of liability. This limitation will apply even if Xilinx has been 
// advised of the possibility of such damage. This limitation shall apply 
// not-withstanding the failure of the essential purpose of any limited 
// remedies herein.
//------------------------------------------------------------------------------ 
/*
Module Description:

This module performs the SMPTE scrambling and NRZ-to-NRZI conversion algorithms
on 10-bit video words. It is designed to support both SDI (SMPTE 259M) and
HD-SDI (SMPTE 292M) standards.

When encoding HD-SDI video, two of these modules can be used to encode the
two video channels Y and C. Each module would run at the word rate (74.25 MHz)
and accept one video in and generate one encoded data word out per clock cycle.
It is also possible to use just one of these modules to encode both data
channels by running the module a 2X the video rate.

When encoding SD-SDI video, one module is used and data is encoded one word
per clock cycle.

The module has two clock cycles of latency. It accepts one 10-bit word every
clock cycle and also produces 10-bits of encoded data every clock cycle.

One clock cycle is used to scramble the data using the SMPTE X^9 + X^4 + 1
polynomial. During the second clock cycles, the scrambled data is converted to
NRZI data using the X + 1 polynomial.

Both the scrambling and NRZ-to-NRZI conversion have separate enable inputs. The
scram input enables scrambling when High. The nrzi input enables NRZ-to-NRZI
conversion when high.

The p_scram input vector provides 9 bits of data that was scrambled by the
during the previous clock cycle or by the other channel's smpte_encoder module. 
When implementing a HD-SDI encoder, the p_scram input of the C scrambler module 
must be connected to the i_scram_q output of the Y module and the p_scram input 
of the Y scrambler module must be connected to the i_scram output of the C 
module. For SD-SDI or for HD-SDI when running this module at 2X the HD-SDI word
rate, the p_scram input must be connected to the i_scram_q output of this same 
module.

The p_nrzi input provides one bit of data that was converted to NRZI by the
companion hdsdi_scram_lower module. When implementing a HD-SDI encoder, the 
p_nrzi input of the C scrambler module must be connected to the q[9] bit from 
the Y module and the p_nrzi input of the Y scrambler module must be connected to
the i_nrzi output of the C module. For SD-SDI or for HD-SDI when running this
module at 2X the HD-SDI word rate, the p_nrzi input must be connected to the 
q[9] output bit of this same module.

--------------------------------------------------------------------------------
*/

module smpte_encoder (
    input  wire         clk,        // input clock (bit-rate)
    input  wire         rst,        // reset signal
    input  wire         ce,         // clock enable
    input  wire         nrzi,       // enables NRZ-to-NRZI conversion when high
    input  wire         scram,      // enables SDI scrambler when high
    input  wire [9:0]   d,          // input data
    input  wire [8:0]   p_scram,    // previously scrambled data input 
    input  wire         p_nrzi,     // MSB of previously converted NRZI word
    output wire [9:0]   q,          // output data
    output wire [8:0]   i_scram,    // intermediate scrambled data output
    output wire [8:0]   i_scram_q,  // registered intermediate scrambled data output
    output wire         i_nrzi      // intermediate nrzi data output
);

//
// Signal definitions
//
reg     [9:0]       scram_reg = 0;  // pipeline delay register
reg     [9:0]       out_reg = 0;    // output register
wire    [13:0]      scram_in;       // input to the scrambler
reg     [9:0]       scram_temp;     // intermediate output of the scrambler
wire    [9:0]       scram_out;      // output of the scrambler
wire    [9:0]       nrzi_out;       // output of NRZ-to-NRZI converter
reg     [9:0]       nrzi_temp;      // intermediate output of the NRZ-to-NRZI converter
integer             i, j;           // for loop variables

//
// Scrambler
//
// This block of logic implements the SDI scrambler algorithm. The scrambler
// uses the 10 incoming bits from the input port and a 14-bit vector called 
// scram_in. scram_in is made up of 9 bits that were scrambled in the previous 
// clock cycle (p_scram) and the 5 LS scrambled bits that have been generated 
// during the current clock cycle. The results of the scrambler are assigned to 
// scram_temp.
//
// A MUX will output either the value of scram_temp or the d input word
// depending on the scram enable input. The output of the MUX is stored in the
// scram_reg.
//
assign scram_in = {scram_temp[4:0], p_scram[8:0]};

always @ *
    for (i = 0; i < 10; i = i + 1)
        scram_temp[i] = d[i] ^ scram_in[i] ^ scram_in[i + 4];

assign scram_out = scram ? scram_temp : d;

always @ (posedge clk or posedge rst)
    if (rst)
        scram_reg <= 0;
    else if (ce)
        scram_reg <= scram_out;

//
// NRZ-to-NRZI converter
//
// This block of logic implements the NRZ-to-NRZI conversion. It operates on the
// 10 bits coming from the scram_reg and the MSB from the output of the NRZ-to
// NRZI conversion done on the previous word (p_nrzi).. A MUX bypasses the 
// conversion process if the nrzi input is low.
//
always @ *
    begin
        nrzi_temp[0] = p_nrzi ^ scram_reg[0];
        for (j = 1; j < 10; j = j + 1)
            nrzi_temp[j] = nrzi_out[j - 1] ^ scram_reg[j];
    end

assign nrzi_out = nrzi ? nrzi_temp : scram_reg;

//
// out_reg: Output register
//
always @ (posedge clk or posedge rst)
    if (rst)
        out_reg <= 0;
    else if (ce)
        out_reg <= nrzi_out;

//
// output assignments
//
assign q = out_reg;
assign i_scram = scram_temp[9:1];
assign i_scram_q = scram_reg[9:1];
assign i_nrzi = nrzi_temp[9];

endmodule
