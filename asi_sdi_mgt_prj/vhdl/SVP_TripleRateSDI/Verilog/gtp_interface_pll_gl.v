////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Author : Reed P. Tidwell
//  \   \         Filename: $RCSfile: gtp_interface_pll_gl.v,v $
//  /   /         Date Last Modified:  $Date: 2010-04-08 15:38:48-06 $
// /___/   /\     Date Created: March 15, 2010
// \   \  /  \ 
//  \___\/\___\
//
// Revision History: 
// $Log: gtp_interface_pll_gl.v,v $
// Revision 1.1  2010-04-08 15:38:48-06  reedt
// April 2010 release.
//
// Revision 1.0  2010-04-07 13:29:50-06  reedt
// Initial revision
//
//
// (c) Copyright 2010 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of,
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES. 


`timescale 1ns / 1ps
`default_nettype none


module gtp_interface_pll_gl 
(
  input wire        outclk,           // txoutclk  or recovered clock on fabric routing not used by PLL
  input wire        gtpoutclk,        //input clock to PLL on dedicated clock routing
  input wire        pll_reset_in,
  input wire [19:0] rx_data_in,       // data passes through
  input wire [19:0] tx_data_in,       // data passes through 
  input wire        sd_mode,
  input wire        rx_ce,
  
  output wire[19:0] rx_data_out,
  output wire[19:0] tx_data_out,
  output wire       rx_usrclk,           // output user clock at input clock rate
  output wire       rx_usrclk2,          // output user clock at input clock /2
  output wire       tx_usrclk,           // output user clock at input clock rate
  output wire       tx_usrclk2,          // output user clock at input clock /2
  output wire       rx_pipe_clk,         // output clock for SDI pipeline
  output wire       tx_pipe_clk,         // output clock for SDI pipeline
  output wire       pll_locked_out, 
  output reg        pre_tx_ce            // TX ce with 5/6/5/6 synced to rx_ce
);                                                 
                                                  	
    
//************************** Signal Declarations ****************************
wire            pll_fb;
// clock enable generator registers
reg [4:0] seq_counter;
reg [3:0] rx_ce_sr = 0;       // rx_ce shift register

assign rx_data_out = rx_data_in;
assign tx_data_out = tx_data_in;
assign rx_pipe_clk = rx_usrclk2;
assign tx_pipe_clk = tx_usrclk2;

//   PLL    
    usrclk_pll_gl #
      (
          .MULT                           (3),
          .DIVIDE                         (1),
          .CLK_PERIOD                     (6.734006),//(3.367003),
          .OUT0_DIVIDE                    (3),
          .OUT1_DIVIDE                    (6),
          .OUT2_DIVIDE                    (12),
          .OUT3_DIVIDE                    (66)
      )
      usrclk_pll_gl
      (
          .CLK0_OUT                       (tx_usrclk),      //  divide by 1  
          .CLK1_OUT                       (rx_usrclk),    
          .CLK2_OUT                       (tx_usrclk2),     //  divide by 2               
          .CLK3_OUT                       (rx_usrclk2),
          .CLK_SEL                        (sd_mode),         
          .CLK_IN                         (gtpoutclk),    //   
          .CLKFB_IN                       (pll_fb),                
          .CLKFB_OUT                      (pll_fb),
          .PLL_LOCKED_OUT                 (pll_locked_out),  
          .PLL_RESET_IN                   (pll_reset_in)
      );

     // clock enable generator 
     always @ (posedge tx_pipe_clk) begin
       rx_ce_sr <= {rx_ce_sr[2:0], rx_ce}; 
       
       // Create tx_ce pattern synchronized with rx_ce 
       if (rx_ce_sr == 4'b1100 && rx_ce)   // reset pattern detected
         seq_counter <= 0;
       else
         seq_counter <= seq_counter + 1;
         
       // make 5/6/5/6 pattern according to sequence   
       case (seq_counter)
         3,9,14,20:
           pre_tx_ce <= 1;
         default:
           pre_tx_ce <= 0;
       endcase
       
     end
       
       
endmodule
