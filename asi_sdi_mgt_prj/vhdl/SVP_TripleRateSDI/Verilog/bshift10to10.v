///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008 Xilinx, Inc.
// All Rights Reserved
///////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: 1.0
//  \   \         Application : Non Integer DRU
//  /   /         Filename: bshift10to10.v
// /___/   /\     Timestamp: October 20 2008
// \   \  /  \
//  \___\/\___\
//
///////////////////////////////////////////////////////////////////////////////
//Design Name: Non integer DRU
//Purpose: barrel shifter for the DRU.
//Authors: Paolo Novellini, Giovanni Guasti. 
///////////////////////////////////////////////////////////////////////////////

module bshift10to10(CLK, RST, DIN, DV, DV10, DOUT10);
    input           CLK;
    input           RST;
    input  [9:0]    DIN;
    input  [3:0]    DV;
    output          DV10;
    output [9:0]    DOUT10;



    reg  [9:0]  DOUT10;
    reg         DV10;
    reg  [19:0] reg20;
    reg  [4:0]  i;

    wire [19:0] dinext;
    wire [19:0] dinext_rot;
    wire [19:0] mask;
    wire [4:0]  pointer1;
    wire [9:0]  regout;
    wire        wrflag;
    wire        valid;


    maskencoder I_Maskdec (
        .CLK(CLK),
        .RST(RST),
        .SHIFT(pointer1),
        .MASK(mask));


    control I_control (
        .CLK(CLK),
        .RST(RST),
        .DV(DV),
        .SHIFT(pointer1),
        .WRFLAG(wrflag),
        .VALID(valid));


    assign dinext = {10'b0000000000, DIN};

    rot20 Inst_data_bs (
        .CLK(CLK),
        .RST(RST),
        .HIN(dinext),
        .HOUT(dinext_rot),
        .P  (pointer1));



    // writing in the 20 bit register
    always @(posedge CLK or negedge RST)
    if (RST == 1'b0)
        reg20 <= 20'b00000000000000000000;
    else
        begin
        for(i=0; i<20; i=i+1)
            begin
                if (mask[i]==1'b1)
                    reg20[i] <= dinext_rot[i];
                else
                    reg20[i] <= reg20[i]; 
        end //for
        end //process begin
              
    
    
    assign regout =  (wrflag == 1'b0) ?  reg20[9:0] : reg20[19:10];       
    always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
          DOUT10 <= 10'b0000000000;
      else
          DOUT10 <= regout;

    always @(posedge CLK or negedge RST)
      if (RST == 1'b0)
          DV10 <= 1'b0;
      else
          DV10 <= valid;


//------------------------------------------------------------------------------
// for Elisa
//------------------------------------------------------------------------------


endmodule