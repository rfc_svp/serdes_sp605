-------------------------------------------------------------------------------- 
-- Copyright (c) 2010 Xilinx, Inc. 
-- All Rights Reserved 
-------------------------------------------------------------------------------- 
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /   Vendor: Xilinx 
-- \   \   \/    Author: Reed P. Tidwell
--  \   \        Filename: $RCSfile: mux12_wide.vhd,v $
--  /   /        Date Last Modified:  $Date: 2010-01-07 16:41:53-07 $
-- /___/   /\    Date Created: January 7, 2010
-- \   \  /  \ 
--  \___\/\___\ 
-- 
--
-- Revision History: 
-- $Log: mux12_wide.vhd,v $
-- Revision 1.0  2010-01-07 16:41:53-07  reedt
-- Working version.  Initial Checkin.
--
-------------------------------------------------------------------------------- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of,
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES. 
-------------------------------------------------------------------------------- 
--
-- Module Description:
--
-- This module implements a simple 12:1 MUX.  This is generic, rather than optimized
-- in order to work with multiple device families. The width of the MUX is set by 
-- the parameter WIDTH.
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

library unisim; 
use unisim.vcomponents.all; 

entity mux12_wide is
    generic (
        WIDTH:  integer := 10);
    port (
        d0 :    in  std_logic_vector(WIDTH-1 downto 0);
        d1 :    in  std_logic_vector(WIDTH-1 downto 0);
        d2 :    in  std_logic_vector(WIDTH-1 downto 0);
        d3 :    in  std_logic_vector(WIDTH-1 downto 0);
        d4 :    in  std_logic_vector(WIDTH-1 downto 0);
        d5 :    in  std_logic_vector(WIDTH-1 downto 0);
        d6 :    in  std_logic_vector(WIDTH-1 downto 0);
        d7 :    in  std_logic_vector(WIDTH-1 downto 0);
        d8 :    in  std_logic_vector(WIDTH-1 downto 0);
        d9 :    in  std_logic_vector(WIDTH-1 downto 0);
        d10 :   in  std_logic_vector(WIDTH-1 downto 0);
        d11 :   in  std_logic_vector(WIDTH-1 downto 0);
        sel :   in  std_logic_vector(3 downto 0);
        y :     out std_logic_vector(WIDTH-1 downto 0));
end mux12_wide;
architecture mux12_wide_arch of mux12_wide is
begin
  process (sel,d0,d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11)
  begin
    case (sel) is
      when "0000" => y <= d0 ;
      when "0001" => y <= d1 ;
      when "0010" => y <= d2 ;
      when "0011" => y <= d3 ;
      when "0100" => y <= d4 ;
      when "0101" => y <= d5 ;
      when "0110" => y <= d6 ;
      when "0111" => y <= d7 ;
      when "1000" => y <= d8 ;
      when "1001" => y <= d9 ;
      when "1010" => y <= d10;
      when "1011" => y <= d11;
      when others => y <= d0;  
    end case;
  end process;
  
end mux12_wide_arch;