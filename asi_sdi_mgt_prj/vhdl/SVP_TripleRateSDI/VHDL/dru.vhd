
------------------------------------------------------------------------------/
-- Copyright (c) 2003 Xilinx, Inc.
-- All Rights Reserved
------------------------------------------------------------------------------/
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: 1.0
--  \   \         Application : Non Integer DRU	Optimized for SD-SDI
--  /   /         Filename: tb_hw_dru.v
-- /___/   /\     Timestamp: May 1 2007
-- \   \  /  \
--  \___\/\___\
--
--Device: Virtex 5 LXT/FXT/TXT
--Design Name: dru.v
--Purpose: The NIDRU wrapper.
--Authors: Paolo Novellini, Giovanni Guasti. 
--
--    
------------------------------------------------------------------------------/

entity dru is
port(
  DT_IN:    in  std_logic_vector  (19 downto 0);
  CENTER_F: in  std_logic_vector  (36 downto 0); 	
  G1:       in  std_logic_vector  (4 downto 0); 	
  G1_P:     in  std_logic_vector  (4 downto 0); 	
  G2:       in  std_logic_vector  (4 downto 0); 	
  CLK:      in  std_logic; 			 
  RST:      in  std_logic; 
  RST_FREQ: in  std_logic;
  VER:      out std_logic_vector  (7 downto 0); 
  EN:       in  std_logic;      
  INTEG:    out std_logic_vector  (31 downto 0);
  DIRECT:	 out std_logic_vector  (31 downto 0);		 
  CTRL:     out std_logic_vector  (31 downto 0); 	
	 PH_OUT:   out std_logic_vector  (20 downto 0);	    
	 RECCLK:   out std_logic_vector  (19 downto 0);    	
  SAMV:     out std_logic_vector  (3 downto 0); 	
  SAM:      out std_logic_vector  (9 downto 0) 	
    );


end dru;
