-------------------------------------------------------------------------------- 
-- Copyright (c) 2009 Xilinx, Inc. 
-- All Rights Reserved 
-------------------------------------------------------------------------------- 
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /   Vendor: Xilinx 
-- \   \   \/    Author: Reed P. Tidwell
--  \   \        Filename: $RCSfile: s6gtp_sdi_rx_reset.vhd,v $
--  /   /        Date Last Modified:  $Date: 2010-01-04 16:39:53-07 $
-- /___/   /\    Date Created: December 30, 2009
-- \   \  /  \ 
--  \___\/\___\ 
-- 
--
-- Revision History: 
-- $Log: s6gtp_sdi_rx_reset.vhd,v $
-- Revision 1.0  2010-01-04 16:39:53-07  reedt
-- Working version.  Initial Checkin.
--
-------------------------------------------------------------------------------- 
--   
-- This file contains confidential and proprietary information of Xilinx, Inc. 
-- and is protected under U.S. and international copyright and other 
-- intellectual property laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any rights to the 
-- materials distributed herewith. Except as otherwise provided in a valid 
-- license issued to you by Xilinx, and to the maximum extent permitted by 
-- applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL 
-- FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, 
-- IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF 
-- MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; 
-- and (2) Xilinx shall not be liable (whether in contract or tort, including 
-- negligence, or under any other theory of liability) for any loss or damage of 
-- any kind or nature related to, arising under or in connection with these 
-- materials, including for any direct, or any indirect, special, incidental, 
-- or consequential loss or damage (including loss of data, profits, goodwill, 
-- or any type of loss or damage suffered as a result of any action brought by 
-- a third party) even if such damage or loss was reasonably foreseeable or 
-- Xilinx had been advised of the possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in 
-- any application requiring fail-safe performance, such as life-support or 
-- safety devices or systems, Class III medical devices, nuclear facilities, 
-- applications related to the deployment of airbags, or any other applications 
-- that could lead to death, personal injury, or severe property or 
-- environmental damage (individually and collectively, "Critical Applications").
-- Customer assumes the sole risk and liability of any use of Xilinx products in
-- Critical Applications, subject only to applicable laws and regulations 
-- governing limitations on product liability. 
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT
-- ALL TIMES.
-------------------------------------------------------------------------------- 
--  Module Description:
--
-- This module implements the reset circuits for a GTP receiver for SDI.
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity s6gtp_sdi_rx_reset is
generic (
    CBL_DISCONNECT_RST_CNTR_SIZE : integer := 12);
port (
    free_clk:           in  std_logic;                      -- free running clock
    usr_clk:            in  std_logic;                      -- RXUSRCLK
    gtp_reset:          in  std_logic;                      -- 1 = reset entire GTP
    pcs_reset:          in  std_logic;                      -- 1 = reset PCS
    cdr_reset:          in  std_logic;                      -- 1 = reset CDR & PCS
    eq_cd_n:            in  std_logic;                      -- from cable EQ carrier detect
    rate_change:        in  std_logic;                      -- 1 = bit rate change
    mode_change:        in  std_logic;                      -- 1 = mode change
    buffer_error:       in  std_logic;                      -- 1 = Rx buffer error
    reset_done:         in  std_logic;                      -- 1 = GTP internal reset complete
    gtp_rx_reset:       out std_logic;                      -- RXRESET
    gtp_rx_buf_reset:   out std_logic;                      -- RXBUFRESET
    gtp_rx_cdr_reset:   out std_logic;                      -- RXCDRRESET
    fabric_reset:       out std_logic;                      -- synchronous reset to fabric
    clk_mux_sel:        out std_logic);             -- BUFGMUX clock select
end s6gtp_sdi_rx_reset;

architecture xilinx of s6gtp_sdi_rx_reset is

subtype CBLDIS_CNTR_TYPE is std_logic_vector(CBL_DISCONNECT_RST_CNTR_SIZE-1 downto 0);

--
-- Internal signals
--
signal pcs_rst_trigger :    std_logic;
signal fabric_rst_sync :    std_logic_vector(1 downto 0) := "11";
signal eq_cd_sync :         std_logic_vector(3 downto 0) := (others => '0');
signal cdr_rst_capture :    std_logic := '0';
signal cdr_rst_sync :       std_logic_vector(1 downto 0) := (others => '0');
signal rate_change_capture: std_logic := '1';
signal rate_change_sync :   std_logic_vector(1 downto 0) := (others => '0');
signal disconnect_cntr :    CBLDIS_CNTR_TYPE := (others => '0');
signal disconnect_ff :      std_logic := '0';
signal cdr_rst_int :        std_logic;
signal reset_done_sync :    std_logic_vector(1 downto 0) := (others => '0');
signal gtp_reset_sync_reg : std_logic_vector(1 downto 0) := (others => '0');
signal gtp_reset_sync :     std_logic;
signal cdr_rst_disable :    std_logic := '1';
signal gtp_reset_capture :  std_logic := '1';
signal gtp_reset_done :     std_logic;
signal buffer_error_ff :    std_logic := '0';
signal cable_disconnect:    std_logic;

begin

--
-- This logic disables the CDR reset logic on initial power up or whenever a
-- GTP reset occurs. The GTP Rx doesn't seem to recover correctly if CDR
-- reset happens during the initial GTP reset after power on. 
--
process(free_clk)
begin
    if rising_edge(free_clk) then
        gtp_reset_sync_reg <= (gtp_reset_sync_reg(0) & gtp_reset);
    end if;
end process;

gtp_reset_sync <= gtp_reset_sync_reg(1);

process(free_clk)
begin
    if rising_edge(free_clk) then
        gtp_reset_capture <= gtp_reset_sync;
    end if;
end process;

gtp_reset_done <= gtp_reset_capture and not gtp_reset_sync;

process(free_clk)
begin
    if rising_edge(free_clk) then
        if gtp_reset_done = '1' then
            cdr_rst_disable <= '0';
        end if;
    end if;
end process;

-- 
-- gtp_rx_reset
--
-- The pcs_reset input directly controls the gtp_rx_reset output
--
gtp_rx_reset <= pcs_reset;

--
-- gtp_rx_buf_reset
--
-- The buffer_error signal directly controls the gtp_rx_buf_reset output.
-- The buffer_error signal goes through a FF in order to reduce the length of
-- the timing path.
--
process(free_clk)
begin
    if rising_edge(free_clk) then
        buffer_error_ff <= buffer_error;
    end if;
end process;

gtp_rx_buf_reset <= buffer_error_ff;

--
-- fabric_reset
--
-- When pcs_reset, buffer_error, or cdr_rst_int signals are asserted, assert
-- the fabric_reset output. This output is asserted asynchronously to any clock
-- but its falling edge (negation) is synchronous with the rising edge of the 
-- usr_clk.
--
pcs_rst_trigger <= pcs_reset or buffer_error_ff or cdr_rst_int;

process(usr_clk, pcs_rst_trigger)
begin
    if pcs_rst_trigger = '1' then
        fabric_rst_sync <= "11";
    elsif rising_edge(usr_clk) then
        fabric_rst_sync <= (fabric_rst_sync(0) & '0');
    end if;
end process;

fabric_reset <= fabric_rst_sync(1);

--
-- Cable disconnect glitch filter and synchronizer
--
-- This logic synchronizes the eq_cd_n signal (carrier detect -- asserted low
-- when cable is connected and signal is good) and removes short duration 
-- glitches. The signal must be High (cable disconnected) for three consecutive
-- cycles of free_clk before cable_disconnect will be asserted High.
--
process(free_clk)
begin
    if rising_edge(free_clk) then
        eq_cd_sync <= (eq_cd_sync(2 downto 0) & eq_cd_n);
    end if;
end process;

cable_disconnect <= '1' when eq_cd_sync(3 downto 1) = "111" else '0';

--
-- CDR reset synchronizer and pulse stretcher
--
-- This logic synchronizes the cdr_reset input to free_clk. It is edge
-- triggered on the rising edge of cdr_reset so that input pulses shorter than
-- the period of free_clk are detected and result in output pulses that are
-- multiple free_clk cycles long. The output signal will remain asserted until
-- several clock cycles after cdr_reset goes low.
--
process(free_clk, cdr_reset)
begin
    if cdr_reset = '1' then
        cdr_rst_capture <= '1';
    elsif rising_edge(free_clk) then
        if cdr_rst_sync(1) = '1' or gtp_reset_sync = '1' then
            cdr_rst_capture <= '0';
        end if;
    end if;
end process;

process(free_clk)
begin
    if rising_edge(free_clk) then
        if gtp_reset_sync = '1' then
            cdr_rst_sync <= (others => '0');
        else
            cdr_rst_sync <= (cdr_rst_sync(0) & cdr_rst_capture);
        end if;
    end if;
end process;

--
-- Rate change synchronizer and pulse stretcher
--
-- This logic synchronizes the rate_change input to free_clk. It is edge
-- triggered on the rising edge of rate_change so that input pules shorter than
-- the period of free_clk are detected and result in output pulses that are
-- multiple free_clk cycles long. The output signal will remain asserted until
-- several clock cycles after rate_change goes low.
--
process(free_clk, rate_change)
begin
    if rate_change = '1' then
        rate_change_capture <= '1';
    elsif rising_edge(free_clk) then
        if rate_change_sync(1) = '1' or gtp_reset_sync = '1' then
            rate_change_capture <= '0';
        end if;
    end if;
end process;

process(free_clk)
begin
    if rising_edge(free_clk) then
        if gtp_reset_sync = '1' then
            rate_change_sync <= (others => '0');
        else
            rate_change_sync <= (rate_change_sync(0) & rate_change_capture);
        end if;
    end if;
end process;

--
-- Cable disconnect timer
--
-- This counter generates an approximately 60 usec long reset pulse whenever
-- the cable_disconnect from the cable EQ carrier detect synchronizer and
-- glitch filter is asserted. The output signal is asserted as long as
-- cable_disconnect is asserted and will remain asserted for an additional 60 us
-- after cable_disconnect is negated.
--
process(free_clk)
begin
    if rising_edge(free_clk) then
        if cable_disconnect = '1' then
            disconnect_cntr <= (others => '0');
        elsif disconnect_ff = '1' then
            disconnect_cntr <= disconnect_cntr + 1;
        end if;
    end if;
end process;

process(free_clk)
begin
    if rising_edge(free_clk) then
        if cable_disconnect = '1' then
            disconnect_ff <= '1';
        elsif disconnect_cntr(CBL_DISCONNECT_RST_CNTR_SIZE-1) = '1' or gtp_reset_sync = '1' then
            disconnect_ff <= '0';
        end if;
    end if;
end process;

--
-- CDR reset
--
-- The gtp_rx_cdr_reset output and its internal equivalent, cdr_rst_int, are
-- asserted whenever the disconnect_ff is High or when any of the other
-- reset sources (cdr_rst_sync[1], rate_change_sync[1], or mode_change) are
-- High. The cdr_rst_int signal also causes the fabric_reset signal to be
-- asserted. The GTP's RXCDRRESET input, driven by the gtp_rx_cdr_reset signal
-- resets both the CDR and PCS sections of the GTP Rx. So, this signal resets
-- the entire Rx, but does not reset the common PLL section of the GTP.
--
cdr_rst_int <= (disconnect_ff or cdr_rst_sync(1) or rate_change_sync(1) or
                mode_change) and not cdr_rst_disable;

--gtp_rx_cdr_reset <= cdr_rst_int;
gtp_rx_cdr_reset <= '0';  --RFC
                     
--
-- BUFGMUX control
--
-- The clk_mux_sel signal is intended to be used with a BUFGCTRL primitive to
-- switch the RXUSRCLK from the RXRECCLK recovered clock to a reference clock
-- whenever the CDR reset is asserted. This is because during CDR resets
-- (including the entire time the cable is disconnected) the RXRECCLK is 
-- stopped. Many applications require a continuous clock, even if the the input
-- video bit stream is stopped.
--
process(free_clk, cdr_rst_int)
begin
    if cdr_rst_int = '1' then
        reset_done_sync <= (others => '0');
    elsif rising_edge(free_clk) then
        reset_done_sync <= (reset_done_sync(0) & reset_done);
    end if;
end process;

process(free_clk, cdr_rst_int)
begin
    if cdr_rst_int = '1' then
        clk_mux_sel <= '0';
    elsif rising_edge(free_clk) then
        if (reset_done_sync(1) = '1') then
            clk_mux_sel <= '1';
        end if;
    end if;
end process;

end xilinx;
