-------------------------------------------------------------------------------- 
-- Copyright (c) 2009 Xilinx, Inc. 
-- All Rights Reserved 
-------------------------------------------------------------------------------- 
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /   Vendor: Xilinx 
-- \   \   \/    Author: Reed P. Tidwell
--  \   \        Filename: $RCSfile: s6gtp_sdi_drp_control.vhd,v $
--  /   /        Date Last Modified:  $Date: 2010-12-06 10:43:19-07 $
-- /___/   /\    Date Created: December 30, 2009
-- \   \  /  \ 
--  \___\/\___\ 
-- 
--
-- Revision History: 
-- $Log: s6gtp_sdi_drp_control.vhd,v $
-- Revision 1.1  2010-12-06 10:43:19-07  reedt
-- Modified to match  corresponding Verilog module.
--
--
-- Revision 1.0  2010-01-04 16:39:14-07  reedt
-- Working version.  Initial Checkin.
--
-------------------------------------------------------------------------------- 
--   
-- This file contains confidential and proprietary information of Xilinx, Inc. 
-- and is protected under U.S. and international copyright and other 
-- intellectual property laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any rights to the 
-- materials distributed herewith. Except as otherwise provided in a valid 
-- license issued to you by Xilinx, and to the maximum extent permitted by 
-- applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL 
-- FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, 
-- IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF 
-- MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; 
-- and (2) Xilinx shall not be liable (whether in contract or tort, including 
-- negligence, or under any other theory of liability) for any loss or damage of 
-- any kind or nature related to, arising under or in connection with these 
-- materials, including for any direct, or any indirect, special, incidental, 
-- or consequential loss or damage (including loss of data, profits, goodwill, 
-- or any type of loss or damage suffered as a result of any action brought by 
-- a third party) even if such damage or loss was reasonably foreseeable or 
-- Xilinx had been advised of the possibility of the same.
-- herein.
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in 
-- any application requiring fail-safe performance, such as life-support or 
-- safety devices or systems, Class III medical devices, nuclear facilities, 
-- applications related to the deployment of airbags, or any other applications 
-- that could lead to death, personal injury, or severe property or 
-- environmental damage (individually and collectively, "Critical Applications").
-- Customer assumes the sole risk and liability of any use of Xilinx products in
-- Critical Applications, subject only to applicable laws and regulations 
-- governing limitations on product liability. 
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT
-- ALL TIMES.
-------------------------------------------------------------------------------- 
--
-- Module Description:
--
-- This module connects to the DRP of the GTP and modifies attributes in the GTP
-- tile in response to changes on its input control signals. This module is
-- specifically designed to support triple-rate SDI interfaces implemented in 
-- the GTP tile. It can independently select SD-SDI, HD-SDI, or 3G-SDI mode for 
-- each of the two Rx and two Tx units in the tile. It also can change the 
-- reference clock used by the tile. And, it can control the muxes that select 
-- what clock is driven onto the CLKOUTNORTH and CLKOUTSOUTH ports of the GTP 
-- tile.
-- 
-- On initial startup and after a reset, the module will initialize all 
-- attributes that it controls to match the input selections. After that it will
-- selectively update attributes in response to changes on the control inputs.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity s6gtp_sdi_drp_control is
generic (
    PMA_RX_CFG_HD:          std_logic_vector(27 downto 0) := X"05ce059";
    PMA_RX_CFG_SD:          std_logic_vector(27 downto 0) := X"05ce000";
    PMA_RX_CFG_3G:          std_logic_vector(27 downto 0) := X"05ce089";
    
    PLL_RXDIVSEL_OUT_HD:    std_logic_vector(1 downto 0)  := "01"; 
    PLL_RXDIVSEL_OUT_SD:    std_logic_vector(1 downto 0)  := "01"; 
    PLL_RXDIVSEL_OUT_3G:    std_logic_vector(1 downto 0)  := "00"; 

    PLL_TXDIVSEL_OUT_HD:    std_logic_vector(1 downto 0)  := "00"; 
    PLL_TXDIVSEL_OUT_SD:    std_logic_vector(1 downto 0)  := "00"; 
    PLL_TXDIVSEL_OUT_3G:    std_logic_vector(1 downto 0)  := "00");
port (
    clk:                in  std_logic;                      -- DRP DCLK
    rst:                in  std_logic;                      -- async reset
    tx0_mode:           in  std_logic_vector(1 downto 0);   -- TX0 mode select: 00=HD, 01=SD, 10=3G
    tx1_mode:           in  std_logic_vector(1 downto 0);   -- TX1 mode select: 00=HD, 01=SD, 10=3G
    rx0_mode:           in  std_logic_vector(1 downto 0);   -- RX0 mode select: 00=HD, 01=SD, 10=3G
    rx1_mode:           in  std_logic_vector(1 downto 0);   -- RX1 mode select: 00=HD, 01=SD, 10=3G
    do:                 in  std_logic_vector(15 downto 0);  -- connect to GTP DO port
    drdy:               in  std_logic;                      -- connect to GTP DRDY port 
    daddr:              out std_logic_vector(7 downto 0)    -- connect to GTP DADDR port 
                            := (others => '0');
    di:                 out std_logic_vector(15 downto 0);  -- connect to GTP DI port
    den:                out std_logic;                      -- connect to GTP DEN port
    dwe:                out std_logic;                      -- connect to GTP DWE port
    tx0_mode_change:    out std_logic;                      -- 1 = Tx0 mode changed
    tx1_mode_change:    out std_logic;                      -- 1 = Tx1 mode changed
    rx0_mode_change:    out std_logic;                      -- 1 = Rx0 mode changed
    rx1_mode_change:    out std_logic);                     -- 1 = Rx1 mode changed
end s6gtp_sdi_drp_control;

architecture xilinx of s6gtp_sdi_drp_control is

--
-- This group of constants defines the states of the master state machine.
--
constant MSTR_STATE_WIDTH :  integer := 5;
subtype  MSTR_STATE_TYPE is std_logic_vector(MSTR_STATE_WIDTH-1 downto 0);

constant MSTR_START      :       MSTR_STATE_TYPE := "00000";
constant MSTR_DO_REF     :       MSTR_STATE_TYPE := "00001";
constant MSTR_WAIT_REF   :       MSTR_STATE_TYPE := "00010";
constant MSTR_DO_RX0     :       MSTR_STATE_TYPE := "00011";
constant MSTR_WAIT_RX0   :       MSTR_STATE_TYPE := "00100";
constant MSTR_DO_RX0_B   :       MSTR_STATE_TYPE := "00101";
constant MSTR_WAIT_RX0_B :       MSTR_STATE_TYPE := "00110";
constant MSTR_DO_RX0_C   :       MSTR_STATE_TYPE := "00111";
constant MSTR_WAIT_RX0_C :       MSTR_STATE_TYPE := "01000";
constant MSTR_DO_RX1     :       MSTR_STATE_TYPE := "01001";
constant MSTR_WAIT_RX1   :       MSTR_STATE_TYPE := "01010";
constant MSTR_DO_RX1_B   :       MSTR_STATE_TYPE := "01011";
constant MSTR_WAIT_RX1_B :       MSTR_STATE_TYPE := "01100";
constant MSTR_DO_RX1_C   :       MSTR_STATE_TYPE := "01101";
constant MSTR_WAIT_RX1_C :       MSTR_STATE_TYPE := "01110";
constant MSTR_DO_TX0     :       MSTR_STATE_TYPE := "10001";
constant MSTR_WAIT_TX0   :       MSTR_STATE_TYPE := "10010";
constant MSTR_DO_TX1     :       MSTR_STATE_TYPE := "10101";
constant MSTR_WAIT_TX1   :       MSTR_STATE_TYPE := "10110";
constant MSTR_RST_0      :       MSTR_STATE_TYPE := "10111";
constant MSTR_RST_1      :       MSTR_STATE_TYPE := "11000";
 
--
-- This group of constants defines the states of the DRP state machine.
--
constant DRP_STATE_WIDTH :  integer := 3;
subtype  DRP_STATE_TYPE is std_logic_vector(DRP_STATE_WIDTH-1 downto 0);

constant DRP_STATE_WAIT :   DRP_STATE_TYPE := "000";
constant DRP_STATE_RD1 :    DRP_STATE_TYPE := "001";
constant DRP_STATE_RD2 :    DRP_STATE_TYPE := "011";
constant DRP_STATE_WR1 :    DRP_STATE_TYPE := "010";
constant DRP_STATE_WR2 :    DRP_STATE_TYPE := "110";

--
-- This group of constants defines the values for the what_changed encoder.
--   
constant CHANGED_MSB  : integer := 2;
subtype  CHANGED_TYPE is std_logic_vector(CHANGED_MSB downto 0);

constant NOTHING_CHANGED :  CHANGED_TYPE := "000";
constant TX0_CHANGED :      CHANGED_TYPE := "001";
constant TX1_CHANGED :      CHANGED_TYPE := "011";
constant RX0_CHANGED :      CHANGED_TYPE := "100";
constant RX1_CHANGED :      CHANGED_TYPE := "101";

--
-- This group of constants defines the values used to control the refclk select
-- mux.
--
subtype MODE_TYPE        is std_logic_vector(1 downto 0);   -- mode select registers
subtype DRP_DATA_TYPE    is std_logic_vector(15 downto 0);  -- DRP di & do
subtype DRP_ADDR_TYPE    is std_logic_vector(6 downto 0);   -- DRP DADDR
subtype PMA_CFG_TYPE     is std_logic_vector(24 downto 0);  -- PMA_CFG type
subtype CDR_SCAN_TYPE    is std_logic_vector(26 downto 0);  -- PMA_CDR_SCAN type
subtype PLL_DIV_TYPE     is std_logic_vector(1 downto 0);   -- PLL_DIVSELOUT type
subtype TIMEOUT_TYPE     is std_logic_vector(9 downto 0);   -- DRP access timeout timer type
subtype DRP_LOC_CNT_TYPE is std_logic_vector(2 downto 0);   -- DRP location counter type
 
constant DRP_TO_TC :    TIMEOUT_TYPE := (others => '1');    -- terminal count of drp_to_counter

--
-- Local signal declarations
--
signal tx0_in_reg :         MODE_TYPE       := (others => '0'); -- Sync & changed registers
signal tx0_sync_reg :       MODE_TYPE       := (others => '0'); -- for TX0 mode select
signal tx0_last_reg :       MODE_TYPE       := (others => '0');
signal tx0_changed_reg:     std_logic       := '1';
signal tx0_mc :             std_logic       := '1';

signal tx1_in_reg :         MODE_TYPE       := (others => '0'); -- Sync & changed registers
signal tx1_sync_reg :       MODE_TYPE       := (others => '0'); -- for TX1 mode select
signal tx1_last_reg :       MODE_TYPE       := (others => '0');
signal tx1_changed_reg:     std_logic       := '1';
signal tx1_mc :             std_logic       := '1';

signal rx0_in_reg :         MODE_TYPE       := (others => '0'); -- Sync & changed registers
signal rx0_sync_reg :       MODE_TYPE       := (others => '0'); -- for RX0 mode select
signal rx0_last_reg :       MODE_TYPE       := (others => '0');
signal rx0_changed_reg:     std_logic       := '1';
signal rx0_mc :             std_logic       := '1';

signal rx1_in_reg :         MODE_TYPE       := (others => '0'); -- Sync & changed registers
signal rx1_sync_reg :       MODE_TYPE       := (others => '0'); -- for RX1 mode select
signal rx1_last_reg :       MODE_TYPE       := (others => '0');
signal rx1_changed_reg:     std_logic       := '1';
signal rx1_mc :             std_logic       := '1';

signal what_changed :       CHANGED_TYPE    := NOTHING_CHANGED; -- indicates what value changed
signal mstr_current_state : MSTR_STATE_TYPE := MSTR_START;      -- master FSM current state
signal mstr_next_state :    MSTR_STATE_TYPE;                    -- master FSM next state
signal drp_current_state :  DRP_STATE_TYPE  := DRP_STATE_WAIT;  -- DRP FSM current state
signal drp_next_state :     DRP_STATE_TYPE;

signal ld_changed :         std_logic := '1';                   -- 1 = load sync & changed regs
signal ld_changed_x :       std_logic;
signal clr_changed :        std_logic;                          -- 1 = clear changed registers
signal drp_go :             std_logic;                          -- Go signal from master FSM to DRP FSM
signal drp_rdy :            std_logic;                          -- Ready signal from DRP FSM to master FSM
signal ld_capture :         std_logic;                          -- 1 = load capture register

signal capture :            DRP_DATA_TYPE   := (others => '0'); -- Holds data from DRP read cycle
signal mask :               DRP_DATA_TYPE;                      -- Masks bits not to be modified
signal mask_reg :           DRP_DATA_TYPE   := (others => '0'); -- Holds mask value
signal new_data :           DRP_DATA_TYPE;                      -- New data to be ORed into read data
signal new_data_reg :       DRP_DATA_TYPE   := (others => '0'); -- Holds new data value
signal ld_mask_nd :         std_logic;                          -- Loads the mask & new data regs
signal drp_daddr :          std_logic_vector(7 downto 0);       -- DRP address to be accessed
signal rx0_pma_cfg :        PMA_CFG_TYPE;                       -- Holds new PMA_RX_CFG0 value
signal rx1_pma_cfg :        PMA_CFG_TYPE;                       -- Holds new PMA_RX_CFG1 value
signal rx0_pll_div :        PLL_DIV_TYPE;                       -- Holds new PLL_RXDIVSEL_OUT0 value
signal rx1_pll_div :        PLL_DIV_TYPE;                       -- Holds new PLL_RXDIVSEL_OUT1 value
signal tx0_pll_div :        PLL_DIV_TYPE;                       -- Holds new PLL_TXDIVSEL_OUT0 value
signal tx1_pll_div :        PLL_DIV_TYPE;                       -- Holds new PLL_TXDIVSEL_OUT1 value

signal drp_timeout :        std_logic;                          -- 1 = DRP access timeout
signal drp_to_counter :     TIMEOUT_TYPE;                       -- DRP access timeout counter
signal clr_drp_to :         std_logic;                          -- 1 = clear DRP timeout counter

begin

--------------------------------------------------------------------------------
-- Input change detectors
--
-- For input signal there is an input register and a sync register, forming a
-- dual-rank synchronizer to synchronize the signal to the DRP controller clock
-- domain. This is followed by a "last" register hold the previous value. The
-- sync register and the last register are compared and the changed FF is set
-- if the values differ. The changed FF is cleared by a command from the master
-- state machine when it is done processing the change request. All registers
-- and the changed FF only clock when the ld_changed signal from the master
-- state machine is asserted High (the exception is the input register). This
-- is to prevent a change on a input data set from being missed should it 
-- happen right when the master state machine is servicing a previous change
-- on the same input data set.
--
process(clk)
begin
    if rising_edge(clk) then
        tx0_in_reg <= tx0_mode;
        if ld_changed = '1' then
            tx0_sync_reg <= tx0_in_reg;
            tx0_last_reg <= tx0_sync_reg;
        end if;
    end if;
end process;

process(clk, rst)
begin
    if rst = '1' then
        tx0_changed_reg <= '1';
    elsif rising_edge(clk) then
        if ld_changed = '1' and ((tx0_last_reg xor tx0_sync_reg) /= "00") then
            tx0_changed_reg <= '1';
        elsif clr_changed = '1' and (what_changed = TX0_CHANGED) then
            tx0_changed_reg <= '0';
        end if;
    end if;
end process;

process(clk)
begin
    if rising_edge(clk) then
        tx0_mc <= tx0_changed_reg;
    end if;
end process;

tx0_mode_change <= tx0_mc and not tx0_changed_reg;

process(clk)
begin
    if rising_edge(clk) then
        tx1_in_reg <= tx1_mode;
        if ld_changed = '1' then
            tx1_sync_reg <= tx1_in_reg;
            tx1_last_reg <= tx1_sync_reg;
        end if;
    end if;
end process;

process(clk, rst)
begin
    if rst = '1' then
        tx1_changed_reg <= '1';
    elsif rising_edge(clk) then
        if ld_changed = '1' and ((tx1_last_reg xor tx1_sync_reg) /= "00") then
            tx1_changed_reg <= '1';
        elsif clr_changed = '1' and (what_changed = TX1_CHANGED) then
            tx1_changed_reg <= '0';
        end if;
    end if;
end process;

process(clk)
begin
    if rising_edge(clk) then
        tx1_mc <= tx1_changed_reg;
    end if;
end process;

tx1_mode_change <= tx1_mc and not tx1_changed_reg;

process(clk)
begin
    if rising_edge(clk) then
        rx0_in_reg <= rx0_mode;
        if ld_changed = '1' then
            rx0_sync_reg <= rx0_in_reg;
            rx0_last_reg <= rx0_sync_reg;
        end if;
    end if;
end process;

process(clk, rst)
begin
    if rst = '1' then
        rx0_changed_reg <= '1';
    elsif rising_edge(clk) then
        if ld_changed = '1' and ((rx0_last_reg xor rx0_sync_reg) /= "00") then
            rx0_changed_reg <= '1';
        elsif clr_changed = '1' and (what_changed = RX0_CHANGED) then
            rx0_changed_reg <= '0';
        end if;
    end if;
end process;

process(clk)
begin
    if rising_edge(clk) then
        rx0_mc <= rx0_changed_reg;
    end if;
end process;

rx0_mode_change <= rx0_mc and not rx0_changed_reg;

process(clk)
begin
    if rising_edge(clk) then
        rx1_in_reg <= rx1_mode;
        if ld_changed = '1' then
            rx1_sync_reg <= rx1_in_reg;
            rx1_last_reg <= rx1_sync_reg;
        end if;
    end if;
end process;

process(clk, rst)
begin
    if rst = '1' then
        rx1_changed_reg <= '1';
    elsif rising_edge(clk) then
        if ld_changed = '1' and ((rx1_last_reg xor rx1_sync_reg) /= "00") then
            rx1_changed_reg <= '1';
        elsif clr_changed = '1' and (what_changed = RX1_CHANGED) then
            rx1_changed_reg <= '0';
        end if;
    end if;
end process;

process(clk)
begin
    if rising_edge(clk) then
        rx1_mc <= rx1_changed_reg;
    end if;
end process;

rx1_mode_change <= rx1_mc and not rx1_changed_reg;

        
--                                                              
-- Create values used for the new data word
--

with rx0_sync_reg select
    rx0_pma_cfg  <= PMA_RX_CFG_SD(24 downto 0) when "01",
                    PMA_RX_CFG_3G(24 downto 0) when "10",
                    PMA_RX_CFG_HD(24 downto 0) when others;

with rx1_sync_reg select
    rx1_pma_cfg  <= PMA_RX_CFG_SD(24 downto 0) when "01",
                    PMA_RX_CFG_3G(24 downto 0) when "10",
                    PMA_RX_CFG_HD(24 downto 0) when others;

with rx0_sync_reg select
    rx0_pll_div  <= PLL_RXDIVSEL_OUT_SD when "01",
                    PLL_RXDIVSEL_OUT_3G when "10",
                    PLL_RXDIVSEL_OUT_HD when others;

with rx1_sync_reg select
    rx1_pll_div  <= PLL_RXDIVSEL_OUT_SD when "01",
                    PLL_RXDIVSEL_OUT_3G when "10",
                    PLL_RXDIVSEL_OUT_HD when others;

with tx0_sync_reg select
    tx0_pll_div  <= PLL_TXDIVSEL_OUT_SD when "01",
                    PLL_TXDIVSEL_OUT_3G when "10",
                    PLL_TXDIVSEL_OUT_HD when others;

with tx1_sync_reg select
    tx1_pll_div  <= PLL_TXDIVSEL_OUT_SD when "01",
                    PLL_TXDIVSEL_OUT_3G when "10",
                    PLL_TXDIVSEL_OUT_HD when others;

--------------------------------------------------------------------------------
-- What Changed priority encoder
--
-- This code looks at all the change requests and selects the current highest
-- priority request to load into the what_changed register.
--    
process(clk)
begin
    if rising_edge(clk) then
        if clr_changed = '1' then
            what_changed <= NOTHING_CHANGED;
        elsif ld_changed = '1' and what_changed = NOTHING_CHANGED then
            if rx0_changed_reg = '1' then
                what_changed <= RX0_CHANGED;
            elsif rx1_changed_reg = '1' then
                what_changed <= RX1_CHANGED;
            elsif tx0_changed_reg = '1' then
                what_changed <= TX0_CHANGED;
            elsif tx1_changed_reg = '1' then
                what_changed <= TX1_CHANGED;
            else
                what_changed <= NOTHING_CHANGED;
            end if;
        end if;
    end if;
end process;

--------------------------------------------------------------------------------        
-- Master state machine
--
-- The master FSM examines the what_changed register and then initiates one
-- or more RMW cycles to the DRP to modify the correct attributes for that
-- particular change request.
--
-- The actual DRP RMW cycles are handled by a separate FSM, the DRP FSM. The
-- master FSM provides a DRP address, mask value, and new data words to the
-- DRP FSM and asserts a drp_go signal. The DRP FSM does the actual RMW cycle
-- and responds with a drp_rdy signal when the cycle is complete.
--

--
-- Current state register
-- 
process(clk, rst)
begin
    if rst = '1' then
        mstr_current_state <= MSTR_START;
        ld_changed <= '1';
    elsif rising_edge(clk) then
        mstr_current_state <= mstr_next_state;
        ld_changed <= ld_changed_x;
    end if;
end process;

--
-- Next state logic
--
process(mstr_current_state, what_changed, drp_rdy)
begin
    ld_changed_x <= '0';

    case mstr_current_state is
        when MSTR_START => 
            case what_changed is
                when RX0_CHANGED => mstr_next_state <= MSTR_DO_RX0;
                when RX1_CHANGED => mstr_next_state <= MSTR_DO_RX1;
                when TX0_CHANGED => mstr_next_state <= MSTR_DO_TX0;
                when TX1_CHANGED => mstr_next_state <= MSTR_DO_TX1;
                when others      => mstr_next_state <= MSTR_START;
                                    ld_changed_x <= '1';
            end case;

        when MSTR_DO_REF => 
            mstr_next_state <= MSTR_WAIT_REF;

        when MSTR_WAIT_REF => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_START;
                ld_changed_x <= '1';
            else
                mstr_next_state <= MSTR_WAIT_REF;
            end if;
    
        when MSTR_DO_RX0 => 
            mstr_next_state <= MSTR_WAIT_RX0;

        when MSTR_WAIT_RX0 => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_DO_RX0_B;
            else
                mstr_next_state <= MSTR_WAIT_RX0;
            end if;
    
        when MSTR_DO_RX0_B => 
            mstr_next_state <= MSTR_WAIT_RX0_B;

        when MSTR_WAIT_RX0_B => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_DO_RX0_C;
            else
                mstr_next_state <= MSTR_WAIT_RX0_B;
            end if;
    
        when MSTR_DO_RX0_C => 
            mstr_next_state <= MSTR_WAIT_RX0_C;

        when MSTR_WAIT_RX0_C => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_START;
                ld_changed_x <= '1';
            else
                mstr_next_state <= MSTR_WAIT_RX0_C;
            end if;

        when MSTR_DO_RX1 => 
            mstr_next_state <= MSTR_WAIT_RX1;

        when MSTR_WAIT_RX1 => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_DO_RX1_B;
            else
                mstr_next_state <= MSTR_WAIT_RX1;
            end if;

        when MSTR_DO_RX1_B => 
            mstr_next_state <= MSTR_WAIT_RX1_B;

        when MSTR_WAIT_RX1_B => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_DO_RX1_C;
            else
                mstr_next_state <= MSTR_WAIT_RX1_B;
            end if;

        when MSTR_DO_RX1_C => 
            mstr_next_state <= MSTR_WAIT_RX1_C;

        when MSTR_WAIT_RX1_C => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_START;
                ld_changed_x <= '1';
            else
                mstr_next_state <= MSTR_WAIT_RX1_C;
            end if;


        when MSTR_DO_TX0 => 
            mstr_next_state <= MSTR_WAIT_TX0;

        when MSTR_WAIT_TX0 => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_START;
                ld_changed_x <= '1';
            else
                mstr_next_state <= MSTR_WAIT_TX0;
            end if;

        when MSTR_DO_TX1 => 
            mstr_next_state <= MSTR_WAIT_TX1;

        when MSTR_WAIT_TX1 => 
            if drp_rdy = '1' then
                mstr_next_state <= MSTR_START;
                ld_changed_x <= '1';
            else
                mstr_next_state <= MSTR_WAIT_TX1;
            end if;

        when MSTR_RST_0 => 
            mstr_next_state <= MSTR_RST_1;
            ld_changed_x <= '1';

        when MSTR_RST_1 => 
            mstr_next_state <= MSTR_START;
            ld_changed_x <= '1';

        when others => 
            mstr_next_state <= MSTR_RST_0;
            ld_changed_x <= '1';
    end case;
end process;

--
-- Output logic
--
process(mstr_current_state, rx0_pma_cfg, rx0_pll_div, 
        rx1_pma_cfg, rx1_pll_div, tx0_pll_div, tx1_pll_div)
begin
    clr_changed <= '0';
    drp_go <= '0';
    mask <= (others => '0');
    new_data <= (others => '0');
    ld_mask_nd <= '0';
    drp_daddr <= (others => '0');

    case mstr_current_state is
        when MSTR_DO_REF =>     drp_go <= '1';
                                mask <= To_StdLogicVector(Bit_Vector'(X"ffff"));
                                new_data <= (X"0000");
                                ld_mask_nd <= '1';

        when MSTR_WAIT_REF =>   clr_changed <= '1';

        when MSTR_DO_RX0 =>     drp_go <= '1';
                                drp_daddr <= To_StdLogicVector(Bit_Vector'(X"36"));
                                mask <= To_StdLogicVector(Bit_Vector'(X"0000"));
                                new_data <= rx0_pma_cfg(15 downto 0) ;
                                ld_mask_nd <= '1';

        when MSTR_DO_RX0_B =>   drp_go <= '1';
                                drp_daddr <= To_StdLogicVector(Bit_Vector'(X"37"));
                                mask <= To_StdLogicVector(Bit_Vector'(X"fe00"));
                                new_data <= ("0000000" & rx0_pma_cfg(24 downto 16) );
                                ld_mask_nd <= '1';

        when MSTR_DO_RX0_C =>   drp_go <= '1';
                                drp_daddr <= To_StdLogicVector(Bit_Vector'(X"34"));
                                mask <= To_StdLogicVector(Bit_Vector'(X"fcff"));
                                new_data <= ("000000" & rx0_pll_div(1 downto 0) & X"00");
                                ld_mask_nd <= '1';

        when MSTR_WAIT_RX0_C => clr_changed <= '1';

        when MSTR_DO_RX1 =>     drp_go <= '1';
                                drp_daddr <= To_StdLogicVector(Bit_Vector'(X"76"));
                                mask <= To_StdLogicVector(Bit_Vector'(X"0000"));
                                new_data <= rx1_pma_cfg(15 downto 0) ;
                                ld_mask_nd <= '1';
                            
        when MSTR_DO_RX1_B =>   drp_go <= '1';
                                drp_daddr <= To_StdLogicVector(Bit_Vector'(X"77"));
                                mask <= To_StdLogicVector(Bit_Vector'(X"fe00"));
                                new_data <= ("0000000" & rx1_pma_cfg(24 downto 16));
                                ld_mask_nd <= '1';
            
        when MSTR_DO_RX1_C =>   drp_go <= '1';
                                drp_daddr <= To_StdLogicVector(Bit_Vector'(X"74"));
                                mask <= To_StdLogicVector(Bit_Vector'(X"fcff"));
                                new_data <= ("000000" &rx1_pll_div(1 downto 0) & X"00");
                                ld_mask_nd <= '1';
                                        

        when MSTR_WAIT_RX1_C => clr_changed <= '1';

        when MSTR_DO_TX0 =>     drp_go <= '1';
                                drp_daddr <= To_StdLogicVector(Bit_Vector'(X"34"));
                                mask <= To_StdLogicVector(Bit_Vector'(X"f3ff"));
                                new_data <= (X"0" & tx0_pll_div(1 downto 0) & "0000000000");
                                ld_mask_nd <= '1';

        when MSTR_WAIT_TX0 =>   clr_changed <= '1';

        when MSTR_DO_TX1 =>     drp_go <= '1';
                                drp_daddr <= To_StdLogicVector(Bit_Vector'(X"74"));
                                mask <= To_StdLogicVector(Bit_Vector'(X"f3ff"));
                                new_data <= (X"0" & tx1_pll_div(1 downto 0) & "0000000000");
                                ld_mask_nd <= '1';

        when MSTR_WAIT_TX1 =>   clr_changed <= '1';

        when others => 
    end case;
end process;


--------------------------------------------------------------------------------
-- DRP state machine
--
-- The DRP state machine performs the RMW cycle on the DRP at the request of the
-- master FSM. The master FSM provides the DRP address, a 16-bit mask indicating
-- which bits are to be modified (bits set to 0), and a 16-bit new data value
-- containing the new value. When the drp_go signal from the master FSM is
-- asserted, the DRP FSM will execute the RMW cycle. The DRP FSM asserts the
-- drp_rdy signal when it is ready to execute a RMW cycle and negates it for
-- the duration of the RMW cycle.
--
-- A timeout timer is used to timeout a DRP access should the DRP fail to
-- respond with a DRDY signal within a reasonable amount of time.
--

--
-- Current state register
--
process(clk, rst)
begin
    if rst = '1' then
        drp_current_state <= DRP_STATE_WAIT;
    elsif rising_edge(clk) then
        if drp_timeout = '1' then
            drp_current_state <= DRP_STATE_WAIT;
        else
            drp_current_state <= drp_next_state;
        end if;
    end if;
end process;

--
-- Next state logic
--
process(drp_current_state, drp_go, drdy)
begin
    case drp_current_state is
        when DRP_STATE_WAIT => 
            if drp_go = '1' then
                drp_next_state <= DRP_STATE_RD1;
            else
                drp_next_state <= DRP_STATE_WAIT;       
            end if;

        when DRP_STATE_RD1 => 
            drp_next_state <= DRP_STATE_RD2;

        when DRP_STATE_RD2 => 
            if drdy = '1' then
                drp_next_state <= DRP_STATE_WR1;
            else
                drp_next_state <= DRP_STATE_RD2;
            end if;

        when DRP_STATE_WR1 => 
            drp_next_state <= DRP_STATE_WR2;

        when DRP_STATE_WR2 => 
            if drdy = '1' then
                drp_next_state <= DRP_STATE_WAIT;
            else
                drp_next_state <= DRP_STATE_WR2;
            end if;

        when others => 
            drp_next_state <= DRP_STATE_WAIT;
    end case;
end process;

process(clk)
begin
    if rising_edge(clk) then
        if ld_mask_nd = '1' then
            mask_reg <= mask;
            new_data_reg <= new_data;
            daddr <= drp_daddr(7 downto 0);
        end if;
    end if;
end process;

--
-- Output logic
--
process(drp_current_state)
begin
    den <= '0';
    dwe <= '0';
    ld_capture <= '0';
    drp_rdy <= '0';
    clr_drp_to <= '0';

    case drp_current_state is
        when DRP_STATE_WAIT =>  drp_rdy <= '1';
                                clr_drp_to <= '1';

        when DRP_STATE_RD1 =>   den <= '1';

        when DRP_STATE_RD2 =>   ld_capture <= '1';

        when DRP_STATE_WR1 =>   den <= '1';
                                dwe <= '1';
                                clr_drp_to <= '1';

        when others =>
    end case;
end process;

--
-- A timeout counter for DRP accesses. If the timeout counter reaches its
-- terminal count, the DRP state machine aborts the transfer.
--
process(clk)
begin
    if rising_edge(clk) then
        if clr_drp_to = '1' then
            drp_to_counter <= (others => '0');
        else
            drp_to_counter <= drp_to_counter + 1;
        end if;
    end if;
end process;

drp_timeout <= '1' when drp_to_counter = DRP_TO_TC else '0';

--
-- DRP di capture register
--
process(clk)
begin
    if rising_edge(clk) then
        if ld_capture = '1' then
            capture <= do;
        end if;
    end if;
end process;

di <= (capture and mask_reg) or (new_data_reg and not mask_reg);

end xilinx;
