-------------------------------------------------------------------------------- 
-- Copyright (c) 2007 Xilinx, Inc. 
-- All Rights Reserved 
-------------------------------------------------------------------------------- 
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /   Vendor: Xilinx 
-- \   \   \/    Author: Reed P.Tidwell, Advanced Product Division, Xilinx, Inc.
--  \   \        Filename: $RCSfile: sync_one_shot.vhd,v $
--  /   /        Date Last Modified:  $Date: 2007-10-25 15:25:14-06 $
-- /___/   /\    Date Created: Sept 26, 2007
-- \   \  /  \ 
--  \___\/\___\ 
-- 
--
-- Revision History: 
-- $Log: sync_one_shot.vhd,v $
-- Revision 1.2  2007-10-25 15:25:14-06  reedt
-- Initial release.
--
--
-------------------------------------------------------------------------------- 
--
-- LIMITED WARRANTY AND DISCLAMER. These designs are provided to you "as is" or 
-- as a template to make your own working designs exclusively with Xilinx
-- products. Xilinx and its licensors make and you receive no warranties or 
-- conditions, express, implied, statutory or otherwise, and Xilinx specifically
-- disclaims any implied warranties of merchantability, non-infringement, or 
-- fitness for a particular purpose. Xilinx does not warrant that the functions
-- contained in these designs will meet your requirements, or that the operation
-- of these designs will be uninterrupted or error free, or that defects in the 
-- Designs will be corrected. Furthermore, Xilinx does not warrant or make any 
-- representations regarding use or the results of the use of the designs in 
-- terms of correctness, accuracy, reliability, or otherwise. The designs are 
-- not covered by any other agreement that you may have with Xilinx. 
--
-- LIMITATION OF LIABILITY. In no event will Xilinx or its licensors be liable 
-- for any damages, including without limitation direct, indirect, incidental, 
-- special, reliance or consequential damages arising from the use or operation 
-- of the designs or accompanying documentation, however caused and on any 
-- theory of liability. This limitation will apply even if Xilinx has been 
-- advised of the possibility of such damage. This limitation shall apply 
-- not-withstanding the failure of the essential purpose of any limited 
-- remedies herein.
-------------------------------------------------------------------------------- 
--
--  This module creates a pulse that is one ce cycle long, synchronized to clk 
--  and occurs shortly after the rising edge of the "in" input. This can 
--  accommodate an extremely wide range of pulse widths on the input.  The 
--  minimum width of the pulse is just long enough to be recognized as a 
--  high level by the SR latch.  There is no maximum width; however the input 
--  must remain low for at least four clock cycles in order for the next rising 
--  edge to be recognized as a distinct pulse.
--
--------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use UNISIM.VComponents.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity sync_one_shot is
Port(	
  clk:    in   std_logic;
  rst:    in   std_logic;
  ce:     in   std_logic;
  in1:    in   std_logic;
  out1:   out  std_logic
  );
end entity sync_one_shot;

architecture sync_one_shot of sync_one_shot is	

  signal       sr_out:    std_logic;   --output of SR Flip-Flop
  signal       sr_del1:   std_logic;   -- SR FF delayed one enabled state
  signal       sr_del2:   std_logic;   -- SR FF delayed two
  signal       sr_del3:   std_logic;   -- SR FF delayed three
  signal       reg_reset: std_logic;
begin  
    -- combinatorial output
  out1 <= '1' when (sr_del2 = '1' and  sr_del3 = '0')
  else 
    '0';

-- delay registers  
  process (clk)
  begin
    if (clk'event and clk = '1') then
      if (rst = '1') then
        sr_del1 <= '0';
        sr_del2 <= '0';
        sr_del3 <= '0';
      elsif (ce = '1') then 
        sr_del1 <= sr_out;
        sr_del2 <= sr_del1;
        sr_del3 <= sr_del2;
      end if;
	end if;
  end process; 
  
 reg_reset <= rst or sr_del3; 
  
-- define SR flip-flop 
  process ( in1, reg_reset, sr_out)
  begin 
    if (reg_reset= '1') then
      sr_out <= '0';
    elsif (in1'event and in1= '1') then 
      sr_out <= '1';
    else
      sr_out <= sr_out;
    end if;
  end  process;

end architecture sync_one_shot;
