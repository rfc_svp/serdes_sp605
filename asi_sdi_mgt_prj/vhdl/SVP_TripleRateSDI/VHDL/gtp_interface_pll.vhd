--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Author : Reed P. Tidwell
--  \   \         Filename: $RCSfile:  $
--  /   /         Date Last Modified:  $Date:  $
-- /___/   /\     Date Created: January 28, 2010
-- \   \  /  \ 
--  \___\/\___\
--
-- Revision History: 
-- $Log:  $
-- (c) Copyright 2010 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of,
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES. 


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity gtp_interface_pll is 
  port(
    outclk:           in  std_logic;                     -- txoutclk on fabric routing not used by PLL
    gtpoutclk :       in  std_logic;                     --input clock to PLL on dedicated clock routing
    pll_reset_in:     in  std_logic;        
    data_in:          in  std_logic_vector(19 downto 0); -- 20-bit input data
   
    data_out:         out std_logic_vector(19 downto 0);        -- 20-bit output data 
    usrclk:           out std_logic;                     -- output user clock at input clock rate
    usrclk2:          out std_logic;                     -- output user clock at input clock /4
    pipe_clk:         out std_logic;                     -- output clock for SDI pipeline
    pll_locked_out:   out std_logic         
);                                                 
                                                  	
end gtp_interface_pll;

architecture gtp_interface_pll_arch of gtp_interface_pll is
    
--************************** Signal Declarations ****************************
signal    pll_fb:           std_logic;
signal    temp_usrclk2:     std_logic;

  component usrclk_pll 
      generic (
          MULT            : integer;  
          DIVIDE          : integer;  
          CLK_PERIOD      : real;  
          OUT0_DIVIDE     : integer;  
          OUT1_DIVIDE     : integer;  
          OUT2_DIVIDE     : integer;  
          OUT3_DIVIDE     : integer  
      );                  
  port
  ( 
      CLK0_OUT           : out std_logic;
      CLK1_OUT           : out std_logic;
      CLK2_OUT           : out std_logic;
      CLK3_OUT           : out std_logic;
      CLK1_SEL           : in  std_logic;
      CLK_IN             : in  std_logic;
      CLKFB_IN           : in  std_logic;
      CLKFB_OUT          : out std_logic;
      PLL_LOCKED_OUT     : out std_logic;
      PLL_RESET_IN       : in  std_logic
  );
  end component;
  
begin

    data_out <= data_in;
    usrclk2  <= temp_usrclk2;
    pipe_clk <= temp_usrclk2;
    
--   PLL   
    usrclk_pll_inst : usrclk_pll 
      generic map(
          MULT            =>    3,
          DIVIDE          =>    1,
          CLK_PERIOD      =>    6.73406,
          OUT0_DIVIDE     =>    3,
          OUT1_DIVIDE     =>    3,
          OUT2_DIVIDE     =>    6,
          OUT3_DIVIDE     =>    66
      )                  
     port map (
          CLK0_OUT        =>    usrclk,       --  divide by 1
          CLK1_OUT        =>    open,    
          CLK2_OUT        =>    temp_usrclk2, --  divide by 2           
          CLK3_OUT        =>    open,
          CLK1_SEL        =>    '0',
          CLK_IN          =>    gtpoutclk,     
          CLKFB_IN        =>    pll_fb,                
          CLKFB_OUT       =>    pll_fb,
          PLL_LOCKED_OUT  =>    pll_locked_out,  
          PLL_RESET_IN    =>    pll_reset_in
      );

  
end gtp_interface_pll_arch;
