--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Author : Reed P. Tidwell
--  \   \         Filename: $RCSfile: SVP_SDI.vhd,v $
--  /   /         Date Last Modified:  $Date: 2010-11-05 13:22:27-06 $
-- /___/   /\     Date Created: February 2, 2010
-- \   \  /  \ 
--  \___\/\___\
--
-- Revision History: 
-- $Log: SVP_SDI.vhd,v $
-- Revision 1.0  2010-11-05 13:22:27-06  reedt
-- Initial revision
--
-- Revision 1.2  2010-02-25 17:34:43-07  reedt
-- Feb. 2010 full release
--
-- Revision 1.1  2010-02-19 14:42:48-07  reedt
-- Removal of debug modules and additon of production chipscope module in preparation for release.
--
-- Revision 1.0  2010-02-16 16:08:04-07  reedt
-- First working version.
--
-- (c) Copyright 2010 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of,
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES. 

-------------------------------------------------------------------------------- 
--
-- Module Description:
--
-- This module is a triple-rate SDI transmitter with signal generator, and receiver 
-- with data recovery, error checking, and line standard identification. It 
-- supports SD-SDI, HD-SDI, and level A 3G-SDI (1080p 50, 59.94, or 60 Hz only). 
-- It runs on  SP605 boards with the AVB FMC daughter card.
-- 
-------------------------------------------------------------------------------- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.hdsdi_pkg.all;
use work.anc_edh_pkg.all;

library unisim; 
use unisim.vcomponents.all; 

entity SVP_SDI is
generic(
   EXAMPLE_SIM_GTPRESET_SPEEDUP  : integer  :=   1;
   EXAMPLE_USE_CHIPSCOPE         : integer  :=   1;
   EXAMPLE_SIMULATION            : integer  :=   0 
);
port(
  TILE0_GTP1_REFCLK_PAD_N_IN:   in  std_logic;              -- GTP
  TILE0_GTP1_REFCLK_PAD_P_IN:   in  std_logic;          
  USER_SMA_GPIO_P:              out std_logic;          
  USER_SMA_GPIO_N:              out std_logic;          
  GPIO_SWITCH_0:                in  std_logic;          
  GPIO_SWITCH_1:                in  std_logic;          
  GPIO_SWITCH_2:                in  std_logic;          
  GPIO_SWITCH_3:                in  std_logic;          
  GPIO_BUTTON0:                 in  std_logic;          
  GPIO_BUTTON1:                 in  std_logic;          
  GPIO_BUTTON2:                 in  std_logic;          
  GPIO_BUTTON3:                 in  std_logic;          
  CPU_RESET:                    in  std_logic;          
  GPIO_HEADER_0_LS:             out std_logic;          
  GPIO_HEADER_1_LS:             out std_logic;          
  GPIO_HEADER_2_LS:             out std_logic;          
  GPIO_HEADER_3_LS:             out std_logic;          
  GPIO_LED_0:                   out std_logic;          
  GPIO_LED_1:                   out std_logic;          
  GPIO_LED_2:                   out std_logic;          
  GPIO_LED_3:                   out std_logic;          
  FPGA_AWAKE:                   out std_logic;          
  RXN_IN:                       in  std_logic_vector (1 downto 0);   
  RXP_IN:                       in  std_logic_vector (1 downto 0);   
  TXN_OUT:                      out std_logic_vector (1 downto 0);   
  TXP_OUT:                      out std_logic_vector (1 downto 0);   
    
-- ************************** FMC connector *****************************
  clk_27M_in:           		in  std_logic                -- 27 MHz X0

);
end SVP_SDI;

architecture SVP_SDI_arch of SVP_SDI is

  attribute iostandard : string;
  attribute keep : string;
  attribute equivalent_register_removal : string;
  attribute use_clock_enable : string;

--************************** Register Declarations ****************************

  signal ila_in0_r           : std_logic_vector(84 downto 0);
  signal ila_in1_r           : std_logic_vector(84 downto 0);
  signal tile0_resetdone0_r  : std_logic;
  signal tile0_resetdone0_r2 : std_logic;
  signal tile0_resetdone1_r  : std_logic;
  signal tile0_resetdone1_r2 : std_logic;
    

--**************************** Wire Declarations ******************************

    -------------------------- MGT Wrapper Wires ------------------------------

    ------------------------ Loopback and Powerdown Ports ----------------------
  signal tile0_loopback0_i   : std_logic_vector (2 downto 0) := "000";
  signal tile0_loopback1_i   : std_logic_vector (2 downto 0) := "000";
    --------------------------------- PLL Ports --------------------------------
  signal tile0_gtpreset0_i   : std_logic;
  signal tile0_gtpreset1_i   : std_logic;
  signal tile0_plllkdet0_i   : std_logic;
  signal tile0_plllkdet1_i   : std_logic;
  signal tile0_resetdone0_i  : std_logic;
  signal tile0_resetdone1_i  : std_logic;
    ------------------- Receive Ports - RX Data Path interface -----------------
  signal tile0_rxdata0_i     : std_logic_vector  (19 downto 0);
  signal rxdata1_20_gtp      : std_logic_vector  (19 downto 0);
  signal rxdata1_20          : std_logic_vector  (19 downto 0);
  signal tile0_rxrecclk0_i   : std_logic;
  signal tile0_rxrecclk1_i   : std_logic;
  signal rxrecclk1_buf       : std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
  signal tile0_rxcdrreset0_i : std_logic;
  signal tile0_rxcdrreset1_i : std_logic;
  signal auto_rxcdrreset     : std_logic;
    ----------- Receive Ports - RX Elastic Buffer and Phase Alignment ----------
  signal tile0_rxbufreset0_i : std_logic;
  signal tile0_rxbufreset1_i : std_logic;
  signal auto_rxbufreset     : std_logic;
  signal tile0_rxbufstatus0_i : std_logic_vector  (2 downto 0);
  signal tile0_rxbufstatus1_i : std_logic_vector  (2 downto 0);
    ---------------------------- TX/RX Datapath Ports --------------------------
  signal tile0_gtpclkout0_i   : std_logic_vector (1 downto 0);
  signal tile0_gtpclkout1_i   : std_logic_vector (1 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
  signal tile0_txdata0_i      : std_logic_vector(19 downto 0);
  signal txdata1_20           : std_logic_vector(19 downto 0);
  signal txdata1_20_gtp       : std_logic_vector(19 downto 0);
  signal tile0_txoutclk0_i    : std_logic;
  signal tile0_txoutclk1_i    : std_logic;


    ------------------------------- Global Signals -----------------------------
  signal tied_to_ground_i     : std_logic;
  signal tied_to_ground_vec_i : std_logic_vector  (191 downto 0);
  signal tied_to_vcc_i        : std_logic;         
  signal tied_to_vcc_vec_i    : std_logic_vector  (7 downto 0);  
  signal drp_clk_in_i         : std_logic;
  signal tile0_refclkout_bufg_i : std_logic;
    
    
    ----------------------------- User Clocks ---------------------------------
  signal txusrclk             : std_logic;
  signal txusrclk2            : std_logic;
  signal txpipeclk            : std_logic;
  signal rxusrclk             : std_logic;
  signal rxusrclk2            : std_logic;
  signal rxpipeclk            : std_logic;
  signal gtpclkout1_0_pll_locked : std_logic;
  signal gtpclkout1_0_pll_reset  : std_logic;
  signal gtpclkout1_0_bufio      : std_logic;
  signal pll2_fb_out_i           : std_logic;
  signal gtpclkout1_1_pll_locked : std_logic;
  signal gtpclkout1_1_pll_reset  : std_logic;
  signal gtpclkout1_1_bufio      : std_logic;

    ----------------------- Frame check/gen Module Signals --------------------
  signal tile0_gtp0_refclk_i     : std_logic;

  signal tile0_matchn0_i         : std_logic;
                                          
  signal tile0_txcharisk0_float_i : std_logic_vector     (1 downto 0); 
  signal tile0_txdata0_float_i    : std_logic_vector     (19 downto 0);
  signal tile0_track_data0_i      : std_logic;           
  signal tile0_error_count0_i     : std_logic_vector     (7 downto 0); 
  signal tile0_frame_check0_reset_i : std_logic;         
  signal tile0_inc_in0_i            : std_logic;         
  signal tile0_inc_out0_i           : std_logic;         
  signal tile0_matchn1_i            : std_logic;         
    
  signal tile0_txcharisk1_float_i   : std_logic_vector   (1 downto 0); 
  signal tile0_txdata1_float_i      : std_logic_vector   (19 downto 0);
  signal tile0_track_data1_i        : std_logic;         
  signal tile0_error_count1_i       : std_logic_vector   (7 downto 0); 
  signal tile0_frame_check1_reset_i : std_logic;
  signal tile0_inc_in1_i            : std_logic;
  signal tile0_inc_out1_i           : std_logic;

  signal reset_on_data_error_i      : std_logic;
  signal track_data_out_i           : std_logic;
    
    ------------------------- Sync Module Signals -----------------------------
  signal tile0_rx_sync_done0_i      : std_logic;
  signal tile0_rx_sync_done1_i      : std_logic;
  signal tile0_tx_sync_done0_i      : std_logic;
  signal tile0_tx_sync_done1_i      : std_logic;
   
----------------------- Chipscope Signals ---------------------------------
   component chipscope_icon
    port (
      control0 : inout std_logic_vector(35 downto 0)
    );
	end component;
  
  component chipscope_ila
  port (
    control : inout std_logic_vector(35 downto 0);
    clk : in std_logic;
    trig0 : in std_logic_vector(63 downto 0));

	end component;

  signal control0 : std_logic_vector(35 downto 0);
  signal trig0      :  std_logic_vector(63 downto 0);


  signal gtpreset0_i               : std_logic:= '0';   
  signal gtpreset1_i               : std_logic:= '0';   
  signal gtp_reset0_in             : std_logic:= '0';   
  signal gtp_reset1_in             : std_logic:= '0';   
  signal user_tx1_reset_i          : std_logic:= '0';   
  signal user_rx_reset_i           : std_logic:= '0';   
    
  signal dip_switch_in        : std_logic_vector (3 downto 0);     
  signal dip_switch_sync      : std_logic_vector (3 downto 0);     
  signal push_button_bus      : std_logic_vector (3 downto 0);     
  signal push_button_count    : std_logic_vector (9 downto 0);     
  signal push_button_1        : std_logic_vector (3 downto 0);     
  signal push_button_2        : std_logic_vector (3 downto 0);     
  signal push_button_3        : std_logic_vector (3 downto 0);     
  signal push_button_4        : std_logic_vector (3 downto 0);     
  signal push_button_5        : std_logic_vector (3 downto 0);     
  signal push_button_6        : std_logic_vector (3 downto 0);     
  signal push_button_7        : std_logic_vector (3 downto 0);     
  signal push_button_8        : std_logic_vector (3 downto 0);     
  signal push_button_stretch  : std_logic_vector (3 downto 0);     
  signal push_button_sync     : std_logic_vector (3 downto 0);     
    -- Tx signals 
  signal hdgen_y              : xavb_10b_vcomp_type;
  signal hdgen_c              : xavb_10b_vcomp_type;
  signal hdgen_ln             : xavb_hd_line_num_type;
  signal ntsc_patgen          : xavb_10b_vcomp_type;
  signal pal_patgen           : xavb_10b_vcomp_type;
  signal sd_patgen            : xavb_10b_vcomp_type;
  signal tile0_txbufstatus0   : std_logic_vector  (1 downto 0); 
  signal tile0_txbufstatus1   : std_logic_vector  (1 downto 0); 
  signal tx_sdimode_sel       : std_logic_vector  (1 downto 0):= "00";  
  signal tx_sdimode_sel_1     : std_logic_vector  (1 downto 0):= "00";   
  signal tx_sdimode_sel_2     : std_logic_vector  (1 downto 0):= "00";   
  signal tx_sdimode_sel_3     : std_logic_vector  (1 downto 0):= "00";  
  signal rx_sd_mode           : std_logic;    
  signal tx1_slew           : std_logic;    
  signal tx_format            : std_logic_vector (2 downto 0); 
  signal tx_format_sel        : std_logic_vector  (2 downto 0) := "010"; 
  signal tx_bitrate_sel       : std_logic;           
  signal tx_bitrate           : std_logic;           
  signal tx_bitrate_1         : std_logic;           
  signal tx_bitrate_2         : std_logic;           
  signal tx_bitrate_3         : std_logic;           
  signal tx_bitrate_4         : std_logic;           
  signal tx_bitrate_5         : std_logic;           
  signal tx_bitrate_6         : std_logic;           
  signal tx_bitrate_7         : std_logic;           
  signal tx_bitrate_8         : std_logic;           
  signal tx_rate_change       : std_logic := '0';            
  signal tx_rate_change_del   : std_logic := '0';           
  signal tx_pattern_sel       : std_logic_vector     (1 downto 0):= "00"; 
  signal tx_y_in              : xavb_10b_vcomp_type;
  signal tx_c_in              : xavb_10b_vcomp_type;
  signal tx_vpid_byte2        : std_logic_vector     (7 downto 0); 
  signal tx_ds1a              : xavb_10b_vcomp_type; 
  signal tx_ds2a              : xavb_10b_vcomp_type; 
  signal tx_ds1b              : xavb_10b_vcomp_type; 
  signal tx_ds2b              : xavb_10b_vcomp_type;      
  signal tx_eav               : std_logic;           
  signal tx_sav               : std_logic;           
  signal tx_out_mode          : std_logic_vector     (1 downto 0); 
  signal txreset1             : std_logic;           
  signal auto_txreset1        : std_logic;           
  signal tx1_fabric_reset     : std_logic;           
  signal auto_tx1_fabric_reset : std_logic;          
  signal gtp101_daddr             : std_logic_vector (7 downto 0); 
  signal gtp101_di                : std_logic_vector (15 downto 0);
  signal gtp101_drpo              : std_logic_vector (15 downto 0);
  signal gtp101_den               : std_logic;       
  signal gtp101_dwe               : std_logic;       
  signal gtp101_drdy              : std_logic; 
  -- VHDL only signals      
  signal sd_ntsc_vid :             std_ulogic_vector(9 downto 0);
  signal sd_pal_vid :              std_ulogic_vector(9 downto 0);
  signal tx_mode_HD :         std_logic;
  signal tx_mode_SD :         std_logic;
  signal tx_mode_3G :         std_logic;
 ----------------------

  signal mode_switches            : std_logic_vector (1 downto 0); 
  signal mode_switches_dly        : std_logic_vector (1 downto 0); 
  signal old_mode                 : std_logic_vector (1 downto 0); 
  signal vidgen_enable            : std_logic;       
  signal vidgen_disable_dly       : std_logic_vector (11 downto 0) := "000000000000";
  signal mode_change              : std_logic;       
  signal vidgen_disable_dly_tc    : std_logic;       

    -- Clock enables

  signal tx_ce    : std_logic_vector(4 downto 0) := "11111"; 
  attribute keep of tx_ce : signal is "TRUE";
  attribute equivalent_register_removal of tx_ce  :signal is "NO";

  signal sd_ce    : std_logic := '0';
  attribute keep of sd_ce : signal is "TRUE";
  attribute equivalent_register_removal of sd_ce : signal is "NO";

  signal gen_sd_ce   : std_logic_vector(10 downto 0) := "00001000001"; 
  attribute keep of gen_sd_ce : signal is "TRUE";
  attribute equivalent_register_removal of gen_sd_ce : signal is "NO";

  signal ce_mux     : std_logic;
    
------------------------------------------------------------------------------
-- SDI Receiver signals 
------------------------------------------------------------------------------
  constant RX1_NUM_CE     : integer  := 2;
  constant RX1_NUM_B_DRDY : integer  := 2;
  signal rx1_ce         : std_logic_vector (RX1_NUM_CE-1 downto 0);    
  attribute keep of rx1_ce :  signal is "TRUE";
  signal rx1_lvlb_drdy  : std_logic_vector (RX1_NUM_B_DRDY-1 downto 0);
  -- Clock signals

  signal gtp123_1_refclk : std_logic;            -- clock input
  signal rx1_gtp_recclk : std_logic;             -- recovered clock from GTX Rx #1
  signal rx1_recclk     : std_logic;             -- recovered clock buffered from DCM
  signal rx1_usrclk     : std_logic;             -- 297/148.5 MHz Rx user clock
  signal refclk_out     : std_logic;             -- 148.5 MHz reference clock from GTX
  signal refclk_out_buf : std_logic;                 -- 148.5 MHz reference clock from GTX
  signal rx1_clk_mux_sel : std_logic;            -- control BUFGMUX driving rx1_usrclk
  signal clkdiv_tx_clk  : std_logic_vector (3 downto 0);           -- output clocks produced by the clock divider

  signal rx1_gtp_data   : std_logic_vector (19 downto 0);        -- GTX RXDATA port
  signal rx1_mode       : std_logic_vector (1 downto 0);         -- operating mode (3G/HD/SD)
  signal rx1_mode_HD    : std_logic;           
  signal rx1_mode_SD    : std_logic;           
  signal rx1_mode_3G    : std_logic;           
  signal rx1_mode_locked  : std_logic;          
  signal rx1_rate         : std_logic;          
  signal rx1_format       : std_logic_vector (3 downto 0); 
  signal rx1_locked       : std_logic;       
  signal rx1_crc_err      : std_logic;       
  signal rx1_ln_a         : std_logic_vector (10 downto 0);
  signal rx1_ln_b         : std_logic_vector (10 downto 0);
  signal rx1_a_vpid       : std_logic_vector (31 downto 0);
  signal rx1_a_vpid_valid : std_logic;      
  signal rx1_b_vpid       : std_logic_vector (31 downto 0);
  signal rx1_b_vpid_valid : std_logic;      
  signal rx1_a_y          : xavb_10b_vcomp_type;
  signal rx1_a_c          : xavb_10b_vcomp_type;
  signal rx1_trs          : std_logic;       
  signal rx1_eav          : std_logic;       
  signal rx1_sav          : std_logic;       
  signal rx1_b_y          : xavb_10b_vcomp_type; 
  signal rx1_b_c          : xavb_10b_vcomp_type; 
  signal rx1_crc_err2     : std_logic;
  signal rx1_level_b      : std_logic;
  signal rx1_crc_err_ff   : std_logic := '0';
  attribute use_clock_enable of rx1_crc_err_ff :  signal is "YES";


  -- GTX reset related signals for tile 101
  signal gtp101_rxbufstatus1    : std_logic_vector (2 downto 0);
  signal gtp101_rxbufreset1     : std_logic;
  signal gtp101_resetdone1      : std_logic;
  signal gtp101_rxcdrreset1     : std_logic;
  signal rxreset1               : std_logic;
  signal auto_rxreset           : std_logic;

  signal gtp_reset_in           : std_logic;
  signal rx1_fabric_reset       : std_logic;
  signal auto_rx1_fabric_reset  : std_logic;

  signal edh_errcnt             : std_logic_vector (23 downto 0);
  signal clr_errs               : std_logic;
  signal rx1_err                : std_logic;
  signal edh_err                : std_logic;
  signal std                    : vidstd_type;
  signal sd_locked              : std_logic;
  signal locked                 : std_logic;
  
------------------------------------------------------------------------------
-- end SDI Receiver signals
------------------------------------------------------------------------------
-- startup clocks 
  signal   clk_out_bufg:  std_logic;
  signal   ringclk_i:     std_logic;

-- FMC LED signals
signal aes_rx2_red_led    : std_logic_vector(1 downto 0);
signal aes_rx2_grn_led    : std_logic_vector(1 downto 0);
signal madi_tx_red_led    : std_logic_vector(1 downto 0);
signal madi_tx_grn_led    : std_logic_vector(1 downto 0);
signal sync_red_led		   : std_logic_vector(1 downto 0);
signal sync_grn_led		   : std_logic_vector(1 downto 0);
signal gpioLED1           : std_logic;
signal gpioLED2           : std_logic;

-- SP605 LED signals

--
-- Component Declarations 
--

component Osc9  
  port( 
    out_clk:  out std_logic; 
    enable:   in  std_logic
   );
end component;

component s6_gtpwizard_v1_11 
generic
(
    -- Simulation attributes  
    WRAPPER_SIM_GTPRESET_SPEEDUP    : integer   := 1; -- Set to 1 to speed up sim reset
    WRAPPER_SIMULATION              : integer   := 0  -- Set to 1 for simulation  
);
port
(
    
    --_________________________________________________________________________
    --_________________________________________________________________________
    --TILE0  (X1_Y0)

    ------------------------ Loopback and Powerdown Ports ----------------------
    TILE0_LOOPBACK0_IN                      : in   std_logic_vector(2 downto 0);
    TILE0_LOOPBACK1_IN                      : in   std_logic_vector(2 downto 0);
    --------------------------------- PLL Ports --------------------------------
    TILE0_CLK00_IN                          : in   std_logic;
    TILE0_CLK01_IN                          : in   std_logic;
    TILE0_GTPRESET0_IN                      : in   std_logic;
    TILE0_GTPRESET1_IN                      : in   std_logic;
    --TILE0_PLLLKDET0_OUT                     : out  std_logic;
    TILE0_PLLLKDET1_OUT                     : out  std_logic;
    TILE0_RESETDONE0_OUT                    : out  std_logic;
    TILE0_RESETDONE1_OUT                    : out  std_logic;
    ------------------- Receive Ports - RX Data Path interface -----------------
    TILE0_RXDATA0_OUT                       : out  std_logic_vector(19 downto 0);
    TILE0_RXDATA1_OUT                       : out  std_logic_vector(19 downto 0);
    TILE0_RXRECCLK0_OUT                     : out  std_logic;
    TILE0_RXRECCLK1_OUT                     : out  std_logic;
    TILE0_RXRESET0_IN                       : in   std_logic;
    TILE0_RXRESET1_IN                       : in   std_logic;
    TILE0_RXUSRCLK0_IN                      : in   std_logic;
    TILE0_RXUSRCLK1_IN                      : in   std_logic;
    TILE0_RXUSRCLK20_IN                     : in   std_logic;
    TILE0_RXUSRCLK21_IN                     : in   std_logic;
    ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
    TILE0_RXCDRRESET0_IN                    : in   std_logic;
    TILE0_RXCDRRESET1_IN                    : in   std_logic;
    TILE0_RXN0_IN                           : in   std_logic;
    TILE0_RXN1_IN                           : in   std_logic;
    TILE0_RXP0_IN                           : in   std_logic;
    TILE0_RXP1_IN                           : in   std_logic;
    ----------- Receive Ports - RX Elastic Buffer and Phase Alignment ----------
    TILE0_RXBUFRESET0_IN                    : in   std_logic;
    TILE0_RXBUFRESET1_IN                    : in   std_logic;
    TILE0_RXBUFSTATUS0_OUT                  : out  std_logic_vector(2 downto 0);
    TILE0_RXBUFSTATUS1_OUT                  : out  std_logic_vector(2 downto 0);
    ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
    TILE0_DADDR_IN                          : in   std_logic_vector(7 downto 0);
    TILE0_DCLK_IN                           : in   std_logic;
    TILE0_DEN_IN                            : in   std_logic;
    TILE0_DI_IN                             : in   std_logic_vector(15 downto 0);
    TILE0_DRDY_OUT                          : out  std_logic;
    TILE0_DRPDO_OUT                         : out  std_logic_vector(15 downto 0);
    TILE0_DWE_IN                            : in   std_logic;
    ---------------------------- TX/RX Datapath Ports --------------------------
    TILE0_GTPCLKOUT0_OUT                    : out  std_logic_vector(1 downto 0);
    TILE0_GTPCLKOUT1_OUT                    : out  std_logic_vector(1 downto 0);
    --------------- Transmit Ports - TX Buffer and Phase Alignment -------------
    TILE0_TXBUFSTATUS0_OUT                  : out  std_logic_vector(1 downto 0);
    TILE0_TXBUFSTATUS1_OUT                  : out  std_logic_vector(1 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    TILE0_TXDATA0_IN                        : in   std_logic_vector(19 downto 0);
    TILE0_TXDATA1_IN                        : in   std_logic_vector(19 downto 0);
    TILE0_TXOUTCLK0_OUT                     : out  std_logic;
    TILE0_TXOUTCLK1_OUT                     : out  std_logic;
    TILE0_TXRESET0_IN                       : in   std_logic;
    TILE0_TXRESET1_IN                       : in   std_logic;
    TILE0_TXUSRCLK0_IN                      : in   std_logic;
    TILE0_TXUSRCLK1_IN                      : in   std_logic;
    TILE0_TXUSRCLK20_IN                     : in   std_logic;
    TILE0_TXUSRCLK21_IN                     : in   std_logic;
    --------------- Transmit Ports - TX Driver and OOB signalling --------------
    TILE0_TXN0_OUT                          : out  std_logic;
    TILE0_TXN1_OUT                          : out  std_logic;
    TILE0_TXP0_OUT                          : out  std_logic;
    TILE0_TXP1_OUT                          : out  std_logic


);
end component;

component gtp_interface_pll 
  port(
    outclk:           in  std_logic;                    
    gtpoutclk :       in  std_logic;                    
    pll_reset_in:     in  std_logic;        
    data_in:          in  std_logic_vector(19 downto 0);
   
    data_out:         out std_logic_vector(19 downto 0);
    usrclk:           out std_logic;                    
    usrclk2:          out std_logic;                    
    pipe_clk:         out std_logic;                    
    pll_locked_out:   out std_logic         
);                                                                                                   	
end component;

component s6gtp_sdi_control
generic (
    RATE_REFCLK_FREQ :              integer);

port (
    dclk :              in  std_logic;      
    rate_refclk:        in  std_logic;      
    rx0_usrclk :        in  std_logic;      
    rx1_usrclk :        in  std_logic;      
    tx0_usrclk :        in  std_logic;      
    tx1_usrclk :        in  std_logic;      
    drst :              in  std_logic;      
    rx0_mode :          in  std_logic_vector(1 downto 0);
    rx1_mode :          in  std_logic_vector(1 downto 0);
    tx0_mode :          in  std_logic_vector(1 downto 0);
    tx1_mode :          in  std_logic_vector(1 downto 0);
    tx0_slew :          out std_logic;   
    tx1_slew :          out std_logic;   
    rx0_rate :          out std_logic;   
    rx1_rate :          out std_logic;   
    gtp_reset0_in:      in  std_logic;   
    gtp_reset1_in:      in  std_logic;   
    clocks_stable :     in  std_logic;   
    rx0_pcs_reset :     in  std_logic;   
    rx0_cdr_reset :     in  std_logic;   
    rx0_fabric_reset :  out std_logic;   
    rx1_pcs_reset :     in  std_logic;   
    rx1_cdr_reset :     in  std_logic;   
    rx1_fabric_reset :  out std_logic;   
    tx0_reset :         in  std_logic;   
    tx0_fabric_reset :  out std_logic;   
    tx1_reset :         in  std_logic;   
    tx1_fabric_reset :  out std_logic;   
    daddr :             out std_logic_vector(7 downto 0); 
    den :               out std_logic;                    
    di :                out std_logic_vector(15 downto 0);
    drpo :              in  std_logic_vector(15 downto 0);
    drdy :              in  std_logic;                    
    dwe :               out std_logic;                    
    txbufstatus0_b1 :   in  std_logic;                    
    txbufstatus1_b1 :   in  std_logic;                    
    rxbufstatus0_b2 :   in  std_logic;                    
    rxbufstatus1_b2 :   in  std_logic;                    
    gtpreset0 :         out std_logic;                    
    gtpreset1 :         out std_logic;                    
    resetdone0 :        in  std_logic;                    
    rxreset0 :          out std_logic;                    
    rxbufreset0 :       out std_logic;                    
    rxcdrreset0 :       out std_logic;                    
    resetdone1 :        in  std_logic;                    
    rxreset1 :          out std_logic;                    
    rxbufreset1 :       out std_logic;                    
    rxcdrreset1 :       out std_logic;                    
    txreset0 :          out std_logic;                    
    txreset1 :          out std_logic);                   

end component;

component sync_one_shot
Port(	
  clk:    in   std_logic;
  rst:    in   std_logic;
  ce:     in   std_logic;
  in1:    in   std_logic;
  out1:   out  std_logic
  );
end component;

component multigenHD
  port (
        clk :       in  std_logic;                      
        rst :       in  std_logic;                      
        ce :        in  std_logic;                      
        std:        in  std_logic_vector(2 downto 0);   
        pattern:    in  std_logic_vector(1 downto 0);   
        user_opt:   in  std_logic_vector(1 downto 0);   
        y:          out hd_video_type;                  
        c:          out hd_video_type;                  
        h_blank:    out std_logic;                      
        v_blank:    out std_logic;                      
        field:      out std_logic;                      
        trs:        out std_logic;                      
        xyz:        out std_logic;                      
        line_num:   out hd_vpos_type);
end component;

component vidgen_ntsc
    generic (
        VID_WIDTH : integer := 10);
    port (
      clk_a:      in  std_logic;                                 
      rst_a:      in  std_logic;                                 
      ce_a:       in  std_logic;                                 
      pattern_a:  in  std_logic;                                 
      q_a:        out std_ulogic_vector(VID_WIDTH - 1 downto 0); 
      h_sync_a:   out std_logic;                                 
      v_sync_a:   out std_logic;                                 
      field_a:    out std_logic;                                 
      clk_b:      in  std_logic;                                 
      rst_b:      in  std_logic;                                 
      ce_b:       in  std_logic;                                 
      pattern_b:  in  std_logic;                                 
      q_b:        out std_ulogic_vector(VID_WIDTH - 1 downto 0); 
      h_sync_b:   out std_logic;                                 
      v_sync_b:   out std_logic;                                 
      field_b:    out std_logic);
end component;

component vidgen_pal
    generic (
        VID_WIDTH : integer := 10);
    port (
      clk_a:      in  std_logic;                                 
      rst_a:      in  std_logic;                                 
      ce_a:       in  std_logic;                                 
      pattern_a:  in  std_logic;                                 
      q_a:        out std_ulogic_vector(VID_WIDTH - 1 downto 0); 
      h_sync_a:   out std_logic;                                 
      v_sync_a:   out std_logic;                                 
      field_a:    out std_logic;                                 
      clk_b:      in  std_logic;                                 
      rst_b:      in  std_logic;                                 
      ce_b:       in  std_logic;                                 
      pattern_b:  in  std_logic;                                 
      q_b:        out std_ulogic_vector(VID_WIDTH - 1 downto 0); 
      h_sync_b:   out std_logic;                                 
      v_sync_b:   out std_logic;                                 
      field_b:    out std_logic);
end component;

component triple_sdi_vpid_insert
    port (
        clk:            in  std_logic;                      
        ce:             in  std_logic;                      
        din_rdy:        in  std_logic;                      
                                                            
        rst:            in  std_logic;                      
        sdi_mode:       in  std_logic_vector(1 downto 0);   
        level:          in  std_logic;                      
        enable:         in  std_logic;                      
        overwrite:      in  std_logic;                      
        byte1:          in  std_logic_vector(7 downto 0);   
        byte2:          in  std_logic_vector(7 downto 0);   
        byte3:          in  std_logic_vector(7 downto 0);   
        byte4a:         in  std_logic_vector(7 downto 0);   
        byte4b:         in  std_logic_vector(7 downto 0);   
        ln_a:           in  xavb_hd_line_num_type;          
        ln_b:           in  xavb_hd_line_num_type;          
        line_f1:        in  xavb_hd_line_num_type;          
        line_f2:        in  xavb_hd_line_num_type;          
        line_f2_en:     in  std_logic;                      
        a_y_in:         in  xavb_data_stream_type;          
        a_c_in:         in  xavb_data_stream_type;          
        b_y_in:         in  xavb_data_stream_type;          
        b_c_in:         in  xavb_data_stream_type;          
        ds1a_out:       out xavb_data_stream_type;          
        ds2a_out:       out xavb_data_stream_type;          
        ds1b_out:       out xavb_data_stream_type;          
        ds2b_out:       out xavb_data_stream_type;          
        eav_out:        out std_logic;                      
        sav_out:        out std_logic;                      
        out_mode:       out std_logic_vector(1 downto 0));                                                      
end component;

component triple_sdi_tx_output_20b
    port (
        clk:            in  std_logic;                                  
        din_rdy:        in  std_logic;                                  
        ce:             in  std_logic_vector(1 downto 0);                                  
        rst:            in  std_logic;                                  
        mode:           in  std_logic_vector(1 downto 0);               
        ds1a:           in  xavb_data_stream_type;                      
        ds2a:           in  xavb_data_stream_type;                      
        ds1b:           in  xavb_data_stream_type;                      
        ds2b:           in  xavb_data_stream_type;                      
        insert_crc:     in  std_logic;                                  
        insert_ln:      in  std_logic;                                  
        insert_edh:     in  std_logic;                                  
        ln_a:           in  xavb_hd_line_num_type;                      
        ln_b:           in  xavb_hd_line_num_type;                      
        eav:            in  std_logic;                                  
        sav:            in  std_logic;                                  
        txdata:         out std_logic_vector(19 downto 0);
        ce_align_err:   out std_logic);
end component;

component s6_sdi_rx_light_20b 
generic (
    NUM_SD_CE:          integer := 2;                  
    NUM_3G_DRDY:        integer := 2;                  
    ERRCNT_WIDTH:       integer := 4;                  
    MAX_ERRS_LOCKED:    integer := 15;                 
    MAX_ERRS_UNLOCKED:  integer := 2);                 
port (
    clk:            in  std_logic;                      
    rst:            in  std_logic;                      
    data_in:        in  std_logic_vector(19 downto 0);  
    frame_en:       in  std_logic;                      

    mode:           out std_logic_vector(1 downto 0);   
    mode_HD:        out std_logic;                      
    mode_SD:        out std_logic;                      
    mode_3G:        out std_logic;                      
    mode_locked:    out std_logic;                      
    rx_locked:      out std_logic;                      
    t_format:       out xavb_vid_format_type;           
    level_b_3G:     out std_logic;                      
    ce_sd:          out std_logic_vector(NUM_SD_CE-1 downto 0);
    nsp:            out std_logic;                     
    ln_a:           out xavb_hd_line_num_type;         
    a_vpid:         out std_logic_vector(31 downto 0); 
    a_vpid_valid:   out std_logic;                     
    b_vpid:         out std_logic_vector(31 downto 0); 
    b_vpid_valid:   out std_logic;                     
    crc_err_a:      out std_logic;                     
    ds1_a:          out xavb_data_stream_type;         
    ds2_a:          out xavb_data_stream_type;         
    eav:            out std_logic;                     
    sav:            out std_logic;                     
    trs:            out std_logic;                     
    ln_b:           out xavb_hd_line_num_type;        
    dout_rdy_3G:    out std_logic_vector(NUM_3G_DRDY-1 downto 0);
    crc_err_b:      out std_logic;                 
    ds1_b:          out xavb_data_stream_type;     
    ds2_b:          out xavb_data_stream_type);    
end component;

component edh_processor
    port (
        clk:            in  std_ulogic;         
        ce:             in  std_ulogic;         
        rst:            in  std_ulogic;         
        vid_in:         in  video_type;         
        reacquire:      in  std_ulogic;         
        en_sync_switch: in  std_ulogic;         
        en_trs_blank:   in  std_ulogic;         
        anc_idh_local:  in  std_ulogic;         
        anc_ues_local:  in  std_ulogic;         
        ap_idh_local:   in  std_ulogic;         
        ff_idh_local:   in  std_ulogic;         
        errcnt_flg_en:  in  edh_allflg_type;    
        clr_errcnt:     in  std_ulogic;         
        receive_mode:   in  std_ulogic;         
        vid_out:        out video_type;         
        std:            out vidstd_type;        
        std_locked:     out std_ulogic;         
        trs:            out std_ulogic;         
        field:          out std_ulogic;         
        v_blank:        out std_ulogic;         
        h_blank:        out std_ulogic;         
        horz_count:     out hpos_type;          
        vert_count:     out vpos_type;          
        sync_switch:    out std_ulogic;         
        locked:         out std_ulogic;         
        eav_next:       out std_ulogic;         
        sav_next:       out std_ulogic;         
        xyz_word:       out std_ulogic;         
        anc_next:       out std_ulogic;         
        edh_next:       out std_ulogic;         
        rx_ap_flags:    out edh_flgset_type;    
        rx_ff_flags:    out edh_flgset_type;    
        rx_anc_flags:   out edh_flgset_type;    
        ap_flags:       out edh_flgset_type;    
        ff_flags:       out edh_flgset_type;    
        anc_flags:      out edh_flgset_type;    
        packet_flags:   out edh_pktflg_type;    
        errcnt:         out edh_errcnt_type;    
        edh_packet:     out std_ulogic);        
end component;


-- component icon_2
  -- PORT (
    -- CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    -- CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

-- end component;

-- component ILA_67
  -- PORT (
    -- CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    -- CLK : IN STD_LOGIC;
    -- TRIG0 : IN STD_LOGIC_VECTOR(66 DOWNTO 0));

-- end component;

-- component vio_71
  -- PORT (
    -- CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    -- ASYNC_IN : IN STD_LOGIC_VECTOR(70 DOWNTO 0));

-- end component;

begin
--**************************** Main Body of Code *******************************

    --  Static signal Assigments    
    tied_to_ground_i             <= '0';
    tied_to_ground_vec_i         <= X"000000000000000000000000000000000000000000000000";
    tied_to_vcc_i                <= '1';
    tied_to_vcc_vec_i            <= X"ff";

--****************************************************************   
    -- Temporary work around for GTP startup issue
    -- Not needed for production silicon
    --

    mybufg: BUFG                                 
      port map(                                                 
          I     =>     ringclk_i,         
          O     =>     clk_out_bufg        
      );                                                
    
    Iring : Osc9 
      port map( 
        out_clk => ringclk_i, 
        enable  => '1'
      );
    
    Istartup : STARTUP_SPARTAN6 
      port map (
        GSR   	    => '0', 
        CFGCLK	    => open, 
    	   CFGMCLK    => open, 
    	   EOS	      => open,
	       CLK	      => clk_out_bufg,
	       GTS   	    => '0',
	       KEYCLEARB  => '1'
    );

    -----------------------Dedicated GTP Reference Clock Inputs ---------------
    -- The dedicated reference clock inputs you selected in the GUI are implemented using
    -- IBUFDS instances.
    -- This network is the highest performace (lowest jitter) option for providing clocks
    -- to the GTP transceivers.
    
ibufds_gtp123_1 : IBUFDS 
generic map(
  IOSTANDARD	=> "LVDS_25",
  DIFF_TERM	=> TRUE)
port map(
	 I			 => TILE0_GTP1_REFCLK_PAD_P_IN,
	 IB			 => TILE0_GTP1_REFCLK_PAD_N_IN,
	 O			 => gtp123_1_refclk);

    ----------------------------------- User Clocks ---------------------------
    
    -- The clock resources in this section were added based on userclk source selections on
    -- the Latency, Buffering, and Clocking page of the GUI. A few notes about user clocks:
    -- * The userclk and userclk2 for each GTP datapath (TX and RX) must be phase aligned to 
    --   avoid data errors in the fabric interface whenever the datapath is wider than 10 bits
    -- * To minimize clock resources, you can share clocks between GTPs. GTPs using the same frequency
    --   or multiples of the same frequency can be accomadated using DCMs and PLLs. Use caution when
    --   using the recovered clock(gtpclkout(1)) as a clock source, however - these clocks can 
    --   typically only be shared if all the channels using the clock are receiving data 
    --   from TX channels that share a reference clock source with each other.

    gtpclkout1_0_txpll_bufio2_i : BUFIO2 
    generic map(
        DIVIDE                         => 1,
        DIVIDE_BYPASS                  => TRUE
    )    
    port map(
        I                              => tile0_gtpclkout1_i(0),
        DIVCLK                         => gtpclkout1_0_bufio,
        IOCLK                          => open,
        SERDESSTROBE                   => open
    );
    
    gtpclkout1_1_dcm2_bufio2_i : BUFIO2 
    generic map(
        DIVIDE                         => 1,
        DIVIDE_BYPASS                  => TRUE
    )    
    port map(
        I                              => tile0_gtpclkout1_i(0),
        DIVCLK                         => gtpclkout1_1_bufio,
        IOCLK                          => open,
        SERDESSTROBE                   => open
    );

--------------------------------------------------------------------------------
-- Generate clock enables
--
-- sd_ce runs at 27 MHz and is asserted at a 5/6/5/6 cadence
-- tx_ce is always 1 for 3G-SDI and HD-SDI and equal to sd_ce for SD-SDI
--
process (txpipeclk) begin
  if rising_edge(txpipeclk) then
     if tx1_fabric_reset  = '1' then
         gen_sd_ce <= "00001000001";
     else
         gen_sd_ce <= (gen_sd_ce(9 downto 0) & gen_sd_ce(10));
     end if;
  end if;
end process;

process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    sd_ce <= gen_sd_ce(10);
  end if;
end process;

ce_mux <= gen_sd_ce(10) when tx_sdimode_sel_3 = "01" else '1'; 

process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    tx_ce <=  ce_mux & ce_mux & ce_mux & ce_mux & ce_mux ;
  end if;
end process;
           
process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    tx_bitrate_1 <= tx_bitrate;
    tx_bitrate_2 <= tx_bitrate_1;
    tx_bitrate_3 <= tx_bitrate_2;
    tx_bitrate_4 <= tx_bitrate_3; 
    tx_bitrate_5 <= tx_bitrate_4; 
    tx_bitrate_6 <= tx_bitrate_5; 
    tx_bitrate_7 <= tx_bitrate_6; 
    tx_bitrate_8 <= tx_bitrate_7; 
    if tx_bitrate_7 /= tx_bitrate_2  then   
      tx_rate_change     <= '1';
    else
      tx_rate_change     <= '0';
    end if;
    
    if tx_bitrate_8 /= tx_bitrate_7 then
      tx_rate_change_del <= '1';
    else
      tx_rate_change_del <= '0';
    end if;     
  end if;
end process;
   
  tx_mode_HD      <= '1' when tx_sdimode_sel_3 = "00" else '0';
  tx_mode_SD      <= '1' when tx_sdimode_sel_3 = "01" else '0';
  tx_mode_3G      <= '1' when tx_sdimode_sel_3 = "10" else '0';


  gtp_reset0_in <= gtpreset0_i or tx_rate_change_del or gtp_reset_in; --RFC
  gtp_reset1_in <= gtpreset1_i or tx_rate_change_del or gtp_reset_in; --RFC
-- resetCDR after  a TX rate change
  tile0_rxcdrreset1_i  <=  auto_rxcdrreset;   
  tile0_rxcdrreset0_i  <=  auto_rxcdrreset;   
  rxreset1             <=  auto_rxreset;   
  tile0_rxbufreset1_i  <=  auto_rxbufreset;
  tile0_rxbufreset0_i  <=  auto_rxbufreset;
  rx1_fabric_reset     <=  auto_rx1_fabric_reset;
  txreset1             <=  auto_txreset1;   
  tx1_fabric_reset     <=  auto_tx1_fabric_reset;
       
    ----------------------------- The GTP Wrapper -----------------------------
    
    -- Use the instantiation template in the examples directory to add the GTP wrapper to your design.
    -- In this example, the wrapper is wired up for basic operation with a frame generator and frame 
    -- checker. The GTPs will reset, then attempt to align and transmit data. If channel bonding is 
    -- enabled, bonding should occur after alignment.


    wiz1_4_20b_i : s6_gtpwizard_v1_11
    generic map
    (
        WRAPPER_SIM_GTPRESET_SPEEDUP    =>      1,    -- Set this to 1 for simulation
        WRAPPER_SIMULATION              =>      0    -- Set this to 1 for simulation
    )
    port map
    (
        --_____________________________________________________________________
        --_____________________________________________________________________
        --TILE0  (X1_Y0)

        ------------------------ Loopback and Powerdown Ports ----------------------
        TILE0_LOOPBACK0_IN              =>      tile0_loopback0_i,
        TILE0_LOOPBACK1_IN              =>      tile0_loopback1_i,
        --------------------------------- PLL Ports --------------------------------
        TILE0_CLK00_IN                  =>      gtp123_1_refclk,
        TILE0_CLK01_IN                  =>      gtp123_1_refclk,
        TILE0_GTPRESET0_IN              =>      tile0_gtpreset0_i,
        TILE0_GTPRESET1_IN              =>      tile0_gtpreset1_i,
        --TILE0_PLLLKDET0_OUT             =>      tile0_plllkdet0_i,
        TILE0_PLLLKDET1_OUT             =>      tile0_plllkdet1_i,
        TILE0_RESETDONE0_OUT            =>      tile0_resetdone0_i,
        TILE0_RESETDONE1_OUT            =>      tile0_resetdone1_i,
        ------------------- Receive Ports - RX Data Path interface -----------------
        TILE0_RXDATA0_OUT               =>      tile0_rxdata0_i,
        TILE0_RXDATA1_OUT               =>      rxdata1_20_gtp,
        TILE0_RXRECCLK0_OUT             =>      tile0_rxrecclk0_i,
        TILE0_RXRECCLK1_OUT             =>      tile0_rxrecclk1_i,
        TILE0_RXRESET0_IN               =>      rxreset1,
        TILE0_RXRESET1_IN               =>      rxreset1,
        TILE0_RXUSRCLK0_IN              =>      rxusrclk,
        TILE0_RXUSRCLK1_IN              =>      rxusrclk,
        TILE0_RXUSRCLK20_IN             =>      rxusrclk2,
        TILE0_RXUSRCLK21_IN             =>      rxusrclk2,
        ------- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        TILE0_RXCDRRESET0_IN            =>      tile0_rxcdrreset0_i,
        TILE0_RXCDRRESET1_IN            =>      tile0_rxcdrreset1_i,
        TILE0_RXN0_IN                   =>      RXN_IN(0),
        TILE0_RXN1_IN                   =>      RXN_IN(1),
        TILE0_RXP0_IN                   =>      RXP_IN(0),
        TILE0_RXP1_IN                   =>      RXP_IN(1),
        ----------- Receive Ports - RX Elastic Buffer and Phase Alignment ----------
        TILE0_RXBUFRESET0_IN            =>      tile0_rxbufreset0_i,
        TILE0_RXBUFRESET1_IN            =>      tile0_rxbufreset1_i,
        TILE0_RXBUFSTATUS0_OUT          =>      tile0_rxbufstatus0_i,
        TILE0_RXBUFSTATUS1_OUT          =>      tile0_rxbufstatus1_i,
        ------------- Shared Ports - Dynamic Reconfiguration Port (DRP) ------------
        TILE0_DADDR_IN                  =>      gtp101_daddr,
        TILE0_DCLK_IN                   =>      clk_27M_in,
        TILE0_DEN_IN                    =>      gtp101_den,
        TILE0_DI_IN                     =>      gtp101_di,
        TILE0_DRDY_OUT                  =>      gtp101_drdy,
        TILE0_DRPDO_OUT                 =>      gtp101_drpo,
        TILE0_DWE_IN                    =>      gtp101_dwe,
        ---------------------------- TX/RX Datapath Ports --------------------------
        TILE0_GTPCLKOUT0_OUT            =>      tile0_gtpclkout0_i,
        TILE0_GTPCLKOUT1_OUT            =>      tile0_gtpclkout1_i,
        --------------- Transmit Ports - TX Buffer and Phase Alignment -------------
        TILE0_TXBUFSTATUS0_OUT          =>      tile0_txbufstatus0,
        TILE0_TXBUFSTATUS1_OUT          =>      tile0_txbufstatus1,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        TILE0_TXDATA0_IN                =>      txdata1_20_gtp,
        TILE0_TXDATA1_IN                =>      txdata1_20_gtp,
        TILE0_TXOUTCLK0_OUT             =>      tile0_txoutclk0_i,
        TILE0_TXOUTCLK1_OUT             =>      tile0_txoutclk1_i,
        TILE0_TXRESET0_IN               =>      txreset1,
        TILE0_TXRESET1_IN               =>      txreset1,
        TILE0_TXUSRCLK0_IN              =>      txusrclk,
        TILE0_TXUSRCLK1_IN              =>      txusrclk,
        TILE0_TXUSRCLK20_IN             =>      txusrclk2,
        TILE0_TXUSRCLK21_IN             =>      txusrclk2,
        --------------- Transmit Ports - TX Driver and OOB signalling --------------
        TILE0_TXN0_OUT                  =>      TXN_OUT(0),
        TILE0_TXN1_OUT                  =>      TXN_OUT(1),
        TILE0_TXP0_OUT                  =>      TXP_OUT(0),
        TILE0_TXP1_OUT                  =>      TXP_OUT(1) 


    );
    
  rx_sd_mode <= '1' when rx1_mode = "01" else '0';        -- rx1_mode changes during lock-in 
  --gtpclkout1_0_pll_reset <=  not tile0_plllkdet0_i; 
  gtpclkout1_1_pll_reset <=  not tile0_plllkdet1_i; 
  gtpclkout1_0_pll_reset <=  not tile0_plllkdet1_i; 
 
 
-- GTP TX interface.
   gtp_interface_pll_tx : gtp_interface_pll  
   port map(
    outclk          => tile0_txoutclk1_i,        
    gtpoutclk       => gtpclkout1_0_bufio,      
    pll_reset_in    => gtpclkout1_0_pll_reset, 
    data_in         => txdata1_20,        

    data_out        => txdata1_20_gtp,
    usrclk          => txusrclk,         
    usrclk2         => txusrclk2,        
    pipe_clk        => txpipeclk,       
    pll_locked_out  => gtpclkout1_0_pll_locked 
  );
  
-- GTP RX interface 

  gtp_interface_pll_rx : gtp_interface_pll  
  port map(
     outclk           => tile0_rxrecclk1_i,       
     gtpoutclk        => gtpclkout1_1_bufio,     
     pll_reset_in     => gtpclkout1_1_pll_reset,
     data_in          => rxdata1_20_gtp,

     data_out         => rxdata1_20,
     usrclk           => rxusrclk,         
     usrclk2          => rxusrclk2,        
     pipe_clk         => rxpipeclk,       
     pll_locked_out   => gtpclkout1_1_pll_locked 
   );
    -------------------------- User Module Resets -----------------------------
    -- All the User Modules i.e. FRAME_GEN, FRAME_CHECK and the sync modules
    -- are held in reset till the RESETDONE goes high. 
    -- The RESETDONE is registered a couple of times on USRCLK2 and connected 
    -- to the reset of the modules
    
    process (txpipeclk, tile0_resetdone0_i)
    begin
      if  tile0_resetdone0_i = '0' then
            tile0_resetdone0_r    <=   '0';
            tile0_resetdone0_r2   <=   '0';
      
      elsif txpipeclk'event and txpipeclk = '1' then
            tile0_resetdone0_r    <=   tile0_resetdone0_i;
            tile0_resetdone0_r2   <=   tile0_resetdone0_r;
            
      end if;
    end process;
    
    process (txpipeclk,tile0_resetdone1_i)
    begin
      if tile0_resetdone1_i = '0' then
            tile0_resetdone1_r    <=    '0';
            tile0_resetdone1_r2   <=    '0';
      elsif  txpipeclk'event and txpipeclk = '1' then
          tile0_resetdone1_r    <=    tile0_resetdone1_i;
          tile0_resetdone1_r2   <=    tile0_resetdone1_r;
      end if;
    end process;

--
-- GTP SDI control module for GTP tile 123
--




GTPCTRL101 : s6gtp_sdi_control 
generic map(
     RATE_REFCLK_FREQ       => 27000000
 )
port map(
    dclk               => clk_27M_in,
    rate_refclk        => clk_27M_in,
    rx0_usrclk         => '0',
    rx1_usrclk         => rxpipeclk,
    tx0_usrclk         => '0',
    tx1_usrclk         => txpipeclk,   
    drst               => CPU_RESET,

    rx0_mode           => tied_to_ground_vec_i(1 downto 0),            
    rx1_mode           => rx1_mode,
    tx0_mode           => tied_to_ground_vec_i(1 downto 0),
    tx1_mode           => tx_sdimode_sel_3,

    tx0_slew           => open,
    tx1_slew           => tx1_slew, 

    rx0_rate           => open,
    rx1_rate           => rx1_rate,

    gtp_reset0_in      => gtp_reset0_in,
    gtp_reset1_in      => gtp_reset1_in,
    clocks_stable      => '1',

    rx0_pcs_reset      => '0',
    rx0_cdr_reset      => '0',
    rx0_fabric_reset   => open,

    rx1_pcs_reset      => '0',
    rx1_cdr_reset      => '0',
    rx1_fabric_reset   => auto_rx1_fabric_reset,

    tx0_reset          => '0',
    tx0_fabric_reset   => open,
    tx1_reset          => user_tx1_reset_i,
    tx1_fabric_reset   => auto_tx1_fabric_reset,   
    
    daddr              => gtp101_daddr,
    den                => gtp101_den,
    di                 => gtp101_di,
    drpo               => gtp101_drpo,
    drdy               => gtp101_drdy,
    dwe                => gtp101_dwe,

    txbufstatus0_b1    => tile0_txbufstatus0(1),
    txbufstatus1_b1    => tile0_txbufstatus1(1),
    rxbufstatus0_b2    => tile0_rxbufstatus0_i(2),
    rxbufstatus1_b2    => tile0_rxbufstatus1_i(2),

    gtpreset0          => tile0_gtpreset0_i,
    gtpreset1          => tile0_gtpreset1_i,

    resetdone0         => tile0_resetdone0_i,
    rxreset0           => open,
    rxbufreset0        => open,
    rxcdrreset0        => open,

    resetdone1         => tile0_resetdone1_i,
    rxreset1           => auto_rxreset,
    rxbufreset1        => auto_rxbufreset,
    rxcdrreset1        => auto_rxcdrreset,

    txreset0           => open,
    txreset1           => auto_txreset1
   );

--
-- For 3G-SDI, and SD-SDI, only use the LS bit of tx_format_sel to choose between 1080p
-- 50 Hz and 60 Hz. In HD-SDI, all 3 bits of tx_format_sel are used.
--
process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    if (tx_sdimode_sel_3 = "10") then
        tx_format <= ("10" & tx_format_sel(0));
    elsif (tx_sdimode_sel_3 = "01")   then
        tx_format <= ("00" & tx_format_sel(0));
    else    
        tx_format <= tx_format_sel;
    end if; 
  end if;
end process;

--
-- HD video pattern generator
--

-- Generate a disable signal to keep the HD/3G video generator's clock enable
-- deasserted during modes switches when the timing of the clock can become
-- unpredictable. The BRAMs in the multigenHD module can become corrupted due
-- to unpredictable timing of the clock and this can be prevented by keeping
-- the clock enable input deasserted.

process (txpipeclk)  begin
  if rising_edge(txpipeclk) then
       tx_sdimode_sel_1 <= tx_sdimode_sel;
       tx_sdimode_sel_2 <= tx_sdimode_sel_1;
       tx_sdimode_sel_3 <= tx_sdimode_sel_2;
       if tx_sdimode_sel_1 /= tx_sdimode_sel_3 then
         mode_change <= '1';  
       else 
         mode_change <= '0';
       end if;
  end if;
end process;

process (txpipeclk) begin
  if rising_edge(txpipeclk) then
        if (mode_change = '1' or tx_rate_change = '1' or gtpclkout1_0_pll_locked = '0') then
            vidgen_disable_dly <= "000000000000";
        elsif (vidgen_disable_dly_tc = '0') then
            vidgen_disable_dly <= vidgen_disable_dly + '1';
        end if;
  end if;
end process;

  --vidgen_disable_dly_tc <= '1' when vidgen_disable_dly = "111111111111" else '0';
  vidgen_disable_dly_tc <= '1' when vidgen_disable_dly = "000011110000" else '0';  --Only for simulation

process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    vidgen_enable <=  vidgen_disable_dly_tc;
  end if;
end process;

--
-- Sync the DIP switch signals to the local clock and assign them to the
-- appropriate control signals.
--
process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    if (tx_ce(0) = '1') then
        dip_switch_in <= (GPIO_SWITCH_3 & GPIO_SWITCH_2 &
                          GPIO_SWITCH_1 & GPIO_SWITCH_0);
    end if;
  end if;
end process;
                           
process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    if (tx_ce(0) = '1') then
        dip_switch_sync   <= dip_switch_in;
    end if;
  end if;
end process;

  tx_bitrate_sel <= dip_switch_sync(2);
  mode_switches  <= (dip_switch_sync(0) & dip_switch_sync(1));
  tx_sdimode_sel <=  mode_switches;    
  --tx_clk_freq    <= "0100" when tx_bitrate_8 = '1' else "0011";  --3= 148.5 MHz , 4= 148.5/1.001
  --Si5324_in_fsel <=  "10111";  -- h17 = 27 MHz clock 
  --Si5324_bw_sel  <= "1000" when tx_bitrate_8 = '1' else "1010";  -- @ 148.5 MHz, 8=> 4Hz; @ 148.35MHz, 10 => 6Hz 

process (txpipeclk) begin
  if rising_edge(txpipeclk) then
      -- Force tx_bitrate  to 0 for TX line standards where /M clock rates are not allowed
      case tx_sdimode_sel_3 is
        when "01" =>  tx_bitrate <= '0';          -- SD-SDI mode PAL & NTSC              
        when "10" =>                              -- 3G mode
          if (tx_format_sel(0) = '1') then         -- 3G  1080p 50Hz
            tx_bitrate <= '0';
          else                                     -- 3G  1080p 60Hz & 59.94
            tx_bitrate <= tx_bitrate_sel;
          end if;
        when others =>             -- HD mode
          case (tx_format_sel)is
            when "000" =>  tx_bitrate <= '0';              --  0 =  SMPTE 296M - 720p   50Hz              
            when "001" =>  tx_bitrate <= tx_bitrate_sel; --  1 =  SMPTE 274M - 1080sF 24Hz & 23.98Hz    
            when "010" =>  tx_bitrate <= tx_bitrate_sel; --  2 =  SMPTE 274M - 1080i  30Hz & 29.97 Hz   
            when "011" =>  tx_bitrate <= '0';              --  3 =  SMPTE 274M - 1080i  25Hz              
            when "100" =>  tx_bitrate <= tx_bitrate_sel; --  4 =  SMPTE 274M - 1080p  30Hz & 29.97Hz    
            when "101" =>  tx_bitrate <= '0';              --  5 =  SMPTE 274M - 1080p  25Hz              
            when "110" =>  tx_bitrate <= tx_bitrate_sel; --  6 =  SMPTE 274M - 1080p  24Hz & 23.98Hz    
            when others => tx_bitrate <= tx_bitrate_sel; --  7 =  SMPTE 296M - 720p   60Hz & 59.94Hz    
          end case;      
      end case;
  end if;
end process;

-- debounce push buttons
  push_button_bus <= GPIO_BUTTON3 & GPIO_BUTTON2 & GPIO_BUTTON1 & GPIO_BUTTON0;
process (clk_27M_in) begin
  if rising_edge(clk_27M_in) then
    push_button_count <= push_button_count + '1';
    if (push_button_count = "0000000000") then
      push_button_1 <= push_button_bus;
      push_button_2 <= push_button_1;
      push_button_3 <= push_button_2;
      push_button_4 <= push_button_3;
      push_button_5 <= push_button_4;
      push_button_6 <= push_button_5;
      push_button_7 <= push_button_6;
      push_button_8 <= push_button_7;
      push_button_stretch <= push_button_8 and push_button_7 and push_button_6  
      and push_button_5 and push_button_4  and push_button_3 and push_button_2 
      and push_button_1 and push_button_bus;
    end if;
  end if;
end process;
 


-- Test patterns are cycled through by push button 0
  -- synchroninze push button
  sync_one_shot_pb0 : sync_one_shot 
  port map(	
    clk   => txpipeclk,
    rst   => '0',
    ce    => tx_ce(0),
    in1   => push_button_stretch(0),
    out1  => push_button_sync(0)
  );
  
  
  -- cycle through test patterns
process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    if (tx_ce(0) = '1')  then
      if (push_button_sync(0)= '1')  then
        if(tx_pattern_sel = "10") then
          tx_pattern_sel <= "00";
        else
          tx_pattern_sel <= tx_pattern_sel + '1';
        end if;
      end if;
    end if;
  end if;
end process; 
     
-- Line standards are cycled through by push button 2
  -- synchroninze push button
  sync_one_shot_pb1 : sync_one_shot 
  port map(	
    clk   => txpipeclk,
    rst   => '0',
    ce    => tx_ce(0),
    in1   => push_button_stretch(2),
    out1  => push_button_sync(2)
  );
  
  
  -- cycle through test patterns
process (txpipeclk) begin
  if rising_edge(txpipeclk) then
    if (tx_ce(0) = '1') then
      if (push_button_sync(2) = '1') then
        tx_format_sel <= tx_format_sel + 1;
      end if;
    end if;
  end if;
end process;
      
 gtp_reset_in <= CPU_RESET;      

VIDGEN1 : multigenHD  
port map(
    clk        => txpipeclk,
    rst        => tx1_fabric_reset,
    ce         => vidgen_enable,
    std        => tx_format,
    pattern    => tx_pattern_sel,
    user_opt   => "00",
      
    y          => hdgen_y,
    c          => hdgen_c,
    h_blank    => open,
    v_blank    => open,    
    field      => open,
    trs        => open,
    xyz        => open,
    line_num   => hdgen_ln);

--
-- SD video pattern generators
--
NTSC : vidgen_ntsc  
port map(
    clk_a      => txpipeclk,
    rst_a      => '0',
    ce_a       => sd_ce,
    pattern_a  => tx_pattern_sel(0),
    q_a        => sd_ntsc_vid,
    h_sync_a   => open,
    v_sync_a   => open,
    field_a    => open,
    clk_b      => '0',
    rst_b      => '0',
    ce_b       => '0',
    pattern_b  => '0',
    q_b        => open,
    h_sync_b   => open,
    v_sync_b   => open,
    field_b    => open
  );
ntsc_patgen <= xavb_10b_vcomp_type(sd_ntsc_vid);

 PAL : vidgen_pal 
 port map(
    clk_a      => txpipeclk,
    rst_a      => '0',
    ce_a       => sd_ce,
    pattern_a  => tx_pattern_sel(0),
    q_a        => sd_pal_vid,
    h_sync_a   => open,
    v_sync_a   => open,
    field_a    => open,
    clk_b      => '0',
    rst_b      => '0',
    ce_b       => '0',
    pattern_b  => '0',
    q_b        => open,
    h_sync_b   => open,
    v_sync_b   => open,
    field_b    => open
  );
pal_patgen <= xavb_10b_vcomp_type(sd_pal_vid);

--
-- Video pattern generator output muxes.
--
  sd_patgen <=  pal_patgen when tx_format_sel(0) = '1' else ntsc_patgen;
  tx_y_in <= sd_patgen when tx_mode_SD = '1' else hdgen_y;
  tx_c_in <= hdgen_c;

--
-- Generate the SMPTE 352M VPID byte 2 for 3G-SDI based on the tx_format and
-- bit rate.
--
process (tx_format(0), tx_bitrate_8) begin
    if (tx_format(0)= '1')  then
        tx_vpid_byte2 <= X"C9";      -- 50 Hz
    elsif (tx_bitrate_8 = '1')  then
        tx_vpid_byte2 <= X"CA";      -- 60 Hz
    else
        tx_vpid_byte2 <= X"CB";      -- 59.94 Hz
    end if;
end process;
--
-- Triple-rate SDI SMPTE 352M VPID packet insertion.
--
-- In this demo, VPID packets are only inserted for 3G-SDI mode.
--
VPIDINS : triple_sdi_vpid_insert  
  port map(
    clk             => txpipeclk,
    ce              => tx_ce(1),
    din_rdy         => '1',
    rst             => tx1_fabric_reset,
    sdi_mode        => tx_sdimode_sel_3,
    level           => '0',             -- always level A
    enable          => tx_mode_3G,      -- enabled on 3G only
    overwrite       => '1',
    byte1           => X"89",            -- 1080-line 3G-SDI level A
    byte2           => tx_vpid_byte2,
    byte3           => X"00",
    byte4a          => X"09",
    byte4b          => X"09",
    ln_a            => hdgen_ln,
    ln_b            => hdgen_ln,
    line_f1         => std_logic_vector(to_unsigned(10,  11)),
    line_f2         => std_logic_vector(to_unsigned(572, 11)),
    line_f2_en      => '0',
    a_y_in          => tx_y_in,
    a_c_in          => tx_c_in,
    b_y_in          => "0000000000",
    b_c_in          => "0000000000",
    ds1a_out        => tx_ds1a,
    ds2a_out        => tx_ds2a,
    ds1b_out        => tx_ds1b,
    ds2b_out        => tx_ds2b,
    eav_out         => tx_eav,
    sav_out         => tx_sav,
    out_mode        => tx_out_mode
  );

-- Triple-rate SDI Tx output module.
TXOUTPUT : triple_sdi_tx_output_20b 
  port map (
    clk             => txpipeclk,
    ce              => tx_ce(3 downto 2),
    din_rdy         => '1',
    rst             => tx1_fabric_reset,
    mode            => tx_out_mode,
    ds1a            => tx_ds1a,
    ds2a            => tx_ds2a,
    ds1b            => tx_ds1b,
    ds2b            => tx_ds2b,
    insert_crc      => '1',
    insert_ln       => '1',
    insert_edh      => '1',
    ln_a            => hdgen_ln,
    ln_b            => hdgen_ln,
    eav             => tx_eav,
    sav             => tx_sav,
    txdata          => txdata1_20,
    ce_align_err    => open);

------------------------------------------------------------------------------
-- SDI Receiver 
----------------------------------------------------------------------------/
--
-- Triple-rate SDI RX data path
--

SDIRX1 : s6_sdi_rx_light_20b 
  generic map(
    NUM_SD_CE               => RX1_NUM_CE,
    NUM_3G_DRDY             => RX1_NUM_B_DRDY
  )
  port map(
    clk                     => rxpipeclk,
    rst                     => rx1_fabric_reset,
    data_in                 => rxdata1_20,
    frame_en                => '1',
    mode                    => rx1_mode,
    mode_HD                 => rx1_mode_HD,
    mode_SD                 => rx1_mode_SD,
    mode_3G                 => rx1_mode_3G,
    mode_locked             => rx1_mode_locked,
    rx_locked               => rx1_locked,
    t_format                => rx1_format,
    level_b_3G              => rx1_level_b,
    ce_sd                   => rx1_ce,
    nsp                     => open,
    ln_a                    => rx1_ln_a,
    a_vpid                  => rx1_a_vpid,
    a_vpid_valid            => rx1_a_vpid_valid,
    b_vpid                  => rx1_b_vpid,
    b_vpid_valid            => rx1_b_vpid_valid,
    crc_err_a               => rx1_crc_err,
    ds1_a                   => rx1_a_y,
    ds2_a                   => rx1_a_c,
    eav                     => rx1_eav,
    sav                     => rx1_sav,
    trs                     => rx1_trs,
    ln_b                    => rx1_ln_b,
    dout_rdy_3G             => rx1_lvlb_drdy,
    crc_err_b               => rx1_crc_err2,
    ds1_b                   => rx1_b_y,
    ds2_b                   => rx1_b_c);

--    
-- EDH processor to check for SD-SDI errors.
--
EDH : edh_processor 
port map (
    clk                     => rxpipeclk,
    ce                      => rx1_ce(1),
    rst                     => rx1_fabric_reset,
    vid_in                  => video_type(rx1_a_y),
    reacquire               => '0',
    en_sync_switch          => '1',
    en_trs_blank            => '0',
    anc_idh_local           => '0',
    anc_ues_local           => '0',
    ap_idh_local            => '0',
    ff_idh_local            => '0',
    errcnt_flg_en           => B"0_00001_00001_00000",
    clr_errcnt              => clr_errs,
    receive_mode            => '1',                   
    vid_out                 => open,
    std                     => std,
    std_locked              => sd_locked,
    trs                     => open,
    field                   => open,
    v_blank                 => open,
    h_blank                 => open,
    horz_count              => open,
    vert_count              => open,
    sync_switch             => open,
    locked                  => open,
    eav_next                => open,
    sav_next                => open,
    xyz_word                => open,
    anc_next                => open,
    edh_next                => open,
    rx_ap_flags             => open,
    rx_ff_flags             => open,
    rx_anc_flags            => open,
    ap_flags                => open,
    ff_flags                => open,
    anc_flags               => open,
    packet_flags            => open,
    errcnt                  => edh_errcnt,
    edh_packet              => open );

edh_err <= '0' when edh_errcnt = X"000000" else '1';

locked <= sd_locked when rx1_mode_SD = '1' else rx1_locked;


--
-- Capture any CRC error and cause an LED to light until cleared by pushing
-- PB3 push button.
--
  clr_errs <=  push_button_stretch(1);

process(rxpipeclk, clr_errs) begin
    if clr_errs = '1' then
        rx1_crc_err_ff <= '0';
    elsif rising_edge(rxpipeclk) then
        if rx1_ce(0) = '1' then
            if rx1_crc_err = '1' or rx1_crc_err2 = '1' then
                rx1_crc_err_ff <= '1';
            end if;
        end if;
    end if;
end process;

rx1_err <= edh_err when rx1_mode_SD = '1' else rx1_crc_err_ff;

------------------------------------------------------------------------------
-- end SDI Receiver code for FMC
------------------------------------------------------------------------------
  process (rxpipeclk) begin
    if rising_edge(rxpipeclk) then
       gpioLED1 <= rx1_mode(0);
       gpioLED2 <= rx1_mode(1);
    end if;
  end process;
--  GPIO LED Controls
  GPIO_LED_0 <= '0' when rx_sd_mode = '1' else rx1_rate;
  GPIO_LED_1 <= gpioLED1;
  GPIO_LED_2 <= gpioLED2;
  GPIO_LED_3 <= rx1_err;
  FPGA_AWAKE <= locked;

--__________________________ CS ___________________ 
  	

	ila_inst : chipscope_ila
	port map (
    control => control0,
    clk => rxpipeclk,
    trig0 => trig0);		
	
	 icon_inst:  chipscope_icon
    port map (
      control0 => control0 -- inout bus [35:0] 
    );
  
	trig0(63 downto 44) <= 	rxdata1_20;
	trig0(43 downto 24) <= 	txdata1_20;
	trig0(23) <= 	gtpclkout1_1_pll_locked;
	trig0(22) <= 	gtpclkout1_0_pll_locked;
	trig0(21) <= 	tx_mode_HD;
	trig0(20) <= 	tx_mode_SD;
	trig0(19) <= 	tx_mode_3G;
	trig0(18) <= 	rx1_mode_HD;
	trig0(17) <= 	rx1_mode_SD;
	trig0(16) <= 	rx1_mode_3G;
	trig0(15) <= 	rx1_mode_locked;
	trig0(14 downto 12) <= 	tx_format;
	trig0(11 downto 10) <= 	tx_pattern_sel;
	
	
     
 end architecture;



