-- **************************************************************--
-- Module Name: Osc9.v 
-- Description : freq  ~ 50 Mhz 
-- **************************************************************--
library ieee;
use ieee.std_logic_1164.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

 entity Osc9 is 
   port( 
     out_clk:  out std_logic; 
     enable:   in  std_logic
    );
end Osc9;

architecture Osc9_arch of Osc9 is
   
   signal invout1 : std_logic;
   signal invout2 : std_logic;
   signal invout3 : std_logic;
   signal invout4 : std_logic;
   signal invout5 : std_logic;
   signal invout6 : std_logic;
   signal invout7 : std_logic;
   signal invout8 : std_logic;
   
   signal out_clk_temp: std_logic;
   signal one: std_logic := '1'; 

component XORCY
 port(
   CI : in   std_logic;
   LI:  in   std_logic;
   O:   out  std_logic
 );   
 end component;
   
begin   

   Ixor1 : XORCY port map( CI => out_clk_temp, LI => enable, O => invout1);
   Iinv2 : XORCY port map( CI => invout1, LI => one, O => invout2);
   Iinv3 : XORCY port map( CI => invout2, LI => one, O => invout3);
   Iinv4 : XORCY port map( CI => invout3, LI => one, O => invout4);
   Iinv5 : XORCY port map( CI => invout4, LI => one, O => invout5);
   Iinv6 : XORCY port map( CI => invout5, LI => one, O => invout6);
   Iinv7 : XORCY port map( CI => invout6, LI => one, O => invout7);
   Iinv8 : XORCY port map( CI => invout7, LI => one, O => invout8);
   Iinv9 : XORCY port map( CI => invout8, LI => one, O => out_clk_temp);
   
   out_clk <= out_clk_temp;
   

--synthesis attribute LOC of Ixor1 is SLICE_X0Y10;
--synthesis attribute LOC of Iinv2 is SLICE_X0Y11;
--synthesis attribute LOC of Iinv3 is SLICE_X0Y12;
--synthesis attribute LOC of Iinv4 is SLICE_X0Y13;
--synthesis attribute LOC of Iinv5 is SLICE_X0Y14;
--synthesis attribute LOC of Iinv6 is SLICE_X0Y15;
--synthesis attribute LOC of Iinv7 is SLICE_X0Y16;
--synthesis attribute LOC of Iinv8 is SLICE_X0Y17;
--synthesis attribute LOC of Iinv9 is SLICE_X0Y18;


 end Osc9_arch;
