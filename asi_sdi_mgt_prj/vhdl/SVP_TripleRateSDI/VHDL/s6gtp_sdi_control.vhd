-------------------------------------------------------------------------------- 
-- Copyright (c) 2009 Xilinx, Inc. 
-- All Rights Reserved 
-------------------------------------------------------------------------------- 
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /   Vendor: Xilinx 
-- \   \   \/    Author: Reed P. Tidwell
--  \   \        Filename: $RCSfile: s6gtp_sdi_control.vhd,v $
--  /   /        Date Last Modified:  $Date: 2010-01-05 10:42:54-07 $
-- /___/   /\    Date Created: December 30, 2009
-- \   \  /  \ 
--  \___\/\___\ 
-- 
--
-- Revision History: 
-- $Log: s6gtp_sdi_control.vhd,v $
-- Revision 1.0  2010-01-05 10:42:54-07  reedt
-- Working version.  Initial Checkin.
--
-------------------------------------------------------------------------------- 
-- This file contains confidential and proprietary information of Xilinx, Inc. 
-- and is protected under U.S. and international copyright and other 
-- intellectual property laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any rights to the 
-- materials distributed herewith. Except as otherwise provided in a valid 
-- license issued to you by Xilinx, and to the maximum extent permitted by 
-- applicable law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL 
-- FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, 
-- IMPLIED, OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF 
-- MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; 
-- and (2) Xilinx shall not be liable (whether in contract or tort, including 
-- negligence, or under any other theory of liability) for any loss or damage of 
-- any kind or nature related to, arising under or in connection with these 
-- materials, including for any direct, or any indirect, special, incidental, 
-- or consequential loss or damage (including loss of data, profits, goodwill, 
-- or any type of loss or damage suffered as a result of any action brought by 
-- a third party) even if such damage or loss was reasonably foreseeable or 
-- Xilinx had been advised of the possibility of the same.
-- 
-- CRITICAL APPLICATIONS
--   Xilinx products are not designed or intended to be fail-safe, or for use in 
--   any application requiring fail-safe performance, such as life-support or 
--   safety devices or systems, Class III medical devices, nuclear facilities, 
--   applications related to the deployment of airbags, or any other applications 
--   that could lead to death, personal injury, or severe property or 
--   environmental damage (individually and collectively, "Critical Applications").
--   Customer assumes the sole risk and liability of any use of Xilinx products in
--   Critical Applications, subject only to applicable laws and regulations 
--   governing limitations on product liability. 
--
--   THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT
--   ALL TIMES.

-------------------------------------------------------------------------------- 
--
-- Module Description:
--
-- This module provides several functions required when using the Spartan-6 GTP
-- to receiver and/or transmit the SMPTE SDI protocols: SD-SDI, HD-SDI, and
-- 3G-SDI. The functions provided are;
-- 
-- 1. A DRP controller that controls the mode of operation and the selection 
-- and routing of the GTP's reference clocks.
-- 
-- 2. Reset logic.
-- 
-- 3. Rx bit rate detection.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity s6gtp_sdi_control is
generic (
    --
    -- The GTP_RESET_CNTR_SIZE parameter specifies the size (number of bits) of 
    -- the GTP reset delay counter. This counter is reset to 0 when gtp_reset_in
    -- is 1. When gtp_reset_in goes low, the counter will be counting and the 
    -- gtpreset output signal will be 1 until the terminal count is reached. The
    -- terminal  count occurs when the MSB of the counter becomes 1. The reset 
    -- counter is clocked by the dclk clock input, so the delay count is dclk 
    -- frequency / (2^GTP_RESET_CNTR_SIZE). The CBL_DISCONNECT_RESET_CNTR_SIZE 
    -- parameter specifies the size of the cable disconnect reset duration 
    -- counter in terms of DCLK cycles.
    --
    GTP_RESET_CNTR_SIZE :           integer := 17;
    CBL_DISCONNECT_RST_CNTR_SIZE :  integer := 12;

    --
    -- This group of parameters specifies the PMA_RX_CFG to be used for each of
    -- the three SDI modes of operation: HD, SD, and 3G. Normally, the default
    -- values given here should be used.
    --
    PMA_RX_CFG_HD :                 std_logic_vector(27 downto 0) := X"05ce059";
    PMA_RX_CFG_SD :                 std_logic_vector(27 downto 0) := X"05ce000";
    PMA_RX_CFG_3G :                 std_logic_vector(27 downto 0) := X"05ce089";

    --
    -- This group of parameters specifies the RXDIVSEL_OUT divider value to be
    -- used for each of the three SDI modes of operation. The default values
    -- given here should normally be used. They default to divide by 2 for HD
    -- (1.485 Gb/s line rate. 148.5 refclk) ,  SD (1.485 Gb/s line rate,
    -- 27MHz MHz refclk) and 3G (2.97 Gb/s line rate, 297 MHz refclk).
    --  
    PLL_RXDIVSEL_OUT_HD :           std_logic_vector(1 downto 0) := "01";
    PLL_RXDIVSEL_OUT_SD :           std_logic_vector(1 downto 0) := "01";
    PLL_RXDIVSEL_OUT_3G :           std_logic_vector(1 downto 0) := "00";

    --
    -- This group of parameters specifies the TXDIVSEL_OUT divider value to be
    -- used for each of the three SDI modes of operation. The default value
    -- given here should normally be used. The default for all line rates is 
    -- divide by 1.  This results in a 2.97 Gb/s line rate with a 148.5 MHz 
    -- refclk.  This is the normal 3G rate.   HD is sent in an oversampled 
    -- fashion with 2X oversampling (1.485 Gb/s data rete).  SD is oversampled 
    -- at 11X (270 MHz data rate).

    PLL_TXDIVSEL_OUT_HD :           std_logic_vector(1 downto 0) := "01";
    PLL_TXDIVSEL_OUT_SD :           std_logic_vector(1 downto 0) := "00";
    PLL_TXDIVSEL_OUT_3G :           std_logic_vector(1 downto 0) := "00";

    -- 
    -- This parameter specifies the frequency of the reference clock used to
    -- detect the HD and 3G bit rates. The default value is 148.5 MHz.
    --
    RATE_REFCLK_FREQ :              integer := 33333333);

port (
    dclk :              in  std_logic;                      -- DRP clock
    rate_refclk:        in  std_logic;                      -- rate detection reference clock
    rx0_usrclk :        in  std_logic;                      -- RXUSRCLK for RX0
    rx1_usrclk :        in  std_logic;                      -- RXUSRCLK for RX1
    tx0_usrclk :        in  std_logic;                      -- TXUSRCLK for TX0
    tx1_usrclk :        in  std_logic;                      -- TXUSRCLK for TX1
    drst :              in  std_logic;                      -- async reset for DRP controller

--
-- These inputs control the reference clock selection and reference clock 
-- routing through the GTP.
--
    rx0_mode :          in  std_logic_vector(1 downto 0);   -- RX0 mode: 00=HD, 01=SD, 10=3G
    rx1_mode :          in  std_logic_vector(1 downto 0);   -- RX1 mode: 00=HD, 01=SD, 10=3G
    tx0_mode :          in  std_logic_vector(1 downto 0);   -- TX0 mode: 00=HD, 01=SD, 10=3G
    tx1_mode :          in  std_logic_vector(1 downto 0);   -- TX1 mode: 00=HD, 01=SD, 10=3G

    tx0_slew :          out std_logic;                      -- controls slew rate of TX0 cable driver
    tx1_slew :          out std_logic;                      -- controls slew rate of Tx1 cable driver

-- 
-- Rate detection outputs
--
    rx0_rate :          out std_logic;                      -- 1 = /1.001 rate
    rx1_rate :          out std_logic;                      -- 1 = /1.001 rate

-- 
-- These signals are for the reset logic.
--
    -- Entire GTP reset signals
    gtp_reset0_in:      in  std_logic;                      -- reset the entire GTP tile
    gtp_reset1_in:      in  std_logic;                      -- reset the entire GTP tile
    clocks_stable :     in  std_logic;                      -- 1 when reference clocks are stable

    -- Rx0 reset signals
    rx0_pcs_reset :     in  std_logic;                      -- 1 = reset RX0 PCS
    rx0_cdr_reset :     in  std_logic;                      -- 1 = reset RX0 CDR & PCS
    rx0_fabric_reset :  out std_logic;                      -- reset synchronous with rx0_usrclk

    -- Rx1 reset signals
    rx1_pcs_reset :     in  std_logic;                      -- 1 = reset RX1 PCS
    rx1_cdr_reset :     in  std_logic;                      -- 1 = reset RX1 CDR & PCS
    rx1_fabric_reset :  out std_logic;                      -- reset synchronous with rx1_usrclk

    -- Tx0 reset signals
    tx0_reset :         in  std_logic;                      -- 1 = reset TX0
    tx0_fabric_reset :  out std_logic;                      -- reset synchronous with tx0_usrclk

    -- Tx1 reset signals
    tx1_reset :         in  std_logic;                      -- 1 = reset TX1
    tx1_fabric_reset :  out std_logic;                      -- reset synchronous with tx1_usrclk
   
--
-- These signals must be connected to the GTP wrapper module created by the
-- RocketIO wizard.
--
    daddr :             out std_logic_vector(7 downto 0);   -- connect to DADDR port on GTP
    den :               out std_logic;                      -- connect to DEN port on GTP
    di :                out std_logic_vector(15 downto 0);  -- connect to DI port on GTP
    drpo :                in  std_logic_vector(15 downto 0);  -- connect to DO port on GTP
    drdy :              in  std_logic;                      -- connect to DRDY port on GTP
    dwe :               out std_logic;                      -- connect to DWEN port on GTP

    txbufstatus0_b1 :   in  std_logic;                      -- connect to bit 1 of TXBUFSTATUS0
    txbufstatus1_b1 :   in  std_logic;                      -- connect to bit 1 of TXBUFSTATUS1
    rxbufstatus0_b2 :   in  std_logic;                      -- connect to bit 2 of RXBUFSTATUS0
    rxbufstatus1_b2 :   in  std_logic;                      -- connect to bit 2 of RXBUFSTATUS1

    gtpreset0 :          out std_logic;                      -- connect to GTPRESET0 input input of GTP
    gtpreset1 :          out std_logic;                      -- connect to GTPRESET1 input input of GTP
    
    resetdone0 :        in  std_logic;                      -- connect to RESETDONE0 of GTP
    rxreset0 :          out std_logic;                      -- connect to RXRESET0 of GTP
    rxbufreset0 :       out std_logic;                      -- connect to RXBUFRESET0 of GTP
    rxcdrreset0 :       out std_logic;                      -- connect to RXCDRRESET0 of GTP

    resetdone1 :        in  std_logic;                      -- connect to RESETDONE1 of GTP
    rxreset1 :          out std_logic;                      -- connect to RXRESET1 of GTP
    rxbufreset1 :       out std_logic;                      -- connect to RXBUFRESET1 of GTP
    rxcdrreset1 :       out std_logic;                      -- connect to RXCDRRESET1 of GTP

    txreset0 :          out std_logic;                      -- connect to TXRESET0 of GTP
    txreset1 :          out std_logic);                     -- connect to TXRESET1 of GTP

end s6gtp_sdi_control;

architecture xilinx of s6gtp_sdi_control is

constant GTPRST_CTR_MSB  : integer := GTP_RESET_CNTR_SIZE - 1;
subtype  GTPRST_CTR_TYPE is std_logic_vector(GTPRST_CTR_MSB downto 0);

--
-- Internal signals
--
signal drst_sync :          std_logic_vector(1 downto 0);   --synchronized rst input to dclk

signal gtprst0_cntr :        GTPRST_CTR_TYPE;                -- GTP reset counter
signal gtprst0_cntr_tc :     std_logic;                      -- GTP reset counter terminal count
signal gtprst0_ff :          std_logic := '1';               -- internal version of gtpreset out
signal gtprst1_cntr :        GTPRST_CTR_TYPE;                -- GTP reset counter
signal gtprst1_cntr_tc :     std_logic;                      -- GTP reset counter terminal count
signal gtprst1_ff :          std_logic := '1';               -- internal version of gtpreset out

signal rx0_rate_detect_std: std_logic_vector(1 downto 0) := (others => '0');
signal rx0_rate_chg_det :   std_logic;
signal rx0_rate_change :    std_logic;
signal rx0_mode_change :    std_logic;

signal rx1_rate_detect_std: std_logic_vector(1 downto 0) := (others => '0');
signal rx1_rate_chg_det :   std_logic;
signal rx1_rate_change :    std_logic;
signal rx1_mode_change :    std_logic;

signal txreset0_int :       std_logic;
signal tx0_rst_sync_reg :   std_logic := '0';
signal tx0_fabric_reset_int:std_logic := '0';
signal tx0_mode_change :    std_logic;

signal txreset1_int :       std_logic;
signal tx1_rst_sync_reg :   std_logic := '0';
signal tx1_fabric_reset_int:std_logic := '0';
signal tx1_mode_change :    std_logic;

signal gtpreset0_in:         std_logic;
signal gtpreset1_in:         std_logic;
signal zero:                 std_logic := '0';

component s6gtp_sdi_drp_control
generic (
    PMA_RX_CFG_HD:          std_logic_vector(27 downto 0) := X"05ce059";
    PMA_RX_CFG_SD:          std_logic_vector(27 downto 0) := X"05ce000";
    PMA_RX_CFG_3G:          std_logic_vector(27 downto 0) := X"05ce089";
    
    PLL_RXDIVSEL_OUT_HD:    std_logic_vector(1 downto 0)  := "01"; 
    PLL_RXDIVSEL_OUT_SD:    std_logic_vector(1 downto 0)  := "01"; 
    PLL_RXDIVSEL_OUT_3G:    std_logic_vector(1 downto 0)  := "00"; 

    PLL_TXDIVSEL_OUT_HD:    std_logic_vector(1 downto 0)  := "00"; 
    PLL_TXDIVSEL_OUT_SD:    std_logic_vector(1 downto 0)  := "00"; 
    PLL_TXDIVSEL_OUT_3G:    std_logic_vector(1 downto 0)  := "00");
port (
    clk:                in  std_logic;                      
    rst:                in  std_logic;                      
    tx0_mode:           in  std_logic_vector(1 downto 0);   
    tx1_mode:           in  std_logic_vector(1 downto 0);   
    rx0_mode:           in  std_logic_vector(1 downto 0);   
    rx1_mode:           in  std_logic_vector(1 downto 0);   
    do:                 in  std_logic_vector(15 downto 0);  
    drdy:               in  std_logic;                      
    daddr:              out std_logic_vector(7 downto 0)    
                            := (others => '0');
    di:                 out std_logic_vector(15 downto 0);  
    den:                out std_logic;                      
    dwe:                out std_logic;                      
    tx0_mode_change:    out std_logic;                      
    tx1_mode_change:    out std_logic;                      
    rx0_mode_change:    out std_logic;                      
    rx1_mode_change:    out std_logic);                     
end component;

component s6gtp_sdi_rx_reset
generic (
    CBL_DISCONNECT_RST_CNTR_SIZE : integer := 12);
port (
    free_clk:           in  std_logic;            
    usr_clk:            in  std_logic;            
    gtp_reset:          in  std_logic;            
    pcs_reset:          in  std_logic;            
    cdr_reset:          in  std_logic;            
    eq_cd_n:            in  std_logic;            
    rate_change:        in  std_logic;            
    mode_change:        in  std_logic;            
    buffer_error:       in  std_logic;            
    reset_done:         in  std_logic;            
    gtp_rx_reset:       out std_logic;            
    gtp_rx_buf_reset:   out std_logic;            
    gtp_rx_cdr_reset:   out std_logic;            
    fabric_reset:       out std_logic;            
    clk_mux_sel:        out std_logic);           
end component;

component s6gtp_sdi_rate_detect
  generic (

           REFCLK_FREQ:  integer:= 33333333); 

  port (
        rst:        in  std_logic;
        refclk:     in  std_logic;
        recvclk:    in  std_logic;
        std:        in  std_logic;         
        enable:     in  std_logic;         
                                           
        reset_out:  out std_logic;
        drift:      out std_logic := '0';  
                                           
        rate:       out std_logic);

end component;

begin

--------------------------------------------------------------------------------
-- DRP controller
--
-- The DRP controller dynamically changes the GTP attributes for reference
-- clock selection, CDR mode, and PLL dividers to change the clock routing
-- and operating mode of the GTP Rx and Tx units.
--
process(dclk)
begin
    if rising_edge(dclk) then
        drst_sync <= (drst_sync(0) & drst);
    end if;
end process;

DRPCTRL : s6gtp_sdi_drp_control
generic map (
    PMA_RX_CFG_HD           => PMA_RX_CFG_HD,
    PMA_RX_CFG_SD           => PMA_RX_CFG_SD,
    PMA_RX_CFG_3G           => PMA_RX_CFG_3G,

    PLL_RXDIVSEL_OUT_HD     => PLL_RXDIVSEL_OUT_HD,
    PLL_RXDIVSEL_OUT_SD     => PLL_RXDIVSEL_OUT_SD,
    PLL_RXDIVSEL_OUT_3G     => PLL_RXDIVSEL_OUT_3G,

    PLL_TXDIVSEL_OUT_HD     => PLL_TXDIVSEL_OUT_HD,
    PLL_TXDIVSEL_OUT_SD     => PLL_TXDIVSEL_OUT_SD,
    PLL_TXDIVSEL_OUT_3G     => PLL_TXDIVSEL_OUT_3G)

port map (
    clk                     => dclk,
    rst                     => drst_sync(1),
    tx0_mode                => tx0_mode,
    tx1_mode                => tx1_mode,
    rx0_mode                => rx0_mode,
    rx1_mode                => rx1_mode,
    do                      => drpo,
    drdy                    => drdy,
    daddr                   => daddr,
    di                      => di,
    den                     => den,
    dwe                     => dwe,
    tx0_mode_change         => tx0_mode_change,
    tx1_mode_change         => tx1_mode_change,
    rx0_mode_change         => rx0_mode_change,
    rx1_mode_change         => rx1_mode_change);

--
-- The cable driver slew rate control is determined by the Tx mode.
--
tx0_slew <= tx0_mode(0);
tx1_slew <= tx1_mode(0);

--------------------------------------------------------------------------------
-- GTP reset circuit
--
-- Hold the entire GTP in reset for some period of time until all clocks are
-- stable.
--

-- gtpreset0
process(dclk, gtp_reset0_in)
begin
    if gtp_reset0_in = '1' then
        gtprst0_cntr <= (others => '0');
    elsif rising_edge(dclk) then
        if gtprst0_cntr_tc = '0' then
            gtprst0_cntr <= gtprst0_cntr + 1;
        end if;
    end if;
end process;

gtprst0_cntr_tc <= gtprst0_cntr(GTPRST_CTR_MSB);

process(dclk, gtp_reset0_in)
begin
    if gtp_reset0_in = '1' then
        gtprst0_ff <= '1';
    elsif rising_edge(dclk) then
        if gtprst0_cntr_tc = '1' then
            gtprst0_ff <= '0';
        end if;
    end if;
end process;

--gtpreset0_in <= gtprst0_ff or (not clocks_stable);
gtpreset0_in <= gtp_reset0_in; --Only for simulation
gtpreset0    <= gtpreset0_in;

-- gtpreset1
process(dclk, gtp_reset1_in)
begin
    if gtp_reset1_in = '1' then
        gtprst1_cntr <= (others => '0');
    elsif rising_edge(dclk) then
        if gtprst1_cntr_tc = '0' then
            gtprst1_cntr <= gtprst1_cntr + 1;
        end if;
    end if;
end process;

gtprst1_cntr_tc <= gtprst1_cntr(GTPRST_CTR_MSB);

process(dclk, gtp_reset1_in)
begin
    if gtp_reset1_in = '1' then
        gtprst1_ff <= '1';
    elsif rising_edge(dclk) then
        if gtprst1_cntr_tc = '1' then
            gtprst1_ff <= '0';
        end if;
    end if;
end process;

--gtpreset1_in <= gtprst1_ff or (not clocks_stable);
gtpreset1_in <= gtp_reset1_in; --Only for simulation
gtpreset1    <= gtpreset1_in;

--
-- Reset logic for Rx0
--
process (rx0_mode, rx1_mode, rx0_rate_chg_det, rx1_rate_chg_det)
begin
  if rx0_mode = "01" then
    rx0_rate_change <= '0';
  else
    rx0_rate_change <= rx0_rate_chg_det;
  end if;
  if rx1_mode = "01" then
    rx1_rate_change <= '0';
  else
    rx1_rate_change <= rx1_rate_chg_det;
  end if;
end process;

RX0RST : s6gtp_sdi_rx_reset
generic map (
    CBL_DISCONNECT_RST_CNTR_SIZE => CBL_DISCONNECT_RST_CNTR_SIZE)
port map (
    free_clk            => dclk,
    usr_clk             => rx0_usrclk,
    gtp_reset           => gtpreset0_in,
    pcs_reset           => rx0_pcs_reset,
    cdr_reset           => rx0_cdr_reset,
    eq_cd_n             => zero,
    rate_change         => rx0_rate_change,
    mode_change         => rx0_mode_change,
    buffer_error        => rxbufstatus0_b2,
    reset_done          => resetdone0,
    gtp_rx_reset        => rxreset0,
    gtp_rx_buf_reset    => rxbufreset0,
    gtp_rx_cdr_reset    => rxcdrreset0,
    fabric_reset        => rx0_fabric_reset,
    clk_mux_sel         => open);

--
-- Reset logic for Rx1
--
RX1RST : s6gtp_sdi_rx_reset
generic map (
    CBL_DISCONNECT_RST_CNTR_SIZE => CBL_DISCONNECT_RST_CNTR_SIZE)
port map (
    free_clk            => dclk,
    usr_clk             => rx1_usrclk,
    gtp_reset           => gtpreset1_in,
    pcs_reset           => rx1_pcs_reset,
    cdr_reset           => rx1_cdr_reset,
    eq_cd_n             => zero,
    rate_change         => rx1_rate_change,
    mode_change         => rx1_mode_change,
    buffer_error        => rxbufstatus1_b2,
    reset_done          => resetdone1,
    gtp_rx_reset        => rxreset1,
    gtp_rx_buf_reset    => rxbufreset1,
    gtp_rx_cdr_reset    => rxcdrreset1,
    fabric_reset        => rx1_fabric_reset,
    clk_mux_sel         => open);

--------------------------------------------------------------------------------
-- Reset logic for Tx0
--
-- The transmitter is reset on a mode change, when a Tx buffer under/over run
-- occurs, or when the tx0_reset input to this module is asserted. This reset
-- resets the PCS section of the GTP Tx. Also asserted at the same time is
-- the tx0_fabric_reset to reset the fabric portion of the transmitter.
--
txreset0_int <= tx0_reset or tx0_mode_change or txbufstatus0_b1;

process(tx0_usrclk, txreset0_int)
begin
    if txreset0_int = '1' then
        tx0_rst_sync_reg <= '1';
        tx0_fabric_reset_int <= '1';
    elsif rising_edge(tx0_usrclk) then
        tx0_rst_sync_reg <= txreset0_int;
        tx0_fabric_reset_int <= tx0_rst_sync_reg;
    end if;
end process;

txreset0 <= txreset0_int;
tx0_fabric_reset <= tx0_fabric_reset_int;

-- -----------------------------------------------------------------------------
-- Reset logic for Tx1
--
-- The transmitter is reset on a mode change, when a Tx buffer under/over run
-- occurs, or when the tx1_reset input to this module is asserted. This reset
-- resets the PCS section of the GTP Tx. Also asserted at the same time is
-- the tx1_fabric_reset to reset the fabric portion of the transmitter.
--
txreset1_int <= tx1_reset or tx1_mode_change or txbufstatus1_b1;

process(tx1_usrclk, txreset1_int)
begin
    if txreset1_int = '1' then
        tx1_rst_sync_reg <= '1';
        tx1_fabric_reset_int <= '1';
    elsif rising_edge(tx1_usrclk) then
        tx1_rst_sync_reg <= txreset1_int;
        tx1_fabric_reset_int <= tx1_rst_sync_reg;
    end if;
end process;

txreset1 <= txreset1_int;
tx1_fabric_reset <= tx1_fabric_reset_int;


--------------------------------------------------------------------------------
-- Rate detection for Rx0
--


process(rx0_usrclk)
begin
    if rising_edge(rx0_usrclk) then
        rx0_rate_detect_std <= (rx0_rate_detect_std(0) & (rx0_mode(1) or rx0_mode(0)));
    end if;
end process;

RATE0 : s6gtp_sdi_rate_detect
generic map (
    REFCLK_FREQ => RATE_REFCLK_FREQ)
port map (
    rst         => '0',
    refclk      => rate_refclk,
    recvclk     => rx0_usrclk,
    std         => rx0_rate_detect_std(1),
    enable      => resetdone0,
    reset_out   => rx0_rate_chg_det,
    drift       => open,
    rate        => rx0_rate);


--------------------------------------------------------------------------------
-- Rate detection for Rx1
--
process(rx1_usrclk)
begin
    if rising_edge(rx1_usrclk) then
        rx1_rate_detect_std <= (rx1_rate_detect_std(0) & (rx1_mode(1) or rx1_mode(0)));
    end if;
end process;

RATE1 : s6gtp_sdi_rate_detect
generic map (
    REFCLK_FREQ => RATE_REFCLK_FREQ)
port map (
    rst         => '0',
    refclk      => rate_refclk,
    recvclk     => rx1_usrclk,
    std         => rx1_rate_detect_std(1),
    enable      => resetdone1,
    reset_out   => rx1_rate_chg_det,
    drift       => open,
    rate        => rx1_rate);

end xilinx;
