library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all;
USE ieee.std_logic_unsigned.all;
use work.utils_uart_pkg.all;

entity USER_UART is 
	generic (
		ClkFreq				: INTEGER:=40000000;	-- clock freq (Hz)
		BaudRate			: INTEGER:=9600;		-- bps
		bit8				: std_logic:='1';		-- byte = '1' 
		parity_en			: std_logic:='1';		-- enable = '1' 
		odd_n_even			: std_logic:='1'		-- odd = '1' even = '0'
	);
	port (
		n_RESET        		: in        std_logic;
		sys_clk            	: in        std_logic;
		-- i/f CORE IP
		RXDATA         		: in        std_logic;
		TXDATA         		: out       std_logic;
		-- i/f MASTER BUS
		ADDRESS_UART   		:   out		std_logic_vector (15 downto 0); -- Address bus
		RD_DATA_BUS_UART    :   in		std_logic_vector (15 downto 0); -- Input data bus
		WD_DATA_BUS_UART  	:   out		std_logic_vector (15 downto 0); -- Output data bus
		RD_nWD_EN_UART		:   out		std_logic;                      -- Read(active high)/write(active low) enable
		nCS_UART			:	out		std_logic;						-- Operation request(active low)
		ACCESS_VALID_UART	:	in		ACCESS_VALID_UART_type;			-- Access valid acknowledge
		WAIT_UP_ADC_DATA	:	in		std_logic;						-- Ready data in the ADC
		--	error flags
		UART_BIT_I_S		:	out	std_logic;						-- module in invalid state
		UART_BIT_W_D		:	out	std_logic;						-- module is STOPED
		-- debug outputs
		RXRDY_o 			:	out	std_logic;
		TXRDY_o 			:	out	std_logic
	);
end;



architecture bhu of USER_UART is

-- UART baud rate
constant baud_val: std_logic_vector(12 downto 0) := conv_std_logic_vector((ClkFreq/(16*BaudRate))-1,13);

-- timeouts
constant TIME_OUT_MX_UART	: std_logic_vector (15 downto 0) := conv_std_logic_vector(((11/BaudRate)+1)*ClkFreq,16);
constant W_D_MAX_TIME_UART	: std_logic_vector (15 downto 0) := conv_std_logic_vector(((11/BaudRate)+1)*ClkFreq,16);

-------------------------------------------------
-- USERUART component
-------------------------------------------------

component USERUART is

    port( BIT8        : in    std_logic;
          CLK         : in    std_logic;
          CSN         : in    std_logic;
          ODD_N_EVEN  : in    std_logic;
          OEN         : in    std_logic;
          OVERFLOW    : out   std_logic;
          PARITY_EN   : in    std_logic;
          PARITY_ERR  : out   std_logic;
          RESET_N     : in    std_logic;
          RX          : in    std_logic;
          RXRDY       : out   std_logic;
          TX          : out   std_logic;
          TXRDY       : out   std_logic;
          WEN         : in    std_logic;
          FRAMING_ERR : out   std_logic;
          BAUD_VAL    : in    std_logic_vector(12 downto 0);
          DATA_IN     : in    std_logic_vector(7 downto 0);
          DATA_OUT    : out   std_logic_vector(7 downto 0)
        );

end component;
--
--attribute syn_black_box	: boolean;
--attribute syn_black_box of USERUART	: component is true;

-------------------------------------------------
-- internal signals
-------------------------------------------------

signal    OEN_S                   : std_logic := '1';
signal    WEN_S                   : std_logic := '1';
signal    CSN_S                   : std_logic := '1'; 
signal    RXRDY_S                 : std_logic := '0'; 
signal    TXRDY_S                 : std_logic := '0'; 
signal    FRAMING_ERR_S           : std_logic := '0'; 
signal    PARITY_ERR_S            : std_logic := '0'; 
signal    OVERFLOW_S              : std_logic := '0'; 
signal    FRAMING_ERR_S1          : std_logic := '0'; 
signal    PARITY_ERR_S1           : std_logic := '0'; 
signal    OVERFLOW_S1             : std_logic := '0'; 
signal    DATA_FROM_UART         : std_logic_vector(7 downto 0);
signal    DATA_TO_UART          : std_logic_vector(7 downto 0);


type STADOS is (WAIT_OPERATION, WAIT_OPERATION_1, DECODE_OPERATION, SELECT_OP, 
				CLEAR_OEN_1, CLEAR_OEN_2, CLEAR_OEN_3, CLEAR_OEN_4, CLEAR_OEN_5, 
				CLEAR_OEN_6, CLEAR_OEN_7, CLEAR_OEN_8, CLEAR_OEN_9, CLEAR_OEN_10, CLEAR_OEN_11,
				READ_ADDR_1, READ_ADDR_2, READ_ADDR_3, READ_ADDR_4,
				READ_COMA, READ_QUESTION, READ_CR, READ_LF,
				READ_DATA_TO_WRITE_1, READ_DATA_TO_WRITE_2, READ_DATA_TO_WRITE_3, READ_DATA_TO_WRITE_4,
				WAIT_RESPONSE, WAIT_VALIDATION_W, WAIT_VALIDATION_R,
				SEND_DATA_RD, SEND_DATA_RD_1, SEND_FAIL_INFO, SEND_FAIL_INFO_1,
				INVALID_DATA, INVALID_DATA_1, VALID_DATA, VALID_DATA_1,
				SEND_CR_CHAR, SEND_CR_CHAR_1, SEND_LF_CHAR, SEND_LF_CHAR_1);  

signal STADO           :   STADOS; 


signal ADDRESS_UART_R			:	std_logic_vector (15 downto 0); 
signal RD_DATA_BUS_UART_R		:	std_logic_vector (15 downto 0); 
signal WD_DATA_BUS_UART_R		:	std_logic_vector (15 downto 0); 

signal COUNT_SEND_DATA			:	std_logic_vector (4 downto 0); 



-- time out signal
signal	TIME_OUT_UART    	:   std_logic_vector (15 downto 0):= (others => '0');      	-- time out counter 
signal	RST_TIME_OUT_UART	: 	std_logic;

-- Watch dog equivalent
signal	WATCH_DOG_TIMER_UART		:	std_logic_vector (15 downto 0);
signal	RELOAD_WD			:	std_logic := '0';


-- READ / WRITE FLAG
signal	READ_FLAG		:	std_logic := '0';
signal	WRITE_FLAG		:	std_logic := '0';
--signal	VALID_FLAG		:	std_logic := '0';
signal	INVALID_FLAG	:	std_logic := '0';

signal	FAILED_UART		:	std_logic := '0';
signal	NO_CLEAN_OEN	:	std_logic := '0';
signal	RESET_UART_FAILURE	:	std_logic := '0';

signal RD_DATA_BUS_UART_R_MSB1 : std_logic;
signal RD_DATA_BUS_UART_R_MSB2 : std_logic;
signal RD_DATA_BUS_UART_R_MSB3 : std_logic;
signal RD_DATA_BUS_UART_R_MSB4 : std_logic;

begin

RXRDY_o <= RXRDY_S;
TXRDY_o <= TXRDY_S;

-------------------------------------------------
-- component instantiations
-------------------------------------------------

UART1 : USERUART 
        port map (
            BAUD_VAL    =>	baud_val,
			BIT8        =>	bit8,
			CLK         =>	sys_clk,
			CSN         =>	CSN_S,
			DATA_IN     =>	DATA_TO_UART,
			DATA_OUT    =>	DATA_FROM_UART,
			ODD_N_EVEN  => 	odd_n_even,
			OEN         =>	OEN_S,
			OVERFLOW    =>	OVERFLOW_S1,
			PARITY_EN   => 	parity_en,
			PARITY_ERR  =>	PARITY_ERR_S1,
			RESET_N     =>	n_RESET	, 
			RX          =>	RXDATA,
			RXRDY       =>	RXRDY_S,
			TX          =>	TXDATA,
			TXRDY       =>	TXRDY_S,
			WEN         =>	WEN_S,
			FRAMING_ERR =>	FRAMING_ERR_S1
);

CSN_S			<= '0';

-------------------failure register process--------------------------------
process(sys_clk, n_RESET)   

Begin
if ( n_RESET= '0') then  
	FAILED_UART		<= '0';
	FRAMING_ERR_S	<= '0';
	PARITY_ERR_S 	<= '0';
	OVERFLOW_S   	<= '0';
	
elsif (sys_clk'event and sys_clk='1') then
	if ((FRAMING_ERR_S1 = '1') or (PARITY_ERR_S1 = '1') or (OVERFLOW_S1 = '1') ) then
		FAILED_UART		<=	'1' ;
		FRAMING_ERR_S	<=	FRAMING_ERR_S1 ;
		PARITY_ERR_S 	<=	PARITY_ERR_S1 ;
		OVERFLOW_S   	<=	OVERFLOW_S1   ;
		
	elsif (RESET_UART_FAILURE = '0') then
		FAILED_UART		<= '0';
		FRAMING_ERR_S	<= '0';
		PARITY_ERR_S 	<= '0';
		OVERFLOW_S   	<= '0';
	end if;
end if;
end process;


process (sys_clk, n_RESET)                                   
begin




if ( n_RESET= '0') then     
	-- DATA_FROM_UART		<= (others => '0');
    DATA_TO_UART  		<= (others => '0');
	UART_BIT_I_S		<= '0';
	RELOAD_WD			<= '0';
	WEN_S 				<= '1';
	OEN_S				<= '1';
	READ_FLAG			<= '0';
	WRITE_FLAG      	<= '0';
	--VALID_FLAG			<= '0';
	INVALID_FLAG    	<= '1';
	nCS_UART 			<= '1';
	ADDRESS_UART_R		<= (others => '0');		
	ADDRESS_UART		<= (others => '0');	
	--RD_DATA_BUS_UART_R	<= (others => '0');	
	WD_DATA_BUS_UART_R	<= (others => '0');		
	WD_DATA_BUS_UART	<= (others => '0');	
	RD_nWD_EN_UART 		<= '1';
	NO_CLEAN_OEN 		<= '0';
	COUNT_SEND_DATA		<= (others => '0');	
	RESET_UART_FAILURE	<= '0';
	STADO <= WAIT_OPERATION;
	
elsif (sys_clk'event and sys_clk='1') then
    case STADO is
		when WAIT_OPERATION =>
			WEN_S 				<= '1';
			OEN_S				<= '1';
			READ_FLAG			<= '0';
			WRITE_FLAG  		<= '0';
			RELOAD_WD			<= '0';
			nCS_UART 			<= '1';
			--VALID_FLAG			<= '0';
			INVALID_FLAG		<= '1';
			RD_nWD_EN_UART		<= '1';
			NO_CLEAN_OEN 		<= '0';
			RST_TIME_OUT_UART	<= '1';
			ADDRESS_UART_R		<= (others => '0');		
			ADDRESS_UART		<= (others => '0');	
			--RD_DATA_BUS_UART_R	<= (others => '0');	
			WD_DATA_BUS_UART_R	<= (others => '0');		
			WD_DATA_BUS_UART	<= (others => '0');	
			COUNT_SEND_DATA		<= (others => '0');		
			RESET_UART_FAILURE	<= '0';
			if (( RXRDY_S = '1') and (WAIT_UP_ADC_DATA = '1')) then
				STADO <= DECODE_OPERATION;
			else
				STADO <= WAIT_OPERATION_1;
			end if;
			
		when WAIT_OPERATION_1 =>
			RELOAD_WD	<='1';
			if (FAILED_UART = '1')then
				STADO <= INVALID_DATA;
			else
				STADO <= WAIT_OPERATION;		
			end if;
			
		when DECODE_OPERATION =>
			RESET_UART_FAILURE	<= '1';
			RELOAD_WD	<='1';
			if (((DATA_FROM_UART = asscii_R) or (DATA_FROM_UART = asscii_rl)) and (FAILED_UART = '0') )then
					READ_FLAG	<= '1';
					WRITE_FLAG  <= '0';
					OEN_S <= '0';
					STADO <= CLEAR_OEN_1;
			elsif (((DATA_FROM_UART = asscii_W) or (DATA_FROM_UART = asscii_wl)) and (FAILED_UART = '0') ) then
					READ_FLAG	<= '0';
					WRITE_FLAG  <= '1';
					OEN_S <= '0';
					STADO <= CLEAR_OEN_1;
			else
				STADO <= INVALID_DATA;
			end if;
		
		when CLEAR_OEN_1 =>
			if ( RXRDY_S = '0')then
				OEN_S <= '1';
				RELOAD_WD	<='0';
				STADO <= READ_ADDR_1;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
				STADO <= CLEAR_OEN_1;
			
			end if;
		
		--------------------------------------------------------
		-- Read Addres Operation -- 
		-- MSB_1--
		when READ_ADDR_1 =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (((DATA_FROM_UART >= asscii_0) and (DATA_FROM_UART <= asscii_9)) and (FAILED_UART = '0') )  then
					ADDRESS_UART_R(15 downto 12) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_2;
				elsif ( (((DATA_FROM_UART >= asscii_A) and (DATA_FROM_UART <= asscii_F)) or 
						((DATA_FROM_UART >= asscii_al) and (DATA_FROM_UART <= asscii_fl))) and (FAILED_UART = '0')  )then
					ADDRESS_UART_R(15 downto 12) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_2;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_ADDR_1;
			end if;
		
		when CLEAR_OEN_2 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_ADDR_2;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
				STADO <= CLEAR_OEN_2;
			end if;
			
		-- MSB_2--
		when READ_ADDR_2 =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (((DATA_FROM_UART >= asscii_0) and (DATA_FROM_UART <= asscii_9)) and (FAILED_UART = '0')) then
					ADDRESS_UART_R(11 downto 8) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_3;
				elsif ( (((DATA_FROM_UART >= asscii_A) and (DATA_FROM_UART <= asscii_F)) or 
						((DATA_FROM_UART >= asscii_al) and (DATA_FROM_UART <= asscii_fl)) ) and (FAILED_UART = '0') )then
					ADDRESS_UART_R(11 downto 8) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_3;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_ADDR_2;
			end if;	
		
		when CLEAR_OEN_3 =>
			if ( RXRDY_S = '0')then	
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_ADDR_3;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else									
				STADO <= CLEAR_OEN_3;               
			end if;                                 
		
		-- MSB_3--
		when READ_ADDR_3 =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (((DATA_FROM_UART >= asscii_0) and (DATA_FROM_UART <= asscii_9)) and (FAILED_UART = '0')) then
					ADDRESS_UART_R(7 downto 4) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_4;
				elsif ((((DATA_FROM_UART >= asscii_A) and (DATA_FROM_UART <= asscii_F)) or 
						((DATA_FROM_UART >= asscii_al) and (DATA_FROM_UART <= asscii_fl))) and (FAILED_UART = '0')) then
					ADDRESS_UART_R(7 downto 4) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_4;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_ADDR_3;
			end if;	
		
		when CLEAR_OEN_4 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_ADDR_4;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
		    else
		    	STADO <= CLEAR_OEN_4;
		    end if;
		
		-- MSB_4--
		
		when READ_ADDR_4 =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (((DATA_FROM_UART >= asscii_0) and (DATA_FROM_UART <= asscii_9)) and (FAILED_UART = '0')) then
					ADDRESS_UART_R(3 downto 0) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					STADO 		<= SELECT_OP;
				elsif ( (((DATA_FROM_UART >= asscii_A) and (DATA_FROM_UART <= asscii_F)) or 
						((DATA_FROM_UART >= asscii_al) and (DATA_FROM_UART <= asscii_fl))) and (FAILED_UART = '0'))then
					ADDRESS_UART_R(3 downto 0) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					STADO 		<= SELECT_OP;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_ADDR_4;
			end if;	
		
			
		when SELECT_OP =>
			RELOAD_WD	<='0';
			if ( RXRDY_S = '0')then											
				OEN_S <= '1';                           
				if ( READ_FLAG = '1') then              
					STADO <= READ_QUESTION;   
				elsif (WRITE_FLAG = '1') then           
					STADO <= READ_COMA;  		
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= SELECT_OP;
			end if;
			
		--------------------------------------------------------	
		-- Read coma mark --	
		when READ_COMA =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (DATA_FROM_UART = asscii_COMA) then
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_5;
				else
					STADO 		<= INVALID_DATA;
				end if;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_COMA;
			end if;	
		
		when CLEAR_OEN_5 =>													
			if ( RXRDY_S = '0')then																		
				RELOAD_WD	<='0';
				OEN_S <= '1';														
				STADO <= READ_DATA_TO_WRITE_1;	
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else																	
		    	STADO <= CLEAR_OEN_5;
			end if;
		
		-- read Data operation ---
		--------------------------------------------------------
		-- MSB_1--   
		when READ_DATA_TO_WRITE_1 =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (((DATA_FROM_UART >= asscii_0) and (DATA_FROM_UART <= asscii_9)) and (FAILED_UART = '0'))then
					WD_DATA_BUS_UART_R(15 downto 12) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_6;
				elsif ( (((DATA_FROM_UART >= asscii_A) and (DATA_FROM_UART <= asscii_F)) or 
						((DATA_FROM_UART >= asscii_al) and (DATA_FROM_UART <= asscii_fl)))and (FAILED_UART = '0') )then
					WD_DATA_BUS_UART_R(15 downto 12) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_6;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_DATA_TO_WRITE_1;
			end if;
		
		when CLEAR_OEN_6 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_DATA_TO_WRITE_2;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
            	STADO <= CLEAR_OEN_6;
			end if;
			
		-- MSB_2--
		when READ_DATA_TO_WRITE_2 =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (((DATA_FROM_UART >= asscii_0) and (DATA_FROM_UART <= asscii_9)) and (FAILED_UART = '0')) then
					WD_DATA_BUS_UART_R(11 downto 8) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_7;
				elsif ((((DATA_FROM_UART >= asscii_A) and (DATA_FROM_UART <= asscii_F)) or 
						((DATA_FROM_UART >= asscii_al) and (DATA_FROM_UART <= asscii_fl))) and (FAILED_UART = '0') )then
					WD_DATA_BUS_UART_R(11 downto 8) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_7;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_DATA_TO_WRITE_2;
			end if;	
			
		when CLEAR_OEN_7 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_DATA_TO_WRITE_3;	
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
				STADO <= CLEAR_OEN_7;
			end if;
			
		-- MSB_3--
		when READ_DATA_TO_WRITE_3 =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (((DATA_FROM_UART >= asscii_0) and (DATA_FROM_UART <= asscii_9)) and (FAILED_UART = '0'))then
					WD_DATA_BUS_UART_R(7 downto 4) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_8;
				elsif ((((DATA_FROM_UART >= asscii_A) and (DATA_FROM_UART <= asscii_F)) or 
						((DATA_FROM_UART >= asscii_al) and (DATA_FROM_UART <= asscii_fl))) and (FAILED_UART = '0'))then
					WD_DATA_BUS_UART_R(7 downto 4) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_8;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_DATA_TO_WRITE_3;
			end if;	
		
		when CLEAR_OEN_8 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_DATA_TO_WRITE_4;	
		    elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
		    	STADO <= CLEAR_OEN_8;
		    end if;
		
		-- MSB_4--
		when READ_DATA_TO_WRITE_4 =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (((DATA_FROM_UART >= asscii_0) and (DATA_FROM_UART <= asscii_9)) and (FAILED_UART = '0'))then
					WD_DATA_BUS_UART_R(3 downto 0) <= DATA_FROM_UART(3 downto 0);
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_9;
				elsif ((((DATA_FROM_UART >= asscii_A) and (DATA_FROM_UART <= asscii_F)) or
						((DATA_FROM_UART >= asscii_al) and (DATA_FROM_UART <= asscii_fl)))and (FAILED_UART = '0') )then
					WD_DATA_BUS_UART_R(3 downto 0) <= DATA_FROM_UART(3 downto 0) + x"9";
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_9;
				else
					STADO 		<= INVALID_DATA;
				end if;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_DATA_TO_WRITE_4;
			end if;	
		
		when CLEAR_OEN_9 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_QUESTION;	
	        elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
	        	STADO <= CLEAR_OEN_9;
	        end if;
	
		-- Read question mark --		
		when READ_QUESTION =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (DATA_FROM_UART = asscii_QUES) then
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_10;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_QUESTION;
			end if;	
		
		when CLEAR_OEN_10 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_CR;	
		    elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
		    	STADO <= CLEAR_OEN_10;
		    end if;
			
		-- Read CR --
		when READ_CR =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (DATA_FROM_UART = asscii_CR) then
					OEN_S <= '0';
					STADO 		<= CLEAR_OEN_11;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_CR;
			end if;	
		
		when CLEAR_OEN_11 =>
			if ( RXRDY_S = '0')then
				RELOAD_WD	<='0';
				OEN_S <= '1';
				STADO <= READ_LF;
			elsif (FAILED_UART = '1') then
				STADO <= INVALID_DATA;
			else
				STADO <= CLEAR_OEN_11;
			end if;
			
		-- Read LF --
		when READ_LF =>
			RST_TIME_OUT_UART <= '0';
			RELOAD_WD	<='1';
			if ( RXRDY_S = '1') then
				RST_TIME_OUT_UART <= '1';
				if (DATA_FROM_UART = asscii_LF) then
					OEN_S <= '0';
					ADDRESS_UART <= ADDRESS_UART_R;
					STADO 		<= WAIT_RESPONSE;
				else
					STADO 		<= INVALID_DATA;
				end if;
				
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= READ_LF;
			end if;	
		---------------------------------------------------
		
		---- SEND RESPONSE TO THE COMMAND
		when WAIT_RESPONSE =>
			NO_CLEAN_OEN<='1';
			RELOAD_WD <= '1';
			OEN_S <= '1';
			if (READ_FLAG = '1') then
				RD_nWD_EN_UART <= '1';
				nCS_UART <= '0';
				STADO <= WAIT_VALIDATION_R;
			elsif (WRITE_FLAG = '1')then
				RD_nWD_EN_UART 	<= '0';
				nCS_UART <= '0';
				WD_DATA_BUS_UART<= WD_DATA_BUS_UART_R;
				STADO <= WAIT_VALIDATION_W;
			end if;

		
		when WAIT_VALIDATION_W =>
			RST_TIME_OUT_UART <= '0';
			--nCS_UART <= '0';
			NO_CLEAN_OEN<='1';
			if (ACCESS_VALID_UART = DEV_DATA_OK) then
				STADO	<= VALID_DATA;
			elsif (ACCESS_VALID_UART = DEV_DATA_FL) then 
				STADO	<= INVALID_DATA;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= WAIT_VALIDATION_W;
			end if;
		

		when WAIT_VALIDATION_R =>
			RST_TIME_OUT_UART <= '0';
			--nCS_UART <= '0';
			NO_CLEAN_OEN<='1';
			COUNT_SEND_DATA		<= (others => '0');	
			if (ACCESS_VALID_UART = DEV_DATA_OK) then
				--RD_DATA_BUS_UART_R	<= RD_DATA_BUS_UART;
				STADO	<= SEND_DATA_RD;
			elsif (ACCESS_VALID_UART = DEV_DATA_FL) then 
				--RD_DATA_BUS_UART_R	<= (others => '0');
				STADO	<= INVALID_DATA;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= INVALID_DATA;
			else
				STADO <= WAIT_VALIDATION_R;
			end if;
			
		-- Send data adquired --
			when SEND_DATA_RD =>
				nCS_UART 	<= '1';
				WEN_S 		<= '1';
				RELOAD_WD	<='1';
				NO_CLEAN_OEN<='1';
				if (COUNT_SEND_DATA <= x"3") then	
					if (COUNT_SEND_DATA = x"0") then
					-- MSB 1 --
						if (RD_DATA_BUS_UART_R_MSB1='1') then
							DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(15 downto 12);
						else	
							DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(15 downto 12) - x"9");
						end if;
					-- MSB 2 --
					elsif (COUNT_SEND_DATA = x"1") then
						if (RD_DATA_BUS_UART_R_MSB2='1') then
							DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(11 downto 8);
						else	
							DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(11 downto 8) - x"9");
						end if;
					-- MSB 3 --
					elsif (COUNT_SEND_DATA = x"2") then
						if (RD_DATA_BUS_UART_R_MSB3='1') then
							DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(7 downto 4);
						else	
							DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(7 downto 4) - x"9");
						end if;
					-- MSB 4 --
					elsif (COUNT_SEND_DATA = x"3") then
						if (RD_DATA_BUS_UART_R_MSB4='1') then
							DATA_TO_UART (7 downto 0) <= x"3" & RD_DATA_BUS_UART_R(3 downto 0);
						else	
							DATA_TO_UART (7 downto 0) <= x"4" & (RD_DATA_BUS_UART_R(3 downto 0)- x"9");
						end if;
					end if;
					COUNT_SEND_DATA <= COUNT_SEND_DATA + x"1";
					STADO 		<= SEND_DATA_RD_1;
					
				elsif (COUNT_SEND_DATA > x"3") then
						STADO 		<= VALID_DATA;
				end if;
			
			when SEND_DATA_RD_1 =>
				NO_CLEAN_OEN<='1';
				if( TXRDY_S = '1') then
					WEN_S <= '0';
					RELOAD_WD	<='0';
					STADO <= SEND_DATA_RD;	
				elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
					STADO 		<= INVALID_DATA;
				else
						STADO <= SEND_DATA_RD_1;
				end if;
			
			
			
		--------------------------------------------------------	
		-- Send 'N' character INVALID ACCESS OR DATA-- 
		when INVALID_DATA =>
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= asscii_N;
			RST_TIME_OUT_UART <= '1';
			STADO 		<= INVALID_DATA_1;

		when INVALID_DATA_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				STADO <= SEND_FAIL_INFO;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= WAIT_OPERATION;
			else
				STADO <= INVALID_DATA_1;
			end if;
		
		-- Send 'y' character VALID ACCESS OR DATA-- 
		when VALID_DATA =>
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= asscii_Y;
			RST_TIME_OUT_UART <= '1';
			STADO 		<= VALID_DATA_1;

		when VALID_DATA_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				STADO <= SEND_CR_CHAR;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= WAIT_OPERATION;
			else
				STADO <= VALID_DATA_1;
			end if;
		
		-- send fail info -------
		when SEND_FAIL_INFO =>
			RST_TIME_OUT_UART <= '1';
			RELOAD_WD	<='1';
			STADO 		<= SEND_FAIL_INFO_1;
			if (FRAMING_ERR_S = '1') and (PARITY_ERR_S = '1') and (OVERFLOW_S = '1') then
				WEN_S <= '1';
				DATA_TO_UART<= asscii_7;
					
			elsif (FRAMING_ERR_S = '1') and (PARITY_ERR_S = '1') and (OVERFLOW_S = '0') then
				WEN_S <= '1';
				DATA_TO_UART<= asscii_6;

			elsif (FRAMING_ERR_S = '1') and (PARITY_ERR_S = '0') and (OVERFLOW_S = '1') then
				WEN_S <= '1';
				DATA_TO_UART<= asscii_5;
			elsif (FRAMING_ERR_S = '1') and (PARITY_ERR_S = '0') and (OVERFLOW_S = '0') then
				WEN_S <= '1';
				DATA_TO_UART<= asscii_4;	
			elsif (FRAMING_ERR_S = '0') and (PARITY_ERR_S = '1') and (OVERFLOW_S = '1') then
				WEN_S <= '1';
				DATA_TO_UART<= asscii_3;
			elsif (FRAMING_ERR_S = '0') and (PARITY_ERR_S = '1') and (OVERFLOW_S = '0') then
				WEN_S <= '1';
				DATA_TO_UART<= asscii_2;	
			elsif (FRAMING_ERR_S = '0') and (PARITY_ERR_S = '0') and (OVERFLOW_S = '1') then
				WEN_S <= '1';
				DATA_TO_UART<= asscii_1;	
			else
				WEN_S <= '1';
				DATA_TO_UART<= asscii_0;	
			end if;

		when SEND_FAIL_INFO_1 =>
			if (NO_CLEAN_OEN ='0') then
				OEN_S <= '0';
			else
				OEN_S <= '1';
			end if;
				
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				STADO <= SEND_CR_CHAR;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= WAIT_OPERATION;
			else
				STADO <= SEND_FAIL_INFO_1;
			end if;
	
		-- Send 'CR' character -- 
		when SEND_CR_CHAR =>
			NO_CLEAN_OEN <='0';
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= asscii_CR;
			RST_TIME_OUT_UART <= '1';
			STADO 		<= SEND_CR_CHAR_1;

		when SEND_CR_CHAR_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				STADO <= SEND_LF_CHAR;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= WAIT_OPERATION;
			else
				STADO <= SEND_CR_CHAR_1;
			end if;
			
		-- Send 'LF' character -- 
		when SEND_LF_CHAR =>
			WEN_S <= '1';
			RELOAD_WD	<='1';
			DATA_TO_UART<= asscii_LF;
			RST_TIME_OUT_UART <= '1';
			STADO 		<= SEND_LF_CHAR_1;

		when SEND_LF_CHAR_1 =>
			if( TXRDY_S = '1') then
				WEN_S <= '0';
				RELOAD_WD	<='0';
				STADO <= WAIT_OPERATION;
			elsif (TIME_OUT_UART > TIME_OUT_MX_UART) then
				STADO 		<= WAIT_OPERATION;
			else
				STADO <= SEND_LF_CHAR_1;
			end if;
			------------------------- 

		when others =>
			UART_BIT_I_S	<= '0';
			RELOAD_WD	<='0';
			STADO <= WAIT_OPERATION;
			
    end case;
end if;	

	
end process;

-- Rx Data timeout
process (sys_clk, n_RESET)                                   
begin

if ( n_RESET= '0') then     
	TIME_OUT_UART<=(others=>'0'); 
elsif (sys_clk'event and sys_clk='1') then 
	if (RST_TIME_OUT_UART='1') then
		TIME_OUT_UART<=(others=>'0'); 
	else
		TIME_OUT_UART<=TIME_OUT_UART+x"0001"; 
	end if;
end if;
end process; 
     
-- Watch Dog timer control, after 100 useg a fail is detected until this module is reset
process (sys_clk, n_RESET)                                   
begin

if ( n_RESET= '0') then     
	UART_BIT_W_D<='0'; 
	WATCH_DOG_TIMER_UART<=W_D_MAX_TIME_UART;
	
elsif (sys_clk'event and sys_clk='1') then 
	if (RELOAD_WD='1') then
		WATCH_DOG_TIMER_UART<=WATCH_DOG_TIMER_UART-'1';
		if (WATCH_DOG_TIMER_UART = x"0000") then
			UART_BIT_W_D<='1';
			WATCH_DOG_TIMER_UART<=W_D_MAX_TIME_UART;
		end if;
	elsif (RELOAD_WD='0') then
		WATCH_DOG_TIMER_UART<=W_D_MAX_TIME_UART;
	end if;
end if;
end process;

-- Rx DATA Register
process (sys_clk, n_RESET)                                   
begin

if ( n_RESET= '0') then     
	RD_DATA_BUS_UART_R<=(others=>'0'); 
elsif (sys_clk'event and sys_clk='1') then 
	if STADO=WAIT_VALIDATION_R AND ACCESS_VALID_UART=DEV_DATA_OK THEN
		RD_DATA_BUS_UART_R	<= RD_DATA_BUS_UART;
	end if;
end if;
end process;

RD_DATA_BUS_UART_R_MSB1 <= '1' when (RD_DATA_BUS_UART_R(15 downto 12) >= x"0") and (RD_DATA_BUS_UART_R(15 downto 12) <= x"9")
	else '0';
RD_DATA_BUS_UART_R_MSB2 <= '1' when (RD_DATA_BUS_UART_R(11 downto 8) >= x"0") and (RD_DATA_BUS_UART_R(11 downto 8) <= x"9")
	else '0';
RD_DATA_BUS_UART_R_MSB3 <= '1' when (RD_DATA_BUS_UART_R(7 downto 4) >= x"0") and (RD_DATA_BUS_UART_R(7 downto 4) <= x"9")
	else '0';
RD_DATA_BUS_UART_R_MSB4 <= '1' when (RD_DATA_BUS_UART_R(3 downto 0) >= x"0") and (RD_DATA_BUS_UART_R(3 downto 0) <= x"9")
	else '0';

end bhu;                           