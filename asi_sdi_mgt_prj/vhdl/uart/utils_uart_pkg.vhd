library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

package utils_uart_pkg is

type ACCESS_VALID_UART_type is (DEV_FREE,DEV_DATA_OK,DEV_DATA_FL,DEV_BUSY);  

--------------------------------
----- ASSCII TO HEX DECODE -----
--------------------------------
constant asscii_0	:	std_logic_vector (7 downto 0):=x"30"; -- dec 48
constant asscii_1	:	std_logic_vector (7 downto 0):=x"31"; -- dec 49
constant asscii_2	:	std_logic_vector (7 downto 0):=x"32"; -- dec 50
constant asscii_3	:	std_logic_vector (7 downto 0):=x"33"; -- dec 51
constant asscii_4	:	std_logic_vector (7 downto 0):=x"34"; -- dec 52
constant asscii_5	:	std_logic_vector (7 downto 0):=x"35"; -- dec 53
constant asscii_6	:	std_logic_vector (7 downto 0):=x"36"; -- dec 54
constant asscii_7	:	std_logic_vector (7 downto 0):=x"37"; -- dec 55
constant asscii_8	:	std_logic_vector (7 downto 0):=x"38"; -- dec 56
constant asscii_9	:	std_logic_vector (7 downto 0):=x"39"; -- dec 57
-- uppercase letter
constant asscii_A	:	std_logic_vector (7 downto 0):=x"41"; -- dec 65
constant asscii_B	:	std_logic_vector (7 downto 0):=x"42"; -- dec 66
constant asscii_C	:	std_logic_vector (7 downto 0):=x"43"; -- dec 67
constant asscii_D	:	std_logic_vector (7 downto 0):=x"44"; -- dec 68
constant asscii_E	:	std_logic_vector (7 downto 0):=x"45"; -- dec 69
constant asscii_F	:	std_logic_vector (7 downto 0):=x"46"; -- dec 70
-- lower case letter
constant asscii_al	:	std_logic_vector (7 downto 0):=x"61"; -- dec 97
constant asscii_bl	:	std_logic_vector (7 downto 0):=x"62"; -- dec 98
constant asscii_cl	:	std_logic_vector (7 downto 0):=x"63"; -- dec 99
constant asscii_dl	:	std_logic_vector (7 downto 0):=x"64"; -- dec 100
constant asscii_el	:	std_logic_vector (7 downto 0):=x"65"; -- dec 101
constant asscii_fl	:	std_logic_vector (7 downto 0):=x"66"; -- dec 102
-- Operation symbol
constant asscii_R	:	std_logic_vector (7 downto 0):=x"52"; -- dec 82
constant asscii_rl	:	std_logic_vector (7 downto 0):=x"72"; -- dec 114
constant asscii_W	:	std_logic_vector (7 downto 0):=x"57"; -- dec 87
constant asscii_wl	:	std_logic_vector (7 downto 0):=x"77"; -- dec 119
-- separator symbol
constant asscii_COMA:	std_logic_vector (7 downto 0):=x"2C"; -- dec 44
constant asscii_QUES:	std_logic_vector (7 downto 0):=x"3F"; -- dec 63
-- validate symbol
constant asscii_Y	:	std_logic_vector (7 downto 0):=x"59"; -- dec 89
constant asscii_N	:	std_logic_vector (7 downto 0):=x"4E"; -- dec 78
-- carrier Return and Line Feed
constant asscii_LF	:	std_logic_vector (7 downto 0):=x"0A"; -- dec 10
constant asscii_CR	:	std_logic_vector (7 downto 0):=x"0D"; -- dec 13  

end utils_uart_pkg;

package body utils_uart_pkg is



end utils_uart_pkg;