----------------------------------------------------------------------
-- Created by Actel SmartDesign Tue Jan 15 17:20:39 2013
-- Parameters for COREUART
----------------------------------------------------------------------


package coreparameters is
    constant FAMILY : integer := 17;
    constant HDL_license : string( 1 to 1 ) := "O";
    constant RX_FIFO : integer := 1;
    constant RX_LEGACY_MODE : integer := 0;
    constant testbench : string( 1 to 4 ) := "User";
    constant TX_FIFO : integer := 1;
    constant USE_SOFT_FIFO : integer := 1;
end coreparameters;
