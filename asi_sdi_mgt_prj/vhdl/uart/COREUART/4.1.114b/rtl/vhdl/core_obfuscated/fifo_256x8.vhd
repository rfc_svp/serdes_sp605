--  Copyright 2008 Actel Corporation.  All rights reserved.
-- ANY USE OR REDISTRIBUTION IN PART OR IN WHOLE MUST BE HANDLED IN
-- ACCORDANCE WITH THE ACTEL LICENSE AGREEMENT AND MUST BE APPROVED
--  Revision Information:
-- Jun09    Revision 4.1
-- SVN Revision Information:
-- SVN $Revision: 8508 $
library iEEE;
use ieee.std_loGIC_1164.all;
use IEEE.STD_logic_arith.all;
use ieee.STD_LOGic_unsigned.all;
entity CUARTooi is
port (CUARTloi: out stD_LOGIC_VECTOR(7 downto 0);
CUARTioi: in STD_LOgic;
CUARToli: in std_logic;
CUARTLLI: in Std_logic_vector(7 downto 0);
WRB: in STD_LOGIC;
rdb: in sTD_LOGIC;
reset: in STD_logic;
full: out std_logic;
EMPTY: out std_logic);
end entity CUARTooi;

architecture CUARTl0 of CUARTOOI is

component CUARTioil is
generic (CUARTOLIL: integer := 16;
CUARTLLil: integER := 4;
CUARTilil: INTEGER := 8);
port (CLOck: in std_logic;
reSET_N: in std_logic;
DATA_in: in stD_LOGIC_VECTOr(CUARTiLIL-1 downto 0);
CUARTOIil: in std_logic;
CUARTLIIL: in STD_LOGIC;
CUARTIiil: in Std_logic_vector(7 downto 0);
data_OUT: out STD_logic_vector(CUARTILIl-1 downto 0);
FULL: out Std_logic;
empty: out std_logic;
CUARTO0IL: out STD_Logic);
end component;

constant CUARTiIIL: std_lOGIC_VECTOR(7 downto 0) := "01000000";

signal CUARTl0il: std_logic_VECTOR(7 downto 0);

signal CUARTi0iL: std_logic;

signal CUARTo1IL: std_logic;

signal CUARTL1IL: stD_LOGIC;

signal CUARTi1il: STD_logic;

begin
CUARTLOI <= CUARTL0IL;
fulL <= CUARTI0IL;
empTY <= CUARTo1il;
CUARToo0L: CUARTIOIL
port map (data_in => CUARTLLI,
data_out => CUARTL0IL,
CUARTliil => wRB,
CUARToiil => RDB,
CLOck => CUARToli,
FULL => CUARTI0Il,
empty => CUARTo1IL,
CUARTO0il => CUARTi1il,
RESET_N => RESET,
CUARTIiil => CUARTiiil);
end architecture CUARTl0;

library ieee;
use IEEE.STD_LOGIC_1164.all;
use IEEE.stD_LOGIC_ARITH.all;
use ieee.std_logic_uNSIGNED.all;
entity CUARTioil is
generic (CUARTOLIL: iNTEGER := 16;
CUARTLLIL: INTEGER := 4;
CUARTiLIL: inteGER := 8); port (CLOCk: in stD_LOGIC;
REset_n: in std_logiC;
data_IN: in std_logic_veCTOR(CUARTilil-1 downto 0);
CUARToiil: in STD_LOGIC;
CUARTliil: in std_logiC;
CUARTiIIL: in STD_LOGic_vector(7 downto 0);
DATA_OUT: out std_logic_VECTOR(CUARTilil-1 downto 0);
full: out STD_LOGic;
EMPTY: out std_logic;
CUARTO0il: out stD_LOGIC);
end entity CUARTIoil;

architecture CUARTl0 of CUARTiOIL is

component CUARTlO0L is
generic (CUARTIO0L: INTEGER := 8;
CUARTOL0L: integeR := 16;
CUARTLL0L: integER := 4);
port (CLK: in STD_logic;
DATA_in: in STD_logic_vector(CUARTIO0L-1 downto 0);
CUARTil0l: in Std_logic_vector(CUARTLL0L-1 downto 0);
CUARToi0l: in STD_Logic_vector(CUARTll0l-1 downto 0);
CUARTliil: in STD_LOgic;
CUARToiIL: in STD_LOgic;
data_oUT: out std_logic_VECTOR(CUARTio0L-1 downto 0));
end component;

signal CUARTLI0l: std_logiC_VECTOR(CUARTilil-1 downto 0);

signal CUARTii0l: std_loGIC;

signal CUARTO00L: STD_Logic_vector(CUARTllil-1 downto 0);

signal CUARTl00l: Std_logic_vector(CUARTllil-1 downto 0);

signal CUARTI00L: STD_LOGIc_vector(CUARTLlil-1 downto 0);

signal CUARTO10l: STD_Logic;

signal CUARTl10l: STD_Logic;

signal CUARTo10: STD_LOGIC;

signal CUARTi10l: STD_LOGic_vector(CUARTILIL-1 downto 0);

signal CUARTi0il: sTD_LOGIC;

signal CUARTO1IL: Std_logic;

signal CUARToo1l: STD_LOgic;

begin
Data_out <= CUARTI10L;
FULL <= CUARTI0IL;
EMPTY <= CUARTo1iL;
CUARTo0IL <= CUARToO1L;
CUARTO10l <= '1' when (CUARTo00l = CONv_std_logic_vectOR(CUARTolil-1,
4)) else
'0';
CUARTI0IL <= CUARTo10L;
CUARTL10l <= '1' when (CUARTO00L = "0000") else
'0';
CUARTo1il <= CUARTl10l;
CUARTo10 <= '1' when (CUARTo00L >= CUARTIIIL) else
'0';
CUARTOO1L <= CUARTO10;
process (cLOCK,reset_n)
begin
if (not Reset_n = '1') then
CUARTl00L <= ( others => '0');
CUARTI00L <= ( others => '0');
CUARTo00l <= ( others => '0');
elsif (CLOCK'EVENT and CLOCK = '1') then
if (not CUARTOIIL = '1') then
if (CUARTLIIL = '1') then
CUARTO00L <= CUARTo00l-"0001";
end if;
if (CUARTl00l = conv_std_logIC_VECTOR(CUARTOLIL-1,
4)) then
CUARTl00l <= ( others => '0');
else
CUARTl00l <= CUARTl00l+"0001";
end if;
end if;
if (not CUARTliil = '1') then
if (CUARToiil = '1') then
CUARTo00l <= CUARTo00l+"0001";
end if;
if (CUARTi00l = CONV_STD_LOgic_vector(CUARTolil-1,
4)) then
CUARTi00l <= ( others => '0');
else
CUARTi00l <= CUARTi00L+"0001";
end if;
end if;
end if;
end process;
process (CLOCK,reset_n)
begin
if (not reset_N = '1') then
CUARTII0L <= '0';
elsif (clock'EVENT and cloCK = '1') then
CUARTii0L <= CUARToiil;
if (CUARTii0l = '0') then
CUARTI10L <= CUARTli0l;
else
CUARTI10l <= CUARTi10l;
end if;
end if;
end process;
CUARTlo1l: CUARTlo0l
port map (cLK => CLOCK,
DATA_IN => DATA_IN,
data_out => CUARTli0l,
CUARToi0l => CUARTi00L,
CUARTil0l => CUARTl00l,
CUARTLIIL => CUARTLIIL,
CUARToiil => CUARToiil);
end architecture CUARTl0;

library ieee;
use ieEE.stD_LOGIC_1164.all;
use ieee.std_logic_arITH.all;
use IEEE.Std_logic_unsigned.all;
entity CUARTLO0L is
generic (CUARTio0l: integer := 8;
CUARTol0l: integer := 16;
CUARTll0l: Integer := 4); port (clk: in STD_LOGIc;
DATA_IN: in STD_LOGIC_Vector(CUARTIO0L-1 downto 0);
CUARTIL0l: in sTD_LOGIC_VECTOr(CUARTll0l-1 downto 0);
CUARToi0l: in std_LOGIC_VECTOR(CUARTLL0L-1 downto 0);
CUARTliil: in std_logiC;
CUARToiIL: in std_logic;
DATA_OUT: out STD_LOGIC_vector(CUARTio0l-1 downto 0));
end entity CUARTlo0l;

architecture CUARTl0 of CUARTlo0l is

type CUARTiO1L is array (CUARTOL0L-1 downto 0) of STD_LOGIC_VECtor(CUARTIO0L-1 downto 0);

signal CUARTol1l: CUARTIO1L;

signal CUARTI10L: Std_logic_vector(CUARTio0l-1 downto 0);

function To_integer(val: std_logiC_VECTOR)
return integer is
constant CUARTll1l: STD_logic_vector(Val'High-VAL'low downto 0) := VAL;
variable CUARTil1l: INTEGER := 0;
begin
for CUARToi1l in CUARTll1L'range
loop
if (CUARTLL1L(CUARTOI1L) = '1') then
CUARTil1l := CUARTil1l+(2**CUARToi1l);
end if;
end loop;
return (CUARTil1L);
end To_integer;

begin
data_out <= CUARTi10l;
process (CLK)
variable CUARTli1l: CUARTIO1L;
begin
if (CLK'event and clK = '1') then
if (CUARTliil = '0') then
CUARTLI1l(TO_INTeger(CUARToi0l)) := data_iN;
end if;
end if;
CUARTol1l <= CUARTli1L;
end process;
process (clk)
variable CUARTIi1l: STD_LOGic_vector(CUARTIO0l-1 downto 0);
begin
if (cLK'event and clk = '1') then
if (CUARTOIIL = '0') then
CUARTII1L := CUARTol1l(To_integer(CUARTIl0l));
end if;
end if;
CUARTi10l <= CUARTii1l;
end process;
end architecture CUARTL0;
