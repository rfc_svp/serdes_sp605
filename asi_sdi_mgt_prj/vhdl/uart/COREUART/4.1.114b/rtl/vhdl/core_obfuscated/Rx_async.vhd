--  Copyright 2008 Actel Corporation.  All rights reserved.
-- ANY USE OR REDISTRIBUTION IN PART OR IN WHOLE MUST BE HANDLED IN
-- ACCORDANCE WITH THE ACTEL LICENSE AGREEMENT AND MUST BE APPROVED
--  Revision Information:
-- Jun09    Revision 4.1
-- SVN Revision Information:
-- SVN $Revision: 8508 $
library ieee;
use IEEE.std_lOGIC_1164.all;
use ieee.STD_LOGIC_arith.all;
use IEEE.STD_LOGIC_unsigned.all;
entity CUARTi0 is
generic (rx_fifo: Integer := 0); port (clk: in std_LOGIC;
CUARTl: in std_LOGIC;
RESet_n: in std_logiC;
bit8: in STD_LOGIC;
parity_en: in STD_LOGIC;
odd_n_even: in STd_logic;
CUARTO1: in Std_logic;
CUARTL1: in STD_logic;
CUARTI1: in STD_LOGIc;
rx: in STD_Logic;
OVERflow: out std_logic;
PARity_err: out std_LOGIC;
CUARTool: out std_logic;
CUARTLOL: out STD_LOGIC;
CUARTIOl: out STD_LOgic;
CUARTlll: out STD_logic;
CUARToll: out std_logic;
CUARTill: out std_logic;
CUARToil: out STD_logic_vector(7 downto 0);
CUARTLIL: out std_LOGIC);
end entity CUARTi0;

architecture CUARTl0 of CUARTi0 is

type CUARTo0li is (CUARTII0,CUARTl0li,CUARTI0LI);

signal CUARTo11: CUARTO0LI;

signal CUARTO1Li: std_logic_vector(3 downto 0);

signal CUARTl1li: std_LOGIC;

signal CUARTi1li: std_logic_vector(8 downto 0);

signal CUARTooii: STD_LOGIC;

signal CUARTLOII: STD_LOGIC_VECtor(3 downto 0);

signal CUARTIOIi: std_logic;

signal CUARTOLii: std_LOGIC_VECTOR(2 downto 0);

signal CUARTLLII: STD_LOgic;

signal CUARTILII: std_logic_vectOR(1 downto 0);

signal CUARToiii: sTD_LOGIC_VECTor(1 downto 0);

signal CUARTLIIi: STD_LOGIC_VECTor(3 downto 0);

signal CUARTO10: STD_LOGIC_vector(3 downto 0);

signal CUARTIIII: std_logic_vector(3 downto 0);

signal CUARTo0ii: std_logic_VECTOR(3 downto 0);

signal CUARTl0ii: Std_logic;

signal CUARTi0ii: STD_Logic;

signal CUARTo1ii: STD_LOgic;

signal CUARTl1ii: STd_logic;

signal CUARTI1II: std_lOGIC;

signal CUARTOO0I: std_logIC;

signal CUARTLO0I: STD_LOGIC;

signal CUARTio0i: STD_logic;

signal CUARTol0i: STD_logic_vector(7 downto 0);

signal CUARTLL0I: STD_LOGIC;

begin
CUARTiol <= CUARTi1ii;
CUARTool <= CUARTo1ii;
OVERflow <= CUARTl0ii;
PARIty_err <= CUARTi0ii;
CUARToll <= CUARToo0I;
CUARTILL <= CUARTio0i;
CUARTOIl <= CUARTOL0I;
CUARTLIL <= CUARTLL0I;
CUARTlol <= '1' when (CUARTo11 = CUARTii0) else
'0';
CUARTlLL <= CUARTLO0I;
CUARTIL0i:
process (clk,reset_n)
begin
if (RESEt_n = '0') then
CUARTolii <= "000";
elsif (CLK'EVENT and CLK = '1') then
if (CUARTl = '1') then
CUARTOLII(1 downto 0) <= CUARTOLII(2 downto 1);
CUARTolii(2) <= RX;
end if;
end if;
end process CUARTil0i;
process (CUARTOLii)
begin
case CUARTOLII is
when "000" =>
CUARTl1lI <= '0';
when "001" =>
CUARTl1LI <= '0';
when "010" =>
CUARTL1LI <= '0';
when "011" =>
CUARTl1li <= '1';
when "100" =>
CUARTL1LI <= '0';
when "101" =>
CUARTl1li <= '1';
when "110" =>
CUARTl1li <= '1';
when others =>
CUARTl1li <= '1';
end case;
end process;
CUARTOI0I:
process (cLK,rESET_N)
begin
if (reset_n = '0') then
CUARTO1LI <= "0000";
elsif (clk'EVENT and clk = '1') then
if (CUARTl = '1') then
if (CUARTo11 = CUARTii0 and (CUARTl1li = '1' or CUARTO1Li = "1000")) then
CUARTo1li <= "0000";
else
CUARTo1li <= CUARTo1li+"0001";
end if;
end if;
end if;
end process CUARTOi0i;
CUARTli0i:
process (clK,resET_N)
begin
if (resET_N = '0') then
CUARTL0II <= '0';
elsif (clk'event and clk = '1') then
if (CUARTL = '1') then
if (CUARTLLII = '1') then
CUARTl0ii <= '1';
end if;
end if;
if (CUARTO1 = '1') then
CUARTL0II <= '0';
end if;
end if;
end process CUARTli0i;
CUARTii0i:
process (CLK,RESET_N)
begin
if (reseT_N = '0') then
CUARTO1II <= '0';
elsif (clk'EVENT and CLK = '1') then
if (CUARTi1 = '1') then
CUARTO1II <= '0';
elsif (CUARTL = '1') then
if (CUARTl1ii = '1') then
CUARTo1iI <= '1';
end if;
else
CUARTO1Ii <= CUARTO1II;
end if;
end if;
end process CUARTIi0i;
CUARTo10 <= "1000" when (bit8 = '1' and parity_en = '0') else
"1001";
CUARTiiii <= "1000" when (BIT8 = '0' and parity_en = '1') else
CUARTo10;
CUARTO0II <= "0111" when (bit8 = '0' and pariTY_EN = '0') else
CUARTIIIi;
CUARTliII <= CUARTO0II;
CUARTo00i:
process (cLK,reset_n)
begin
if (RESET_N = '0') then
CUARTo11 <= CUARTII0;
CUARTol0i <= "00000000";
CUARTLLII <= '0';
CUARTl1ii <= '0';
CUARTi1ii <= '0';
elsif (CLK'EVEnt and clk = '1') then
if (CUARTl = '1') then
CUARTllii <= '0';
CUARTL1II <= '0';
CUARTI1II <= '0';
case CUARTo11 is
when CUARTII0 =>
if (CUARTo1li = "1000") then
CUARTo11 <= CUARTl0li;
else
CUARTo11 <= CUARTii0;
end if;
when CUARTl0LI =>
if (CUARTLoii = CUARTliii) then
CUARTo11 <= CUARTI0Li;
CUARTllii <= CUARTIOII;
if (CUARTioii = '0') then
CUARTol0i <= (bit8 and CUARTi1li(7))&CUARTI1LI(6 downto 0);
end if;
else
CUARTo11 <= CUARTl0li;
end if;
when CUARTi0li =>
if (CUARTo1li = "1110") then
if (CUARTl1li = '0') then
CUARTl1II <= '1';
end if;
elsif (CUARTO1LI = "1111") then
CUARTi1ii <= '1';
CUARTO11 <= CUARTiI0;
else
CUARTO11 <= CUARTI0LI;
end if;
when others =>
CUARTO11 <= CUARTii0;
end case;
end if;
end if;
end process CUARTo00i;
CUARTilii <= Bit8&PArity_en;
CUARTL00I:
process (clk,RESET_N)
begin
if (reSET_N = '0') then
CUARTi1li(8 downto 0) <= "000000000";
CUARTloii <= "0000";
elsif (clk'event and clk = '1') then
if (CUARTl = '1') then
if (CUARTO11 = CUARTii0) then
CUARTi1li(8 downto 0) <= "000000000";
CUARTLOII <= "0000";
else
if (CUARTo1li = "1111") then
CUARTloii <= CUARTloii+"0001";
case CUARTilii is
when "00" =>
CUARTi1li(5 downto 0) <= CUARTi1li(6 downto 1);
CUARTi1LI(6) <= CUARTL1LI;
when "11" =>
CUARTI1li(7 downto 0) <= CUARTi1li(8 downto 1);
CUARTi1li(8) <= CUARTL1li;
when others =>
CUARTI1Li(6 downto 0) <= CUARTI1li(7 downto 1);
CUARTi1li(7) <= CUARTl1li;
end case;
end if;
end if;
end if;
end if;
end process CUARTL00I;
CUARTi00i:
process (clk,Reset_n)
begin
if (RESET_n = '0') then
CUARTooii <= '0';
elsif (CLK'EVENT and CLK = '1') then
if (CUARTL = '1') then
if (CUARTO1LI = "1111" and Parity_en = '1') then
CUARTooii <= CUARTOOII xor CUARTL1LI;
end if;
if (CUARTo11 = CUARTi0li) then
CUARTooii <= '0';
end if;
end if;
end if;
end process CUARTI00I;
CUARTOIII <= bit8&odd_n_EVEN;
CUARTo10i:
process (clk,RESEt_n)
begin
if (rESET_N = '0') then
CUARTi0ii <= '0';
elsif (clk'EVent and Clk = '1') then
if ((CUARTL = '1' and parity_en = '1') and CUARTO1LI = "1111") then
case CUARToiii is
when "00" =>
if (CUARTloii = "0111") then
CUARTI0II <= CUARTooii xor CUARTL1LI;
end if;
when "01" =>
if (CUARTloii = "0111") then
CUARTI0II <= not (CUARTOOII xor CUARTl1li);
end if;
when "10" =>
if (CUARTloii = "1000") then
CUARTI0II <= CUARTooii xor CUARTL1LI;
end if;
when "11" =>
if (CUARTLOii = "1000") then
CUARTi0iI <= not (CUARTooii xor CUARTl1li);
end if;
when others =>
CUARTI0ii <= '0';
end case;
end if;
if (CUARTl1 = '1') then
CUARTi0ii <= '0';
end if;
end if;
end process CUARTo10i;
CUARTl10i:
process (clk,RESET_n)
begin
if (resET_N = '0') then
CUARTioii <= '0';
CUARTll0i <= '1';
CUARToo0I <= '0';
CUARTLO0I <= '0';
elsif (clk'EVENT and CLK = '1') then
CUARTll0i <= '1';
CUARTOO0I <= '0';
CUARTlo0i <= '0';
if (CUARTl = '1') then
if (BIT8 = '1') then
if (paritY_EN = '1') then
if (CUARTloii = "1001" and CUARTO11 = CUARTL0LI) then
CUARTLL0I <= '0';
CUARTOO0I <= '1';
CUARTlo0i <= '1';
if (rx_FIFO = 2#0#) then
CUARTIOII <= '1';
end if;
end if;
else
if (CUARTloii = "1000" and CUARTO11 = CUARTL0LI) then
CUARTll0i <= '0';
CUARToo0i <= '1';
CUARTLo0i <= '1';
if (RX_Fifo = 2#0#) then
CUARTIOIi <= '1';
end if;
end if;
end if;
else
if (parity_en = '1') then
if (CUARTloii = "1000" and CUARTo11 = CUARTl0LI) then
CUARTll0i <= '0';
CUARToo0i <= '1';
CUARTLO0I <= '1';
if (RX_fifo = 2#0#) then
CUARTIOII <= '1';
end if;
end if;
else
if (CUARTLoii = "0111" and CUARTo11 = CUARTl0li) then
CUARTll0i <= '0';
CUARTOO0I <= '1';
CUARTlo0i <= '1';
if (RX_fifo = 2#0#) then
CUARTIOII <= '1';
end if;
end if;
end if;
end if;
end if;
if (CUARTO1 = '1') then
CUARTIOII <= '0';
end if;
end if;
end process CUARTl10i;
CUARTIO0I <= CUARTioii;
end architecture CUARTl0;
