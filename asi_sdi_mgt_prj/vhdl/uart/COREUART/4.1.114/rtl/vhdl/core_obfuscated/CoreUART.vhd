--  Copyright 2008 Actel Corporation.  All rights reserved.
-- ANY USE OR REDISTRIBUTION IN PART OR IN WHOLE MUST BE HANDLED IN
-- ACCORDANCE WITH THE ACTEL LICENSE AGREEMENT AND MUST BE APPROVED
--  Revision Information:
-- Jun09    Revision 4.1
-- SVN Revision Information:
-- SVN $Revision: 8508 $
library Ieee;
use IEEE.std_logic_1164.all;
use ieee.std_lOGIC_ARIth.all;
use ieEE.std_logic_unsigNED.all;
entity COREUART is
generic (family: integer := 15;
tx_fiFO: integER := 0;
rx_fiFO: integer := 0;
rx_legacy_mode: Integer := 0); port (reset_N: in STd_logic;
CLK: in STD_LOGIC;
wen: in std_LOGIC;
OEN: in STD_LOgic;
csn: in std_logic;
DATA_IN: in std_logic_vECTOR(7 downto 0);
rx: in std_logic;
BAUD_val: in std_lOGIC_VECTOR(12 downto 0);
BIT8: in STD_LOGic;
parITY_EN: in STD_LOGIC;
ODD_N_even: in STd_logic;
parity_err: out std_logic;
overflow: out Std_logic;
TXRDY: out STD_LOGIC;
rxrdy: out Std_logic;
datA_OUT: out std_lOGIC_VECTOR(7 downto 0);
TX: out STd_logic;
framiNG_ERR: out std_logic);
end entity coreuART;

architecture CUARTL0 of COREUART is

component CUARTi0
generic (rx_fifo: integER := 0);
port (clk: in STD_LOGIC;
CUARTl: in std_lOGIC;
RESET_n: in std_logic;
bit8: in STD_LOGIC;
parity_en: in std_logic;
ODD_N_EVEN: in std_logic;
CUARTo1: in STD_LOGIC;
CUARTl1: in std_loGIC;
CUARTi1: in std_lOGIC;
rx: in std_logiC;
overflow: out std_logiC;
PARITY_ERR: out STD_logic;
CUARTOOL: out std_logic;
CUARTlol: out std_lOGIC;
CUARTiol: out std_logic;
CUARToll: out std_logic;
CUARTLLL: out STd_logic;
CUARTill: out Std_logic;
CUARTOIL: out STD_LOGIC_Vector(7 downto 0);
CUARTliL: out std_LOGIC);
end component;

component CUARTiil
generic (Tx_fifo: INTeger := 0);
port (clk: in std_logIC;
CUARTi: in std_logic;
RESet_n: in std_logic;
CUARTO0L: in STD_logic;
CUARTl0l: in STD_LOGIc_vector(7 downto 0);
CUARTI0L: in std_logiC_VECTOR(7 downto 0);
CUARTO1L: in Std_logic;
CUARTl1l: in STD_logic;
biT8: in STd_logic;
PARITY_En: in STD_LOGIC;
odd_n_evEN: in STd_logic;
txRDY: out STD_logic;
tx: out STD_LOGIC;
CUARTi1l: out STD_LOGIC);
end component;

component CUARTo
port (clk: in Std_logic;
RESET_n: in STD_LOgic;
baud_val: in STD_LOGic_vector(12 downto 0);
CUARTL: out stD_LOGIC;
CUARTi: out STD_LOGic);
end component;

component CUARTooi is
port (CUARTLOI: out std_logic_VECTOR(7 downto 0);
CUARTIOI: in Std_logic;
CUARTOLI: in STD_LOgic;
CUARTlli: in std_logic_vecTOR(7 downto 0);
wrb: in std_logic;
RDB: in std_logic;
reset: in STD_LOGIC;
full: out STD_LOGIC;
EMPTY: out STD_logic);
end component;

constant S0: STD_LOGIC_VECTor(1 downto 0) := "00";

constant s1: Std_logic_vector(1 downto 0) := "01";

constant CUARTiLI: std_logic_VECTOR(1 downto 0) := "10";

constant CUARToII: std_logic_veCTOR(1 downto 0) := "11";

signal CUARTliI: STD_LOGic;

signal CUARTill: STD_logic;

signal CUARTiii: std_logic;

signal CUARTlil: STD_Logic;

signal CUARTI: std_LOGIC;

signal CUARTl: STD_LOGIC;

signal CUARTo0l: STD_logic;

signal CUARTl0l: std_logic_vectOR(7 downto 0);

signal CUARTI0L: STD_LOGIc_vector(7 downto 0);

signal CUARTo0i: std_logiC_VECTOR(7 downto 0);

signal CUARTo1: Std_logic;

signal CUARTl0i: std_logic_veCTOR(7 downto 0);

signal CUARToil: STd_logic_vector(7 downto 0);

signal CUARTi0i: Std_logic_vector(7 downto 0);

signal CUARTO1I: STD_LOGIC;

signal CUARTl1i: STD_LOgic;

signal CUARTi1I: std_logic;

signal CUARToO0: std_logic;

signal CUARTi1l: stD_LOGIC;

signal CUARTlo0: stD_LOGIC;

signal CUARTio0: sTD_LOGIC;

signal CUARTL1: STD_logic;

signal CUARToll: std_logiC;

signal CUARTol0: std_loGIC;

signal CUARTll0: std_loGIC;

signal CUARTil0: STD_LOGIC;

signal CUARTIOL: STD_LOgic;

signal CUARTi1: std_logic;

signal CUARTlll: std_logic;

signal CUARTOI0: std_logic;

signal CUARTLI0: STD_logic;

signal CUARTii0: std_logic;

signal CUARTO00: STD_LOGIC;

signal CUARTl00: std_logIC;

signal CUARTi00: std_logic;

signal CUARTO10: STD_LOGIC;

signal CUARTl10: std_LOGIC;

signal CUARTi10: STD_logic;

signal CUARTOO1: STD_LOGIC;

signal CUARTlo1: std_logIC;

signal CUARTio1: std_logIC_VECTOR(7 downto 0);

signal CUARTOL1: STD_LOGic;

signal CUARTLL1: std_logIC;

signal CUARTil1: STD_logic;

signal CUARTOI1: std_logIC;

signal CUARTli1: STD_LOGIC;

signal CUARTii1: STD_LOGIC;

signal CUARTO01: stD_LOGIC_VECTOR(7 downto 0);

signal CUARTl01: std_logic;

signal CUARTi01: STD_LOGIC;

signal CUARTo11: STD_logic_vector(1 downto 0);

signal CUARTl11: std_loGIC_VECTOR(1 downto 0);

begin
FRAMING_ERR <= CUARTO00;
parity_err <= CUARTil1;
Overflow <= CUARToi1;
txrdy <= CUARTLI1;
RXRDY <= CUARTii1;
data_out <= CUARTo01;
TX <= CUARTl01;
CUARTI11:
process (clk,RESET_n)
begin
if (reset_n = '0') then
CUARTl0l <= '0'&'0'&'0'&'0'&'0'&'0'&'0'&'0';
CUARToo0 <= '1';
elsif (clK'event and clk = '1') then
CUARToo0 <= '1';
if (csn = '0' and wen = '0') then
CUARTL0l <= DATA_IN;
CUARTOo0 <= '0';
end if;
end if;
end process CUARTI11;
CUARTo10 <= '1' when (wen = '0' and csN = '0') else
'0';
CUARTO0L <= CUARTO10;
process (CUARTOIl,CUARTL0i,CUARTil1)
variable CUARToool: STd_logic_vector(7 downto 0);
begin
if (rx_fifo = 2#0#) then
CUARTOOOL := CUARTOIl;
else
if (CUARTil1 = '1') then
CUARTOOOL := CUARToil;
else
CUARTOOOL := CUARTl0i;
end if;
end if;
CUARTo01 <= CUARToooL;
end process;
CUARTL10 <= '1' when (csn = '0' and oen = '0') else
'0';
CUARTI10 <= (CUARTl10) when (rx_fifo = 2#0#) else
not CUARTIO0;
CUARTo1 <= CUARTI10;
CUARTOO1 <= '1' when (CSN = '0' and OEN = '0') else
'0';
CUARTlo1 <= CUARTLL0 when Rx_fifo /= 0 else
(CUARToo1);
CUARTl1 <= CUARTlo1;
CUARTio1 <= CUARToil when (CUARTIL1 = '0') else
"00000000";
CUARTi0i <= CUARTio1;
CUARTI1 <= CUARTli0 when rx_fifo /= 0 else
(CUARToo1);
CUARTI00 <= '1' when (csn = '0' and OEN = '0') else
'0';
CUARTlooL:
if (rx_legacy_mode = 1)
generate
process (CUARTill,CUARTi01)
variable CUARTIOOL: std_logic;
begin
if (RX_fifo = 2#0#) then
CUARTIOol := CUARTILL;
else
CUARTiool := not CUARTI01;
end if;
CUARTII1 <= CUARTIOOL;
end process;
end generate;
CUARTolol:
if (rx_legacy_MODE = 0)
generate
process (CLK,RESET_n)
begin
if (reset_n = '0') then
CUARTii1 <= '0';
elsif (CLK'EVENT and CLK = '1') then
if (RX_FIFO = 0) then
if (CUARTioL = '1' or CUARTill = '0') then
CUARTII1 <= CUARTILL;
end if;
else
if (CUARTIOl = '1' or CUARTI01 = '1'
or (CUARTi01 = '0' and CUARTII0 = '1')) then
CUARTiI1 <= not CUARTi01;
end if;
end if;
end if;
end process;
end generate;
process (Clk,REset_n)
begin
if (RESet_n = '0') then
CUARTLL0 <= '0';
CUARTOL0 <= '0';
elsif (CLK'event and clk = '1') then
CUARTOL0 <= CUARTolL;
CUARTLl0 <= CUARTOL0;
end if;
end process;
process (clk,reset_n)
begin
if (reset_n = '0') then
CUARTli0 <= '0';
CUARTOI0 <= '0';
elsif (cLK'EVENT and CLK = '1') then
CUARToi0 <= CUARTLll;
CUARTLI0 <= CUARTOI0;
end if;
end process;
process (CLK,RESET_N)
begin
if (reset_N = '0') then
CUARTo11 <= S0;
elsif (CLK'EVEnt and CLK = '1') then
CUARTo11 <= CUARTL11;
end if;
end process;
process (CUARTO11,CUARTi01,CUARTL1i)
begin
CUARTL11 <= CUARTO11;
CUARTi1i <= '1';
CUARTil0 <= '0';
case CUARTO11 is
when S0 =>
if (CUARTI01 = '1' and CUARTl1i = '0') then
CUARTL11 <= s1;
CUARTi1i <= '0';
end if;
when s1 =>
CUARTl11 <= CUARTili;
when CUARTILI =>
CUARTL11 <= CUARTOII;
when CUARToii =>
CUARTL11 <= S0;
CUARTIL0 <= '1';
when others =>
CUARTL11 <= CUARTO11;
end case;
end process;
process (CLk,RESET_N)
begin
if (reset_n = '0') then
CUARTL0I <= "00000000";
elsif (clk'evENT and clk = '1') then
if (CUARTil0 = '1') then
CUARTl0i <= CUARTo0i;
end if;
end if;
end process;
process (clk,reset_n)
begin
if (RESet_n = '0') then
CUARTI01 <= '1';
elsif (clk'event and clk = '1') then
if (CUARTIl0 = '1') then
CUARTi01 <= '0';
else
if (csN = '0' and oen = '0') then
CUARTi01 <= '1';
end if;
end if;
end if;
end process;
process (clk,RESET_N)
begin
if (RESET_N = '0') then
CUARTl00 <= '0';
elsif (CLK'EVENT and clk = '1') then
if (CUARTlil = '0' and CUARTio0 = '1') then
CUARTL00 <= '1';
elsif (CUARTi00 = '1') then
CUARTl00 <= '0';
else
CUARTL00 <= CUARTL00;
end if;
end if;
end process;
CUARTol1 <= CUARTl00 when RX_Fifo /= 0 else
CUARTLII;
CUARToi1 <= CUARTol1;
CUARTll1 <= '1' when (CUARTil1 = '1' or CUARTio0 = '1') else
CUARTLIL;
CUARTiii <= CUARTll1;
CUARTLLol: CUARTo
port map (CLK => CLK,
reSET_N => RESET_N,
baud_val => baud_vaL,
CUARTl => CUARTl,
CUARTI => CUARTi);
CUARTilol: CUARTiIL
generic map (tx_fifo => tx_fiFO)
port map (clk => cLK,
CUARTI => CUARTi,
RESET_n => reSET_N,
CUARTO0L => CUARTO0L,
CUARTL0L => CUARTl0l,
CUARTi0l => CUARTI0L,
CUARTO1L => CUARTo1i,
CUARTL1l => CUARTLO0,
Bit8 => BIT8,
paritY_EN => PARIty_en,
ODD_N_EVEN => odd_n_even,
txrdy => CUARTLI1,
Tx => CUARTL01,
CUARTI1L => CUARTi1l);
CUARToiol: CUARTi0
generic map (RX_FIFO => RX_FIFO)
port map (CLK => CLK,
CUARTl => CUARTL,
RESET_N => reset_n,
BIT8 => BIt8,
PARITY_en => PARITY_EN,
ODD_N_Even => ODD_N_EVEN,
CUARTo1 => CUARTO1,
CUARTL1 => CUARTl1,
RX => RX,
overflow => CUARTlii,
PARITY_Err => CUARTil1,
CUARToll => CUARTOLL,
CUARTill => CUARTill,
CUARTOIL => CUARToil,
CUARTlil => CUARTliL,
CUARTi1 => CUARTI1,
CUARTlll => CUARTlll,
CUARTOOL => CUARTo00,
CUARTLol => CUARTIi0,
CUARTioL => CUARTIOL);
CUARTliol:
if (TX_FIFO = 2#1#)
generate
CUARTIIOL: CUARTooi
port map (CUARTloi => CUARTI0l,
CUARTioi => clk,
CUARToli => clK,
CUARTLLI => CUARTL0L,
WRB => CUARToo0,
rdb => CUARTI1L,
RESET => RESET_N,
Full => CUARTlo0,
empty => CUARTO1i);
end generate;
CUARTO0Ol:
if (rx_fIFO = 2#1#)
generate
CUARTl0ol: CUARTooi
port map (CUARTLOI => CUARTo0I,
CUARTIOI => CLK,
CUARToli => Clk,
CUARTlli => CUARTI0I,
WRb => CUARTiii,
rdB => CUARTI1I,
Reset => reset_n,
full => CUARTIO0,
EMPTY => CUARTl1i);
end generate;
end architecture CUARTl0;
