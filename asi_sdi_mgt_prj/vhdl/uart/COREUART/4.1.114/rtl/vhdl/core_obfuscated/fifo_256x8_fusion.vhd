--  Copyright 2008 Actel Corporation.  All rights reserved.
-- ANY USE OR REDISTRIBUTION IN PART OR IN WHOLE MUST BE HANDLED IN
-- ACCORDANCE WITH THE ACTEL LICENSE AGREEMENT AND MUST BE APPROVED
--  Revision Information:
-- Jun09    Revision 4.1
-- SVN Revision Information:
-- SVN $Revision: 8508 $
library ieee;
use ieee.STD_logic_1164.all;
library fusioN;
entity CUARTooi is
port (CUARTloi: out std_logic_VECTOR(7 downto 0);
CUARTioi: in STD_LOGIC;
CUARTOLI: in STD_LOGIC;
CUARTlli: in std_logiC_VECTOR(7 downto 0);
WRB: in std_logic;
rdb: in std_LOGIC;
RESet: in std_logIC;
FULL: out STD_logic;
empty: out STD_LOGIC);
end entity CUARTOOI;

architecture CUARTl0 of CUARTooi is

component CUARTioli
port (CUARTiooi: in STD_LOGIC_VECtor(7 downto 0);
q: out stD_LOGIC_VECTOr(7 downto 0);
CUARTiloi: in STD_LOgic;
re: in std_LOGIC;
CUARTOLI: in STD_logic;
CUARTioi: in std_lOGIC;
full: out sTD_LOGIC;
EMPTY: out STD_LOGIc;
RESET: in std_logic;
aempty: out std_logic;
afull: out std_logiC;
CUARTIIIL: in std_LOGIC_VECTOR(7 downto 0));
end component;

constant CUARTiiil: std_logic_vectoR(7 downto 0) := "11111111";

signal aempty: STD_LOGIC;

signal AFULL: std_logic;

signal CUARTo01l: STD_LOGIc_vector(7 downto 0);

signal CUARTl0il: STD_LOGIC_vector(7 downto 0);

signal CUARTi0iL: STD_logic;

signal CUARTo1il: STD_LOGIC;

signal CUARTl1il: std_LOGIC;

signal CUARTI1IL: std_logic;

begin
CUARTloi <= CUARTl0il;
FULL <= CUARTi0il;
EMPTy <= CUARTo1il;
process (CUARTIOI)
begin
if (CUARTIOI'EVENT and CUARTIOI = '1') then
CUARTl0il <= CUARTO01L;
end if;
end process;
CUARToLLI: CUARTioli
port map (CUARTiooi => CUARTllI,
q => CUARTO01L,
CUARTiloi => wrb,
rE => rdb,
CUARTOLI => CUARToli,
CUARTIOI => CUARTIOi,
aempty => aempty,
afull => CUARTI1IL,
FULL => CUARTi0il,
empty => CUARTO1IL,
REset => RESET,
CUARTiiil => CUARTIiil);
end architecture CUARTl0;

library IEEE;
use ieee.std_logic_1164.all;
library Fusion;
entity CUARTioli is
port (CUARTiooi: in std_logic_vECTOR(7 downto 0);
Q: out std_loGIC_VECTOR(7 downto 0);
CUARTILOI: in STD_logic;
re: in STD_logic;
CUARToLI: in std_logiC;
CUARTIoi: in STD_LOGic;
full: out STD_logic;
empty: out STd_logic;
reSET: in std_logic;
AEMPTY: out std_logic;
afuLL: out std_logic;
CUARTiiil: in std_logic_vector(7 downto 0));
end entity CUARTioli;

architecture CUARTL0 of CUARTioli is

component INV
port (a: in STD_logic := 'U';
y: out std_logic);
end component;

component FIFO4k18
port (aeval11,aeval10,aeval9,aeval8,aeval7,Aeval6,AEVAL5,AEVAL4,AEVAL3,aeval2,Aeval1,AEVAL0,afval11,afval10,AFVAL9,afvAL8,AFVAL7,afvaL6,AFVAL5,afval4,afval3,AFVAL2,AFVAL1,afval0,wd17,wd16,wd15,wd14,wd13,WD12,WD11,wd10,WD9,wd8,Wd7,wd6,wd5,WD4,Wd3,wd2,wd1,wd0,ww0,ww1,ww2,rw0,RW1,RW2,RPIPE,WEN,ren,WBLK,RBLK,wclk,RCLK,reset,ESTop,fstop: in STD_LOGIC := 'U';
RD17,rd16,rd15,RD14,RD13,RD12,rd11,rd10,RD9,RD8,RD7,rd6,Rd5,RD4,rd3,Rd2,RD1,rd0,FULL,afull,emPTY,AEMpty: out std_logic);
end component;

component VCC
port (y: out Std_logic);
end component;

component GND
port (Y: out std_logIC);
end component;

signal CUARTLLLI: STD_LOGIC;

signal CUARTo0oi: std_LOGIC;

signal CUARTL0OI: std_logic;

signal CUARTi0oi: STD_Logic_vector(7 downto 0);

signal CUARTi0il: STD_LOGic;

signal CUARTo1il: std_loGIC;

signal CUARTILLI: std_logiC;

signal CUARTOILI: std_logic;

begin
Q <= CUARTI0OI;
fulL <= CUARTi0il;
EMPTY <= CUARTO1IL;
aempTY <= CUARTilli;
AFULL <= CUARToili;
CUARTo1oi: VCC
port map (y => CUARTo0oi);
CUARTl1oi: GND
port map (Y => CUARTL0OI);
CUARTlili: INV
port map (A => re,
y => CUARTllli);
CUARTiili: FIFO4k18
port map (AEVAL11 => CUARTL0OI,
aeval10 => CUARTl0oi,
aeval9 => CUARTL0OI,
aeval8 => CUARTl0oi,
aeVAL7 => CUARTl0oi,
aeval6 => CUARTL0OI,
Aeval5 => CUARTL0oi,
aevaL4 => CUARTL0OI,
aeval3 => CUARTo0OI,
AEVAL2 => CUARTl0oi,
AEVAL1 => CUARTL0oi,
aeval0 => CUARTL0OI,
AFVAL11 => CUARTL0OI,
afvAL10 => CUARTIiil(7),
afval9 => CUARTIIil(6),
Afval8 => CUARTiiil(5),
aFVAL7 => CUARTIIil(4),
AFVAL6 => CUARTIIIL(3),
afval5 => CUARTIIIL(2),
AFVAl4 => CUARTIIIL(1),
afvAL3 => CUARTIIIL(0),
AFVAL2 => CUARTl0oi,
AFVAL1 => CUARTl0oi,
afVAL0 => CUARTL0OI,
WD17 => CUARTL0OI,
WD16 => CUARTl0OI,
WD15 => CUARTL0oi,
wd14 => CUARTl0oi,
wd13 => CUARTL0OI,
WD12 => CUARTL0OI,
WD11 => CUARTl0oi,
wd10 => CUARTl0oi,
wD9 => CUARTl0oi,
wd8 => CUARTL0oi,
wd7 => CUARTiooi(7),
wd6 => CUARTIOOI(6),
WD5 => CUARTIOOI(5),
wd4 => CUARTiooi(4),
wd3 => CUARTIOOI(3),
WD2 => CUARTiooi(2),
wd1 => CUARTiooi(1),
wd0 => CUARTiooI(0),
ww0 => CUARTO0OI,
WW1 => CUARTo0oi,
ww2 => CUARTL0OI,
RW0 => CUARTO0OI,
RW1 => CUARTo0oi,
rw2 => CUARTL0OI,
RPIPE => CUARTl0oi,
wen => CUARTiloi,
REN => CUARTLLLI,
WBLK => CUARTL0OI,
RBLK => CUARTl0oi,
WCLK => CUARTolI,
RClk => CUARTioi,
Reset => RESET,
estop => CUARTo0oi,
FSTOP => CUARTO0Oi,
rd17 => open ,
rd16 => open ,
RD15 => open ,
rd14 => open ,
rD13 => open ,
RD12 => open ,
rd11 => open ,
rd10 => open ,
rD9 => open ,
RD8 => open ,
rd7 => CUARTI0OI(7),
rd6 => CUARTI0OI(6),
rd5 => CUARTi0oi(5),
rd4 => CUARTI0OI(4),
RD3 => CUARTI0oi(3),
rd2 => CUARTi0oi(2),
rd1 => CUARTi0oi(1),
rd0 => CUARTi0oi(0),
FULl => open ,
afull => CUARTi0IL,
EMPTy => CUARTo1il,
aempty => CUARTilli);
end architecture CUARTL0;
