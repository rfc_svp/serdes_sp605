--  Copyright 2008 Actel Corporation.  All rights reserved.
-- ANY USE OR REDISTRIBUTION IN PART OR IN WHOLE MUST BE HANDLED IN
-- ACCORDANCE WITH THE ACTEL LICENSE AGREEMENT AND MUST BE APPROVED
--  Revision Information:
-- Jun09    Revision 4.1
-- SVN Revision Information:
-- SVN $Revision: 8508 $
library ieee;
use ieee.std_logic_1164.all;
use ieee.Std_logic_aRITH.all;
use ieee.STD_logic_unsigned.all;
entity CUARTo is
port (clk: in stD_LOGIC;
reset_n: in sTD_LOGIC;
baud_vAL: in STD_logic_vector(12 downto 0);
CUARTL: out sTD_LOGIC;
CUARTi: out STD_logic);
end CUARTo;

architecture CUARTOL of CUARTO is

signal CUARTLL: std_logic_vecTOR(12 downto 0);

signal CUARTil: std_logic;

signal CUARTOI: Std_logic;

signal CUARTLI: STD_LOGIC_Vector(3 downto 0);

begin
CUARTII:
process (CLK,RESET_N)
begin
if (reset_n = '0') then
CUARTll <= "0000000000000";
CUARTil <= '0';
elsif (CLK'event and Clk = '1') then
if (CUARTll = "0000000000000") then
CUARTll <= BAUD_val;
CUARTil <= '1';
else
CUARTLL <= CUARTLL-'1';
CUARTil <= '0';
end if;
end if;
end process;
CUARTO0:
process (CLK,reset_n)
begin
if (RESET_N = '0') then
CUARTli <= "0000";
CUARToi <= '0';
elsif (Clk'EVENT and clk = '1') then
if (CUARTil = '1') then
CUARTli <= CUARTli+'1';
if (CUARTli = "1111") then
CUARToi <= '1';
else
CUARToi <= '0';
end if;
end if;
end if;
end process;
CUARTI <= CUARToi and CUARTil;
CUARTL <= CUARTil;
end CUARTOL;
