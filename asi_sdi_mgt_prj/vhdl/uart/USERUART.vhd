-- Version: 9.0 SP3A 9.0.3.7

library ieee;
use ieee.std_logic_1164.all;
--library fusion;
--use fusion.all;
--library COREUART_LIB;
--use COREUART_LIB.all;

entity USERUART is

    port( BIT8        : in    std_logic;
          CLK         : in    std_logic;
          CSN         : in    std_logic;
          ODD_N_EVEN  : in    std_logic;
          OEN         : in    std_logic;
          OVERFLOW    : out   std_logic;
          PARITY_EN   : in    std_logic;
          PARITY_ERR  : out   std_logic;
          RESET_N     : in    std_logic;
          RX          : in    std_logic;
          RXRDY       : out   std_logic;
          TX          : out   std_logic;
          TXRDY       : out   std_logic;
          WEN         : in    std_logic;
          FRAMING_ERR : out   std_logic;
          BAUD_VAL    : in    std_logic_vector(12 downto 0);
          DATA_IN     : in    std_logic_vector(7 downto 0);
          DATA_OUT    : out   std_logic_vector(7 downto 0)
        );

end USERUART;

architecture DEF_ARCH of USERUART is 

  -- component VCC
    -- port( Y : out   std_logic
        -- );
  -- end component;

  -- component GND
    -- port( Y : out   std_logic
        -- );
  -- end component;

  component COREUART
    generic (FAMILY:integer := 0; RX_FIFO:integer := 0; 
        RX_LEGACY_MODE:integer := 0; TX_FIFO:integer := 0);

    port( BIT8        : in    std_logic := 'U';
          CLK         : in    std_logic := 'U';
          CSN         : in    std_logic := 'U';
          ODD_N_EVEN  : in    std_logic := 'U';
          OEN         : in    std_logic := 'U';
          OVERFLOW    : out   std_logic;
          PARITY_EN   : in    std_logic := 'U';
          PARITY_ERR  : out   std_logic;
          RESET_N     : in    std_logic := 'U';
          RX          : in    std_logic := 'U';
          RXRDY       : out   std_logic;
          TX          : out   std_logic;
          TXRDY       : out   std_logic;
          WEN         : in    std_logic := 'U';
          FRAMING_ERR : out   std_logic;
          BAUD_VAL    : in    std_logic_vector(12 downto 0) := (others => 'U');
          DATA_IN     : in    std_logic_vector(7 downto 0) := (others => 'U');
          DATA_OUT    : out   std_logic_vector(7 downto 0)
        );
  end component;
  
--	attribute syn_black_box	: boolean;
----	attribute syn_black_box of VCC			: component is true;
----	attribute syn_black_box of GND			: component is true;
--	attribute syn_black_box of COREUART		: component is true;

    signal GND_net, VCC_net : std_logic;

begin 


    -- \VCC\ : VCC
      -- port map(Y => VCC_net);
    
    -- \GND\ : GND
      -- port map(Y => GND_net);
    
    USERUART_0 : COREUART
      generic map(FAMILY => 17, RX_FIFO => 1, RX_LEGACY_MODE => 0,
         TX_FIFO => 1)

      port map(BIT8 => BIT8, CLK => CLK, CSN => CSN, ODD_N_EVEN
         => ODD_N_EVEN, OEN => OEN, OVERFLOW => OVERFLOW, 
        PARITY_EN => PARITY_EN, PARITY_ERR => PARITY_ERR, RESET_N
         => RESET_N, RX => RX, RXRDY => RXRDY, TX => TX, TXRDY
         => TXRDY, WEN => WEN, FRAMING_ERR => FRAMING_ERR, 
        BAUD_VAL(12) => BAUD_VAL(12), BAUD_VAL(11) => 
        BAUD_VAL(11), BAUD_VAL(10) => BAUD_VAL(10), BAUD_VAL(9)
         => BAUD_VAL(9), BAUD_VAL(8) => BAUD_VAL(8), BAUD_VAL(7)
         => BAUD_VAL(7), BAUD_VAL(6) => BAUD_VAL(6), BAUD_VAL(5)
         => BAUD_VAL(5), BAUD_VAL(4) => BAUD_VAL(4), BAUD_VAL(3)
         => BAUD_VAL(3), BAUD_VAL(2) => BAUD_VAL(2), BAUD_VAL(1)
         => BAUD_VAL(1), BAUD_VAL(0) => BAUD_VAL(0), DATA_IN(7)
         => DATA_IN(7), DATA_IN(6) => DATA_IN(6), DATA_IN(5) => 
        DATA_IN(5), DATA_IN(4) => DATA_IN(4), DATA_IN(3) => 
        DATA_IN(3), DATA_IN(2) => DATA_IN(2), DATA_IN(1) => 
        DATA_IN(1), DATA_IN(0) => DATA_IN(0), DATA_OUT(7) => 
        DATA_OUT(7), DATA_OUT(6) => DATA_OUT(6), DATA_OUT(5) => 
        DATA_OUT(5), DATA_OUT(4) => DATA_OUT(4), DATA_OUT(3) => 
        DATA_OUT(3), DATA_OUT(2) => DATA_OUT(2), DATA_OUT(1) => 
        DATA_OUT(1), DATA_OUT(0) => DATA_OUT(0));
    

end DEF_ARCH; 
